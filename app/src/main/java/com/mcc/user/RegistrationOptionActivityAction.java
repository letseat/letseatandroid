package com.mcc.user;

		import java.util.ArrayList;
		import java.util.Arrays;

		import android.app.Activity;
		import android.app.AlertDialog;
		import android.content.Intent;
		import android.graphics.Color;
		import android.os.AsyncTask;
		import android.os.Bundle;
		import android.util.Log;
		import android.view.Menu;
		import android.view.MotionEvent;
		import android.view.View;
		import android.view.View.OnClickListener;
		import android.view.View.OnTouchListener;
		import android.widget.LinearLayout;
		import android.widget.TextView;
		import android.widget.Toast;

		import com.facebook.CallbackManager;
		import com.facebook.FacebookCallback;
		import com.facebook.FacebookException;
		import com.facebook.GraphRequest;
		import com.facebook.GraphResponse;

		import com.facebook.login.LoginResult;
		import com.foody.AppData.StaticObjects;
		import com.foody.jsondata.FoodyResturent;
		import com.foody.jsondata.user.User;
		import com.google.android.gms.analytics.GoogleAnalytics;
		import com.google.android.gms.common.SignInButton;
		import com.mcc.googlepluslogin.MainActivity;
		import com.mcc.letseat.HomeT;
		import com.mcc.letseat.R;
		import com.mcc.libs.AnimationTween;

		import com.facebook.FacebookSdk;

		import org.json.JSONObject;

/**
 * Options for user registration e.i: FB, G+, Let's Eat
 * @author Arif
 *
 */
public class RegistrationOptionActivityAction extends Activity implements
		OnClickListener {

	public static int count;

	public static boolean isProcessRunning;

	private SignInButton btnSignIn;

	LinearLayout linearLayoutSkip;

	// fb
	/*private LoginButton fbLoginBtn;
	private UiLifecycleHelper uiHelper;*/
	//private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");

	//fb new sdk login
	com.facebook.login.widget.LoginButton fbLoginBtn1;
	CallbackManager callbackManager;

	// for fb to le data transfer
	String photoname;
	String username;
	String pwd;
	String name;
	String email;
	String phone;
	String gender;
	String country;
	String city;
	String zip;
	String about;

	// task
	private LoginCheck loginCheck;
	private FromFbToLEDataUpdateTask fromFbToLEDataUpdateTask;
	boolean isDBUpdateTaskDone;
	boolean isLoginTaskDone;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		FacebookSdk.sdkInitialize(getApplicationContext());
		callbackManager = CallbackManager.Factory.create();
		// Initialize the SDK before executing any other operations,
		// especially, if you're using Facebook UI elements.

		setContentView(R.layout.activity_registration_option);

		//fb new sdk
		fbNewSdkLogin();


		// hide actionbar
		getActionBar().hide();

		// fb
		/*statusCallback = new Session.StatusCallback() {
			@Override
			public void call(Session session, SessionState state,
					Exception exception) {
				if (state.isOpened()) {
					// buttonsEnabled(true);
					Log.d("FacebookSampleActivity", "Facebook session opened");
				} else if (state.isClosed()) {
					// buttonsEnabled(false);
					Log.d("FacebookSampleActivity", "Facebook session closed");
				}
			}
		};

		uiHelper = new UiLifecycleHelper(this, statusCallback);
		uiHelper.onCreate(savedInstanceState);*/

		btnSignIn = (SignInButton) findViewById(R.id.google_signin);
		setGooglePlusButtonText(btnSignIn, "Log in With G+");

		btnSignIn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(RegistrationOptionActivityAction.this,
						MainActivity.class);
				intent.putExtra(StaticObjects.NEV_TYPE_KEY,
						StaticObjects.nev_type);
				startActivity(intent);
				finish();
			}
		});

		// Button click listeners
		// btnSignIn.setOnClickListener(this);

		// registration button
		final TextView btnNevRegistration = (TextView) findViewById(R.id.btnNevRegistration);
		btnNevRegistration.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(RegistrationOptionActivityAction.this,
						SignupActivity.class);
				intent.putExtra(StaticObjects.NEV_TYPE_KEY,
						StaticObjects.nev_type);
				startActivity(intent);
				finish();
			}
		});

		final LinearLayout lNevRegistration = (LinearLayout) findViewById(R.id.lNevRegistration);
		lNevRegistration.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(RegistrationOptionActivityAction.this,
						SignupActivity.class);
				intent.putExtra(StaticObjects.NEV_TYPE_KEY,
						StaticObjects.nev_type);
				startActivity(intent);
				finish();
			}
		});
		// registration button touch listener
		btnNevRegistration.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent e) {
				// TODO Auto-generated method stub

				if (v == btnNevRegistration
						&& e.getAction() == MotionEvent.ACTION_DOWN) {

					lNevRegistration.setBackgroundColor(Color.parseColor("#80CCCCCC"));

				} else if (v == btnNevRegistration&& (e.getAction() == MotionEvent.ACTION_CANCEL || e
						.getAction() == MotionEvent.ACTION_UP)) {

					lNevRegistration.setBackgroundColor(Color.parseColor("#cccc99"));

				}



				return false;
			}
		});

		lNevRegistration.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent e) {
				// TODO Auto-generated method stub

				if (v == lNevRegistration && e.getAction() == MotionEvent.ACTION_DOWN) {

					lNevRegistration.setBackgroundColor(Color.parseColor("#80CCCCCC"));

				} else if (v == lNevRegistration && (e.getAction() == MotionEvent.ACTION_CANCEL || e.getAction() == MotionEvent.ACTION_UP)) {

					lNevRegistration.setBackgroundColor(Color.parseColor("#cccc99"));
				}

				return false;
			}
		});

		// login button click
		final TextView btnNevLogin = (TextView) findViewById(R.id.btnNevLogin);
		btnNevLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(RegistrationOptionActivityAction.this,
						LoginActivity.class);
				intent.putExtra(StaticObjects.NEV_TYPE_KEY,
						StaticObjects.nev_type);
				startActivity(intent);
				finish();
			}
		});
		final LinearLayout lNevLogin = (LinearLayout) findViewById(R.id.lNevLogin);
		lNevLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(RegistrationOptionActivityAction.this,
						LoginActivity.class);
				intent.putExtra(StaticObjects.NEV_TYPE_KEY,
						StaticObjects.nev_type);
				startActivity(intent);
				finish();
			}
		});

		// login button touch listener
		btnNevLogin.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent e) {
				// TODO Auto-generated method stub

				if (v == btnNevLogin && e.getAction() == MotionEvent.ACTION_DOWN) {
					lNevLogin.setBackgroundColor(Color.parseColor("#80CCCCCC"));
				} else if (v == btnNevLogin && (e.getAction() == MotionEvent.ACTION_CANCEL || e.getAction() == MotionEvent.ACTION_UP)) {

					lNevLogin.setBackgroundColor(Color.parseColor("#cccc99"));

				}

				return false;
			}
		});

		lNevLogin.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent e) {
				// TODO Auto-generated method stub

				if (v == btnNevLogin
						&& e.getAction() == MotionEvent.ACTION_DOWN) {
					lNevLogin.setBackgroundColor(Color.parseColor("#80CCCCCC"));
				} else if (v == btnNevLogin
						&& (e.getAction() == MotionEvent.ACTION_CANCEL || e
						.getAction() == MotionEvent.ACTION_UP)) {
					lNevLogin.setBackgroundColor(Color.parseColor("#cccc99"));
				}

				return false;
			}
		});

		/*fbLoginBtn = (LoginButton) findViewById(R.id.fb_login);
		fbLoginBtn.setUserInfoChangedCallback(new UserInfoChangedCallback() {
			@Override
			public void onUserInfoFetched(GraphUser userdata) {
				if (!isProcessRunning && userdata != null) {
					isProcessRunning = true;
					Toast.makeText(RegistrationOptionActivityAction.this,
							"Login as " + userdata.getName(), Toast.LENGTH_LONG)
							.show();
					*//*
					 * User user=new User(ID, userdata.getUsername(), PASSWORD,
					 * userdata.getFirstName(), "0",
					 * "http://graph.facebook.com/"+userdata.getId()+"/picture",
					 * EMAIL, COUNTRY, CITY, ZIP, DATE, STATUS, PHONE);
					 * SavedPrefernce savedPrefernce=new SavedPrefernce();
					 * savedPrefernce
					 * .saveSharedpreferences(RegistrationOptionActivityAction.this,
					 * user);
					 *//*

					photoname = "http://graph.facebook.com/" + userdata.getId()
							+ "/picture";
					username = userdata.getId();
					pwd = userdata.getId();
					name = userdata.getName();

					if (userdata.getProperty("email") != null) {
						email = userdata.getProperty("email").toString();
						Log.e("email", userdata.getProperty("email").toString());
					} else {
						email = "";
					}

					phone = "";

					if (userdata.getProperty("gender") != null) {
						if (userdata.getProperty("gender").toString()
								.equalsIgnoreCase("male")) {
							gender = "0";
						} else if (userdata.getProperty("gender").toString()
								.equalsIgnoreCase("female")) {
							gender = "1";
						} else {
							gender = "2";
						}

						// Log.e("gender",
						// userdata.getProperty("gender").toString());
					}

					country = "";
					city = "";
					zip = "";
					about = "";

					fromFbToLEDataUpdateTask = new FromFbToLEDataUpdateTask();
					if (!isDBUpdateTaskDone
							&& fromFbToLEDataUpdateTask != null
							&& fromFbToLEDataUpdateTask.getStatus() != AsyncTask.Status.RUNNING) {

						fromFbToLEDataUpdateTask.execute();

						count++;
						Log.e("Count", "value of count " + count);
					}

				} else {

					// Toast.makeText(RegistrationOptionActivityAction.this,
					// "You are not logged" , Toast.LENGTH_LONG).show();
				}
			}

		});*/


		linearLayoutSkip = (LinearLayout) findViewById(R.id.linearLayoutSkip);
		linearLayoutSkip.setVisibility(View.INVISIBLE);
		linearLayoutSkip.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent e) {
				if (e.getAction() == MotionEvent.ACTION_DOWN) {

					linearLayoutSkip.setBackgroundColor(Color.parseColor("#FFFFFF"));

				} else if (e.getAction() == MotionEvent.ACTION_UP) {
					linearLayoutSkip.setBackgroundColor(0x00000000);
				}

				return false;
			}
		});

		linearLayoutSkip.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(RegistrationOptionActivityAction.this,HomeT.class);
				startActivity(intent);

			}
		});

		/*
		 * linearLayoutSkip=(LinearLayout)findViewById(R.id.linearLayoutSkip);
		 * if(nev_type==null || nev_type.equals(NEV_TYPE_ACTION) ||
		 * nev_type.isEmpty()){ linearLayoutSkip.setVisibility(View.INVISIBLE);
		 * }else if(nev_type!=null && nev_type.equals(NEV_TYPE_HOME)){
		 * linearLayoutSkip.setVisibility(View.VISIBLE);
		 * linearLayoutSkip.setOnClickListener(new View.OnClickListener() {
		 *
		 * @Override public void onClick(View v) { Intent intent=new
		 * Intent(RegistrationOptionActivityAction.this, HomeT.class);
		 * startActivity(intent);
		 *
		 * } }); }
		 */

		AnimationTween.animateViewPosition(findViewById(R.id.ll_btnContainer),this);
		AnimationTween.animateViewPosition(findViewById(R.id.fb_login),this);
		AnimationTween.animateViewPosition(btnSignIn,this);
	}


	void fbNewSdkLogin (){
		fbLoginBtn1 = (com.facebook.login.widget.LoginButton) findViewById(R.id.fb_login);
		fbLoginBtn1.setReadPermissions(Arrays.asList( "public_profile, email, user_birthday, user_friends"));
		// If using in a fragment
		//fbLoginBtn1.setFragment(this);
		// Other app specific specialization

		// Callback registration
		fbLoginBtn1.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
			@Override
			public void onSuccess(LoginResult loginResult) {
				// App code
				//loginResult.getAccessToken().getUserId();
				Log.d("FacebookSampleActivity", "Facebook session opened");

				GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),new GraphRequest.GraphJSONObjectCallback() {
					@Override
					public void onCompleted(JSONObject object,GraphResponse response) {
						// Application code
						Log.v("fblogin", response.toString());
						try {
							if (!isProcessRunning && object != null) {
								isProcessRunning = true;
								Toast.makeText(RegistrationOptionActivityAction.this,
										"Login as " + object.getString("name"), Toast.LENGTH_LONG)
										.show();


								photoname = "http://graph.facebook.com/" + object.getString("id")
										+ "/picture";
								username =  object.getString("id");
								pwd =  object.getString("id");
								name =  object.getString("name");

								if ( object.has("email")) {
									email = object.getString("email");
									Log.e("email", object.getString("email"));
								} else {
									email = "";
								}

								phone = "";

								if (object.getString("gender") != null) {
									if (object.getString("gender")
											.equalsIgnoreCase("male")) {
										gender = "0";
									} else if (object.getString("gender")
											.equalsIgnoreCase("female")) {
										gender = "1";
									} else {
										gender = "2";
									}

									// Log.e("gender",
									// userdata.getProperty("gender").toString());
								}else{
									gender="";
								}

								country = "";
								city = "";
								zip = "";
								about = "";

								fromFbToLEDataUpdateTask = new FromFbToLEDataUpdateTask();
								if (!isDBUpdateTaskDone
										&& fromFbToLEDataUpdateTask != null
										&& fromFbToLEDataUpdateTask.getStatus() != AsyncTask.Status.RUNNING) {

									fromFbToLEDataUpdateTask.execute();

									count++;
									Log.v("Count", "value of count " +photoname+username+pwd+gender+ count);
								}
							}


						}catch (Exception e){
							//Log.v("Exception", e.getMessage());
							e.printStackTrace();
						}

					}

				});

				Bundle parameters = new Bundle();
				parameters.putString("fields", "id, email,name,\n" +
						"\t\t\t\tfirst_name,\n" +
						"\t\t\t\tlast_name,\n" +
						"\t\t\t\tage_range,\n" +
						"\t\t\t\tlink,\n" +
						"\t\t\t\tgender,\n" +
						"\t\t\t\tlocale,\n" +
						"\t\t\t\tpicture,\n" +
						"\t\t\t\ttimezone,\n" +
						"\t\t\t\tupdated_time,\n" +
						"\t\t\t\tverified");

				request.setParameters(parameters);
				request.executeAsync();
			}

			@Override
			public void onCancel() {
				// App code
			}

			@Override
			public void onError(FacebookException exception) {
				// App code
			}
		});


	}



	//change google sign in button text
	protected void setGooglePlusButtonText(SignInButton signInButton, String buttonText) {
		// Find the TextView that is inside of the SignInButton and set its text
		for (int i = 0; i < signInButton.getChildCount(); i++) {
			View v = signInButton.getChildAt(i);

			if (v instanceof TextView) {
				TextView tv = (TextView) v;
				tv.setText(buttonText);
				return;
			}
		}
	}



	AlertDialog helpDialog = null;

	// for fb
//	private Session.StatusCallback statusCallback;

	@Override
	public void onPause() {
		super.onPause();
		//uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		//uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle savedState) {
		super.onSaveInstanceState(savedState);
		//uiHelper.onSaveInstanceState(savedState);
	}

	// ======
	class FromFbToLEDataUpdateTask extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		protected String doInBackground(String... args) {
			// creating Places class object
			String response = null;
			FoodyResturent foodyResturent = new FoodyResturent();

			try {

				response = foodyResturent.insertProfile(photoname, username,
						pwd, name, email, phone, gender, country, city, zip,
						about, StaticObjects.REG_TYPE_FB);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return response;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String result) {
			isDBUpdateTaskDone = true;

			if (result.equals("failed")) {
				Toast.makeText(RegistrationOptionActivityAction.this,
						"Registration Failed", Toast.LENGTH_LONG).show();
			} else if (result.equals("success")) {
				Toast.makeText(RegistrationOptionActivityAction.this,
						"Registration Success", Toast.LENGTH_LONG).show();

				loginCheck = new LoginCheck();
				if (!isLoginTaskDone && loginCheck != null
						&& loginCheck.getStatus() != AsyncTask.Status.RUNNING) {
					loginCheck.execute();
				}
			} else if (result.equals("SimiralExists")) {
				// Toast.makeText(RegistrationOptionActivityAction.this,
				// "User Simiral Exists", Toast.LENGTH_LONG).show();

				loginCheck = new LoginCheck();
				if (!isLoginTaskDone && loginCheck != null
						&& loginCheck.getStatus() != AsyncTask.Status.RUNNING) {
					loginCheck.execute();
				}

			}
		}

	}

	// fb end

	ArrayList<User> users;

	class LoginCheck extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			// Toast.makeText(RegistrationOptionActivityAction.this, "Checking...",
			// Toast.LENGTH_SHORT).show();
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			FoodyResturent foodyResturent = new FoodyResturent();

			try {
				// String types = "LocId=" + LocId+"&Page="+currentPage;
				users = foodyResturent.loginCheck(username, pwd);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			// pDialog.dismiss();
			// updating UI from Background Thread

			Log.e("users", "===================" + users.size());

			isProcessRunning = false;

			if (users != null && users.size() > 0) {
				User tempUser = users.get(0);

				// save user data
				// saveSharedpreferences(tempUser);
				SavedPrefernce prefernce = new SavedPrefernce(
						RegistrationOptionActivityAction.this);
				prefernce.saveSharedpreferences(tempUser);
				// Toast.makeText(RegistrationOptionActivityAction.this, "Logged In",
				// Toast.LENGTH_SHORT).show();
				Log.e(RegistrationOptionActivityAction.class.getSimpleName(),
						StaticObjects.nev_type + " before condition");

				if (!isLoginTaskDone
						&& StaticObjects.nev_type != null
						&& StaticObjects.nev_type
						.equals(StaticObjects.NEV_TYPE_ACTION)) {

					// Toast.makeText(RegistrationOptionActivityAction.this, nev_type,
					// Toast.LENGTH_SHORT).show();
					RegistrationOptionActivityAction.this.finish();
					isLoginTaskDone = true;
					cancel(true);

				} else if (!isLoginTaskDone
						&& StaticObjects.nev_type != null
						&& StaticObjects.nev_type
						.equals(StaticObjects.NEV_TYPE_HOME)) {
					// Toast.makeText(RegistrationOptionActivityAction.this, nev_type,
					// Toast.LENGTH_SHORT).show();

					isLoginTaskDone = true;
					RegistrationOptionActivityAction.this.finish();
				}

			} else {
				Toast.makeText(RegistrationOptionActivityAction.this, "Try again",
						Toast.LENGTH_SHORT).show();

			}

		}
	}

	/*
	 * @Override public boolean onOptionsItemSelected(MenuItem item) { // TODO
	 * Auto-generated method stub
	 *
	 * switch (item.getItemId()) {
	 *
	 * case R.id.action_search: // search action RelativeLayout
	 * relativeLayout=(RelativeLayout
	 * )findViewById(R.id.activity_registration_option_root);
	 * addSearchLayout(relativeLayout); return true;
	 *
	 * default: return super.onOptionsItemSelected(item); }
	 *
	 * }
	 */

	protected void onStart() {
		super.onStart();

		GoogleAnalytics.getInstance(this).reportActivityStart(this);

	}

	protected void onStop() {
		super.onStop();

		GoogleAnalytics.getInstance(this).reportActivityStop(this);

	}

/*	@Override
	protected void onActivityResult(int requestCode, int responseCode,
			Intent intent) {
		super.onActivityResult(requestCode, responseCode, intent);

		// fb
		uiHelper.onActivityResult(requestCode, responseCode, intent);

	}*/

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		callbackManager.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	/**
	 * Button on click listener
	 * */
	@Override
	public void onClick(View v) {
		/*
		 * switch (v.getId()) { case R.id.google_signin: // Signin button
		 * clicked //signInWithGplus(); Intent intent=new Intent(this,
		 * MainActivity.class); intent.putExtra(NEV_TYPE_KEY, nev_type);
		 * startActivity(intent); break; }
		 */
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, HomeT.class);
		startActivity(intent);
	}

}
