package com.mcc.user;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.user.User;
import com.mcc.letseat.HomeT;
import com.mcc.letseat.MyProgressDialog;
import com.mcc.letseat.R;
import com.mcc.libs.AnimationTween;

/**
 * User Login who SignUp via Let's Eat 
 * @author Arif
 *
 */
public class LoginActivity extends Activity{

	EditText etUserName, etPassword;
	TextView textViewForgotPass;
	
	ArrayList<User> users; 
	
	// Progress dialog
	MyProgressDialog pDialog;
	
	
	
	// Alert Dialog Manager
	//	AlertDialogManager alert = new AlertDialogManager();
		
	//
	SharedPreferences sharedpreferences;
	
	 String nev_type;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.slide_up_in,R.anim.stay);
		
		setContentView(R.layout.activity_login);
		
		nev_type=getIntent().getStringExtra(StaticObjects.NEV_TYPE_KEY);
		
		//hide actionbar
		getActionBar().hide();
		
		 sharedpreferences = getSharedPreferences(RegistrationActivity.LET_EAT_PREFERENCES, Context.MODE_PRIVATE);
		
		etUserName=(EditText) findViewById(R.id.txtUserName);
		etPassword=(EditText) findViewById(R.id.txtpassword);
		textViewForgotPass=(TextView) findViewById(R.id.textViewForgotPass);
		
		final Button btnNevLogin=(Button) findViewById(R.id.btnLogin);
		btnNevLogin.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				/*Intent intent=new Intent(LoginActivity.this, LoginActivity.class);
				startActivity(intent);*/
				// TODO Auto-generated method stub
				/*btnNevLogin.setBackgroundResource(R.drawable.border_round_red_button);

				new Handler().postDelayed(new Runnable() {

					public void run() {

						btnNevLogin.setBackgroundResource(R.drawable.border_round_dark_red_button);

						
					}

				}, 100L);*/
				
				// Button Click Code Here
				if(etUserName.getText().toString().trim().equals("") || etUserName.getText().toString().trim().isEmpty() ){
					AnimationTween.shakeOnError(LoginActivity.this, etUserName);
					return;
				}
					
					
				if(etPassword.getText().toString().trim().equals("") || etPassword.getText().toString().trim().isEmpty() ){
					AnimationTween.shakeOnError(LoginActivity.this, etPassword);
					return;
				}
				
				new LoginCheck().execute();
				
				
			}
		});
		
		textViewForgotPass.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				textViewForgotPass.setBackgroundResource(R.drawable.border_round_gry);
				

				new Handler().postDelayed(new Runnable() {

					public void run() {

						textViewForgotPass.setBackgroundResource(R.drawable.border_round_forgot_pass);

						// Button Click Code Here
						Intent intent=new Intent(LoginActivity.this, ResetPasswordActivity.class);
						startActivity(intent);
					}

				}, 100L);
				
				
				
				
			}
		});
		
		//AnimationTween.animateViewPosition(findViewById(R.id.activity_login_root), LoginActivity.this);
	}
	

	@Override
	protected void onPause() {
	// TODO Auto-generated method stub
	super.onPause();
	overridePendingTransition(R.anim.stay,R.anim.slide_down_out);
	}
	
	/*@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
	
		case R.id.action_search:
			// search action
			RelativeLayout relativeLayout=(RelativeLayout)findViewById(R.id.activity_login_root);		
			addSearchLayout(relativeLayout);			
			return true;
		
		default:
			return super.onOptionsItemSelected(item);
		}

	}*/

	
/*	public void saveSharedpreferences(User user ){
	      
	      Editor editor = sharedpreferences.edit();
	      editor.putString(RegistrationActivity.Photo, user.IMG);
	      editor.putString(RegistrationActivity.Username, user.USERNAME);
	      editor.putString(RegistrationActivity.Password, user.PASSWORD);
	      editor.putString(RegistrationActivity.Name, user.NAME);
	      editor.putString(RegistrationActivity.Phone, "0000000");
	      editor.putString(RegistrationActivity.Email, user.EMAIL);
	      editor.putString(RegistrationActivity.Gender, user.SEX);	     
	      editor.putString(RegistrationActivity.Country, user.COUNTRY);
	      editor.putString(RegistrationActivity.City, user.CITY); 
	      editor.putString(RegistrationActivity.Zip, user.ZIP);	     
	      editor.commit(); 

	   }*/

	
	//
	
	class LoginCheck extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			pDialog = new MyProgressDialog(LoginActivity.this);
			// pDialog.setMessage(Html.fromHtml("<b>Loading ...</b>"));
			// pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
			
			
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			FoodyResturent foodyResturent = new FoodyResturent();

			try {
				// String types = "LocId=" + LocId+"&Page="+currentPage;
			//	Toast.makeText(LoginActivity.this, ">"+etUserName.getText().toString()+"<>"+etPassword.getText().toString()+"<", Toast.LENGTH_SHORT).show();
				users = foodyResturent.loginCheck(etUserName.getText().toString(), etPassword.getText().toString());

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			
			if (users!=null) {
				// updating UI from Background Thread
				runOnUiThread(new Runnable() {
					public void run() {
						Log.e("users", "==================="+users.size());
						
						if(users.size()>0){
							User tempUser= users.get(0);
							
							//save user data
							//saveSharedpreferences(tempUser);
							SavedPrefernce prefernce=new SavedPrefernce(LoginActivity.this);
							prefernce.saveSharedpreferences( tempUser);
							
							//alert.showAlertDialog(LoginActivity.this, getString(R.string.app_name), "Login", false);
							//finish();
							if( nev_type.equals(StaticObjects.NEV_TYPE_ACTION) ){
								finish();
							}else{
								Intent intent=new Intent(LoginActivity.this, HomeT.class);
								startActivity(intent);
								
							}
							
							
						}else {
							//alert.showAlertDialog(LoginActivity.this,	getString(R.string.app_name), "Incorrect information. Try again.", false);
							Toast.makeText(LoginActivity.this, "Incorrect information. Try again", Toast.LENGTH_SHORT).show();
							
							
						}

					}

				});
			}else{
				Toast.makeText(LoginActivity.this, "Try again", Toast.LENGTH_SHORT).show();
				//Intent intent=new Intent(LoginActivity.this, Home.class);
				//startActivity(intent);
			}
			

		}

	}

}
