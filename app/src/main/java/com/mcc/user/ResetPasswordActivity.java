package com.mcc.user;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.foody.jsondata.FoodyResturent;
import com.foody.nearby.AlertDialogManager;
import com.mcc.letseat.R;
import com.mcc.libs.ActivityWithSliding;
import com.mcc.libs.AnimationTween;

/**
 * Reset User account password
 * @author Arif
 *
 */
public class ResetPasswordActivity extends Activity implements
		OnClickListener {

	EditText etResetEmail;
	TextView textViewForgotPass;

	Button btnPassSend;

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forgotpass);

		// hide actionbar
		getActionBar().hide();

		etResetEmail = (EditText) findViewById(R.id.etResetEmail);
		btnPassSend = (Button) findViewById(R.id.btnPassSend);

		btnPassSend.setOnClickListener(this);

	}

	public final static boolean isValidEmail(CharSequence target) {
		if (TextUtils.isEmpty(target)) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
					.matches();
		}
	}



	class ResetPasswordTask extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		String response;

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			Toast.makeText(getApplicationContext(), "Please wait...",
					Toast.LENGTH_LONG).show();

		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			FoodyResturent foodyResturent = new FoodyResturent();

			try {
				// String types = "LocId=" + LocId+"&Page="+currentPage;
				response = foodyResturent.resetPassword(etResetEmail.getText()
						.toString());

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {

					if (response.equals("success")) {
						//Toast.makeText(getApplicationContext(), "Success",Toast.LENGTH_SHORT).show();
						Toast.makeText(getApplicationContext(),"Check your email", Toast.LENGTH_LONG).show();
						finish();

					} else {

						Toast.makeText(getApplicationContext(), "Try again",
								Toast.LENGTH_LONG).show();
					}
					
					btnPassSend.setOnClickListener(ResetPasswordActivity.this);

				}

			});

		}

	}

	@Override
	public void onClick(View v) {

		if (v == btnPassSend) {

			// TODO Auto-generated method stub
			btnPassSend.setBackgroundResource(R.drawable.border_round_gry);
			btnPassSend.setOnClickListener(null);

			new Handler().postDelayed(new Runnable() {

				public void run() {

					btnPassSend.setBackgroundResource(R.drawable.border_round_green);

					// Button Click Code Here
				}

			}, 100L);

			String email = etResetEmail.getText().toString().trim();

			if (email.equals("") || email.isEmpty() || !isValidEmail(email)) {
				AnimationTween.shakeOnError(ResetPasswordActivity.this,
						etResetEmail);
				return;
			}

			Toast.makeText(getApplicationContext(), "Reseting Password...",
					Toast.LENGTH_LONG).show();

			new ResetPasswordTask().execute();
		}
	}

}
