package com.mcc.user.profile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.LetsEatRequest;
import com.foody.jsondata.Review;
import com.foody.jsondata.user.UserBookedDeal;
import com.foody.nearby.AlertDialogManager;
import com.foody.nearby.GPSTracker;
import com.foody.nearby.MyListAdapterReviewUser;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mcc.letseat.R;
import com.mcc.letseat.Adapter.BookedDealsAdapter;
import com.mcc.letseat.tab.TabMainActivity;
import com.mcc.libs.AnimationTween;

/**
 * Provide list of all reviews by user
 * @author Arif
 *
 */
public class UserDealsWishlistFragment extends Fragment {

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Google Places
	FoodyResturent foodyResturent;

	// Places List
	ArrayList<Review> reviewList;

	// Button
	ImageView btnShowOnMap;

	

	//  Listview
	ListView lv;
	
	View progress; 

	// ListItems data
	ArrayList<HashMap<String, String>> reviewListItems = new ArrayList<HashMap<String, String>>();

	// //admob
	AdView adView = null;

	

	String userid;

	View currentView=null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment

		userid = getArguments().getString("userid");
		System.out.println("===============userid==================="+ userid);
		
		 return currentView =inflater.inflate(R.layout.activity_user_booked_deals, container, false);
		
	}

	@Override
	public void onStart() {
		super.onStart();

		

		// Getting listview
		lv = (ListView) currentView.findViewById(R.id.list_booked_deals);
		progress = (View) currentView.findViewById(R.id.booked_deals_custom_progress);

		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, final View view,
					int position, long arg3) {
				// TODO Auto-generated method stub
				Toast.makeText(getActivity(), "Wait. Redeeming Deal...", Toast.LENGTH_SHORT).show();
				
				String BookingId =((UserBookedDeal)lv.getItemAtPosition(position)).BookingId;
				
				
				
				Button button= (Button)view.findViewById(R.id.button1);
				
			
				if(!TextUtils.isEmpty(button.getText())){
					
					button.setText("");
					View v=view.findViewById(R.id.progressBarPreparing);
					v.setVisibility(View.VISIBLE);
					redeem(BookingId, ((UserBookedDeal)lv.getItemAtPosition(position)), button,  v);
				}
				
				
				
				
			}

			
		});

		

		adView = (AdView) this.currentView.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder().addTestDevice(
				AdRequest.DEVICE_ID_EMULATOR).build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});

		

		StringRequest postRequestBookedDeals = new StringRequest(Request.Method.POST, LetsEatRequest.USER_BOOKED_DEALS,
		        new Response.Listener<String>() {
		            @Override
		            public void onResponse(String response) {
		                try {
		                	// Result handling 
		                	progress.setVisibility(View.GONE);
		                	ArrayList<UserBookedDeal> bookedDels = new LetsEatRequest().getBookedDeals(response);
		                	if(bookedDels != null && bookedDels.size() >= 0){
		                		BookedDealsAdapter adapter =new BookedDealsAdapter(getActivity(),  bookedDels);
		                		lv.setAdapter(adapter);
		                		adapter.notifyDataSetChanged();
		                		
		                		
		                	}
		                	
		    		    
		                } catch (Exception e) {
		                	
		                	
		                    e.printStackTrace();
		                }
		            }
		        },
		        new Response.ErrorListener() {
		            @Override
		            public void onErrorResponse(VolleyError error) {
		                error.printStackTrace();
		            }
		        }
		) {
		    @Override
		    protected Map<String, String> getParams()
		    {
		        Map<String, String>  params = new HashMap<String, String>();
		        // the POST parameters:
		        params.put("UserId", userid);
		        return params;
		    }
		};
		
		Volley.newRequestQueue(getActivity()).add(postRequestBookedDeals);

	}

	

	

	@Override
	public void onPause() {
		super.onPause();

		if (adView != null)
			adView.pause();

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		if (adView != null)
			adView.destroy();
		super.onDestroy();
	}
	
	
private void redeem(final String BookingId, final UserBookedDeal ubd, final Button button, final View view) {
		

		StringRequest postRequestReedem = new StringRequest(Request.Method.POST, LetsEatRequest.USER_REDEEM_DEALS,
		        new Response.Listener<String>() {
		            @Override
		            public void onResponse(String response) {
		                try {
		                	// Result handling 

		                	if(response.equals("Success")){
		                		((BookedDealsAdapter)lv.getAdapter()).remove(ubd);
		                		Toast.makeText(getActivity(), "Thank you for Redeeming the Deal", Toast.LENGTH_LONG).show();
		                	}else{
		                		button.setText("Redeem");					
		    					view.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
		    					 Toast.makeText(getActivity(), "Can't Redeem. Try again", Toast.LENGTH_LONG).show();
		                	}
		                	
		                	
		    		    
		                } catch (Exception e) {
		                	
		                	
		                    e.printStackTrace();
		                }
		            }
		        },
		        new Response.ErrorListener() {
		            @Override
		            public void onErrorResponse(VolleyError error) {
		                error.printStackTrace();
		                
		                Toast.makeText(getActivity(), "Can't Redeem. Try again", Toast.LENGTH_SHORT).show();
		                
		                button.setText("Redeem");					
    					view.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
		            }
		        }
		) {
		    @Override
		    protected Map<String, String> getParams()
		    {
		        Map<String, String>  params = new HashMap<String, String>();
		        // the POST parameters:
		        params.put("UserId", userid);
		        params.put("BookingId", BookingId); 
		        return params;
		    }
		};
		
		Volley.newRequestQueue(getActivity()).add(postRequestReedem);
		
		
		
		
	}

	

}
