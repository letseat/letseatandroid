package com.mcc.user.profile;

import java.util.ArrayList;
import java.util.HashMap;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView.OnEditorActionListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.NewsFeed.Comment;
import com.foody.jsondata.NewsFeed.Favorite;
import com.foody.jsondata.NewsFeed.Follow;
import com.foody.jsondata.NewsFeed.NewsFeed;
import com.foody.jsondata.NewsFeed.PhotoShare;
import com.foody.jsondata.user.UserPubInfo;
import com.foody.nearby.AlertDialogManager;
import com.foody.nearby.MyListAdapterFeed;
import com.foody.nearby.SearchResturentListView;
import com.foody.nearby.SearchUserListView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.mcc.letseat.R;
import com.mcc.letseat.tab.TabMainActivity;
import com.mcc.libs.AnimationTween;
import com.mcc.libs.FragmentActivityWithSliding;
import com.mcc.user.RegistrationActivity;
import com.mcc.user.SavedPrefernce;
import com.squareup.picasso.Picasso;

/**
 * UserActivity is a FragmentActivity host all user Fragment : UserNewsFeed, UserFavoriteFragment, UserFollowerFragment, UserFollowingFrgment,
 * UserPhotoFragment, UserReviewFragment
 * @author Arif
 *
 */
public class UserActivity extends FragmentActivityWithSliding implements OnClickListener {

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Google Places
	FoodyResturent foodyResturent;

	// UserPubInfo 
	ArrayList<UserPubInfo> userPubInfoList;
	ArrayList<NewsFeed> userNewsFeedList;
	
	// ListItems data
	ArrayList<HashMap<String, String>> followerListItems = new ArrayList<HashMap<String, String>>();
	
	ArrayList<HashMap<String, String>> userNewsFeedListItems = new ArrayList<HashMap<String, String>>();

	UserPubInfo userPubInfo;

	String userid = null;
	
	ListView lv;
	
	//Ad interstitial;
	private InterstitialAd interstitial;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_profile);
		
		//hide my deals button
		findViewById(R.id.user_deals).setVisibility(View.GONE);

		/*// action bar hide title change icon
		getActionBar().setIcon(R.drawable.ic_home_drawer);
		getActionBar().setDisplayShowTitleEnabled(false);

		// hide actionbar back arrow
		getActionBar().setDisplayHomeAsUpEnabled(false);

		// hide or show back icon on action bar
		// getActionBar().setDisplayHomeAsUpEnabled(true);

		// enable app icon as home button
		getActionBar().setHomeButtonEnabled(true);

		// Set whether to include the application home affordance in the action
		// bar. Home is presented as either an activity icon or logo.
		// getActionBar().setDisplayShowHomeEnabled(true);
*/
		createInterstitial();
		
		lv=(ListView) findViewById(R.id.list_news_feed);
		
		userid = getIntent().getStringExtra("userid");
		if (userid != null && !userid.isEmpty()){
			new LoaduserData().execute();
		}
		
		//analytics
		analyticsSendScreenView("User view: "+userid);
		
		//set home button
		setHomeButton(this);
		
	}
	
	public void createInterstitial(){
		// Create the interstitial.
	    interstitial = new InterstitialAd(this);
	    interstitial.setAdUnitId(getString(R.string.adUnitInterstitial));
	}
	
	
	public void loadInterstitial(){
		 // Create ad request.
	    AdRequest adRequest = new AdRequest.Builder().build();

	    // Begin loading your interstitial.
	    interstitial.loadAd(adRequest);
	    
	    interstitial.setAdListener(new AdListener(){
	          public void onAdLoaded(){
	               displayInterstitial();
	          }
	          
	          public void onAdFailedToLoad(int errorcode) {
	        	  	//Toast.makeText(HomeT.this, "Error:" + errorcode, Toast.LENGTH_SHORT).show();
					System.out.println("Error:" + errorcode);
				}
	          
	    });
	}
	
	// Invoke displayInterstitial() when you are ready to display an interstitial.
	  public void displayInterstitial() {
		 // Toast.makeText(this, ""+interstitial.isLoaded(), Toast.LENGTH_SHORT).show();
	    if (interstitial.isLoaded()) {
	      interstitial.show();
	      
	      
	    }
	  }
	
	
	
	public static boolean isUserProfilePicChanged=false;
	public static  LoaduserData loaduserData;
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		if(isUserProfilePicChanged && userid != null && !userid.isEmpty()){
			
			isUserProfilePicChanged=false;
			new LoaduserData().execute();
		}
	}
	
	
	public static void onProfilePicChanged(){
		if(isUserProfilePicChanged ){
			
			isUserProfilePicChanged=false;
			
			 if(loaduserData!=null)
				 loaduserData.execute();
		}
	}

	void registerListenertToscrollBTN() {
		findViewById(R.id.user).setOnClickListener(this);
		findViewById(R.id.user_review).setOnClickListener(this);
		findViewById(R.id.user_photo).setOnClickListener(this);
		findViewById(R.id.user_follower).setOnClickListener(this);
		findViewById(R.id.user_following).setOnClickListener(this);
		findViewById(R.id.user_favourite).setOnClickListener(this);
		
		SavedPrefernce savedPrefernce=new SavedPrefernce(this);
		if(userid.equals(savedPrefernce.getUserID()) ){
			findViewById(R.id.user_deals).setVisibility(View.VISIBLE);
			findViewById(R.id.user_deals).setOnClickListener(this);
		}else {
			findViewById(R.id.user_deals).setVisibility(View.GONE);
		}
		
		
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub

		Bundle bundle = new Bundle();
		bundle.putString("userid", userid);

		Fragment fr = null;

		if (view == findViewById(R.id.user)) {
			findViewById(R.id.ui_layout1).setVisibility(View.VISIBLE);
			findViewById(R.id.profile_fragment).setVisibility(View.GONE);
			
			//analytics
			analyticsSendScreenView("User:"+userid+">NewsFeed");
			
		} else if (view == findViewById(R.id.user_review)) {

			fr = new UserReviewFragment();
			fr.setArguments(bundle);
			
			//analytics
			analyticsSendScreenView("User:"+userid+">Review");
			
		} else if (view == findViewById(R.id.user_photo)) {

			fr = new UserPhotoFragment();
			fr.setArguments(bundle);
			
			//analytics
			analyticsSendScreenView("User:"+userid+">Photo");
			
		} else if (view == findViewById(R.id.user_follower)) {

			fr = new UserFollowerFragment();
			fr.setArguments(bundle);
			
			//analytics
			analyticsSendScreenView("User:"+userid+">Follower");
			
		} else if (view == findViewById(R.id.user_following)) {

			fr = new UserFollowingFragment();
			fr.setArguments(bundle);
			
			//analytics
			analyticsSendScreenView("User:"+userid+">Following");
			
		}else if (view == findViewById(R.id.user_favourite)) {

			fr = new UserFavoriteFragment();
			fr.setArguments(bundle);
			
			//analytics
			analyticsSendScreenView("User:"+userid+">Favorite");
		}else if (view == findViewById(R.id.user_deals)) {
		
		fr = new UserDealsWishlistFragment();
		fr.setArguments(bundle);
		
		//analytics
		analyticsSendScreenView("User:"+userid+">Favorite");
	}

		if (view != findViewById(R.id.user)) {

			findViewById(R.id.ui_layout1).setVisibility(View.GONE);
			findViewById(R.id.profile_fragment).setVisibility(View.VISIBLE);

			FragmentTransaction transaction = getSupportFragmentManager()
					.beginTransaction();
			transaction.setCustomAnimations(android.R.anim.slide_out_right,
					android.R.anim.slide_out_right);
			// Replace whatever is in the fragment_container view with this
			// fragment,
			// and add the transaction to the back stack so the user can
			// navigate
			// back
			transaction.replace(R.id.profile_fragment, fr);
			transaction.addToBackStack(null);

			// Commit the transaction
			transaction.commit();
		}

	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
		
		case R.id.action_search:
			RelativeLayout relativeLayout =(RelativeLayout)findViewById(R.id.activity_user_profile_root_layout);
			addSearchLayout(relativeLayout);
			
			// search action
			return true;
			
		case R.id.action_profile:
			SavedPrefernce savedPrefernce=new SavedPrefernce(this);
			if(userid.equals(savedPrefernce.getUserID()) ){
				
				loaduserData=new LoaduserData();
				
				Intent intent=new Intent(UserActivity.this, RegistrationActivity.class);
				startActivity(intent);
				return true;
			}else{
				return super.onOptionsItemSelected(item);
			}
			
			// search action
			
		
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	
	@Override
	public void addSearchLayout(RelativeLayout relativeLayout){
		if(relativeLayout.findViewWithTag("searchview")==null){
			
			if(super.isSearchViewOpen){
				//relativeLayout.removeView(searchview);
				((RelativeLayout)searchview.getParent()).removeView(searchview);
				isSearchViewOpen=false;
				
				
				return;
			}
			
			
			 searchview = getLayoutInflater().inflate(R.layout.search,  relativeLayout, false);
			 searchview.setTag(searchview);
			 
			final ImageView img_edittext_cancel=(ImageView)searchview.findViewById(R.id.img_edittext_cancel);
			
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
			
			relativeLayout.addView(searchview, params);
			
			final EditText editTextSearch=(EditText)  searchview.findViewById(R.id.txtSearch);
			editTextSearch.setHint("Search People");
			
			final Animator.AnimatorListener animatorListener2= new Animator.AnimatorListener() {
			    public void onAnimationStart(Animator animation) {}
			    public void onAnimationRepeat(Animator animation) { }

			    public void onAnimationEnd(Animator animation) {			    	
			    	// hide soft keyboard
					
					String searchText=editTextSearch.getText().toString().trim();					
					
					searchview.clearAnimation();
					Intent i = new Intent(UserActivity.this,SearchUserListView.class);
					i.putExtra("searchKeyWord_home", searchText);
					startActivity(i);
									
			    }
			    public void onAnimationCancel(Animator animation) {}
			  };
			
			
			editTextSearch.setOnEditorActionListener(new OnEditorActionListener() {

				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					// TODO Auto-generated method stub
					if (actionId == EditorInfo.IME_ACTION_SEARCH) {
						String searchText=editTextSearch.getText().toString().trim();
						if(searchText.isEmpty() || searchText=="" || searchText==null){
							AnimationTween.shakeOnError(UserActivity.this, editTextSearch);
							
						}else{
							InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(editTextSearch.getWindowToken(), 0);
							
							AnimationTween.animateView(searchview,animatorListener2, UserActivity.this);
						}
						
					}
					return false;
				}
			});
			
			// Edittext lose focus hide search view 
			editTextSearch.setOnFocusChangeListener(new OnFocusChangeListener() {          

		        public void onFocusChange(View v, boolean hasFocus) {
		        	 if(hasFocus){
		        	     //   Toast.makeText(getApplicationContext(), "got the focus", Toast.LENGTH_LONG).show();
		        	       
		        	    }else {
		        	       // Toast.makeText(getApplicationContext(), "lost the focus", Toast.LENGTH_LONG).show();
		        	        
		        	    }
		        
		               
		        }
		    });
			
				
			Animator.AnimatorListener animatorListener 
			  = new Animator.AnimatorListener() {

			    public void onAnimationStart(Animator animation) {}
			    public void onAnimationRepeat(Animator animation) {}
			    public void onAnimationEnd(Animator animation) {			    	
			    	//Toast.makeText(getApplicationContext(), "onAnimationEnd", Toast.LENGTH_LONG).show();
			    	editTextSearch.requestFocus();
			    	searchview.clearAnimation();
			    }

			    public void onAnimationCancel(Animator animation) {}
			  };			  
			  AnimationTween.animateViewPosition(searchview,animatorListener ,UserActivity.this);
			  
			  
			  editTextSearch.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub
					if(editTextSearch!=null && s.length()>0){
						img_edittext_cancel.setVisibility(View.VISIBLE);
					}else{
						img_edittext_cancel.setVisibility(View.INVISIBLE);
					}
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					
				}
			});
			  
			  img_edittext_cancel.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						editTextSearch.setText("");
						
					}
				});
			  
			  isSearchViewOpen=true;
			
		}
	}
	
	MenuItem edit_profile;
	MenuItem profile; 
	@Override
	public boolean onCreateOptionsMenu(Menu menu ) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate( R.menu.activity_main, menu );
	    
	    SavedPrefernce savedPrefernce=new SavedPrefernce(this);
	    if(userid.equals(savedPrefernce.getUserID()) ){
			    edit_profile=menu.findItem(R.id.action_profile);
			    edit_profile.setIcon(R.mipmap.ic_edit_profile_action_bar);
			    menu.findItem(R.id.action_profile).setVisible(true);
			    
		    }
	    return true;
	}

	
	@Override
	protected void onStart() {
		super.onStart();
		
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}


	/*
	 * 
	 * if (userPubInfo == null) { // results found View view = (View)
	 * findViewById(R.id.custom_progress_user_info);
	 * view.setVisibility(View.VISIBLE);
	 * view.findViewById(R.id.progressBarPreparing) .setVisibility(View.GONE);
	 * 
	 * TextView textPreparing = (TextView) view
	 * .findViewById(R.id.textPreparing);
	 * textPreparing.setText("Nothing Found. Try Again.");
	 * ui_txtName.setText(""); ui_txtAddress.setText("");
	 * ui_txtAbout.setText("");
	 * 
	 * } else if (userPubInfo != null) {
	 * 
	 * ui_txtName.setText(userPubInfo.NAME);
	 * ui_txtAddress.setText(userPubInfo.CITY + ", " + userPubInfo.COUNTRY);
	 * ui_txtAbout.setText(userPubInfo.ABOUT); }
	 */

	// ================

	class LoaduserData extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {

			super.onPreExecute();

			findViewById(R.id.custom_progress_user_info).setVisibility(View.VISIBLE);
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating JSONData class object
			foodyResturent = new FoodyResturent();

			try {

				if (userid.isEmpty()) {
					this.cancel(true);
					// goto login activity

					// return null;
				}

				String postData = "userid=" + userid;
				userPubInfoList = foodyResturent.getUserInfo(FoodyResturent.USER_INFO_URL, postData);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all data
			findViewById(R.id.custom_progress_user_info).setVisibility(View.GONE);
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status

					if (userPubInfoList == null || userPubInfoList.size() <= 0) {
						findViewById(R.id.custom_progress_user_info).setVisibility(View.VISIBLE);
						findViewById(R.id.custom_progress_user_info).findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
						TextView textPreparing = (TextView) findViewById(R.id.custom_progress_user_info).findViewById(R.id.textPreparing);
						textPreparing.setText("No visible activity");
						AnimationTween.animateViewPosition(findViewById(R.id.custom_progress_user_info), UserActivity.this);

					} else if (userPubInfoList != null) {
						// Check for all possible status

						// Successfully got places details
						if (userPubInfoList != null) {
							// loop through each place
							for (UserPubInfo p : userPubInfoList) {

								HashMap<String, String> map = new HashMap<String, String>();

								map.put("FOLLOWER_NAME", p.NAME);
								map.put("IMG", p.IMG);
								map.put("TOTALFOLLOWER", "Follower "+ p.NOFOllOWERS + ", Review "+ p.NOREVIEW);
								map.put("ID", p.ID);
								map.put("FOLLOWERID", p.ID);

								// adding HashMap to ArrayList
								followerListItems.add(map);

								TextView ui_txtName = (TextView) findViewById(R.id.ui_txtName);
								ui_txtName.setOnClickListener(new View.OnClickListener() {
									
									@Override
									public void onClick(View v) {
										// TODO Auto-generated method stub
										Intent intent=new Intent(UserActivity.this, RegistrationActivity.class);
										//intent.putExtra("userid", new SavedPrefernce().getUserID(getActivity()) );
										//intent.putExtra("resid", getArguments().getString("restaurantId"));					
										startActivity(intent);
									}
								});
								
								TextView ui_txtAddress = (TextView) findViewById(R.id.ui_txtAddress);
								TextView ui_txtAbout = (TextView) findViewById(R.id.ui_txtAbout);
								TextView ui_txtReview = (TextView) findViewById(R.id.ui_txtReview);
								TextView ui_txtPhoto = (TextView) findViewById(R.id.ui_txtPhoto);
								ImageView imageView = (ImageView) findViewById(R.id.ui_img_profile);

								ui_txtName.setText(p.NAME);
								ui_txtAddress.setText(p.CITY + ", " + p.COUNTRY);
								ui_txtAbout.setText(p.ABOUT);
								ui_txtReview.setText(p.NOREVIEW + " REVIEWS");
								ui_txtPhoto.setText(p.NOFOllOWERS+ " FOLLOWERS");
								
								ui_txtName.setVisibility(View.GONE);
								ui_txtAddress.setVisibility(View.GONE);
								ui_txtAbout.setVisibility(View.GONE);
								ui_txtReview.setVisibility(View.GONE);
								ui_txtPhoto.setVisibility(View.GONE);

								//loading image
								if(p.IMG!=null &&  !p.IMG.isEmpty()){
										Picasso.with(UserActivity.this)
								    .load(p.IMG)
								    .placeholder(R.mipmap.dummy_avatar)
								    .error(R.mipmap.dummy_avatar)
								    .into(imageView);
								}
								
								
								Log.e("UserActivity", p.IMG);

								// complete data loading and register listener
								//registerListenertToscrollBTN();
								//loading newsfeed data
								
								findViewById(R.id.profile_fragment).setVisibility(View.GONE);
								
								userPubInfo=p;
								new LoaduserNewsFeed().execute();
							}
						} else if (userPubInfoList == null) {
							// Zero results found
							findViewById(R.id.custom_progress_user_info).setVisibility(View.VISIBLE);
							findViewById(R.id.custom_progress_user_info).findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
							TextView textPreparing = (TextView) findViewById(R.id.custom_progress_user_info).findViewById(R.id.textPreparing);
							textPreparing.setText("No visible activity");
							AnimationTween.animateViewPosition(findViewById(R.id.custom_progress_user_info), UserActivity.this);
						}
					}
				}

			});

		}

	}
	
	
	class LoaduserNewsFeed extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {

			super.onPreExecute();

			findViewById(R.id.custom_progress_user_info).setVisibility(
					View.VISIBLE);
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating JSONData class object
			foodyResturent = new FoodyResturent();

			try {

				if (userid.isEmpty()) {
					this.cancel(true);
					// goto login activity

					// return null;
				}

				String postData = userid;
				userNewsFeedList = foodyResturent.getUserNewsFeed(postData);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all data
			findViewById(R.id.custom_progress_user_info).setVisibility(View.GONE);
			AnimationTween.animateViewPosition(lv, UserActivity.this);
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status

					if (userNewsFeedList == null || userNewsFeedList.size() <= 0) {
						findViewById(R.id.custom_progress_user_info).setVisibility(View.VISIBLE);
						findViewById(R.id.custom_progress_user_info).findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
						TextView textPreparing = (TextView) findViewById(R.id.custom_progress_user_info).findViewById(R.id.textPreparing);
						textPreparing.setText("No visible activity");
						AnimationTween.animateViewPosition(findViewById(R.id.custom_progress_user_info), UserActivity.this);

					} else if (userNewsFeedList != null) {
						// Check for all possible status

						// Successfully got places details
						if (userNewsFeedList != null) {
							// 
							userNewsFeedListItems= new ArrayList<HashMap<String, String>>();
							for (NewsFeed p : userNewsFeedList) {
								
								HashMap<String, String> map = new HashMap<String, String>();
								if(p instanceof Favorite ){
									Favorite f=(Favorite)p;
									map.put("Type", f.Type);
									map.put("a", f.FavResName);									
									map.put("d", f.FavDate);
									map.put("b", f.FavResId);
									map.put("c", f.RESIMG);
									map.put("e", "");
									
								}else if(p instanceof Follow ){
									Follow f=(Follow)p;
									map.put("Type", f.Type);
									map.put("a", f.FolloerName);
									map.put("d", f.FolloDate);
									map.put("b", f.FolloerId);
									map.put("c", f.IMGFOLWR);
									map.put("e", "");
									
								}else if(p instanceof PhotoShare ){
									PhotoShare f=(PhotoShare)p;
									map.put("Type", f.Type);
									map.put("a", f.PhotResName);
									map.put("d", f.PhotDate);
									map.put("b", f.PhotRes);
									map.put("c", f.Photo);
									map.put("e", "");
									
								}else if(p instanceof Comment ){
									Comment f=(Comment)p;
									map.put("Type", f.Type);
									map.put("a", f.RevComm);
									map.put("d", f.RevDate);
									map.put("b", f.RevRes);
									map.put("c", f.RevRat);
									map.put("e", f.RevResName);
									
								}

								
								// adding HashMap to ArrayList
								userNewsFeedListItems.add(map);
	
							}
							
							MyListAdapterFeed adapter = new MyListAdapterFeed(
									UserActivity.this, userNewsFeedListItems,
									new String[] { "Type", "a", "d" , "b", "c", "e"}, userPubInfo);

							// Adding data into listview
							lv.setAdapter(adapter);
							adapter.notifyDataSetChanged();
							
							// complete data loading and register listener
							registerListenertToscrollBTN();
							
							lv.setOnItemClickListener(new OnItemClickListener() {

								@Override
								public void onItemClick(AdapterView<?> parent, View view,
										int position, long id){
									// TODO Auto-generated method stub
									HashMap<String, String> map =userNewsFeedListItems.get(position);
									String type=map.get("Type");
									
									if(type.equals(FoodyResturent.FEED_TYPE_RATING) || type.equals(FoodyResturent.FEED_TYPE_FAV) || type.equals(FoodyResturent.FEED_TYPE_PHOTO) || type.equals(FoodyResturent.FEED_TYPE_REVIEW)){
										Intent intent=new Intent(UserActivity.this, TabMainActivity.class);
										intent.putExtra("Id", map.get("b"));
										startActivity(intent);
									}else if( type.equals(FoodyResturent.FEED_TYPE_FOLLOW) ){
										Intent intent=new Intent(UserActivity.this, UserActivity.class);
										intent.putExtra("userid", map.get("b"));
										startActivity(intent);
									}
								}
							} );
							
						} else if (userPubInfoList == null) {
							// Zero results found
							alert.showAlertDialog(UserActivity.this,
									getString(R.string.app_name),
									" No Resturent found.", false);
						}
					}
					
					
				}

			});
			
			//dealyInterstitialAd();
		}

	}
	
	
	void dealyInterstitialAd(){
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				
				//load Interstitial ad 
				loadInterstitial();	
			}
		}, 5000);
	}
	
	
}
