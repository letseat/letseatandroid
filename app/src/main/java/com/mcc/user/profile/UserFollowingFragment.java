package com.mcc.user.profile;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.user.Follower;
import com.foody.nearby.AlertDialogManager;
import com.foody.nearby.GPSTracker;
import com.foody.nearby.MyListAdapterFollowing;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mcc.letseat.R;
import com.mcc.libs.AnimationTween;

/**
 * Provide List of user's following
 * @author Arif
 *
 */
public class UserFollowingFragment extends Fragment {

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Google Places
	FoodyResturent foodyResturent;

	// Places List
	ArrayList<Follower> followerList;

	// Button
	ImageView btnShowOnMap;

	

	// Places Listview
	ListView lv;

	// ListItems data
	ArrayList<HashMap<String, String>> followingListItems = new ArrayList<HashMap<String, String>>();

	//admob
	AdView adView = null;

	ImageView searchAny;

	String searchKeywords;

	// post value
	// String LocId;
	int currentPage = 0;
	int totalPage = 1;

	String userid;

	TextView txtTitleNearBy;
	
	View currentView=null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		
		userid = getArguments().getString("userid");// getIntent().getStringExtra("userid");		
		return  currentView=inflater.inflate(R.layout.n_activity_main, container, false);

	}
	
	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		// Getting listview
				lv = (ListView) currentView.findViewById(R.id.list);

				lv.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {

						Follower r = followerList.get(position);

						// restaurentId= resturent.Id;

						Intent in = new Intent(getActivity().getApplicationContext(),UserActivity.class);
						in.putExtra("userid", r.FOLLOWERID);				
						startActivity(in);

					}
				});

				adView = (AdView) this.currentView.findViewById(R.id.adView);

				AdRequest adRequest = new AdRequest.Builder().build();

				adView.loadAd(adRequest);

				adView.setAdListener(new AdListener() {
					public void onAdLoaded() {
					}

					public void onAdFailedToLoad(int errorcode) {
						System.out.println("Error:" + errorcode);
					}

				});

				new LoadFollowers().execute();

				txtTitleNearBy = (TextView) currentView.findViewById(R.id.txtTitleNearBy);
				txtTitleNearBy.setText("Following (0)" );
	}

	@Override
	public void onPause() {
		super.onPause();

		if (adView != null)
			adView.pause();

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		if (adView != null)
			adView.destroy();
		super.onDestroy();
	}

	class LoadFollowers extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			lv.setOnScrollListener(null);
			super.onPreExecute();
			
			currentView.findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating JSONData class object
			foodyResturent = new FoodyResturent();

			try {

				if (userid.isEmpty()) {
					this.cancel(true);
					// goto login activity

					// return null;
				}

				
				followerList = foodyResturent.getFollowingList(userid);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all data
			
			
			if(currentView!=null){
				
				AnimationTween.animateView(currentView.findViewById(R.id.main_custom_progress), getActivity());
				AnimationTween.animateViewPosition(lv, getActivity());
			}
			
			if(getActivity()==null)
				return;
			
			// updating UI from Background Thread
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status

					if (followerList == null || followerList.size() <= 0) {
						if(currentView!=null){
						currentView.findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);
						currentView.findViewById(R.id.main_custom_progress).findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
						TextView textPreparing = (TextView) currentView.findViewById(R.id.main_custom_progress).findViewById(R.id.textPreparing);
						textPreparing.setText("You are not following anyone.");
						AnimationTween.animateViewPosition(currentView.findViewById(R.id.main_custom_progress), getActivity());
						
						}

					} else if (followerList != null) {
						// Check for all possible status
						
						// Successfully got places details
						if (followerList != null) {
							followingListItems = new ArrayList<HashMap<String, String>>();
							// loop through each place
							for (Follower p : followerList) {

								HashMap<String, String> map = new HashMap<String, String>();

								map.put("FOLLOWER_NAME", p.FOLLOWER_NAME);
								map.put("IMG", p.IMG);
								map.put("TOTALFOLLOWER", "Follower "+p.TOTALFOLLOWER + ", Review "+ p.TOTALFOLLOWER);
								map.put("ID", p.ID);
								map.put("FOLLOWERID", p.USERID);

								// adding HashMap to ArrayList
								followingListItems.add(map);
							}
							// list adapter
							MyListAdapterFollowing adapter = new MyListAdapterFollowing(getActivity(), followingListItems,new String[] { "FOLLOWER_NAME", "IMG","TOTALFOLLOWER" });

							// Adding data into listview
							lv.setAdapter(adapter);
							adapter.notifyDataSetChanged();
							currentPage++;

							txtTitleNearBy.setText("Following ("
									+ followingListItems.size() + ")");

							lv.setOnScrollListener(new OnScrollListener() {

								@Override
								public void onScrollStateChanged(
										AbsListView view, int scrollState) {
									// TODO Auto-generated method stub

								}

								@Override
								public void onScroll(AbsListView view,
										int firstVisibleItem,
										int visibleItemCount, int totalItemCount) {
									// TODO Auto-generated method stub
									boolean loadMore = firstVisibleItem
											+ visibleItemCount >= totalItemCount;

									if (loadMore && currentPage < totalPage) {
										Toast.makeText(getActivity(),
												"load More", Toast.LENGTH_SHORT)
												.show();

										new LoadFollowers().execute();

									}
								}
							});

						}

						else if (followerList == null) {
							if(currentView!=null){
									currentView.findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);
									currentView.findViewById(R.id.main_custom_progress).findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
									TextView textPreparing = (TextView) currentView.findViewById(R.id.main_custom_progress).findViewById(R.id.textPreparing);
									textPreparing.setText("You are not following anyone.");
									AnimationTween.animateViewPosition(currentView.findViewById(R.id.main_custom_progress), getActivity());
								}
						}
					}
				}

			});

		}

	}

	

}
