package com.mcc.user.profile;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.Review;
import com.foody.nearby.AlertDialogManager;
import com.foody.nearby.GPSTracker;
import com.foody.nearby.MyListAdapterReviewUser;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mcc.letseat.R;
import com.mcc.letseat.tab.TabMainActivity;
import com.mcc.libs.AnimationTween;

/**
 * Provide list of all reviews by user
 * @author Arif
 *
 */
public class UserReviewFragment extends Fragment {

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Google Places
	FoodyResturent foodyResturent;

	// Places List
	ArrayList<Review> reviewList;

	// Button
	ImageView btnShowOnMap;

	

	// Places Listview
	ListView lv;

	// ListItems data
	ArrayList<HashMap<String, String>> reviewListItems = new ArrayList<HashMap<String, String>>();

	// //admob
	AdView adView = null;

	ImageView searchAny;

	TextView restaurantName;

	String userid;

	View currentView=null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment

		userid = getArguments().getString("userid");
		System.out.println("===============userid==================="+ userid);
		
		 return currentView =inflater.inflate(R.layout.restaurant_review, container, false);
		
	}

	@Override
	public void onStart() {
		super.onStart();

		

		// Getting listview
		lv = (ListView) currentView.findViewById(R.id.list);

		restaurantName = (TextView) currentView.findViewById(R.id.restName);
		restaurantName.setVisibility(View.GONE);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
			 if(reviewList!=null){
					
					Intent intent=new Intent(getActivity(), TabMainActivity.class);
					intent.putExtra("Id", reviewList.get(position).RESID);
					startActivity(intent);
				}
				
				
				/*
				 * Restaurent resturent = (Restaurent)
				 * resturentList.get(position); //restaurentId= resturent.Id;
				 * 
				 * Intent in = new
				 * Intent(getApplicationContext(),ResturentActivity.class);
				 * in.putExtra( "Id" , resturentList.get(position).Id);
				 * 
				 * startActivity(in);
				 */

			}
		});

		adView = (AdView) this.currentView.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder().addTestDevice(
				AdRequest.DEVICE_ID_EMULATOR).build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});

		// Toast.makeText(ResturentListView.this, "hello"+searchKeywords,
		// Toast.LENGTH_LONG).show();

		// restaurentId=getActivity().getIntent().getStringExtra("restaurentId");
		restaurantName.setText(getActivity().getIntent().getStringExtra(
				"restaurentName"));

		new LoadRewiews().execute();
		// handleIntent(getIntent());

	}

	

	

	@Override
	public void onPause() {
		super.onPause();

		if (adView != null)
			adView.pause();

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		if (adView != null)
			adView.destroy();
		super.onDestroy();
	}

	class LoadRewiews extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			currentView.findViewById(R.id.menu_custom_progress).setVisibility(View.VISIBLE);
		}

		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {

				System.out.println("===============userid==================="+ userid);
				String postData = "userid=" + userid;
				

				reviewList = foodyResturent.getReviewList(postData,  FoodyResturent.TYPE_USER);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			// pDialog.dismiss();
			if(currentView!=null){
				
					AnimationTween.animateView(currentView.findViewById(R.id.menu_custom_progress), getActivity());
					AnimationTween.animateViewPosition(lv, getActivity());
			}
			
			if(getActivity()==null)
				return;
			
			// updating UI from Background Thread
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status

					if (reviewList == null || reviewList.size() <= 0) {

						if(currentView!=null){
							currentView.findViewById(R.id.menu_custom_progress).setVisibility(View.VISIBLE);
							currentView.findViewById(R.id.menu_custom_progress).findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
							TextView textPreparing = (TextView) currentView.findViewById(R.id.menu_custom_progress).findViewById(R.id.textPreparing);
							textPreparing.setText("No reviews posted.");
							AnimationTween.animateViewPosition(currentView.findViewById(R.id.menu_custom_progress), getActivity());
							}

					} else if (reviewList != null) {
						// Check for all possible status

						// Successfully got places details
						if (reviewList != null) {
							
							reviewListItems = new ArrayList<HashMap<String, String>>();
							// loop through each place
							for (Review p : reviewList) {

								HashMap<String, String> map = new HashMap<String, String>();
								map.put("ID", p.ID);
								map.put("USERID", p.USERID);
								map.put("FULLNAME", p.FULLNAME);
								map.put("metaData", p.NO_OF_REVIEW
										+ " review  " + p.NO_OF_FOLLOWERS + " "
										+ "followers");
								map.put("REVIEW", p.REVIEW);
								map.put("DATE", p.DATE_DIFF);

								map.put("RATING", " " + p.RATING + " ");
								map.put("NO_OF_COMMENTS", " "
										+ p.NO_OF_COMMENTS + " ");

								map.put("NO_OF_REVIEW_LIKE",
										p.NO_OF_REVIEW_LIKE);

								map.put("IMG", p.IMG);
								map.put("FOLLOW", p.FOLLOW);
								map.put("RESNAME", p.RESNAME);
								map.put("RESIMG", p.RESIMG);

								// map.put("RATING", p.RATING);

								// adding HashMap to ArrayList
								reviewListItems.add(map);
							}
							// list adapter
							MyListAdapterReviewUser adapter = new MyListAdapterReviewUser(
									getActivity(), reviewListItems,
									new String[] { "FULLNAME", "metaData",
											"REVIEW", "DATE", "RATING",
											"NO_OF_COMMENTS",
											"NO_OF_REVIEW_LIKE", "IMG",
											"FOLLOW", "RESNAME", "RESIMG" });

							// Adding data into listview
							lv.setAdapter(adapter);
						}

						else if (reviewList == null) {
							// Zero results found
							if(currentView!=null){
								currentView.findViewById(R.id.menu_custom_progress)
									.setVisibility(View.VISIBLE);
								currentView.findViewById(R.id.menu_custom_progress)
									.findViewById(R.id.progressBarPreparing)
									.setVisibility(View.GONE);
								TextView textPreparing = (TextView) currentView
									.findViewById(R.id.menu_custom_progress)
									.findViewById(R.id.textPreparing);
								textPreparing.setText("No Review found.");
								
								}
							
							
						}
					}
				}

			});

		}

	}

}
