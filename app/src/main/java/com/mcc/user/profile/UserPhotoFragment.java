package com.mcc.user.profile;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.Photo;
import com.foody.nearby.AlertDialogManager;
import com.foody.nearby.GPSTracker;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mcc.letseat.R;
import com.mcc.letseat.imageslider.adapter.GridViewImageAdapterNew;
import com.mcc.letseat.imageslider.helper.AppConstant;
import com.mcc.letseat.imageslider.helper.Utils;
import com.mcc.libs.AnimationTween;

/**
 * Provide all photo's uploaded by user
 * @author Arif
 *
 */

public class UserPhotoFragment extends Fragment {
	   

	

		// Alert Dialog Manager
		AlertDialogManager alert = new AlertDialogManager();

		// Google Places
		FoodyResturent foodyResturent;

		// Places List
		ArrayList<Photo> menuList;

		// Button
		ImageView btnShowOnMap;

		

		

		// ListItems data
		ArrayList<HashMap<String, String>> resturantListItems = new ArrayList<HashMap<String, String>>();

		// //admob
		AdView adView = null;

		ImageView searchAny;
		
		TextView restaurantName;

		String userid;
		
		
		// 
		private Utils utils;
		
		private GridViewImageAdapterNew adapter;
		private GridView gridView;
		private int columnWidth;
		View currentView=null;
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		        // Inflate the layout for this fragment
				userid = getArguments().getString("userid");
		        return currentView=inflater.inflate(R.layout.activity_grid_view, container, false);
		 }
		 
		
		@Override	
		public void onStart() {
			super.onStart();

			
			adView = (AdView) this.currentView.findViewById(R.id.adView);

			AdRequest adRequest = new AdRequest.Builder().addTestDevice(
					AdRequest.DEVICE_ID_EMULATOR).build();

			adView.loadAd(adRequest);

			adView.setAdListener(new AdListener() {
				public void onAdLoaded() {
				}

				public void onAdFailedToLoad(int errorcode) {
					System.out.println("Error:" + errorcode);
				}

			});
			
			//Toast.makeText(ResturentListView.this, "hello"+searchKeywords, Toast.LENGTH_LONG).show();
			
		
			 
			 restaurantName= (TextView)currentView.findViewById(R.id.restName);
			 restaurantName.setText(getArguments().getString("restaurentName"));
			
			gridView = (GridView) currentView.findViewById(R.id.grid_view);

			utils = new Utils(getActivity());

			// Initilizing Grid View
			InitilizeGridLayout();

			
			new LoadPhotos().execute();
			
		}
		
		
		private void InitilizeGridLayout() {
			Resources r = getResources();
			float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
					AppConstant.GRID_PADDING, r.getDisplayMetrics());

			columnWidth = (int) ((utils.getScreenWidth() - ((AppConstant.NUM_OF_COLUMNS + 1) * padding)) / AppConstant.NUM_OF_COLUMNS);

			gridView.setNumColumns(AppConstant.NUM_OF_COLUMNS);
			gridView.setColumnWidth(columnWidth);
			gridView.setStretchMode(GridView.NO_STRETCH);
			gridView.setPadding((int) padding, (int) padding, (int) padding,
					(int) padding);
			gridView.setHorizontalSpacing((int) padding);
			gridView.setVerticalSpacing((int) padding);
		}


		

		@Override
		public void onPause() {
			super.onPause();

			if (adView != null)
				adView.pause();

		}

		@Override
		public void onResume() {
			super.onResume();

			if (adView != null)
				adView.resume();
		}

		@Override
		public void onDestroy() {
			if (adView != null)
				adView.destroy();
			super.onDestroy();
		}

		class LoadPhotos extends AsyncTask<String, String, String> {

			/**
			 * Before starting background thread Show Progress Dialog
			 * */
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				
				currentView.findViewById(R.id.grid_view_custom_progress).setVisibility(View.VISIBLE);
			}

			/**
			 * getting Places JSON
			 * */
			protected String doInBackground(String... args) {
				// creating Places class object
				foodyResturent = new FoodyResturent();

				try {

					
					String postData = "userid=" + userid;
					

					menuList = foodyResturent.getPhotoList(postData, FoodyResturent.TYPE_USER);

				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}

			/**
			 * After completing background task Dismiss the progress dialog and show
			 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
			 * from background thread, otherwise you will get error
			 * **/
			protected void onPostExecute(String file_url) {
				// dismiss the dialog after getting all products
				//pDialog.dismiss();
				if(currentView!=null){
						AnimationTween.animateView(currentView.findViewById(R.id.grid_view_custom_progress), getActivity());
						AnimationTween.animateViewPosition(gridView, getActivity());
				}
			
				if(getActivity()==null)
					return;
				
				// updating UI from Background Thread
				getActivity().runOnUiThread(new Runnable() {
					public void run() {
						/**
						 * Updating parsed Places into LISTVIEW
						 * */
						// Get json response status

						if (menuList == null || menuList.size() <= 0) {

							
							if(currentView!=null){
								currentView.findViewById(R.id.grid_view_custom_progress).setVisibility(View.VISIBLE);
								currentView.findViewById(R.id.grid_view_custom_progress).findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
								TextView textPreparing = (TextView) currentView.findViewById(R.id.grid_view_custom_progress).findViewById(R.id.textPreparing);
								textPreparing.setText("Never upload any photo");
								AnimationTween.animateViewPosition(currentView.findViewById(R.id.grid_view_custom_progress), getActivity());
								}
							
							 
						} else if (menuList != null) {
							// Check for all possible status

							// Successfully got places details
							if (menuList != null) {
								resturantListItems = new ArrayList<HashMap<String, String>>();
								// loop through each place
								for (Photo p : menuList) {
									
									HashMap<String, String> map = new HashMap<String, String>();
									map.put("IMG", p.IMG);								
									map.put("CAPTION", p.CAPTION);
									map.put("RESID", p.RESID);
									map.put("THUMBIMG", p.THUMBIMG);
									map.put("STATUS", p.STATUS);

									// adding HashMap to ArrayList
									resturantListItems.add(map);
								}
								
								HashMap<String, String> map = new HashMap<String, String>();
								map.put("IMG","add");	
								resturantListItems.add(map);
								/*// list adapter
								MyPhotoListAdapter adapter = new MyPhotoListAdapter(
										getActivity(),
										resturantListItems,
										new String[] {  "IMG"});
								

								// Adding data into listview
								lv.setAdapter(adapter);
								*/
								
								// Gridview adapter
								adapter = new GridViewImageAdapterNew(getActivity(), resturantListItems,columnWidth, new String[] {  "IMG", "CAPTION","RESID", "THUMBIMG"}, GridViewImageAdapterNew.GalleryTypeGal);

								// setting grid view adapter
								
								gridView.setAdapter(adapter);
								adapter.notifyDataSetChanged();
							}

							else if (menuList == null) {
								// Zero results found
								if(currentView!=null){
									currentView.findViewById(R.id.grid_view_custom_progress)
										.setVisibility(View.VISIBLE);
									currentView.findViewById(R.id.grid_view_custom_progress)
										.findViewById(R.id.progressBarPreparing)
										.setVisibility(View.GONE);
									TextView textPreparing = (TextView) currentView
										.findViewById(R.id.grid_view_custom_progress)
										.findViewById(R.id.textPreparing);
									textPreparing.setText("Nothing found.");
									}
							}
						}
					}

				});

			}

		}
		
		
		
		

		/*@Override
		public boolean onNavigationItemSelected(int arg0, long arg1) {
			// TODO Auto-generated method stub
			return false;
		}*/
		
		
		/*private void showPopUp() {

			AlertDialog.Builder helpBuilder = new AlertDialog.Builder(getActivity());
			// helpBuilder.setTitle("Pop Up");
			helpBuilder.setMessage("Nothing Found. Want to request this suggestion?");
			helpBuilder.setPositiveButton("No",
					new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {

						}
					});

			helpBuilder.setNegativeButton("Yes",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

							Intent in = new Intent(
									getActivity().getApplicationContext(),
									SuggetionRequest.class);
							in.putExtra("keyword", userid);

							startActivity(in);
						}
					});

			// Remember, create doesn't show the dialog
			AlertDialog helpDialog = helpBuilder.create();
			helpDialog.show();

		}*/

		
	}

