package com.mcc.user.profile;

import java.util.ArrayList;
import java.util.HashMap;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.NewsFeed.NewsFeed;
import com.foody.jsondata.user.UserPubInfo;
import com.foody.nearby.AlertDialogManager;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mcc.letseat.R;

/**
 * All activity of a user 
 * @author Arif
 *
 */
public class UserNewsFeed extends Fragment {

	// admob
	AdView adView = null;
	
	View currentView=null;
	
	// Alert Dialog Manager
		AlertDialogManager alert = new AlertDialogManager();

		// Google Places
		FoodyResturent foodyResturent;

		// UserPubInfo 
		ArrayList<UserPubInfo> userPubInfoList;
		ArrayList<NewsFeed> userNewsFeedList;
		
		// ListItems data
		ArrayList<HashMap<String, String>> followerListItems = new ArrayList<HashMap<String, String>>();
		
		ArrayList<HashMap<String, String>> userNewsFeedListItems = new ArrayList<HashMap<String, String>>();

		UserPubInfo userPubInfo;

		String userid = null;
		
		ListView lv;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		userid = getArguments().getString("userid");
		 currentView = inflater.inflate(R.layout.activity_user_info,container, false);

		return currentView;
	}

	@Override
	public void onStart() {
		super.onStart();

		// admob
		adView = (AdView) this.currentView.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder().build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});

	}

}
