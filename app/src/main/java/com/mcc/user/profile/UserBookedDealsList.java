package com.mcc.user.profile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.foody.jsondata.LetsEatRequest;
import com.foody.jsondata.Offer;
import com.foody.jsondata.user.UserBookedDeal;
import com.foody.nearby.NearByNativeRestaurantList;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.mcc.letseat.R;
import com.mcc.letseat.Adapter.BookedDealsAdapter;
import com.mcc.letseat.tab.TabMainActivity;
import com.mcc.libs.ActivityWithSliding;
import com.mcc.libs.AnimationTween;
import com.mcc.user.SavedPrefernce;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class UserBookedDealsList  extends ActivityWithSliding{
	
	String userId;
	
	ListView lv;
	View progress; 
	
	//Admob
		AdView adView = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_booked_deals);
		
		lv = (ListView) findViewById(R.id.list_booked_deals);
		progress = (View) findViewById(R.id.booked_deals_custom_progress);
		
		userId = new SavedPrefernce(this).getUserID(); 
		
		
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, final View view,
					int position, long arg3) {
				// TODO Auto-generated method stub
				Toast.makeText(UserBookedDealsList.this, "Wait. Redeeming Deal...", Toast.LENGTH_SHORT).show();
				
				String BookingId =((UserBookedDeal)lv.getItemAtPosition(position)).BookingId;
				
				
				
				Button button= (Button)view.findViewById(R.id.button1);
				
			
				if(!TextUtils.isEmpty(button.getText())){
					
					button.setText("");
					View v=view.findViewById(R.id.progressBarPreparing);
					v.setVisibility(View.VISIBLE);
					redeem(BookingId, ((UserBookedDeal)lv.getItemAtPosition(position)), button,  v);
				}
				
				
				
				
			}

			
		});
		
		Response.Listener<String> listener=new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                	// Result handling 
                	progress.setVisibility(View.GONE);
                	ArrayList<UserBookedDeal> bookedDels = new LetsEatRequest().getBookedDeals(response);
                	if(bookedDels != null && bookedDels.size() > 0){
                		BookedDealsAdapter adapter =new BookedDealsAdapter(UserBookedDealsList.this,  bookedDels);
                		lv.setAdapter(adapter);
                		adapter.notifyDataSetChanged();
                		
                		
                	}else{
                		progress.setVisibility(View.VISIBLE);
						TextView textPreparing = (TextView) progress.findViewById(R.id.textPreparing);
						textPreparing.setText("You have no wishlist. Make your wishlist. Please check deals section");
						progress.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
						
						AnimationTween.animateViewPosition(progress, UserBookedDealsList.this);
                	}
                	
    		    
                } catch (Exception e) {
                	
                	
                    e.printStackTrace();
                }
            }
        };
		
		
		
		StringRequest postRequestBookedDeals = new StringRequest(Request.Method.POST, LetsEatRequest.USER_BOOKED_DEALS,
		        listener,
		        new Response.ErrorListener() {
		            @Override
		            public void onErrorResponse(VolleyError error) {
		                error.printStackTrace();
		            }
		        }
		) {
		    @Override
		    protected Map<String, String> getParams()
		    {
		        Map<String, String>  params = new HashMap<String, String>();
		        // the POST parameters:
		        params.put("UserId", userId);
		        return params;
		    }
		};
		
		Volley.newRequestQueue(this).add(postRequestBookedDeals);
		
		
		adView = (AdView) this.findViewById(R.id.adView);
		adView.setVisibility(View.GONE);

		AdRequest adRequest = new AdRequest.Builder().build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
				adView.setVisibility(View.VISIBLE);
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});
		
		
		//set home button
		setHomeButton(this);
		
		
	}
	
	private void redeem(final String BookingId, final UserBookedDeal ubd, final Button button, final View view) {
		

		StringRequest postRequestReedem = new StringRequest(Request.Method.POST, LetsEatRequest.USER_REDEEM_DEALS,
		        new Response.Listener<String>() {
		            @Override
		            public void onResponse(String response) {
		                try {
		                	// Result handling 

		                	if(response.equals("Success")){
		                		((BookedDealsAdapter)lv.getAdapter()).remove(ubd);
		                		Toast.makeText(UserBookedDealsList.this, "Thank you for Redeeming the Deal", Toast.LENGTH_LONG).show();
		                	}else{
		                		button.setText("Redeem");					
		    					view.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
		    					 Toast.makeText(UserBookedDealsList.this, "Can't Redeem. Try again", Toast.LENGTH_LONG).show();
		                	}
		                	
		                	
		    		    
		                } catch (Exception e) {
		                	
		                	
		                    e.printStackTrace();
		                }
		            }
		        },
		        new Response.ErrorListener() {
		            @Override
		            public void onErrorResponse(VolleyError error) {
		                error.printStackTrace();
		                
		                Toast.makeText(UserBookedDealsList.this, "Can't Redeem. Try again", Toast.LENGTH_SHORT).show();
		                
		                button.setText("Redeem");					
    					view.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
		            }
		        }
		) {
		    @Override
		    protected Map<String, String> getParams()
		    {
		        Map<String, String>  params = new HashMap<String, String>();
		        // the POST parameters:
		        params.put("UserId", userId);
		        params.put("BookingId", BookingId); 
		        return params;
		    }
		};
		
		Volley.newRequestQueue(this).add(postRequestReedem);
		
		
		
		
	}
	
	
	
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
	
		case R.id.action_search:
			// search action			
			addSearchLayout((RelativeLayout)findViewById(R.id.rr_booked_deals));			
			return true;
		
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	
	@Override
	protected void onStart() {		
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	public void onPause() {
		super.onPause();

		if (adView != null)
			adView.pause();

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		
		if (adView != null)
		{	adView.destroy();}
		
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}
	
	

}
