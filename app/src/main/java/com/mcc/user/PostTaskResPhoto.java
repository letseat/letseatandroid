package com.mcc.user;

import com.mcc.letseat.R;

import namelessinc.asynctaskmanager.Task;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.widget.Toast;

/**
 * Restaurant photo upload task
 * @author Arif
 *
 */
public class PostTaskResPhoto extends Task {
	/*String user = "";
	String cat = "";
	String title = "";
	String summary = "";
	String isFB = "";
	Uri fileUri = null;*/
	int type = 2;

	Uri photoname = null;
	String userid;
	String resid;
	String caption;
	Context ctx;
	

	public PostTaskResPhoto(Context ctx, Resources resources, String start,
			Uri photoname, String userid, String resid, String caption,  int type) {
		super(ctx, resources, start);
		this.photoname = photoname;
		this.userid = userid;
		this.resid = resid;
		this.caption=caption;
		this.type = type;
		this.ctx=ctx;
	}

	

	protected Boolean doInBackground(Void... arg0) {
		FoodyUserWebApi w = new FoodyUserWebApi();
		//return w.makePost(this, user, title, summary, fileUri, cat, type, isFB);
		//return w.makePostResPhoto(this, photoname, userid, resid, caption, type);
		return w.makePostRestPhoto(this, photoname, userid, resid, caption, type);

	}
	
	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		Toast.makeText(ctx, ctx.getString(R.string.PhotoUploadMsg), Toast.LENGTH_LONG).show();
	}

}
