package com.mcc.user;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;

import namelessinc.asynctaskmanager.Task;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;

import android.net.Uri;
import android.util.Log;

import com.foody.jsondata.FoodyResturent;
import com.mcc.libs.CustomMultiPartEntity;
import com.mcc.libs.CustomMultiPartEntity.ProgressListener;
import com.mcc.libs.Functions;

/**
 * Used to execute task user profile picture, Restaurant photo
 * @author Arif
 *
 */
public class FoodyUserWebApi {
    
    
  //fileUri photoname
	public boolean makePost(final Task task,  Uri fileUri,  String username,  String pwd,  String fullName, String email, String about,  String phone,  String gender,  String country, String city,  String zip, String regType, int type){
	
	
    //public boolean makePost(final Task task, String user, String title, String summary, Uri fileUri, String cat, int type, String isFB){
       
            String response;// = postFileMultipart(url, file, fileName, mimeType, nameValuePairs) ;            
            try {
            	
            	String url = new String(FoodyResturent.PROFILE_INSERT_URL) ;
            	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("username", username));
                nameValuePairs.add(new BasicNameValuePair("pwd", pwd));
                nameValuePairs.add(new BasicNameValuePair("name", fullName));
                nameValuePairs.add(new BasicNameValuePair("email", email));
                nameValuePairs.add(new BasicNameValuePair("about", about));
                nameValuePairs.add(new BasicNameValuePair("phone", phone)) ; 
                nameValuePairs.add(new BasicNameValuePair("gender", gender));
                nameValuePairs.add(new BasicNameValuePair("country", country));
                nameValuePairs.add(new BasicNameValuePair("city", city));
                nameValuePairs.add(new BasicNameValuePair("zip", zip));
                nameValuePairs.add(new BasicNameValuePair("usertype", regType));
                
                File file=null; String fileName=null;
                if(type!=1)
                {
                	file = new File(fileUri.getPath()); 
                	if(type==3)
                		file = new File(fileUri.getPath());
                	else 
                	{
                		Log.e("Image......", "Image......");
                		file = new Functions().resizeImageFile(fileUri);// File(fileUri.getPath()); 
                	}
                		 
    	            if(type==2 || type==3) fileName="photoname";
    	            else 
                		fileName="photoname";
                }
                
                String mimeType = "image/*" ;
               
                
                /* prepare HttpClient parameter */
                HttpParams params = new BasicHttpParams();
                params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
                /* prepare request */
                DefaultHttpClient mHttpClient = new DefaultHttpClient(params);
                HttpPost httppost = new HttpPost(url);
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs)) ;
                
                
                final long totalSize;
                
                if(file!=null)
                	totalSize=file.length()+username.length()+pwd.length()+fullName.length()+email.length()+phone.length()+gender.length()+country.length()+city.length()+zip.length();
                else
                	totalSize=username.length()+pwd.length()+fullName.length()+email.length()+phone.length()+gender.length()+country.length()+city.length()+zip.length();
                              
                /* add Multipart entity */
                CustomMultiPartEntity multipartEntity = new CustomMultiPartEntity(new ProgressListener()
        		{
        			@Override
        			public void transferred(long num)
        			{
        				task.updateProgress((int) ((num / (float) totalSize) * 100));
//        				pd.setProgress((int) ((num / (float) totalSize) * 100));
//        				System.out.println((int) ((num / (float) totalSize) * 100));
        			}
        		});
                
                //MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
                
                for (int i = 0 ; i < nameValuePairs.size() ; i++){
                    NameValuePair np = nameValuePairs.get(i) ;
                    String name = np.getName() ;
                    String value = np.getValue() ;
                    multipartEntity.addPart(name, new StringBody(value)) ;
                }
                
                if (file!=null)
                {
                    ContentBody cbFile = new FileBody(file, mimeType);
                    multipartEntity.addPart(fileName, cbFile);
                }
                
                httppost.setEntity(multipartEntity);
                /* execute to get response */
                HttpResponse res = mHttpClient.execute(httppost) ;
                response = readResponse(res) ;
               
                Log.e("UPLOAD_RESPONSE", response) ;
                
            } catch(Exception e){
            e.printStackTrace() ;
            return false ;
        }
        return true ;
    }
	
	
	public boolean changeProfilePic(final Task task,  Uri fileUri,  String username, String id,  int type){
		
		
	    //public boolean makePost(final Task task, String user, String title, String summary, Uri fileUri, String cat, int type, String isFB){
	       
	            String response;// = postFileMultipart(url, file, fileName, mimeType, nameValuePairs) ;            
	            try {
	            	
	            	String url = new String(FoodyResturent.PROFILE_EDIT_URL) ;
	            	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	                nameValuePairs.add(new BasicNameValuePair("username", username));
	                nameValuePairs.add(new BasicNameValuePair("id", id));
	                
	                File file=null; String fileName=null;
	                if(type!=1)
	                {
	                	file = new File(fileUri.getPath()); 
	                	if(type==3)
	                		file = new com.mcc.letseat.Functions().resizeImageFile(fileUri.getPath());
	                	
	                	else 
	                	{
	                		Log.e("Image......", "Image......");
	                		//file = new Functions().resizeImageFile(fileUri);
	                		file = new com.mcc.letseat.Functions().resizeImageFile(fileUri.getPath());
	                	}
	                		 
	    	            if(type==2 || type==3) fileName="photoname";
	    	            else 
	                		fileName="photoname";
	                }
	                
	                String mimeType = "image/*" ;
	               
	                
	                /* prepare HttpClient parameter */
	                HttpParams params = new BasicHttpParams();
	                params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
	                /* prepare request */
	                DefaultHttpClient mHttpClient = new DefaultHttpClient(params);
	                HttpPost httppost = new HttpPost(url);
	                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs)) ;
	                
	                
	                final long totalSize;
	                
	                if(file!=null)
	                	totalSize=file.length()+username.length()+id.length();
	                else
	                	totalSize=username.length()+id.length();
	                              
	                /* add Multipart entity */
	                CustomMultiPartEntity multipartEntity = new CustomMultiPartEntity(new ProgressListener()
	        		{
	        			@Override
	        			public void transferred(long num)
	        			{
	        				task.updateProgress((int) ((num / (float) totalSize) * 100));
//	        				pd.setProgress((int) ((num / (float) totalSize) * 100));
//	        				System.out.println((int) ((num / (float) totalSize) * 100));
	        			}
	        		});
	                
	                //MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
	                
	                for (int i = 0 ; i < nameValuePairs.size() ; i++){
	                    NameValuePair np = nameValuePairs.get(i) ;
	                    String name = np.getName() ;
	                    String value = np.getValue() ;
	                    multipartEntity.addPart(name, new StringBody(value)) ;
	                }
	                
	                if (file!=null)
	                {
	                    ContentBody cbFile = new FileBody(file, mimeType);
	                    multipartEntity.addPart(fileName, cbFile);
	                }
	                
	                httppost.setEntity(multipartEntity);
	                /* execute to get response */
	                HttpResponse res = mHttpClient.execute(httppost) ;
	                response = readResponse(res) ;
	               
	                Log.e("UPLOAD_RESPONSE", response) ;
	                
	            } catch(Exception e){
	            e.printStackTrace() ;
	            return false ;
	        }
	        return true ;
	    }
	
	
	
    
    
    
	
	public boolean makePostRestPhoto(final Task task,  Uri fileUri,  String userid,  String resid,  String caption,  int type){
		
		
	    //public boolean makePost(final Task task, String user, String title, String summary, Uri fileUri, String cat, int type, String isFB){
	       
	            String response;// = postFileMultipart(url, file, fileName, mimeType, nameValuePairs) ;            
	            try {
	            	
	            	String url = new String(FoodyResturent.PHOTO_INSERT_URL) ;
	            	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	                nameValuePairs.add(new BasicNameValuePair("userid", userid));
	                nameValuePairs.add(new BasicNameValuePair("resid", resid));
	                nameValuePairs.add(new BasicNameValuePair("caption", caption));
	               
	                
	                File file=null; String fileName=null;
	                if(type!=1)
	                {
	                	file = new File(fileUri.getPath()); 
	                	if(type==3)
	                		//file = new File(fileUri.getPath());
	                		file = new com.mcc.letseat.Functions().resizeImageFile(fileUri.getPath());
	                	else 
	                	{
	                		Log.e("Image......", "Image......");
	                		file = new com.mcc.letseat.Functions().resizeImageFile(fileUri.getPath());// File(fileUri.getPath()); 
	                	}
	                		 
	    	            if(type==2 || type==3) fileName="photoname";
	    	            else 
	                		fileName="photoname";
	                }
	                
	                String mimeType = "image/*" ;
	               
	                
	                /* prepare HttpClient parameter */
	                HttpParams params = new BasicHttpParams();
	                params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
	                /* prepare request */
	                DefaultHttpClient mHttpClient = new DefaultHttpClient(params);
	                HttpPost httppost = new HttpPost(url);
	                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs)) ;
	                
	                
	                final long totalSize;
	                
	                if(file!=null)
	                	totalSize=file.length()+userid.length()+resid.length()+caption.length();
	                else
	                	totalSize=userid.length()+resid.length()+caption.length();
	                              
	                /* add Multipart entity */
	                CustomMultiPartEntity multipartEntity = new CustomMultiPartEntity(new ProgressListener()
	        		{
	        			@Override
	        			public void transferred(long num)
	        			{
	        				task.updateProgress((int) ((num / (float) totalSize) * 100));
//	        				pd.setProgress((int) ((num / (float) totalSize) * 100));
//	        				System.out.println((int) ((num / (float) totalSize) * 100));
	        			}
	        		});
	                
	                //MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
	                
	                for (int i = 0 ; i < nameValuePairs.size() ; i++){
	                    NameValuePair np = nameValuePairs.get(i) ;
	                    String name = np.getName() ;
	                    String value = np.getValue() ;
	                    multipartEntity.addPart(name, new StringBody(value)) ;
	                }
	                
	                if (file!=null)
	                {
	                    ContentBody cbFile = new FileBody(file, mimeType);
	                    multipartEntity.addPart(fileName, cbFile);
	                }
	                
	                httppost.setEntity(multipartEntity);
	                /* execute to get response */
	                HttpResponse res = mHttpClient.execute(httppost) ;
	                response = readResponse(res) ;
	               
	                Log.e("UPLOAD_RESPONSE", response) ;
	            } catch(Exception e){
	            e.printStackTrace() ;
	            return false ;
	        }
	        return true ;
	    }
  
	
	
	public boolean makePostResPhoto(final Task task,  Uri fileUri,  String userid ,  String resid, String caption, int type){
		
		
	    //public boolean makePost(final Task task, String user, String title, String summary, Uri fileUri, String cat, int type, String isFB){
	       
	            String response;// = postFileMultipart(url, file, fileName, mimeType, nameValuePairs) ;            
	            try {
	            	
	            	String url = new String(FoodyResturent.PHOTO_INSERT_URL) ;
	            	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	                nameValuePairs.add(new BasicNameValuePair("userid", userid));
	                nameValuePairs.add(new BasicNameValuePair("resid", resid));
	                nameValuePairs.add(new BasicNameValuePair("caption", caption));
	               
	                
	                File file=null; String fileName=null;
	                if(type!=1)
	                {
	                	file = new File(fileUri.getPath()); 
	                	if(type==3)
	                		file = new File(fileUri.getPath());
	                	else 
	                	{
	                		Log.e("Image......", "Image......");
	                		file = new Functions().resizeImageFile(fileUri);// File(fileUri.getPath()); 
	                	}
	                		 
	    	            if(type==2 || type==3) fileName="photoname";
	    	            else 
	                		fileName="photoname";
	                }
	                
	                String mimeType = "image/*" ;
	               
	                
	                /* prepare HttpClient parameter */
	                HttpParams params = new BasicHttpParams();
	                params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
	                /* prepare request */
	                DefaultHttpClient mHttpClient = new DefaultHttpClient(params);
	                HttpPost httppost = new HttpPost(url);
	                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs)) ;
	                
	                
	                final long totalSize;
	                
	                if(file!=null)
	                	totalSize=file.length()+userid.length()+resid.length();
	                else
	                	totalSize=userid.length()+resid.length();
	                              
	                /* add Multipart entity */
	                CustomMultiPartEntity multipartEntity = new CustomMultiPartEntity(new ProgressListener()
	        		{
	        			@Override
	        			public void transferred(long num)
	        			{
	        				task.updateProgress((int) ((num / (float) totalSize) * 100));
//	        				pd.setProgress((int) ((num / (float) totalSize) * 100));
//	        				System.out.println((int) ((num / (float) totalSize) * 100));
	        			}
	        		});
	                
	                //MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
	                
	                for (int i = 0 ; i < nameValuePairs.size() ; i++){
	                    NameValuePair np = nameValuePairs.get(i) ;
	                    String name = np.getName() ;
	                    String value = np.getValue() ;
	                    multipartEntity.addPart(name, new StringBody(value)) ;
	                }
	                
	                if (file!=null)
	                {
	                    ContentBody cbFile = new FileBody(file, mimeType);
	                    multipartEntity.addPart(fileName, cbFile);
	                }
	                
	                httppost.setEntity(multipartEntity);
	                /* execute to get response */
	                HttpResponse res = mHttpClient.execute(httppost) ;
	                response = readResponse(res) ;
	               
	                Log.e("UPLOAD_RESPONSE", response) ;
	            } catch(Exception e){
	            e.printStackTrace() ;
	            return false ;
	        }
	        return true ;
	    }
	    
	   
	
    /*
     * Utility Method
     * Performing Request-Response GET format in order to complete a API
     * Input: Url
     * Output: Response String containing JSON or XML
     */
    public String getResponse(String url){
        String response = "" ;
        try{
            /* prepare request */
            HttpClient client = new DefaultHttpClient() ;
            HttpGet req = new HttpGet(url) ;
            /* execute to get response */
            HttpResponse res = client.execute(req) ;
            /* read response stream */
            response = readResponse(res) ;
        }catch(Exception e){
            Log.e(this.getClass().toString() + "Get Response", e.toString()) ;
            e.printStackTrace() ;
            return "" ;
        }
        return response ;
    }



        /*
         * Utility Method
         * Performing Request-Response POST format Multipart in order to complete an API
         * We don't need anything else in Multipart so we used this method for sending
         * Multipart Image Signature
         * Input: Url, fileName
         * Output: Response String containg JSON or XML
         *
         * Implementation Note: This method uses some external jars
         * Just for the multipart purpose
         */
    /*    private String postFileMultipart(String url, File file, String fileName, String type, ArrayList<NameValuePair>parameters) {
            String response = "" ;
            try {
                 prepare HttpClient parameter 
                HttpParams params = new BasicHttpParams();
                params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
                 prepare request 
                DefaultHttpClient mHttpClient = new DefaultHttpClient(params);
                HttpPost httppost = new HttpPost(url);
                httppost.setEntity(new UrlEncodedFormEntity(parameters)) ;
                
                
                final long totalSize=file.length();
                
                 add Multipart entity 
                CustomMultiPartEntity multipartEntity = new CustomMultiPartEntity(new ProgressListener()
        		{
        			@Override
        			public void transferred(long num)
        			{
        				//pd.setProgress((int) ((num / (float) totalSize) * 100));
        				System.out.println((int) ((num / (float) totalSize) * 100));
        			}
        		});
                
                //MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
                
                for (int i = 0 ; i < parameters.size() ; i++){
                    NameValuePair np = parameters.get(i) ;
                    String name = np.getName() ;
                    String value = np.getValue() ;
                    multipartEntity.addPart(name, new StringBody(value)) ;
                    Log.e("PARAMS", name + "---" + value) ;
                }
                if (file!=null)
                {
                    ContentBody cbFile = new FileBody(file, type);
                    multipartEntity.addPart(fileName, cbFile);
                }
                
                httppost.setEntity(multipartEntity);
                 execute to get response 
                HttpResponse res = mHttpClient.execute(httppost) ;
                response = readResponse(res) ;
                Log.e("UPLOAD_RESPONSE", response) ;
            } catch (Exception e) {
                Log.e("UPLOAD", e.getLocalizedMessage(), e);
                return "" ;
            }
            return response ;
        }*/
        
        
        

        /*
         * Utility Method
         * Read Response Stream from a Http Response and return it as string
         * Input: Response Object
         * Output:Response String  
         */
        private String readResponse(HttpResponse res){
            String response = "" ;
            try{
                BufferedReader rd = new BufferedReader(
                    new InputStreamReader(res.getEntity().getContent())) ;
                       
                String line = "" ;
                while ((line = rd.readLine()) != null){
                    Log.e("IN READ", line) ;
                    response += line ;
                }
            }catch (Exception e){
                e.printStackTrace() ;
                return "" ;
            }
            return response ;
        }

    
}