package com.mcc.user;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.user.User;
import com.foody.nearby.AlertDialogManager;
import com.mcc.letseat.HomeT;
import com.mcc.letseat.MyProgressDialog;
import com.mcc.letseat.R;
import com.mcc.libs.AnimationTween;

/**
 * Let's Eat SignUp
 * @author Arif
 *
 */
public class SignupActivity extends Activity implements OnClickListener {
	
	
	 String nev_type;
	
	//pref key
	public static final String LET_EAT_PREFERENCES = "foodybd_session" ;
	
	public static final String Photo = "photoKey"; 
	
	
	public static final String Password = "passwordKey"; 
	public static final String Username = "usernameKey"; 
	
	public static final String Name = "nameKey"; 
	public static final String Phone = "phoneKey"; 
	public static final String Email = "emailKey"; 
		
	public static final String Country = "CountryKey"; 
	public static final String City = "cityKey";
	public static final String Gender = "genderKey";
	public static final String Zip = "zipKey"; 
	public static final String About = "aboutKey"; 
	
	   

	SharedPreferences sharedpreferences;

	// Progress dialog
	MyProgressDialog pDialog;
	
	//EditText
	private EditText etEmail;
	private EditText etPass;
	
	private EditText etName;
	
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();
	
	String profilePhotoUrl="";
	
	private Button btnRegistration;

	private String photo;
	private String password;
	private String username;
	private String name;
	private String phone;
	private String email;
	private String gender;
	private String country;
	private String city;
	private String zip;
	private String about;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.slide_up_in,R.anim.stay);
		
		setContentView(R.layout.activity_signup);
		
		nev_type=getIntent().getStringExtra(StaticObjects.NEV_TYPE_KEY);
		
		//hide actionbar
		getActionBar().hide();
		
		 sharedpreferences = getSharedPreferences(LET_EAT_PREFERENCES, Context.MODE_PRIVATE);


		
		

		btnRegistration = (Button) findViewById(R.id.btnSignup);
		btnRegistration.setOnClickListener(this);

		etName = (EditText) findViewById(R.id.etsignup_Name);
		etEmail = (EditText) findViewById(R.id.etsignup_email);	
		etPass = (EditText) findViewById(R.id.etsignup_password);
		
		
	
		

	}
	
	@Override
	protected void onPause() {
	// TODO Auto-generated method stub
	super.onPause();
	overridePendingTransition(R.anim.stay,R.anim.slide_down_out);
	}

	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == btnRegistration) {

			btnRegistration.setBackgroundResource(R.drawable.sign_up_red_with_shadow_press);
			

			new Handler().postDelayed(new Runnable() {

				public void run() {

					btnRegistration.setBackgroundResource(R.drawable.sign_up_red_with_shadow);

					// Button Click Code Here
				}

			}, 100L);
			
			password = etPass.getText().toString();			
			photo = username+".jpg";
			name = etName.getText().toString();		
			email = etEmail.getText().toString();
		

			
			
			if(name.equals("") ){
				AnimationTween.shakeOnError(this, etName);
				return;
			}else if( email.equals("") || !ResetPasswordActivity.isValidEmail(email)){
				AnimationTween.shakeOnError(this, etEmail);
				return;
			}else if(password.equals("")){
				AnimationTween.shakeOnError(this, etPass);
				return;
			}
			
			username=email;
			
			photo="";
			phone="";
			gender="";
			country="";
			city="";
			zip="";
			about="";
			
			new DataUpdateTask().execute();
			//new PostTask(this, getResources(), "Uploading.." , photoUri, username, password, name, email, about, phone, gender, country, city, zip, StaticObjects.REG_TYPE_LE, 2).execute();
			
			
			
		}

	}
	
	
	
	
	
	
	

	
	

	
	class DataUpdateTask extends AsyncTask<String, String, String> {

		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			
		}

		
		protected String doInBackground(String... args) {
			// creating Places class object
			String response=null;
			FoodyResturent foodyResturent = new FoodyResturent();

			try {
				 
				response = foodyResturent.insertProfile(photo, username, password, name, email, phone, gender, country, city, zip, about, StaticObjects.REG_TYPE_LE);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return response;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String result) {
			
			 if (result.equals("failed"))
				Toast.makeText(SignupActivity.this, "Failed", Toast.LENGTH_LONG).show();
			else if (result.equals("success")){
				Toast.makeText(SignupActivity.this, "Success", Toast.LENGTH_LONG).show();
				new LoginCheck().execute();
			}
			else if (result.equals("SimiralExists")){
				//Toast.makeText(SignupActivity.this, "User Exists. Try Login", Toast.LENGTH_LONG).show();
				
			}
		}

	}
	
	ArrayList<User> users; 
	class LoginCheck extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			//Toast.makeText(SignupActivity.this,  "Checking...", Toast.LENGTH_SHORT).show();
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			FoodyResturent foodyResturent = new FoodyResturent();

			try {
				// String types = "LocId=" + LocId+"&Page="+currentPage;
				users = foodyResturent.loginCheck(username, password);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			//pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					Log.e("users", "==================="+users.size());
					
					if(users.size()>0){
						User tempUser= users.get(0);
						
						//save user data
						//saveSharedpreferences(tempUser);
						SavedPrefernce prefernce=new SavedPrefernce(SignupActivity.this);
						prefernce.saveSharedpreferences( tempUser);
						Toast.makeText(SignupActivity.this,  "Log in as "+tempUser.NAME, Toast.LENGTH_SHORT).show();
						
						
						
						if( nev_type.equals(StaticObjects.NEV_TYPE_ACTION) ){
							finish();
						}else{
							
							Intent i = new Intent(SignupActivity.this, HomeT.class);							
							startActivity(i);
							
						}
						
						
					}else {
						Toast.makeText(SignupActivity.this,  "Try again", Toast.LENGTH_SHORT).show();
						
						
					}

				}

			});

		}

	}

	
	

}
