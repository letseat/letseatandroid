package com.mcc.user;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

//import com.facebook.Session;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.foody.AppData.StaticObjects;
import com.foody.jsondata.user.User;
import com.mcc.letseat.R;

/**
 * Saved app data and session via SharedPreferences  
 * @author Arif
 *
 */

public class SavedPrefernce {

	public static final String Let_eat_PREFERENCES = "foodybd_session";

	public static final String ID = "idKey";

	public static final String Photo = "photoKey";
	public static final String Password = "passwordKey";
	public static final String Username = "usernameKey";

	public static final String Name = "nameKey";
	public static final String Phone = "phoneKey";
	public static final String Email = "emailKey";

	public static final String Country = "CountryKey";
	public static final String City = "cityKey";
	public static final String Gender = "genderKey";
	public static final String Zip = "zipKey";
	public static final String About = "aboutKey";		
	
	public static final String ZoneId = "ZoneIdkey";
	public static final String ZoneName = "ZoneNameKey";
	public static final String LocationId = "LocationIdkey";
	public static final String LocationName = "LocationNameKey";
	public static final String District = "DistrictKey";
	public static final String DistrictID = "DistrictIDKey";
	public static final String UserType = "UserTypeKey";
	
	public static final String FavRes = "FavReskey";
	public static final String FavFood = "FavFoodkey";
	public static final String FavCus = "FavCuskey";
	public static final String NoResVisit = "NoResVisitkey";
	public static final String VisitWith = "VisitWithkey";
	public static final String VisitTime = "";
	public static final String Btime = "Btimekey";
	public static final String Ltime = "Ltimekey";
	public static final String Dtime = "Dtimekey";
	public static final String Stime = "Stimekey";

	public static final String CityDataExpireKey = "CityDataExpireKey";
	public static final String CitiesKey = "CitiesKey";

	public static final String ZoneDataExpireKey = "ZoneDataExpireKey";
	public static final String ZoneKey = "ZoneKey";
	
	public static final String LocDataExpireKey = "LocDataExpireKey";
	public static final String LocsKey = "LocsKey";

	public static final String LocRestListDataExpireKey = "LocRestListDataExpireKey";
	public static final String LocRestListKey = "LocRestListKey";

	public static final String LocCuisineListDataExpireKey = "LocCuisineListDataExpireKey";
	public static final String LocCuisineListKey = "LocCuisineListKey";
	
	public static final String LocCuisineRestListDataExpireKey="lcrlde";
	private static final String LocCuisineRestListKey="lcrl";
	

	public static final String DealsListDataExpireKey="dldek";
	public static final String DealsListKey="dlk";
	
	public static final String INIT="init";
	
	public static final long DATA_EXPIRE_CONST_DAY=1;
	public static final long DATA_EXPIRE_CONST_HOUR=1;

	private static final long REMINDER_TIME_INTERVAL_MINUTE=10;
	
	
	private Activity activity;
	SharedPreferences sharedpreferences;
	
	public SavedPrefernce(Activity activityT) {
		this.activity=activityT;
		sharedpreferences = activity.getSharedPreferences(Let_eat_PREFERENCES, Context.MODE_PRIVATE);
	}
	
	

	public void saveSharedpreferences( User u) {

		/*SharedPreferences sharedpreferences = activity.getSharedPreferences(
				Let_eat_PREFERENCES, Context.MODE_PRIVATE);*/

		Editor editor = sharedpreferences.edit();
		editor.putString(ID, u.ID);
		editor.putString(Photo, u.IMG);
		editor.putString(Username, u.USERNAME);
		editor.putString(Password, u.PASSWORD);
		editor.putString(Name, u.NAME);
		editor.putString(Phone, u.PHONE);
		editor.putString(Email, u.EMAIL);
		editor.putString(Gender, u.SEX);
		editor.putString(Country, u.COUNTRY);
		editor.putString(City, u.CITY);
		editor.putString(Zip, u.ZIP);
		editor.putString(About, u.ABOUT);
		editor.putString(UserType, u.USERTYPE);

		editor.putString(FavRes, u.FavRes);
		editor.putString(FavCus, u.FavCus);
		editor.putString(FavFood, u.FavFood);
		editor.putString(NoResVisit, u.NoResVisit);
		editor.putString(VisitWith, u.VisitWith);
		editor.putString(VisitTime, u.VisitTime);
		editor.commit();
		
		//change user login stutus in app menu list
		//StaticObjects.changeLoginStatusInAppMenuData();
	}
	
	public User getUser(){
		
		/*SharedPreferences sharedpreferences = activity.getSharedPreferences(Let_eat_PREFERENCES, Context.MODE_PRIVATE);*/
		
		 User user=new User( 
				 sharedpreferences.getString(ID, "") , 
				 sharedpreferences.getString(Username, "") , 
				 sharedpreferences.getString(Password, "") , 
				 sharedpreferences.getString(Name, "") , 
				 sharedpreferences.getString(Gender, "") , 
				 sharedpreferences.getString(Photo, "") , 
				 sharedpreferences.getString(Email, "") , 
				 sharedpreferences.getString(Country, "") , 
				 sharedpreferences.getString(City, "") , 
				 sharedpreferences.getString(Zip, "") ,
				 "",
				 "",
				 sharedpreferences.getString(Phone, ""),
				 sharedpreferences.getString(About, ""),
				 sharedpreferences.getString(UserType, ""),
				 sharedpreferences.getString(FavRes, ""),
				 sharedpreferences.getString(FavFood, ""),
				 sharedpreferences.getString(FavCus, ""),
				 sharedpreferences.getString(NoResVisit, ""),
				 sharedpreferences.getString(VisitWith, ""),
				 sharedpreferences.getString(VisitTime, ""));
		 
		
		return user;
		
	}
	
	public void removeSharedpreferences() {

		/*SharedPreferences sharedpreferences = activity.getSharedPreferences(
				Let_eat_PREFERENCES, Context.MODE_PRIVATE);*/
		
		//fb logout
		callFacebookLogout(activity);

		Editor editor = sharedpreferences.edit();
		editor.putString(ID, "");
		editor.putString(Photo, "");
		editor.putString(Username, "");
		editor.putString(Password, "");
		editor.putString(Name, "");
		editor.putString(Phone, "");
		editor.putString(Email, "");
		editor.putString(Gender, "");
		editor.putString(Country, "");
		editor.putString(City, "");
		editor.putString(Zip, "");
		editor.putString(About, "");
		editor.commit();
		
		//change user login stutus in app menu list
		//StaticObjects.changeLoginStatusInAppMenuData();
				
		//after removed preference call registration 
		//StaticObjects.callRegForAction(activity);
	}
	
	public static void callFacebookLogout(Context context){
		FacebookSdk.sdkInitialize(context);
		LoginManager.getInstance().logOut();
	/*
	    Session session = Session.getActiveSession();
	    if (session != null) {

	        if (!session.isClosed()) {
	            session.closeAndClearTokenInformation();
	            //clear your preferences if saved
	        }
	    } else {

	        session = new Session(context);
	        Session.setActiveSession(session);

	        session.closeAndClearTokenInformation();
	            //clear your preferences if saved

	    }

	*/}

	public boolean isUserExists() {
		/*SharedPreferences sharedpreferences = activity.getSharedPreferences(Let_eat_PREFERENCES, Context.MODE_PRIVATE);*/

		if (sharedpreferences.getString(Username, "").isEmpty()) {
			return false;
		} else {
			return true;
		}

	}
	
	
	public void setShowInfo(boolean status){
		//SharedPreferences sharedpreferences = activity.getSharedPreferences(Let_eat_PREFERENCES, Context.MODE_PRIVATE);
		Editor editor = sharedpreferences.edit();
		editor.putBoolean(INIT, status);		
		editor.commit();
	}
	public boolean isShowInfoScreen() {
		/*SharedPreferences sharedpreferences = activity.getSharedPreferences(Let_eat_PREFERENCES, Context.MODE_PRIVATE);*/

		return sharedpreferences.getBoolean(INIT, false);
			

	}

	public String getUserID() {
		/*SharedPreferences sharedpreferences = activity.getSharedPreferences(Let_eat_PREFERENCES, Context.MODE_PRIVATE);*/
		
		if(sharedpreferences==null)
			return "";
		
		return sharedpreferences.getString(ID, "NotFound");
	}
	
	public String getUserEmail() {
		/*SharedPreferences sharedpreferences = activity.getSharedPreferences(Let_eat_PREFERENCES, Context.MODE_PRIVATE);*/
		
		if(sharedpreferences==null)
			return "";
		
		return sharedpreferences.getString(Email, "");
	}
	
	 AlertDialog helpDialog =null;


	
	public void callRegAlert(){
		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(activity);
		// helpBuilder.setTitle("Pop Up");
		helpBuilder.setMessage(activity.getString(R.string.msg_login));
		helpBuilder.setPositiveButton("Continue",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						
						
						StaticObjects.callRegForAction(activity);
							
						
						
						
					}
				});

		helpBuilder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(helpDialog!=null)
						helpDialog.cancel();
					}
				});

		// Remember, create doesn't show the dialog
		 helpDialog = helpBuilder.create();
		 
		 if(helpDialog!=null)
		helpDialog.show();
		
		
	}
	
	public String getLocationId(){
		//SharedPreferences sharedpreferences = activity.getSharedPreferences(Let_eat_PREFERENCES, Context.MODE_PRIVATE);
		return sharedpreferences.getString(LocationId, ""); 
		
	}
	
	public void setLocationId(String locationId){
		//SharedPreferences sharedpreferences = activity.getSharedPreferences(Let_eat_PREFERENCES, Context.MODE_PRIVATE);
		Editor editor = sharedpreferences.edit();
		editor.putString(LocationId, locationId);		
		editor.commit();
	}
	
	public String getLocationName(){
		//SharedPreferences sharedpreferences = activity.getSharedPreferences(Let_eat_PREFERENCES, Context.MODE_PRIVATE);
		return sharedpreferences.getString(LocationName, ""); 
		
	}
	
	public void setLocationName(String locationName){
		//SharedPreferences sharedpreferences = activity.getSharedPreferences(Let_eat_PREFERENCES, Context.MODE_PRIVATE);
		Editor editor = sharedpreferences.edit();
		editor.putString(LocationName, locationName);		
		editor.commit();
	}
	
	public String getDistrict(){
		//SharedPreferences sharedpreferences = activity.getSharedPreferences(Let_eat_PREFERENCES, Context.MODE_PRIVATE);
		return sharedpreferences.getString(District, ""); 
		
	}
	
	public void setDistrict(String district){
		//SharedPreferences sharedpreferences = activity.getSharedPreferences(Let_eat_PREFERENCES, Context.MODE_PRIVATE);
		Editor editor = sharedpreferences.edit();
		editor.putString(District, district);		
		editor.commit();
	}
	
	public String getDistrictID(){
		//SharedPreferences sharedpreferences = activity.getSharedPreferences(Let_eat_PREFERENCES, Context.MODE_PRIVATE);
		return sharedpreferences.getString(DistrictID, ""); 
		
	}
	
	public void setDistrictID(String districtID){
		//SharedPreferences sharedpreferences = activity.getSharedPreferences(Let_eat_PREFERENCES, Context.MODE_PRIVATE);
		Editor editor = sharedpreferences.edit();
		editor.putString(DistrictID, districtID);		
		editor.commit();
	}
	

	
	public void setZoneName(String zoneName){
		Editor editor = sharedpreferences.edit();
		editor.putString(ZoneName, zoneName);		
		editor.commit();
	}
	
		public String getZoneName(){
		return sharedpreferences.getString(ZoneName, ""); 
		
	}
	
	
	public void setZoneID(String zoneID){
		
		Editor editor = sharedpreferences.edit();
		editor.putString(ZoneId, zoneID);		
		editor.commit();
	}
	
	public String getZoneID(){
		return sharedpreferences.getString(ZoneId, ""); 
		
	}
	
	public boolean isCityExpire(/*Activity activity*/){
		//SharedPreferences sharedpreferences = activity.getSharedPreferences(Let_eat_PREFERENCES, Context.MODE_PRIVATE);
		
		return (( System.currentTimeMillis()-sharedpreferences.getLong(CityDataExpireKey, 0))/(24 * 60 * 60 * 1000) >= DATA_EXPIRE_CONST_DAY);
	}
	
	public void setCityDataExpirDate( /*Activity activity*/){
		//SharedPreferences sharedpreferences = activity.getSharedPreferences(Let_eat_PREFERENCES, Context.MODE_PRIVATE);
		Editor editor = sharedpreferences.edit();
		editor.putLong(CityDataExpireKey, System.currentTimeMillis());		
		editor.commit();
	}
	
	public void saveCities(String citiesJson){
		//SharedPreferences sharedpreferences = activity.getSharedPreferences(Let_eat_PREFERENCES, Context.MODE_PRIVATE);
		Editor editor = sharedpreferences.edit();
		editor.putString(CitiesKey, citiesJson);
		editor.commit();
	}
	
	public String getCities(){
		//SharedPreferences sharedpreferences = activity.getSharedPreferences(Let_eat_PREFERENCES, Context.MODE_PRIVATE);
		return sharedpreferences.getString(CitiesKey, ""); 
	}
	
	//
	public boolean isLocExpire(String DisID){
		
		
		return (( System.currentTimeMillis()-sharedpreferences.getLong(LocDataExpireKey+DisID, 0))/(24 * 60 * 60 * 1000) >= DATA_EXPIRE_CONST_DAY);
	}
	
	public void setLocDataExpirDate( String DisID){
		
		Editor editor = sharedpreferences.edit();
		editor.putLong(LocDataExpireKey+DisID, System.currentTimeMillis());		
		editor.commit();
	}
	
	public void saveLocs(String LocsJson, String DisID){
		
		Editor editor = sharedpreferences.edit();
		editor.putString(LocsKey+DisID, LocsJson);
		editor.commit();
	}
	
	public String getLocs(String DisID){
		
		return sharedpreferences.getString(LocsKey+DisID, ""); 
	}
	
	//zone
	public boolean isZoneExpire(String DisID){
		
		
		return (( System.currentTimeMillis()-sharedpreferences.getLong(ZoneDataExpireKey+DisID, 0))/(24 * 60 * 60 * 1000) >= DATA_EXPIRE_CONST_DAY);
	}
	
	public void setZoneDataExpirDate( String DisID){
		
		Editor editor = sharedpreferences.edit();
		editor.putLong(ZoneDataExpireKey+DisID, System.currentTimeMillis());		
		editor.commit();
	}
	
	public void saveZones(String zoneJson, String DisID){
		
		Editor editor = sharedpreferences.edit();
		editor.putString(ZoneKey+DisID, zoneJson);
		editor.commit();
	}
	
	public String getZones(String DisID){
		
		return sharedpreferences.getString(ZoneKey+DisID, ""); 
	}
	
	
	
	// list of restaurant in a location
	//
	public boolean isLocRestExpire(String LocId){
		
		
		return (( System.currentTimeMillis()-sharedpreferences.getLong(LocRestListDataExpireKey+LocId, 0))/( 60 * 60 * 1000) >= DATA_EXPIRE_CONST_HOUR);
	}
	
	public void setLocRestDataExpirDate( String LocId){
		
		Editor editor = sharedpreferences.edit();
		editor.putLong(LocRestListDataExpireKey+LocId, System.currentTimeMillis());		
		editor.commit();
	}
	
	public void saveLocRestList(String LocRestListJson, String LocId){
		
		Editor editor = sharedpreferences.edit();
		editor.putString(LocRestListKey+LocId, LocRestListJson);
		editor.commit();
	}
	
	public String getLocRestList(String LocId){
		
		return sharedpreferences.getString(LocRestListKey+LocId, ""); 
	}
	
	// list of cuisine  in a location
		
		public boolean isLocCuisineListExpire(String LocId){
			
			
			return (( System.currentTimeMillis()-sharedpreferences.getLong(LocCuisineListDataExpireKey+LocId, 0))/( 60 * 60 * 1000) >= DATA_EXPIRE_CONST_HOUR);
		}
		
		public void setLocCuisineListDataExpirDate( String LocId){
			
			Editor editor = sharedpreferences.edit();
			editor.putLong(LocCuisineListDataExpireKey+LocId, System.currentTimeMillis());		
			editor.commit();
		}
		
		public void saveLocCuisineList(String cuisineListJson, String LocId){
			
			Editor editor = sharedpreferences.edit();
			editor.putString(LocCuisineListKey+LocId, cuisineListJson);
			editor.commit();
		}
		
		public String getLocCuisineList(String LocId){
			
			return sharedpreferences.getString(LocCuisineListKey+LocId, ""); 
		}
		
	// list of restaurant cuisine  in a location
		
		public boolean isLocCuisineRestListExpire(String LocId, String cusId){
			
			
			return (( System.currentTimeMillis()-sharedpreferences.getLong(LocCuisineRestListDataExpireKey+LocId+cusId, 0))/( 60 * 60 * 1000) >= DATA_EXPIRE_CONST_HOUR);
		}
		
		public void setLocCuisineRestListDataExpirDate( String LocId, String cusId){
			
			Editor editor = sharedpreferences.edit();
			editor.putLong(LocCuisineRestListDataExpireKey+LocId+cusId, System.currentTimeMillis());		
			editor.commit();
		}
		
		public void saveLocCuisineRestList(String cuisineRestListJson, String LocId, String cusId){
			
			Editor editor = sharedpreferences.edit();
			editor.putString(LocCuisineRestListKey+LocId+cusId, cuisineRestListJson);
			editor.commit();
		}
		
		public String getLocCuisineRestList(String LocId,  String cusId){
			
			return sharedpreferences.getString(LocCuisineRestListKey+LocId+cusId, ""); 
		}		
		
		
		// list of deals  in a location
		
				public boolean isdealsListExpire(String LocId, String disFrom, int page){
					
					
					return (( System.currentTimeMillis()-sharedpreferences.getLong(DealsListDataExpireKey+LocId+disFrom+page, 0))/( 60 * 60 * 1000) >= DATA_EXPIRE_CONST_HOUR);
				}
				
				public void setDealsDataExpirDate(String LocId, String disFrom, int page){
					
					Editor editor = sharedpreferences.edit();
					editor.putLong(DealsListDataExpireKey+LocId+disFrom+page, System.currentTimeMillis());		
					editor.commit();
				}
				
				public void saveDealsList(String dealsListJson, String LocId, String disFrom, int page){
					
					Editor editor = sharedpreferences.edit();
					editor.putString(DealsListKey+LocId+disFrom+page, dealsListJson);
					editor.commit();
				}
				
				public String getDealsList( String LocId, String disFrom, int page){
					
					return sharedpreferences.getString(DealsListKey+LocId+disFrom+page, ""); 
				}


	public void saveEventAdID(String eventID){

		Editor editor = sharedpreferences.edit();
		editor.putString("event"+eventID, eventID);
		editor.commit();
	}

	public String getEventAdID(String eventID ){

		return sharedpreferences.getString("event"+eventID, "");
	}

	// Reminder value 0= no, 1 = yes;
	public void saveEventAdReminder(String eventID, boolean reminder){

		Editor editor = sharedpreferences.edit();
		editor.putBoolean("reminder" + eventID, reminder);
		editor.commit();

		setRemindtimeInterval(eventID);
	}

	public boolean getsaveEventAdReminder(String eventID ){


		if(sharedpreferences.getBoolean("reminder"+eventID, false)&& getRemindtimeInterval(eventID) ){
			return  true;
		}else
			return false;
	}


	public boolean getRemindtimeInterval(String eventID ){


		return (( System.currentTimeMillis()-sharedpreferences.getLong("rte"+eventID, 0))/(60 * 1000) >= REMINDER_TIME_INTERVAL_MINUTE);
	}

	public void setRemindtimeInterval(String eventID ){

		Editor editor = sharedpreferences.edit();
		editor.putLong("rte"+eventID, System.currentTimeMillis());
		editor.commit();
	}


}
