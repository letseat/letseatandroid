package com.mcc.user;

import java.util.ArrayList;

import namelessinc.asynctaskmanager.Task;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.Toast;

import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.user.User;
import com.mcc.user.profile.UserActivity;

/**
 * User Profile picture change task
 * @author Arif
 *
 */
public class ChangeProfilePicTask extends Task {
	/*String user = "";
	String cat = "";
	String title = "";
	String summary = "";
	String isFB = "";
	Uri fileUri = null;*/
	int type = 1;

	Uri photoname = null;
	String username;
	String password;
	Activity activity;
	
	String id;
	
	

	
	

	public ChangeProfilePicTask(Context ctx, Resources resources, String start,
			Uri photoname, String username, String id, String password, int type) {
		super(ctx, resources, start);
		this.photoname = photoname;
		this.username = username;
		this.id=id;
		this.password=password;
		this.activity=(Activity)ctx;
		this.type=type;
		
	}

	

	protected Boolean doInBackground(Void... arg0) {
		FoodyUserWebApi w = new FoodyUserWebApi();
		
		return w.changeProfilePic(this,  photoname,  username,  id,   type);

	}
	
	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if(result){
			new LoginCheck().execute();
		}
		
	}
	
	ArrayList<User> users; 
	class LoginCheck extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			FoodyResturent foodyResturent = new FoodyResturent();

			try {
				
				users = foodyResturent.loginCheck(username, password);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			//pDialog.dismiss();
			
			if (users!=null) {
				// updating UI from Background Thread
				activity.runOnUiThread(new Runnable() {
					public void run() {
					
						
						if(users.size()>0){
							User tempUser= users.get(0);
							
							//save user data
							//saveSharedpreferences(tempUser);
							SavedPrefernce prefernce=new SavedPrefernce(activity);
							prefernce.saveSharedpreferences( tempUser);
							//Log.e("after change pic", tempUser.IMG);
							//toast
							
							//change status to load new data when resume to UserActivity
							UserActivity.isUserProfilePicChanged=true;
							UserActivity.onProfilePicChanged();
							
							Toast.makeText(activity, "Profile picture changed", Toast.LENGTH_SHORT).show();
						}else {
							//Toast
							
						}

					}

				});
			}else{
				
				Toast.makeText(activity, "Can't fetch updated data", Toast.LENGTH_SHORT).show();
			}
			

		}

	}

}
