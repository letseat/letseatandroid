package com.mcc.user;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.District;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.user.User;
import com.foody.nearby.AlertDialogManager;
import com.markmao.pullscrollview.ui.widget.PullScrollView;
import com.mcc.letseat.MyProgressDialog;
import com.mcc.letseat.R;
import com.mcc.libs.AnimationTween;
import com.squareup.picasso.Picasso;

/**
 * Let's Eat SignUp
 * @author Arif
 *
 */
public class RegistrationActivity extends Activity implements
		OnItemSelectedListener, OnClickListener, PullScrollView.OnTurnListener {

	// Activity request codes for
	private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	static final int PIC_FROM_FILE_REQUEST_CODE = 4;

	public static final int MEDIA_TYPE_IMAGE = 1;

	// directory name to store captured images and videos
	private static final String IMAGE_DIRECTORY_NAME = "lets_eat";

	private Uri fileUri, photoUri; // file url to store image/video

	private ImageView imgPreview;

	// pref key
	public static final String LET_EAT_PREFERENCES = "foodybd_session";

	public static final String Photo = "photoKey";

	public static final String Password = "passwordKey";
	public static final String Username = "usernameKey";

	public static final String Name = "nameKey";
	public static final String Phone = "phoneKey";
	public static final String Email = "emailKey";

	public static final String Country = "CountryKey";
	public static final String City = "cityKey";
	public static final String Gender = "genderKey";
	public static final String Zip = "zipKey";
	public static final String About = "aboutKey";

	SharedPreferences sharedpreferences;

	// Progress dialog
	MyProgressDialog pDialog;

	// EditText
	private EditText etEmail;
	private EditText etPass;
	private EditText etPhone;
	private EditText etName;
	private EditText etUser_name;
	private EditText etZip;
	private EditText etAbout;

	private Spinner spinnerCity;
	private Spinner spinnerCountry;
	private Spinner spinnerGender;
	
	private ArrayAdapter<String> adapterCity; 

	private Button btnRegistration, btnNavChangPassword;
	ImageView btnUpdateCancel, btnUpdateConfirm, imageViewEditPic;
	LinearLayout layUpdateConfirm;

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	String profilePhotoUrl = "";

	private String photo;
	private String password;
	private String username;
	private String name;
	private String phone;
	private String email;
	private String gender;
	private String country;
	private String city;
	private String zip;
	private String about;
	
	// Places List
	ArrayList<District> districtList;
	

	
	ArrayList<String> offer_provider_list = new ArrayList<String>();


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.slide_up_in, R.anim.stay);

		setContentView(R.layout.act_pull_down);

		// hide actionbar
		getActionBar().hide();

		sharedpreferences = getSharedPreferences(LET_EAT_PREFERENCES,
				Context.MODE_PRIVATE);

		spinnerGender = (Spinner) findViewById(R.id.spinnerGender);
		// Create an ArrayAdapter using the string array and a default spinner
		// layout
		ArrayAdapter<CharSequence> adapterGender = new ArrayAdapter<CharSequence>(this, android.R.layout.simple_spinner_item){
			@Override		
			public View getView(int position, View convertView, ViewGroup parent) {

			        View v = super.getView(position, convertView, parent);
			        ((TextView) v).setTypeface(StaticObjects.gettfLight(RegistrationActivity.this));
			        
			        if (position == getCount()) {
			          //  ((TextView)v.findViewById(android.R.id.text1)).setText("");
			           // ((TextView)v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
			        }

			        return v;
			}
			
			@Override
		    public int getCount() {
		        return super.getCount()-1; // you dont display last item. It is used as hint.
		    }
			
			@Override
			public View getDropDownView(int position, View convertView, ViewGroup parent) {
				
				View v=super.getDropDownView(position, convertView, parent);
				((TextView) v).setTypeface(StaticObjects.gettfLight(RegistrationActivity.this));
				
				return v;
			}
		};
		
		
		// Specify the layout to use when the list of choices appears
		adapterGender.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		adapterGender.add("Male");
		adapterGender.add("Female");
		adapterGender.add("Gender");//hint
		// Apply the adapter to the spinner
		spinnerGender.setAdapter(adapterGender);
		//spinnerGender.setSelection(2);
		spinnerGender.setSelection(adapterGender.getCount());
		
		
		

		spinnerCountry = (Spinner) findViewById(R.id.spinnerCountry);
		// Create an ArrayAdapter using the string array and a default spinner
		// layout
		ArrayAdapter<CharSequence> adapterCountry = ArrayAdapter.createFromResource(this, R.array.country,android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		adapterCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinnerCountry.setAdapter(adapterCountry);

		
		
		
		
		spinnerCity = (Spinner) findViewById(R.id.spinnerCity);
		
		/*adapterCity = ArrayAdapter.createFromResource(this, R.array.city,android.R.layout.simple_spinner_item);
		adapterCity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerCity.setAdapter(adapterCity);*/
		//=====city
		new LoadPlaces().execute();
		//polulateCity();
		//====city end
		
		
		
		
		

		spinnerCity.setOnItemSelectedListener(this);
		spinnerCountry.setOnItemSelectedListener(this);
		spinnerGender.setOnItemSelectedListener(this);

		btnRegistration = (Button) findViewById(R.id.btnRegistraation);
		btnRegistration.setOnClickListener(this);

		btnUpdateCancel = (ImageView) findViewById(R.id.btnUpdateCancel);
		btnUpdateCancel.setOnClickListener(this);

		btnUpdateConfirm = (ImageView) findViewById(R.id.btnUpdateConfirm);
		btnUpdateConfirm.setOnClickListener(this);

		layUpdateConfirm = (LinearLayout) findViewById(R.id.layUpdateConfirm);
		layUpdateConfirm.setOnClickListener(this);

		imageViewEditPic = (ImageView) findViewById(R.id.imageViewEditPic);
		imageViewEditPic.setOnClickListener(this);

		btnNavChangPassword = (Button) findViewById(R.id.btnNavChangPassword);
		btnNavChangPassword.setOnClickListener(this);
		btnNavChangPassword.setVisibility(View.GONE);

		
		Typeface tf=StaticObjects.gettfNormal(this);
		
		etName = (EditText) findViewById(R.id.etName);
		etName.setTypeface(tf);
		
		etEmail = (EditText) findViewById(R.id.etEmail);
		etEmail.setTypeface(tf);
		
		etPass = (EditText) findViewById(R.id.etPass);

		etPhone = (EditText) findViewById(R.id.etPhone);
		etPhone.setTypeface(tf);
		
		etUser_name = (EditText) findViewById(R.id.etUser_name);
		etUser_name.setTypeface(tf);
		
		etZip = (EditText) findViewById(R.id.etZip);
		etZip.setTypeface(tf);
		
		etAbout = (EditText) findViewById(R.id.etAbout);
		etAbout.setTypeface(tf);
		etAbout.setText("");

		imgPreview = (ImageView) findViewById(R.id.imageUserPhoto);

		// alert select image
		//final String[] items = new String[] { "Camera", "Gallery" };
		//ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.select_dialog_item, items);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		final Item[] items2 = { new Item("Camera", R.mipmap.ic_menu_camera),
				new Item("Gallery", R.mipmap.ic_menu_gallery) };

		ListAdapter adapter2 = new ArrayAdapter<Item>(this,
				android.R.layout.select_dialog_item, android.R.id.text1, items2) {
			public View getView(int position, View convertView, ViewGroup parent) {
				// User super class to create the View
				View v = super.getView(position, convertView, parent);
				TextView tv = (TextView) v.findViewById(android.R.id.text1);
				tv.setTextColor(Color.parseColor("#939597"));

				// Put the image on the TextView
				tv.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						items2[position].icon, 0);

				// Add margin between image and text (support various screen
				// densities)
				int dp5 = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
				tv.setCompoundDrawablePadding(dp5);

				return v;
			}
		};

		builder.setTitle("Select Image");
		builder.setAdapter(adapter2, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) { // pick from
																	// camera
				if (item == 0) {
					// capture picture
					captureImage();

				} else {
					// pick from file
					imageFromFile();
				}
			}
		});

		final AlertDialog dialog = builder.create();
		imgPreview.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.show();
			}
		});

		// initiate pulldown scroll view
		initView();



		// animate view
		// AnimationTween.animateViewPosition(findViewById(R.id.registration_ui_container_layout),
		// RegistrationActivity.this);

		/*
		 * Picasso.with(this) .load(
		 * "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfa1/v/t1.0-1/c2.59.738.738/s200x200/10151361_874033465943876_8453763682910497335_n.jpg?oh=35c1fd28af88c8d30c646443d16c5698&oe=54F5076E&__gda__=1425042043_0701b9ca7b6f8c5400a5490839586c06"
		 * ) .placeholder(R.drawable.dummy_avatar)
		 * .error(R.drawable.dummy_avatar) .into(imgPreview);
		 */

	}
	
	

	private void polulateCity() {
		
		//new LoadPlaces().execute();

		 adapterCity = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, offer_provider_list) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {

				View v = super.getView(position, convertView, parent);
				((TextView) v).setTypeface(StaticObjects.gettfLight(RegistrationActivity.this));

				return v;
			}

			@Override
			public View getDropDownView(int position, View convertView,ViewGroup parent) {
				
				View v = super.getDropDownView(position, convertView, parent);
				((TextView) v).setTypeface(StaticObjects.gettfLight(RegistrationActivity.this));
				return v;
			}

		};
		// Specify the layout to use when the list of choices appears
		adapterCity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinnerCity.setAdapter(adapterCity);
		spinnerCity.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,int pos, long id) {
				

				/*if (!provider_filter.equals(offer_provider_list.get(pos))) {

					provider_filter = offer_provider_list.get(pos);

					// Toast.makeText(OfferActivity.this,
					// offer_provider_list.get(pos), Toast.LENGTH_SHORT).show();
					if (!loadFirstTime) {
						// load data with filter
						// Toast.makeText(OfferActivity.this, ""+loadFirstTime,
						// Toast.LENGTH_SHORT).show();
						resturantListItems = new ArrayList<HashMap<String, String>>();
						currentPage = 0;

						loadPlaces = new LoadPlaces();
						loadPlaces.execute();
					}
				}*/

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});
		
		
		//get saved user information
		getSharedpreferencesData();
		
	}

	// pulldown scroll view

	private PullScrollView mScrollView;
	private ImageView mHeadImg;
	

	protected void initView() {
		mScrollView = (PullScrollView) findViewById(R.id.scroll_view);
		mHeadImg = imgPreview;

	

		mScrollView.setHeader(mHeadImg);
		mScrollView.setOnTurnListener(this);
	}

	@Override
	public void onTurn() {

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		overridePendingTransition(R.anim.stay, R.anim.slide_down_out);
	}

	public void onItemSelected(AdapterView<?> parent, View view, int pos,
			long id) {
		// An item was selected. You can retrieve the selected item using
		// parent.getItemAtPosition(pos)

		/*
		 * Log.e("getItemAtPosition: ", "" + parent.getItemAtPosition(pos));
		 * Toast.makeText(this, "" + parent.getItemAtPosition(pos),
		 * Toast.LENGTH_LONG).show();
		 */
	}

	private void captureImage() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		// start the image capture Intent
		startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
	}

	private void imageFromFile() {
		Intent intent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

		intent.setType("image/*");
		// intent.setAction(Intent.ACTION_GET_CONTENT);

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
		photoUri = fileUri;
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		startActivityForResult(
				Intent.createChooser(intent, "Complete action using"),
				PIC_FROM_FILE_REQUEST_CODE);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// save file url in bundle as it will be null on scren orientation
		// changes
		outState.putParcelable("file_uri", fileUri);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		// get the file url
		fileUri = savedInstanceState.getParcelable("file_uri");
	}

	/**
	 * Receiving activity result method will be called after closing the camera
	 * */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// if the result is capturing Image
		if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				// successfully captured the image
				// display it in image view
				previewCapturedImage();
			} else if (resultCode == RESULT_CANCELED) {
				// user cancelled Image capture
				Toast.makeText(getApplicationContext(),
						"User cancelled image capture", Toast.LENGTH_SHORT)
						.show();
			} else {
				// failed to capture image
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed to capture image", Toast.LENGTH_SHORT)
						.show();
			}
		} else if (requestCode == PIC_FROM_FILE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				// successfully choose the image
				// display it in image view
				processPicFromFile(data);
			} else if (resultCode == RESULT_CANCELED) {
				// user cancelled Image capture
				Toast.makeText(getApplicationContext(),
						"Cancelled image Selection", Toast.LENGTH_SHORT).show();
			} else {
				// failed to capture image
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed to select image", Toast.LENGTH_SHORT)
						.show();
			}

		}
	}

	private void processPicFromFile(Intent data) {
		// TODO Auto-generated method stub
		try {
			fileUri = data.getData();
			Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), fileUri);
			

			//bitmap = Bitmap.createScaledBitmap(bitmap, dstWidth, dstHeight, false);

			//resize bitmap keep aspect ratio
			Matrix m = new Matrix();
			m.setRectToRect(new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight()), new RectF(0, 0, 800, 600), Matrix.ScaleToFit.CENTER);
			bitmap= Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
			
			// bimatp factory
			BitmapFactory.Options options = new BitmapFactory.Options();

			// downsizing image as it throws OutOfMemory Exception for larger
			// images
			options.inSampleSize = 8;

			// final Bitmap bitmap =
			// BitmapFactory.decodeFile(fileUri.getPath(),options);

			photoUri = fileUri;
			imgPreview.setImageBitmap(bitmap);

			photoUri = Uri.parse(getRealPathFromURI(photoUri));

			// uploading image
			uploadImage();
			// Toast.makeText(this, getRealPathFromURI(photoUri),
			// Toast.LENGTH_LONG).show();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String getRealPathFromURI(Uri contentURI) {
		String result;
		Cursor cursor = getContentResolver().query(contentURI, null, null,
				null, null);
		if (cursor == null) { // Source is Dropbox or other similar local file
								// path
			result = contentURI.getPath();
		} else {
			cursor.moveToFirst();
			int idx = cursor
					.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
			result = cursor.getString(idx);
			cursor.close();
		}
		return result;
	}
	
	public  Bitmap rotateBitmap(Bitmap source, float angle)
	{
	      Matrix matrix = new Matrix();
	      matrix.postRotate(angle);
	      return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
	}

	// upload capture or selected image
	void uploadImage() {
		// get user data from cache
		getUser();
		new ChangeProfilePicTask(this, getResources(), "Uploading..", photoUri,
				user.USERNAME, user.ID, user.PASSWORD, 2).execute();
		
	}

	/*
	 * Display image from a path to ImageView
	 */
	private void previewCapturedImage() {
		try {
			// hide video preview
			// videoPreview.setVisibility(View.GONE);

			imgPreview.setVisibility(View.VISIBLE);

			// bimatp factory
			BitmapFactory.Options options = new BitmapFactory.Options();

			// downsizing image as it throws OutOfMemory Exception for larger
			// images
			options.inSampleSize = 8;

			final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),options);
			
			photoUri = fileUri;
			
			//check orientation 
			ExifInterface ei=null;
			try {
				ei = new ExifInterface(getRealPathFromURI(photoUri));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Bitmap bitmapRotated=null;
			if(ei!=null){
				int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

				switch(orientation) {
				    case ExifInterface.ORIENTATION_ROTATE_90:
				    	bitmapRotated=rotateBitmap(bitmap, 90);
				        break;
				    case ExifInterface.ORIENTATION_ROTATE_180:
				    	bitmapRotated=rotateBitmap(bitmap, 180);
				        break;
				    // etc.
				}
			}
			
			if(bitmapRotated!=null){				
				imgPreview.setImageBitmap(bitmapRotated);
			}else{
				imgPreview.setImageBitmap(bitmap);
			}
			
			
			// uploading image
			uploadImage();

		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}
	
	
	public void saveImageToExternalStorage(Bitmap finalBitmap) {
	    String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
	    File myDir = new File(root + "/saved_images");
	    myDir.mkdirs();
	    Random generator = new Random();
	    int n = 10000;
	    n = generator.nextInt(n);
	    String fname = "Image-" + n + ".jpg";
	    File file = new File(myDir, fname);
	    if (file.exists())
	        file.delete();
	    try {
	        FileOutputStream out = new FileOutputStream(file);
	        finalBitmap.compress(Bitmap.CompressFormat.JPEG, 200, out);
	        out.flush();
	        out.close();
	    }
	    catch (Exception e) {
	        e.printStackTrace();
	    }


	    // Tell the media scanner about the new file so that it is
	    // immediately available to the user.
	    MediaScannerConnection.scanFile(this, new String[] { file.toString() }, null,
	            new MediaScannerConnection.OnScanCompletedListener() {
	                public void onScanCompleted(String path, Uri uri) {
	                    Log.e("ExternalStorage", "Scanned " + path + ":");
	                    Log.e("ExternalStorage", "-> uri=" + uri);
	                }
	    });

	}
	
	

	/*
	 * Creating file uri to store image/video
	 */
	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/*
	 * returning image / video
	 */
	private static File getOutputMediaFile(int type) {

		// External sdcard location
		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
						+ IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
				Locale.getDefault()).format(new Date());
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + timeStamp + ".jpg");
		} else {
			return null;
		}

		return mediaFile;
	}

	public void onNothingSelected(AdapterView<?> parent) {
		// Another interface callback
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == btnRegistration || v == btnUpdateConfirm
				|| v == layUpdateConfirm) {
			
			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(etName.getWindowToken(), 0);
            imm.hideSoftInputFromWindow(etEmail.getWindowToken(), 0);
            imm.hideSoftInputFromWindow(etAbout.getWindowToken(), 0);
            imm.hideSoftInputFromWindow(etPhone.getWindowToken(), 0);
            imm.hideSoftInputFromWindow(etZip.getWindowToken(), 0);

			btnRegistration.setBackgroundResource(R.drawable.border_round_gry);

			new Handler().postDelayed(new Runnable() {

				public void run() {

					btnRegistration.setBackgroundResource(R.drawable.border_round_green);

					// Button Click Code Here
				}

			}, 100L);

			password = etPass.getText().toString();
			username = etUser_name.getText().toString();
			photo = username + ".jpg";
			name = etName.getText().toString();
			phone = etPhone.getText().toString();
			email = etEmail.getText().toString();
			about = etAbout.getText().toString();

			gender = spinnerGender.getSelectedItemPosition() + "";
			country = spinnerCountry.getSelectedItemPosition() + "";
			city = spinnerCity.getSelectedItemPosition() + "";

			zip = etZip.getText().toString();

			if (name.equals("")) {
				AnimationTween.shakeOnError(this, etName);
				return;
			} else if (email.equals("")) {
				AnimationTween.shakeOnError(this, etEmail);
				return;
			}/*
			 * else if(photoUri==null){ AnimationTween.shakeOnError(this,
			 * imgPreview); return; }
			 */
			
			getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

			//username = email;

			Toast.makeText(RegistrationActivity.this, "wait. updating profile",
					Toast.LENGTH_SHORT).show();

			getUser();
			user.NAME = etName.getText().toString().trim();
			user.EMAIL = etEmail.getText().toString().trim();
			user.ABOUT = etAbout.getText().toString().trim();
			user.PHONE = etPhone.getText().toString().trim();
			user.SEX = spinnerGender.getSelectedItemPosition() + "";
			
			
			user.CITY = districtList.get(spinnerCity.getSelectedItemPosition()).DisId;//spinnerCity.getSelectedItemPosition() + "";
			
			user.ZIP = etZip.getText().toString().trim();

			new EditProfile().execute();

		} else if (v == btnUpdateCancel) {
			finish();
		} else if (v == imgPreview || v == imageViewEditPic) {
			callCahngePicDailog();
		} else if (v == btnNavChangPassword) {

			btnNavChangPassword
					.setBackgroundResource(R.drawable.border_round_red_button_press);

			new Handler().postDelayed(new Runnable() {

				public void run() {

					btnRegistration
							.setBackgroundResource(R.drawable.border_round_red_button);

					// Button Click Code Here
				}

			}, 100L);

			callChangePasswordDailog();
		}

	}

	Dialog changePasswordDialog;
	EditText etOldPass;
	EditText etNewPass;
	EditText etConfirmPass ;
	LinearLayout layChangPassword ;

	private void callChangePasswordDailog() {

		changePasswordDialog = new Dialog(this, R.style.FullHeightDialog);

		changePasswordDialog.setContentView(R.layout.dailog_change_pass);
		changePasswordDialog.getWindow().setLayout(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT);

		
		etOldPass = (EditText) changePasswordDialog.findViewById(R.id.etOldPass);
		etNewPass = (EditText) changePasswordDialog.findViewById(R.id.etNewPass);
		etConfirmPass = (EditText) changePasswordDialog.findViewById(R.id.etConfirmPass);		
		
		etOldPass.addTextChangedListener(mTextEditorWatcher);
		etNewPass.addTextChangedListener(mTextEditorWatcher);
		etConfirmPass.addTextChangedListener(mTextEditorWatcher);
		
		 layChangPassword = (LinearLayout) changePasswordDialog.findViewById(R.id.layChangPassword);
		final ImageView imgchangePassCancel = (ImageView) changePasswordDialog.findViewById(R.id.imgchangePassCancel);
		
		View.OnClickListener changePassListener=new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				

				String oldPass = etOldPass.getText().toString().trim();
				String newPass = etNewPass.getText().toString().trim();
				String confirmPass = etConfirmPass.getText().toString().trim();

				Log.e("oldPass n ",
						">" + oldPass + "<>"
								+ sharedpreferences.getString(Password, ""));
				if (oldPass.isEmpty()
						|| !oldPass.equals(sharedpreferences.getString(
								Password, ""))) {
					AnimationTween.shakeOnError(RegistrationActivity.this,
							etOldPass);
					return;
				}

				if (newPass.isEmpty()) {
					AnimationTween.shakeOnError(RegistrationActivity.this,
							etNewPass);
					return;
				}
				if (confirmPass.isEmpty() || !confirmPass.equals(newPass)) {
					AnimationTween.shakeOnError(RegistrationActivity.this,
							etConfirmPass);
					return;
				}

				// change password
				callChangePassword(newPass);
			}

		};
		

				
		layChangPassword.setOnClickListener(changePassListener);
		
		imgchangePassCancel.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				changePasswordDialog.cancel();
			}
		});

		changePasswordDialog.setCancelable(true);
		changePasswordDialog.show();
		
		

	}
	
	//text change listener
		private final TextWatcher mTextEditorWatcher = new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub			
				showHideConfirmButton();
			}
		};
		
		void showHideConfirmButton(){
			
			if( etOldPass.getText().toString().isEmpty() && etNewPass.getText().toString().isEmpty() && etConfirmPass.getText().toString().isEmpty() ){
				
				layChangPassword.setVisibility(View.INVISIBLE);
						
			}else{
				layChangPassword.setVisibility(View.VISIBLE);
			}
			
		}

	private void callChangePassword(String newPassword) {
		// TODO Auto-generated method stub
		getUser().PASSWORD = newPassword;
		Toast.makeText(RegistrationActivity.this, "wait. changing password",
				Toast.LENGTH_LONG).show();

		new ChangePassword().execute();

	}

	private User getUser() {
		SavedPrefernce prefernce = new SavedPrefernce(RegistrationActivity.this);
		user = prefernce.getUser();
		return user;
	}

	private void saveUser(User curentUser) {
		SavedPrefernce prefernce = new SavedPrefernce(RegistrationActivity.this);
		prefernce.saveSharedpreferences( curentUser);
	}

	public void getSharedpreferencesData() {
		if (sharedpreferences.contains(Username)) {
			etUser_name.setText(sharedpreferences.getString(Username, ""));

			btnRegistration.setText("Update");
		} else {
			showHideField(View.GONE);
		}

		if (sharedpreferences.contains(Password)) {
			etPass.setText(sharedpreferences.getString(Password, ""));
			password = sharedpreferences.getString(Password, "");
			Log.e("Password", password);
		}

		if (sharedpreferences.contains(Name)) {
			etName.setText(sharedpreferences.getString(Name, ""));
		}

		if (sharedpreferences.contains(Gender)) {
			String g = sharedpreferences.getString(Gender, "");
			if (g.equals("M") || g.equals("0") ) {
				spinnerGender.setSelection(0);
			} else if (g.equals("F") || g.equals("1") ) {
				spinnerGender.setSelection(1);
			}else{
				spinnerGender.setSelection(2);
			}

		}

		if (sharedpreferences.contains(Email)) {
			etEmail.setText(sharedpreferences.getString(Email, ""));
		}

		if (sharedpreferences.contains(Phone)) {
			etPhone.setText(sharedpreferences.getString(Phone, ""));
		}

		if (sharedpreferences.contains(About)) {
			String ab=sharedpreferences.getString(About, "");
			if(ab==null || ab.equalsIgnoreCase("null")){
				etAbout.setText("");
			}else{
				etAbout.setText(ab);
			}
			
		}

		if (sharedpreferences.contains(Country)) {
			spinnerCountry.setSelection(0);
		}

		if (sharedpreferences.contains(City)) {
			
			try {
				
				 //spinnerCity.setSelection( Integer.parseInt(sharedpreferences.getString(City, "")));
				 
				 String savedDisID=sharedpreferences.getString(City, "");
				 
				 for (District d : districtList) {
					
					 if(d.DisId.equals(savedDisID)){
						 
						 spinnerCity.setSelection( districtList.indexOf(d));
						 break;
					 }
				}
				
				
				 
				 Log.e("City", sharedpreferences.getString(City, "") );
				
			} catch (Exception e) {
				// TODO: handle exception
				spinnerCity.setSelection(0);
			}
			
			
			
			
		}

		if (sharedpreferences.contains(Zip)) {
			etZip.setText(sharedpreferences.getString(Zip, ""));
		}

		if (sharedpreferences.contains(Photo)) {
			profilePhotoUrl = sharedpreferences.getString(Photo, "").trim();

			if (profilePhotoUrl != null && !profilePhotoUrl.isEmpty()) {
				Picasso.with(this).load(profilePhotoUrl)						
						.placeholder(R.mipmap.dummy_avatar_big)
						.error(R.mipmap.dummy_avatar_big).into(imgPreview);
				Log.e("profilePhotoUrl", profilePhotoUrl );
			}

		}
		
		if(getUser().USERTYPE.equals(StaticObjects.REG_TYPE_LE) ){
			btnNavChangPassword.setVisibility(View.VISIBLE);
		}

	}  

	void showHideField(int visibility) {
		// etUser_name.

		etPhone.setVisibility(visibility);
		etUser_name.setVisibility(visibility);
		etZip.setVisibility(visibility);
		etAbout.setVisibility(visibility);

		spinnerCity.setVisibility(visibility);
		spinnerCountry.setVisibility(visibility);
		spinnerGender.setVisibility(visibility);

	}

	public static class Item {
		public final String text;
		public final int icon;

		public Item(String text, Integer icon) {
			this.text = text;
			this.icon = icon;
		}

		@Override
		public String toString() {
			return text;
		}
	}

	Dialog changePicDialog;

	void callCahngePicDailog() {
		changePicDialog = new Dialog(this, R.style.FullHeightDialog);

		changePicDialog.setContentView(R.layout.dailog_change_photo);
		changePicDialog.getWindow().setLayout(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT);

		changePicDialog.setCancelable(true);

		ImageView dcp_imageView = (ImageView) changePicDialog
				.findViewById(R.id.dcp_imageView);
		Bitmap bitmap = ((BitmapDrawable) imgPreview.getDrawable()).getBitmap();
		dcp_imageView.setImageBitmap(bitmap);

		LinearLayout layout_from_camera = (LinearLayout) changePicDialog
				.findViewById(R.id.layout_from_camera);
		layout_from_camera.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				changePicDialog.cancel();
				// capture picture
				captureImage();
			}
		});

		LinearLayout layout_from_gallery = (LinearLayout) changePicDialog
				.findViewById(R.id.layout_from_gallery);
		layout_from_gallery.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				changePicDialog.cancel();
				// pick from file
				imageFromFile();
			}
		});

		LinearLayout layout_remove_picture = (LinearLayout) changePicDialog
				.findViewById(R.id.layout_remove_picture);
		layout_remove_picture.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				changePicDialog.cancel();
				
				getUser();
				
				new DeletePhoto().execute();

			}
		});
		// dialog show
		changePicDialog.show();
	}

	String response;
	User user;

	class EditProfile extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// AnimationTween.zoomInOut(imageFav, getActivity());

		}

		protected String doInBackground(String... args) {

			FoodyResturent foodyResturent = new FoodyResturent();

			try {

				response = foodyResturent.editProfile(user);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {

				public void run() {

					if (response.equals("failed")) {
						Toast.makeText(RegistrationActivity.this, "Try again",
								Toast.LENGTH_SHORT).show();

					} else if (response.equals("success")) {
						Toast.makeText(RegistrationActivity.this,"Your profile updated", Toast.LENGTH_SHORT).show();
						saveUser(user);
						
						finish();

					} else if (response.equals("NotFound")) {
						Toast.makeText(RegistrationActivity.this,"It's seem you deleted saved data",Toast.LENGTH_SHORT).show();

					} else {
						Toast.makeText(RegistrationActivity.this, "Try again",
								Toast.LENGTH_SHORT).show();

					}
				}

			});

		}

	}

	class ChangePassword extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// AnimationTween.zoomInOut(imageFav, getActivity());

			Toast.makeText(RegistrationActivity.this,
					"window will close after update", Toast.LENGTH_SHORT)
					.show();
			changePasswordDialog.setCancelable(false);

		}

		protected String doInBackground(String... args) {

			FoodyResturent foodyResturent = new FoodyResturent();

			try {

				response = foodyResturent.changePassword(user);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {

				public void run() {

					if (response.equals("failed")) {
						Toast.makeText(RegistrationActivity.this, "Try again",
								Toast.LENGTH_SHORT).show();

						changePasswordDialog.setCancelable(true);
						changePasswordDialog.cancel();

					} else if (response.equals("success")) {

						changePasswordDialog.setCancelable(true);
						changePasswordDialog.cancel();
						Toast.makeText(RegistrationActivity.this,
								"Password updated", Toast.LENGTH_SHORT).show();
						saveUser(user);

					} else if (response.equals("NotFound")) {
						Toast.makeText(RegistrationActivity.this,
								"It's seem you deleted saved data",
								Toast.LENGTH_SHORT).show();

					} else {
						Toast.makeText(RegistrationActivity.this, "Try again",
								Toast.LENGTH_SHORT).show();

						changePasswordDialog.setCancelable(true);
						changePasswordDialog.cancel();

					}
				}

			});

		}

	}
	
	
	class DeletePhoto extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// AnimationTween.zoomInOut(imageFav, getActivity());

		}

		protected String doInBackground(String... args) {

			FoodyResturent foodyResturent = new FoodyResturent();

			try {

				response = foodyResturent.deletePhoto(user.USERNAME, FoodyResturent.DELETE_TYPE_PROFILE);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {

				public void run() {

					if (response.equals("failed")) {
						Toast.makeText(RegistrationActivity.this, "Try again",Toast.LENGTH_SHORT).show();

					} else if (response.equals("success")) {
						Toast.makeText(RegistrationActivity.this, "Your profile updated", Toast.LENGTH_SHORT).show();
						imgPreview.setImageResource(R.mipmap.dummy_avatar);
						user.IMG="";
						saveUser(user);
						imgPreview.setImageResource(R.mipmap.dummy_avatar);

					} else {
						Toast.makeText(RegistrationActivity.this, "Try again",
								Toast.LENGTH_SHORT).show();

					}
				}

			});

		}

	}
	
	
	
	class LoadPlaces extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			FoodyResturent foodyResturent = new FoodyResturent();

			try {
				districtList = foodyResturent.getDistricts(RegistrationActivity.this);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			
			
			/*AnimationTween.animateView(currentView.findViewById(R.id.choose_city_custom_progress), _activity);
			AnimationTween.animateViewPosition(lv, _activity);*/
			
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status

					if (districtList == null || districtList.size() <= 0) {

						

					} else if (districtList != null) {
						
						offer_provider_list.add("Others");
						
						// loop through each place
						for (District p : districtList) {

							//if(p.Status.equals("1")){
								
								//if(offer_provider_list.contains(p.DisName))
									offer_provider_list.add(p.DisName);
								
								
							//}
							
						}
						
						
						//populate spinner
						polulateCity();
						
					
						
					}
				}

			});

		}

	}
	
	

}
