package com.mcc.pager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.foody.AppData.StaticObjects;
import com.foody.jsondata.Restaurent;
import com.mcc.letseat.HomeT;
import com.mcc.letseat.R;
import com.mcc.letseat.tab.TabMainActivity;
import com.mcc.libs.CF;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class ImageSlideAdapter extends PagerAdapter {
	
	Activity activity;
	ArrayList<HashMap<String, String>> restaurentList;
	String listTags[];
	
	
	
	

	public ImageSlideAdapter(Activity activity, ArrayList<HashMap<String, String>> resturantListItems, String listTags[]) {
		this.activity = activity;		
		this.restaurentList = resturantListItems;
		this.listTags=listTags;
		
	}

	@Override
	public int getCount() {
		return restaurentList.size();
	}
	
	@Override
	public int getItemPosition(Object object) {
		
		return super.getItemPosition(object);
	}

	@Override
	public View instantiateItem(ViewGroup container, final int position) {
		LayoutInflater inflater = (LayoutInflater) activity
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.res_item, container, false);
		
		String RestName=restaurentList.get(position).get("RestName");
		

		ImageView mImageView = (ImageView) view.findViewById(R.id.imageView_resItem);
		LinearLayout llCloseIndicator=(LinearLayout) view.findViewById(R.id.closeIndicator);
		LinearLayout llOpenIndicator=(LinearLayout) view.findViewById(R.id.openIndicator);
		
		Typeface tf = Typeface.createFromAsset(activity.getAssets(), StaticObjects.fontPath_SEGOEUI_light);
		
		TextView textViewResName = (TextView) view.findViewById(R.id.textViewResName);
		textViewResName.setText(RestName);
		textViewResName.setTypeface(tf);
		
		TextView txtOpenClose = (TextView) view.findViewById(R.id.txtOpenClose);
		
		final View ll_transparent =view.findViewById(R.id.ll_transparent);
		
		
		String openClose="Open";
		
		try {
			if ((restaurentList.get(position).get("Open").equals(restaurentList.get(position).get("Close")))  || CF.isTimeBetweenTwoTime( CF.timePatternmaker(restaurentList.get(position).get("Open")), CF.timePatternmaker(restaurentList.get(position).get("Close") ) )){
				openClose="Open";
				llOpenIndicator.setVisibility(View.VISIBLE);
				
				
			}else{
				openClose="Closed";
				llCloseIndicator.setVisibility(View.VISIBLE);
				
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		txtOpenClose.setText(openClose);
		txtOpenClose.setTypeface(tf);
		
		
		TextView txtResItemRating = (TextView) view.findViewById(R.id.txtResItemRating);		
		String rating=""+ (Math.round(Float.parseFloat(restaurentList.get(position).get("Rating")) / StaticObjects.RATING_OFFSET));
		RelativeLayout rl_rating = (RelativeLayout) view.findViewById(R.id.rl_rating);
		rating= rating.equals("0")?"":rating;
		txtResItemRating.setText(rating);
		if(rating.equals(""))
			rl_rating.setVisibility(View.INVISIBLE);
		//txtResItemRating.setTypeface(tf);
		
		//loading image
		if(restaurentList.get(position).get("IMG")!=null && !restaurentList.get(position).get("IMG").isEmpty()){
			Picasso.with(activity)
		    .load(restaurentList.get(position).get("IMG"))
		    .placeholder(R.drawable.sample_res_photo)
		    .error(R.drawable.sample_res_photo)
		    .into(mImageView);
		}
		
		ImageView imageViewWifi=(ImageView)view.findViewById(R.id.imageViewWifi);//LiveMusic
		if( restaurentList.get(position).get("Wifi").equalsIgnoreCase("No") ){
			imageViewWifi.setVisibility(View.GONE);
		}
		
		ImageView imageViewLiveMusic=(ImageView)view.findViewById(R.id.imageViewLiveMusic);
		if( restaurentList.get(position).get("LiveMusic").equalsIgnoreCase("No") ){
			imageViewLiveMusic.setVisibility(View.GONE);
		}
		
		view.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(! ((HomeT)activity).checkNetConnection()){
					((HomeT)activity).showNetworkMsg();
					return;
				}
				
				Intent intent=new Intent(activity, TabMainActivity.class);
				intent.putExtra("Id", restaurentList.get(position).get("Id"));
				activity.startActivity(intent);
				
		        //Toast.makeText(activity, timePatternmaker(restaurentList.get(position).get("Open"))+" "+timePatternmaker(restaurentList.get(position).get("Close") ), Toast.LENGTH_LONG).show();  
				
				
				
			}
		});
		
		view.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent e) {
				
				if (e.getAction() == MotionEvent.ACTION_UP ||e.getAction() == MotionEvent.ACTION_CANCEL || e.getAction() == MotionEvent.ACTION_OUTSIDE) {
					
					v.findViewById(R.id.ll_transparent).setVisibility(View.VISIBLE);
					
				} else if (e.getAction() == MotionEvent.ACTION_DOWN) {
					v.findViewById(R.id.ll_transparent).setVisibility(View.INVISIBLE);
				}
				
				
				return false;
			}
		});
		
		container.addView(view);
		return view;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}
	
	
	
	
	

	
}