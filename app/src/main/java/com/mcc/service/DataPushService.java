package com.mcc.service;

import java.util.ArrayList;

import com.foody.jsondata.SearchResturent;
import com.mcc.letseat.search.DBOperation;
import com.mcc.letseat.search.SearchSuggestionDBHelper;

import android.app.IntentService;
import android.content.Intent;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.widget.Toast;

public class DataPushService extends IntentService{

	public DataPushService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	
	public DataPushService(){
		super(DataPushService.class.getName());
	}

	int i=0;
	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		
		ArrayList<SearchResturent> searchRestList= ((ArrayList<SearchResturent>)intent.getSerializableExtra("ResData"));
		addDataToDB(searchRestList);
		
		//Toast.makeText(this, "Data added"+(i++)+"<>"+System.currentTimeMillis(), Toast.LENGTH_LONG).show();
	}
	
	
	private void addDataToDB(ArrayList<SearchResturent> resturentList) {
		try {
			SearchSuggestionDBHelper dbHelper = new SearchSuggestionDBHelper(this);
			SQLiteDatabase mDatabase = dbHelper.getWritableDatabase();
			DBOperation dbOperation = new DBOperation(mDatabase);
			dbOperation.addRestaurantToDB(resturentList);
			
		} catch (SQLiteCantOpenDatabaseException e) {
			// TODO: handle exception
		} catch (Exception e) {
		// TODO: handle exception
		}

	
	}

}
