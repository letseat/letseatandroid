package com.mcc.le.gcm;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.foody.AppData.StaticObjects;
import com.foody.nearby.FeaturedRestaurantListView;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.mcc.letseat.Main;
import com.mcc.letseat.R;
import com.mcc.letseat.Splash;
import com.mcc.letseat.imageslider.FullScreenViewActivity;
import com.mcc.letseat.imageslider.adapter.GridViewImageAdapterNew;
import com.mcc.letseat.tab.TabMainActivity;
import com.mcc.user.profile.UserActivity;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Target;

/**
 * Define GCM Message landing page 
 * @author Arif
 *
 */

public class GcmIntentService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
	private String TAG="GcmIntentService";
	
	private final String N_STYLE_TEXT="0";
	private final String N_STYLE_PIC="1";
	
	private String n_style="0";
	
	
	String mes;
	 private Handler handler;

    public GcmIntentService() {
        super("GcmIntentService");
    }
    
    @Override
    public void onCreate() {
    	// TODO Auto-generated method stub
    	super.onCreate();
    	 handler = new Handler();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM
             * will be extended in the future with new message types, just ignore
             * any message types you're not interested in, or that you don't
             * recognize.
             */
            if (GoogleCloudMessaging.
                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("Deleted messages on server: " + extras.toString());
            // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                // This loop represents the service doing some work.
                for (int i=0; i<5; i++) {
                    Log.i(TAG, "Working... " + (i+1)
                            + "/5 @ " + SystemClock.elapsedRealtime());
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                    }
                }
                Log.i(TAG, "Completed work @ " + SystemClock.elapsedRealtime());
                // Post notification of received message.
                String title=extras.getString("title");               
                String message=extras.getString("message");
                String type=extras.getString("type");
                String param1=extras.getString("param1");
                String param2=extras.getString("param2");
                n_style=extras.getString("style");
                
                
                sendNotification(title, message, type, param1, param2, n_style);
               // sendNotification(title,  message, type, param1, 1);
                
                Log.i(TAG, "Received: " + extras.toString());
            }
        }
        
        mes = extras.getString("title");
        //showToast();
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }
    
    public void showToast(){
        handler.post(new Runnable() {
            public void run() {
                Toast.makeText(getApplicationContext(),mes , Toast.LENGTH_LONG).show();
            }
         });

    }

    
    
    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
  
    PendingIntent contentIntent ;
    
    //picasso container for image loader
    Target target;
    
    private void sendNotification(final String title,  final String message,  final String type, String param1, final String param2, String Style) {
        
    	mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

         contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, Splash.class), 0);
        
        if(type!=null && type.equals(StaticObjects.GCM_TYPE_GENERIC)){//Generic Notification > Open App 
        	
        	 contentIntent = PendingIntent.getActivity(this, 0,
                     new Intent(this, Splash.class), 0);
        	 
        }else if(type!=null && type.equals(StaticObjects.GCM_TYPE_REST)){//update info/new menu/food photo > Go to particular restaurant
        	Intent intent=new Intent(this, TabMainActivity.class);
        	intent.putExtra("Id", param1); 
        	 contentIntent = PendingIntent.getActivity(this, 0,intent, 0);
        	 
        }else if(type!=null && type.equals(StaticObjects.GCM_TYPE_OFFER)){//Discount Offer > Go to particular restaurant 
        	Intent intent=new Intent(this, TabMainActivity.class);
        	intent.putExtra("Id", param1); 
        	 contentIntent = PendingIntent.getActivity(this, 0,intent, 0);
        	 
        }else if(type!=null && type.equals(StaticObjects.GCM_TYPE_EVENT)){//Event Notification > list of restaurant
        	Intent intent = new Intent(this, FeaturedRestaurantListView.class);
        	intent.putExtra("Id", param1);
        	intent.putExtra("param2", param2);
        	
        	contentIntent = PendingIntent.getActivity(this, 0,intent, 0);
        	 
        }else if(type!=null && type.equals(StaticObjects.GCM_TYPE_FOLLOW)){//User Follow > Go to profile page
        	Intent intent = new Intent(this, UserActivity.class);
        	intent.putExtra("userid",param1);							
        	contentIntent = PendingIntent.getActivity(this, 0,intent, 0);
        	 
        }else if(type!=null && type.equals(StaticObjects.GCM_TYPE_PHOTO_PUBLISHED)){//Photo Publish > Go to user's uploaded photo page
        	
        	
        	 ArrayList<HashMap<String, String>> _filePaths = new ArrayList<HashMap<String, String>>();
        	 HashMap<String, String> map = new HashMap<String, String>();
			map.put("IMG",param1);	
			map.put("CAPTION", "");
			_filePaths.add(map);
			//add false data as last map is removed from arrayList when "GridViewImageAdapterNew.GalleryTypeGal"
			 map = new HashMap<String, String>();
				map.put("IMG",param2);	
				map.put("CAPTION", "");
				_filePaths.add(map);
			
        	Intent i = new Intent(this, FullScreenViewActivity.class);
			i.putExtra("_filePaths", _filePaths);
			i.putExtra("position", 0);
			i.putExtra("galleryType", GridViewImageAdapterNew.GalleryTypeGal);
			
			contentIntent = PendingIntent.getActivity(this, 0,i, 0);
			
        	 
        }else if(type!=null && type.equals(StaticObjects.GCM_TYPE_APP_UPDATE)){
        	
        	Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse("market://details?id=com.mcc.letseat"));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
        }





      /*  NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
        .setSmallIcon(R.drawable.ic_gcm)
        .setContentTitle(title)
        .setStyle(new NotificationCompat.BigTextStyle()
        .bigText(message))
        .setContentText(message);
        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());*/
        
        
        if(Style==null || Style.equals("0")){
       	 NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(this)
            .setSmallIcon(R.mipmap.ic_gcm)
            .setContentTitle(title)
            .setStyle(new NotificationCompat.BigTextStyle()
            .bigText(message))
            .setContentText(message);
       	 
       	 	//auto clear notification
       	 	mBuilder.setAutoCancel(true);
       	 	
       	 	// set notification priority to max
       	 	mBuilder.setPriority(Notification.PRIORITY_MAX);

            mBuilder.setContentIntent(contentIntent);
            mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
            
       }else if(Style.equals("1")){
    	   
    	   
			 target = new Target() {
    		     
				@Override
				public void onBitmapFailed(Drawable arg0) {
					// TODO Auto-generated method stub
					
				}
				@Override
				public void onBitmapLoaded(Bitmap bitmap, LoadedFrom arg1) {
					// TODO Auto-generated method stub
					Bitmap aBigBitmap = bitmap; //BitmapFactory.decodeResource(GcmIntentService.this.getResources(),  R.drawable.sp);
			           Bitmap aBitmap = BitmapFactory.decodeResource(GcmIntentService.this.getResources(),
			           		R.mipmap.ic_gcm);
			           
			        /*// Create the style object with BigPictureStyle subclass.
			           NotificationCompat.BigPictureStyle notiStyle = new  NotificationCompat.BigPictureStyle();
			           notiStyle.setBigContentTitle(title);
			           notiStyle.setSummaryText(message);
			        // Add the big picture to the style.
			           notiStyle.bigPicture(aBigBitmap);
			           
			           Notification picNotification= new NotificationCompat.Builder(GcmIntentService.this)
			           .setSmallIcon(R.drawable.ic_gcm)
			           .setAutoCancel(true)
			           .setPriority(Notification.PRIORITY_MAX)
			           .setLargeIcon(aBigBitmap)
			           .setContentIntent(contentIntent)
			           .setContentTitle(title)
			           .setContentText(message)
			           .setStyle(notiStyle).build();*/
			           
			           Notification picNotification= new NotificationCompat.Builder(GcmIntentService.this)
			           .setContentTitle(title)
			           .setAutoCancel(true)
			           .setPriority(Notification.PRIORITY_MAX)
			           .setContentText(message)
			           .setSmallIcon(R.mipmap.ic_gcm)
			           .setLargeIcon(aBitmap)
			           .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(aBigBitmap))
			           .setContentIntent(contentIntent)			           
			           .build();
			           
			           picNotification.flags=Notification.FLAG_AUTO_CANCEL;
			                       
			           mNotificationManager.notify(NOTIFICATION_ID, picNotification);
				}
				
				@Override
				public void onPrepareLoad(Drawable arg0) {
					// TODO Auto-generated method stub
					
				}
    		};
    		
    		if(param2!=null && !param2.isEmpty()){
    			
    			Handler uiHandler = new Handler(Looper.getMainLooper());
    	        uiHandler.post(new Runnable(){
    	            @Override
    	            public void run() {
    	               // Picasso.with(mContext).load(formatStaticImageUrlWith(location)).transform(new StaticMapTransformation()).into(remoteView, R.id.static_map_image, Constants.NOTIFICATION_ID_NEW_TRIP_OFFER, notification);
    	            	Picasso.with(GcmIntentService.this).load(param2).into(target);
    	            }
    	        });
    			
    			
    			
    		}
    		
       }
        
    }
    
    
   /* private void sendNotification(String title,  String message,  String type, String param1, String param2, String Style, int i) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, Splash.class), 0);
        
        if(type.equals(StaticObjects.GCM_TYPE_REST)){
        	Intent intent=new Intent(this, TabMainActivity.class);
        	intent.putExtra("Id", param1); 
        	 contentIntent = PendingIntent.getActivity(this, 0,intent, 0);
        }else  if(type.equals(StaticObjects.GCM_TYPE_OFFER)){
        	Intent intent=new Intent(this, TabMainActivity.class);
        	intent.putExtra("Id", param1); 
        	 contentIntent = PendingIntent.getActivity(this, 0,intent, 0);
        }
        
        if(Style.equals("0")){
        	 NotificationCompat.Builder mBuilder =
                     new NotificationCompat.Builder(this)
             .setSmallIcon(R.drawable.ic_gcm)
             .setContentTitle(title)
             .setStyle(new NotificationCompat.BigTextStyle()
             .bigText(message))
             .setContentText(message);

             mBuilder.setContentIntent(contentIntent);
             mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
             
        }else if(Style.equals("1")){
        	Bitmap aBigBitmap= BitmapFactory.decodeResource(this.getResources(),
                    R.drawable.sp);
            Bitmap aBitmap= BitmapFactory.decodeResource(this.getResources(),
            		R.drawable.grid_sample_photo);
            
            Notification picNotification= new NotificationCompat.Builder(this)
            .setContentTitle(title)
            .setContentText(message)
            .setSmallIcon(R.drawable.ic_gcm)
            .setLargeIcon(aBitmap)
            .setStyle(new NotificationCompat.BigPictureStyle()
                .bigPicture(aBigBitmap))
                .setContentIntent(contentIntent)
            .build();

                        
            mNotificationManager.notify(NOTIFICATION_ID, picNotification);
        }

       
        
        
        
        
    }*/
    
    
    private void sendNotification( String Message) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, Main.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
        .setSmallIcon(R.mipmap.ic_gcm)
        .setContentTitle("GCM Notification")
        .setStyle(new NotificationCompat.BigTextStyle()
        .bigText(Message))
        .setContentText(Message);

        //auto clear notification
        mBuilder.setAutoCancel(true);
        
        //set notification priority to max
   	 	mBuilder.setPriority(Notification.PRIORITY_MAX);
        
        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}