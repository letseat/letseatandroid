package com.mcc.le.gcm;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;


import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.JSONParser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.mcc.letseat.R;
import com.mcc.libs.ActivityWithSliding;
import com.mcc.user.RegistrationActivity;
import com.mcc.user.SavedPrefernce;

import android.R.string;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Settings, also use for push activation and de activation
 * @author Arif
 *
 */
public class PushActivity extends ActivityWithSliding implements OnClickListener {

	public static final String EXTRA_MESSAGE = "message";
	public static final String PROPERTY_REG_ID = "registration_id";
	private static final String PROPERTY_APP_VERSION = "appVersion";
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	/**
	 * Substitute you own sender ID here. This is the project number you got
	 * from the API Console, as described in "Getting Started."
	 */
	String SENDER_ID = "812541912855";

	/**
	 * Tag used on log messages.
	 */
	static final String TAG = "GCM";

	//TextView mDisplay;
	GoogleCloudMessaging gcm;
	AtomicInteger msgId = new AtomicInteger();
	SharedPreferences prefs;
	Context context;

	String regid;
	
	TextView textEditProfile, textFeedback, textTnC, textHelp, textAbout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_push);
		
		textEditProfile=(TextView) findViewById(R.id.textEditProfile); 
		textFeedback =(TextView) findViewById(R.id.textFeedback);
		textTnC =(TextView) findViewById(R.id.textTnC);
		textHelp =(TextView) findViewById(R.id.textHelp);
		textAbout =(TextView) findViewById(R.id.textAbout);
		textEditProfile.setOnClickListener(this); 
		textFeedback.setOnClickListener(this); 
		textTnC.setOnClickListener(this); 
		textHelp.setOnClickListener(this); 
		textAbout.setOnClickListener(this);

		//mDisplay = (TextView) findViewById(R.id.display);

		context = getApplicationContext();

		
		
		
		regid=getRegistrationId(this);
		
		CheckBox checkBoxPush=(CheckBox)findViewById(R.id.checkBoxPush);
		if(regid.isEmpty()){
			checkBoxPush.setChecked(false);
		}else{
			checkBoxPush.setChecked(true);
		}
		checkBoxPush.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					gcmRegistration();
					Toast.makeText(PushActivity.this, "wait, registering...", Toast.LENGTH_LONG).show();
				}else{
					Toast.makeText(PushActivity.this, "wait, unregistering...", Toast.LENGTH_LONG).show();
					gcm = GoogleCloudMessaging.getInstance(PushActivity.this);
					try {
						gcm.unregister();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});

	}

	
	
	
	void gcmRegistration(){

    	//sendNotification("demo");
		// Check device for Play Services APK. If check succeeds, proceed with
		// GCM registration.
    	if (checkPlayServices()) {
			gcm = GoogleCloudMessaging.getInstance(this);
			regid = getRegistrationId(context);

			if (regid.isEmpty()) {
				registerInBackground();
			}
		} else {
			Log.i(TAG, "No valid Google Play Services APK found.");
		}
    	
    	new AsyncTask<Object, Object, Object>(){
    		  String msg = "";
    		
            @Override
            protected String doInBackground(Object... params) {
              
                try {
                    Bundle data = new Bundle();
                        data.putString("my_message", "Hello World");
                        data.putString("my_action",
                                "com.google.android.gcm.demo.app.ECHO_NOW");
                        String id = Integer.toString(msgId.incrementAndGet());
                        gcm.send(SENDER_ID + "@gcm.googleapis.com", id, data);
                        msg = "Sent message";
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }
            
            @Override
			protected void onPostExecute(Object result) {
            	 
 	               // mDisplay.append(msg + "\n");
            };

            
        }.execute();
    
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		checkPlayServices();

	}

	/**
	 * Check the device to make sure it has the Google Play Services APK. If it
	 * doesn't, display a dialog that allows users to download the APK from the
	 * Google Play Store or enable it in the device's system settings.
	 */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i(TAG, "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}

	/**
	 * Gets the current registration ID for application on GCM service.
	 * <p>
	 * If result is empty, the app needs to register.
	 * 
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	private String getRegistrationId(Context context) {
		final SharedPreferences prefs = getGCMPreferences(context);
		String registrationId = prefs.getString(PROPERTY_REG_ID, "");
		if (registrationId.isEmpty()) {
			Log.i(TAG, "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
				Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i(TAG, "App version changed.");
			return "";
		}
		return registrationId;
	}

	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	private SharedPreferences getGCMPreferences(Context context) {
		// This sample app persists the registration ID in shared preferences,
		// but
		// how you store the regID in your app is up to you.
		return getSharedPreferences(PushActivity.class.getSimpleName(),
				Context.MODE_PRIVATE);
	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration ID and app versionCode in the application's
	 * shared preferences.
	 */
	private void registerInBackground() {
		
		new AsyncTask<Object, Object, Object>() {
			String msg = "";
			 String regResponse ="";
			@Override
			protected Object doInBackground(Object... params) {
				
	            try {
	                if (gcm == null) {
	                    gcm = GoogleCloudMessaging.getInstance(context);
	                }
	                regid = gcm.register(SENDER_ID);
	                msg = "Device registered, registration ID=" + regid;

	                // You should send the registration ID to your server over HTTP,
	                // so it can use GCM/HTTP or CCS to send messages to your app.
	                // The request to your server should be authenticated if your app
	                // is using accounts.
	                sendRegistrationIdToBackend();
	                
	                 regResponse =pushRegistration(regid);

	                // For this demo: we don't need to send it because the device
	                // will send upstream messages to a server that echo back the
	                // message using the 'from' address in the message.

	                // Persist the regID - no need to register again.
	                storeRegistrationId(context, regid);
	            } catch (IOException ex) {
	                msg = "Error :" + ex.getMessage();
	                // If there is an error, don't just keep trying to register.
	                // Require the user to click a button again, or perform
	                // exponential back-off.
	            }
	            return msg;

			}
			
			@Override
			protected void onPostExecute(Object result) {
				//mDisplay.append(msg + "\n");
				
				  if(regResponse.equals("success")){
	                	Toast.makeText(PushActivity.this, "Success", Toast.LENGTH_LONG).show();
	                }else{
	                	Toast.makeText(PushActivity.this, "Fail", Toast.LENGTH_LONG).show();
	                }

			}
		}.execute();
	}

	/**
	 * Sends the registration ID to your server over HTTP, so it can use
	 * GCM/HTTP or CCS to send messages to your app. Not needed for this demo
	 * since the device sends upstream messages to a server that echoes back the
	 * message using the 'from' address in the message.
	 */
	private void sendRegistrationIdToBackend() {
		// Your implementation here.
		
	}

	/**
	 * Stores the registration ID and app versionCode in the application's
	 * {@code SharedPreferences}.
	 * 
	 * @param context
	 *            application's context.
	 * @param regId
	 *            registration ID
	 */
	private void storeRegistrationId(Context context, String regId) {
		final SharedPreferences prefs = getGCMPreferences(context);
		int appVersion = getAppVersion(context);
		Log.i(TAG, "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PROPERTY_REG_ID, regId);
		editor.putInt(PROPERTY_APP_VERSION, appVersion);
		editor.commit();
	}
	

														

	
	public String pushRegistration(String regToken) {

		String userid =   new SavedPrefernce(this).getUserID();
		String postData = "token=" + regToken +"&userid="+userid ;

		JSONParser jParser = new JSONParser();

		String json = jParser.requestPost(FoodyResturent.PUSH_REG_URL, postData);
		try {
			if (json.startsWith("Error") || json.startsWith("failed"))
				return "failed";
			else if (json.startsWith("success"))
				return "success";
			else 
				return "failed";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "failed";
	}

	@Override
	public void onClick(final View view) {
		
	    //if (view == findViewById(R.id.send)) {gcmRegistration();} 
	    /*else if (view == findViewById(R.id.clear)) {
	       // mDisplay.setText("");
	    }*/
		//textEditProfile, , , , 
		if (view==textEditProfile) {
			//Toast.makeText(PushActivity.this, "textEditProfile", Toast.LENGTH_SHORT).show();
			if(new SavedPrefernce(this).isUserExists()){
				Intent intent=new Intent(PushActivity.this, RegistrationActivity.class);
				startActivity(intent);
			}else{
				new SavedPrefernce(this).callRegAlert();
			}
			
			
		}else if (view==textFeedback) {
			//Toast.makeText(PushActivity.this, "textFeedback", Toast.LENGTH_SHORT).show();
			Intent intent=new Intent(PushActivity.this, FeedBackActivity.class);
			startActivity(intent);
			
		}else if (view==textTnC) {
			//Toast.makeText(PushActivity.this, "textTnC", Toast.LENGTH_SHORT).show();
			Intent intent=new Intent(PushActivity.this, TermsConditionActivity.class);
			startActivity(intent);
			
		}else if (view==textHelp) {
			//Toast.makeText(PushActivity.this, "textHelp", Toast.LENGTH_SHORT).show();
			Intent intent=new Intent(PushActivity.this, HelpActivity.class);
			startActivity(intent);
			
		}else if (view==textAbout) {
			//Toast.makeText(PushActivity.this, "textAbout", Toast.LENGTH_SHORT).show();
			Intent intent=new Intent(PushActivity.this, AboutActivity.class);
			startActivity(intent);
		}
		
	}
	
	 private NotificationManager mNotificationManager;
	  public static final int NOTIFICATION_ID = 1;
	  
	 private void sendNotification(String msg) {
	        mNotificationManager = (NotificationManager)
	                this.getSystemService(Context.NOTIFICATION_SERVICE);

	        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
	                new Intent(this, PushActivity.class), 0);

	        NotificationCompat.Builder mBuilder =
	                new NotificationCompat.Builder(this)
	        .setSmallIcon(R.mipmap.ic_gcm)
	        .setContentTitle("GCM Notification")
	        .setStyle(new NotificationCompat.BigTextStyle()
	        .bigText(msg))
	        .setContentText(msg);

	        mBuilder.setContentIntent(contentIntent);
	        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
	    }
	
	


}
