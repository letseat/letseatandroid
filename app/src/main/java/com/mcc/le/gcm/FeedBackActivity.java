package com.mcc.le.gcm;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;

import com.foody.nearby.PostFeedBackTask;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mcc.letseat.R;
import com.mcc.user.SavedPrefernce;

/**
 * feedback about app and restaurant information correction
 * @author Arif
 *
 */
public class FeedBackActivity extends Activity implements OnClickListener, OnCheckedChangeListener{

	public static final int EXTRA_FROM_MENU=0;
	public static final int EXTRA_FROM_OTHERS=1;
	
	// admob
	AdView adView = null;
	
	String seperator="/";
	
	CheckBox checkbox_RestName, checkbox_cuisine,checkbox_address, checkbox_cost, checkbox_cash_card, checkbox_hours,
	checkbox_home_delivery,
	checkbox_dine_outside,
	checkbox_breakfast,
	checkbox_lunch,
	checkbox_dinner,
	
	checkbox_reservation,
	checkbox_bar,
	checkbox_buffet,
	checkbox_live_music,
	checkbox_veg,
	
	checkbox_non_veg,
	checkbox_seating_capacity,
	checkbox_kids_zone,
	checkbox_ac,
	checkbox_wifi, checkbox_all;
	
	EditText txtFeedback;
	String feedback=null;
	String restaurantId=null;
	int FLAG=0;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.slide_up_in,R.anim.stay);
		setContentView(R.layout.activity_feedback);
		
		restaurantId=getIntent().getStringExtra("resid");
		FLAG=getIntent().getIntExtra("flag", 0);
		
		getActionBar().hide();
		findViewById(R.id.layFeedbackConfirm).setVisibility(View.INVISIBLE);
		
		findViewById(R.id.btnFeedbackCancel).setOnClickListener(this);
		findViewById(R.id.layFeedbackConfirm).setOnClickListener(this);
		
		txtFeedback=(EditText) findViewById(R.id.editTextFeedback);
		txtFeedback.addTextChangedListener(mTextEditorWatcher);
		
		checkbox_RestName=((CheckBox)findViewById(R.id.checkbox_RestName));
		checkbox_cuisine=((CheckBox)findViewById(R.id.checkbox_cuisine));
		checkbox_address=((CheckBox)findViewById(R.id.checkbox_address));
		checkbox_cost=((CheckBox)findViewById(R.id.checkbox_cost));		
		checkbox_hours=((CheckBox)findViewById(R.id.checkbox_hours));
		checkbox_cash_card=((CheckBox)findViewById(R.id.checkbox_cash_card));
		
		checkbox_home_delivery=((CheckBox)findViewById(R.id.checkbox_home_delivery));
		checkbox_dine_outside=((CheckBox)findViewById(R.id.checkbox_dine_outside));
		checkbox_breakfast=((CheckBox)findViewById(R.id.checkbox_breakfast));		
		checkbox_lunch=((CheckBox)findViewById(R.id.checkbox_lunch));
		checkbox_dinner=((CheckBox)findViewById(R.id.checkbox_dinner));
		
		checkbox_reservation=((CheckBox)findViewById(R.id.checkbox_reservation));
		checkbox_bar=((CheckBox)findViewById(R.id.checkbox_bar));
		checkbox_buffet=((CheckBox)findViewById(R.id.checkbox_buffet));		
		checkbox_live_music=((CheckBox)findViewById(R.id.checkbox_live_music));
		checkbox_veg=((CheckBox)findViewById(R.id.checkbox_veg));
		
		checkbox_non_veg=((CheckBox)findViewById(R.id.checkbox_non_veg));
		checkbox_seating_capacity=((CheckBox)findViewById(R.id.checkbox_seating_capacity));
		checkbox_kids_zone=((CheckBox)findViewById(R.id.checkbox_kids_zone));		
		checkbox_ac=((CheckBox)findViewById(R.id.checkbox_ac));
		checkbox_wifi=((CheckBox)findViewById(R.id.checkbox_wifi));
		
		checkbox_all=((CheckBox)findViewById(R.id.checkbox_all));
		
		
		checkbox_RestName.setOnCheckedChangeListener(this);
		checkbox_cuisine.setOnCheckedChangeListener(this);
		checkbox_address.setOnCheckedChangeListener(this);
		checkbox_cost.setOnCheckedChangeListener(this);
		checkbox_cash_card.setOnCheckedChangeListener(this);
		checkbox_hours.setOnCheckedChangeListener(this);
		
		checkbox_home_delivery.setOnCheckedChangeListener(this);
		checkbox_dine_outside.setOnCheckedChangeListener(this);
		checkbox_breakfast.setOnCheckedChangeListener(this);
		checkbox_lunch.setOnCheckedChangeListener(this);
		checkbox_dinner.setOnCheckedChangeListener(this);
		
		checkbox_reservation.setOnCheckedChangeListener(this);
		checkbox_bar.setOnCheckedChangeListener(this);
		checkbox_buffet.setOnCheckedChangeListener(this);
		checkbox_live_music.setOnCheckedChangeListener(this);
		checkbox_veg.setOnCheckedChangeListener(this);
		
		checkbox_non_veg.setOnCheckedChangeListener(this);
		checkbox_seating_capacity.setOnCheckedChangeListener(this);
		checkbox_kids_zone.setOnCheckedChangeListener(this);
		checkbox_ac.setOnCheckedChangeListener(this);
		checkbox_wifi.setOnCheckedChangeListener(this);
				
		checkbox_all.setOnCheckedChangeListener(this);
		
		
		adView = (AdView) this.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder().build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});
		
		if(FLAG==EXTRA_FROM_MENU){
			TextView txtFeedbackTitle=(TextView)findViewById(R.id.txtFeedbackTitle);
			txtFeedbackTitle.setText("Feedback");
			hideCheckBox();
		}
	}
	
	private void hideCheckBox() {
		
		checkbox_RestName.setVisibility(View.GONE);
		checkbox_cuisine.setVisibility(View.GONE);
		checkbox_address.setVisibility(View.GONE);
		checkbox_cost.setVisibility(View.GONE);
		checkbox_cash_card.setVisibility(View.GONE);
		checkbox_hours.setVisibility(View.GONE);
		
		checkbox_home_delivery.setVisibility(View.GONE);
		checkbox_dine_outside.setVisibility(View.GONE);
		checkbox_breakfast.setVisibility(View.GONE);
		checkbox_lunch.setVisibility(View.GONE);
		checkbox_dinner.setVisibility(View.GONE);
		
		checkbox_reservation.setVisibility(View.GONE);
		checkbox_bar.setVisibility(View.GONE);
		checkbox_buffet.setVisibility(View.GONE);
		checkbox_live_music.setVisibility(View.GONE);
		checkbox_veg.setVisibility(View.GONE);
		
		checkbox_non_veg.setVisibility(View.GONE);
		checkbox_seating_capacity.setVisibility(View.GONE);
		checkbox_kids_zone.setVisibility(View.GONE);
		checkbox_ac.setVisibility(View.GONE);
		checkbox_wifi.setVisibility(View.GONE);
		
		findViewById(R.id.lay_feedback_checkbox).setVisibility(View.GONE);
		txtFeedback.setLines(6);
		
		
	}

	//confirm cancel button onclick listener
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v==findViewById(R.id.btnFeedbackCancel)){
			finish();
		}else if(v==findViewById(R.id.layFeedbackConfirm)){
			
			
			SavedPrefernce savedPrefernce=new SavedPrefernce(this); 
			String email=savedPrefernce.getUserEmail();
			if(email==""){
				savedPrefernce.callRegAlert();
			}else{
				sendFeedback(feedback, email, PostFeedBackTask.TYPE_RESTAURANT);
			}
			
		} 
		
	}
	
	//checkbox Checkedchange listener
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		if(buttonView==checkbox_all ){
			
			checkbox_RestName.setChecked(isChecked);
			checkbox_cuisine.setChecked(isChecked);
			checkbox_address.setChecked(isChecked); 
			checkbox_cost.setChecked(isChecked); 
			checkbox_cash_card.setChecked(isChecked); 
			checkbox_hours.setChecked(isChecked);
			
			checkbox_home_delivery.setChecked(isChecked);
			checkbox_dine_outside.setChecked(isChecked);
			checkbox_breakfast.setChecked(isChecked);
			checkbox_lunch.setChecked(isChecked);
			checkbox_dinner.setChecked(isChecked);
			
			checkbox_reservation.setChecked(isChecked);
			checkbox_bar.setChecked(isChecked);
			checkbox_buffet.setChecked(isChecked);
			checkbox_live_music.setChecked(isChecked);
			checkbox_veg.setChecked(isChecked);
			
			checkbox_non_veg.setChecked(isChecked);
			checkbox_seating_capacity.setChecked(isChecked);
			checkbox_kids_zone.setChecked(isChecked);
			checkbox_ac.setChecked(isChecked);
			checkbox_wifi.setChecked(isChecked); 

		}
		
		showHideConfirmButton();
	}
	
	
	
	
	//text change listener
	private final TextWatcher mTextEditorWatcher = new TextWatcher() {
		
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
			
		}
		
		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub			
			showHideConfirmButton();
		}
	};
	
	void showHideConfirmButton(){
		
		
		if( txtFeedback.getText().toString().isEmpty() && !checkbox_RestName.isChecked() &&  !checkbox_hours.isChecked() &&  !checkbox_cuisine.isChecked() && !checkbox_address.isChecked() && !checkbox_cost.isChecked() && !checkbox_cash_card.isChecked() 
				&& !checkbox_home_delivery.isChecked() 
				&& !checkbox_dine_outside.isChecked() 
				&& !checkbox_breakfast.isChecked() 
				&& !checkbox_lunch.isChecked() 
				&& !checkbox_dinner.isChecked() 
				
				&& !checkbox_reservation.isChecked() 
				&& !checkbox_bar.isChecked() 
				&& !checkbox_buffet.isChecked() 
				&& !checkbox_live_music.isChecked() 
				&& !checkbox_veg.isChecked() 
				
				&& !checkbox_non_veg.isChecked() 
				&& !checkbox_seating_capacity.isChecked() 
				&& !checkbox_kids_zone.isChecked() 
				&& !checkbox_ac.isChecked() 
				&& !checkbox_wifi.isChecked() ){
			
			findViewById(R.id.layFeedbackConfirm).setVisibility(View.INVISIBLE);
			feedback="";			
		}else{
			findViewById(R.id.layFeedbackConfirm).setVisibility(View.VISIBLE);
		}
		
		feedback="";	
		
		if(FLAG==EXTRA_FROM_OTHERS)	{	
			feedback="resid:"+restaurantId+" feedback: ";
			
			if(checkbox_RestName.isChecked())
				feedback+= seperator+checkbox_RestName.getText();
			
			if(checkbox_cuisine.isChecked())
				feedback+= seperator+checkbox_cuisine.getText();
			
			if(checkbox_hours.isChecked())
				feedback+=  seperator+checkbox_hours.getText();
			
			if(checkbox_address.isChecked())
				feedback+=  seperator+checkbox_address.getText();
			
			if(checkbox_cost.isChecked())
				feedback+=  seperator+checkbox_cost.getText();
			
			if(checkbox_cash_card.isChecked())
				feedback+=  seperator+checkbox_cash_card.getText();
			
			if(checkbox_home_delivery.isChecked())
				feedback+=  seperator+checkbox_home_delivery.getText();
			
			if(checkbox_dine_outside.isChecked())
				feedback+=  seperator+checkbox_dine_outside.getText();
			
			if(checkbox_breakfast.isChecked())
				feedback+=  seperator+checkbox_breakfast.getText();
			
			if(checkbox_lunch.isChecked())
				feedback+=  seperator+checkbox_lunch.getText();
			
			if(checkbox_dinner.isChecked())
				feedback+=  seperator+checkbox_dinner.getText();
			
			if(checkbox_reservation.isChecked())
				feedback+=  seperator+checkbox_reservation.getText();
			
			if(checkbox_bar.isChecked())
				feedback+=  seperator+checkbox_bar.getText();
			
			if(checkbox_buffet.isChecked())
				feedback+=  seperator+checkbox_buffet.getText();
			
			if(checkbox_live_music.isChecked())
				feedback+=  seperator+checkbox_live_music.getText();
			
			if(checkbox_veg.isChecked())
				feedback+=  seperator+checkbox_veg.getText();
			
			if(checkbox_non_veg.isChecked())
				feedback+=  seperator+checkbox_non_veg.getText();
			
			if(checkbox_seating_capacity.isChecked())
				feedback+=  seperator+checkbox_seating_capacity.getText();
			
			if(checkbox_kids_zone.isChecked())
				feedback+=  seperator+checkbox_kids_zone.getText();
			
			if(checkbox_ac.isChecked())
				feedback+=  seperator+checkbox_ac.getText();
			
			if(checkbox_wifi.isChecked())
				feedback+=  seperator+checkbox_wifi.getText();
		}
			
		if(!txtFeedback.getText().toString().isEmpty())
			feedback+= seperator+txtFeedback.getText().toString();
		
		
		
		
	}

	private void sendFeedback(String problem, String email, int type) {
		// TODO Auto-generated method stub
		//Toast.makeText(FeedBackActivity.this, problem, Toast.LENGTH_SHORT).show();
		new PostFeedBackTask(FeedBackActivity.this, email, problem, ""+type).execute();
	}
	
	
	@Override
	public void onPause() {
		super.onPause();
		overridePendingTransition(R.anim.stay,R.anim.slide_down_out);
		if (adView != null)
			adView.pause();
		
		//overridePendingTransition(0,R.anim.view_close_translate);

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		if (adView != null)
			adView.destroy();
		super.onDestroy();
		overridePendingTransition(R.anim.stay,R.anim.slide_down_out);
	}

	

	

}
