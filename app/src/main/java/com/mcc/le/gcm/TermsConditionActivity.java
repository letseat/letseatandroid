package com.mcc.le.gcm;

import android.os.Bundle;
import android.webkit.WebView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mcc.letseat.R;
import com.mcc.libs.ActivityWithSliding;

/**
 * Terms n Condition to use Let's Eat
 * @author Arif
 *
 */

public class TermsConditionActivity extends ActivityWithSliding{
	
	
	// //admob
	AdView adView = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		overridePendingTransition(R.anim.slide_up_in,R.anim.stay);
		setContentView(R.layout.activity_tnc);
		
		
		
		
		WebView mWebView = null;
	    mWebView = (WebView) findViewById(R.id.webViewPrivacyPolicy);
	    mWebView.getSettings().setJavaScriptEnabled(true);
	    mWebView.loadUrl("file:///android_asset/privecy.htm");
		
		//ad mob
		adView = (AdView) this.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder().build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});
	}
	
	
	
	@Override
	public void onPause() {
		super.onPause();
		overridePendingTransition(R.anim.stay,R.anim.slide_down_out);
		if (adView != null)
			adView.pause();
		
		//overridePendingTransition(0,R.anim.view_close_translate);

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		if (adView != null)
			adView.destroy();
		super.onDestroy();
		overridePendingTransition(R.anim.stay,R.anim.slide_down_out);
	}

}
