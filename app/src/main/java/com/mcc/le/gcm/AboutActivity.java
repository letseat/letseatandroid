package com.mcc.le.gcm;

import android.os.Bundle;
import android.widget.TextView;

import com.foody.AppData.StaticObjects;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mcc.letseat.R;
import com.mcc.libs.ActivityWithSliding;

/**
 * about app
 * @author Arif
 *
 */

public class AboutActivity extends ActivityWithSliding{
	
	
	// //admob
		AdView adView = null;
		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			
			overridePendingTransition(R.anim.slide_up_in,R.anim.stay);
			setContentView(R.layout.activity_about);
			
			
			
			
			TextView txtAppAbout=(TextView)findViewById(R.id.txtAppAbout);
			txtAppAbout.setTypeface(StaticObjects.gettfNormal(this));
			
			//ad mob
			adView = (AdView) this.findViewById(R.id.adView);

			AdRequest adRequest = new AdRequest.Builder().build();

			adView.loadAd(adRequest);

			adView.setAdListener(new AdListener() {
				public void onAdLoaded() {
				}

				public void onAdFailedToLoad(int errorcode) {
					System.out.println("Error:" + errorcode);
				}

			});
		}
		
		
		
		@Override
		public void onPause() {
			super.onPause();
			overridePendingTransition(R.anim.stay,R.anim.slide_down_out);
			if (adView != null)
				adView.pause();
			
			//overridePendingTransition(0,R.anim.view_close_translate);

		}

		@Override
		public void onResume() {
			super.onResume();

			if (adView != null)
				adView.resume();
		}

		@Override
		public void onDestroy() {
			if (adView != null)
				adView.destroy();
			super.onDestroy();
			overridePendingTransition(R.anim.stay,R.anim.slide_down_out);
		}
}
