package com.mcc.libs;

import com.mcc.letseat.R;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Animation.AnimationListener;

public class AnimationTween {

	
	
	public static  int width = 0;
	public static int height = 0;
	public static final TimeInterpolator sDecelerator = new DecelerateInterpolator();
	private static final int ANIM_DURATION = 500;
	static float sAnimatorScale = 2;
	final static long duration = (long) (ANIM_DURATION * sAnimatorScale);
	
	
	
	
	public static void zoomInOut(final View view, final Activity activity){
		if(activity==null)
			return;
		
		// Animation
	    Animation anim_zoom_in = AnimationUtils.loadAnimation(activity.getApplicationContext(),
                R.anim.zoom_in);
	    
	    anim_zoom_in.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub
				 Animation anim_zoom_out = AnimationUtils.loadAnimation(activity.getApplicationContext(),
			                R.anim.zoom_out);
				 view.startAnimation(anim_zoom_out);
			}
		});
	    view.startAnimation(anim_zoom_in);
	}
	
	public static void fade(final View view,  int mShortAnimationDuration) {

	    // Set the content view to 0% opacity but visible, so that it is visible
	    // (but fully transparent) during the animation.
	    view.setAlpha(0f);	    
	    view.setVisibility(View.VISIBLE);

	    // Animate the content view to 100% opacity, and clear any animation
	    // listener set on the view.
	    view.animate()
	            .alpha(1f)
	            .setDuration(mShortAnimationDuration)
	            .setListener(null);

	    
	  
	}

	
	public static void crossfade(final View firstView, final View secondView,  int mShortAnimationDuration) {

	    // Set the content view to 0% opacity but visible, so that it is visible
	    // (but fully transparent) during the animation.
	    secondView.setAlpha(0f);	    
	    secondView.setVisibility(View.VISIBLE);

	    // Animate the content view to 100% opacity, and clear any animation
	    // listener set on the view.
	    secondView.animate()
	            .alpha(1f)
	            .setDuration(mShortAnimationDuration)
	            .setListener(null);

	    
	    // Animate the loading view to 0% opacity. After the animation ends,
	    // set its visibility to GONE as an optimization step (it won't
	    // participate in layout passes, etc.)
	    firstView.animate()
	            .alpha(0f)
	            .setDuration(mShortAnimationDuration)
	            .setListener(new AnimatorListenerAdapter() {
	                @Override
	                public void onAnimationEnd(Animator animation) {
	                	firstView.setVisibility(View.GONE);
	                }
	            });
	}
	
	
	public static void animateTextReverse(View viewToanimate, Activity activity) {
		if(activity==null)
			return;
		
		getsize(activity);
		viewToanimate.setVisibility(View.VISIBLE);
		viewToanimate.setAlpha(1); 
		viewToanimate.setTranslationY( height+viewToanimate.getHeight() );
		viewToanimate.animate().setDuration(duration ).translationY(0)
				.alpha(0).setInterpolator(sDecelerator);

	}
	
	
	public static void animateText(View viewToanimate, Activity activity) {
		if(activity==null)
			return;
		
		getsize(activity);
		viewToanimate.setVisibility(View.VISIBLE);
		viewToanimate.setAlpha(1); 
		viewToanimate.setTranslationY(-viewToanimate.getHeight() + height);
		viewToanimate.animate().setDuration(duration ).translationY(0)
				.alpha(1).setInterpolator(sDecelerator);

	}
	
	
	
	public static void animateText(View viewToanimate, Animator.AnimatorListener animatorListene, Activity activity) {
		if(activity==null)
			return;
		
		getsize(activity);
		viewToanimate.setAlpha(0); 
		viewToanimate.setTranslationY(-viewToanimate.getHeight() + height);
		viewToanimate.animate().setDuration(duration ).translationY(0)
				.alpha(1).setInterpolator(sDecelerator).setListener(animatorListene);

	}
	
	

	public static void animateView(View viewToanimate,Activity activity) {
		if(activity==null)
			return;
		
		getsize(activity);
		viewToanimate.setAlpha(1); 
		viewToanimate.setTranslationY(0);
		viewToanimate.animate().setDuration(duration ).translationY(-viewToanimate.getHeight() - height)
				.alpha(1).setInterpolator(sDecelerator);

	}
	
	public static void animateView(View viewToanimate, Animator.AnimatorListener animatorListener, Activity activity) {
		if(activity==null)
			return;
		
		getsize(activity);
		viewToanimate.setAlpha(1); 
		//viewToanimate.setTranslationY(0);
		viewToanimate.animate().setDuration(duration ).translationY(-viewToanimate.getHeight() - height)
				.alpha(1).setInterpolator(sDecelerator).setListener(animatorListener);

	}
	
	public static void animateViewPosition(View viewToanimate, Activity activity) {
		if(activity==null)
			return;
		
		getsize( activity);
		viewToanimate.setAlpha(1); 
		viewToanimate.setTranslationY(-viewToanimate.getHeight() - height);
		viewToanimate.animate().setDuration(duration ).translationY(0).alpha(1).setInterpolator(sDecelerator);

	}
	
	
	public static void animateViewPositionUp(View viewToanimate, Activity activity) {
		if(activity==null)
			return;
		
		getsize( activity);
		viewToanimate.setAlpha(1); 
		viewToanimate.setTranslationY(viewToanimate.getHeight() + height);
		viewToanimate.animate().setDuration(duration ).translationY(0).alpha(1).setInterpolator(sDecelerator);

	}
	
	public static void animateViewPosition(View viewToanimate, Animator.AnimatorListener animatorListener, Activity activity) {
		if(activity==null)
			return;
		
		getsize( activity);
		viewToanimate.setAlpha(1); 
		viewToanimate.setTranslationY(-viewToanimate.getHeight() - height);
		viewToanimate.animate().setDuration(duration ).translationY(0)
		.alpha(1).setInterpolator(sDecelerator).setListener(animatorListener);

	}
	
	public static void getsize(Activity activity) {
		if(activity==null)
			return;
		
		Display display = ((WindowManager) activity.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		width = size.x;
		height = size.y;
	}
	
	public static void shakeOnError(Activity activity , View view) {
		if(activity==null)
			return;
		
		Animation shake = AnimationUtils.loadAnimation(activity, R.anim.shake);
		view.startAnimation(shake);
		} 
}

class MyApplicationContext extends Application{

    private static Context context;

    public void onCreate(){
        super.onCreate();
        MyApplicationContext.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return MyApplicationContext.context;
    }
}
