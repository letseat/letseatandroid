package com.mcc.libs;

import java.util.ArrayList;
import java.util.HashMap;

import com.foody.AppData.StaticObjects;
import com.foody.nearby.MyListAdapterAppMenu;
import com.foody.nearby.NearByplaceListView;
import com.foody.nearby.SearchResturentListView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mcc.letseat.ApplicationBase;
import com.mcc.letseat.HomeT;
import com.mcc.letseat.R;
import com.mcc.letseat.ApplicationBase.TrackerName;
import com.mcc.sildermenu.SlidingMenu;
import com.mcc.user.RegistrationActivity;
import com.mcc.user.SavedPrefernce;
import com.mcc.user.profile.UserActivity;

import android.animation.Animator;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView.OnEditorActionListener;

public class FragmentActivityWithSliding extends FragmentActivity {
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
		initNaddSlideMenu();
		
		randomBackground();

		try {
			// action bar hide title change icon
			getActionBar().setIcon(R.mipmap.ic_home_drawer);
			getActionBar().setDisplayShowTitleEnabled(false);

			// hide or show back icon on action bar
			getActionBar().setDisplayHomeAsUpEnabled(false);

			// enable app icon as home button
			getActionBar().setHomeButtonEnabled(true);
			
			

			// Set whether to include the application home affordance in the action
			// bar. Home is presented as either an activity icon or logo.
			// getActionBar().setDisplayShowHomeEnabled(true);
			
			/*ImageView icon = (ImageView) findViewById(android.R.id.home);
			FrameLayout.LayoutParams iconLp = (FrameLayout.LayoutParams) icon.getLayoutParams();
			iconLp.topMargin = iconLp.bottomMargin = 0;
			iconLp.leftMargin=-10;
			icon.setLayoutParams( iconLp );*/
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	 FloatingActionButton fabButton;
	
	 public void setHomeButton(final Activity activity) {
		  fabButton = new FloatingActionButton.Builder(this)
			.withDrawable(getResources().getDrawable(R.mipmap.ic_home))
			.withButtonColor(Color.parseColor("#80FFFFFF"))
			.withGravity(Gravity.BOTTOM | Gravity.RIGHT)
			.withMargins(0, 0, 16, 72)
			.create(); 
			
			fabButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//Toast.makeText(getApplicationContext(), "floating", Toast.LENGTH_SHORT).show();
					Intent mainIntent = new Intent(activity, HomeT.class);							
					startActivity(mainIntent);									
					finish(); 
				}
			});
			
			homeButtonhide();

	}
	 
	 public void setHomeButtonLeft(final Activity activity) {
		  fabButton = new FloatingActionButton.Builder(this)
			.withDrawable(getResources().getDrawable(R.mipmap.ic_home))
			.withButtonColor(Color.parseColor("#80FFFFFF"))
			.withGravity(Gravity.BOTTOM | Gravity.LEFT)
			.withMargins(0, 0, 16, 72)
			.create(); 
			
			fabButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//Toast.makeText(getApplicationContext(), "floating", Toast.LENGTH_SHORT).show();
					Intent mainIntent = new Intent(activity, HomeT.class);							
					startActivity(mainIntent);									
					finish(); 
				}
			});
			
			homeButtonhide();

	}
	 
	public void homeButtonshow(){
		 if(fabButton!=null)
			 fabButton.setVisibility(View.VISIBLE);
	 }
	 
	public  void homeButtonhide(){
		 if(fabButton!=null)
			 fabButton.setVisibility(View.GONE);
	 }
	 
	 
	
	
	
	@Override
	protected void onPause() {
		
		super.onPause();
		 overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
	}
	
	
	void randomBackground(){
		/*try {
			int randomNum = (int) Math.floor(Math.random() * StaticObjects.backRes.length);	
			//Bitmap myBitmap = BitmapFactory.decodeStream
			
			// bimatp factory
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inSampleSize = 8;
			Bitmap bm = BitmapFactory.decodeResource(getResources(), StaticObjects.backRes[randomNum]);			
			BitmapDrawable bd = new BitmapDrawable(bm);
			getWindow().getDecorView().findViewById(android.R.id.content).setBackgroundDrawable(bd);
		} catch (OutOfMemoryError e) {
			// TODO: handle exception
		}catch (Exception e) {
			// TODO: handle exception
		}finally{
			Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.back8);			
			BitmapDrawable bd = new BitmapDrawable(bm);
			getWindow().getDecorView().findViewById(android.R.id.content).setBackgroundDrawable(bd);
		}*/
		

	}
	
	
	public SlidingMenu mSlideHolder;

	void initNaddSlideMenu() {
		// slide
		mSlideHolder = new SlidingMenu(this);

		mSlideHolder.setMode(SlidingMenu.LEFT);

		mSlideHolder.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);

		mSlideHolder.setBehindOffset(100);

		mSlideHolder.attachToActivity(this, SlidingMenu.SLIDING_WINDOW, true);

		mSlideHolder.setMenu(R.layout.sliding_menu_list);
		mSlideHolder.setSlidingEnabled(true);

		// mSlideHolder.toggle();

		// populate data on list
		ListView appMenuList = (ListView) findViewById(R.id.menu_list);

		appMenuList.setClickable(true);

		appMenuList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Log.e("menu======================", "menu " + position);
				// menuNevigation(position);
			}
		});

		
		

		ListAdapter adapter = new MyListAdapterAppMenu(this, StaticObjects.getAppMenuData(this), StaticObjects.Menu_Key, mSlideHolder);
		appMenuList.setAdapter(adapter);

		TextView appName = (TextView) findViewById(R.id.txtAppName);
		TextView appVersoion = (TextView) findViewById(R.id.txtAppVersion);
		TextView aboutApp = (TextView) findViewById(R.id.txtSlidingAbout);
		PackageInfo pInfo;
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			appName.setText(getString(R.string.app_name) );
			appVersoion.setText("v"+ pInfo.versionName);

			aboutApp.setText(getString(R.string.app_about) );
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
		case android.R.id.home:
			mSlideHolder.toggle();
			// NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.action_search:
			// search action
			return true;
		case R.id.action_nearby:
			Intent i = new Intent(this, NearByplaceListView.class);
			startActivity(i);
			return true;
		case R.id.action_profile:
			SavedPrefernce savedPrefernce=new SavedPrefernce(this);
			if(savedPrefernce.isUserExists()){
				Intent intent = new Intent(this, UserActivity.class);
				intent.putExtra("userid", savedPrefernce.getUserID());
				startActivity(intent);
			}else{
				savedPrefernce.callRegAlert();
			}			
			return true;	
		
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	
	// action bar menu and event
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// TODO Auto-generated method stub

			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.activity_main, menu);
			/*// Associate searchable configuration with the SearchView
			SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
			SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
			searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));*/

			return super.onCreateOptionsMenu(menu);
		}
		
		
		@Override
		public boolean onKeyDown(int keyCode, KeyEvent event) {
			// Handle the back button
			if (keyCode == KeyEvent.KEYCODE_MENU) {
				if(mSlideHolder!=null)
					mSlideHolder.toggle();
				return true;
			}else if (keyCode == KeyEvent.KEYCODE_BACK && isSearchViewOpen) {
				
				((RelativeLayout)searchview.getParent()).removeView(searchview);
				isSearchViewOpen=false;
				return true;
			} else {
				return super.onKeyDown(keyCode, event);
			}
		}
		
		
		public View searchview ;
		protected boolean isSearchViewOpen;
		public void addSearchLayout(RelativeLayout relativeLayout){
			if(relativeLayout.findViewWithTag("searchview")==null){
				
				if(isSearchViewOpen){
					//relativeLayout.removeView(searchview);
					((RelativeLayout)searchview.getParent()).removeView(searchview);
					isSearchViewOpen=false;
					
					
					return;
				}
				
				
				 searchview = getLayoutInflater().inflate(R.layout.search,  relativeLayout, false);
				 searchview.setTag(searchview);
				 
				final ImageView img_edittext_cancel=(ImageView)searchview.findViewById(R.id.img_edittext_cancel);
				
				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
				
				relativeLayout.addView(searchview, params);
				
				final EditText editTextSearch=(EditText)  searchview.findViewById(R.id.txtSearch);
				
				final Animator.AnimatorListener animatorListener2= new Animator.AnimatorListener() {
				    public void onAnimationStart(Animator animation) {}
				    public void onAnimationRepeat(Animator animation) { }

				    public void onAnimationEnd(Animator animation) {			    	
				    	// hide soft keyboard
						
						String searchText=editTextSearch.getText().toString().trim();					
						
						searchview.clearAnimation();
						Intent i = new Intent(FragmentActivityWithSliding.this,SearchResturentListView.class);
						i.putExtra("searchKeyWord_home", searchText);
						startActivity(i);
										
				    }
				    public void onAnimationCancel(Animator animation) {}
				  };
				
				
				editTextSearch.setOnEditorActionListener(new OnEditorActionListener() {

					@Override
					public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
						// TODO Auto-generated method stub
						if (actionId == EditorInfo.IME_ACTION_SEARCH) {
							String searchText=editTextSearch.getText().toString().trim();
							if(searchText.isEmpty() || searchText=="" || searchText==null){
								AnimationTween.shakeOnError(FragmentActivityWithSliding.this, editTextSearch);
								
							}else{
								InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
								imm.hideSoftInputFromWindow(editTextSearch.getWindowToken(), 0);
								
								AnimationTween.animateView(searchview,animatorListener2, FragmentActivityWithSliding.this);
							}
							
						}
						return false;
					}
				});
				
				// Edittext lose focus hide search view 
				editTextSearch.setOnFocusChangeListener(new OnFocusChangeListener() {          

			        public void onFocusChange(View v, boolean hasFocus) {
			        	 if(hasFocus){
			        	     //   Toast.makeText(getApplicationContext(), "got the focus", Toast.LENGTH_LONG).show();
			        	       
			        	    }else {
			        	       // Toast.makeText(getApplicationContext(), "lost the focus", Toast.LENGTH_LONG).show();
			        	        
			        	    }
			        
			               
			        }
			    });
				
					
				Animator.AnimatorListener animatorListener 
				  = new Animator.AnimatorListener() {

				    public void onAnimationStart(Animator animation) {}
				    public void onAnimationRepeat(Animator animation) {}
				    public void onAnimationEnd(Animator animation) {			    	
				    	//Toast.makeText(getApplicationContext(), "onAnimationEnd", Toast.LENGTH_LONG).show();
				    	editTextSearch.requestFocus();
				    	searchview.clearAnimation();
				    }

				    public void onAnimationCancel(Animator animation) {}
				  };			  
				  AnimationTween.animateViewPosition(searchview,animatorListener ,FragmentActivityWithSliding.this);
				  
				  
				  editTextSearch.addTextChangedListener(new TextWatcher() {
					
					@Override
					public void onTextChanged(CharSequence s, int start, int before, int count) {
						// TODO Auto-generated method stub
						if(editTextSearch!=null && s.length()>0){
							img_edittext_cancel.setVisibility(View.VISIBLE);
						}else{
							img_edittext_cancel.setVisibility(View.INVISIBLE);
						}
					}
					
					@Override
					public void beforeTextChanged(CharSequence s, int start, int count,
							int after) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void afterTextChanged(Editable s) {
						// TODO Auto-generated method stub
						
					}
				});
				  
				  img_edittext_cancel.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							editTextSearch.setText("");
							
						}
					});
				  
				  isSearchViewOpen=true;
				
			}
		}
		
		
		public void analyticsSendScreenView(String screenName) {

			// Obtain the shared Tracker instance.
			ApplicationBase application = (ApplicationBase) getApplication();
			Tracker tracker = application.getDefaultTracker(TrackerName.GLOBAL_TRACKER);

			// All subsequent hits will be send with screen name = "Home screen"
			tracker.setScreenName(screenName);
			// Send a screen view.
			tracker.send(new HitBuilders.ScreenViewBuilder().build());
			tracker.setScreenName(null);

		}
		
		public void analyticsSendEvent(String screenName, String category, String action){
			
			
			// Obtain the shared Tracker instance.
			ApplicationBase application = (ApplicationBase) getApplication();
			Tracker tracker = application.getDefaultTracker(TrackerName.GLOBAL_TRACKER);

			// All subsequent hits will be send with screen name = "Home screen"
			tracker.setScreenName(screenName);
			tracker.send(new HitBuilders.EventBuilder()
		    .setCategory(category)
		    .setAction(action)
		    .build());
			tracker.send(new HitBuilders.ScreenViewBuilder().build());
			// Clear the screen name field when we're done.
			tracker.setScreenName(null);
		}
		
		public void analyticsSendEvent(String screenName, String category, String action, String label){
			
			
			// Obtain the shared Tracker instance.
			ApplicationBase application = (ApplicationBase) getApplication();
			Tracker tracker = application.getDefaultTracker(TrackerName.GLOBAL_TRACKER);

			// All subsequent hits will be send with screen name = "Home screen"
			tracker.setScreenName(screenName);
			tracker.send(new HitBuilders.EventBuilder()
		    .setCategory(category)
		    .setAction(action)
		    .setLabel(label)
		    .build());
			tracker.send(new HitBuilders.ScreenViewBuilder().build());
			// Clear the screen name field when we're done.
			tracker.setScreenName(null);
		}
		
		
}
