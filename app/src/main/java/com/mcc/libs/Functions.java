package com.mcc.libs;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore.Images.Media;

public class Functions {
//	Bitmap.createScaledBitmap(src, dstWidth, dstHeight, filter)
	
	public File resizeImageFile( Uri fileUri){
		
		
		try{
		
		
		File file = new File(fileUri.getPath());  
		Bitmap bit=decodeFile(file);
		
		
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		bit.compress(CompressFormat.JPEG, 60 /*ignored for PNG*/, bos);
		byte[] bitmapdata = bos.toByteArray();
		new FileOutputStream(file).write(bitmapdata);
		return file;
//		Bitmap capturedBmp=BitmapFactory.decodeFile(fileUri.getPath());
//		
//		//Bitmap capturedBmp = Media.getBitmap(context.getContentResolver(), fileUri);
//		
//		ByteArrayOutputStream bos = new ByteArrayOutputStream();
//		capturedBmp=getResizedBitmap(capturedBmp, 1024, 768);
//		
//		capturedBmp.compress(CompressFormat.JPEG, 60 /*ignored for PNG*/, bos);
//		byte[] bitmapdata = bos.toByteArray();
//		
//		new FileOutputStream(file).write(bitmapdata);
//		
//		
//		return file;
//
		}catch(Exception e){}
		
		return null;
		
		
	}
	
	public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {

		int width = bm.getWidth();

		int height = bm.getHeight();

		float scaleWidth = ((float) newWidth) / width;

		float scaleHeight = ((float) newHeight) / height;

		
		float ratio = (float)scaleWidth/(float)scaleHeight;
		// create a matrix for the manipulation

		Matrix matrix = new Matrix();

		// resize the bit map

		matrix.postScale(ratio, ratio);

		// recreate the new Bitmap

		Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);

		return resizedBitmap;

		
		
		}
	private Bitmap decodeFile(File f){
	    try {
	        //Decode image size
	        BitmapFactory.Options o = new BitmapFactory.Options();
	        o.inJustDecodeBounds = true;
	        BitmapFactory.decodeStream(new FileInputStream(f),null,o);

	        //The new size we want to scale to
	        final int REQUIRED_SIZE=400;

	        //Find the correct scale value. It should be the power of 2.
	        int width_tmp=o.outWidth, height_tmp=o.outHeight;
	        int scale=1;
	        while(true){
	            if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
	                break;
	            width_tmp/=2;
	            height_tmp/=2;
	            scale*=2;
	        }

	        //Decode with inSampleSize
	        BitmapFactory.Options o2 = new BitmapFactory.Options();
	        o2.inSampleSize=scale;
	        return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
	    } catch (FileNotFoundException e) {}
	    return null;
	}
	
}
