package com.mcc.libs;

import com.foody.AppData.StaticObjects;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewWithCustFont extends TextView {

    public TextViewWithCustFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode())         
        this.setTypeface(StaticObjects.gettfLight(context));
    }

    public TextViewWithCustFont(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if(!isInEditMode()) 
        this.setTypeface(StaticObjects.gettfLight(context));
    }

    public TextViewWithCustFont(Context context) {
        super(context);
        if(!isInEditMode()) 
        this.setTypeface(StaticObjects.gettfLight(context));
    }
    
    

}