package com.mcc.libs;

import android.graphics.Color;
import android.view.MotionEvent;
import android.view.View;

public class TouchEffectListener implements View.OnTouchListener {
	
	private String upColor;
	private String downColor;
	
	public TouchEffectListener(String upColor, String downColor) {
		this.upColor=upColor;
		this.downColor=downColor;
	}

	@Override
	public boolean onTouch(View v, MotionEvent e) {
		// TODO Auto-generated method stub
		if (e.getAction() == MotionEvent.ACTION_UP ||e.getAction() == MotionEvent.ACTION_CANCEL || e.getAction() == MotionEvent.ACTION_OUTSIDE) {
			
			v.setBackgroundColor(Color.parseColor(upColor));
			
			//return true;
		} else if (e.getAction() == MotionEvent.ACTION_DOWN) {

			v.setBackgroundColor(Color.parseColor(downColor));
			//return true;
		}

		return false;
	}

	// View
}
