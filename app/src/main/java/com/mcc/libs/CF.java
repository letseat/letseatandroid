package com.mcc.libs;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

public class CF {

	public static boolean isTimeBetweenTwoTime(String initialTime,
			String finalTime) throws ParseException {

		// current time
		Calendar calendar = new GregorianCalendar();
		int hour = calendar.get(Calendar.HOUR);
		int minute = calendar.get(Calendar.MINUTE);

		if (calendar.get(Calendar.AM_PM) != 0)
			hour += 12;

		String hh = "" + hour;
		String mm = "" + minute;

		if (hh.length() == 1)
			hh = "0" + hour;

		if (mm.length() == 1)
			mm = "0" + minute;

		String currentTime = hh + ":" + mm + ":" + "00";

		String reg = "^([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$";
		if (initialTime.matches(reg) && finalTime.matches(reg)
				&& currentTime.matches(reg)) {
			boolean valid = false;
			// Start Time
			java.util.Date inTime = new SimpleDateFormat("HH:mm:ss")
					.parse(initialTime);
			Calendar calendar1 = Calendar.getInstance();
			calendar1.setTime(inTime);

			// Current Time
			java.util.Date checkTime = new SimpleDateFormat("HH:mm:ss")
					.parse(currentTime);
			Calendar calendar3 = Calendar.getInstance();
			calendar3.setTime(checkTime);

			// End Time
			java.util.Date finTime = new SimpleDateFormat("HH:mm:ss")
					.parse(finalTime);
			Calendar calendar2 = Calendar.getInstance();
			calendar2.setTime(finTime);

			if (finalTime.compareTo(initialTime) < 0) {
				calendar2.add(Calendar.DATE, 1);
				calendar3.add(Calendar.DATE, 1);
			}

			java.util.Date actualTime = calendar3.getTime();
			if ((actualTime.after(calendar1.getTime()) || actualTime
					.compareTo(calendar1.getTime()) == 0)
					&& actualTime.before(calendar2.getTime())) {
				valid = true;
			}
			return valid;
		} else {
			throw new IllegalArgumentException(
					"Not a valid time, expecting HH:MM:SS format");
		}

	}

	public static String openTodayOrTomorrow(String openTime,
			String closeTime) throws ParseException{
		
		//current time
				Calendar calendar = new GregorianCalendar();           
		        int hour = calendar.get( Calendar.HOUR );   
		        int minute = calendar.get( Calendar.MINUTE );
		        
		        if( calendar.get( Calendar.AM_PM ) != 0 )
		        	hour+=12;
		        
		        String hh=""+hour;
		        String mm=""+minute;
		        
		        if(hh.length()==1)
		        	hh="0"+hour;
		        
		        if(mm.length()==1)
		        	mm="0"+minute;
		        
		        String currentTime= hh+":"+mm+":"+"00";
				
		        String reg = "^([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$";
		        if (openTime.matches(reg) 
		        		&& closeTime.matches(reg) 
		        		&& currentTime.matches(reg)) {
		        	
		        	String todayOrTomorrow = "";
		            //Start Time
		            java.util.Date inTime = new SimpleDateFormat("HH:mm:ss").parse(openTime);
		            Calendar openCalendar = Calendar.getInstance();
		            openCalendar.setTime(inTime);

		            //Current Time
		            java.util.Date checkTime = new SimpleDateFormat("HH:mm:ss").parse(currentTime);
		            Calendar calendar3 = Calendar.getInstance();
		            calendar3.setTime(checkTime);

		            //End Time
		            java.util.Date finTime = new SimpleDateFormat("HH:mm:ss").parse(closeTime);
		            Calendar closeCalendar = Calendar.getInstance();
		            closeCalendar.setTime(finTime);

		            if (closeTime.compareTo(openTime) < 0) {
		            	closeCalendar.add(Calendar.DATE, 1);
		                calendar3.add(Calendar.DATE, 1);
		            }

		            java.util.Date actualTime = calendar3.getTime();
		            
		            if ((actualTime.before(openCalendar.getTime()) || actualTime.compareTo(openCalendar.getTime()) == 0) ){
		            	todayOrTomorrow = "Today";
		            }else if(actualTime.after(closeCalendar.getTime()) ){
		            	todayOrTomorrow = "Tomorrow";
		            }
		            
		            return todayOrTomorrow;
		        } else {
		            throw new IllegalArgumentException("Not a valid time, expecting HH:MM:SS format");
		        }


		//return ""
	}

	public static String timePatternmaker(final String open) {
		String startTime = "";
		
		try {
			
			
			int ish = open.indexOf(":");
			int ism = open.lastIndexOf(":");

			String sh = open.substring(0, ish);

			String sm = open.substring(ish + 1, ism);
			String am_pm = open.substring(ism + 1, open.length());

			if (am_pm.equalsIgnoreCase("PM") && Integer.parseInt(sh) < 12) {
				sh = "" + (Integer.parseInt(sh) + Integer.parseInt("12"));
			}

			if (am_pm.equalsIgnoreCase("AM") && Integer.parseInt(sh) == 12) {
				sh = "" + (Integer.parseInt(sh) + Integer.parseInt("11"));
			}

			if (sh.length() == 1)
				startTime = "0" + sh;
			else
				startTime = sh;

			startTime += ":";

			if (sm.length() == 1)
				startTime += ("0" + sm);
			else
				startTime += sm;

			startTime += ":00";

			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return startTime;
		
	

	}

	public static double getDistanceKM(LatLng fromPosition, LatLng toPosition) {
		Location locationA = new Location("point A");

		locationA.setLatitude(fromPosition.latitude);

		locationA.setLongitude(fromPosition.longitude);

		Location locationB = new Location("point B");

		locationB.setLatitude(toPosition.latitude);

		locationB.setLongitude(toPosition.longitude);

		double value = (locationA.distanceTo(locationB) / 1000);
		// double rounded = (double) Math.round(value,2 ) ;
		Log.i("kilometer before format", value + "");

		DecimalFormat df = new DecimalFormat("###0.00");
		double rounded = Double.parseDouble(df.format(value));
		Log.i("kilometer after format", rounded + "");

		return rounded;

	}
	
	public static final int OPEN=1;
	public static final int CLOSE=0;
	public static int  openOrClose(String Open, String Close){
		//open or close
		String _Open = Open.equals("") ? "N/A": Open + "-" + Close;
		
		int openClose=0;
		
		if(!_Open.equals("N/A")){
			try {
				if ( Open.equals(Close) || CF.isTimeBetweenTwoTime(CF.timePatternmaker(Open),  CF.timePatternmaker(Close)) ){
					
					openClose=OPEN; 
					
					
				}else{
					
					openClose=CLOSE;
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return openClose;
	}
	
	
	

}
