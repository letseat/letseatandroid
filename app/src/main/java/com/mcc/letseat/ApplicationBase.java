package com.mcc.letseat;


import java.util.HashMap;

import org.acra.*;
import org.acra.annotation.*;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.mcc.extensions.JsonSender; 
import com.mcc.letseat.R;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.Picasso;

//http://www.letseatasia.com/mobsvc/reports/crashreport.php
//http://lifeliapp.com/appli/reports/crashreport.php

@ReportsCrashes(formKey = "", formUri = "http://www.letseatasia.com/mobsvc/reports/crashreport.php", customReportContent = {
		ReportField.APP_VERSION_CODE, ReportField.APP_VERSION_NAME,
		ReportField.ANDROID_VERSION, ReportField.PHONE_MODEL,
		ReportField.USER_COMMENT, ReportField.STACK_TRACE }, mode = ReportingInteractionMode.DIALOG, resToastText = R.string.crash_toast_text, resDialogText = R.string.crash_dialog_text, resDialogIcon = android.R.drawable.ic_dialog_info, resDialogTitle = R.string.crash_dialog_title, resDialogCommentPrompt = R.string.crash_dialog_comment_prompt, resDialogOkToast = R.string.crash_dialog_ok_toast)

public class ApplicationBase extends android.support.multidex.MultiDexApplication  {

	private static ReportsCrashes mReportsCrashes;
	
	
	//analytics
/*	public static GoogleAnalytics analytics;
    public static Tracker tracker;
    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }
    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();*/


	@Override
	public void onCreate() {
        super.onCreate();

		ACRA.init(this);
		
		mReportsCrashes = this.getClass().getAnnotation(ReportsCrashes.class);
		JsonSender jsonSender = new JsonSender(mReportsCrashes.formUri(), null);
		ErrorReporter.getInstance().setReportSender(jsonSender);
		
		/*//analytics initiate 	
		analytics = GoogleAnalytics.getInstance(this);
		analytics.setLocalDispatchPeriod(1800);

		tracker = analytics.newTracker("UA-60218631-2");//R.xml.analytics_global_config); 	    
		tracker.enableAdvertisingIdCollection(true);
		tracker.enableAutoActivityTracking(true);*/

		Picasso p = new Picasso.Builder(this)
	    .memoryCache(new LruCache(20000))
	    .build();
	}
	
	
	/*synchronized public Tracker getDefaultTracker() {
	    if (tracker == null) {
	      GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
	      tracker = analytics.newTracker("UA-60218631-2");//R.xml.analytics_global_config);
	      tracker.enableAdvertisingIdCollection(true);
		  tracker.enableAutoActivityTracking(true);
	    }
	    return tracker;
	  }*/

	
	//analytics
	 // The following line should be changed to include the correct property id.
    private static final String PROPERTY_ID = "UA-65963391-1";

    public static int GENERAL_TRACKER = 0;

    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }

    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    

    public synchronized Tracker getDefaultTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            analytics.setLocalDispatchPeriod(15);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID)
                    : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(  R.xml.global_tracker)
                            : analytics.newTracker(R.xml.ecommerce_tracker);
            mTrackers.put(trackerId, t);
            
            t.enableAutoActivityTracking(true);

        }
        return mTrackers.get(trackerId);
    }
    
   protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}