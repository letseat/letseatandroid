/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mcc.letseat.quary;



import java.util.HashMap;
import java.util.Map;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.LetsEatRequest;
import com.foody.jsondata.user.User;
import com.mcc.letseat.HomeT;
import com.mcc.letseat.R;
import com.mcc.user.LoginActivity;
import com.mcc.user.RegistrationActivity;
import com.mcc.user.SavedPrefernce;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.transition.Scene;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

/**
 * 
 *  ScreenSlidePageFragment
 */
public class ScreenSlideActivity extends FragmentActivity {
	
	static public String FavRes;
	static public String FavFood;
	static public String NoResVisit;
	static public String VisitWith;
	static public String VisitTime;	
	static public String locationId;
	static public String location;
	/*static public String DistrictId;
	static public String District;*/
	static public String ZoneId;
	static public String Zone;
	public static  String CuisineId ;
	
    /**
     * The number of pages (wizard steps) to show in this demo.
     */
    private static final int NUM_PAGES = 5;
	

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    public static ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_slide);

        
        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setOffscreenPageLimit(0);
        FragmentManager fragmentManager= getSupportFragmentManager();
        mPagerAdapter = new ScreenSlidePagerAdapter(fragmentManager);
        mPager.setAdapter(mPagerAdapter);
        mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // When changing pages, reset the action bar actions since they are dependent
                // on which page is currently active. An alternative approach is to have each
                // fragment expose actions itself (rather than the activity exposing actions),
                // but for simplicity, the activity provides the actions in this sample.
                invalidateOptionsMenu();
                //setData(position-1);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.activity_screen_slide, menu);

        menu.findItem(R.id.action_previous).setEnabled(mPager.getCurrentItem() > 0);

        // Add either a "next" or "finish" button to the action bar, depending on which page
        // is currently selected.
        MenuItem item = menu.add(Menu.NONE, R.id.action_next, Menu.NONE,
                (mPager.getCurrentItem() == mPagerAdapter.getCount() - 1)
                        ? R.string.action_finish
                        : R.string.action_next);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Navigate "up" the demo structure to the launchpad activity.
                // See http://developer.android.com/design/patterns/navigation.html for more.
                NavUtils.navigateUpTo(this, new Intent(this, HomeT.class));
                return true;

            case R.id.action_previous:
                // Go to the previous step in the wizard. If there is no previous step,
                // setCurrentItem will do nothing.
                mPager.setCurrentItem(mPager.getCurrentItem() - 1);
                isLastSlide=false;
                return true;

            case R.id.action_next:
                // Advance to the next step in the wizard. If there is no next step, setCurrentItem
                // will do nothing.
                mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                //goToSearch();
                updateProfile();
               // Toast.makeText(this, NUM_PAGES+"<>"+(mPager.getCurrentItem() + 1), Toast.LENGTH_SHORT).show();
                if(NUM_PAGES==(mPager.getCurrentItem() + 1)){
                	isLastSlide=true;
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    
    boolean isLastSlide=false;
    void goToSearch(){
    	if (isLastSlide) {
		//Intent intent=new Intent(this, QuarySearchResult.class);
		Intent intent=new Intent(this, HomeT.class);
		/*intent.putExtra("SearchString", FavFood);
		intent.putExtra("SearchLocation", location);
		intent.putExtra("SearchLocationId", locationId);*/
		
		startActivity(intent);
		finish();
		}    	
    }
    
    private void updateProfile(){
    	if (isLastSlide) {
    		new EditProfile().execute();
    		
    	/*	SavedPrefernce prefernce=new SavedPrefernce(ScreenSlideActivity.this);
			user=prefernce.getUser();
			user.FavRes = ScreenSlideActivity.FavRes;
			user.FavFood = ScreenSlideActivity.FavFood;
			user.NoResVisit = ScreenSlideActivity.NoResVisit;
			user.VisitWith = ScreenSlideActivity.VisitWith ;
			user.VisitTime = ScreenSlideActivity.VisitTime;
			user.FavCus = ScreenSlideActivity.CuisineId;
			user.CITY = ScreenSlideActivity.Zone;
    		*/
    		//volley 
    	/*	StringRequest postRequestUpdateProfile = new StringRequest(Request.Method.POST, LetsEatRequest.PROFILE_EDIT_URL,
    		        new Response.Listener<String>() {
    		
    		            @Override
    		            public void onResponse( String response) {
    		                try {
    		                	//final  String response=r;
    		        			// dismiss the dialog after getting all products

    		        			// updating UI from Background Thread
    		        			runOnUiThread(new Runnable() {

    		        				public void run() {

    		        					if (response.equals("failed")) {
    		        						Toast.makeText(ScreenSlideActivity.this, "Try again",
    		        								Toast.LENGTH_SHORT).show();

    		        					} else if (response.equals("success")) {
    		        						SavedPrefernce prefernce=new SavedPrefernce(ScreenSlideActivity.this);
    		        						prefernce.saveSharedpreferences( user);
    		        						prefernce.setLocationId(locationId);
    		        						prefernce.setLocationName(location);
    		        						
    		        						
    		        						Toast.makeText(ScreenSlideActivity.this,"Your profile updated", Toast.LENGTH_SHORT).show();
    		        						goToSearch();

    		        					} else if (response.equals("NotFound")) {
    		        						Toast.makeText(ScreenSlideActivity.this,"It's seem you deleted saved data",Toast.LENGTH_SHORT).show();

    		        					} else {
    		        						Toast.makeText(ScreenSlideActivity.this, "Try again",
    		        								Toast.LENGTH_SHORT).show();

    		        					}
    		        				}

    		        			});

    		        		
    		                	
    		                } catch (Exception e) {
    		                	e.printStackTrace();
    		                }
    		            }
    		        },
    		        new Response.ErrorListener() {
    		            @Override
    		            public void onErrorResponse(VolleyError error) {
    		                error.printStackTrace();
    		            }
    		        }
    		) {
    		    @Override
    		    protected Map<String, String> getParams()
    		    {
    		        Map<String, String>  params = new HashMap<String, String>();
    		        // the POST parameters:
    		        		params.put("id",  user.ID );
    		        		params.put("username",  user.USERNAME);
    						params.put("name",  user.NAME );
    						params.put("email",  user.EMAIL );
    						params.put("about",  user.ABOUT );
    						params.put("phone",  user.PHONE );
    						params.put("gender",  user.SEX);
    						params.put("country",  user.COUNTRY );
    						params.put("city",  user.CITY );
    						params.put("zip",  user.ZIP);
    						params.put("FavRes",  user.FavRes);
    						params.put("FavFood",  user.FavFood);
    						params.put("FavCus",  user.FavCus);
    						params.put("NoResVisit",  user.NoResVisit);
    						params.put("VisitWith",  user.VisitWith);
    						params.put("VisitTime",  user.VisitTime);
    		      
    		        
    		        
    		        return params;
    		    }
    		};*/
    		
    		
    		//Volley.newRequestQueue(this).add(postRequestUpdateProfile);
    	}
    }
    


    /**
     * A simple pager adapter that represents 5 {@link Q1ChooseCuisine} objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
    	
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
        	Fragment fragment=null;
        	
        	switch (position) {
        	
			case 0:
				  fragment= Q0ZoneExpandable.create(position);				
				break;
			case 1:
				  fragment= Q1ChooseCuisine.create(position);
				break;			
			case 2:
				  fragment= Q5NoOfVisit.create(position);
				break;
			case 3:
				  fragment= Q6VisitMeals.create(position);
				break;			
			case 4:
				  fragment= Q8Partner.create(position);
				break;
			/*case 6:
				  fragment= Q10search.create(position);
				break;*/
			
			}
        	
        	//if(position%2==1)
            return fragment;
        /*	else
            return ScreenSlidePageFragment.create(position);*/
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
    
    
    
    String response;
	User user;

	class EditProfile extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Toast.makeText(ScreenSlideActivity.this,"Wait. updating your profile ", Toast.LENGTH_LONG).show();

		}

		protected String doInBackground(String... args) {

			FoodyResturent foodyResturent = new FoodyResturent();

			try {
				SavedPrefernce prefernce=new SavedPrefernce(ScreenSlideActivity.this);
				user=prefernce.getUser();
				user.FavRes = ScreenSlideActivity.FavRes;
				user.FavFood = ScreenSlideActivity.FavFood;
				user.NoResVisit = ScreenSlideActivity.NoResVisit;
				user.VisitWith = ScreenSlideActivity.VisitWith ;
				user.VisitTime = ScreenSlideActivity.VisitTime;
				user.FavCus = ScreenSlideActivity.CuisineId;
				user.CITY = ScreenSlideActivity.Zone;
				
				response = foodyResturent.editProfile(user);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {

				public void run() {

					if (response.equals("failed")) {
						Toast.makeText(ScreenSlideActivity.this, "Try again",
								Toast.LENGTH_SHORT).show();

					} else if (response.equals("success")) {
						SavedPrefernce prefernce=new SavedPrefernce(ScreenSlideActivity.this);
						prefernce.saveSharedpreferences( user);
						prefernce.setLocationId(locationId);
						prefernce.setLocationName(location);
						
						
						Toast.makeText(ScreenSlideActivity.this,"Your profile updated", Toast.LENGTH_SHORT).show();
						goToSearch();

					} else if (response.equals("NotFound")) {
						Toast.makeText(ScreenSlideActivity.this,"It's seem you deleted saved data",Toast.LENGTH_SHORT).show();

					} else {
						Toast.makeText(ScreenSlideActivity.this, "Try again",
								Toast.LENGTH_SHORT).show();

					}
				}

			});

		}

	}
	
	
	
	
	
	
}
