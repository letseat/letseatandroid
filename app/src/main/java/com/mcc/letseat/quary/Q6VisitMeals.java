/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mcc.letseat.quary;



import com.mcc.letseat.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A fragment representing a single step in a wizard. The fragment shows a dummy title indicating
 * the page number, along with some dummy text.
 */
public class Q6VisitMeals extends Fragment {
	static public String VisitTime;
	
    /**
     * The argument key for the page number this fragment represents.
     */
    public static final String ARG_PAGE = "page";

    /**
     * The fragment's page number, which is set to the argument value for {@link #ARG_PAGE}.
     */
    private int mPageNumber;

    /**
     * Factory method for this fragment class. Constructs a new fragment for the given page number.
     */
    
    CheckBox checkBox1; 
    CheckBox checkBox2; 
    CheckBox checkBox3; 
    CheckBox checkBox4;
    
    public static Q6VisitMeals create(int pageNumber) {
        Q6VisitMeals fragment = new Q6VisitMeals();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public Q6VisitMeals() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout containing a title and body text.
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.activity_q5_meal, container, false);
        
         checkBox1=(CheckBox) rootView.findViewById(R.id.checkBox1); 
         checkBox2=(CheckBox) rootView.findViewById(R.id.checkBox2); 
         checkBox3=(CheckBox) rootView.findViewById(R.id.checkBox3); 
         checkBox4=(CheckBox) rootView.findViewById(R.id.checkBox4); 
         
         checkBox1.setOnCheckedChangeListener(checkedChangeListener);
         checkBox2.setOnCheckedChangeListener(checkedChangeListener);
         checkBox3.setOnCheckedChangeListener(checkedChangeListener);
         checkBox4.setOnCheckedChangeListener(checkedChangeListener);
         
        return rootView;
    }

    /**
     * Returns the page number represented by this fragment object.
     */
    public int getPageNumber() {
        return mPageNumber;
    }
    
    CompoundButton.OnCheckedChangeListener checkedChangeListener=new CompoundButton.OnCheckedChangeListener() {
		 
        @Override
        public void onCheckedChanged(CompoundButton vw, boolean isChecked) {
        	
        	ScreenSlideActivity.VisitTime="";
        	
        	if(checkBox1.isChecked() )
        		ScreenSlideActivity.VisitTime+="1,"; 
        	
        	if(checkBox2.isChecked())
        		ScreenSlideActivity.VisitTime+="2,"; 
        	
        	if(checkBox3.isChecked())
        		ScreenSlideActivity.VisitTime+="3,";
        	
        	if(checkBox4.isChecked())
        		ScreenSlideActivity.VisitTime+="4,";
        	
        //	Toast.makeText(getActivity(), ScreenSlideActivity.VisitTime, Toast.LENGTH_LONG).show();
            

        }
    };
}
