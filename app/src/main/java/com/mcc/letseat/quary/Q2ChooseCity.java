package com.mcc.letseat.quary;



import java.util.ArrayList;
import java.util.HashMap;

import com.foody.jsondata.District;
import com.foody.jsondata.FoodyResturent;
import com.mcc.letseat.R;
import com.mcc.letseat.imageslider.adapter.GridViewCuisineAdapte;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

/**
 * A fragment representing a single step in a wizard. The fragment shows a dummy title indicating
 * the page number, along with some dummy text.

 */
public class Q2ChooseCity extends FragmentForListViewHeader {
	
	static public String DisticId;
    /**
     * The argument key for the page number this fragment represents.
     */
    public static final String ARG_PAGE = "page";

    /**
     * The fragment's page number, which is set to the argument value for {@link #ARG_PAGE}.
     */
    private int mPageNumber;

    /**
     * Factory method for this fragment class. Constructs a new fragment for the given page number.
     */
    
    ListView lv;
    ListViewDistrictAdapter adapter;
	ArrayList<District> districtList;
	
	
	
	
    public static Q2ChooseCity create(int pageNumber) {
        Q2ChooseCity fragment = new Q2ChooseCity();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public Q2ChooseCity() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout containing a title and body text.
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.activity_q2_choose_cuisine, container, false);

        // Set the title view to show the page number.
        /*((TextView) rootView.findViewById(android.R.id.text1)).setText(
                getString(R.string.title_template_step, mPageNumber + 1));*/
        lv =(ListView) rootView.findViewById(R.id.lv_choose_cuisine);
        lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position,long id) {
				
				CheckBox checkBoxChooseRes=(CheckBox) view.findViewById(R.id.checkBoxDistrictLocation);				
				checkBoxChooseRes.setChecked(!checkBoxChooseRes.isChecked());
				
				if(checkBoxChooseRes.isChecked());{
					//ScreenSlideActivity.DistrictId= districtList.get(position-listViewOffset).DisId;
					//ScreenSlideActivity.District= districtList.get(position-listViewOffset).DisName;					
					 ScreenSlideActivity.mPager.setCurrentItem(ScreenSlideActivity.mPager.getCurrentItem() + 1);
				}
				
				
			}
		});
		
		/*View convertView = View.inflate(this, R.layout.cuisine_item, null);
		lv.addHeaderView(convertView);*/
		
		 addHeader(lv, "Choose your City", R.mipmap.back_fav_food, R.mipmap.ic_choose_location );

        return rootView;
    }
    
    @Override
    public void onStart() {
    	// TODO Auto-generated method stub
    	super.onStart();
    	new LoadCuisine().execute();
    }

    /**
     * Returns the page number represented by this fragment object.
     */
    public int getPageNumber() {
        return mPageNumber;
    }
    
    class LoadCuisine extends AsyncTask<String, String, String> {

		

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			// findViewById(R.id.cat_custom_progress).setVisibility(View.VISIBLE);

		}

		/**
		 * getting JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			FoodyResturent foodyResturent = new FoodyResturent();

			try {
				districtList = foodyResturent.getDistricts(getActivity());

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {

			if (getActivity()==null) {
				
				return;
			}

			// updating UI from Background Thread
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status

					if (districtList == null || districtList.size() <= 0) {

						// show message
						/*
						 * findViewById(R.id.cat_custom_progress).setVisibility(View
						 * .VISIBLE); View v =
						 * findViewById(R.id.cat_custom_progress); TextView
						 * textPreparing = (TextView)
						 * v.findViewById(R.id.textPreparing);
						 * textPreparing.setText("Nothing found.");
						 * v.findViewById
						 * (R.id.progressBarPreparing).setVisibility(View.GONE);
						 * AnimationTween.animateViewPosition(v, HomeTest.this);
						 */

					} else if (districtList != null) {
						adapter = new ListViewDistrictAdapter(getActivity(),districtList);

						lv.setAdapter(adapter);
						adapter.notifyDataSetChanged();
						
						//change data loading status
						
					}
				}

			});

		}

	}
}
