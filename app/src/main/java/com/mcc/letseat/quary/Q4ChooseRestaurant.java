package com.mcc.letseat.quary;

import java.util.ArrayList;

import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.Restaurent;
import com.mcc.letseat.R;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class Q4ChooseRestaurant extends FragmentForListViewHeader{
	
	static public String FavRes;
	
	ListView lv;
	ListViewRestaurantAdapter adapter;
	ArrayList<Restaurent> resList;
	
	

    /**
     * The argument key for the page number this fragment represents.
     */
    public static final String ARG_PAGE = "page";

    /**
     * The fragment's page number, which is set to the argument value for {@link #ARG_PAGE}.
     */
    private int mPageNumber;

    /**
     * Factory method for this fragment class. Constructs a new fragment for the given page number.
     */
    public static Q4ChooseRestaurant create(int pageNumber) {
    	Q4ChooseRestaurant fragment = new Q4ChooseRestaurant();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public Q4ChooseRestaurant() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout containing a title and body text.
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.activity_q2_choose_cuisine, container, false);

        lv =(ListView) rootView.findViewById(R.id.lv_choose_cuisine);		
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,long id) {
				CheckBox checkBoxChooseRes=(CheckBox) view.findViewById(R.id.checkBoxChooseRes);				
				checkBoxChooseRes.setChecked(!checkBoxChooseRes.isChecked());
			}
		});
		
		addHeader(lv, "Your Favorite City Restaurant?", R.mipmap.back_fav_food, R.mipmap.ic_restaurant );
		 
        return rootView;
    }
    
    @Override
    public void onStart() {
    	// TODO Auto-generated method stub
    	super.onStart();
    	
    	
    }
    
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
    	// TODO Auto-generated method stub
    	super.setUserVisibleHint(isVisibleToUser);
    	if (isVisibleToUser) {
    		new LoadCuisine().execute();
		}
    }

    /**
     * Returns the page number represented by this fragment object.
     */
    public int getPageNumber() {
        return mPageNumber;
    }

	
	

	
	
	class LoadCuisine extends AsyncTask<String, String, String> {

		

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			// findViewById(R.id.cat_custom_progress).setVisibility(View.VISIBLE);

		}

		/**
		 * getting JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			FoodyResturent foodyResturent = new FoodyResturent();

			try {
				resList = foodyResturent.getRsturent(0,"", 0, getActivity()) ;

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {


			
			if(getActivity()==null)
				return;
			
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status

					if (resList == null || resList.size() <= 0) {

						// show message
						/*
						 * findViewById(R.id.cat_custom_progress).setVisibility(View
						 * .VISIBLE); View v =
						 * findViewById(R.id.cat_custom_progress); TextView
						 * textPreparing = (TextView)
						 * v.findViewById(R.id.textPreparing);
						 * textPreparing.setText("Nothing found.");
						 * v.findViewById
						 * (R.id.progressBarPreparing).setVisibility(View.GONE);
						 * AnimationTween.animateViewPosition(v, HomeTest.this);
						 */

					} else if (resList != null) {
						

						adapter = new ListViewRestaurantAdapter(getActivity(),resList);

						lv.setAdapter(adapter);
						adapter.notifyDataSetChanged();
						
						//change data loading status
						
					}
				}

			});

		}

	}

}
