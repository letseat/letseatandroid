/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mcc.letseat.quary;



import java.util.ArrayList;
import java.util.HashMap;

import com.foody.jsondata.Cusine;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.Location;
import com.mcc.letseat.R;
import com.mcc.letseat.imageslider.adapter.GridViewCuisineAdapte;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

/**
 * A fragment representing a single step in a wizard. The fragment shows a dummy title indicating
 * the page number, along with some dummy text.
 */
public class Q3ChooseLocation extends FragmentForListViewHeader {
	static public String locationId;
	
    /**
     * The argument key for the page number this fragment represents.
     */
    public static final String ARG_PAGE = "page";

    /**
     * The fragment's page number, which is set to the argument value for {@link #ARG_PAGE}.
     */
    private int mPageNumber;

    /**
     * Factory method for this fragment class. Constructs a new fragment for the given page number.
     */
    
    ListView lv;
    ListViewLocationAdapter adapter;
	ArrayList<Location> locationList;
	
	
    public static Q3ChooseLocation create(int pageNumber) {
        Q3ChooseLocation fragment = new Q3ChooseLocation();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public Q3ChooseLocation() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout containing a title and body text.
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.activity_q2_choose_cuisine, container, false);

        // Set the title view to show the page number.
        /*((TextView) rootView.findViewById(android.R.id.text1)).setText(
                getString(R.string.title_template_step, mPageNumber + 1));*/
        lv =(ListView) rootView.findViewById(R.id.lv_choose_cuisine);
        lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position,long id) {
				
				CheckBox checkBoxChooseRes=(CheckBox) view.findViewById(R.id.checkBoxDistrictLocation);				
				checkBoxChooseRes.setChecked(!checkBoxChooseRes.isChecked());
				
				if(checkBoxChooseRes.isChecked());{
//					ScreenSlideActivity.locationId= locationList.get(position-listViewOffset).LocId;
//					ScreenSlideActivity.location= locationList.get(position-listViewOffset).LocName;
//					ScreenSlideActivity.mPager.setCurrentItem(ScreenSlideActivity.mPager.getCurrentItem() + 1);
				}
				
				 
			}
		});
		
		addHeader(lv, "Choose your Location", R.mipmap.back_fav_food, R.mipmap.ic_choose_location );
		
        return rootView;
    }
    
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
     
               
      
    }
    
    @Override
    public void onStart() {
    	// TODO Auto-generated method stub
    	super.onStart();
    	
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
    	// TODO Auto-generated method stub
    	super.setUserVisibleHint(isVisibleToUser);
    	
    	if (isVisibleToUser) {
    		new LoadLocation().execute();
		}
    }
    
    /**
     * Returns the page number represented by this fragment object.
     */
    public int getPageNumber() {
        return mPageNumber;
    }
    
    class LoadLocation extends AsyncTask<String, String, String> {

		

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			// findViewById(R.id.cat_custom_progress).setVisibility(View.VISIBLE);

		}

		/**
		 * getting JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			FoodyResturent foodyResturent = new FoodyResturent();

			try {
				
				//locationList = foodyResturent.getLocation(ScreenSlideActivity.DistrictId, getActivity());

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {

			if(getActivity()==null)
				return;

			
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status

					if (locationList == null || locationList.size() <= 0) {

						// show message
						/*
						 * findViewById(R.id.cat_custom_progress).setVisibility(View
						 * .VISIBLE); View v =
						 * findViewById(R.id.cat_custom_progress); TextView
						 * textPreparing = (TextView)
						 * v.findViewById(R.id.textPreparing);
						 * textPreparing.setText("Nothing found.");
						 * v.findViewById
						 * (R.id.progressBarPreparing).setVisibility(View.GONE);
						 * AnimationTween.animateViewPosition(v, HomeTest.this);
						 */

					} else if (locationList != null) {
						

						adapter = new ListViewLocationAdapter(getActivity(),locationList);

						lv.setAdapter(adapter);
						adapter.notifyDataSetChanged();
						
						//change data loading status
						
					}
				}

			});

		}

	}
}
