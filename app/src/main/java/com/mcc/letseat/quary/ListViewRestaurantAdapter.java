package com.mcc.letseat.quary;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;
import android.widget.Toast;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.Cusine;
import com.foody.jsondata.Restaurent;
import com.foody.nearby.CusineMenuListView;
import com.mcc.letseat.HomeT;
import com.mcc.letseat.R;
import com.mcc.libs.ActivityWithSliding;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Picasso.LoadedFrom;

/**
 * Custom Adapter for Cuisine GridView
 * 
 * @author Arif
 * 
 */
public class ListViewRestaurantAdapter extends BaseAdapter {

	

	String fontPath = "fonts/SEGOEUI.TTF";

	private Activity _activity;
	private ArrayList<Restaurent> resList;
	TypedArray imgs;
	
	

	
	Typeface tf;

	public ListViewRestaurantAdapter(Activity activity,ArrayList<Restaurent> resList) {
		this._activity = activity;
		this.resList = resList;
		
		
		imgs = _activity.getResources().obtainTypedArray(R.array.image_ids);
		
		
		 tf = Typeface.createFromAsset(_activity.getAssets(), fontPath);
	}

	@Override
	public int getCount() {
		return this.resList.size();
	}

	@Override
	public Object getItem(int position) {
		return this.resList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final ViewHolder holder;

		
		
		if(convertView == null){
			try {
				// v = li.inflate(R.layout.cuisine_item, null);
				convertView = View.inflate(_activity, R.layout.list_item_choose_res, null);
			} catch (InflateException e) {
				System.gc();
				// v = li.inflate(R.layout.cuisine_item, null);
				convertView = View.inflate(_activity, R.layout.list_item_choose_res, null);
			}
			
			holder = new ViewHolder();
			holder.img_list_item_choose_res = (ImageView) convertView.findViewById(R.id.img_list_item_choose_res);					
			holder.txtChooseRes = (TextView) convertView.findViewById(R.id.txtChooseRes);
			holder.txtChooseResLoc = (TextView) convertView.findViewById(R.id.txtChooseResLoc);
			holder.checkBoxChooseRes= (CheckBox) convertView.findViewById(R.id.checkBoxChooseRes);
			
			convertView.setTag(holder);
			convertView.setTag(R.id.img_list_item_choose_res, holder.img_list_item_choose_res);
			convertView.setTag(R.id.txtChooseRes, holder.txtChooseRes);
			convertView.setTag(R.id.txtChooseResLoc, holder.txtChooseResLoc);
			convertView.setTag(R.id.checkBoxChooseRes, holder.checkBoxChooseRes);
			
			 holder.checkBoxChooseRes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				 
                 @Override
                 public void onCheckedChanged(CompoundButton vw, boolean isChecked) {

                     int getPosition = (Integer) vw.getTag();
                     resList.get(getPosition).setSelected( vw.isChecked());
                     getSelectedContacts() ;

                 }
             });
			
			

		}else{
			// recycle the already inflated view 
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.checkBoxChooseRes.setTag(position);
		holder.checkBoxChooseRes.setChecked(resList.get(position).isSelected());
		
		
		String resName = resList.get(position).RestName;
		holder.txtChooseRes.setText(resName);
		holder.txtChooseRes.setTypeface(tf);
		
		String resLoc = resList.get(position).RestLoc;
		holder.txtChooseResLoc.setText(resLoc);
		holder.txtChooseResLoc.setTypeface(tf);
		
		holder.img_list_item_choose_res.setScaleType(ScaleType.CENTER_CROP);
		Resources res= _activity.getResources();
		int resId = getRandomResourceID( ((position + 1) % 11));
		
		
		
		
		
		Bitmap bitmap=  decodeBitmapFromResource(res, resId, 480, 200); 
		
		holder.img_list_item_choose_res.setImageDrawable(new BitmapDrawable(res, bitmap));

		String resImgUrl = resList.get(position).IMG;

		//loadBitmap(getRandomResourceID((position + 1) % 11), holder.imageView);
		
		if (resImgUrl != null && !resImgUrl.isEmpty()) {
			Picasso.with(_activity).load(resImgUrl)
					.placeholder(holder.img_list_item_choose_res.getDrawable())
					.error(holder.img_list_item_choose_res.getDrawable())
					.into(holder.img_list_item_choose_res);
		} 

		
		

		

		return convertView;
	}
	
	private static class ViewHolder{
		ImageView img_list_item_choose_res ;		
		
		TextView txtChooseRes ;		
		CheckBox checkBoxChooseRes;
		TextView txtChooseResLoc;
	}
	
	 private void getSelectedContacts() {
	        // TODO Auto-generated method stub
	 
	        StringBuffer sb = new StringBuffer();
	 
	        for (Restaurent bean : resList) {
	 
	            if (bean.isSelected()) {
	                sb.append(bean.Id);
	                sb.append(",");
	            }
	        }
	 
	        String s = sb.toString().trim();
	        ScreenSlideActivity.FavRes=s;
	        if (TextUtils.isEmpty(s)) {
	            Toast.makeText(_activity, "Select atleast one Contact",
	                    Toast.LENGTH_SHORT).show();
	        } else {
	 
	            s = s.substring(0, s.length() - 1);
	            Toast.makeText(_activity, "Selected Contacts : " + s,
	                    Toast.LENGTH_SHORT).show();
	 
	        }
	 
	    }
	
	private int getRandomResourceID(int random) {
		
		int d = 0;
		switch (random) {
		case 1:
			d = R.mipmap.back5;
			break;
		case 2:
			d = R.mipmap.back2;
			break;
		case 3:
			d = R.mipmap.back8;
			break;
		case 4:
			d = R.mipmap.back4;
			break;
		case 5:
			d = R.mipmap.back_login;
			break;
		case 6:
			d = R.mipmap.back6;
			break;
		case 7:
			d = R.mipmap.back7;
			break;
		case 8:
			d = R.mipmap.back3;
			break;
		case 9:
			d = R.mipmap.back9;
			break;
		case 10:
			d = R.mipmap.back10;
			break;
		case 11:
			d = R.mipmap.back11;
			break;

		default:
			d = R.mipmap.back8;
			break;
		}

		return d;
	}
	
	public  int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
    // Raw height and width of image
    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = 1;

    if (height > reqHeight || width > reqWidth) {

        final int halfHeight = height / 2;
        final int halfWidth = width / 2;

        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
        // height and width larger than the requested height and width.
        while ((halfHeight / inSampleSize) > reqHeight
                && (halfWidth / inSampleSize) > reqWidth) {
            inSampleSize *= 2;
        }
    }

    	return inSampleSize;
	}
	
	public  Bitmap decodeBitmapFromResource(Resources res, int resId,
	        int reqWidth, int reqHeight) {

	    // First decode with inJustDecodeBounds=true to check dimensions
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeResource(res, resId, options);

	    // Calculate inSampleSize
	    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	    return BitmapFactory.decodeResource(res, resId, options);
	}
	
	
}
