package com.mcc.letseat.quary;

import java.util.ArrayList;

import android.app.Activity;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.view.InflateException;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.foody.jsondata.Location;
import com.mcc.letseat.R;
import com.squareup.picasso.Picasso;

/**
 * Custom Adapter for ListViewLocationAdapter GridView
 * 
 * @author Arif
 * 
 */
public class ListViewLocationAdapter extends BaseAdapter {

	

	String fontPath = "fonts/SEGOEUI.TTF";

	private Activity _activity;
	private ArrayList<Location> locationList = new ArrayList<Location>();	
	TypedArray imgs;
	
	
	

	
	Typeface tf;

	public ListViewLocationAdapter(Activity activity,
			ArrayList<Location> locationList) {
		this._activity = activity;
		this.locationList = locationList;
		imgs = _activity.getResources().obtainTypedArray(R.array.image_ids);
		
		
		 tf = Typeface.createFromAsset(_activity.getAssets(), fontPath);
	}

	@Override
	public int getCount() {
		return this.locationList.size();
	}

	@Override
	public Object getItem(int position) {
		return this.locationList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

final ViewHolder holder;

		
		
		//LayoutInflater li = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		//v = li.inflate(R.layout.cuisine_item, null);
		if(convertView == null){
			try {
				// v = li.inflate(R.layout.cuisine_item, null);
				convertView = View.inflate(_activity, R.layout.list_item_city_location, null);
			} catch (InflateException e) {
				System.gc();
				// v = li.inflate(R.layout.cuisine_item, null);
				convertView = View.inflate(_activity, R.layout.list_item_city_location, null);
			}
			
			holder = new ViewHolder();
			holder.img_list_item_city_location = (ImageView) convertView.findViewById(R.id.img_list_item_city_location);				
			holder.txtDistictLocation = (TextView) convertView.findViewById(R.id.txtDistictLocation);
			holder.checkBoxDistrictLocation = (CheckBox) convertView.findViewById(R.id.checkBoxDistrictLocation);
			convertView.setTag(holder);
			
			convertView.setTag(R.id.img_list_item_city_location, holder.img_list_item_city_location);
			convertView.setTag(R.id.txtDistictLocation, holder.txtDistictLocation);
			convertView.setTag(R.id.checkBoxDistrictLocation, holder.checkBoxDistrictLocation);
			
			 holder.checkBoxDistrictLocation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				 
                 @Override
                 public void onCheckedChanged(CompoundButton vw, boolean isChecked) {

                     int getPosition = (Integer) vw.getTag();
                     locationList.get(getPosition).setSelected( vw.isChecked());
                     /*if(vw.isChecked()){
                    	 ScreenSlideActivity.DisticId = districtList.get(getPosition).DisId;
                     }*/
                    // getSelectedContacts() ;

                 }
             });

		}else{
			// recycle the already inflated view 
			holder = (ViewHolder) convertView.getTag();
		}
		
		 holder.checkBoxDistrictLocation.setTag(position);
		 holder.checkBoxDistrictLocation.setChecked(locationList.get(position).isSelected());
		
		
		
		holder.txtDistictLocation.setText(locationList.get(position).LocName);
		holder.txtDistictLocation.setTypeface(tf);
		
		holder.img_list_item_city_location.setScaleType(ScaleType.CENTER_CROP);
		Resources res= _activity.getResources();
		int resId = getRandomResourceID( ((position + 1) % 11));
		
		
		
		
		
		Bitmap bitmap=  decodeBitmapFromResource(res, resId, 480, 200); 
		
		holder.img_list_item_city_location.setImageDrawable(new BitmapDrawable(res, bitmap));

		String cusineImgUrl = "";//locationList.get(position).get("IMG");

		//loadBitmap(getRandomResourceID((position + 1) % 11), holder.imageView);
		
		if (cusineImgUrl != null && !cusineImgUrl.isEmpty()) {
			Picasso.with(_activity).load(cusineImgUrl)
					.placeholder(holder.img_list_item_city_location.getDrawable())
					.error(holder.img_list_item_city_location.getDrawable())
					.into(holder.img_list_item_city_location);
		} 

		
		


		return convertView;
	}
	
	private static class ViewHolder{
		public CheckBox checkBoxDistrictLocation;
		public TextView txtDistictLocation;
		public ImageView img_list_item_city_location ;		
	
	}
	
	private int getRandomResourceID(int random) {
		
		int d = 0;
		switch (random) {
		case 1:
			d = R.mipmap.back5;
			break;
		case 2:
			d = R.mipmap.back2;
			break;
		case 3:
			d = R.mipmap.back8;
			break;
		case 4:
			d = R.mipmap.back4;
			break;
		case 5:
			d = R.mipmap.back_login;
			break;
		case 6:
			d = R.mipmap.back6;
			break;
		case 7:
			d = R.mipmap.back7;
			break;
		case 8:
			d = R.mipmap.back3;
			break;
		case 9:
			d = R.mipmap.back9;
			break;
		case 10:
			d = R.mipmap.back10;
			break;
		case 11:
			d = R.mipmap.back11;
			break;

		default:
			d = R.mipmap.back8;
			break;
		}

		return d;
	}
	
	public  int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
    // Raw height and width of image
    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = 1;

    if (height > reqHeight || width > reqWidth) {

        final int halfHeight = height / 2;
        final int halfWidth = width / 2;

        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
        // height and width larger than the requested height and width.
        while ((halfHeight / inSampleSize) > reqHeight
                && (halfWidth / inSampleSize) > reqWidth) {
            inSampleSize *= 2;
        }
    }

    	return inSampleSize;
	}
	
	public  Bitmap decodeBitmapFromResource(Resources res, int resId,
	        int reqWidth, int reqHeight) {

	    // First decode with inJustDecodeBounds=true to check dimensions
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeResource(res, resId, options);

	    // Calculate inSampleSize
	    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	    return BitmapFactory.decodeResource(res, resId, options);
	}
	
	
}
