/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mcc.letseat.quary;

import com.mcc.letseat.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A fragment representing a single step in a wizard. The fragment shows a dummy
 * title indicating the page number, along with some dummy text.
 * 
 * <p>
 * This class is used by the {@link CardFlipActivity} and
 * {@link ScreenSlideActivity} samples.
 * </p>
 */
public class Q10search extends Fragment {
	private TextView txtSearch;

	/**
	 * The argument key for the page number this fragment represents.
	 */
	public static final String ARG_PAGE = "page";

	/**
	 * The fragment's page number, which is set to the argument value for
	 * {@link #ARG_PAGE}.
	 */
	private int mPageNumber;

	/**
	 * Factory method for this fragment class. Constructs a new fragment for the
	 * given page number.
	 */
	public static Q10search create(int pageNumber) {
		Q10search fragment = new Q10search();
		Bundle args = new Bundle();
		args.putInt(ARG_PAGE, pageNumber);
		fragment.setArguments(args);
		return fragment;
	}

	public Q10search() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mPageNumber = getArguments().getInt(ARG_PAGE);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout containing a title and body text.
		ViewGroup rootView = (ViewGroup) inflater.inflate(
				R.layout.activity_q10_search_for, container, false);

		txtSearch = (TextView) rootView.findViewById(R.id.txtSearch);

		return rootView;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		txtSearch.setText(

		ScreenSlideActivity.ZoneId + "\n" + ScreenSlideActivity.Zone + "\n"
				+ ScreenSlideActivity.FavFood + "\n"
				+ ScreenSlideActivity.NoResVisit + "\n"
				+ ScreenSlideActivity.VisitWith + "\n"
				+ ScreenSlideActivity.VisitTime + "\n"
				+ ScreenSlideActivity.CuisineId);
	}

	/**
	 * Returns the page number represented by this fragment object.
	 */
	public int getPageNumber() {
		return mPageNumber;
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		// TODO Auto-generated method stub
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser) {
			txtSearch.setText(

			ScreenSlideActivity.ZoneId + "\n" + ScreenSlideActivity.Zone + "\n"
					+ ScreenSlideActivity.FavFood + "\n"
					+ ScreenSlideActivity.NoResVisit + "\n"
					+ ScreenSlideActivity.VisitWith + "\n"
					+ ScreenSlideActivity.VisitTime + "\n"
					+ ScreenSlideActivity.CuisineId);
		}
	}
}
