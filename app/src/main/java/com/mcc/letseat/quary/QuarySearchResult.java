package com.mcc.letseat.quary;

import java.util.ArrayList;
import java.util.HashMap;
import android.widget.AbsListView;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.SearchResturent;
import com.foody.nearby.AlertDialogManager;
import com.foody.nearby.ListAdapterRestaurantImage;
import com.foody.nearby.MyListAdapter;
import com.foody.nearby.ResturentListViewImage;
import com.foody.nearby.SearchAdvResturentListView;
import com.foody.nearby.SuggetionRequest;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.mcc.extensions.GPSTracker;
import com.mcc.letseat.CapturePhotoActivity;
import com.mcc.letseat.R;
import com.mcc.letseat.tab.TabMainActivity;
import com.mcc.libs.ActivityWithSliding;
import com.mcc.libs.AnimationTween;
import com.mcc.libs.CF;
import com.mcc.user.SavedPrefernce;

/**
 * Search result ListView use in Search and advance Search
 */
public class QuarySearchResult extends Activity implements
		ActionBar.OnNavigationListener   {

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Google Places
	FoodyResturent foodyResturent;

	// Places List
	ArrayList<SearchResturent> resturentList;

	// GPS Location
	GPSTracker gps;

	// Button
	ImageView btnShowOnMap;

	

	// Places Listview
	ListView lv;

	// ListItems data
	ArrayList<HashMap<String, String>> placesListItems = new ArrayList<HashMap<String, String>>();

	// //admob
	AdView adView = null;


	String SearchString, SearchLocation, SearchLocationId;
	
	TextView txtSearchTitle;
	TextView txtSearchMetaData;
	TextView txtSearchLocation;
	TextView txtSearchString;
	
	boolean onlySearchString, onlyLocation;
	 ViewGroup header; 
	 RelativeLayout listSearchResultHeaderHolder;
	 
	 //as header added list index offset is 1
	 int listoffset = 1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.slide_up_in,R.anim.stay);
		
		setContentView(R.layout.activity_q_result);
		
		
		
		
		
		// Getting listview
		lv = (ListView) findViewById(R.id.list_search_result);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				SearchResturent resturent = (SearchResturent) resturentList.get(position-listoffset);
				
				
				
					
					Intent in = new Intent(getApplicationContext(),
							TabMainActivity.class);
					in.putExtra("name", resturent.Name);
					in.putExtra("address", resturent.Address);
					in.putExtra("imageLink", resturent.IMG);
					in.putExtra("Id",resturent.Id);

					startActivity(in);
				

				
			}
		});
		
		// inflate custom header and attach it to the list
        LayoutInflater inflater = getLayoutInflater();
         header = (ViewGroup) inflater.inflate(R.layout.list_search_result_header, lv, false);
         listSearchResultHeaderHolder= (RelativeLayout) header.findViewById(R.id.listSearchResultHeaderHolder);
        lv.addHeaderView(header, null, false);
        lv.setOnScrollListener(onScrollListener);
		
        txtSearchTitle= (TextView) header.findViewById(R.id.txtSearchTitle);
		txtSearchMetaData= (TextView) header.findViewById(R.id.txtSearchMetaData);
		txtSearchLocation= (TextView) header.findViewById(R.id.txtSearchLocation);
		txtSearchString= (TextView) header.findViewById(R.id.txtSearchString);
        
        txtSearchLocation.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				 types = "Search=" + SearchString+"&LocId="+SearchLocationId;
				 onlyLocation=true; onlySearchString=false ;
				 findViewById(R.id.activity_q_result_custom_progress).setVisibility(View.VISIBLE);
					new LoadSearchRestaurant().execute(types);
			}
		});
		
		txtSearchString.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				 types = "Search=" + SearchString;
				 onlyLocation=false; onlySearchString=true ;
				 findViewById(R.id.activity_q_result_custom_progress).setVisibility(View.VISIBLE);
					new LoadSearchRestaurant().execute(types);
			}
		});

		adView = (AdView) this.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder().build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});

		try {
			handleIntents(getIntent());	
		} catch (Exception e) {
			// TODO: handle exception
		}
		

	}
	
	  private int lastTopValue = 0;
	AbsListView.OnScrollListener  onScrollListener = new OnScrollListener() {
		
		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {
			 Rect rect = new Rect();
			 listSearchResultHeaderHolder.getLocalVisibleRect(rect);
		        if (lastTopValue != rect.top) {
		            lastTopValue = rect.top;
		            listSearchResultHeaderHolder.setY((float) (rect.top / 2.0));
		        }
			
		}
	};
	
	
	
	
	
/*	@Override
	public boolean onCreateOptionsMenu(Menu menu ) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate( R.menu.activity_main, menu );
		menu.findItem(R.id.action_search).setVisible(false);
		
	    return true;
	}*/

	private void handleIntents(Intent intent) {
		
			
			SearchString = intent.getStringExtra("SearchString");
			SearchLocation = intent.getStringExtra("SearchLocation");
			SearchLocationId = intent.getStringExtra("SearchLocationId");
			
			txtSearchTitle.setText( SearchString +" in "+SearchLocation);
			txtSearchLocation.setText( "Show All In "+ SearchLocation);
			txtSearchString.setText( "Show All Sale "+ SearchString);
			
			
			 types = "Search=" + SearchString+"&LocId="+SearchLocationId;
			new LoadSearchRestaurant().execute(types);
			//txtTitleNearBy.setText("Showing results for: ");
		
		
		
        
       
        /*Spannable WordToSpan = new SpannableString("Did you mean ");        
        WordToSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#333333")), 0, WordToSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtSearchTitle.setText(WordToSpan);
        
        Spannable WordToSpan1 = new SpannableString("'"+SearchString+"'");        
        WordToSpan1.setSpan(new ForegroundColorSpan(Color.parseColor("#BE1010")), 0, WordToSpan1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtSearchTitle.append(WordToSpan1);*/

	}

	

	

	@Override
	public void onPause() {
		super.onPause();
		overridePendingTransition(R.anim.stay,R.anim.slide_down_out);
		if (adView != null)
			adView.pause();
		
		//overridePendingTransition(0,R.anim.view_close_translate);

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		
		if (adView != null)
			{adView.destroy();}
		
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}
	
	@Override
	protected void onStart() {	
		super.onStart();
		
		//analytics stop tracking
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}
	
	String types;
	class LoadSearchRestaurant extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			findViewById(R.id.activity_q_result_custom_progress).setVisibility(View.VISIBLE);
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {
				
				if(onlyLocation && !onlySearchString){					
					resturentList = foodyResturent.searchAdv("", "", SearchLocationId, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
				}else if(!onlyLocation && onlySearchString){
					
					
					resturentList = foodyResturent.search("Search=" + SearchString);
				}else{
					resturentList = foodyResturent.searchAdv(SearchString, "", SearchLocationId, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
				}
				
				
				

				


				//resturentList = foodyResturent.search(args[0]);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			//findViewById(R.id.activity_q_result_custom_progress).setVisibility(View.GONE);
			AnimationTween.animateView(findViewById(R.id.activity_q_result_custom_progress), QuarySearchResult.this);
			AnimationTween.animateViewPosition(lv, QuarySearchResult.this);
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status

					if (resturentList == null || resturentList.size() <= 0) {

						//show popup for sugest request
						showPopUpAdvSearch();
						
						 
					} else if (resturentList != null) {
						// Check for all possible status

						// Successfully got places details
						if (resturentList != null) {
							ArrayList<SearchResturent> searchRestList= new ArrayList<SearchResturent>(); 
							placesListItems = new ArrayList<HashMap<String, String>>();
							// loop through each place
							for (SearchResturent p : resturentList) {

								HashMap<String, String> map = new HashMap<String, String>();
								map.put("Id", p.Id);
								

								map.put("Id", p.Id);
								map.put("RestName", p.Name);
								map.put("Rating", "3");
								map.put("DisFrom", p.Address);
								
								map.put("DisAmount", "1");
								map.put("IMG", p.IMG);

								// adding HashMap to ArrayList
								placesListItems.add(map);
								
								//search suggestion
								//searchRestList.add(new SearchResturent(p.Id, p.Name, null, p.Address, null, null, null));
							}
							
							//search suggestion
							//addDataToDB(searchRestList);
							
							// list adapter
						/*	MyListAdapter adapter = new MyListAdapter(
									QuarySearchResult.this,
									placesListItems,new String[] {  "Name", "Address","Id" });*/
							ListAdapterRestaurantImage adapter= new ListAdapterRestaurantImage(QuarySearchResult.this, placesListItems, new String[]{"RestName","DisFrom","Rating","DisAmount","IMG"});


							
							

							if(onlyLocation && !onlySearchString){
								
								txtSearchMetaData.setText(placesListItems.size()+" restaurant in "+SearchLocation);
								txtSearchTitle.setText( SearchLocation);
								
							}else if(!onlyLocation && onlySearchString){
								
								txtSearchMetaData.setText(placesListItems.size()+" restaurant sale "+SearchString);
								txtSearchTitle.setText( SearchString);
								
							}else{
								
								txtSearchMetaData.setText(placesListItems.size()+" restaurant sale "+SearchString+ " in "+SearchLocation);
								txtSearchTitle.setText( SearchString +" in "+SearchLocation);
								
							}
							
							// Adding data into listview
							lv.setAdapter(adapter);
							adapter.notifyDataSetChanged();
							
						}

						else if (resturentList == null) {
							//showPopUpAdvSearch();
							txtSearchMetaData.setText("No restaurant sale "+SearchString+ " in "+SearchLocation);
							
							showPopUp();
						}
					}
					
					onlyLocation = onlySearchString = false;
				}

			});

		}

	}
	
	

	@Override
	public boolean onNavigationItemSelected(int arg0, long arg1) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	private void showPopUp() {

		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
		// helpBuilder.setTitle("Pop Up");
		helpBuilder.setMessage("Nothing Found. Want to request this suggestion?");
		helpBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						finish();
						Intent in = new Intent(
								getApplicationContext(),
								SuggetionRequest.class);
						in.putExtra("keyword", SearchString);

						startActivity(in);
					}
				});

		helpBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
						
					}
				});

		// Remember, create doesn't show the dialog
		AlertDialog helpDialog = helpBuilder.create();
		
		if(!(this.isFinishing()))
		{
		    //show dialog
			helpDialog.show(); 
		}

	}
	
	private void showPopUpAdvSearch() {

		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
		// helpBuilder.setTitle("Pop Up");
		helpBuilder.setMessage("No matches found. Try advanced search.");
		helpBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						finish();
						Intent in = new Intent(getApplicationContext(),SearchAdvResturentListView.class);
						
						in.putExtra(StaticObjects.NEV_TYPE, getIntent().getStringExtra(StaticObjects.NEV_TYPE));
						in.putExtra("searchKeyWord_home", SearchString);

						startActivity(in);
					}
				});

		helpBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						showPopUp();
						//finish();
						
					}
				});

		// Remember, create doesn't show the dialog
		AlertDialog helpDialog = helpBuilder.create();
		if(!(this.isFinishing()))
		{
		    //show dialog
			helpDialog.show();
		}
		

	}

}