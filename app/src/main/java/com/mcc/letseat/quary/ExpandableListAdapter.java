package com.mcc.letseat.quary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.james.mime4j.io.PositionInputStream;

import com.foody.jsondata.Zone;
import com.mcc.letseat.R;
import com.mcc.user.SavedPrefernce;
import com.squareup.picasso.Picasso;
 
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
 
public class ExpandableListAdapter extends BaseExpandableListAdapter {
 
    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;
    private ArrayList<Zone> zoneList;
    
    
    //color array #6699cc 009966 ff9933 cc3366
 
    public ExpandableListAdapter(Context context,   ArrayList<Zone> zoneList, List<String> listDataHeader,
            HashMap<String, List<String>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.zoneList = zoneList;
    }
 
    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, final int childPosition,
            boolean isLastChild, View convertView, ViewGroup parent) {
 
        final String childText = (String) getChild(groupPosition, childPosition);
 
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.expandable_list_item, null);
        }
 
        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.lblListItem);
 
        txtListChild.setText(childText);
        return convertView;
    }
 
    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }
 
    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }
 
    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }
 
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
            View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.expandable_list_group, null);
        }
 
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText("");

        TextView lblListHeader1 = (TextView) convertView.findViewById(R.id.txtZone);
       // lblListHeader1.setTypeface(null, Typeface.BOLD);
        lblListHeader1.setText(headerTitle);
        
       /* TextView txtAreaNearby = (TextView) convertView.findViewById(R.id.txtAreaNearby);
      //  txtAreaNearby.setTypeface(null, Typeface.BOLD);
        txtAreaNearby.setText("Near by area "+ zoneList.get(groupPosition).LocName);*/
        
       // ImageView imageViewStaticMap=(ImageView) convertView.findViewById(R.id.imageViewStaticMap);
        
       /* String lat = zoneList.get(groupPosition).Lat, lng = zoneList.get(groupPosition).Lng, zoom="14", width="400", height="350"; 
        String path="https://maps.googleapis.com/maps/api/staticmap?center="+lat+","+lng+"&zoom="+zoom+"&size="+width+"x"+height;
        Picasso.with(_context).load(path)
		.placeholder(imageViewStaticMap.getDrawable())
		.error(imageViewStaticMap.getDrawable())
		.into(imageViewStaticMap);
        */
        ImageView imageViewIndicator=(ImageView) convertView.findViewById(R.id.imageViewIndicator);
        imageViewIndicator.setImageResource(isExpanded?android.R.drawable.arrow_up_float:android.R.drawable.arrow_down_float);
       
       
        
       /* //#6699cc 009966 ff9933 cc3366
        RelativeLayout layout=(RelativeLayout) convertView.findViewById(R.id.rl_expand_list_group_holder);
        
        if(groupPosition%4==0){
        	layout.setBackgroundColor(Color.parseColor("#6699cc"));
        }else if(groupPosition%4==1){
        	layout.setBackgroundColor(Color.parseColor("#009966"));
        }else if(groupPosition%4==2){
        	layout.setBackgroundColor(Color.parseColor("#ff9933"));
        }else if(groupPosition%4==3){
        	layout.setBackgroundColor(Color.parseColor("#cc3366"));
        }*/
        
        /*final CheckBox checkZone = (CheckBox)convertView.findViewById(R.id.checkZone);
        
        if(zoneList.get(groupPosition).ZoneId.equals(new SavedPrefernce((Activity)_context).getZoneID())){
        	checkZone.setChecked(true);
        }*/
        
        /*layout.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				checkZone.setChecked(!checkZone.isChecked());
				 if(checkZone.isChecked()){
					   //Toast.makeText(_context,"Checked", Toast.LENGTH_SHORT).show();
					
				 }
				
			}
		});
        
      
        checkZone.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 CheckBox box=(CheckBox) v;
				 if(box.isChecked()){
					   //Toast.makeText(_context,"Checked",   Toast.LENGTH_SHORT).show();
				 }
				
			}
		});*/
        		
 
        return convertView;
    }
 
    @Override
    public boolean hasStableIds() {
        return false;
    }
 
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}