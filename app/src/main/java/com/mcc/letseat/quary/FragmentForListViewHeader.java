/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mcc.letseat.quary;




import com.foody.AppData.StaticObjects;
import com.mcc.letseat.R;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

public class FragmentForListViewHeader extends Fragment {
	
	//if header added offset is 1 
		int listViewOffset=0;
   
	void addHeader(ListView listView,String headerText, int backImageResourceId, int iconImageResourceId ){
		//change offset as header is added
		listViewOffset=1;
		
		LayoutInflater li = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = li.inflate(R.layout.listview_header, null);

		ImageView iconImageView=(ImageView)v.findViewById(R.id.listview_header_image);
		ImageView backImageView=(ImageView)v.findViewById(R.id.listview_header_back_image);
		TextView textView =(TextView)v.findViewById(R.id.listview_header_txt);;
		
		//String cusineType= _filePaths.get(position).get(listTags[1]);
		
		textView.setText(headerText);
		textView.setTypeface(StaticObjects.gettfNormal(getActivity()));
		iconImageView.setScaleType(ScaleType.CENTER_CROP);
		
		
		if(headerText!=null && !headerText.isEmpty()){
			
			Picasso.with(getActivity())
		    .load(backImageResourceId)
		    .placeholder(R.drawable.sample_res_photo)
		    .error(R.drawable.sample_res_photo)
		    .into(backImageView);
			
			Picasso.with(getActivity())
		    .load(iconImageResourceId)
		    .placeholder(R.drawable.sample_res_photo)
		    .error(R.drawable.sample_res_photo)
		    .into(iconImageView);
			
		}
		
		listView.addHeaderView(v, "header", false);
		//adjustPosition=1;
	}
}
