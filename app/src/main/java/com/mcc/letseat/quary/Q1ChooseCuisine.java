package com.mcc.letseat.quary;

import java.util.ArrayList;
import java.util.HashMap;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.Cusine;
import com.foody.jsondata.FoodyResturent;

import com.mcc.letseat.R;
import com.mcc.letseat.imageslider.adapter.GridViewCuisineAdapte;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.Toast;

public class Q1ChooseCuisine extends FragmentForListViewHeader{
	
	public static  String CuisineId ;
	
	ListView lv;
	ListViewCuisineAdapte adapter;
	ArrayList<Cusine> cusineList;
	
	

    /**
     * The argument key for the page number this fragment represents.
     */
    public static final String ARG_PAGE = "page";

    /**
     * The fragment's page number, which is set to the argument value for {@link #ARG_PAGE}.
     */
    private int mPageNumber;

    /**
     * Factory method for this fragment class. Constructs a new fragment for the given page number.
     */
    public static Q1ChooseCuisine create(int pageNumber) {
    	Q1ChooseCuisine fragment = new Q1ChooseCuisine();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public Q1ChooseCuisine() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout containing a title and body text.
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.activity_q2_choose_cuisine, container, false);
        
       

        lv =(ListView) rootView.findViewById(R.id.lv_choose_cuisine);	
        lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position,long id) {
				CheckBox checkBoxCusine=(CheckBox) view.findViewById(R.id.checkBoxCusine);
				
				 checkBoxCusine.setChecked(!checkBoxCusine.isChecked());
				 Log.e("list item", "==================================================================");
				 //Toast.makeText(getActivity(), "Click"+position, Toast.LENGTH_SHORT).show();
				 
				 /*TextView txtCusine = (TextView) view.findViewById(R.id.txtCusine);
				 txtCusine.setText("");*/
			}
		});
		
		// addHeader(lv, "What is your favorite cuisine?", R.drawable.back_fav_food, R.drawable.ic_cuisine_list );

        return rootView;
    }
    
    
    
    @Override
    public void onStart() {
    	// TODO Auto-generated method stub
    	super.onStart();
    	
    	
    }

    /**
     * Returns the page number represented by this fragment object.
     */
    public int getPageNumber() {
        return mPageNumber;
    }

    
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
    	// TODO Auto-generated method stub
    	super.setUserVisibleHint(isVisibleToUser);
    	
    	if(isVisibleToUser){
    		new LoadCuisine().execute();
    	}
    }
	
	

	
	
	class LoadCuisine extends AsyncTask<String, String, String> {

		private static final String LocId = "13";

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			// findViewById(R.id.cat_custom_progress).setVisibility(View.VISIBLE);

		}

		/**
		 * getting JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			FoodyResturent foodyResturent = new FoodyResturent();

			try {
				cusineList = foodyResturent.getCusine(0,"", getActivity());

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {

			/*
			 * AnimationTween.animateView(findViewById(R.id.cat_custom_progress),
			 * CusineListView.this); AnimationTween.animateViewPosition(lv,
			 * CusineListView.this);
			 */
			if(getActivity()==null)
				return;
			// updating UI from Background Thread
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status

					if (cusineList == null || cusineList.size() <= 0) {

						// show message
						/*
						 * findViewById(R.id.cat_custom_progress).setVisibility(View
						 * .VISIBLE); View v =
						 * findViewById(R.id.cat_custom_progress); TextView
						 * textPreparing = (TextView)
						 * v.findViewById(R.id.textPreparing);
						 * textPreparing.setText("Nothing found.");
						 * v.findViewById
						 * (R.id.progressBarPreparing).setVisibility(View.GONE);
						 * AnimationTween.animateViewPosition(v, HomeTest.this);
						 */

					} else if (cusineList != null) {
						
						adapter = new ListViewCuisineAdapte(getActivity(),cusineList);
						lv.setAdapter(adapter);
						adapter.notifyDataSetChanged();
						
						//change data loading status
						
					}
				}

			});

		}

	}

}
