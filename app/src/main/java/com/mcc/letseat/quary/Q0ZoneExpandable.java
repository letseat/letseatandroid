package com.mcc.letseat.quary;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.foody.AppData.StaticObjects;
import com.foody.jsondata.DistrictLocZone;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.LetsEatRequest;
import com.foody.jsondata.Location;
import com.mcc.letseat.R;
import com.mcc.user.SavedPrefernce;
import com.squareup.picasso.Picasso;
 
public class Q0ZoneExpandable extends FragmentForListViewHeader { 
    ExpandableListAdapterAll listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<Location>> listDataChild;
    private int lastExpandedPosition = -1;
 
    private View rootView;

    /**
     * The argument key for the page number this fragment represents.
     */
    public static final String ARG_PAGE = "page";

    /**
     * The fragment's page number, which is set to the argument value for {@link #ARG_PAGE}.
     */
    private int mPageNumber;

    /**
     * Factory method for this fragment class. Constructs a new fragment for the given page number.
     */
    public static Q0ZoneExpandable create(int pageNumber) {
    	Q0ZoneExpandable fragment = new Q0ZoneExpandable();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public Q0ZoneExpandable() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       
        // Inflate the layout containing a title and body text.
         rootView = (View) inflater.inflate(R.layout.expandable_list_activity, container, false);
 
        // get the listview
        expListView = (ExpandableListView) rootView.findViewById(R.id.lvExp);
        expListView.setIndicatorBounds(345,375);
        expListView.setGroupIndicator(null);
        
        //add header
        addHeader(expListView, "Choose your Location", R.mipmap.back_fav_food, R.mipmap.ic_choose_location );
 
        // preparing list data
        prepareListData();
 
      
 
        // Listview Group click listener
        expListView.setOnGroupClickListener(new OnGroupClickListener() {
 
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                    int groupPosition, long id) {
            	
            	  
                // Toast.makeText(getActivity(),"Group Clicked " +  ScreenSlideActivity.ZoneId+ listDataHeader.get(groupPosition), Toast.LENGTH_SHORT).show();
                return false;
            }
        });
 
      
        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new OnGroupExpandListener() {
 
            @Override
            public void onGroupExpand(int groupPosition) {
            	if (lastExpandedPosition != -1 && groupPosition != lastExpandedPosition) {
            		expListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            		
              
                //Toast.makeText(getActivity(), listDataHeader.get(groupPosition) + " Expanded"+ zoneList.get(groupPosition).ZoneId,Toast.LENGTH_SHORT).show();
            }
        });
 
        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new OnGroupCollapseListener() {
 
            @Override
            public void onGroupCollapse(int groupPosition) {
                //Toast.makeText(getActivity(), listDataHeader.get(groupPosition) + " Collapsed",Toast.LENGTH_SHORT).show();
 
            }
        });
 
        // Listview on child click listener
        expListView.setOnChildClickListener(new OnChildClickListener() {
 
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                    int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
            	View view= expListView.getExpandableListAdapter().getChildView(groupPosition, childPosition, false, v, parent);
                CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkboxLocation);
                checkBox.setChecked(true);
          	
          	 SavedPrefernce p= new SavedPrefernce(getActivity());
               p.setDistrictID(zoneList.get(groupPosition).DisId);
               p.setDistrict(zoneList.get(groupPosition).DisName);
               p.setLocationId(zoneList.get(groupPosition).locations.get(childPosition).LocId);
               p.setLocationName(zoneList.get(groupPosition).locations.get(childPosition).LocName);
               
               ScreenSlideActivity.locationId = zoneList.get(groupPosition).locations.get(childPosition).LocId;
        	   ScreenSlideActivity.location = zoneList.get(groupPosition).locations.get(childPosition).LocName;
        	  /* CheckBox checkZone = (CheckBox)v.findViewById(R.id.checkZone);
        		checkZone.setChecked(!checkZone.isChecked());*/
        		ScreenSlideActivity.mPager.setCurrentItem(ScreenSlideActivity.mPager.getCurrentItem()+1);
             
               
                return false;
            }  
        });
        
        return rootView;
    }
    
  //if header added offset is 1 
  		int listViewOffset=0;
     
  	void addHeader(ListView listView,String headerText, int backImageResourceId, int iconImageResourceId ){
  		//change offset as header is added
  		listViewOffset=1;
  		
  		LayoutInflater li = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  		View v = li.inflate(R.layout.listview_header, null);

  		ImageView iconImageView=(ImageView)v.findViewById(R.id.listview_header_image);
  		ImageView backImageView=(ImageView)v.findViewById(R.id.listview_header_back_image);
  		TextView textView =(TextView)v.findViewById(R.id.listview_header_txt);;
  		
  		//String cusineType= _filePaths.get(position).get(listTags[1]);
  		
  		textView.setText(headerText);
  		textView.setTypeface(StaticObjects.gettfNormal(getActivity()));
  		iconImageView.setScaleType(ScaleType.CENTER_CROP);
  		
  		
  		if(headerText!=null && !headerText.isEmpty()){
  			
  			Picasso.with(getActivity())
  		    .load(backImageResourceId)
  		    .placeholder(R.drawable.sample_res_photo)
  		    .error(R.drawable.sample_res_photo)
  		    .into(backImageView);
  			
  			Picasso.with(getActivity())
  		    .load(iconImageResourceId)
  		    .placeholder(R.drawable.sample_res_photo)
  		    .error(R.drawable.sample_res_photo)
  		    .into(iconImageView);
  			
  		}
  		
  		listView.addHeaderView(v, "header", false);
  		//adjustPosition=1;
  	}
 
    /*
     * Preparing the list data
     */
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new  HashMap<String, List<Location>> ();
        
      
      //  new Loadzone().execute();
        rootView.findViewById(R.id.expanadble_list_progress).setVisibility(View.VISIBLE);        
        //Add the request to the queue
        Volley.newRequestQueue(getActivity()).add(stringRequest);
    }
    
    
    
    ArrayList<DistrictLocZone> zoneList;
    class Loadzone extends AsyncTask<String, String, String> {

		private static final String disId = "1";

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			rootView.findViewById(R.id.expanadble_list_progress).setVisibility(View.VISIBLE);

		}

		/**
		 * getting JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			FoodyResturent foodyResturent = new FoodyResturent();

			try {
				zoneList = foodyResturent.getDistrictsLocZone( getActivity());

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {

			/*
			 * AnimationTween.animateView(findViewById(R.id.cat_custom_progress),
			 * CusineListView.this); AnimationTween.animateViewPosition(lv,
			 * CusineListView.this);
			 */
			if(getActivity()==null)
				return;
			// updating UI from Background Thread
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status

					if (zoneList == null || zoneList.size() <= 0) {

						

					} else if (zoneList != null) {
						
						for (DistrictLocZone z : zoneList) {
							 listDataHeader.add(z.DisName);
							 //List<String> zoneLoc =  new ArrayList<String>(Arrays.asList(z.LocName.split(",")));
							 listDataChild.put(z.DisName, z.locations);
						}
						
						  listAdapter = new ExpandableListAdapterAll(getActivity(), zoneList, listDataHeader, listDataChild);						  
					      //setting list adapter
					       expListView.setAdapter(listAdapter);
					       listAdapter.notifyDataSetChanged();
					       
					       rootView.findViewById(R.id.expanadble_list_progress).setVisibility(View.GONE);
						
						  
						
					}
				}

			});

		}

	}
    
    //volley
    // Request a string response
    		StringRequest stringRequest = new StringRequest(Request.Method.POST, LetsEatRequest.DIS_LOC_ZONE_LIST_URL,
    		            new Response.Listener<String>() {
    		    @Override
    		    public void onResponse(String response) {
    		 
    		       LetsEatRequest ler= new LetsEatRequest();
    		       zoneList = ler.getDistrictsLocZone(response);
    		       
   				if (zoneList == null || zoneList.size() <= 0) {

   					

   				} else if (zoneList != null) {
   					
   					for (DistrictLocZone z : zoneList) {
   						 listDataHeader.add(z.DisName);
   						 //List<String> zoneLoc =  new ArrayList<String>(Arrays.asList(z.LocName.split(",")));
   						 listDataChild.put(z.DisName, z.locations);
   					}
   					
   					  listAdapter = new ExpandableListAdapterAll(getActivity(), zoneList, listDataHeader, listDataChild);						  
   				      //setting list adapter
   				       expListView.setAdapter(listAdapter);
   				       listAdapter.notifyDataSetChanged();
   				       
   				       rootView.findViewById(R.id.expanadble_list_progress).setVisibility(View.GONE);
   					
   					  
   					
   				}
   			
    		 
    		    }
    		}, new Response.ErrorListener() {
    		    @Override
    		    public void onErrorResponse(VolleyError error) {
    		         
    		        
    		 
    		    }
    		});
    
}
