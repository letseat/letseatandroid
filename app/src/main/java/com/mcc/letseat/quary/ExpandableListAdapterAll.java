package com.mcc.letseat.quary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.DistrictLocZone;
import com.foody.jsondata.Location;
import com.mcc.letseat.R;
import com.mcc.user.SavedPrefernce;

public class ExpandableListAdapterAll extends BaseExpandableListAdapter {

	private Context _context;
	// private List<String> _listDataHeader; // header titles
	// child data in format of header title, child title
	// private HashMap<String, List<Location>> _listDataChild;
	private ArrayList<DistrictLocZone> disLocZoneList;
	private ArrayList<DistrictLocZone> disLocZoneListOrigin;

	Typeface tfLight, tfNormal;

	// color array #6699cc 009966 ff9933 cc3366

	public ExpandableListAdapterAll(Context context,
			ArrayList<DistrictLocZone> zoneList, List<String> listDataHeader,
			HashMap<String, List<Location>> listChildData) {
		this._context = context;
		// this._listDataHeader = listDataHeader;
		// this._listDataChild = listChildData;

		this.disLocZoneList = new ArrayList<DistrictLocZone>();
		this.disLocZoneList.addAll(zoneList);

		this.disLocZoneListOrigin = new ArrayList<DistrictLocZone>();
		this.disLocZoneListOrigin.addAll(zoneList);

		try {
			tfLight = Typeface.createFromAsset(context.getAssets(),
					StaticObjects.fontPath_SEGOEUI_light);
			tfNormal = Typeface.createFromAsset(context.getAssets(),
					StaticObjects.fontPath_SEGOEUI);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		// return
		// this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon).toString();
		return this.disLocZoneList.get(groupPosition).locations
				.get(childPosititon).LocName;

	}

	public String getLocationId(int groupPosition, int childPosititon) {
		// return
		// this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon).LocId;
		return this.disLocZoneList.get(groupPosition).locations
				.get(childPosititon).LocId;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		final String childText = (String) getChild(groupPosition, childPosition);

		SavedPrefernce sp = new SavedPrefernce((Activity) _context);
		String curLocId = sp.getLocationId();

		ChildViewHolder viewHolder;
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.expandable_list_item,
					null);
			viewHolder = new ChildViewHolder();
			viewHolder.txtListChild = (TextView) convertView
					.findViewById(R.id.lblListItem);
			viewHolder.checkBox = (CheckBox) convertView
					.findViewById(R.id.checkboxLocation);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ChildViewHolder) convertView.getTag();
		}

		if (curLocId.equals(getLocationId(groupPosition, childPosition)))
			viewHolder.checkBox.setChecked(true);
		else
			viewHolder.checkBox.setChecked(false);

		viewHolder.txtListChild.setText(childText);
		if(tfNormal!=null)
		viewHolder.txtListChild.setTypeface(tfNormal);

		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// return
		// this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
		return this.disLocZoneList.get(groupPosition).locations.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		// return this._listDataHeader.get(groupPosition);
		return this.disLocZoneList.get(groupPosition).DisName;
	}

	public String getdistictId(int groupPosition) {
		// return this._listDataHeader.get(groupPosition);
		return this.disLocZoneList.get(groupPosition).DisId;
	}

	@Override
	public int getGroupCount() {
		// return this._listDataHeader.size();
		return this.disLocZoneList.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String headerTitle = (String) getGroup(groupPosition);
		GroupViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new GroupViewHolder();
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.expandable_list_group,
					null);

			viewHolder.lblListHeader1 = (TextView) convertView
					.findViewById(R.id.txtZone);
			viewHolder.imageViewIndicator = (ImageView) convertView
					.findViewById(R.id.imageViewIndicator);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (GroupViewHolder) convertView.getTag();
		}

		/*
		 * TextView lblListHeader = (TextView)
		 * convertView.findViewById(R.id.lblListHeader);
		 * lblListHeader.setTypeface(null, Typeface.BOLD);
		 * lblListHeader.setText("");
		 */

		// lblListHeader1.setTypeface(null, Typeface.BOLD);
		viewHolder.lblListHeader1.setText(headerTitle);
		if(tfNormal!=null)
		viewHolder.lblListHeader1.setTypeface(tfNormal);

		viewHolder.imageViewIndicator
				.setImageResource(isExpanded ? R.mipmap.loc_arrow_up
						: R.mipmap.loc_arrow);

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	public final class GroupViewHolder {

		TextView lblListHeader1;
		ImageView imageViewIndicator;
	}

	public final class ChildViewHolder {

		TextView txtListChild;
		CheckBox checkBox;
	}

	public void filterData(String query) {

		query = query.toLowerCase();
		// Log.v("MyListAdapter", String.valueOf(disLocZoneList.size()));
		disLocZoneList.clear();

		if (TextUtils.isEmpty(query)) {
			disLocZoneList.addAll(disLocZoneListOrigin);
		} else {

			for (DistrictLocZone dlz : disLocZoneListOrigin) {

				ArrayList<Location> locationList = dlz.locations;
				ArrayList<Location> newList = new ArrayList<Location>();
				for (Location location : locationList) {
					if (/* location.getCode().toLowerCase().contains(query) || */location.LocName
							.toLowerCase().contains(query)) {
						newList.add(location);
					}
				}

				if (newList.size() > 0) {
					DistrictLocZone nContinent = new DistrictLocZone(dlz.DisId,
							dlz.DivId, dlz.DisName, dlz.IsComp, dlz.Status,
							dlz.Lat, dlz.Lng, newList, null);// (districtLocZone.getName(),newList);
					disLocZoneList.add(nContinent);
				}
			}
		}

		// Log.v("MyListAdapter", String.valueOf(disLocZoneList.size()));
		notifyDataSetChanged();

	}

}