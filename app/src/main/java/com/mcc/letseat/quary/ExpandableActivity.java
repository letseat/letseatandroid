package com.mcc.letseat.quary;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.foody.AppData.StaticObjects;
import com.foody.jsondata.Cusine;
import com.foody.jsondata.DistrictLocZone;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.LetsEatRequest;
import com.foody.jsondata.Location;
import com.foody.jsondata.Zone;
import com.mcc.letseat.HomeT;
import com.mcc.letseat.R;
import com.mcc.user.SavedPrefernce;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.ImageView.ScaleType;
import android.widget.Toast;
 
public class ExpandableActivity extends Activity {
	ExpandableListAdapterAll adapterAll;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<Location>> listDataChild;
    private int lastExpandedPosition = -1;
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expandable_list_activity);
 
        // get the listview
        expListView = (ExpandableListView) findViewById(R.id.lvExp);
        expListView.setIndicatorBounds(345,375);
        expListView.setGroupIndicator(null);
        
        //add header
        addHeader(expListView, "Choose your Location", R.mipmap.back_fav_food, R.mipmap.ic_choose_location );
 
        // preparing list data
        prepareListData();
 
      
 
        // Listview Group click listener
        expListView.setOnGroupClickListener(new OnGroupClickListener() {
 
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,int groupPosition, long id) {
               
            	
                return false;
            }
        });
 
      
        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new OnGroupExpandListener() {
 
            @Override
            public void onGroupExpand(int groupPosition) {
            	if (lastExpandedPosition != -1 && groupPosition != lastExpandedPosition) {
            		expListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            		
             /*  View view= expListView.getExpandableListAdapter().getGroupView(groupPosition, false, null, null);
               CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkZone);*/
              /* if(checkBox.isChecked()){
            	   Toast.makeText(getApplicationContext(),"Checked", Toast.LENGTH_SHORT).show();
               }

                Toast.makeText(getApplicationContext(),
                        listDataHeader.get(groupPosition) + " Expanded"+ zoneList.get(groupPosition).ZoneId,
                        Toast.LENGTH_SHORT).show();*/
            }
        });
 
        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new OnGroupCollapseListener() {
 
            @Override
            public void onGroupCollapse(int groupPosition) {
                /*Toast.makeText(getApplicationContext(),
                        listDataHeader.get(groupPosition) + " Collapsed",
                        Toast.LENGTH_SHORT).show();*/
 
            }
        });
 
        // Listview on child click listener
        expListView.setOnChildClickListener(new OnChildClickListener() {
 
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                    int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                /*Toast.makeText(
                        getApplicationContext(),
                        listDataHeader.get(groupPosition)
                                + " : "
                                + listDataChild.get(
                                        listDataHeader.get(groupPosition)).get(
                                        childPosition), Toast.LENGTH_SHORT)
                        .show();*/
            	
            	  View view= expListView.getExpandableListAdapter().getChildView(groupPosition, childPosition, false, v, parent);
                  CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkboxLocation);
                  checkBox.setChecked(true);
            	
            	 SavedPrefernce p= new SavedPrefernce(ExpandableActivity.this);
                 /*p.setDistrictID(zoneList.get(groupPosition).DisId);
                 p.setDistrict(zoneList.get(groupPosition).DisName);
                 p.setLocationId(zoneList.get(groupPosition).locations.get(childPosition).LocId);
                 p.setLocationName(zoneList.get(groupPosition).locations.get(childPosition).LocName);*/
            	  adapterAll=(ExpandableListAdapterAll) expListView.getExpandableListAdapter();
                 p.setDistrictID(adapterAll.getdistictId(groupPosition));
                 p.setDistrict(adapterAll.getGroup(groupPosition).toString());
                 p.setLocationId(adapterAll.getLocationId(groupPosition, childPosition));
                 p.setLocationName(adapterAll.getChild(groupPosition, childPosition).toString());
                 
                 Intent intent = new Intent(ExpandableActivity.this, HomeT.class);
                 intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                 startActivity(intent);
                 finish();
            	
                return false;
            }
        });
        
       
        
        
    }
    
    //text change listener
    TextWatcher textwatcher=new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        	 adapterAll=(ExpandableListAdapterAll) expListView.getExpandableListAdapter();
            if(s.length()==0){
            	 //dumy filter
    	       // valueAdapter.getFilter().filter("~^*()");
            }else{
            	adapterAll.filterData(s.toString());
            	expandAll();
            	
            }
            	
        	
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
    
    
    private void expandAll() {
    	  int count = adapterAll.getGroupCount();
    	  for (int i = 0; i < count; i++){
    		  expListView.expandGroup(i);
    	  }
    	 }
    
  
   
    
  //if header added offset is 1 
  		int listViewOffset=0;
     
  	void addHeader(final ListView listView,String headerText, int backImageResourceId, int iconImageResourceId ){
  		//change offset as header is added
  		listViewOffset=1;
  		
  		LayoutInflater li = (LayoutInflater)ExpandableActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  		View v = li.inflate(R.layout.listview_header_location, null);

  		ImageView iconImageView=(ImageView)v.findViewById(R.id.listview_header_image);
  		final ImageView backImageView=(ImageView)v.findViewById(R.id.listview_header_back_image);
  		TextView textView =(TextView)v.findViewById(R.id.listview_header_txt);;
  		
  		//String cusineType= _filePaths.get(position).get(listTags[1]);
  		
  		textView.setText(headerText);
  		textView.setTypeface(StaticObjects.gettfNormal(ExpandableActivity.this));
  		iconImageView.setScaleType(ScaleType.CENTER_CROP);
  		
  		
  		if(headerText!=null && !headerText.isEmpty()){

			Picasso.with(this).load(backImageResourceId).into(new Target() {

				@Override
				public void onPrepareLoad(Drawable drawable) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
					backImageView.setImageDrawable(new BitmapDrawable(ExpandableActivity.this.getResources(), bitmap));

				/*	if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
						backImageView.setBackground(new BitmapDrawable(ActivityWithSliding.this.getResources(), bitmap));
					} else {
						getWindow().getDecorView().findViewById(android.R.id.content).setBackgroundDrawable(new BitmapDrawable(ActivityWithSliding.this.getResources(),bitmap));
					}*/




				}

				@Override
				public void onBitmapFailed(Drawable drawable) {
					// TODO Auto-generated method stub

				}
			});
  			
  			Picasso.with(ExpandableActivity.this)
  		    .load(backImageResourceId).fit().centerCrop()
  		    .placeholder(R.drawable.sample_res_photo)
  		    .error(R.drawable.sample_res_photo)
  		    .into(backImageView);
  			
  			Picasso.with(ExpandableActivity.this)
  		    .load(iconImageResourceId)
  		    .placeholder(R.drawable.sample_res_photo)
  		    .error(R.drawable.sample_res_photo)
  		    .into(iconImageView);
  			
  		}
  		
  		 EditText editTextSearchLocation =(EditText) v.findViewById(R.id.editTextSearchLocation);
         editTextSearchLocation.addTextChangedListener(textwatcher);
         editTextSearchLocation.setVisibility(View.GONE);
         editTextSearchLocation.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (hasFocus) {
					listView.setSelection(1);
				}else{
					listView.setSelection(0);
				}
				
			}
		});
  		
  		listView.addHeaderView(v, "header", false);
  		//adjustPosition=1;
  	}
 
    /*
     * Preparing the list data
     */
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<Location>>();
        
        //new Loadzone().execute();
        
     findViewById(R.id.expanadble_list_progress).setVisibility(View.VISIBLE);
        
     // Add the request to the queue
     Volley.newRequestQueue(this).add(stringRequest);
        
    }
    
    
    
    ArrayList<DistrictLocZone> zoneList;
    class Loadzone extends AsyncTask<String, String, String> {

		

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			 findViewById(R.id.expanadble_list_progress).setVisibility(View.VISIBLE);

		}

		/**
		 * getting JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			FoodyResturent foodyResturent = new FoodyResturent();

			try {
				zoneList = foodyResturent.getDistrictsLocZone( ExpandableActivity.this);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {

			/*
			 * AnimationTween.animateView(findViewById(R.id.cat_custom_progress),
			 * CusineListView.this); AnimationTween.animateViewPosition(lv,
			 * CusineListView.this);
			 */

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status

					if (zoneList == null || zoneList.size() <= 0) {

						

					} else if (zoneList != null) {
						
						for (DistrictLocZone z : zoneList) {
							 listDataHeader.add(z.DisName);
							 //List<String> zoneLoc =  new ArrayList<String>(Arrays.asList(z.LocName.split(",")));
							 listDataChild.put(z.DisName, z.locations);
						}
						
						adapterAll = new ExpandableListAdapterAll(ExpandableActivity.this, zoneList, listDataHeader, listDataChild);						  
					      //setting list adapter
					       expListView.setAdapter(adapterAll);
					       adapterAll.notifyDataSetChanged();
					       
					       findViewById(R.id.expanadble_list_progress).setVisibility(View.GONE);
						
						  
						
					}
				}

			});

		}

	}
    
    //volley
 // Request a string response
 		StringRequest stringRequest = new StringRequest(Request.Method.POST, LetsEatRequest.DIS_LOC_ZONE_LIST_URL,
 		            new Response.Listener<String>() {
 		    @Override
 		    public void onResponse(String response) {
 		 
 		       LetsEatRequest ler= new LetsEatRequest();
 		       zoneList = ler.getDistrictsLocZone(response);
 		       
				if (zoneList == null || zoneList.size() <= 0) {

					

				} else if (zoneList != null) {
					
					for (DistrictLocZone z : zoneList) {
						 listDataHeader.add(z.DisName);
						 //List<String> zoneLoc =  new ArrayList<String>(Arrays.asList(z.LocName.split(",")));
						 listDataChild.put(z.DisName, z.locations);
					}
					
					adapterAll = new ExpandableListAdapterAll(ExpandableActivity.this, zoneList, listDataHeader, listDataChild);						  
				      //setting list adapter
				       expListView.setAdapter(adapterAll);
				       adapterAll.notifyDataSetChanged();
				       
				       findViewById(R.id.expanadble_list_progress).setVisibility(View.GONE);
					
					  
					
				}
			
 		 
 		    }
 		}, new Response.ErrorListener() {
 		    @Override
 		    public void onErrorResponse(VolleyError error) {
 		         
 		        
 		 
 		    }
 		});



	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// Handle the back button
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			String newLocId = new SavedPrefernce(this).getLocationId();

			if (newLocId == null || newLocId.isEmpty() || newLocId.equals("") ) {
				Toast.makeText(this,"Please Choose a Location From City",Toast.LENGTH_SHORT).show();
				return true;
			} else {
				return super.onKeyDown(keyCode, event);
			}


		} else {
			return super.onKeyDown(keyCode, event);
		}

	}
}
