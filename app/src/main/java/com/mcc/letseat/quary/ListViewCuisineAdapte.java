package com.mcc.letseat.quary;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.view.InflateException;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;
import android.widget.Toast;

import com.foody.jsondata.Cusine;
import com.mcc.letseat.R;
import com.squareup.picasso.Picasso;


/**
 * Custom Adapter for Cuisine Listview
 * 
 * @author Arif
 * 
 */
public class ListViewCuisineAdapte extends BaseAdapter {

	

	String fontPath = "fonts/SEGOEUI.TTF";

	private Activity _activity;
	private  ArrayList<Cusine> cusineList;
	TypedArray imgs;
	
	

	
	Typeface tf;
	
	int reqWidth= 480;
	int reqHeight= 200;

	public ListViewCuisineAdapte(Activity activity, ArrayList<Cusine> cusineList) {
		this._activity = activity;
		this.cusineList = cusineList;				
		imgs = _activity.getResources().obtainTypedArray(R.array.image_ids);
		
		
		 tf = Typeface.createFromAsset(_activity.getAssets(), fontPath);
	}

	@Override
	public int getCount() {
		return this.cusineList.size();
	}

	@Override
	public Object getItem(int position) {
		return this.cusineList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	 ViewHolder holder = null;

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

	//	ViewHolder holder = null;

		 if(convertView == null){
			try {
				
				convertView = View.inflate(_activity, R.layout.list_item_cuisine, null);
			} catch (InflateException e) {
				System.gc();
				
				convertView = View.inflate(_activity, R.layout.list_item_cuisine, null);
			}
			
			holder = new ViewHolder();
			
			holder.img_list_item_cuisine = (ImageView) convertView.findViewById(R.id.img_list_item_cuisine);	
			//get height and width of imageView
			ViewTreeObserver vto = holder.img_list_item_cuisine.getViewTreeObserver();
			vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
			    public boolean onPreDraw() {
			    	holder.img_list_item_cuisine.getViewTreeObserver().removeOnPreDrawListener(this);
			    	reqHeight = holder.img_list_item_cuisine.getMeasuredHeight();
			        reqWidth = holder.img_list_item_cuisine.getMeasuredWidth();
			        
			        return true;
			    }
			});
			
			holder.txtCusine = (TextView) convertView.findViewById(R.id.txtCusine);
			holder.checkBoxCusine = (CheckBox) convertView.findViewById(R.id.checkBoxCusine);
			
			convertView.setTag(holder);
			convertView.setTag(R.id.img_list_item_cuisine, holder.img_list_item_cuisine);
			convertView.setTag(R.id.txtCusine, holder.txtCusine);
			convertView.setTag(R.id.checkBoxCusine, holder.checkBoxCusine);
			
			 holder.checkBoxCusine.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				 
                 @Override
                 public void onCheckedChanged(CompoundButton vw, boolean isChecked) {

                     int getPosition = (Integer) vw.getTag();
                     cusineList.get(getPosition).setSelected( vw.isChecked());
                     getSelectedContacts() ;

                 }
             });
			

		}else{
			// recycle the already inflated view 
			holder = (ViewHolder) convertView.getTag();
		}
		
		 holder.checkBoxCusine.setTag(position);
		 holder.checkBoxCusine.setChecked(cusineList.get(position).isSelected());
		
		String cusineType = cusineList.get(position).cusinename;
		holder.txtCusine.setText(cusineType);
		holder.txtCusine.setTypeface(tf);
		//holder.checkBoxCusine.setChecked(holder.checkBoxCusine.isChecked());
		
		holder.img_list_item_cuisine.setScaleType(ScaleType.CENTER_CROP);
		Resources res= _activity.getResources();
		int resId = getRandomResourceID( ((position + 1) % 11));
		
		
		
		
		
		//Bitmap bitmap=  decodeBitmapFromResource(res, resId, 480, 200); 
		
		//holder.img_list_item_cuisine.setImageDrawable(new BitmapDrawable(res, bitmap));

		String cusineImgUrl = cusineList.get(position).IMG;

		//loadBitmap(getRandomResourceID((position + 1) % 11), holder.imageView);
		
		if (cusineImgUrl != null && !cusineImgUrl.isEmpty()) {
			Picasso.with(_activity).load(cusineImgUrl)
					.placeholder(resId)
					.error(resId)
					.resize(reqWidth, reqWidth)
					.into(holder.img_list_item_cuisine);
		} 

		

		

		return convertView;
	}
	
	  private void getSelectedContacts() {
	        // TODO Auto-generated method stub
	 
	        StringBuffer sb = new StringBuffer();
	        
	 
	        for (Cusine bean : cusineList) {
	 
	            if (bean.isSelected()) {
	                sb.append(bean.Id);
	                sb.append(",");
	               
	            }
	        }
	 
	        String s = sb.toString().trim();
	        ScreenSlideActivity.CuisineId=s;
	 
	        if (TextUtils.isEmpty(s)) {
	            //Toast.makeText(_activity, "Select atleast one Contact",  Toast.LENGTH_SHORT).show();
	        } else {
	 
	            s = s.substring(0, s.length() - 1);
	            //Toast.makeText(_activity, "Selected Cuisine : " + s, Toast.LENGTH_SHORT).show();
	            
	 
	        }
	 
	    }
	
	
	
	private static class ViewHolder{
		ImageView img_list_item_cuisine ;		
		
		TextView txtCusine ;
		TextView txtCusineInfo ;
		CheckBox checkBoxCusine;
	}
	
	private int getRandomResourceID(int random) {
		
		int d = 0;
		switch (random) {
		case 1:
			d = R.mipmap.back5;
			break;
		case 2:
			d = R.mipmap.back2;
			break;
		case 3:
			d = R.mipmap.back8;
			break;
		case 4:
			d = R.mipmap.back4;
			break;
		case 5:
			d = R.mipmap.back_login;
			break;
		case 6:
			d = R.mipmap.back6;
			break;
		case 7:
			d = R.mipmap.back7;
			break;
		case 8:
			d = R.mipmap.back3;
			break;
		case 9:
			d = R.mipmap.back9;
			break;
		case 10:
			d = R.mipmap.back10;
			break;
		case 11:
			d = R.mipmap.back11;
			break;

		default:
			d = R.mipmap.back8;
			break;
		}

		return d;
	}
	
	public  int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
    // Raw height and width of image
    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = 1;

    if (height > reqHeight || width > reqWidth) {

        final int halfHeight = height / 2;
        final int halfWidth = width / 2;

        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
        // height and width larger than the requested height and width.
        while ((halfHeight / inSampleSize) > reqHeight
                && (halfWidth / inSampleSize) > reqWidth) {
            inSampleSize *= 2;
        }
    }

    	return inSampleSize;
	}
	
	public  Bitmap decodeBitmapFromResource(Resources res, int resId,
	        int reqWidth, int reqHeight) {

	    // First decode with inJustDecodeBounds=true to check dimensions
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeResource(res, resId, options);

	    // Calculate inSampleSize
	    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	    return BitmapFactory.decodeResource(res, resId, options);
	}
	
	
}
