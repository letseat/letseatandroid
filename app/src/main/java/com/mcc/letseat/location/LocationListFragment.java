package com.mcc.letseat.location;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.Location;
import com.foody.nearby.ConnectionDetector;
import com.foody.nearby.GPSTracker;
import com.foody.nearby.MyListAdapterSingle;
import com.google.android.gms.maps.model.LatLng;
import com.mcc.letseat.HomeT;
import com.mcc.letseat.MyProgressDialog;
import com.mcc.letseat.R;
import com.mcc.libs.AnimationTween;
import com.mcc.libs.CF;
import com.mcc.user.SavedPrefernce;

/**
 * Provide List of Locations based on selected cities Let's Eat cover
 * @author Arif
 *
 */
public class LocationListFragment extends Fragment {
	
	View currentView = null;
	// Places Listview
	ListView lv;
	
	// Google Places
		FoodyResturent foodyResturent;

		// Places List
		ArrayList<Location> locationList;

		// Button
		ImageView btnShowOnMap;

		// Progress dialog
		MyProgressDialog pDialog;

	

		// ListItems data
		ArrayList<HashMap<String, String>> locationListItems = new ArrayList<HashMap<String, String>>();
		
		String DisId, DisName;
		
		TextView txtChooseLoc;
		
		Typeface tf ,tfNormal ;	
		
		Activity _activity;
		
		// GPS Location
		//GPSTracker gps;
		
		@Override
		public void onAttach(Activity activity) {
			// TODO Auto-generated method stub
			super.onAttach(activity);
			_activity=activity;
		}
		
	public static LocationListFragment newInstance(String imageUrl) {

		final LocationListFragment mf = new LocationListFragment();

		final Bundle args = new Bundle();
		args.putString("somedata", "somedata");
		mf.setArguments(args);

		return mf;
	}

	public LocationListFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//String data = getArguments().getString("somedata");
	}
	
	public void reload(String _DisId, String _DisName){
		
		if(locationListItems!=null && (locationListItems.size()>0) && locationListItems.get(0).get("DisId").equals(_DisId)){			
			this.DisId=_DisId;
			this.DisName=_DisName;
			
			if(txtChooseLoc!=null){
			txtChooseLoc.setText("Choose Location in "+_DisName);
			}
		}else{
			this.DisId=_DisId;
			this.DisName=_DisName;
			
			if(txtChooseLoc!=null){
			txtChooseLoc.setText("Choose Location in "+_DisName);
			}
		
			
			new LoadPlaces().execute();			
		}
		
	}


	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		/*
		 * // Inflate the layout for this fragment restaurentId =
		 * getArguments().getString("restaurantId"); Log.e("restaurentId",
		 * restaurentId); currentView =
		 * inflater.inflate(R.layout.restaurant_menu, container,false); return
		 * currentView;
		 */

		setRetainInstance(true);

		if (currentView != null) {
			ViewGroup parent = (ViewGroup) currentView.getParent();
			if (parent != null)
				parent.removeView(currentView);
		}

		try {

			currentView = inflater.inflate(R.layout.activity_choose_location, container,
					false);
		} catch (InflateException e) {
			/* map is already there, just return view as it is */
		}

		return currentView;
	}
	
	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		 txtChooseLoc=(TextView)currentView.findViewById(R.id.txtChooseLoc);
		 txtChooseLoc.setText("Choose Location ");
		
		 tf = Typeface.createFromAsset(_activity.getAssets(), StaticObjects.fontPath_SEGOEUI_light);
		 tfNormal = Typeface.createFromAsset(_activity.getAssets(), StaticObjects.fontPath_SEGOEUI);
		txtChooseLoc.setTypeface(tfNormal);
		
		// Getting listview
				lv = (ListView) currentView.findViewById(R.id.list_cat);

				lv.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						Location location = (Location) locationList.get(position);
						
						//save data 
						 SavedPrefernce savedPrefernce=new SavedPrefernce(_activity);
						 savedPrefernce.setDistrictID(DisId );
						 savedPrefernce.setDistrict(DisName);
						 
					     savedPrefernce.setLocationId(location.LocId);
					     savedPrefernce.setLocationName(location.LocName);
					     	
					     
					     _activity.finish();
					     Intent in = new Intent(_activity.getApplicationContext(),HomeT.class);
					     _activity.startActivity(in);
						
					}
				});
				
				// Initialize GPS Class object
				/*gps = new GPSTracker(_activity);

				// check if GPS location can get
				if (gps.canGetLocation()) {
					Log.d("Your Location", "latitude:" + gps.getLatitude() + ", longitude: " + gps.getLongitude());
					//myLocation=new LatLng( gps.getLatitude(), gps.getLongitude());
				} else {
					
					gps.showSettingsAlert();
					// stop executing code by return
					return;
				}*/
				
				new LoadPlaces().execute();
	}
	
	
	class LoadPlaces extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				currentView.findViewById(R.id.choose_loc_custom_progress).setVisibility(View.VISIBLE);
				
				currentView.findViewById(R.id.choose_loc_custom_progress).setVisibility(View.VISIBLE);
				View v = currentView.findViewById(R.id.choose_loc_custom_progress);
				TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
				textPreparing.setTypeface(tfNormal);
				textPreparing.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
				textPreparing.setText("Preparing...");
				v.findViewById(R.id.progressBarPreparing).setVisibility(View.VISIBLE);	
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {
				locationList = foodyResturent.getLocation(DisId, _activity);
				

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			//pDialog.dismiss();
			//currentView.findViewById(R.id.choose_loc_custom_progress).setVisibility(View.GONE);
			if( _activity==null){return;}
			
			AnimationTween.animateView(currentView.findViewById(R.id.choose_loc_custom_progress), _activity);
			AnimationTween.animateViewPosition(lv, _activity);
			// updating UI from Background Thread
			_activity.runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status
					
					if (locationList == null || locationList.size() <= 0) {

						currentView.findViewById(R.id.choose_loc_custom_progress).setVisibility(View.VISIBLE);
						View v = currentView.findViewById(R.id.choose_loc_custom_progress);
						TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
						textPreparing.setText("Nothing found.");
						v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
						AnimationTween.animateViewPosition(currentView.findViewById(R.id.choose_loc_custom_progress), _activity);
						
						if(!new ConnectionDetector(_activity.getApplicationContext()).isConnectingToInternet()){
							 textPreparing.setText("No internet connection. Retry ");
							 textPreparing.setGravity(Gravity.CENTER);
							 textPreparing.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_refresh, 0);
							 currentView.findViewById(R.id.choose_loc_custom_progress).setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									reload(DisId, DisName);
								}

								
							});
							}
	 
					} else if (locationList != null) {
						// Check for all possible status

						// Successfully got places details
						if (locationList != null) {
							locationListItems = new ArrayList<HashMap<String, String>>();
							
							// loop through each place
							for (Location p : locationList) {

								HashMap<String, String> map = new HashMap<String, String>();
								map.put("DisId", p.DisId);
								// Place name
								map.put("LocId", p.LocId);
								
								
								//distance
								double distance=0;
								/*try {
									 distance=CF.getDistanceKM(new LatLng(gps.latitude, gps.longitude),new LatLng(Double.parseDouble(p.Lat), Double.parseDouble(p.Lng) ));
									// distance=distance(gps.latitude, gps.longitude,Double.parseDouble(p.Lat), Double.parseDouble(p.Lng) );
									
								} catch (Exception e) {
									// TODO: handle exception
								}*/
								map.put("distance", ""+distance);
								
								map.put("LocName", p.LocName);
								map.put("Lat", p.Lat);
								map.put("Lng", p.Lng);

								System.out
										.println(">>>>>>>>>>test>>>>>>>>>>>>>>>>>> "
												+ p.LocName);

								// adding HashMap to ArrayList
								locationListItems.add(map);
							}
							
							//sort by distance
							/*Comparator<HashMap<String, String>> mapComparator = new Comparator<HashMap<String, String>>() {
							    public int compare(HashMap<String, String> m1, HashMap<String, String> m2) {
							        //return m1.get("distance").compareTo(m2.get("distance"));
							        
							        return  Double.compare(Double.parseDouble(m1.get("distance")), Double.parseDouble(m2.get("distance")));
							    }
							};
							
							Collections.sort(locationListItems,mapComparator);*/
							
							
							// list adapter
							MyListAdapterSingle adapter = new MyListAdapterSingle(
									_activity,
									locationListItems,"LocName");

							// Adding data into listview
							lv.setAdapter(adapter);
							
							if(CityLocFragmentActivity.Iscomplete==null ||CityLocFragmentActivity.Iscomplete.equals("0"))
								currentView.findViewById(R.id.txtChooseLocInComMsg).setVisibility(View.VISIBLE);
							else
								currentView.findViewById(R.id.txtChooseLocInComMsg).setVisibility(View.GONE);
						}

						else if (locationList == null) {
							currentView.findViewById(R.id.choose_loc_custom_progress).setVisibility(View.VISIBLE);
							View v = currentView.findViewById(R.id.choose_loc_custom_progress);
							TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
							textPreparing.setText("Nothing found.");
							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);

						}
					}
				}

			});

		}

	}
	
	
	
}