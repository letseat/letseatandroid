package com.mcc.letseat.location;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.District;
import com.foody.jsondata.FoodyResturent;
import com.foody.nearby.ConnectionDetector;
import com.foody.nearby.GPSTracker;
import com.foody.nearby.MyListAdapterSingle;
import com.google.android.gms.maps.model.LatLng;
import com.mcc.letseat.R;
import com.mcc.libs.AnimationTween;
import com.mcc.libs.CF;

/**
 * Provide List of Cities Let's Eat cover
 * @author Arif
 *
 */
public class DistrictListFrgment extends Fragment {
	
	View currentView = null;
	// Places Listview
	ListView lv;
	
	// Places List
	ArrayList<District> districtList;
	
	// ListItems data
	ArrayList<HashMap<String, String>> districtListItems = new ArrayList<HashMap<String, String>>();

	// Google Places
		FoodyResturent foodyResturent;
		
		Typeface tf ,tfNormal ;	
		
	Activity _activity;
	
	// GPS Location
		//GPSTracker gps;
		
	public static DistrictListFrgment newInstance(String imageUrl) {

		final DistrictListFrgment mf = new DistrictListFrgment();

		final Bundle args = new Bundle();
		args.putString("somedata", "somedata");
		mf.setArguments(args);

		return mf;
	}

	public DistrictListFrgment() {
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		_activity=activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//String data = getArguments().getString("somedata");
	}


	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		/*
		 * // Inflate the layout for this fragment restaurentId =
		 * getArguments().getString("restaurantId"); Log.e("restaurentId",
		 * restaurentId); currentView =
		 * inflater.inflate(R.layout.restaurant_menu, container,false); return
		 * currentView;
		 */

		setRetainInstance(true);

		if (currentView != null) {
			ViewGroup parent = (ViewGroup) currentView.getParent();
			if (parent != null)
				parent.removeView(currentView);
		}

		try {

			currentView = inflater.inflate(R.layout.activity_choose_city, container,
					false);
		} catch (InflateException e) {
			/* map is already there, just return view as it is */
		}

		return currentView;
	}
	
	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
		// Initialize GPS Class object
		/*gps = new GPSTracker(_activity);

		// check if GPS location can get
		if (gps.canGetLocation()) {
			Log.d("Your Location", "latitude:" + gps.getLatitude() + ", longitude: " + gps.getLongitude());
			//myLocation=new LatLng( gps.getLatitude(), gps.getLongitude());
		} else {
			
			gps.showSettingsAlert();
			// stop executing code by return
			return;
		}*/
		
		
		TextView txtChooseLoc=(TextView)currentView.findViewById(R.id.txtChooseLoc);
		txtChooseLoc.setText("Choose City");
		tf = Typeface.createFromAsset(_activity.getAssets(), StaticObjects.fontPath_SEGOEUI_light);
		 tfNormal = Typeface.createFromAsset(_activity.getAssets(), StaticObjects.fontPath_SEGOEUI);
		txtChooseLoc.setTypeface(tfNormal);
		
		// Getting listview
				lv = (ListView) currentView.findViewById(R.id.list_cat);

				lv.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						District district = (District) districtList.get(position);
						CityLocFragmentActivity.DisID=district.DisId;
						CityLocFragmentActivity.DisName=district.DisName;
						CityLocFragmentActivity.Iscomplete =district.IsComp;
						CityLocFragmentActivity.viewPager.setCurrentItem(1);
					}
				});
				
				new LoadPlaces().execute();
	}
	
	private void reload() {
		// TODO Auto-generated method stub
		new LoadPlaces().execute();
	}
	
	
	class LoadPlaces extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			currentView.findViewById(R.id.choose_city_custom_progress).setVisibility(View.VISIBLE);
			
			currentView.findViewById(R.id.choose_city_custom_progress).setVisibility(View.VISIBLE);
			View v = currentView.findViewById(R.id.choose_city_custom_progress);
			TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
			textPreparing.setTypeface(tfNormal);
			textPreparing.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			textPreparing.setText("Preparing...");
			v.findViewById(R.id.progressBarPreparing).setVisibility(View.VISIBLE);	
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {
				districtList = foodyResturent.getDistricts(_activity);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			// pDialog.dismiss();
			//currentView.findViewById(R.id.choose_city_custom_progress).setVisibility(View.GONE);
			AnimationTween.animateView(currentView.findViewById(R.id.choose_city_custom_progress), _activity);
			AnimationTween.animateViewPosition(lv, _activity);
			// updating UI from Background Thread
			_activity.runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status

					if (districtList == null || districtList.size() <= 0) {

						currentView.findViewById(R.id.choose_city_custom_progress).setVisibility(View.VISIBLE);
						View v = currentView.findViewById(R.id.choose_city_custom_progress);
						TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
						textPreparing.setText("Nothing found.");
						v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
						AnimationTween.animateViewPosition(currentView.findViewById(R.id.choose_city_custom_progress), _activity);
						
						
						
						
						
						if(!new ConnectionDetector(_activity.getApplicationContext()).isConnectingToInternet()){
							 textPreparing.setText("No internet connection. Retry ");
							 textPreparing.setGravity(Gravity.CENTER);
							 textPreparing.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_refresh, 0);
							 currentView.findViewById(R.id.choose_city_custom_progress).setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									reload();
								}

								
							});
							}

					} else if (districtList != null) {
						// Check for all possible status

						// Successfully got places details
						if (districtList != null) {
							districtListItems = new ArrayList<HashMap<String, String>>();
							// loop through each place
							for (District p : districtList) {

								if(p.Status.equals("1")){
									HashMap<String, String> map = new HashMap<String, String>();
									map.put("DisId", p.DisId);
									// Place name
									map.put("DivId", p.DivId);
									
									
									//distance
									double distance=0;
									/*try {
										 distance=CF.getDistanceKM(new LatLng(gps.latitude, gps.longitude),new LatLng(Double.parseDouble(p.Lat), Double.parseDouble(p.Lng) ));
										// distance=distance(gps.latitude, gps.longitude,Double.parseDouble(p.Lat), Double.parseDouble(p.Lng) );
										
									} catch (Exception e) {
										// TODO: handle exception
									}*/
									
									map.put("DisName", p.DisName);
									
									map.put("distance", ""+distance);

									// adding HashMap to ArrayList
									districtListItems.add(map);
								}
								
							}
							
							//sort by distance
							/*Comparator<HashMap<String, String>> mapComparator = new Comparator<HashMap<String, String>>() {
							    public int compare(HashMap<String, String> m1, HashMap<String, String> m2) {
							        //return m1.get("distance").compareTo(m2.get("distance"));
							        
							        return  Double.compare(Double.parseDouble(m1.get("distance")), Double.parseDouble(m2.get("distance")));
							    }
							};
							
							Collections.sort(districtListItems,mapComparator);*/
							
							
							// list adapter
							MyListAdapterSingle adapter = new MyListAdapterSingle(
									_activity, districtListItems,
									"DisName");
							

							// Adding data into listview
							lv.setAdapter(adapter);
							
							
						}

						else if (districtList == null) {
							currentView.findViewById(R.id.choose_city_custom_progress)
									.setVisibility(View.VISIBLE);
							View v = currentView.findViewById(R.id.choose_city_custom_progress);
							TextView textPreparing = (TextView) v
									.findViewById(R.id.textPreparing);
							textPreparing.setText("Nothing found.");
							v.findViewById(R.id.progressBarPreparing)
									.setVisibility(View.GONE);
							AnimationTween.animateViewPosition(currentView.findViewById(R.id.choose_city_custom_progress), _activity);
						}
					}
				}

			});

		}

	}
	
	
	
	
	
}