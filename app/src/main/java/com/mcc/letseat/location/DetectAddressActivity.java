package com.mcc.letseat.location;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.District;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.Location;
import com.foody.nearby.ConnectionDetector;
import com.foody.nearby.DistrictListView;
import com.foody.nearby.GPSTracker;
import com.foody.nearby.LocatonListView;
import com.google.android.gms.maps.model.LatLng;
import com.mcc.letseat.HomeT;
import com.mcc.letseat.R;
import com.mcc.libs.CF;
import com.mcc.user.SavedPrefernce;

/**
 * Auto Detect location based on lat lng which is closest 
 * @author Arif
 *
 */
public class DetectAddressActivity extends Activity implements OnClickListener {

	// GPS Location
	GPSTracker gps;

	TextView txtDetectCity, txtDetectLoc;
	ProgressBar progressBarDetectAll;
	ImageView imgDetectAll;

	// Places List
	ArrayList<District> districtList;

	// ListItems data
	ArrayList<HashMap<String, String>> districtListItems = new ArrayList<HashMap<String, String>>();

	// Location List
	ArrayList<Location> locationList;

	// ListItems data
	ArrayList<HashMap<String, String>> locationListItems = new ArrayList<HashMap<String, String>>();

	// async task
	LoadDistrict loadDistrict;
	LoadLocation loadLocation;

	String DisId, DisName, LocId, LocName;

	boolean detectAll;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detect_location);

		txtDetectCity = (TextView) findViewById(R.id.txtDetectCity);
		txtDetectLoc = (TextView) findViewById(R.id.txtDetectLoc);

		progressBarDetectAll = (ProgressBar) findViewById(R.id.progressBarDetectAll);

		imgDetectAll = (ImageView) findViewById(R.id.imgDetectAll);

		// Initialize GPS Class object
		gps = new GPSTracker(DetectAddressActivity.this);

		SavedPrefernce sp = new SavedPrefernce(DetectAddressActivity.this);
		DisId = sp.getDistrictID();
		DisName = sp.getDistrict();
		txtDetectCity.setText(DisName.isEmpty() ? "City" : DisName);

		LocId = sp.getLocationId();
		LocName = sp.getLocationName();
		txtDetectLoc.setText(LocName.isEmpty() ? "Location" : LocName);

		findViewById(R.id.rl_detect_city).setOnClickListener(this);
		findViewById(R.id.rl_detect_loc).setOnClickListener(this);
		findViewById(R.id.rl_detect_all).setOnClickListener(this);
		findViewById(R.id.imgDetectCity).setOnClickListener(this);
		findViewById(R.id.imgDetectLoc).setOnClickListener(this);

		// check if GPS location can get
		if (gps.canGetLocation()) {
			Log.d("Your Location", "latitude:" + gps.getLatitude()
					+ ", longitude: " + gps.getLongitude());
			// myLocation=new LatLng( gps.getLatitude(), gps.getLongitude());
			detectAll = true;
			loadDistrict = new LoadDistrict();
			loadDistrict.execute();
		} else {

			gps.showSettingsAlert();
			// stop executing code by return
			return;
		}

	}

	@Override
	public void onClick(View v) {

		if (!new ConnectionDetector(getApplicationContext())
				.isConnectingToInternet()) {
			Toast.makeText(DetectAddressActivity.this, "Connect To Internet",
					Toast.LENGTH_LONG).show();
			return;
		}

		if (v.getId() == R.id.imgDetectCity) {
			loadDistrict = new LoadDistrict();
			loadDistrict.execute();

		} else if (v.getId() == R.id.imgDetectLoc) {

			loadLocation = new LoadLocation();
			loadLocation.execute();

		} else if (v.getId() == R.id.rl_detect_all) {
			detectAll = true;
			loadDistrict = new LoadDistrict();
			loadDistrict.execute();

		} else if (v.getId() == R.id.rl_detect_city) {

			Intent i = new Intent(this, DistrictListView.class);
			i.putExtra(StaticObjects.NEV_TYPE,
					StaticObjects.NEV_TYPE_CHOOSE_LOCTION_N_SAVE);
			startActivity(i);

		} else if (v.getId() == R.id.rl_detect_loc) {

			Intent in = new Intent(this, LocatonListView.class);
			in.putExtra(StaticObjects.NEV_TYPE,
					StaticObjects.NEV_TYPE_CHOOSE_LOCTION_N_SAVE);
			in.putExtra("DisId", DisId);
			in.putExtra("DisName", DisName);
			startActivity(in);

		}

	}

	void saveLocationData() {
		SavedPrefernce sp = new SavedPrefernce(DetectAddressActivity.this);

		sp.setDistrictID(DisId);
		sp.setDistrict(DisName);
		sp.setLocationId(LocId);
		sp.setLocationName(LocName);
	}

	void gotoHome() {
		Handler h = new Handler();
		h.postDelayed(new Runnable() {
			public void run() {
				finish();
				Intent intent = new Intent(DetectAddressActivity.this,
						HomeT.class);
				startActivity(intent);

			}
		}, 500);
	}

	FoodyResturent foodyResturent;

	class LoadDistrict extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			imgDetectAll.setVisibility(View.INVISIBLE);
			progressBarDetectAll.setVisibility(View.VISIBLE);
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {
				districtList = foodyResturent
						.getDistricts(DetectAddressActivity.this);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products

			imgDetectAll.setVisibility(View.VISIBLE);
			progressBarDetectAll.setVisibility(View.INVISIBLE);

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status

					if (districtList == null || districtList.size() <= 0) {

						Toast.makeText(DetectAddressActivity.this,
								"City Not found", Toast.LENGTH_LONG).show();
						if (!new ConnectionDetector(getApplicationContext())
								.isConnectingToInternet()) {
							Toast.makeText(DetectAddressActivity.this,
									"Connect To Internet", Toast.LENGTH_LONG)
									.show();
						}

					} else if (districtList != null) {
						// Check for all possible status

						// Successfully got places details
						if (districtList != null) {
							districtListItems = new ArrayList<HashMap<String, String>>();
							// loop through each place
							for (District p : districtList) {

								if (p.Status.equals("1")) {
									HashMap<String, String> map = new HashMap<String, String>();
									map.put("DisId", p.DisId);
									// Place name
									map.put("DivId", p.DivId);

									// distance
									double distance = 0;
									try {
										distance = CF
												.getDistanceKM(
														new LatLng(
																gps.latitude,
																gps.longitude),
														new LatLng(
																Double.parseDouble(p.Lat),
																Double.parseDouble(p.Lng)));
										// distance=distance(gps.latitude,
										// gps.longitude,Double.parseDouble(p.Lat),
										// Double.parseDouble(p.Lng) );

									} catch (Exception e) {
										// TODO: handle exception
									}

									map.put("DisName", p.DisName);

									map.put("distance", "" + distance);

									// adding HashMap to ArrayList
									districtListItems.add(map);
								}

							}

							Comparator<HashMap<String, String>> mapComparator = new Comparator<HashMap<String, String>>() {
								public int compare(HashMap<String, String> m1,
										HashMap<String, String> m2) {
									// return
									// m1.get("distance").compareTo(m2.get("distance"));

									return Double.compare(Double.parseDouble(m1
											.get("distance")), Double
											.parseDouble(m2.get("distance")));
								}
							};

							Collections.sort(districtListItems, mapComparator);

							HashMap<String, String> map = districtListItems
									.get(0);

							DisId = map.get("DisId");
							DisName = map.get("DisName");
							txtDetectCity.setText(DisName);

							// for detect all
							if (detectAll) {
								detectAll = false;
								loadLocation = new LoadLocation();
								loadLocation.execute();
							}

						}

						else if (districtList == null) {
							Toast.makeText(DetectAddressActivity.this,
									"City Not found", Toast.LENGTH_LONG).show();
						}
					}
				}

			});

		}

	}

	class LoadLocation extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			try {
				imgDetectAll.setVisibility(View.INVISIBLE);
				progressBarDetectAll.setVisibility(View.VISIBLE);
			} catch (Exception e) {
				// TODO: handle exception
			}

		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {
				locationList = foodyResturent.getLocation(DisId,
						DetectAddressActivity.this);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			imgDetectAll.setVisibility(View.VISIBLE);
			progressBarDetectAll.setVisibility(View.INVISIBLE);

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status

					if (locationList == null || locationList.size() <= 0) {

						Toast.makeText(DetectAddressActivity.this,
								"Location Not found", Toast.LENGTH_LONG).show();
						if (!new ConnectionDetector(getApplicationContext())
								.isConnectingToInternet()) {
							Toast.makeText(DetectAddressActivity.this,
									"Connect To Internet", Toast.LENGTH_LONG)
									.show();
						}

					} else if (locationList != null) {
						// Check for all possible status

						// Successfully got places details
						if (locationList != null) {
							locationListItems = new ArrayList<HashMap<String, String>>();

							// loop through each place
							for (Location p : locationList) {

								HashMap<String, String> map = new HashMap<String, String>();
								map.put("DisId", p.DisId);
								// Place name
								map.put("LocId", p.LocId);

								// distance
								double distance = 0;
								try {
									distance = CF.getDistanceKM(
											new LatLng(gps.latitude,
													gps.longitude),
											new LatLng(Double
													.parseDouble(p.Lat), Double
													.parseDouble(p.Lng)));
									// distance=distance(gps.latitude,
									// gps.longitude,Double.parseDouble(p.Lat),
									// Double.parseDouble(p.Lng) );

								} catch (Exception e) {
									// TODO: handle exception
								}
								map.put("distance", "" + distance);

								map.put("LocName", p.LocName);
								map.put("Lat", p.Lat);
								map.put("Lng", p.Lng);

								System.out
										.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>> "
												+ p.LocName);

								// adding HashMap to ArrayList
								locationListItems.add(map);
							}

							Comparator<HashMap<String, String>> mapComparator = new Comparator<HashMap<String, String>>() {
								public int compare(HashMap<String, String> m1,
										HashMap<String, String> m2) {
									// return
									// m1.get("distance").compareTo(m2.get("distance"));

									return Double.compare(Double.parseDouble(m1
											.get("distance")), Double
											.parseDouble(m2.get("distance")));
								}
							};

							Collections.sort(locationListItems, mapComparator);

							HashMap<String, String> map = locationListItems
									.get(0);

							LocId = map.get("LocId");
							LocName = map.get("LocName");
							txtDetectLoc.setText(LocName);

							// save auto detect city and location then goto home
							saveLocationData();
							gotoHome();

							/*
							 * findViewById(R.id.rl_next).setVisibility(View.VISIBLE
							 * ); AnimationTween.crossfade(
							 * findViewById(R.id.rl_detect_all),
							 * findViewById(R.id.rl_next), 300);
							 */

						}

						else if (locationList == null) {
							Toast.makeText(DetectAddressActivity.this,
									"Location Not found", Toast.LENGTH_LONG)
									.show();
							if (!new ConnectionDetector(getApplicationContext())
									.isConnectingToInternet()) {
								Toast.makeText(DetectAddressActivity.this,
										"Connect To Internet",
										Toast.LENGTH_LONG).show();
							}
						}
					}
				}

			});

		}

	}

}
