package com.mcc.letseat.location;

import java.util.List;
import java.util.Vector;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.mcc.letseat.R;

/**
 * FragmentActivity Conatin DistrictListFagment and LocationListFragment
 * @author Arif
 *
 */
public class CityLocFragmentActivity extends FragmentActivity{
	
	public static ViewPager viewPager;
	public static String DisID="", DisName="", Iscomplete;
	
	CityLocPagerAdepter mPagerAdapter;
	
	String url="";
	
	AdView adView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.city_loc__fragment_activity);
		initializePager();
		
		List<Fragment> fragments = new Vector<Fragment>();

		//for each fragment you want to add to the pager
		Bundle page = new Bundle();
		page.putString("url", url);
		fragments.add(Fragment.instantiate(this,DistrictListFrgment.class.getName(),page));
		fragments.add(Fragment.instantiate(this,LocationListFragment.class.getName(),page));

		//after adding all the fragments write the below lines

		this.mPagerAdapter  = new CityLocPagerAdepter(super.getSupportFragmentManager(), fragments);

		viewPager.setAdapter(this.mPagerAdapter);
		
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				// on changing the page
				// make respected tab selected
				//Toast.makeText(CityLocFragmentActivity.this, position+"", Toast.LENGTH_LONG).show();
				if(position==1){
					LocationListFragment fragment2=(LocationListFragment)mPagerAdapter.getItem(position);
					if(fragment2!=null)
					fragment2.reload(DisID,DisName);
				}
				
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
		
		adView = (AdView) this.findViewById(R.id.adView);
		  // Create ad request.
	    AdRequest adRequest = new AdRequest.Builder().build();
	    adView.loadAd(adRequest);
	   	adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});
	}
	
	
	
	void initializePager(){

		// Initilization
		viewPager = (ViewPager) findViewById(R.id.pager);
		

	}
	
	@Override
	public void onPause() {
		super.onPause();

		if (adView != null)
			adView.pause();

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		
		if (adView != null)
		{	adView.destroy();}
		
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}
	
	@Override
	protected void onStart() {	
		super.onStart();
		
		//analytics stop tracking
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}
	
		
}
