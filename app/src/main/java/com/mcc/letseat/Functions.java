package com.mcc.letseat;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.mcc.libs.ExifUtil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;

/**
 * Resize Image before upload
 * @author Arif
 * 
 */
public class Functions {

	// Bitmap.createScaledBitmap(src, dstWidth, dstHeight, filter)

	public File resizeImageFile(String fileUri) {

		try {

			File file = new File(fileUri);

			Bitmap bit = decodeFile(file);

			try {

				int orientation = new ExifInterface(fileUri).getAttributeInt(
						ExifInterface.TAG_ORIENTATION, 1);

				if (orientation != 0 && orientation != 1)

				{

					System.out.println("Applying exif");

					bit = ExifUtil.rotateBitmap(fileUri, bit);

				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			//resize photo
			bit = getResizedBitmap(bit, 800, 600);
			
			bit.compress(CompressFormat.JPEG, 60 /* ignored for PNG */, bos);

			byte[] bitmapdata = bos.toByteArray();

			new FileOutputStream(file).write(bitmapdata);

			return file;

		} catch (Exception e) {
		}

		return null;

	}
	
	

	public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {

		int width = bm.getWidth();

		int height = bm.getHeight();

		float scaleWidth = ((float) newWidth) / width;

		float scaleHeight = ((float) newHeight) / height;

		float ratio = (float) scaleWidth / (float) scaleHeight;

		// create a matrix for the manipulation

		Matrix matrix = new Matrix();

		// resize the bit map

		matrix.postScale(ratio, ratio);

		// recreate the new Bitmap

		Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
				matrix, false);

		return resizedBitmap;

	}

	public static String StringCutter(String dat, String start, String end) {

		int point1 = dat.indexOf(start) + start.length();

		int point2 = dat.indexOf(end);

		System.out.println(point1 + "     " + point2);

		dat = dat.substring(point1, point2);

		return dat;

	}

	public static String StringCutter1(String dat, String start) {

		int point1 = dat.indexOf(start) + start.length();

		int point2 = dat.length();

		System.out.println(point1 + "     " + point2);

		dat = dat.substring(point1, point2);

		return dat;

	}

	public static String StringCutter2(String dat, String end) {

		int point1 = 0;

		int point2 = dat.indexOf(end);

		System.out.println(point1 + "     " + point2);

		dat = dat.substring(point1, point2);

		return dat;

	}

	private Bitmap decodeFile(File f) {

		try {

			// Decode image size

			BitmapFactory.Options o = new BitmapFactory.Options();

			o.inJustDecodeBounds = true;

			BitmapFactory.decodeStream(new FileInputStream(f), null, o);

			// The new size we want to scale to

			final int REQUIRED_SIZE = 400;

			// Find the correct scale value. It should be the power of 2.

			int width_tmp = o.outWidth, height_tmp = o.outHeight;

			int scale = 1;

			while (true) {

				if (width_tmp / 2 < REQUIRED_SIZE
						|| height_tmp / 2 < REQUIRED_SIZE)

					break;

				width_tmp /= 2;

				height_tmp /= 2;

				scale *= 2;

			}

			// Decode with inSampleSize

			BitmapFactory.Options o2 = new BitmapFactory.Options();

			o2.inSampleSize = scale;

			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);

		} catch (FileNotFoundException e) {
		}

		return null;

	}

}
