package com.mcc.letseat;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import com.mcc.le.gcm.PushActivity;
import com.mcc.letseat.R;
import com.mcc.user.RegistrationOptionActivity;
import com.mcc.user.SavedPrefernce;
import com.foody.AppData.StaticObjects;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.JSONParser;
import com.foody.nearby.ConnectionDetector;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;

import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import android.widget.ImageView;
import android.widget.Toast;

public class Splash extends Activity {

	// flag for Internet connection status
	Boolean isInternetPresent = false;
	ConnectionDetector cd;

	boolean animationDone, gcmRegDone;

	AnimationDrawable splashAnimation;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// setContentView(R.layout.resturent_new_z);

		setContentView(R.layout.splash);

		ImageView splashImage = (ImageView) findViewById(R.id.icon);
		splashImage.setBackgroundResource(R.drawable.splash_anim);
		splashAnimation = (AnimationDrawable) splashImage.getBackground();
		// splashAnimation.start();
		startAndtrackAnimationDone(splashAnimation);

		if (!checkNetConnection())
			return;

		// gcm
		context = getApplicationContext();
		regid = getRegistrationId(this);

		gcmRegistration();

		/*
		 * new Handler().postDelayed(new Runnable() {
		 * 
		 * @Override public void run() { final Intent mainIntent = new
		 * Intent(Splash.this, Home.class);
		 * Splash.this.startActivity(mainIntent); Splash.this.finish();
		 * 
		 * } }, 10000);
		 */

	}



	private void startAndtrackAnimationDone(AnimationDrawable anim) {
		anim.start();
		final AnimationDrawable a = anim;
		int timeBetweenChecks = 300;
		final Handler h = new Handler();
		h.postDelayed(new Runnable() {
			public void run() {
				if (a.getCurrent() != a.getFrame(a.getNumberOfFrames() - 1)) {
					startAndtrackAnimationDone(a);
				} else {
					// Toast.makeText(getApplicationContext(),
					// "ANIMATION DONE!", Toast.LENGTH_SHORT).show();

					animationDone = true;

					if (!checkNetConnection()) {
						gcmRegDone = true;
					}

					if (animationDone && gcmRegDone) {
						h.removeCallbacksAndMessages(null);
						regOrHome();
						animationDone = false;
					} else {


						
						a.stop();
						a.setOneShot(false);
						startAndtrackAnimationDone(a);
					}

				}
			}
		}, timeBetweenChecks);
	}

	void regOrHome() {
		// SharedPreferences sharedpreferences =
		// getSharedPreferences(RegistrationActivity.LET_EAT_PREFERENCES,
		// Context.MODE_PRIVATE);

		if (!new SavedPrefernce(this).isUserExists()) {

		//	StaticObjects.callRegAndGoHome(Splash.this);
			Intent intent=new Intent(this, RegistrationOptionActivity.class);
			intent.putExtra(StaticObjects.NEV_TYPE_KEY, StaticObjects.NEV_TYPE_HOME);
			StaticObjects.nev_type=StaticObjects.NEV_TYPE_HOME;
			startActivity(intent);
			finish();
			
		} else {
			final Intent mainIntent = new Intent(Splash.this, HomeT.class);
			startActivity(mainIntent);
			finish();
		}
		
		/*else if(new SavedPrefernce(this).getDistrictID().isEmpty() && new SavedPrefernce(this).getLocationId().isEmpty()){
			Intent intent=new Intent(Splash.this, DetectAddressActivity.class);
			startActivity(intent);
			finish();
		}*/
	}

	private void checkNetConnectionShowToast() {
		// check internet
		cd = new ConnectionDetector(getApplicationContext());

		// Check if Internet present
		isInternetPresent = cd.isConnectingToInternet();

		if (!isInternetPresent) {
			// Internet Connection is not present
			Toast.makeText(this, getString(R.string.internetConMsg),
					Toast.LENGTH_SHORT).show();

			return;
		}
	}

	boolean checkNetConnection() {
		cd = new ConnectionDetector(getApplicationContext());

		// Check if Internet present
		return cd.isConnectingToInternet();

	}

	// gcm
	public static final String EXTRA_MESSAGE = "message";
	public static final String PROPERTY_REG_ID = "registration_id";
	private static final String PROPERTY_APP_VERSION = "appVersion";
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	/**
	 * Substitute you own sender ID here. This is the project number you got
	 * from the API Console, as described in "Getting Started."
	 */
	String SENDER_ID = "812541912855";

	/**
	 * Tag used on log messages.
	 */
	static final String TAG = "GCM";

	// TextView mDisplay;
	GoogleCloudMessaging gcm;
	AtomicInteger msgId = new AtomicInteger();
	SharedPreferences prefs;
	Context context;

	String regid;

	void gcmRegistration() {

		// sendNotification("demo");
		// Check device for Play Services APK. If check succeeds, proceed with
		// GCM registration.
		if (checkPlayServices()) {
			gcm = GoogleCloudMessaging.getInstance(this);
			regid = getRegistrationId(context);

			if (regid.isEmpty()) {
				registerInBackground();
			} else {
				gcmRegDone = true;
			}

		} else {
			Log.i(TAG, "No valid Google Play Services APK found.");
		}

		new AsyncTask<Object, Object, Object>() {
			String msg = "";

			@Override
			protected String doInBackground(Object... params) {

				try {
					Bundle data = new Bundle();
					data.putString("my_message", "Hello World");
					data.putString("my_action",
							"com.google.android.gcm.demo.app.ECHO_NOW");
					String id = Integer.toString(msgId.incrementAndGet());
					if (gcm != null)
						gcm.send(SENDER_ID + "@gcm.googleapis.com", id, data);
					msg = "Sent message";
				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
				}
				return msg;
			}

			@Override
			protected void onPostExecute(Object result) {

				// gcmRegDone=true;

			};

		}.execute();

	}

	/**
	 * Check the device to make sure it has the Google Play Services APK. If it
	 * doesn't, display a dialog that allows users to download the APK from the
	 * Google Play Store or enable it in the device's system settings.
	 */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,PLAY_SERVICES_RESOLUTION_REQUEST).show();
				
			} else {
				Log.i(TAG, "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}

	/**
	 * Gets the current registration ID for application on GCM service.
	 * <p>
	 * If result is empty, the app needs to register.
	 * 
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	private String getRegistrationId(Context context) {
		final SharedPreferences prefs = getGCMPreferences(context);
		String registrationId = prefs.getString(PROPERTY_REG_ID, "");
		if (registrationId.isEmpty()) {
			Log.i(TAG, "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
				Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i(TAG, "App version changed.");
			return "";
		}
		return registrationId;
	}

	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	private SharedPreferences getGCMPreferences(Context context) {
		// This sample app persists the registration ID in shared preferences,
		// but
		// how you store the regID in your app is up to you.
		return getSharedPreferences(PushActivity.class.getSimpleName(),
				Context.MODE_PRIVATE);
	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration ID and app versionCode in the application's
	 * shared preferences.
	 */

	/**
	 * Sends the registration ID to your server over HTTP, so it can use
	 * GCM/HTTP or CCS to send messages to your app. Not needed for this demo
	 * since the device sends upstream messages to a server that echoes back the
	 * message using the 'from' address in the message.
	 */
	private void registerInBackground() {

		new AsyncTask<Object, Object, Object>() {
			String msg = "";
			String regResponse = "";

			@Override
			protected Object doInBackground(Object... params) {

				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(context);
					}
					regid = gcm.register(SENDER_ID);
					msg = "Device registered, registration ID=" + regid;

					// You should send the registration ID to your server over
					// HTTP,
					// so it can use GCM/HTTP or CCS to send messages to your
					// app.
					// The request to your server should be authenticated if
					// your app
					// is using accounts.

					regResponse = pushRegistration(regid);

					// For this demo: we don't need to send it because the
					// device
					// will send upstream messages to a server that echo back
					// the
					// message using the 'from' address in the message.

					// Persist the regID - no need to register again.
					storeRegistrationId(context, regid);
				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
					// If there is an error, don't just keep trying to register.
					// Require the user to click a button again, or perform
					// exponential back-off.
				}
				return msg;

			}

			@Override
			protected void onPostExecute(Object result) {
				// mDisplay.append(msg + "\n");

				/*
				 * if(regResponse.equals("success")){
				 * Toast.makeText(Splash.this, "Success",
				 * Toast.LENGTH_LONG).show(); }else{ Toast.makeText(Splash.this,
				 * "Fail", Toast.LENGTH_LONG).show(); }
				 */
				gcmRegDone = true;

			}
		}.execute();
	}

	/**
	 * Stores the registration ID and app versionCode in the application's
	 * {@code SharedPreferences}.
	 * 
	 * @param context
	 *            application's context.
	 * @param regId
	 *            registration ID
	 */
	private void storeRegistrationId(Context context, String regId) {
		final SharedPreferences prefs = getGCMPreferences(context);
		int appVersion = getAppVersion(context);
		Log.i(TAG, "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PROPERTY_REG_ID, regId);
		editor.putInt(PROPERTY_APP_VERSION, appVersion);
		editor.commit();
	}

	public String pushRegistration(String regToken) {

		String userid = new SavedPrefernce(this).getUserID();
		String postData = "token=" + regToken + "&userid=" + userid;

		JSONParser jParser = new JSONParser();

		String json = jParser.requestPost(FoodyResturent.PUSH_REG_URL,
				postData, 10000);
		try {
			if (json.startsWith("Error") || json.startsWith("failed")) {
				System.out.println("pushRegistration failed");
				Log.e("pushRegistration", "failed");
				return "failed";
			} else if (json.startsWith("success")) {
				System.out.println("pushRegistration success");
				Log.e("pushRegistration", "success");
				return "success";
			} else {
				System.out.println("pushRegistration failed");
				Log.e("pushRegistration", "failed");
				return "failed";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "failed";
	}

}
