package com.mcc.letseat;


import com.mcc.letseat.R;
import com.foody.nearby.AlertDialogManager;
import com.foody.nearby.ConnectionDetector;
import com.foody.nearby.NearByplaceListView;

import com.google.android.gms.ads.*;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;



public class Main extends Activity{

	WebView wv;
	ProgressDialog progress;
	AdView adView;
	
	// flag for Internet connection status
		Boolean isInternetPresent = false;

	// Connection detector class
	ConnectionDetector cd;
		
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.foodywebview);
		getActionBar().hide();
		
		adView = (AdView)this.findViewById(R.id.adView);
		
		//check internet
		cd = new ConnectionDetector(getApplicationContext());

		// Check if Internet present
		isInternetPresent = cd.isConnectingToInternet();
		if (!isInternetPresent) {
			// Internet Connection is not present
			alert.showAlertDialog(Main.this, "Internet Connection Error",
					"Please connect to working Internet connection", false);
			
			// stop executing code by return
			return;
		}
		
		String foodyUrl=getIntent().getStringExtra("url");
		
		
		
		wv = (WebView) findViewById(R.id.MyWebView);
		progress = new ProgressDialog(this);
		
		wv.getSettings().setJavaScriptEnabled(true);
		wv.setWebViewClient(new VideoWebViewClient());
		wv.getSettings().setUserAgentString(
				"Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
		wv.loadUrl(foodyUrl);
		wv.setOnTouchListener(new View.OnTouchListener() {

			public boolean onTouch(View v, MotionEvent event) {
				WebView.HitTestResult hr = ((WebView) v).getHitTestResult();

				// Log.e(TAG, "getExtra = "+ hr.getExtra() + "\t\t Type=" +
				// hr.getType());
				return false;
			}
		});
		
		
		
		AdRequest adRequest = new AdRequest.Builder()	
		.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
	    .build();
		
	    
	  adView.loadAd(adRequest);
	  
	  adView.setAdListener(new AdListener() {
		  public void onAdLoaded() {}
		  public void onAdFailedToLoad(int errorcode) {
			  System.out.println("Error:"+errorcode);
		  }
		  
		 
		});
	}

	private class VideoWebViewClient extends WebViewClient {
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			super.onPageStarted(view, url, favicon);
			//progress.setTitle("Loading..");
			progress.setMessage("Please Wait...");
			progress.show();
			
			
			
			
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (url.equalsIgnoreCase("http://foody.asholei.com/mobile/NearBy.php")) {
				// your code
				//wv.loadUrl("https://www.google.com/");
				Intent i=new Intent(Main.this,NearByplaceListView.class);
				startActivity(i);
				return true;
			}else if(url.startsWith("tel:")){
				Intent callIntent = new Intent(Intent.ACTION_CALL);
		        callIntent.setData(Uri.parse(url));
		        startActivity(callIntent);
		        return true;
			}

			return false;
		}
		

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			progress.cancel();
			
		}

	}

	@Override
	public void onBackPressed() {

		if (wv!=null && wv.canGoBack()) {

			wv.goBack();
			
			return;
		}
		else {
			finish();
		}	
	}
	
	
	
	@Override
	public void onPause() {
	  adView.pause();
	  super.onPause();
	}

	@Override
	public void onResume() {
	  super.onResume();
	  adView.resume();
	}

	@Override
	public void onDestroy() {
	  adView.destroy();
	  super.onDestroy();
	}
	
}


