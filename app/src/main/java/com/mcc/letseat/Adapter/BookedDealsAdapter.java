package com.mcc.letseat.Adapter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.CusineMenu;
import com.foody.jsondata.Offer;
import com.foody.jsondata.Restaurent;
import com.foody.jsondata.user.UserBookedDeal;
import com.mcc.letseat.R;
import com.mcc.user.SavedPrefernce;


import com.squareup.picasso.Picasso;

public class BookedDealsAdapter extends ArrayAdapter<UserBookedDeal> implements Filterable {
	
	public final int SORT_BY_NAME=0, SORT_BY_NAME_REVERSE=1, SORT_BY_COST_LOWEST=2, SORT_BY_COST_HIGHEST=3, SORT_BY_CAPACITY_HIGHEST=4,SORT_BY_CAPACITY_LOWEST=5, SORT_BY_DEALS_PROVIDER=6;
	private int sortBY; 
	
	//
	//private ValueFilter valueFilter;
	

	Typeface tfLight ,tfNormal ;	
	Context context;
	List<UserBookedDeal> bookedDeals;
	
	private int lastPosition=-1;

	public BookedDealsAdapter(Context context, List<UserBookedDeal> bookedDeals) {
		super(context, 0 , bookedDeals);
		this.context = context;
		this.bookedDeals = bookedDeals;
		
		//sort by location by default
		//sortAnItemTop(bookedDeals, new SavedPrefernce((Activity)context).getLocationName());
		
		
		tfLight = Typeface.createFromAsset(context.getAssets(), StaticObjects.fontPath_SEGOEUI_light);
		 tfNormal = Typeface.createFromAsset(context.getAssets(), StaticObjects.fontPath_SEGOEUI);
	}
	
	
	void sortAnItemTop(List<Offer> list, String value){
			 
			 List<Offer> listTop = new ArrayList<Offer>();
			 
			 
			 for (int i = 0; i < list.size(); i++) {
				
			     if (list.get(i).RestLoc.equals(value)) {           
			    	 listTop.add(0,  list.remove(i));
			     }        
			 }

			 for (int i = 0; i < listTop.size(); i++) {
			     list.add(0, listTop.get(i));
			 }
		 }
	
	 @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	       // Get the data item for this position
		 UserBookedDeal bookedDeal = getItem(position);    
	      
	    // Check if an existing view is being reused, otherwise inflate the view
	       ViewHolder viewHolder; // view lookup cache stored in tag
	       if (convertView == null) {
	          viewHolder = new ViewHolder();
	          LayoutInflater inflater = LayoutInflater.from(getContext());
	          convertView = inflater.inflate(R.layout.list_item_booked_deals, parent, false);

			   viewHolder.root = (RelativeLayout)convertView.findViewById(R.id.layoutOfferList);

			   viewHolder.txtOfferFor= (TextView)convertView.findViewById(R.id.txtOfferFor);
			   viewHolder.txtOfferFor.setTypeface(tfNormal);

			   viewHolder.txtOfferProvider = (TextView)convertView.findViewById(R.id.txtOfferProvider);
			   viewHolder.txtOfferProvider.setTypeface(tfNormal);

			   viewHolder.txtCuisineType = (TextView)convertView.findViewById(R.id.txtCuisineType);
			   viewHolder.txtCuisineType.setTypeface(tfNormal);

			   viewHolder.txtDiscount = (TextView)convertView.findViewById(R.id.txtDiscount);
			   viewHolder.txtDiscount.setTypeface(tfNormal);

			   viewHolder.txtCost = (TextView)convertView.findViewById(R.id.txtCost);
			   viewHolder.txtCost.setTypeface(tfNormal);


			   viewHolder.imageView = (ImageView)convertView.findViewById(R.id.imageViewOfferList);



	          viewHolder.button1= (Button)convertView.findViewById(R.id.button1);
	          viewHolder.button1.setTypeface(tfNormal);

	          viewHolder.progressBarPreparing = (ProgressBar)convertView.findViewById(R.id.progressBarPreparing);
	  		
	  		
	          
	          convertView.setTag(viewHolder);
	       } else {
	           viewHolder = (ViewHolder) convertView.getTag();
	       }
	   
	   
		if(bookedDeal!=null){
			viewHolder.txtOfferFor.setText(bookedDeal.ResName);
			viewHolder.txtOfferProvider.setText(bookedDeal.DisName);
			//viewHolder.textView3.setText(bookedDeal.DisCompany);
			viewHolder.txtDiscount.setText(bookedDeal.DisPercent);
			viewHolder.txtCuisineType.setText(bookedDeal.DisDetail);
			viewHolder.txtCost.setText("Tracking Code: "+bookedDeal.TrackingCode);
			
			if(!bookedDeal.isRedeem()){
				viewHolder.button1.setText("Redeem");
				  viewHolder.progressBarPreparing.setVisibility(View.GONE);
				
				
			}

			//loading image
			if(!TextUtils.isEmpty(bookedDeal.Img)){
				Picasso.with(context)
						.load(bookedDeal.Img)
						.placeholder(R.drawable.lazy_ic)
						.error(R.drawable.lazy_ic)
						.into(viewHolder.imageView);
			}
			
			
			
		}else{
			/*viewHolder.textView1.setText("");
			viewHolder.textView2.setText("");
			viewHolder.textView3.setText("");
			viewHolder.textView4.setText("");
			viewHolder.textView5.setText("");
			viewHolder.textView6.setText("");*/
		}
		
		
		//apply animation on current view
		 Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
		 convertView.startAnimation(animation);
		 lastPosition = position;
	       // Return the completed view to render on screen
	       return convertView;
	   }
	 
	/* class ViewHolder{
		 
		 Button button1;
			
			TextView textView1;
			TextView textView2 ;
			
			TextView textView3 ;
			TextView textView4;
			TextView textView5;
			TextView textView6;
			ProgressBar progressBarPreparing;
			
			
			//ImageView imageView ;
		 
		 
	 }*/

	class ViewHolder{

		RelativeLayout root;

		TextView txtOfferFor;
		TextView txtOfferProvider ;
		TextView txtCuisineType ;
		TextView txtCost;

		TextView txtDiscount ;

		ImageView imageView ;


		Button button1;
		ProgressBar progressBarPreparing;


	}
	 
	
/*	 
	 public int getSortBY() {
			return sortBY;
		}

		public void setSortBY(int sortBY) {
			if(sortBY==this.sortBY)
				return;
			
			this.sortBY = sortBY;
			sort(getSortBY());
		}
		
		
	 
		public void sort(final int sortBy){
			 this.sort(new Comparator<Offer>() {

				@Override
				public int compare(Offer lhs, Offer rhs) {
					// TODO Auto-generated method stub
					if(sortBy == SORT_BY_COST_LOWEST){
						float rl=Float.parseFloat(lhs.Cost4Two) ;
						float rr=Float.parseFloat(rhs.Cost4Two) ;
					
						return (int) (rl - rr ) ;	
					}else if(sortBy == SORT_BY_COST_HIGHEST){
						float rl=Float.parseFloat(lhs.Cost4Two) ;
						float rr=Float.parseFloat(rhs.Cost4Two) ;
					
						return (int) (rr - rl ) ;	
					}
					
					else if(sortBy == SORT_BY_DEALS_PROVIDER){
						return lhs.DisFrom.compareTo(rhs.DisFrom);
					}else if(sortBy == SORT_BY_CAPACITY_HIGHEST){
						int compare=0;
						try {
							int rl=Integer.parseInt(lhs.Capacity) ;
							int rr=Integer.parseInt(rhs.Capacity) ;
							
							compare=  (rr - rl) ;
						} catch (Exception e) {
							// TODO: handle exception
						}
						return compare;
						
					}else if(sortBy == SORT_BY_CAPACITY_LOWEST){
						int compare=0;
						try {
							int rl=Integer.parseInt(lhs.Capacity) ;
							int rr=Integer.parseInt(rhs.Capacity) ;
							
							compare=  (rl - rr) ;
						} catch (Exception e) {
							// TODO: handle exception
						}
						return compare;
						
					}else if(sortBy == SORT_BY_NAME_REVERSE){ 
						return rhs.RestName.compareTo(lhs.RestName);
					}else 
						return lhs.RestName.compareTo(rhs.RestName);
				}
			});
		 }
	 
	
	 private class ValueFilter extends Filter {
		 //filter adapter data
		 ArrayList<Offer> sourceOfferList;
		 
		 public ValueFilter(List<Offer> objects) {
			 sourceOfferList = new ArrayList<Offer>();
				synchronized (this) {
					sourceOfferList.addAll(objects);
				}
			}
		 
		 

	     //Invoked in a worker thread to filter the data according to the constraint.
	     @Override
	     protected FilterResults performFiltering(CharSequence constraint) {
	         FilterResults results=new FilterResults();
	         String filterSeq = constraint.toString().toLowerCase();
	         if(filterSeq!=null && filterSeq.length()>0){
	        	 
	        	 ArrayList<Offer> filter = new ArrayList<Offer>();

	        	 
	        	 for(int i=0;i<sourceOfferList.size();i++){	             	 
	             	//String lwName = offersFiltered.get(i).DisFrom.toLowerCase(Locale.getDefault());	             	
	             	String disFrom = sourceOfferList.get(i).DisFrom.toLowerCase();
	             	  if(disFrom.equals(filterSeq)) {
	             		 filter.add(sourceOfferList.get(i));	                     

	                 }
	             }


	             results.count=filter.size();

	             results.values=filter;

	         }else{
	        	// add all objects
					synchronized (this) {

			             results.count=sourceOfferList.size();
			             results.values=sourceOfferList;
					}

	         }

	         return results;
	     }


	     //Invoked in the UI thread to publish the filtering results in the user interface.
	     @SuppressWarnings("unchecked")
	     @Override
	     protected void publishResults(CharSequence constraint,
	             FilterResults results) {

	    	 ArrayList<Offer> filter =(ArrayList<Offer>) results.values;

	         notifyDataSetChanged();
				clear();
				for (int i = 0, l = filter.size(); i < l; i++)
					add((Offer) filter.get(i));
				notifyDataSetInvalidated();
	         
	        // addAll(offers);


	     }

	 }
	 
	//Returns a filter that can be used to constrain data with a filtering pattern.
	 @Override
	 public Filter getFilter() {

	     if(valueFilter==null) {

	         valueFilter=new ValueFilter(bookedDeals);
	     }

	     return valueFilter;
	 }*/

}
