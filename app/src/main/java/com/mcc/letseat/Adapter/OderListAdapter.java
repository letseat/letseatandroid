package com.mcc.letseat.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.CusineMenu;
import com.mcc.letseat.R;
import com.mcc.letseat.order.Order;
import com.mcc.libs.CF;
import com.mcc.user.SavedPrefernce;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class OderListAdapter extends ArrayAdapter<Order>
		 {



	Typeface tfLight, tfNormal;
	Context context;
	List<Order> cuisineRestList;

	private int lastPosition = -1;


	public OderListAdapter(Context context, List<Order> orders) {
		super(context, 0, orders);
		this.context = context;
		this.cuisineRestList = orders;

		tfLight = Typeface.createFromAsset(context.getAssets(),
				StaticObjects.fontPath_SEGOEUI_light);
		tfNormal = Typeface.createFromAsset(context.getAssets(),
				StaticObjects.fontPath_SEGOEUI);



	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Get the data item for this position
		Order order = getItem(position);

		// Check if an existing view is being reused, otherwise inflate the view
		ViewHolder viewHolder; // view lookup cache stored in tag
		if (convertView == null) {
			viewHolder = new ViewHolder();
			LayoutInflater inflater = LayoutInflater.from(getContext());
			convertView = inflater.inflate(R.layout.layout_order_list,
					parent, false);



			viewHolder.txtOrderItemName = (TextView) convertView
					.findViewById(R.id.txtOrderItemName);
			viewHolder.txtOrderItemName.setTypeface(tfNormal);

			viewHolder.txtOrderItemPrice = (TextView) convertView
					.findViewById(R.id.txtOrderItemPrice);
			viewHolder.txtOrderItemPrice.setTypeface(tfNormal);



			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}




		// Populate the data into the template view using the data object
		/*if (position % 2 == 0) {
			viewHolder.root.setBackgroundColor(Color.parseColor("#FCFCFC"));
		} else {
			viewHolder.root.setBackgroundColor(Color.parseColor("#FFFFFF"));
		}*/

		// String ww =aa.get(pos);
		if (order != null) {
			viewHolder.txtOrderItemName.setText(order.ItemName);
			viewHolder.txtOrderItemPrice.setText(order.Currency+" "+(order.quntity * order.ItemPrice) );




			// loading image
			/*if (!TextUtils.isEmpty(cuisineRest.IMG)) {
				Picasso.with(context).load(cuisineRest.IMG)
						.placeholder(R.drawable.lazy_ic)
						.error(R.drawable.lazy_ic).into(viewHolder.imageView);
			}*/

			// Log.e("offer image link",
			// "link: "+aa.get(pos).get(listTags[4])+"0"+aa.get(pos).get(listTags[0])+"<1:"+aa.get(pos).get(listTags[1])+"<2:"+aa.get(pos).get(listTags[2])+"<3:"+aa.get(pos).get(listTags[3]));
			// System.out.println(""+aa.get(pos).get(listTags[4]));

		} else {
			viewHolder.txtOrderItemName.setText("");
			viewHolder.txtOrderItemName.setText("");


		}

		/*
		 * name.setText(aa.get(pos).get("name"));
		 * dis.setText(aa.get(pos).get("distance"));
		 * ref.setText(aa.get(pos).get("reference"));
		 */

		// apply animation on current view
		Animation animation = AnimationUtils.loadAnimation(context,
				(position > lastPosition) ? R.anim.up_from_bottom
						: R.anim.down_from_top);
		convertView.startAnimation(animation);
		lastPosition = position;
		// Return the completed view to render on screen
		return convertView;
	}

	class ViewHolder {

		LinearLayout root;
		TextView txtOrderItemName;
		TextView txtOrderItemPrice;

	}



}
