package com.mcc.letseat.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.Restaurent;
import com.mcc.letseat.R;
import com.mcc.libs.CF;
import com.mcc.user.SavedPrefernce;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

public class RestaurantAdapter extends ArrayAdapter<Restaurent> implements
        Filterable {

    public final int SORT_BY_NAME = 0, SORT_BY_NAME_REVERSE = 1,
            SORT_BY_COST_LOWEST = 2, SORT_BY_COST_HIGHEST = 3,
            SORT_BY_CAPACITY_HIGHEST = 4, SORT_BY_CAPACITY_LOWEST = 5,
            SORT_BY_RATING = 6;
    private int sortBY=-1;

    //
    private ValueFilter valueFilter;

    Typeface tfLight, tfNormal;
    Context context;
    List<Restaurent> cuisineRestList;

    private int lastPosition = -1;

    private String currentLocation;



    @Override
    public int getCount() {
        return cuisineRestList.size();
    }



    public RestaurantAdapter(Context context, List<Restaurent> cuisineRestList) {
        super(context, 0, cuisineRestList);
        this.context = context;
        this.cuisineRestList = cuisineRestList;


        tfLight = Typeface.createFromAsset(context.getAssets(), StaticObjects.fontPath_SEGOEUI_light);
        tfNormal = Typeface.createFromAsset(context.getAssets(),StaticObjects.fontPath_SEGOEUI);

        currentLocation = new SavedPrefernce(((Activity)context)).getLocationName();
        setSortBY(SORT_BY_NAME);

    }




    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Restaurent rest = getItem(position);
        boolean showSeparator = false;
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_item_resttaurant,
                    parent, false);

            viewHolder.root = (RelativeLayout) convertView
                    .findViewById(R.id.layoutOfferList);

            viewHolder.txtOfferFor = (TextView) convertView
                    .findViewById(R.id.txtOfferFor);
            viewHolder.txtOfferFor.setTypeface(tfNormal);

            viewHolder.txtOfferProvider = (TextView) convertView
                    .findViewById(R.id.txtOfferProvider);
            viewHolder.txtOfferProvider.setTypeface(tfNormal);

            viewHolder.txtCuisineType = (TextView) convertView
                    .findViewById(R.id.txtCuisineType);
            viewHolder.txtCuisineType.setTypeface(tfNormal);

            viewHolder.txtCost = (TextView) convertView
                    .findViewById(R.id.txtCost);
            viewHolder.txtCost.setTypeface(tfNormal);

            viewHolder.txtCapacity = (TextView) convertView
                    .findViewById(R.id.txtCapacity);
            viewHolder.txtCapacity.setTypeface(tfNormal);

            viewHolder.imgOpenClose = (ImageView) convertView
                    .findViewById(R.id.imgOpenClose);

            viewHolder.imageView = (ImageView) convertView
                    .findViewById(R.id.imageViewOfferList);

            viewHolder.ratingBarResListItem = (RatingBar) convertView
                    .findViewById(R.id.ratingBarResListItem);
            viewHolder.txtHeader  = (TextView)  convertView
                    .findViewById(R.id.txtHeader);
            viewHolder.txtHeader.setTypeface(tfNormal);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }








        //Group header view
        viewHolder.txtHeader.setVisibility(View.GONE);
        if (rest.getHeaderViewStatus()==View.VISIBLE) {
            viewHolder.txtHeader.setVisibility(View.VISIBLE);

            if( rest.RestLoc.equals(currentLocation) ){
                viewHolder.txtHeader.setText(rest.RestLoc);
            }else{
                viewHolder.txtHeader.setText("Nearby Areas");
            }
        }
        else {
            viewHolder.txtHeader.setVisibility(View.GONE);
        }



        // Populate the data into the template view using the data object
        if (position % 2 == 0) {
            viewHolder.root.setBackgroundColor(Color.parseColor("#FCFCFC"));
        } else {
            viewHolder.root.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }

        // String ww =aa.get(pos);
        if (rest != null) {
            viewHolder.txtOfferFor.setText(rest.RestName);
            viewHolder.txtOfferProvider.setText(rest.RestLoc);

            viewHolder.txtCost
                    .setVisibility(TextUtils.isEmpty(rest.Cost4Two) ? View.GONE
                            : View.VISIBLE);
            viewHolder.txtCost.setText(rest.Cost4Two);

            viewHolder.txtCapacity.setVisibility(TextUtils
                    .isEmpty(rest.Capacity) ? View.GONE : View.VISIBLE);
            viewHolder.txtCapacity.setText(rest.Capacity);

            float rating = Float.parseFloat(rest.Rating);

            viewHolder.ratingBarResListItem.setRating(rating
                    / StaticObjects.RATING_OFFSET);

            if (rest.getOpenOrClose() == CF.OPEN) {
                viewHolder.imgOpenClose.setImageResource(R.mipmap.ic_open);
            } else {
                viewHolder.imgOpenClose.setImageResource(R.mipmap.ic_close);
            }

            // loading image
            if (!TextUtils.isEmpty(rest.IMG)) {
                Picasso.with(context).load(rest.IMG)
                        .placeholder(R.drawable.lazy_ic)
                        .error(R.drawable.lazy_ic).into(viewHolder.imageView);
            }

            // Log.e("offer image link",
            // "link: "+aa.get(pos).get(listTags[4])+"0"+aa.get(pos).get(listTags[0])+"<1:"+aa.get(pos).get(listTags[1])+"<2:"+aa.get(pos).get(listTags[2])+"<3:"+aa.get(pos).get(listTags[3]));
            // System.out.println(""+aa.get(pos).get(listTags[4]));

        } else {
            viewHolder.txtOfferFor.setText("");
            viewHolder.txtOfferProvider.setText("");
            viewHolder.txtCuisineType.setText("");

        }

		/*
         * name.setText(aa.get(pos).get("name"));
		 * dis.setText(aa.get(pos).get("distance"));
		 * ref.setText(aa.get(pos).get("reference"));
		 */

        // apply animation on current view
        Animation animation = AnimationUtils.loadAnimation(context,
                (position > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_from_top);
        convertView.startAnimation(animation);
        lastPosition = position;
        // Return the completed view to render on screen
        return convertView;
    }

    class ViewHolder {

        RelativeLayout root;
        TextView txtHeader;
        TextView txtOfferFor;
        RatingBar ratingBarResListItem;
        TextView txtOfferProvider;
        TextView txtCuisineType;
        TextView txtCost;
        TextView txtCapacity;
        ImageView imageView;
        ImageView imgOpenClose;


    }

    public int getSortBY() {
        return sortBY;
    }

    public void setSortBY(int sortBY) {
        if (sortBY == this.sortBY)
            return;

        this.sortBY = sortBY;
        sort(getSortBY());
        sortAnItemTop(cuisineRestList, currentLocation);
        addGroupHeaderStatus(cuisineRestList, currentLocation);
    }

    public void sort(final int sortBy) {
        this.sort(new Comparator<Restaurent>() {

            @Override
            public int compare(Restaurent lhs, Restaurent rhs) {
                // TODO Auto-generated method stub
                if (sortBy == SORT_BY_COST_LOWEST) {
                    float rl = Float.parseFloat(lhs.Cost4Two);
                    float rr = Float.parseFloat(rhs.Cost4Two);

                    return Float.compare(rl , rr);
                } else if (sortBy == SORT_BY_COST_HIGHEST) {
                    float rl = Float.parseFloat(lhs.Cost4Two);
                    float rr = Float.parseFloat(rhs.Cost4Two);

                    return Float.compare(rr, rl);
                } else if (sortBy == SORT_BY_RATING) {
                    float rl = Float.parseFloat(lhs.Rating);
                    float rr = Float.parseFloat(rhs.Rating);

                    return Float.compare (rr , rl);
                } else if (sortBy == SORT_BY_CAPACITY_HIGHEST) {
                    int compare = 0;
                    try {
                        int rl = Integer.parseInt(lhs.Capacity);
                        int rr = Integer.parseInt(rhs.Capacity);


                        compare= Integer.valueOf(rr).compareTo(Integer.valueOf(rl));
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    return compare;

                } else if (sortBy == SORT_BY_CAPACITY_LOWEST) {
                    int compare = 0;
                    try {
                        int rl = Integer.parseInt(lhs.Capacity);
                        int rr = Integer.parseInt(rhs.Capacity);

                        //compare = Integer.compare(rl, rr);
                        compare= Integer.valueOf(rl).compareTo(Integer.valueOf(rr));
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    return compare;

                } else if (sortBy == SORT_BY_NAME_REVERSE) {
                    return rhs.RestName.compareTo(lhs.RestName);
                } else if (sortBy == SORT_BY_NAME)
                    return lhs.RestName.compareToIgnoreCase(rhs.RestName);
                else
                    return lhs.RestName.compareToIgnoreCase(rhs.RestName);
            }
        });
    }

    void sortAnItemTop(List<Restaurent> list, String value) {

        List<Restaurent> listTop = new ArrayList<Restaurent>();

        for (int i = 0; i < list.size(); i++) {

            if (list.get(i).RestLoc.equals(value)) {
                listTop.add(0, list.remove(i));
            }
        }

        for (int i = 0; i < listTop.size(); i++) {
            list.add(0, listTop.get(i));
        }

    }


    private void addGroupHeaderStatus(List<Restaurent> list, String value){
        boolean myLoc = false,  otherLoc =false;
        for (int i = 0; i < list.size(); i++) {

            if (!myLoc && list.get(i).RestLoc.equals(value)) {
                myLoc=true;
                list.get(i).setHeaderViewStatus(View.VISIBLE);
            }else if (!otherLoc && !list.get(i).RestLoc.equals(value)) {
                otherLoc=true;
                list.get(i).setHeaderViewStatus(View.VISIBLE);
            } else{
                list.get(i).setHeaderViewStatus(View.GONE);
            }


        }
    }

    private class ValueFilter extends Filter {
        // filter adapter data
        ArrayList<Restaurent> sourceOfferList;

        public ValueFilter(List<Restaurent> objects) {
            sourceOfferList = new ArrayList<Restaurent>();
            synchronized (this) {
                sourceOfferList.addAll(objects);
            }
        }

        // Invoked in a worker thread to filter the data according to the
        // constraint.
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            String filterSeq = constraint.toString().toLowerCase();
            if (filterSeq != null && filterSeq.length() > 0 && !filterSeq.equals("0")) {

                ArrayList<Restaurent> filter = new ArrayList<Restaurent>();

                for (int i = 0; i < sourceOfferList.size(); i++) {
                    String matchField = "";

                    if (filterSeq.equals("1")) {
                        matchField = ("" + sourceOfferList.get(i).getOpenOrClose());
                        if (matchField.equals("1")) {
                            filter.add(sourceOfferList.get(i));

                        }

                    } else if (filterSeq.equals("2")) {
                        matchField = sourceOfferList.get(i).Discount;
                        if (matchField.equals("Yes")) {
                            filter.add(sourceOfferList.get(i));
                        }
                    }
                }

                results.count = filter.size();

                results.values = filter;

            } else {
                // add all objects
                synchronized (this) {

                    results.count = sourceOfferList.size();
                    results.values = sourceOfferList;
                }

            }

            return results;
        }

        // Invoked in the UI thread to publish the filtering results in the user
        // interface.
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {

            ArrayList<Restaurent> filter = (ArrayList<Restaurent>) results.values;

            notifyDataSetChanged();
            clear();
            for (int i = 0, l = filter.size(); i < l; i++)
                add((Restaurent) filter.get(i));

            int currentSort=getSortBY();
            sortBY=-1;
            setSortBY(currentSort);

            notifyDataSetInvalidated();

            // addAll(offers);

        }

    }

    // Returns a filter that can be used to constrain data with a filtering
    // pattern.
    @Override
    public Filter getFilter() {

        if (valueFilter == null) {

            valueFilter = new ValueFilter(cuisineRestList);
        }

        return valueFilter;
    }

}
