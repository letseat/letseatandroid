package com.mcc.letseat;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.foody.AppData.StaticObjects;
import com.foody.nearby.AlertDialogManager;
import com.foody.nearby.DistrictListView;
import com.foody.nearby.SearchResturentListView;
import com.mcc.libs.ActivityWithSliding;
import com.mcc.libs.AnimationTween;
import com.mcc.libs.ExifUtil;
import com.mcc.user.PostTaskResPhoto;
import com.mcc.user.SavedPrefernce;

/**
 * Capture photo using camera
 * @author Arif
 *
 */
public class CapturePhotoActivity extends ActivityWithSliding {
	
	 private int mShortAnimationDuration; 

	
	// Progress dialog
	MyProgressDialog pDialog;
	
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();
	
	// Activity request codes
	 static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
	 static final int PIC_FROM_FILE_REQUEST_CODE  = 4;
	public static final int GET_RESTAURENT_ID_REQUEST_CODE  = 5;
	
	 static final int MEDIA_TYPE_IMAGE = 1;
	
	//keep track of cropping intent
	 static final int PIC_CROP_REQUEST_CODE = 2;
	 static final int PIC_EFFECT_REQUEST_CODE = 3;
	
	

	// directory name to store captured images and videos
	private static final String IMAGE_DIRECTORY_NAME = "lets_eat";

	private Uri fileUri, photoUri; // file url to store image/video

	private ImageView imgPreview, imageViewCamera, imageViewGallery;
	//private VideoView videoPreview;
	private Button btnCapturePicture, btnUploadPicture,btnCropPicture, btnEditPicture;
	private EditText txtCaption;
	TextView txtSelectReataurent;
	
	String userid;
	public static String resid;
	public static String resname;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_capture_photo);
		resid=resname=null;
		
		imageViewCamera=(ImageView) findViewById(R.id.imageViewCamera);
		imageViewCamera.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				captureImage();
			}
		});
		
		
		imageViewGallery=(ImageView) findViewById(R.id.imageViewGallery);
		imageViewGallery.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				imageFromFile();
			}
		});

		imgPreview = (ImageView) findViewById(R.id.imgPreview);
		imgPreview.setVisibility(View.INVISIBLE);
		//videoPreview = (VideoView) findViewById(R.id.videoPreview);
		btnCapturePicture = (Button) findViewById(R.id.btnCapturePicture);
		btnUploadPicture = (Button) findViewById(R.id.btnUploadPicture);
		
		btnUploadPicture.setVisibility(View.GONE);
		
		btnCropPicture= (Button) findViewById(R.id.btnCropPicture);
		btnEditPicture= (Button) findViewById(R.id.btnEditPicture);
		
		
		
		txtCaption=(EditText)findViewById(R.id.txtCaption);
		txtCaption.setVisibility(View.INVISIBLE);
		
		txtSelectReataurent=(TextView)findViewById(R.id.txtSelectReataurent);
		txtSelectReataurent.setVisibility(View.GONE);
		
		final String [] items			= new String [] {"Take from camera", "Select from gallery"};				
		ArrayAdapter<String> adapter	= new ArrayAdapter<String> (this, android.R.layout.select_dialog_item,items);
		AlertDialog.Builder builder		= new AlertDialog.Builder(this);
		
		builder.setTitle("Select Image");
		builder.setAdapter( adapter, new DialogInterface.OnClickListener() {
			public void onClick( DialogInterface dialog, int item ) { //pick from camera
				if (item == 0) {
					// capture picture
					captureImage();
					
				} else { //pick from file
					imageFromFile();
				}
			}
		} );
		
		final AlertDialog dialog = builder.create();

		/*
		 * Capture image button click event
		 */
		btnCapturePicture.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				
				dialog.show();
			}
		});
		
		btnUploadPicture.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// capture picture
				if(fileUri!=null){
					//Toast.makeText(CapturePhotoActivity.this, fileUri.toString()+"", Toast.LENGTH_LONG).show();
					uploadImage();
				}else{
					Toast.makeText(CapturePhotoActivity.this, "take a photo", Toast.LENGTH_SHORT).show();
					
				}
				
			}
		});
		
		btnCropPicture.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// capture picture
				if(fileUri!=null){
					
					performCrop(fileUri);
					
				}else{
					Toast.makeText(CapturePhotoActivity.this, "take a photo", Toast.LENGTH_SHORT).show();
					
				}
				
			}
		});
		
		
		btnEditPicture.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// capture picture
				if(fileUri!=null){
					
					performEffect(fileUri);
					
					
				}else{
					Toast.makeText(CapturePhotoActivity.this, "take a photo", Toast.LENGTH_SHORT).show();
					
				}
				
			}
		});
		
		/*
		 * Record video button click event
		 */
		

		// Checking camera availability
		if (!isDeviceSupportCamera()) {
			Toast.makeText(getApplicationContext(),
					"Sorry! Your device doesn't support camera",
					Toast.LENGTH_LONG).show();
			// will close the app if the device does't have camera
			finish();
		}
		
		//set animation duration
		mShortAnimationDuration = getResources().getInteger(
	                android.R.integer.config_longAnimTime);
		
		
		
		 // Get intent, action and MIME type
	    Intent intent = getIntent();
	    String action = intent.getAction();
	    String type = intent.getType();
	    
	    if (Intent.ACTION_SEND.equals(action) && type != null ) {
	        if (type.startsWith("image/")) {
	            processPicFromShare(intent); // Handle single image being sent
	        }
	    } 

		
	    userid=getIntent().getStringExtra("userid");
		resid=getIntent().getStringExtra("resid");
		
		
		
		
        
	}

	protected void performEffect(Uri uri) {
		// TODO Auto-generated method stub
		
		/*Intent i = new Intent(this, HelloEffects.class);
		i.setData(uri);		
		startActivityForResult(i, PIC_EFFECT_REQUEST_CODE);*/
		
	}

	protected void uploadImage() {
		
		
		
		String caption=txtCaption.getText().toString();
		
		if(userid==null || userid=="" ){
			
			SavedPrefernce savedPrefernce=new SavedPrefernce(CapturePhotoActivity.this);
			
	    	 if(!savedPrefernce.isUserExists() ){					
				StaticObjects.callRegForAction(this);	
				return;
		   	}else{				
				userid=new SavedPrefernce(this).getUserID();
				}
		}
		
		//Toast.makeText(this, "<res>"+resid+"<cap>", Toast.LENGTH_LONG).show();	
		if(resid==null || resid.isEmpty()|| resid==""){
			AnimationTween.shakeOnError(this, txtSelectReataurent);
			return;
		}
		
		
		
		if(userid!=null && userid!="" && resid!=null && resid!="" && caption!=null && caption!=""){
			
			
			
			
				//Toast.makeText(CapturePhotoActivity.this,"photo path:"+photoUri.getPath(), Toast.LENGTH_SHORT).show();
				new PostTaskResPhoto(this, getResources(), "uploading...", photoUri, userid, resid, caption, 2).execute() ;	
				
			
			
		}else{
			return;
		}
		
		
		
		
	}

	/**
	 * Checking device has camera hardware or not
	 * */
	private boolean isDeviceSupportCamera() {
		if (getApplicationContext().getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA)) {
			// this device has a camera
			return true;
		} else {
			// no camera on this device
			return false;
		}
	}

	/*
	 * Capturing Camera Image will lauch camera app requrest image capture
	 */
	private void captureImage() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
		photoUri=fileUri;
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		// start the image capture Intent
		startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
	}
	
	private void imageFromFile(){
		Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		
        intent.setType("image/*");
        //intent.setAction(Intent.ACTION_GET_CONTENT);
        
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
		photoUri=fileUri;
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        
        startActivityForResult(Intent.createChooser(intent, "Complete action using"), PIC_FROM_FILE_REQUEST_CODE);
	}

	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		// save file url in bundle as it will be null on scren orientation
		// changes
		outState.putParcelable("file_uri", fileUri);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		// get the file url
		fileUri = savedInstanceState.getParcelable("file_uri");
	}



	/**
	 * Receiving activity result method will be called after closing the camera
	 * */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// if the result is capturing Image
		
			if (resultCode == RESULT_OK) {
				// successfully captured the image
				// display it in image view
				if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
					previewCapturedImage();
				}
				else if(requestCode==PIC_FROM_FILE_REQUEST_CODE){
					
					processPicFromFile(data);
				}				
				//user is returning from cropping the image
		    	else if(requestCode == PIC_CROP_REQUEST_CODE){
		    		
		    		//get the returned data
		    		Bundle extras = data.getExtras();
		    		//get the cropped bitmap
		    		Bitmap thePic = extras.getParcelable("data");
		    		//retrieve a reference to the ImageView
		    		ImageView picView = (ImageView)findViewById(R.id.imgPreview);
		    		//display the returned cropped image
		    		picView.setImageBitmap(thePic);
		    		fileUri=getImageUri(this, thePic);
		    		//fileUri=data.getData();  
		    	}
		    	else if(requestCode == PIC_EFFECT_REQUEST_CODE){
		    		//get the returned data
		    		//Bundle extras = data.getExtras();
		    		//get the cropped bitmap
		    		Bitmap thePic  = BitmapFactory.decodeFile( data.getData().getPath());
		    		//retrieve a reference to the ImageView
		    		ImageView picView = (ImageView)findViewById(R.id.imgPreview);
		    		//display the returned cropped image
		    		picView.setImageBitmap(thePic);
		    		fileUri=getImageUri(this, thePic);
		    		//fileUri=data.getData();  
		    	}else if(requestCode== GET_RESTAURENT_ID_REQUEST_CODE){
		    							
		    		if(data.getStringExtra("Id") !=null){
		    			resid=data.getStringExtra("Id");
		    			txtSelectReataurent.setText( data.getStringExtra("RestName"));
		    			
		    		}
		    	}
				
			} else if (resultCode == RESULT_CANCELED) {
				// user cancelled Image capture
				//Toast.makeText(getApplicationContext(),"Cancelled Task", Toast.LENGTH_SHORT).show();
			} else {
				// failed to capture image
				Toast.makeText(getApplicationContext(),
						"Sorry! Failed. ", Toast.LENGTH_SHORT)
						.show();
			}
			
			if(resid !=null && resname !=null){
    			//resid=data.getStringExtra("Id");
    			txtSelectReataurent.setText( resname);//data.getStringExtra("RestName"));
    			
    		}
		
	}
	
	
	private void processPicFromShare(Intent data) {
		// TODO Auto-generated method stub
		 try {
			 Uri imageUri = (Uri) data.getParcelableExtra(Intent.EXTRA_STREAM);
			    if (imageUri != null) {
			        // Update UI to reflect image being shared			  			 
					 fileUri=imageUri;
					Bitmap bitmap =MediaStore.Images.Media.getBitmap(this.getContentResolver(), fileUri);
					int dstHeight= (int)(bitmap.getHeight() * .8);
					int dstWidth= (int) (bitmap.getWidth() * .8);
					
					bitmap=Bitmap.createScaledBitmap(bitmap, dstWidth, dstHeight, false);
					
					//fix orientation problem
					bitmap=ExifUtil.rotateBitmap(fileUri.getPath(), bitmap);
					
					// bimatp factory
					BitmapFactory.Options options = new BitmapFactory.Options();
					
		
					// downsizing image as it throws OutOfMemory Exception for larger
					// images
					options.inSampleSize = 8;
		
					//final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),options);
					
					photoUri=fileUri;
					imgPreview.setImageBitmap(bitmap);
					
					//AnimationTween.crossfade(btnCapturePicture, btnUploadPicture, 3000);
					hideButton();
					
					photoUri=Uri.parse(getRealPathFromURI(photoUri));
					
					//Toast.makeText(this, getRealPathFromURI(photoUri), Toast.LENGTH_LONG).show();
					
					txtSelectReataurent.setVisibility(View.VISIBLE);					
					txtSelectReataurent.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Intent i = new Intent(CapturePhotoActivity.this, DistrictListView.class);
							i.putExtra(StaticObjects.NEV_TYPE, StaticObjects.NEV_TYPE_CHOOSE_RES_PHOTO_UP);
							startActivityForResult(i, GET_RESTAURENT_ID_REQUEST_CODE);
						}
					});
					
		  }
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void processPicFromFile(Intent data) {
		// TODO Auto-generated method stub
		 try {
			 fileUri=data.getData();
			Bitmap bitmap =MediaStore.Images.Media.getBitmap(this.getContentResolver(), fileUri);
			
			
			//bitmap=Bitmap.createScaledBitmap(bitmap, dstWidth, dstHeight, false);
			
			//resize bitmap keep aspect ratio
			Matrix m = new Matrix();
			m.setRectToRect(new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight()), new RectF(0, 0, 800, 600), Matrix.ScaleToFit.CENTER);
			bitmap= Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
			
			

			// bimatp factory
			BitmapFactory.Options options = new BitmapFactory.Options();
			
			//fix orientation problem
			bitmap=ExifUtil.rotateBitmap(fileUri.getPath(), bitmap);

			// downsizing image as it throws OutOfMemory Exception for larger
			// images
			options.inSampleSize = 8;

			//final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),options);
			
			photoUri=fileUri;
			
			Bitmap bitmapRotated=ExifUtil.rotateBitmap(fileUri.getPath(), bitmap);						
			if(bitmapRotated!=null){					
				imgPreview.setImageBitmap(new Functions().getResizedBitmap(bitmapRotated, 800, 600) );
			}else{
				imgPreview.setImageBitmap(new Functions().getResizedBitmap(bitmap, 800, 600) );
			}
			
			
			//imgPreview.setImageBitmap(bitmap);
			
			//AnimationTween.crossfade(btnCapturePicture, btnUploadPicture, 3000);
			hideButton();
			
			//BitmapFactory.decodeFile(pathName, opts)
			
			
			
			photoUri=Uri.parse(getRealPathFromURI3(photoUri));
			
			//Toast.makeText(this, getRealPathFromURI(photoUri), Toast.LENGTH_LONG).show();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	

	private void hideButton() {
		// TODO Auto-generated method stub
		//imageViewCamera.setVisibility(View.GONE);
		//imageViewGallery.setVisibility(View.GONE);
		AnimationTween.crossfade(findViewById(R.id.cam_gal_btn_holder), btnUploadPicture, 1000);
		//AnimationTween.crossfade(imageViewGallery, btnUploadPicture, 1000);
		
		txtCaption.setVisibility(View.VISIBLE);
		txtCaption.setAlpha(0f);		
		txtCaption.animate()
          .alpha(1f)
          .setDuration(2000)
          .setListener(new AnimatorListenerAdapter() {
              @Override
              public void onAnimationEnd(Animator animation) {
            	  txtCaption.requestFocus();
              }
          });
		
		
		imgPreview.setAlpha(0f);
		imgPreview.setVisibility(View.VISIBLE);
		imgPreview.animate()
        .alpha(1f)
        .setDuration(2000)
        .setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
          	  txtCaption.requestFocus();
            }
        });
		
		
		if(resid==null || resid.isEmpty()){
			txtSelectReataurent.setVisibility(View.VISIBLE);					
			txtSelectReataurent.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent i = new Intent(CapturePhotoActivity.this, DistrictListView.class);
					i.putExtra(StaticObjects.NEV_TYPE, StaticObjects.NEV_TYPE_CHOOSE_RES_PHOTO_UP);
					startActivityForResult(i, GET_RESTAURENT_ID_REQUEST_CODE);
				}
			});
		}
		
	
	}

	/*
	 * Display image from a path to ImageView
	 */
	private void previewCapturedImage() {
		try {
			// hide video preview
			//videoPreview.setVisibility(View.GONE);

			

			// bimatp factory
			BitmapFactory.Options options = new BitmapFactory.Options();

			// downsizing image as it throws OutOfMemory Exception for larger
			// images
			options.inSampleSize = 8;

			final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),options);						
			photoUri=fileUri;
			
			//check orientation 
		/*	ExifInterface ei=null;
			try {
				ei = new ExifInterface(getRealPathFromURI(photoUri));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			
			Bitmap bitmapRotated=ExifUtil.rotateBitmap(fileUri.getPath(), bitmap);
			
			/*if(ei!=null){
				int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

				switch(orientation) {
				    case ExifInterface.ORIENTATION_ROTATE_90:
				    	bitmapRotated=rotateBitmap(bitmap, 90);
				        break;
				    case ExifInterface.ORIENTATION_ROTATE_180:
				    	bitmapRotated=rotateBitmap(bitmap, 180);
				        break;
				    // etc.
				}
			}*/
			
			if(bitmapRotated!=null){	
				
				imgPreview.setImageBitmap(new Functions().getResizedBitmap(bitmapRotated, 800, 600) );
			}else{
				imgPreview.setImageBitmap(new Functions().getResizedBitmap(bitmap, 800, 600) );
			}
			
			
			//AnimationTween.crossfade(btnCapturePicture, btnUploadPicture, 3000);
			hideButton();
			
			
			
			
			
			
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}
	
	
	private String getRealPathFromURI(Uri contentURI) {
	    String result;
	    Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
	    if (cursor == null) { 
	        result = contentURI.getPath();
	    } else { 
	        cursor.moveToFirst(); 
	        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA); 
	        result = cursor.getString(idx);
	        cursor.close();
	    }
	    return result;
	}
	
	
	  private String getRealPathFromURI2(Uri contentURI) {
		    Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
		    if (cursor == null) { // Source is Dropbox or other similar local file path
		        return contentURI.getPath();
		    } else { 
		        cursor.moveToFirst(); 
		        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA); 
		        return cursor.getString(idx); 
		    }
		}
	  
	  public String getRealPathFromURI3(Uri uri) {
		    String[] projection = { MediaStore.Images.Media.DATA };
		    @SuppressWarnings("deprecation")
		    Cursor cursor = managedQuery(uri, projection, null, null, null);
		    if(cursor == null){
		    	 return uri.getPath();
		    }else{
		    	int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			    cursor.moveToFirst();
			    return cursor.getString(column_index);
		    }
		    
		}
	
	
	
	
	public  Bitmap rotateBitmap(Bitmap source, float angle)
	{
	      Matrix matrix = new Matrix();
	      matrix.postRotate(angle);
	      return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
	}
	
	
	private void performCrop(Uri photoUri){
		try {
		    //call the standard crop action intent (the user device may not support it)
			Intent cropIntent = new Intent("com.android.camera.action.CROP");
			    //indicate image type and Uri
			cropIntent.setDataAndType(photoUri, "image/*");
			    //set crop properties
			cropIntent.putExtra("crop", "true");
			    //indicate aspect of desired crop
			cropIntent.putExtra("aspectX", 1);
			cropIntent.putExtra("aspectY", 1);
			    //indicate output X and Y
			cropIntent.putExtra("outputX", 256);
			cropIntent.putExtra("outputY", 256);
			    //retrieve data on return
			cropIntent.putExtra("return-data", true);
			    //start the activity - we handle returning in onActivityResult
			startActivityForResult(cropIntent, PIC_CROP_REQUEST_CODE);
		}
		catch(ActivityNotFoundException anfe){
		    //display an error message
		    String errorMessage = "Device doesn't support the crop action!";
		    Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
		    toast.show();
		}
	}

	/*
	 * Previewing recorded video
	 */
	/*private void previewVideo() {
		try {
			// hide image preview
			imgPreview.setVisibility(View.GONE);

			videoPreview.setVisibility(View.VISIBLE);
			videoPreview.setVideoPath(fileUri.getPath());
			// start playing
			videoPreview.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	/**
	 * ------------ Helper Methods ---------------------- 
	 * */

	/*
	 * Creating file uri to store image/video
	 */
	public Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/*
	 * returning image / video
	 */
	private static File getOutputMediaFile(int type) {

		// External sdcard location
		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create " + IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",Locale.getDefault()).format(new Date());
		
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator+ "IMG_" + timeStamp + ".jpg");
		}  else {
			return null;
		}

		return mediaFile;
	}
	
	
	public void savePhotoTosdCard()  {
		    imgPreview.setDrawingCacheEnabled(true);
		    Bitmap bitmap = imgPreview.getDrawingCache();
		    String root = Environment.getExternalStorageDirectory().toString();
		    File newDir = new File(root + "/letseat");
		    newDir.mkdirs();
		    Random gen = new Random();
		    int n = 10000;
		    n = gen.nextInt(n);
		    String fotoname = "Photo-"+ n +".jpg";
		    File file = new File (newDir, fotoname);
		    if (file.exists ()) file.delete ();
		    
		    try {
			    FileOutputStream out = new FileOutputStream(file);
			    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
			    out.flush();
			    out.close();
			    Toast.makeText(getApplicationContext(), "Saved to your folder", Toast.LENGTH_SHORT ).show();
			     
		    } catch (Exception e) {}
    }
	
	
	
    
	
	
	
	public Uri getImageUri(Context inContext, Bitmap inImage) {
		  ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		  inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
		  String path = Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
		  return Uri.parse(path);
		}
	

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
	
		case R.id.action_search:
			// search action
			RelativeLayout relativeLayout=(RelativeLayout)findViewById(R.id.capture_phot_root);		
			addSearchLayout(relativeLayout);			
			return true;
		
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	
	
	public View searchview ;
	@Override
	public void addSearchLayout(RelativeLayout relativeLayout){
		if(relativeLayout.findViewWithTag("searchview")==null){
			relativeLayout.removeView(searchview);
			
			 searchview = getLayoutInflater().inflate(R.layout.search,  relativeLayout, false);
			 searchview.setTag(searchview);
			
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
			
			relativeLayout.addView(searchview, params);
			
			final EditText editTextSearch=(EditText)  searchview.findViewById(R.id.txtSearch);
			
			final Animator.AnimatorListener animatorListener2= new Animator.AnimatorListener() {
			    public void onAnimationStart(Animator animation) {}
			    public void onAnimationRepeat(Animator animation) { }

			    public void onAnimationEnd(Animator animation) {			    	
			    	// hide soft keyboard
					
					String searchText=editTextSearch.getText().toString().trim();					
					
					searchview.clearAnimation();
					Intent i = new Intent(CapturePhotoActivity.this,SearchResturentListView.class);
					i.putExtra("searchKeyWord_home", searchText);
					i.putExtra(StaticObjects.NEV_TYPE, StaticObjects.NEV_TYPE_CHOOSE_RES_PHOTO_UP);
					startActivityForResult(i, GET_RESTAURENT_ID_REQUEST_CODE);
					
					/*Intent i = new Intent(CapturePhotoActivity.this, DistrictListView.class);
					i.putExtra(StaticObjects.NEV_TYPE_RESULT, StaticObjects.NEV_TYPE_RESULT);
					startActivityForResult(i, GET_RESTAURENT_ID_REQUEST_CODE);*/
									
			    }
			    public void onAnimationCancel(Animator animation) {}
			  };
			
			
			editTextSearch.setOnEditorActionListener(new OnEditorActionListener() {

				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					// TODO Auto-generated method stub
					if (actionId == EditorInfo.IME_ACTION_SEARCH) {
						String searchText=editTextSearch.getText().toString().trim();
						if(searchText.isEmpty() || searchText=="" || searchText==null){
							AnimationTween.shakeOnError(CapturePhotoActivity.this, editTextSearch);
							
						}else{
							InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(editTextSearch.getWindowToken(), 0);
							
							AnimationTween.animateView(searchview,animatorListener2, CapturePhotoActivity.this);
						}
						
					}
					return false;
				}
			});
			
			// Edittext lose focus hide search view 
			editTextSearch.setOnFocusChangeListener(new OnFocusChangeListener() {          

		        public void onFocusChange(View v, boolean hasFocus) {
		        	 if(hasFocus){
		        	       // Toast.makeText(getApplicationContext(), "got the focus", Toast.LENGTH_LONG).show();
		        	       
		        	    }else {
		        	        //Toast.makeText(getApplicationContext(), "lost the focus", Toast.LENGTH_LONG).show();
		        	        
		        	    }
		        
		               
		        }
		    });
			
				
			Animator.AnimatorListener animatorListener 
			  = new Animator.AnimatorListener() {

			    public void onAnimationStart(Animator animation) {}
			    public void onAnimationRepeat(Animator animation) {}
			    public void onAnimationEnd(Animator animation) {			    	
			    	//Toast.makeText(getApplicationContext(), "onAnimationEnd", Toast.LENGTH_LONG).show();
			    	editTextSearch.requestFocus();
			    	searchview.clearAnimation();
			    }

			    public void onAnimationCancel(Animator animation) {}
			  };			  
			  AnimationTween.animateViewPosition(searchview,animatorListener ,CapturePhotoActivity.this);
			  
			  
			  
			
		}
	}
	
}



