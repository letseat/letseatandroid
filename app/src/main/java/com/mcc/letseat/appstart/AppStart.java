package com.mcc.letseat.appstart;

import java.util.ArrayList;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/*import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.facebook.widget.LoginButton.UserInfoChangedCallback;*/
import com.foody.AppData.StaticObjects;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.user.User;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.common.SignInButton;
import com.mcc.googlepluslogin.MainActivity;
import com.mcc.letseat.HomeT;
import com.mcc.letseat.R;
import com.mcc.libs.AnimationTween;
import com.mcc.libs.CustomViewPager;
import com.mcc.user.LoginActivity;
import com.mcc.user.RegistrationOptionActivity;
import com.mcc.user.SavedPrefernce;
import com.mcc.user.SignupActivity;
import com.squareup.picasso.Picasso;

/**
 * Options for user registration e.i: FB, G+, Let's Eat
 * 
 * @author Arif
 * 
 */
public class AppStart extends Activity implements OnClickListener {

	public static int count;

	public static boolean isProcessRunning;

	LinearLayout linearLayoutSkip;

	boolean isDBUpdateTaskDone;
	boolean isLoginTaskDone;

	private CustomViewPager viewPager;
	private AppStartViewPagerAdapte adapter;
	
	ImageView imageView ;
	Resources resource ;
		int resId ;
	Bitmap bitmap;

		int resId2;
	Bitmap bitmap2 ;
	TransitionDrawable td;
	
	LinearLayout ll_pager_indicator;

	String st[];
	
	LinearLayout lNevRegistration, lNevLogin, llbtnContainer;
	
	String colorsBtn[];
	
	
	int indicatorNormal, indicatorSelected;
	int currentButtonClolor;

	private String[] stTitle;
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_app_start);
		// hide actionbar
		// getActionBar().hide();
		
		 resource = getResources();
		 
		 indicatorNormal =(int) StaticObjects.convertDpToPixel(10.0f, this);
		 indicatorSelected =(int) StaticObjects.convertDpToPixel(14.0f, this);
		
		colorsBtn=new String[]{"#993333", /*"#cc6633" , */"#669933", "#663366", "#993366", "#669999"};
		
		llbtnContainer = (LinearLayout) findViewById(R.id.btnContainer);

		// registration button
		final TextView btnNevRegistration = (TextView) findViewById(R.id.btnNevRegistration);
		btnNevRegistration.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(AppStart.this, RegistrationOptionActivity.class);
				intent.putExtra(StaticObjects.NEV_TYPE_KEY,
						StaticObjects.nev_type);
				startActivity(intent);
				finish();
			}
		});

		lNevRegistration = (LinearLayout) findViewById(R.id.lNevRegistration);
		lNevRegistration.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(AppStart.this, RegistrationOptionActivity.class);
				intent.putExtra(StaticObjects.NEV_TYPE_KEY,
						StaticObjects.nev_type);
				startActivity(intent);
				finish();
			}
		});
		// registration button touch listener
		btnNevRegistration.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent e) {
				// TODO Auto-generated method stub

				if (v == btnNevRegistration
						&& e.getAction() == MotionEvent.ACTION_DOWN) {
					
					lNevRegistration.setBackgroundColor(Color.parseColor("#80CCCCCC"));
					
				} else if (v == btnNevRegistration
						&& (e.getAction() == MotionEvent.ACTION_CANCEL || e.getAction() == MotionEvent.ACTION_UP)) {
					
					lNevRegistration.setBackgroundColor(Color.parseColor(colorsBtn[viewPager.getCurrentItem()]));
					
				}

				return false;
			}
		});

		lNevRegistration.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent e) {
				// TODO Auto-generated method stub

				if (v == lNevRegistration
						&& e.getAction() == MotionEvent.ACTION_DOWN) {
					lNevRegistration.setBackgroundColor(Color.parseColor("#80CCCCCC"));
				} else if (v == lNevRegistration
						&& (e.getAction() == MotionEvent.ACTION_CANCEL || e.getAction() == MotionEvent.ACTION_UP)) {
					
					lNevRegistration.setBackgroundColor(Color.parseColor(colorsBtn[viewPager.getCurrentItem()]));
					
				}

				return false;
			}
		});

		// login button click
		final TextView btnNevLogin = (TextView) findViewById(R.id.btnNevLogin);
		btnNevLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(AppStart.this, RegistrationOptionActivity.class);
				intent.putExtra(StaticObjects.NEV_TYPE_KEY,
						StaticObjects.nev_type);
				startActivity(intent);
				finish();
			}
		});
		
		lNevLogin = (LinearLayout) findViewById(R.id.lNevLogin);
		lNevLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(AppStart.this, RegistrationOptionActivity.class);
				intent.putExtra(StaticObjects.NEV_TYPE_KEY,
						StaticObjects.nev_type);
				startActivity(intent);
				finish();
			}
		});

		// login button touch listener
		btnNevLogin.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent e) {
				// TODO Auto-generated method stub

				if (v == btnNevLogin&& e.getAction() == MotionEvent.ACTION_DOWN) {
					
					lNevLogin.setBackgroundColor(Color.parseColor("#80CCCCCC"));
					
				} else if (v == btnNevLogin
						&& (e.getAction() == MotionEvent.ACTION_CANCEL || e
								.getAction() == MotionEvent.ACTION_UP)) {
					lNevLogin.setBackgroundColor(Color.parseColor(colorsBtn[viewPager.getCurrentItem()]));
					// btnNevLogin.setBackgroundResource(R.drawable.border_round_red_button);
				}

				return false;
			}
		});

		lNevLogin.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent e) {
				// TODO Auto-generated method stub

				if (v == btnNevLogin&& e.getAction() == MotionEvent.ACTION_DOWN) {
					
					lNevLogin.setBackgroundColor(Color.parseColor("#80CCCCCC"));
					
				} else if (v == btnNevLogin&& (e.getAction() == MotionEvent.ACTION_CANCEL || e.getAction() == MotionEvent.ACTION_UP)) {
					
					lNevLogin.setBackgroundColor(Color.parseColor(colorsBtn[viewPager.getCurrentItem()]));
				}

				return false;
			}
		});
		
		//set button initial color
		llbtnContainer.setBackgroundColor(Color.parseColor(colorsBtn[0]));
		

		linearLayoutSkip = (LinearLayout) findViewById(R.id.linearLayoutSkip);
		linearLayoutSkip.setVisibility(View.VISIBLE);
		linearLayoutSkip.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent e) {
				if (e.getAction() == MotionEvent.ACTION_DOWN) {

					linearLayoutSkip.setBackgroundColor(Color
							.parseColor("#FFFFFF"));

				} else if (e.getAction() == MotionEvent.ACTION_UP) {
					linearLayoutSkip.setBackgroundColor(0x00000000);
				}

				return false;
			}
		});

		linearLayoutSkip.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(AppStart.this, HomeT.class);
				startActivity(intent);

			}
		});

		viewPager = (CustomViewPager) findViewById(R.id.pager_app_start);

		st = new String[] {
				"Let's Eat is the best restaurants and deals finder application in Dhaka, Bangladesh. Use Let's Eat to find out the restaurants selling your favorite cuisine in the places you hang out.",
				/*"Looking for a restaurant that serves your favorite cuisine in nearby location or anywhere in Dhaka? Use Let's Eat to find the perfect restaurant.",*/
				"Looking for membership deals or discounts? We have thousands of partner restaurants offering you the best deals & discounts.",
				"Call the restaurant and book your seats before you go there. Let's Eat users are always premium booked by our partner restaurants.",
				"Liked a restaurant or didn't? Share your experience with friends by giving your review and rating inside the app.",
				"Need a Kids' Zone, Live Music or WiFi while in a Restaurant? No more wandering around. Check and find the restaurant in Let's Eat who are giving the facilities you need."};
		
		stTitle = new String[] {
				/*"", */
				"Find Restaurants",
				"Deals & Discounts ",
				"Call and book Restaurants",
				"Review and Rating",
				"Restaurant Facilities"};
		
		
		//
		ll_pager_indicator = (LinearLayout) findViewById(R.id.ll_pager_indicator);
		
		for (int i = 0; i < st.length; i++) {
			LinearLayout llindictor =new LinearLayout(this);
			LinearLayout.LayoutParams layoutParams = i==0? new LinearLayout.LayoutParams(indicatorSelected, indicatorSelected):new LinearLayout.LayoutParams(indicatorNormal, indicatorNormal);
			layoutParams.setMargins(8, 0, 0, 0);
			llindictor.setLayoutParams(layoutParams);
			
			
			final int sdk = android.os.Build.VERSION.SDK_INT;
			if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
				if(i==0)
				llindictor.setBackgroundDrawable( getResources().getDrawable(R.drawable.shape_circle_white_fill) );
				else
				llindictor.setBackgroundDrawable( getResources().getDrawable(R.drawable.shape_circle_white) );
			} else {
				if(i==0)
				llindictor.setBackground( getResources().getDrawable(R.drawable.shape_circle_white_fill));
				else
				llindictor.setBackground( getResources().getDrawable(R.drawable.shape_circle_white));
			}		
			llindictor.setAlpha(0.5f);
			ll_pager_indicator.addView(llindictor);
			
		}
		

		adapter = new AppStartViewPagerAdapte(this, stTitle, st, viewPager, resource);
		viewPager.setAdapter(adapter);
		viewPager.setCurrentItem(0, false);
	

		 imageView = (ImageView) findViewById(R.id.app_start_back_img);
		
		resId = getRandomResourceID(0);
		bitmap = decodeBitmapFromResource(resource, resId, 600, 800);

		resId2 = getRandomResourceID(0);
		bitmap2 = decodeBitmapFromResource(resource, resId2, 600, 800);
		td = new TransitionDrawable(new Drawable[] {
				new BitmapDrawable(resource, bitmap),
				new BitmapDrawable(resource, bitmap2) });

		// Set background to loading bitmap
		imageView.setBackgroundDrawable(new BitmapDrawable(resource, bitmap2));

		imageView.setImageDrawable(td);
		td.startTransition(1000);

		viewPager.setOnPageChangeListener(new OnPageChangeListener() {
			
			int currentPosition=0;
			int oldPosition=0;
			
			
			int currentColor = Color.parseColor(colorsBtn[currentPosition]);
			int oldColor = currentColor;
		
			
			@Override
			public void onPageSelected(int position) {
				// hide skip button if current page is not 0
				if(position==(st.length-1))
					linearLayoutSkip.setVisibility(View.INVISIBLE);
				else
					linearLayoutSkip.setVisibility(View.VISIBLE);
					
				
				currentPosition = position;
				// mViewPager.getChildAt(i).isFocused()

				View currentView = viewPager.findViewWithTag(viewPager
						.getCurrentItem());
				// View view = viewPager.getFocusedChild();
				// Toast.makeText(FullScreenViewActivity.this,
				// ""+position+"<>"+adapter.mCurrentView,
				// Toast.LENGTH_SHORT).show();

				// TextView txtPhotCaption =(TextView)
				// adapter.mCurrentView.findViewById(R.id.txtPhotCaption);
				// txtPhotCaption.setText("manipulate");
				// txtPhotCaption.setAlpha(1);
				adapter.notifyDataSetChanged();
				
				/*bitmap =bitmap2;
				int resId2 = getRandomResourceID(((position + 1) % 11));
				 bitmap2 = decodeBitmapFromResource(res, resId2, 600, 800);
				 td = new TransitionDrawable(new Drawable[] {
						new BitmapDrawable(res, bitmap),
						new BitmapDrawable(res, bitmap2) });
				td.startTransition(1000);
				imageView.setBackgroundDrawable(new BitmapDrawable(res, bitmap2));
				
				Picasso.with(AppStart.this).load(resId2).into(imageView); 
				
				for (int i = 0; i < st.length; i++) {
					LinearLayout child=(LinearLayout)ll_pager_indicator.getChildAt(i);
					LinearLayout.LayoutParams childlayoutParams = new LinearLayout.LayoutParams(16, 16);
					childlayoutParams.setMarginStart(8);								
					child.setLayoutParams(childlayoutParams);
					
				}
				
				
				//
				LinearLayout child=(LinearLayout)ll_pager_indicator.getChildAt(position);
				LinearLayout.LayoutParams childlayoutParams = new LinearLayout.LayoutParams(24, 24);
				childlayoutParams.setMarginStart(8);								
				child.setLayoutParams(childlayoutParams);*/

			}

			@Override
			public void onPageScrolled(int position, float positionOffset,
					int positionOffsetPixels) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int state) {
				// TODO Auto-generated method stub
				if (state == ViewPager.SCROLL_STATE_IDLE) {
					 //adapter.changeAlpha();
					if(currentPosition==oldPosition)
						return;
					
					
					oldPosition = currentPosition;
								
					// Toast.makeText(AppStart.this, "IDLE", Toast.LENGTH_SHORT).show();
					
					//change button color
					//llbtnContainer.setBackgroundColor(Color.parseColor(colorsBtn[currentPosition]));
					
					 currentColor= Color.parseColor(colorsBtn[currentPosition]);
					
					if(oldColor!=0){
						int transitionTime=1500;
						ObjectAnimator animator = ObjectAnimator.ofInt(llbtnContainer, "backgroundColor", oldColor, currentColor ).setDuration(transitionTime);
					    animator.setEvaluator(new ArgbEvaluator());
					    animator.start();
					}
					
					
					 
					 
					 bitmap =bitmap2;
						int resId2 = getRandomResourceID(((currentPosition + 1) % 11));
						 bitmap2 = decodeBitmapFromResource(resource, resId2, 480, 800);
						 td = new TransitionDrawable(new Drawable[] {
								new BitmapDrawable(resource, bitmap),
								new BitmapDrawable(resource, bitmap2) });
						
						imageView.setBackgroundDrawable(new BitmapDrawable(resource, bitmap2));
						imageView.setImageDrawable(td);
						td.startTransition(1500);
						
						//Picasso.with(AppStart.this).load(resId2).into(imageView); 
						
						
						for (int i = 0; i < st.length; i++) {
							LinearLayout child=(LinearLayout)ll_pager_indicator.getChildAt(i);
							child.setAlpha(0.5f);
							LinearLayout.LayoutParams childlayoutParams = new LinearLayout.LayoutParams(indicatorNormal, indicatorNormal);
							childlayoutParams.setMargins(8,0,0,0);								
							child.setLayoutParams(childlayoutParams);
							
							final int sdk = android.os.Build.VERSION.SDK_INT;
							if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
								
									child.setBackgroundDrawable( getResources().getDrawable(R.drawable.shape_circle_white) );

							} else {
								
								child.setBackground( getResources().getDrawable(R.drawable.shape_circle_white));
								
							}	
							
						}
						
						
						//
						LinearLayout child=(LinearLayout)ll_pager_indicator.getChildAt(currentPosition);
						LinearLayout.LayoutParams childlayoutParams = new LinearLayout.LayoutParams(indicatorSelected, indicatorSelected);
						childlayoutParams.setMargins(8,0,0,0);								
						child.setLayoutParams(childlayoutParams);
						
						final int sdk = android.os.Build.VERSION.SDK_INT;
						if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
							
								child.setBackgroundDrawable( getResources().getDrawable(R.drawable.shape_circle_white_fill) );

						} else {
							
							child.setBackground( getResources().getDrawable(R.drawable.shape_circle_white_fill));
							
						}	
						
						
						/*ObjectAnimator animator = ObjectAnimator.ofFloat(child, "alpha", 1.0f, 0.5f, 1.0f).setDuration(500);
						
					    animator.setEvaluator(new ArgbEvaluator());
					    animator.start();*/
						
						/*ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(child, 
							    PropertyValuesHolder.ofFloat("scaleX", 1.5f),
							    PropertyValuesHolder.ofFloat("scaleY", 1.5f));
							scaleDown.setDuration(1000);
							scaleDown.start();*/
						
						 ObjectAnimator anim 
						    = ObjectAnimator.ofFloat(child, "alpha", 
						         0.25f,  0.75f);  
						  anim.setDuration(300);
						  anim.start();
						
						//set old color
						oldColor=currentColor;
						
						
					 
				} else if (state == ViewPager.SCROLL_STATE_DRAGGING) {
					 //adapter.changeAlpha();
					// Toast.makeText(AppStart.this, "DRAGGING",
					 //Toast.LENGTH_SHORT).show();
					 
				} else if (state == ViewPager.SCROLL_STATE_SETTLING) {
					// adapter.changeAlpha();
					 //Toast.makeText(AppStart.this, "SETTLING",
					 //Toast.LENGTH_SHORT).show();

				}

			}
		});

		linearLayoutSkip.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AppStart.this, RegistrationOptionActivity.class);
				intent.putExtra(StaticObjects.NEV_TYPE_KEY, StaticObjects.NEV_TYPE_HOME);
				StaticObjects.nev_type = StaticObjects.NEV_TYPE_HOME;
				startActivity(intent);
				finish();
			}
		});

		AnimationTween.animateViewPosition(llbtnContainer, AppStart.this);
		
		//change luncher by setting show info true
		new SavedPrefernce(this).setShowInfo(true);
		
	}

	private int getRandomResourceID(int random) {

		int d = 0;
		switch (random) {
		case 1:
			d = R.mipmap.image_01;
			break;
		/*case 2:
			d = R.mipmap.image_02;
			break;*/
		case 2:
			d = R.mipmap.image_03;
			break;
		case 3:
			d = R.mipmap.image_04;
			break;
		case 4:
			d = R.mipmap.image_05;
			break;
		case 5:
			d = R.mipmap.image_06;
			break;		

		default:
			d = R.mipmap.image_01;
			break;
		}

		return d;
	}

	public int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	public Bitmap decodeBitmapFromResource(Resources res, int resId,
			int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resId, options);
	}

	@Override
	public void onPause() {
		super.onPause();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();

	}
	
	@Override
	public void onBackPressed() {
		
		Intent intent = new Intent(AppStart.this, HomeT.class);
		startActivity(intent);
	}

	@Override
	public void onSaveInstanceState(Bundle savedState) {
		super.onSaveInstanceState(savedState);

	}

	protected void onStart() {
		super.onStart();

		GoogleAnalytics.getInstance(this).reportActivityStart(this);

	}

	protected void onStop() {
		super.onStop();

		GoogleAnalytics.getInstance(this).reportActivityStop(this);

	}

	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

}
