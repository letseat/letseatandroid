package com.mcc.letseat.appstart;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mcc.letseat.R;
import com.mcc.libs.CustomViewPager;

/**
 * Photo Gallery Fullscreen adepter
 * @author Arif
 *
 */
public class AppStartViewPagerAdapte extends PagerAdapter {

	public CustomViewPager customViewPager;
	
	private Activity _activity;
	private LayoutInflater inflater;
	private 	Resources res ;
	
	
	public final static String  VIEW_LAYOUT_PRIFIX="FSI";
	
	
	String listTags[];
	String listTitle[];

	
	
	// constructor
		public AppStartViewPagerAdapte(Activity activity, String listTitle[], String listTags[], CustomViewPager customViewPager, 	Resources res ) {
			this._activity = activity;
			
			 this.listTags=listTags;
			 this.customViewPager=customViewPager;
			 this.res = res;
			 this.listTitle= listTitle;
		}
		
	

	@Override
	public int getCount() {
		return this.listTags.length;
	}

	@Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }
	
	@Override
    public Object instantiateItem(ViewGroup container, int position) {
        
        inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.app_start_viewpager_item, container,false);
 
      
        TextView txtPhotCaption =(TextView) viewLayout.findViewById(R.id.textViewSmall);
        TextView txtTitle =(TextView) viewLayout.findViewById(R.id.textView1);
        ImageView imageView_info =(ImageView) viewLayout.findViewById(R.id.imageView_info);
        int resId = getRandomResourceID(position );
//		 Bitmap bitmap = decodeBitmapFromResource(res, resId, 600, 800);		 
//		 BitmapDrawable drawable=	new BitmapDrawable(res, bitmap);
        if(imageView_info!=null)
		imageView_info.setImageDrawable(res.getDrawable(resId));
        
       // txtPhotCaption.setAlpha(0);
        
        
        txtTitle.setText(listTitle[position]);
        txtPhotCaption.setText(listTags[position]);
        
        

        ((ViewPager) container).addView(viewLayout);
        
        viewLayout.setTag(VIEW_LAYOUT_PRIFIX+position);
 
        return viewLayout;
	}
	

	
	@Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((LinearLayout) object);
 
    }
	
	public View mCurrentView;
	public View mCurrentTextViewCaption;

	@Override
	public void setPrimaryItem(ViewGroup container, int position, Object object) {
	    mCurrentView = (View)object;
	    mCurrentTextViewCaption=mCurrentView.findViewById(R.id.txtPhotCaption);
	}
	
	public void changeAlpha(){
		mCurrentTextViewCaption.setAlpha(1);
	}
	
	
	private int getRandomResourceID(int random) {

		int d = 0;
		switch (random) {
		case 0:
			d = R.mipmap.info_1;
			break;
		/*case 1:
			d = R.drawable.info_2;
			break;*/
		case 1:
			d = R.mipmap.info_3;
			break;
		case 2:
			d = R.mipmap.info_4;
			break;
		case 3:
			d = R.mipmap.info_5;
			break;
		case 4:
			d = R.mipmap.info_6;
			break;		
		
		}

		return d;
	}

	public int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	public Bitmap decodeBitmapFromResource(Resources res, int resId,
			int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resId, options);
	}
	
	
	

}
