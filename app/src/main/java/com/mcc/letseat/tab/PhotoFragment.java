package com.mcc.letseat.tab;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.Photo;
import com.foody.nearby.AlertDialogManager;
import com.foody.nearby.GPSTracker;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mcc.letseat.R;
import com.mcc.letseat.imageslider.adapter.GridViewImageAdapterNew;
import com.mcc.letseat.imageslider.helper.AppConstant;
import com.mcc.letseat.imageslider.helper.Utils;
import com.mcc.libs.AnimationTween;
import com.mcc.user.SavedPrefernce;

/**
 * PhootFragment provide Photo of  restaurant collected by Let's Eat Team, or posted by app user  
 * @author Arif
 *
 */

public class PhotoFragment extends Fragment {
	   

	

		// Alert Dialog Manager
		AlertDialogManager alert = new AlertDialogManager();

		// Google Places
		FoodyResturent foodyResturent;

		// Places List
		ArrayList<Photo> photoList;

		// GPS Location
		GPSTracker gps;

		// Button
		ImageView btnShowOnMap;

		

		// Places Listview
		ListView lv;

		// ListItems data
		ArrayList<HashMap<String, String>> resturantListItems = new ArrayList<HashMap<String, String>>();

		// //admob
		AdView adView = null;

		ImageView searchAny;
		
		TextView restaurantName;

		String restaurentId;
	
		
		
		// 
		private Utils utils;
		//private ArrayList<String> imagePaths = new ArrayList<String>();
		private GridViewImageAdapterNew adapter;
		private GridView gridView;
		private int columnWidth;
		
		View currentView = null;

		LoadPhotos loadPhotos ;

		
		 @Override
		 public View onCreateView(LayoutInflater inflater, ViewGroup container,
		        Bundle savedInstanceState) {
		     
			 setRetainInstance(true);
		        
		       /* currentView = inflater.inflate(R.layout.activity_grid_view, container,false);
				return currentView;*/
				
				if (currentView != null) {
					ViewGroup parent = (ViewGroup) currentView.getParent();
					if (parent != null)
						parent.removeView(currentView);
				}
				
				try {
					
					currentView = inflater.inflate(R.layout.activity_grid_view, container,false);
				} catch (InflateException e) {
					/* map is already there, just return view as it is */
				}
				
				return currentView;
		 }
		 
		 @Override
			public void onCreate(Bundle savedInstanceState) {
				// TODO Auto-generated method stub
				super.onCreate(savedInstanceState);
				//enable action bar home button from fragment
				setHasOptionsMenu(true) ;
				getActivity().getActionBar().setHomeButtonEnabled(true);
			
				
				getActivity().getWindow().getDecorView().findViewById(android.R.id.content).setBackgroundResource(R.mipmap.home_back);
			}
		 
		
		@Override	
		public void onStart() {
			super.onStart();

		/*	// action bar hide title change icon
			getActivity().getActionBar().setIcon(R.drawable.ic_title);
			getActivity().getActionBar().setDisplayShowTitleEnabled(false);
			
			//hide or show back icon on action bar
			getActivity().getActionBar().setDisplayHomeAsUpEnabled(false);
			
			//enable app icon as home button
			getActivity().getActionBar().setHomeButtonEnabled(true);		
			
			//Set whether to include the application home affordance in the action bar. Home is presented as either an activity icon or logo. 
			//getActionBar().setDisplayShogetActivity().getActionBar()rue);
*/
			

			adView = (AdView) currentView.findViewById(R.id.adView);

			AdRequest adRequest = new AdRequest.Builder().addTestDevice(
					AdRequest.DEVICE_ID_EMULATOR).build();

			adView.loadAd(adRequest);

			adView.setAdListener(new AdListener() {
				public void onAdLoaded() {
				}

				public void onAdFailedToLoad(int errorcode) {
					System.out.println("Error:" + errorcode);
				}

			});
			
			//Toast.makeText(ResturentListView.this, "hello"+searchKeywords, Toast.LENGTH_LONG).show();
			
			restaurentId = getArguments().getString("restaurantId");
			 
			 restaurantName= (TextView)currentView.findViewById(R.id.restName);
			 restaurantName.setText(getArguments().getString("restaurentName"));
			
			
			
			
			
			///
			gridView = (GridView) currentView.findViewById(R.id.grid_view);

			utils = new Utils(getActivity());

			// Initilizing Grid View
			InitilizeGridLayout();

			// loading all image paths from SD card
			//imagePaths = utils.getFilePaths();
			
			
			//load data async	
			loadPhotos =new LoadPhotos();
			loadPhotos.execute();
			
			

		}
		
		
		private void InitilizeGridLayout() {
			Resources r = getResources();
			float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
					AppConstant.GRID_PADDING, r.getDisplayMetrics());

			columnWidth = (int) ((utils.getScreenWidth() - ((AppConstant.NUM_OF_COLUMNS + 1) * padding)) / AppConstant.NUM_OF_COLUMNS);

			gridView.setNumColumns(AppConstant.NUM_OF_COLUMNS);
			gridView.setColumnWidth(columnWidth);
			gridView.setStretchMode(GridView.NO_STRETCH);
			gridView.setPadding((int) padding, (int) padding, (int) padding,
					(int) padding);
			gridView.setHorizontalSpacing((int) padding);
			gridView.setVerticalSpacing((int) padding);
		}


		/*// action bar menu and event
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// TODO Auto-generated method stub

			MenuInflater inflater = getActivity().getMenuInflater();
			inflater.inflate(R.menu.activity_main, menu);
			// Associate searchable configuration with the SearchView
			SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
			SearchView searchView = (SearchView) menu.findItem(R.id.action_search)
					.getActionView();
			searchView.setSearchableInfo(searchManager
					.getSearchableInfo(getActivity().getComponentName()));

			return super.getActivity().onCreateOptionsMenu(menu);
		}*/

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// TODO Auto-generated method stub

			switch (item.getItemId()) {
			case R.id.action_search:
				// search action
				return true;
			default:
				return super.onOptionsItemSelected(item);
			}

		}

		@Override
		public void onPause() {
			super.onPause();

			if (adView != null)
				adView.pause();

		}

		@Override
		public void onResume() {
			super.onResume();

			if (adView != null)
				adView.resume();
		}

		@Override
		public void onDestroy() {
			if (adView != null)
				adView.destroy();
			super.onDestroy();
			
			if(loadPhotos != null && loadPhotos .getStatus() == Status.RUNNING) {
				loadPhotos .cancel(true);
				}
		}

		class LoadPhotos extends AsyncTask<String, String, String> {

			/**
			 * Before starting background thread Show Progress Dialog
			 * */
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				if(currentView!=null)
				currentView.findViewById(R.id.grid_view_custom_progress).setVisibility(View.VISIBLE);
			}

			/**
			 * getting Places JSON
			 * */
			protected String doInBackground(String... args) {
				// creating Places class object
				foodyResturent = new FoodyResturent();

				try {
					
					SavedPrefernce sp=new SavedPrefernce(getActivity());

					String postData = "resid=" + restaurentId+"&userid="+sp.getUserID();
					

					photoList = foodyResturent.getPhotoList(postData, FoodyResturent.TYPE_RES);

				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}

			/**
			 * After completing background task Dismiss the progress dialog and show
			 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
			 * from background thread, otherwise you will get error
			 * **/
			protected void onPostExecute(String file_url) {
				if(!isCancelled()){
				// dismiss the dialog after getting all products
				//pDialog.dismiss();
				
				if(currentView!=null){
					currentView.findViewById(R.id.grid_view_custom_progress).setVisibility(View.GONE);
					
					AnimationTween.animateView(currentView.findViewById(R.id.grid_view_custom_progress), getActivity());
					AnimationTween.animateViewPosition(gridView, getActivity());
					
				}
				
				if(getActivity()==null)
					return;
				// updating UI from Background Thread
				getActivity().runOnUiThread(new Runnable() {
					public void run() {
						/**
						 * Updating parsed Places into LISTVIEW
						 * */
						// Get json response status

						if (photoList == null || photoList.size() <= 0) {

							
							if(currentView!=null){						
								currentView.findViewById(R.id.grid_view_custom_progress).setVisibility(View.VISIBLE);
								currentView.findViewById(R.id.grid_view_custom_progress).findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
								TextView textPreparing=(TextView)currentView.findViewById(R.id.grid_view_custom_progress).findViewById(R.id.textPreparing);
								textPreparing.setText("Nothing found.");
								}
							
							 
						} else if (photoList != null) {
							// Check for all possible status

							// Successfully got places details
							if (photoList != null) {
								resturantListItems = new ArrayList<HashMap<String, String>>();
								// loop through each place
								for (Photo p : photoList) {
									
									HashMap<String, String> map = new HashMap<String, String>();
									map.put("IMG", p.IMG);	
									map.put("CAPTION", p.CAPTION);	
									map.put("RESID", p.RESID);
									map.put("THUMBIMG", p.THUMBIMG);
									
									//String status=p.STATUS;
									
									map.put("STATUS", p.STATUS);	
									

									// adding HashMap to ArrayList
									resturantListItems.add(map);
								}
								
								HashMap<String, String> map = new HashMap<String, String>();
								map.put("IMG","add");	
								resturantListItems.add(map);
								/*// list adapter
								MyPhotoListAdapter adapter = new MyPhotoListAdapter(
										getActivity(),
										resturantListItems,
										new String[] {  "IMG"});
								

								// Adding data into listview
								lv.setAdapter(adapter);
								*/
								
								// Gridview adapter
								adapter = new GridViewImageAdapterNew(getActivity(), resturantListItems,columnWidth, new String[] {  "IMG", "CAPTION", "RESID","THUMBIMG"}, GridViewImageAdapterNew.GalleryTypeGal);

								// setting grid view adapter
								
								gridView.setAdapter(adapter);
								adapter.notifyDataSetChanged();
							}

							else if (photoList == null) {
								// Zero results found
								if(currentView!=null){						
									currentView.findViewById(R.id.grid_view_custom_progress).setVisibility(View.VISIBLE);
									currentView.findViewById(R.id.grid_view_custom_progress).findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
									TextView textPreparing=(TextView)currentView.findViewById(R.id.grid_view_custom_progress).findViewById(R.id.textPreparing);
									textPreparing.setText("Nothing found.");
									}
							}
						}
					}

				});
				
			}

			}

		}
		
		
		
		

		/*@Override
		public boolean onNavigationItemSelected(int arg0, long arg1) {
			// TODO Auto-generated method stub
			return false;
		}*/
		
		
		/*private void showPopUp() {

			AlertDialog.Builder helpBuilder = new AlertDialog.Builder(getActivity());
			// helpBuilder.setTitle("Pop Up");
			helpBuilder.setMessage("Nothing Found. Want to request this suggestion?");
			helpBuilder.setPositiveButton("No",
					new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {

						}
					});

			helpBuilder.setNegativeButton("Yes",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

							Intent in = new Intent(
									getActivity().getApplicationContext(),
									SuggetionRequest.class);
							in.putExtra("keyword", restaurentId);

							startActivity(in);
						}
					});

			// Remember, create doesn't show the dialog
			AlertDialog helpDialog = helpBuilder.create();
			helpDialog.show();

		}*/

		
	}

