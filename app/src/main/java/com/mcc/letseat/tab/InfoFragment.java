package com.mcc.letseat.tab;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.Card;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.ResturentDetails;
import com.foody.nearby.AlertDialogManager;
import com.foody.nearby.MenuListView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mcc.le.gcm.FeedBackActivity;
import com.mcc.letseat.CapturePhotoActivity;
import com.mcc.letseat.MyProgressDialog;
import com.mcc.letseat.R;
import com.mcc.letseat.imageslider.FullScreenViewActivity;
import com.mcc.letseat.imageslider.adapter.GridViewImageAdapterNew;
import com.mcc.libs.AnimationTween;
import com.mcc.libs.CF;
import com.mcc.libs.NotifyingScrollView;
import com.mcc.user.RegistrationActivity;
import com.mcc.user.RegistrationOptionActivityAction;
import com.mcc.user.SavedPrefernce;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * InfoFragment provide Restaurant info ei: name, address, phone etc that hosted in TabmainActivity 
 * @author Arif
 *
 */
public class InfoFragment extends Fragment implements OnClickListener {

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Google Places
	FoodyResturent foodyResturent;

	// ResturentDetails List
	ArrayList<ResturentDetails> resturentList;

	// gift List
	ListView giftList;
	// gift list data
	//ArrayList<HashMap<String, String>> giftListItems = new ArrayList<HashMap<String, String>>();

	// admob
	AdView adView = null;

	// Progress dialog
	MyProgressDialog pDialog;

	TextView txtRestName, txtRestAddress, txtAddReview, txtAddPhoto,
			txtCuisine, txtTime, txtCost, txtCashCard, txtInternet,
			txtSeatingCapacity, txtRateValue;

	String phonString, lat, lng;
	ImageView imageRestaurent, imageMenu, imageRatting, imagePhone,imageFav, imageShare, imageViewMap;

	// hilights
	ImageView imageHD, imageSO, imageDinner, imageTR, imageB, imageBar,
			imageAC, imageBuffet, imageLM, imageVeg, imageNV, imageKidz, imageSmoking;

	LinearLayout l_rate_info, layoutCardGiftHolder;

	Dialog rankDialog;
	RatingBar ratingBar;
	EditText etWtiteComment;


	Uri uriTitle;
	Uri uriLink;
	Uri uriImage;

	double currentRating = 0;
	

	Activity curActivity;
	View currentView=null;
	
	boolean isCardsAdded=false;
	
	//highLighted deals
	LinearLayout deals_highlight;
	TextView dealsName, dealsProvider, deals;

	
	LoadRestInfo loadRestInfo;
	
	Bundle _savedInstanceState;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		
			_savedInstanceState=savedInstanceState;
			setRetainInstance(true);
			//enable action bar home button from fragment
			setHasOptionsMenu(true) ;
			getActivity().getActionBar().setHomeButtonEnabled(true);
		
			
			getActivity().getWindow().getDecorView().findViewById(android.R.id.content).setBackgroundResource(R.mipmap.home_back);
		
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		/*
		if(currentView==null){
			// Inflate the layout for this fragment
			restaurantId = getArguments().getString("restaurantId"); 
			currentView=inflater.inflate(R.layout.resturent_new_z, container, false);
		}
		return currentView;*/
		
	
		if (currentView != null) {
			ViewGroup parent = (ViewGroup) currentView.getParent();
			if (parent != null)
				parent.removeView(currentView);
		}
		
		try {
			restaurantId = getArguments().getString("restaurantId"); 
			currentView = inflater.inflate(R.layout.resturent_new_z, container, false);
		} catch (InflateException e) {
			/* map is already there, just return view as it is */
		}
	
		
		
		
		return currentView;
	}
	
	

	@Override
	public void onStart() {
		
		super.onStart();
		
		currentView.findViewById(R.id.ressview).setVisibility(View.INVISIBLE);
		/*
		// action bar hide title change icon
		getActivity().getActionBar().setIcon(R.drawable.ic_title);
		getActivity().getActionBar().setDisplayShowTitleEnabled(false);

		// hide or show back arrow icon on action bar
		getActivity().getActionBar().setDisplayHomeAsUpEnabled(false);

		// enable app icon as home button
		getActivity().getActionBar().setHomeButtonEnabled(true);*/

		// Set whether to include the application home affordance in the action
		// bar. Home is presented as either an activity icon or logo.
		// getActionBar().setDisplayShowHomeEnabled(true);

		txtRestName = (TextView) currentView.findViewById(R.id.txtRestName);
		txtRestAddress = (TextView) currentView.findViewById(R.id.txtRestAddress);
		// txtRating= (TextView) findViewById(R.id.txtRestRating);

		txtAddReview = (TextView) currentView.findViewById(R.id.txtAddReview);
		txtAddPhoto = (TextView) currentView.findViewById(R.id.txtAddPhoto);

		imageRestaurent = (ImageView) currentView.findViewById(R.id.imgResturent);
		imageMenu = (ImageView) currentView.findViewById(R.id.imageMenu);

		imageRatting = (ImageView) currentView.findViewById(R.id.imageRatting);
		imagePhone = (ImageView) currentView.findViewById(R.id.imagePhone);
		imageFav = (ImageView) currentView.findViewById(R.id.imageFav);
		imageShare = (ImageView) currentView.findViewById(R.id.imageShare);

		txtCuisine = (TextView) currentView.findViewById(R.id.txtCuisine);
		txtTime = (TextView) currentView.findViewById(R.id.txtTime);
		txtCost = (TextView) currentView.findViewById(R.id.txtCost);
		txtCashCard = (TextView) currentView.findViewById(R.id.txtCashCard);
		txtInternet = (TextView) currentView.findViewById(R.id.txtInternet);
		txtSeatingCapacity = (TextView) currentView.findViewById(R.id.txtSeatingCapacity);

		txtRateValue = (TextView) currentView.findViewById(R.id.txtRateValue);
		
		txtRestName.setTypeface(StaticObjects.gettfNormal(getActivity())); 
		txtRestAddress.setTypeface(StaticObjects.gettfNormal(getActivity())); 
		txtAddReview.setTypeface(StaticObjects.gettfNormal(getActivity())); 
		txtAddPhoto.setTypeface(StaticObjects.gettfNormal(getActivity()));
		txtCuisine.setTypeface(StaticObjects.gettfNormal(getActivity())); 
		txtTime.setTypeface(StaticObjects.gettfNormal(getActivity())); 
		txtCost.setTypeface(StaticObjects.gettfNormal(getActivity()));
		txtCashCard.setTypeface(StaticObjects.gettfNormal(getActivity()));
		txtInternet.setTypeface(StaticObjects.gettfNormal(getActivity()));
		txtSeatingCapacity.setTypeface(StaticObjects.gettfNormal(getActivity())); 
		txtRateValue.setTypeface(StaticObjects.gettfNormal(getActivity()));

		// highlights
		imageHD = (ImageView) currentView.findViewById(R.id.imageHD);
		imageSO = (ImageView) currentView.findViewById(R.id.imageSO);
		imageDinner = (ImageView) currentView.findViewById(R.id.imageDinner);
		imageTR = (ImageView) currentView.findViewById(R.id.imageTR);
		imageB = (ImageView) currentView.findViewById(R.id.imageB);
		imageBar = (ImageView) currentView.findViewById(R.id.imageBar);
		imageAC = (ImageView) currentView.findViewById(R.id.imageAC);
		imageBuffet = (ImageView) currentView.findViewById(R.id.imageBuffet);
		imageLM = (ImageView) currentView.findViewById(R.id.imageLM);
		imageVeg = (ImageView) currentView.findViewById(R.id.imageVeg);
		imageNV = (ImageView) currentView.findViewById(R.id.imageNV);
		imageKidz = (ImageView) currentView.findViewById(R.id.imageKidZone);
		imageSmoking = (ImageView) currentView.findViewById(R.id.imageSmokingZone);

	

		imageMenu.setOnClickListener(this);
		imageRatting.setOnClickListener(this);
		imagePhone.setOnClickListener(this);
		imageFav.setOnClickListener(this);
		imageShare.setOnClickListener(this);

		txtAddReview.setOnClickListener(this);
		txtAddPhoto.setOnClickListener(this);
		
		
		//highLighted deals
		deals_highlight=(LinearLayout)currentView.findViewById(R.id.deals_highlight);
		deals_highlight.setVisibility(View.INVISIBLE);
		dealsName=(TextView)currentView.findViewById(R.id.dealsName);
		dealsProvider=(TextView)currentView.findViewById(R.id.dealsProvider); 
		deals=(TextView)currentView.findViewById(R.id.deals);
		
		
		//final LinearLayout layFeedback=(LinearLayout)currentView.findViewById(R.id.layFeedback);
		currentView.findViewById(R.id.layFeedback).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				 SharedPreferences sharedpreferences = getActivity().getSharedPreferences(RegistrationActivity.LET_EAT_PREFERENCES, Context.MODE_PRIVATE);
				 if(sharedpreferences.getString(RegistrationActivity.Username, "").isEmpty() ){			
						
						//StaticObjects.callRegForAction( getActivity());
					 	Intent intent=new Intent(getActivity(), RegistrationOptionActivityAction.class);
						intent.putExtra(StaticObjects.NEV_TYPE_KEY, StaticObjects.NEV_TYPE_ACTION);
						startActivity(intent);
				}else{
					Intent intent=new Intent(getActivity(), FeedBackActivity.class);
					intent.putExtra("resid", getArguments().getString("restaurantId"));	
					intent.putExtra("flag", FeedBackActivity.EXTRA_FROM_OTHERS);	
					startActivity(intent);
				}
				
				
				
			}
		});
		currentView.findViewById(R.id.layFeedback).setVisibility(View.GONE);
		
		
		
		final NotifyingScrollView ressview=(NotifyingScrollView)currentView.findViewById(R.id.ressview);
		
		 final NotifyingScrollView.OnScrollChangedListener mOnScrollChangedListener = new NotifyingScrollView.OnScrollChangedListener() {
		        public void onScrollChanged(ScrollView scrollView, int l, int t, int oldl, int oldt) {
		        	View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
		            int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
		            
		          //  Log.e("mOnScrollChangedListener", "diff:"+diff+ "  l:"+ l + " t:"+ t + " oldl:"+oldl+ " oldt:"+ oldt);

		            // if diff is zero, then the bottom has been reached
		            if (diff == 0) {
		                // do stuff
		            	
		            	 AnimationTween.animateText(currentView.findViewById(R.id.layFeedback) ,getActivity());

		            }else if (diff > 50){
		            	AnimationTween.animateTextReverse(currentView.findViewById(R.id.layFeedback) ,getActivity());
		            	
		            }
		            
		           
		        }
		    };
		    
		    ressview.setOnScrollChangedListener(mOnScrollChangedListener);
		
		
		    imageViewMap=(ImageView)currentView.findViewById(R.id.imageViewMap);
		    imageViewMap.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					getActivity().getActionBar().setSelectedNavigationItem(TabMainActivity.mapTabPosition);
					TabMainActivity.viewPager.setCurrentItem(TabMainActivity.mapTabPosition); 
				}
			});
		    
		    currentView.findViewById(R.id.ll_address).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					getActivity().getActionBar().setSelectedNavigationItem(TabMainActivity.mapTabPosition);
					TabMainActivity.viewPager.setCurrentItem(TabMainActivity.mapTabPosition); 
				}
			});
		

		curActivity = getActivity();

		// restaurentId = getActivity().getIntent().getStringExtra("Id");
		loadRestInfo=new LoadRestInfo();
		loadRestInfo.execute();

		// adMob
		adView = (AdView) getActivity().findViewById(R.id.adView_resturent);

		AdRequest adRequest = new AdRequest.Builder().build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});

	}
	
	
	
	

	@Override
	public void onClick(View v) {
		Log.d("click", "=========================================" + v);
		// TODO Auto-generated method stub
		// imageMenu, imageMap, imageRatting, imagePhone;
		if (v == imageMenu) {
			Intent in = new Intent(getActivity().getApplicationContext(),
					MenuListView.class);
			in.putExtra("restaurentId", restaurantId);
			in.putExtra("restaurentName", txtRestName.getText());
			startActivity(in);

		}else if (v == imagePhone) {
			if (phonString != null || phonString != "") {
				phoneCallIntent(phonString);
			}

		} else if (v == txtAddPhoto) {
			
			
			SharedPreferences sharedpreferences = getActivity().getSharedPreferences(RegistrationActivity.LET_EAT_PREFERENCES, Context.MODE_PRIVATE);
					
			 if(sharedpreferences.getString(RegistrationActivity.Username, "").isEmpty() ){					
					StaticObjects.callRegForAction( getActivity());
					
				}else{
					Intent intent=new Intent(getActivity(), CapturePhotoActivity.class);
					intent.putExtra("userid", new SavedPrefernce(getActivity()).getUserID() );
					intent.putExtra("resid", getArguments().getString("restaurantId"));					
					startActivity(intent);
				}

		} else if (v == txtAddReview  ) {
			//Toast.makeText(getActivity(), "txtAddReview", Toast.LENGTH_SHORT)	.show();
			 
			 /*&& !StaticObjects.mGoogleApiClient.isConnected()*/
			 SharedPreferences sharedpreferences = getActivity().getSharedPreferences(RegistrationActivity.LET_EAT_PREFERENCES, Context.MODE_PRIVATE);
			 
			 
			// Toast.makeText(getActivity(), ""+sharedpreferences.getString(RegistrationActivity.Username, ""), Toast.LENGTH_LONG).show();
			 
			 if(sharedpreferences.getString(RegistrationActivity.Username, "").isEmpty() ){			
				
				StaticObjects.callRegForAction( getActivity());
			}else{
				//Toast.makeText(getActivity(), "Post review", Toast.LENGTH_LONG).show();
				callReviewDailog();
			}
			
			

		} else if (v == imageRatting ) {
			//Toast.makeText(getActivity(), "txtAddReview", Toast.LENGTH_SHORT)	.show();
			 
			 /*&& !StaticObjects.mGoogleApiClient.isConnected()*/
			 SharedPreferences sharedpreferences = getActivity().getSharedPreferences(RegistrationActivity.LET_EAT_PREFERENCES, Context.MODE_PRIVATE);
			 
			 
			// Toast.makeText(getActivity(), ""+sharedpreferences.getString(RegistrationActivity.Username, ""), Toast.LENGTH_LONG).show();
			 
			 if(sharedpreferences.getString(RegistrationActivity.Username, "").isEmpty() ){			
				
				StaticObjects.callRegForAction( getActivity());
			}else{
				//Toast.makeText(getActivity(), "Post review", Toast.LENGTH_LONG).show();
				callRateDailog();
			}
			
			

		}else if(v==imageFav){
			
			 SharedPreferences sharedpreferences = getActivity().getSharedPreferences(RegistrationActivity.LET_EAT_PREFERENCES, Context.MODE_PRIVATE);
			 
			 
			// Toast.makeText(getActivity(), ""+sharedpreferences.getString(RegistrationActivity.Username, ""), Toast.LENGTH_LONG).show();
			 
			if(  sharedpreferences.getString(RegistrationActivity.Username, "").isEmpty() ){
				
				StaticObjects.callRegForAction( getActivity());
				
				
			}else{
				
				
				userid=new SavedPrefernce(getActivity()).getUserID();
				if(userid.isEmpty()){
					StaticObjects.callRegForAction( getActivity());
				}else{
					//rankDialog.dismiss();						
					new InsertFavorite().execute();
					
					
					
				}
			}
			
			
		} else if(v==imageShare){
			//Toast.makeText(getActivity(), uriLink+"", Toast.LENGTH_SHORT).show();
			shareLink(uriLink);
		}

	}
	
	
	void callReviewDailog(){
		rankDialog = new Dialog(getActivity(), R.style.FullHeightDialog);
		
		rankDialog.setContentView(R.layout.review_dialog);
		rankDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
		
		//open softkeyboard
		rankDialog.getWindow().setSoftInputMode(android.view.WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		
		 /* DisplayMetrics metrics = getResources().getDisplayMetrics();
	      int screenWidth = (int) (metrics.widthPixels *1);
	      rankDialog.setContentView(R.layout.rank_dialog);
	      rankDialog.getWindow().setLayout(screenWidth, LayoutParams.WRAP_CONTENT); */

		rankDialog.setCancelable(true);

		ratingBar = (RatingBar) rankDialog
				.findViewById(R.id.dialog_ratingbar);
		ratingBar.setRating(0);

		 etWtiteComment = (EditText) rankDialog
				.findViewById(R.id.etWtiteComment);
		//text.setText(txtRestName.getText());
		 
		 //change rating bar color
		 //RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingBar);
		// LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
		// stars.getDrawable(2).setColorFilter(Color.YELLOW, Mode.SRC_ATOP);

		 
		 
		 
		 View.OnClickListener updateListener=new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					String ratingConvertedToDB = ""+ (ratingBar.getRating() * StaticObjects.RATING_OFFSET);
					
					rating=ratingConvertedToDB;
					comment=etWtiteComment.getText().toString();
					userid=new SavedPrefernce(getActivity()).getUserID();
					if(userid.isEmpty()){
						StaticObjects.callRegForAction( getActivity());
					}else{
						rankDialog.dismiss();						
						new PostReview().execute();
					}
					

				}
			};
			
			
			
			View.OnClickListener cancelListener=new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					rankDialog.cancel();
				}
			};
			
		ImageView cancelButton = (ImageView) rankDialog.findViewById(R.id.rank_dialog_cancel_button);
		cancelButton .setOnClickListener(cancelListener);	
		
		LinearLayout layRankDailogConfirm = (LinearLayout) rankDialog.findViewById(R.id.layRankDailogConfirm);
		layRankDailogConfirm.setOnClickListener(updateListener);
		
		ImageView btnRankDailogConfirm = (ImageView) rankDialog.findViewById(R.id.btnRankDailogConfirm);
		btnRankDailogConfirm.setOnClickListener(updateListener);
		
		// now that the dialog is set up, it's time to show it
		rankDialog.show();
	}
	
	
	void callRateDailog(){
		rankDialog = new Dialog(getActivity(), R.style.RankDialog);
		
		rankDialog.setContentView(R.layout.ratting_dialog);
		rankDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		
		rankDialog.getWindow().getAttributes().windowAnimations=R.style.dialog_animation;

		rankDialog.setCancelable(true);

		ratingBar = (RatingBar) rankDialog.findViewById(R.id.dialog_ratingbar);
		ratingBar.setRating(0);

		 View.OnClickListener updateListener=new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					String ratingConvertedToDB = ""+ (ratingBar.getRating() * StaticObjects.RATING_OFFSET);
					
					rating=ratingConvertedToDB;
					comment="";
					userid=new SavedPrefernce(getActivity()).getUserID();
					if(userid.isEmpty()){
						StaticObjects.callRegForAction(getActivity());
					}else{
						rankDialog.dismiss();						
						new PostReview().execute();
					}
					

				}
			};
			
			
			
			View.OnClickListener cancelListener=new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					rankDialog.cancel();
				}
			};
			
		Button cancelButton = (Button) rankDialog.findViewById(R.id.rank_dialog_button2);
		cancelButton.setOnClickListener(cancelListener);	
		
		Button updateButton= (Button) rankDialog.findViewById(R.id.rank_dialog_button);
		updateButton.setOnClickListener(updateListener);
		
		// now that the dialog is set up, it's time to show it
		rankDialog.show();
	}

	void phoneCallIntent(final String finalPhone) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				getActivity());

		// set title
		alertDialogBuilder.setTitle("Call "+txtRestName.getText());

		// set dialog message
		alertDialogBuilder
				.setMessage( finalPhone )
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

								Intent callIntent = new Intent(
										Intent.ACTION_CALL);
								callIntent.setData(Uri.parse("tel:"
										+ finalPhone));
								startActivity(callIntent);
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
	
	
	void shareLink(Uri uri){
		Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
 
        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_title));
        share.putExtra(Intent.EXTRA_TEXT, uri+"");
 
       //share window title
        startActivity(Intent.createChooser(share, "Share link!"));
	}
	
	

	String restaurantId = null;
	ResturentDetails resturentDetails;

	 String userid, comment,rating;
	 ProgressBar progressBarRestPic =null;

	class LoadRestInfo extends AsyncTask<String, String, String> {

		protected boolean isAlreadyAdded;

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			currentView.findViewById(R.id.info_custom_progress).setVisibility(View.VISIBLE);
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {

				/*String types = "ResId=" + restaurantId;
				resturentList = foodyResturent.getResturentDetails(types);*/
				
				resturentList = new ArrayList<ResturentDetails>();
				resturentList.add(0, (ResturentDetails)getArguments().getSerializable("ResturentDetails"));
				

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			if(!isCancelled()) {
				if(currentView!=null){
					if(currentView!=null){
						//currentView.findViewById(R.id.info_custom_progress).setVisibility(View.GONE);
						currentView.findViewById(R.id.ressview).setVisibility(View.VISIBLE);
						
						AnimationTween.animateView(currentView.findViewById(R.id.info_custom_progress), getActivity());
						AnimationTween.animateViewPosition(currentView.findViewById(R.id.ressview), getActivity());
					}
					
				}
				
				if(getActivity()==null)
					return;
				// updating UI from Background Thread
				getActivity().runOnUiThread(new Runnable() {
	
					public void run() {
						/**
						 * Updating parsed Places into LISTVIEW
						 * */
						// Get json response status
	
						if (resturentList == null || resturentList.size() <= 0) {
							
							if(currentView!=null){						
							currentView.findViewById(R.id.info_custom_progress).setVisibility(View.VISIBLE);
							currentView.findViewById(R.id.info_custom_progress).findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
							TextView textPreparing=(TextView)currentView.findViewById(R.id.info_custom_progress).findViewById(R.id.textPreparing);
							textPreparing.setText("Nothing found.");
							}
	
						} else if (resturentList != null) {
							// Check for all possible status
	
							// Successfully got places details
							if (resturentList != null) {
								// loop through each place
								ResturentDetails p = resturentList.get(0);
	
								phonString = p.phone;
	
								currentRating = (Math.ceil(Double.parseDouble(p.rateing) / StaticObjects.RATING_OFFSET));
	
								txtRateValue.setText(currentRating+"");
	
								txtRestName.setText(p.Name);
								
								String cusine = p.cusine.equals("")	|| p.cusine.equals("-") ? "N/A": p.cusine;
								txtCuisine.setText(cusine);
								txtCuisine.setOnClickListener(new OnClickListener() {
									
									@Override
									public void onClick(View v) {
									
										gotoMenuTab();
									}
								});
								
								String Address = p.Address.equals("") ? "N/A": p.Address ;
								txtRestAddress.setText(Address);
								
								
								
								//open or close
								String Open = p.Open.equals("") ? "N/A": p.Open + "-" + p.Close;
								String openClose="";
								if(!Open.equals("N/A")){
									try {
										if ( p.Open.equals(p.Close) || CF.isTimeBetweenTwoTime(CF.timePatternmaker(p.Open),  CF.timePatternmaker(p.Close)) ){
											
											openClose="Open "; 
											
											if(p.Open.equals(p.Close))
												Open="24 hours";
											else
												Open = p.Open.equals("") ? "N/A": p.Open + "-" + p.Close;
											
											txtTime.setText(openClose+Open);
											// Spannable string allows  to edit the formatting of the text.	 redish #339933									
									         SpannableString titleText = new SpannableString(txtTime.getText());
									         titleText.setSpan(new ForegroundColorSpan(Color.parseColor("#339933")), 0, openClose.length() , 0);
									         txtTime.setText(titleText);
									       
											
										}else{
											
											String tot=CF.openTodayOrTomorrow(CF.timePatternmaker(p.Open), CF.timePatternmaker(p.Close));
											
											openClose="Closed. "; Open = p.Open.equals("") ? "N/A": "Open "+tot+" "+ p.Open + "-" + p.Close;
											
											txtTime.setText(openClose+Open);
											// Spannable string allows us to edit the formatting of the text.	greenish #ED2524								
									         SpannableString titleText = new SpannableString(txtTime.getText());
									         titleText.setSpan(new ForegroundColorSpan(Color.parseColor("#ED2524")), 0, openClose.length() , 0);
									         txtTime.setText(titleText);
										}
									} catch (ParseException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}else{
									txtTime.setText(openClose+Open);
								}
								
								
								
								
								String Price = p.Price.equals("") ? "N/A": "Tk."+p.Cost4Two+" for two people (approx.)";// abir bhai sust:Approximate cost for two person 
								txtCost.setText(Price);
								//txtCost.setText();
								
								if(p.CreditCard.equals("Yes")){
									txtCashCard.setText("Cash and Credit Card Accepted");
								}else{
									txtCashCard.setText("Cash Accepted");
								}
								
								String Wifi = p.Wifi.equals("") ? "N/A": p.Wifi;
								txtInternet.setText(Wifi);
								
								
								//LayoutSeatingCapacity
								//String SeatingCapacity = p.SeatingCapacity.equals("") ? "N/A": p.SeatingCapacity;
								if(p.SeatingCapacity.equals("") || Integer.parseInt(p.SeatingCapacity)<=0){
									currentView.findViewById(R.id.LayoutSeatingCapacity).setVisibility(View.GONE);
								}else{
									txtSeatingCapacity.setText(p.SeatingCapacity);
								}
								
								
								
	
								// hilights
								if (p.HomeDelivery.equals("Yes")) {
									imageHD.setImageResource(R.drawable.ic_yes);
								}
	
								// SettingOutside
								if (p.SettingOutside.equals("Yes")) {
									imageSO.setImageResource(R.drawable.ic_yes);
								}
	
								// DineIn
								if (p.DineIn.equals("Yes")) {
									imageDinner.setImageResource(R.drawable.ic_yes);
								}
	
								// TableReservation
								if (p.TableReservation.equals("Yes")) {
									imageTR.setImageResource(R.drawable.ic_yes);
								}
	
								// BreakFast
								if (p.BreakFast.equals("Yes")) {
									imageB.setImageResource(R.drawable.ic_yes);
								}
	
								// Bar
								if (p.Bar.equals("Yes")) {
									imageBar.setImageResource(R.drawable.ic_yes);
								}
	
								// Bar
								if (p.Bar.equals("Yes")) {
									imageBar.setImageResource(R.drawable.ic_yes);
								}
	
								// AC
								if (p.AC.equals("Yes")) {
									imageAC.setImageResource(R.drawable.ic_yes);
								}
	
								// imageBuffet
								if (p.RestType.equals("Yes")) {
									imageBuffet.setImageResource(R.drawable.ic_yes);
								}
	
								// imageLM
								if (p.LiveMusic.equals("Yes")) {
									imageLM.setImageResource(R.drawable.ic_yes);
								}
	
								// imageLM
								if (p.Veg.equals("Yes")) {
									imageVeg.setImageResource(R.drawable.ic_yes);
								}
	
								// imageLM
								if (p.NonVeg.equals("Yes")) {
									imageNV.setImageResource(R.drawable.ic_yes);
								}
								if (p.NonVeg.equals("Yes")) {
									imageNV.setImageResource(R.drawable.ic_yes);
								}
								
								//imageKidz 
								if (p.Kidz.equals("Yes")) {
									imageKidz.setImageResource(R.drawable.ic_yes);
								}
								
								//imageSmoking
								if (p.Smoking.equals("Yes")) {
									imageSmoking.setImageResource(R.drawable.ic_yes);
								}
								
								//imageSmoking
								if (p.Buffet.equals("Yes")) {
									imageBuffet.setImageResource(R.drawable.ic_yes);
								}
								
								
								LinearLayout LayoutCardGiftHolder=(LinearLayout) currentView.findViewById(R.id.LayoutCardGiftHolder);
								//LinearLayout LayoutCardGift=(LinearLayout) currentView.findViewById(R.id.LayoutCardGift);
								ImageView imageCard=(ImageView) currentView.findViewById(R.id.imageCard);
								//TextView txtDiscount=(TextView) currentView.findViewById(R.id.txtDiscount);
								
								
								//card and offer
								if(p.Cards.size()<=0){
									
									//LayoutCardGift.setVisibility(LinearLayout.GONE);
									LayoutCardGiftHolder.setVisibility(LinearLayout.GONE);
									
								}else if(!isCardsAdded){
									
									//LayoutCardGiftHolder.removeAllViews();
									
									/*if(LayoutCardGiftHolder.getChildCount()>1) 
										LayoutCardGiftHolder.removeViews(1, (LayoutCardGiftHolder.getChildCount()));*/
									
									for (int i = 0; i <LayoutCardGiftHolder.getChildCount(); i++) {
										LayoutCardGiftHolder.getChildAt(i).setVisibility(View.GONE);
									}
									
									
									
									isAlreadyAdded=false;
									int numOfCard=0;
									for (Card card : p.Cards) {
										
										isCardsAdded=true;
										
										//add dynamic text view
										LinearLayout linearLayoutMain=LayoutCardGiftHolder;
										
										LinearLayout cardLayout= new LinearLayout(getActivity().getApplicationContext());
										LayoutParams cardLayoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
										/*cardLayoutParams.setMargins(0, 3, 0, 0);
										cardLayout.setOrientation(LinearLayout.HORIZONTAL);
										cardLayout.setLayoutParams(cardLayoutParams);
										cardLayout.setGravity(Gravity.TOP);*/
										
										
										//LayoutParams lp = (LayoutParams) LayoutCardGift.getLayoutParams();
										cardLayout.setLayoutParams(cardLayoutParams);
										//(LayoutParams) cardLayout.getLayoutParams();
										
										ImageView giftIcon=new ImageView(getActivity().getApplicationContext());
										LayoutParams lp2 = (LayoutParams)imageCard.getLayoutParams();
										giftIcon.setLayoutParams(lp2);
										giftIcon.setImageResource(R.drawable.gift);
										
										
										
										
										
										TextView tv=new TextView(getActivity().getApplicationContext());
										tv.setGravity(Gravity.TOP);
										tv.setSingleLine(false);
										tv.setMaxLines(2);
										
										
										LayoutParams tvParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
										tvParams.setMargins(8, 0, 0, 0);									
										tv.setLayoutParams(tvParams);
										
										if(card.IsLetsEat.equals("1"))
										tv.setText(card.DisBrief);
										else
										tv.setText(card.DisPercent+" "+card.DisCompany+" "+card.DisName);
										
										// Spannable string allows us to edit the formatting of the text.
										 String title = tv.getText().toString();	
										 tv.setTextColor(Color.parseColor("#6d6e71"));
								         SpannableString titleText = new SpannableString(title);
								         titleText.setSpan(new ForegroundColorSpan(Color.parseColor("#ED2524")), 0, card.DisPercent.length() , 0);
								         tv.setText(titleText);
										
										cardLayout.addView(giftIcon);
										cardLayout.addView(tv);
										linearLayoutMain.addView(cardLayout);
										
										if (numOfCard>=1) {
											giftIcon.setVisibility(View.INVISIBLE);
										}
										
										numOfCard++;
										
										//highLighted deals
										if(!isAlreadyAdded){
											
											if(card.IsHighlighted.equals("1") ){
												
												deals_highlight.setVisibility(View.VISIBLE);
												dealsName.setText(card.DisName);
												dealsProvider.setText(card.DisCompany); 
												deals.setText(card.DisPercent);
												isAlreadyAdded=true;
												deals_highlight.setOnClickListener(new View.OnClickListener() {
													
													@Override
													public void onClick(View v) {
														gotoDealsTab(); 
													}

													
												} );
												
											}else{
												deals_highlight.setVisibility(View.INVISIBLE);
											}
										}
										
										
										
									}
									
									//add listener goto deals tab
									LayoutCardGiftHolder.setOnClickListener(new OnClickListener() {
										
										@Override
										public void onClick(View v) {
											// TODO Auto-generated method stub
											gotoDealsTab();
										}
									});
								}
								
								
								
								if(getView()!=null){
									progressBarRestPic =(ProgressBar)getView().findViewById(R.id.progressBarResDetails);
								}
								
								//loading image
								if(p.IMG!=null && !p.IMG.isEmpty()){
									final String pIMG=p.IMG;
									Picasso.with(getActivity())
								    .load(p.IMG)
								    .placeholder(R.drawable.lazy_ic)
								    .error(R.drawable.lazy_ic)
								    .into(imageRestaurent, new Callback(){
		
										@Override
										public void onError() {
											// TODO Auto-generated method stub
											if(progressBarRestPic!=null)
												progressBarRestPic.setVisibility(View.GONE);
										}
		
										@Override
										public void onSuccess() {
											// TODO Auto-generated method stub
											if(progressBarRestPic!=null)
												progressBarRestPic.setVisibility(View.GONE);
										}
								    	
								    });
									
									imageRestaurent.setOnClickListener(new OnClickListener() {
										
										@Override
										public void onClick(View v) {
											
											bannerImageFullScreen(pIMG);
											
										}
									});
								}else{
									if(progressBarRestPic!=null)
										progressBarRestPic.setVisibility(View.GONE);
								}
	
								// share uri
								uriLink = Uri
										.parse(FoodyResturent.FB_SHARE_URL
												+ p.Id);
								uriTitle = Uri.parse(p.Name);
								uriImage = Uri.parse(p.IMG);
	
							} else if (resturentList == null) {
								// Zero results found
								if(currentView!=null){						
									currentView.findViewById(R.id.info_custom_progress).setVisibility(View.VISIBLE);
									currentView.findViewById(R.id.info_custom_progress).findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
									TextView textPreparing=(TextView)currentView.findViewById(R.id.info_custom_progress).findViewById(R.id.textPreparing);
									textPreparing.setText("Nothing found.");
								}
							}
						}
					}
	
				});
	
			}
	}

	}
	
	
	private void gotoDealsTab() {
		getActivity().getActionBar().setSelectedNavigationItem(TabMainActivity.dealsTabPosition);
		TabMainActivity.viewPager.setCurrentItem(TabMainActivity.dealsTabPosition);
		
	}
	
	private void gotoMenuTab() {
		getActivity().getActionBar().setSelectedNavigationItem(TabMainActivity.menuTabPosition);
		TabMainActivity.viewPager.setCurrentItem(TabMainActivity.menuTabPosition);
		
	}
	
	
	void bannerImageFullScreen(String pIMG){
		// TODO Auto-generated method stub
		ArrayList<HashMap<String, String>> _filePaths = new ArrayList<HashMap<String, String>>();
    	 HashMap<String, String> map = new HashMap<String, String>();
		map.put("IMG",pIMG);	
		map.put("CAPTION", "");
		_filePaths.add(map);
		//add false data as last map is removed from arrayList when "GridViewImageAdapterNew.GalleryTypeGal"
		 map = new HashMap<String, String>();
			map.put("IMG","");	
			map.put("CAPTION", "");
			_filePaths.add(map);
		
    	Intent i = new Intent(getActivity(), FullScreenViewActivity.class);
		i.putExtra("_filePaths", _filePaths);
		i.putExtra("position", 0);
		i.putExtra("galleryType", GridViewImageAdapterNew.GalleryTypeGal);
		startActivity(i);
	}
	
	String response;
	class PostReview extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new MyProgressDialog(getActivity());
			// pDialog.setMessage(Html.fromHtml("<b>Loading ...</b>"));
			// pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected String doInBackground(String... args) {
			
			foodyResturent = new FoodyResturent();

			try {
				response = foodyResturent.insertReview(userid, comment, restaurantId, rating);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

	
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			// updating UI from Background Thread
			getActivity().runOnUiThread(new Runnable() {

				public void run() {
					
					if(response.equals("success")){
						Toast.makeText(getActivity(), "Post Successful.", Toast.LENGTH_SHORT).show();
						double updatedRating=0f;
						//ui update
						//try {
						int ratingDiv=currentRating>0?2:1;
						
							updatedRating = ( currentRating+(Double.parseDouble(rating)/StaticObjects.RATING_OFFSET) ) / ratingDiv;
						//} catch (NumberFormatException e) {
							
						//}
						
						txtRateValue.setText(""+updatedRating);
						
						AnimationTween.zoomInOut(getView().findViewById(R.id.ll_rate_info), getActivity());
					}else{
						Toast.makeText(getActivity(), "Try again.", Toast.LENGTH_SHORT).show();
					}
				}

			});

		}

	}
	
	public static Float precision(int decimalPlace, Float d) {

	    BigDecimal bd = new BigDecimal(Float.toString(d));
	    bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
	    return bd.floatValue();
	  }
	
	
	
	
	class InsertFavorite extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			AnimationTween.zoomInOut(imageFav, getActivity());
		}

		protected String doInBackground(String... args) {
			
			foodyResturent = new FoodyResturent();

			try {
				response = foodyResturent.insertFavorite(userid,  restaurantId);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

	
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			
			// updating UI from Background Thread
			getActivity().runOnUiThread(new Runnable() {

				public void run() {
					
					if(response.equals("Fav")){
						Toast.makeText(getActivity(), "Favorite Added.", Toast.LENGTH_SHORT).show();
						imageFav.setImageResource(R.drawable.ic_user_fav_red);
					}else if(response.equals("UnFav")){
						Toast.makeText(getActivity(), "Favorite Removed.", Toast.LENGTH_SHORT).show();
						imageFav.setImageResource(R.drawable.ic_favourite_gray);
					}else{
						Toast.makeText(getActivity(), "Try again.", Toast.LENGTH_SHORT).show();
					}
				}

			});

		}

	}

	
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

			return super.onOptionsItemSelected(item);
	}
	
	/*@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		super.onCreateOptionsMenu(menu, inflater);
		
		inflater.inflate(R.menu.activity_main, menu);
		// Associate searchable configuration with the SearchView
		SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
	}
	
	
	
			
			
	 
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
		case android.R.id.home:
			Toast.makeText(curActivity, "hello home", Toast.LENGTH_LONG).show();
			// NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.action_search:
			// search action
			return true;
		case R.id.action_nearby:
			Toast.makeText(curActivity, "hello fragment", Toast.LENGTH_LONG).show();
			//startActivity(i);
			return true;
		default:
			break;
		}
		
		return false;

	}*/
	
	
	
	
	
	


	// adMOb
	@Override
	public void onPause() {
		super.onPause();
		 
		isCardsAdded=false;
		 
		if (adView != null)
			adView.pause();

	}

	@Override
	public void onResume() {
		super.onResume();
		
		 isCardsAdded=false;

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		if (adView != null)
			adView.destroy();
		super.onDestroy();
		
		if(loadRestInfo != null && loadRestInfo.getStatus() == Status.RUNNING) {
			loadRestInfo.cancel(true);
			}
	}

}
