package com.mcc.letseat.tab;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.foody.jsondata.FoodyResturent;
import com.foody.nearby.AlertDialogManager;
import com.foody.nearby.GPSTracker;
import com.foody.nearby.MyListAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mcc.letseat.R;
import com.mcc.letseat.imageslider.adapter.GridViewImageAdapterNew;
import com.mcc.letseat.imageslider.helper.AppConstant;
import com.mcc.letseat.imageslider.helper.Utils;
import com.mcc.libs.AnimationTween;

/**
 * MenuFragment provide scan or list menu of a particular restaurant that hosted in TabMainActivity 
 * @author Arif
 *
 */

public class MenuFragment extends Fragment {

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Google Places
	FoodyResturent foodyResturent;

	// Places List
	ArrayList<com.foody.jsondata.Menu> menuList;

	// GPS Location
	GPSTracker gps;

	// Button
	ImageView btnShowOnMap;

	// Progress dialog
	//ProgressDialog pDialog;

	// Places Listview
	ListView lv;

	// ListItems data
	ArrayList<HashMap<String, String>> resturantListItems = new ArrayList<HashMap<String, String>>();

	// //admob
	AdView adView = null;

	ImageView searchAny;

	TextView restaurantName;

	String restaurentId;

	View currentView = null;
	
	LoadMenus asyncTask;
	
	public static final int MENU_TYPE_LIST=1;
	public static final int MENU_TYPE_PHOTO=2;
	
	int menuType=0;
	
	private Utils utils;
	//private ArrayList<String> imagePaths = new ArrayList<String>();
	private GridViewImageAdapterNew adapterImage;
	private GridView gridView;
	private int columnWidth;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		/*// Inflate the layout for this fragment
		restaurentId = getArguments().getString("restaurantId");
		Log.e("restaurentId", restaurentId);
		currentView = inflater.inflate(R.layout.restaurant_menu, container,false);
		return currentView;*/
		
		setRetainInstance(true);
		
		if (currentView != null) {
			ViewGroup parent = (ViewGroup) currentView.getParent();
			if (parent != null)
				parent.removeView(currentView);
		}
		
		try {
			restaurentId = getArguments().getString("restaurantId");
			currentView = inflater.inflate(R.layout.restaurant_menu, container,false);
		} catch (InflateException e) {
			/* map is already there, just return view as it is */
		}
		
		return currentView;
	}
	
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//enable action bar home button from fragment
		setHasOptionsMenu(true) ;
		getActivity().getActionBar().setHomeButtonEnabled(true);
	
		
		getActivity().getWindow().getDecorView().findViewById(android.R.id.content).setBackgroundResource(R.mipmap.home_back);
		
		
	}
	
	
	
	
	
	

	@Override
	public void onStart() {
		super.onStart();

		
		lv = (ListView) currentView.findViewById(R.id.list);

		restaurantName = (TextView) currentView.findViewById(R.id.restName);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				

			}
		});
		
		
		// Initilizing Grid View
		gridView = (GridView) currentView.findViewById(R.id.grid_view_menu);
		utils = new Utils(getActivity());		
		InitilizeGridLayout();
		
		

		adView = (AdView) this.currentView.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder().addTestDevice(
				AdRequest.DEVICE_ID_EMULATOR).build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});

		// Toast.makeText(ResturentListView.this, "hello"+searchKeywords,
		// Toast.LENGTH_LONG).show();

		// restaurentId=getActivity().getIntent().getStringExtra("restaurentId");
		restaurantName.setVisibility(View.GONE);

		//new LoadMenus(this).execute();
		// handleIntent(getIntent());
		
		//configure the fragment instance to be retained on configuration change
		 asyncTask = new LoadMenus(this);
	     asyncTask.execute();

	}
	
	private void InitilizeGridLayout() {
		Resources r = getResources();
		float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				AppConstant.GRID_PADDING, r.getDisplayMetrics());

		columnWidth = (int) ((utils.getScreenWidth() - ((AppConstant.NUM_OF_COLUMNS + 1) * padding)) / AppConstant.NUM_OF_COLUMNS);

		gridView.setNumColumns(AppConstant.NUM_OF_COLUMNS);
		gridView.setColumnWidth(columnWidth);
		gridView.setStretchMode(GridView.NO_STRETCH);
		gridView.setPadding((int) padding, (int) padding, (int) padding,
				(int) padding);
		gridView.setHorizontalSpacing((int) padding);
		gridView.setVerticalSpacing((int) padding);
	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
		case R.id.action_search:
			// search action
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	@Override
	public void onPause() {
		super.onPause();

		if (adView != null)
			adView.pause();

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		if (adView != null)
			adView.destroy();
		super.onDestroy();
		
		if(asyncTask != null && asyncTask.getStatus() == Status.RUNNING) {
			asyncTask.cancel(true);
			}
	}

	private  class LoadMenus extends AsyncTask<String, String, String> {
		
		

        private LoadMenus (MenuFragment fragment) {
           
        }

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			if(currentView!=null)
			currentView.findViewById(R.id.menu_custom_progress).setVisibility(View.VISIBLE);

		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {

				String types = "ResId=" + restaurentId;

				menuList = foodyResturent.getMenu(types);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			super.onPostExecute(file_url);
			if(!isCancelled()){
				
				
				
				// dismiss the dialog after getting all products
				if (currentView != null){
					//currentView.findViewById(R.id.menu_custom_progress).setVisibility(View.GONE);
					
					AnimationTween.animateView(currentView.findViewById(R.id.menu_custom_progress), getActivity());
					
					if(menuType==MENU_TYPE_LIST)
					AnimationTween.animateViewPosition(lv, getActivity());
					
					if(menuType==MENU_TYPE_PHOTO)
					AnimationTween.animateViewPosition(gridView, getActivity());
				}
					
					
	
				if(getActivity()==null)
					return;
				// updating UI from Background Thread
				getActivity().runOnUiThread(new Runnable() {
					public void run() {
						/**
						 * Updating parsed Places into LISTVIEW
						 * */
						// Get json response status
	
						if (menuList == null || menuList.size() <= 0) {
	
							if(currentView!=null){						
								currentView.findViewById(R.id.menu_custom_progress).setVisibility(View.VISIBLE);
								currentView.findViewById(R.id.menu_custom_progress).findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
								TextView textPreparing=(TextView)currentView.findViewById(R.id.menu_custom_progress).findViewById(R.id.textPreparing);
								textPreparing.setText("Nothing found.");
								}
							// showPopUp();
	
						} else if (menuList != null) {
							// Check for all possible status
	
							// Successfully got places details
							if (menuList != null) {
								
								
	
								resturantListItems = new ArrayList<HashMap<String, String>>();
								// loop through each place
								for (com.foody.jsondata.Menu p : menuList) {
	
									HashMap<String, String> map = new HashMap<String, String>();
									if(p.MenuName.equals("") &&  p.Price.equals("") && p.FoodCat.equals("")){
										menuType=MENU_TYPE_PHOTO;
										map.put("IMG", p.IMG);
										map.put("CAPTION", "");	
										map.put("RESID", "");
										map.put("THUMBIMG", p.ThumbIMG);
											
									}
									else
									{
										menuType=MENU_TYPE_LIST;
										map.put("MenuName", p.MenuName);
										map.put("Price", "Tk. " + p.Price);
										map.put("FoodCat", p.FoodCat);
									
									}
									
									
	
									Log.e(">>>>>>>>>>menu>>>>>>>>>>>>>>>>>> ", p.MenuName);
	
									// adding HashMap to ArrayList
									resturantListItems.add(map);
								}
								
								if(menuType==MENU_TYPE_PHOTO){
									lv.setVisibility(View.GONE);
									// Gridview adapter
									adapterImage = new GridViewImageAdapterNew(getActivity(), resturantListItems,columnWidth, new String[] {  "IMG", "CAPTION", "RESID", "THUMBIMG"}, GridViewImageAdapterNew.GalleryTypeMenu);
	
									// setting grid view adapter
									
									gridView.setAdapter(adapterImage);
									adapterImage.notifyDataSetChanged();
								}else{
									gridView.setVisibility(View.GONE);
									// list adapter
									MyListAdapter adapter = new MyListAdapter(getActivity(), resturantListItems,	new String[] { "MenuName", "Price","FoodCat" });
	
									// Adding data into listview
									lv.setAdapter(adapter);
									
								}
							
								
							}
	
							else if (menuList == null) {
								// Zero results found
								if(currentView!=null){						
									currentView.findViewById(R.id.menu_custom_progress).setVisibility(View.VISIBLE);
									currentView.findViewById(R.id.menu_custom_progress).findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
									TextView textPreparing=(TextView)currentView.findViewById(R.id.menu_custom_progress).findViewById(R.id.textPreparing);
									textPreparing.setText("Nothing found.");
									}
							}
						}
					}
	
				});
			
		

			}
		}

	}

	/*
	 * @Override public boolean onNavigationItemSelected(int arg0, long arg1) {
	 * // TODO Auto-generated method stub return false; }
	 */

	/*private void showPopUp() {

		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(getActivity());
		// helpBuilder.setTitle("Pop Up");
		helpBuilder
				.setMessage("Nothing Found. Want to request this suggestion?");
		helpBuilder.setPositiveButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {

					}
				});

		helpBuilder.setNegativeButton("Yes",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						Intent in = new Intent(getActivity()
								.getApplicationContext(),
								SuggetionRequest.class);
						in.putExtra("keyword", restaurentId);

						startActivity(in);
					}
				});

		// Remember, create doesn't show the dialog
		AlertDialog helpDialog = helpBuilder.create();
		helpDialog.show();

	}*/

}
