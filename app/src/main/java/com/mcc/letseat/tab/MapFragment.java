package com.mcc.letseat.tab;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.json.JSONObject;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.foody.jsondata.DirectionsJSONParser;
import com.foody.nearby.GPSTracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mcc.letseat.R;

/**
 * MapFragment provide Location map and route of a particular restaurant that hosted in TabMainActivity, using Google Places API 
 * @author Arif
 *
 */

public class MapFragment extends Fragment implements OnMarkerClickListener,
		OnInfoWindowClickListener, OnMarkerDragListener,
		OnSeekBarChangeListener, OnMyLocationButtonClickListener, ActivityCompat.OnRequestPermissionsResultCallback {

	private LatLng DESTINATION = new LatLng(24.897778, 91.871389);

	GPSTracker gpsTracker;

	boolean isOk = false;

	class CustomInfoWindowAdapter implements InfoWindowAdapter {
		// private final RadioGroup mOptions;

		// These a both viewgroups containing an ImageView with id "badge" and
		// two TextViews with id
		// "title" and "snippet".
		private final View mWindow;

		// private final View mContents;

		CustomInfoWindowAdapter() {
			mWindow = getActivity().getLayoutInflater().inflate(
					R.layout.custom_info_window, null);
			// mContents =
			// getLayoutInflater().inflate(R.layout.custom_info_contents, null);
			// mOptions = (RadioGroup)
			// findViewById(R.id.custom_info_window_options);
		}

		@Override
		public View getInfoWindow(Marker marker) {
			/*
			 * if (mOptions.getCheckedRadioButtonId() !=
			 * R.id.custom_info_window) { // This means that getInfoContents
			 * will be called. return null; }
			 */
			render(marker, mWindow);
			return mWindow;
		}

		@Override
		public View getInfoContents(Marker marker) {
			// if (mOptions.getCheckedRadioButtonId() !=
			// R.id.custom_info_contents) {
			// This means that the default info contents will be used.
			return null;
			/*
			 * } render(marker, mContents); return mContents;
			 */
		}

		private void render(Marker marker, View view) {
			int badge = R.mipmap.ic_launcher;// badge = R.drawable.badge_wa;

			((ImageView) view.findViewById(R.id.badge)).setImageResource(badge);

			String title = marker.getTitle();
			TextView titleUi = ((TextView) view.findViewById(R.id.title));
			if (title != null) {
				// Spannable string allows us to edit the formatting of the
				// text.
				SpannableString titleText = new SpannableString(title);
				titleText.setSpan(
						new ForegroundColorSpan(Color.parseColor("#ED2524")),
						0, titleText.length(), 0);
				titleUi.setText(titleText);
			} else {
				titleUi.setText("");
			}

			String snippet = marker.getSnippet();
			TextView snippetUi = ((TextView) view.findViewById(R.id.snippet));
			if (snippet != null && snippet.length() > 12) {
				SpannableString snippetText = new SpannableString(snippet);
				snippetText.setSpan(new ForegroundColorSpan(Color.GRAY), 0,
						snippet.length(), 0);
				snippetUi.setText(snippetText);
			} else {
				snippetUi.setText("");
			}
		}
	}

	private GoogleMap mMap;

	private Marker mPerth;
	private Marker mAdelaide;

	private final List<Marker> mMarkerRainbow = new ArrayList<Marker>();

	// private TextView mTopText;
	// private SeekBar mRotationBar;
	// private CheckBox mFlatBox;

	private final Random mRandom = new Random();

	String restaurentName = null;
	String restaurentAddress = null;

	private static View view;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


		restaurentName = getArguments().getString("RestName");
		restaurentAddress = getArguments().getString("address");

		/*
		 * Toast.makeText(getActivity(),
		 * ""+getArguments().getString("latitude")+
		 * ":"+getArguments().getString("longitude"), Toast.LENGTH_LONG).show();
		 * Log.e("===========lat lng================",getArguments().getString(
		 * "latitude")+":"+getArguments().getString("longitude"));
		 * System.out.println
		 * ("==========================="+getArguments().getString
		 * ("latitude")+":"+getArguments().getString("longitude"));
		 */

		try {
			DESTINATION = new LatLng(Double.valueOf(
					getArguments().getString("latitude")).doubleValue(), Double.valueOf(getArguments().getString("longitude")).doubleValue());
			isOk = true;
		} catch (Exception e) {
			isOk = false;
		}

		if (view != null) {
			ViewGroup parent = (ViewGroup) view.getParent();
			if (parent != null)
				parent.removeView(view);
		}

		try {
			view = inflater.inflate(R.layout.single_marker, container, false);
		} catch (InflateException e) {
			/* map is already there, just return view as it is */
		}

		return view;

	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//enable action bar home button from fragment
		setHasOptionsMenu(true);
		getActivity().getActionBar().setHomeButtonEnabled(true);


		getActivity().getWindow().getDecorView().findViewById(android.R.id.content).setBackgroundResource(R.mipmap.home_back);
	}

	@Override
	public void onStart() {

		super.onStart();

		if (isOk) {
			setUpMapIfNeeded();

			// map route task
			// set direction path
			gpsTracker = new GPSTracker(getActivity());
			LatLng origin = new LatLng(gpsTracker.latitude,
					gpsTracker.longitude);
			LatLng dest = DESTINATION;

			// Getting URL to the Google Directions API
			String url = getDirectionsUrl(origin, dest);

			DownloadTask downloadTask = new DownloadTask();

			// Start downloading json data from Google Directions API
			downloadTask.execute(url);
		} else {
			Toast.makeText(getActivity(), "Location not found.", Toast.LENGTH_SHORT).show();
		}

	}

	@Override
	public void onResume() {
		super.onResume();
		setUpMapIfNeeded();
	}
	
	/*@Override
	public void onDestroyView() {
	    super.onDestroyView();
	    mMap = ((SupportMapFragment) getActivity()
				.getSupportFragmentManager().findFragmentById(R.id.map))
				.getMap();
	    if (mMap != null) 
	    	getActivity().getSupportFragmentManager().beginTransaction().remove(mMap).commit();
	}*/

	private void setUpMapIfNeeded() {
		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (mMap == null) {
			// Try to obtain the map from the SupportMapFragment.
			FragmentActivity activity = getActivity();
			com.google.android.gms.maps.MapFragment mapFragment = (com.google.android.gms.maps.MapFragment) activity.getFragmentManager().findFragmentById(R.id.map);
			mapFragment.getMapAsync(new OnMapReadyCallback() {
				@Override
				public void onMapReady(GoogleMap map) {
					mMap = map;
					// Check if we were successful in obtaining the map.
					if (mMap != null) {
						setUpMap();
					}
				}

			});

			/*((SupportMapFragment) activity.getSupportFragmentManager().findFragmentById(R.id.map))
					.getMapAsync(new OnMapReadyCallback() {
						@Override
						public void onMapReady(GoogleMap map) {
							mMap = map;
							// Check if we were successful in obtaining the map.
							if (mMap != null) {
								setUpMap();
							}
						}

					});//getMap();*/

		}
	}

	private static final int REQUEST_CODE_LOCATION = 2;

	private void setUpMap() {
		// Hide the zoom controls as the button panel will cover it.
		mMap.getUiSettings().setZoomControlsEnabled(false);


		if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			// Request missing location permission.
			ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_LOCATION);
		} else {
			// Location permission has been granted, continue as usual.
			mMap.setMyLocationEnabled(true);
		}

		mMap.getUiSettings().setZoomGesturesEnabled(true);

		// Add lots of markers to the map.`
		addMarkersToMap();

		// Setting an info window adapter allows us to change the both the
		// contents and look of the
		// info window.
		// mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter());

		// Set listeners for marker events. See the bottom of this class for
		// their behavior.
		mMap.setOnMarkerClickListener(this);
		mMap.setOnInfoWindowClickListener(this);
		mMap.setOnMarkerDragListener(this);
		mMap.setOnMyLocationButtonClickListener(this);

		// Pan to see all markers in view.
		// Cannot zoom to bounds until the map has a size.
		/*final View mapView = getActivity().getFragmentManager()
				.findFragmentById(R.id.map).getView();
		if (mapView.getViewTreeObserver().isAlive()) {
			mapView.getViewTreeObserver().addOnGlobalLayoutListener(
					new OnGlobalLayoutListener() {
						@SuppressWarnings("deprecation")
						// We use the new method when supported
						@SuppressLint("NewApi")
						// We check which build version we are using.
						@Override
						public void onGlobalLayout() {

							if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
								mapView.getViewTreeObserver()
										.removeGlobalOnLayoutListener(this);
							} else {
								mapView.getViewTreeObserver()
										.removeOnGlobalLayoutListener(this);
							}
							
							*//*mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));
							mMap.moveCamera(CameraUpdateFactory.zoomTo(15.0f));*//*
						}
					});
		}*/

		//java.lang.IllegalStateException: Error using newLatLngBounds(LatLngBounds, int): Map size can't be 0. Most likely, layout has not yet occured for the map view. Either wait until layout has occurred or use newLatLngBounds(LatLngBounds, int, int, int) which allows you to specify the map's dimensions.
		mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
			@Override
			public void onMapLoaded() {
				LatLngBounds bounds = new LatLngBounds.Builder()
						.include(DESTINATION).build();
				mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 5));
				mMap.moveCamera(CameraUpdateFactory.zoomTo(15.0f));
			}
		});
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		switch (requestCode) {
			case REQUEST_CODE_LOCATION: {
				// If request is cancelled, the result arrays are empty.
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
					// permission was granted, yay! Do the contacts-related task you need to do.
					setUpMap();
				 else
					Toast.makeText(getActivity(), "Without this permission app can't show your location", Toast.LENGTH_SHORT).show();

				return;
			}

			// other 'case' lines to check for other
			// permissions this app might request
		}
	}

	private void addMarkersToMap() {

		// A few more markers for good measure.
		mPerth = mMap.addMarker(new MarkerOptions()
				.position(DESTINATION)
				.title(restaurentName)
				.snippet(restaurentAddress)
				.icon(BitmapDescriptorFactory
						.fromResource(R.drawable.ic_map_marker)));



		//18 was a number it was fine to my situation.
		CameraPosition cameraPosition = new CameraPosition.Builder().target(DESTINATION).zoom(18).build();



		mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

		mPerth.showInfoWindow();//the marker comes with balloon already open.

	}

	private boolean checkReady() {
		if (mMap == null) {
			Toast.makeText(getActivity(), R.string.map_not_ready,
					Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}

	/** Called when the Clear button is clicked. */
	public void onClearMap(View view) {
		if (!checkReady()) {
			return;
		}
		mMap.clear();
	}

	/** Called when the Reset button is clicked. */
	public void onResetMap(View view) {
		if (!checkReady()) {
			return;
		}
		// Clear the map because we don't want duplicates of the markers.
		mMap.clear();
		addMarkersToMap();
	}

	/** Called when the Reset button is clicked. */
	public void onToggleFlat(View view) {
		if (!checkReady()) {
			return;
		}
		 /*boolean flat = mFlatBox.isChecked();
		for (Marker marker : mMarkerRainbow) {
			 marker.setFlat(flat);
		}*/
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		if (!checkReady()) {
			return;
		}
		float rotation = seekBar.getProgress();
		for (Marker marker : mMarkerRainbow) {
			marker.setRotation(rotation);
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// Do nothing.
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// Do nothing.
	}

	//
	// Marker related listeners.
	//

	@Override
	public boolean onMarkerClick(final Marker marker) {
		if (marker.equals(mPerth)) {
			// This causes the marker at Perth to bounce into position when it
			// is clicked.
			final Handler handler = new Handler();
			final long start = SystemClock.uptimeMillis();
			final long duration = 1500;

			final Interpolator interpolator = new BounceInterpolator();

			handler.post(new Runnable() {
				@Override
				public void run() {
					long elapsed = SystemClock.uptimeMillis() - start;
					float t = Math.max(
							1 - interpolator.getInterpolation((float) elapsed
									/ duration), 0);
					marker.setAnchor(0.5f, 1.0f + 2 * t);

					if (t > 0.0) {
						// Post again 16ms later.
						handler.postDelayed(this, 16);
					}
				}
			});
		} else if (marker.equals(mAdelaide)) {
			// This causes the marker at Adelaide to change color and alpha.
			marker.setIcon(BitmapDescriptorFactory.defaultMarker(mRandom
					.nextFloat() * 360));
			marker.setAlpha(mRandom.nextFloat());
		}
		// We return false to indicate that we have not consumed the event and
		// that we wish
		// for the default behavior to occur (which is for the camera to move
		// such that the
		// marker is centered and for the marker's info window to open, if it
		// has one).
		return false;
	}

	@Override
	public void onInfoWindowClick(Marker marker) {
		// Toast.makeText(getBaseContext(), "Click Info Window"+marker.getId(),
		// Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onMarkerDragStart(Marker marker) {
		// mTopText.setText("onMarkerDragStart");
	}

	@Override
	public void onMarkerDragEnd(Marker marker) {
		// mTopText.setText("onMarkerDragEnd");
	}

	@Override
	public void onMarkerDrag(Marker marker) {
		// mTopText.setText("onMarkerDrag.  Current Position: " +
		// marker.getPosition());
	}

	@Override
	public boolean onMyLocationButtonClick() {
		// Toast.makeText(this, "MyLocation button clicked",
		// Toast.LENGTH_SHORT).show();

		return false;
	}

	// direction route
	private String getDirectionsUrl(LatLng origin, LatLng dest) {

		// Origin of route
		String str_origin = "origin=" + origin.latitude + ","
				+ origin.longitude;

		// Destination of route
		String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

		// Sensor enabled
		String sensor = "sensor=false";

		// Building the parameters to the web service
		String parameters = str_origin + "&" + str_dest + "&" + sensor;

		// Output format
		String output = "json";

		// Building the url to the web service
		String url = "https://maps.googleapis.com/maps/api/directions/"
				+ output + "?" + parameters;

		return url;
	}

	/** A method to download json data from url */
	private String downloadUrl(String strUrl) throws IOException {
		String data = "";
		InputStream iStream = null;
		HttpURLConnection urlConnection = null;
		try {
			URL url = new URL(strUrl);

			// Creating an http connection to communicate with url
			urlConnection = (HttpURLConnection) url.openConnection();

			// Connecting to url
			urlConnection.connect();

			// Reading data from url
			iStream = urlConnection.getInputStream();

			BufferedReader br = new BufferedReader(new InputStreamReader(
					iStream));

			StringBuffer sb = new StringBuffer();

			String line = "";
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

			data = sb.toString();

			br.close();

		} catch (Exception e) {
			Log.d("Exceptiondownloadingurl", e.toString());
		} finally {
			iStream.close();
			urlConnection.disconnect();
		}
		return data;
	}

	// Fetches data from url passed
	private class DownloadTask extends AsyncTask<String, Void, String> {

		// Downloading data in non-ui thread
		@Override
		protected String doInBackground(String... url) {

			// For storing data from web service
			String data = "";

			try {
				// Fetching the data from web service
				data = downloadUrl(url[0]);
			} catch (Exception e) {
				Log.d("Background Task", e.toString());
			}
			return data;
		}

		// Executes in UI thread, after the execution of
		// doInBackground()
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			ParserTask parserTask = new ParserTask();

			// Invokes the thread for parsing the JSON data
			parserTask.execute(result);

		}
	}

	/** A class to parse the Google Places in JSON format */
	private class ParserTask extends
			AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

		// Parsing the data in non-ui thread
		@Override
		protected List<List<HashMap<String, String>>> doInBackground(
				String... jsonData) {

			JSONObject jObject;
			List<List<HashMap<String, String>>> routes = null;

			try {
				jObject = new JSONObject(jsonData[0]);
				DirectionsJSONParser parser = new DirectionsJSONParser();

				// Starts parsing data
				routes = parser.parse(jObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return routes;
		}

		// Executes in UI thread, after the parsing process
		@Override
		protected void onPostExecute(List<List<HashMap<String, String>>> result) {
			ArrayList<LatLng> points = null;
			PolylineOptions lineOptions = null;
			//MarkerOptions markerOptions = new MarkerOptions();
			
			if(result!=null){
				// Traversing through all the routes
				for (int i = 0; i < result.size(); i++) {
					points = new ArrayList<LatLng>();
					lineOptions = new PolylineOptions();

					// Fetching i-th route
					List<HashMap<String, String>> path = result.get(i);

					// Fetching all the points in i-th route
					for (int j = 0; j < path.size(); j++) {
						HashMap<String, String> point = path.get(j);

						double lat = Double.parseDouble(point.get("lat"));
						double lng = Double.parseDouble(point.get("lng"));
						LatLng position = new LatLng(lat, lng);

						points.add(position);
					}

					// Adding all the points in the route to LineOptions
					lineOptions.addAll(points);
					lineOptions.width(10);
					lineOptions.color(Color.parseColor("#C92027"));

				}
			
			
				// Drawing polyline in the Google Map for route
				if(mMap!=null && lineOptions!=null)
				mMap.addPolyline(lineOptions);
			}
		}
	}

}
