package com.mcc.letseat.tab;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Dialog;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.Review;
import com.foody.nearby.AlertDialogManager;
import com.foody.nearby.GPSTracker;
import com.foody.nearby.MyListAdapterReview;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mcc.letseat.MyProgressDialog;
import com.mcc.letseat.R;
import com.mcc.libs.AnimationTween;
import com.mcc.user.SavedPrefernce;

/**
 * ReviewFragment show all reviews posted in a restaurant by app user 
 * @author Arif
 *
 */

public class ReviewFragment extends Fragment {

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Google Places
	FoodyResturent foodyResturent;

	// Places List
	ArrayList<Review> reviewList;

	// GPS Location
	GPSTracker gps;

	// Button
	ImageView btnShowOnMap;

	// Progress dialog
	MyProgressDialog pDialog;

	// Places Listview
	ListView lv;

	// ListItems data
	ArrayList<HashMap<String, String>> reviewListItems = new ArrayList<HashMap<String, String>>();

	// //admob
	AdView adView = null;

	ImageView searchAny;

	TextView restaurantName;

	String restaurantId;

	// review
	Dialog rankDialog;
	RatingBar ratingBar;
	EditText etWtiteComment;

	String userid, comment, rating;

	View currentView = null;
	
	boolean noReview=false;
	
	LoadRewiews loadRewiews;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment

		setRetainInstance(true);

		/*
		 * restaurantId = getArguments().getString("restaurantId");
		 * Log.e("restaurantId", restaurantId);
		 * currentView=inflater.inflate(R.layout.restaurant_review, container,
		 * false);
		 * 
		 * return currentView;
		 */

		if (currentView != null) {
			ViewGroup parent = (ViewGroup) currentView.getParent();
			if (parent != null)
				parent.removeView(currentView);
		}

		try {
			restaurantId = getArguments().getString("restaurantId");
			currentView = inflater.inflate(R.layout.restaurant_review,
					container, false);
		} catch (InflateException e) {
			/* map is already there, just return view as it is */
		}

		return currentView;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// enable action bar home button from fragment
		setHasOptionsMenu(true);
		getActivity().getActionBar().setHomeButtonEnabled(true);

		getActivity().getWindow().getDecorView()
				.findViewById(android.R.id.content)
				.setBackgroundResource(R.mipmap.home_back);
	}

	@Override
	public void onStart() {
		super.onStart();

		/*
		 * // action bar hide title change icon
		 * getActivity().getActionBar().setIcon(R.drawable.ic_title);
		 * getActivity().getActionBar().setDisplayShowTitleEnabled(false);
		 * 
		 * //hide or show back icon on action bar
		 * getActivity().getActionBar().setDisplayHomeAsUpEnabled(false);
		 * 
		 * //enable app icon as home button
		 * getActivity().getActionBar().setHomeButtonEnabled(true);
		 * 
		 * //Set whether to include the application home affordance in the
		 * action bar. Home is presented as either an activity icon or logo.
		 * //getActionBar().setDisplayShogetActivity().getActionBar()rue);
		 */
		// Getting listview
		lv = (ListView) currentView.findViewById(R.id.list);

		restaurantName = (TextView) currentView.findViewById(R.id.restName);
		restaurantName.setVisibility(View.GONE);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				/*
				 * Restaurent resturent = (Restaurent)
				 * resturentList.get(position); //restaurentId= resturent.Id;
				 * 
				 * Intent in = new
				 * Intent(getApplicationContext(),ResturentActivity.class);
				 * in.putExtra( "Id" , resturentList.get(position).Id);
				 * 
				 * startActivity(in);
				 */

			}
		});

		adView = (AdView) this.currentView.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder().addTestDevice(
				AdRequest.DEVICE_ID_EMULATOR).build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});

		// Toast.makeText(ResturentListView.this, "hello"+searchKeywords,
		// Toast.LENGTH_LONG).show();

		// restaurentId=getActivity().getIntent().getStringExtra("restaurentId");
		restaurantName.setText(getActivity().getIntent().getStringExtra(
				"restaurentName"));

		/*
		 * if (!flagLoaded) { flagLoaded=true; //Toast.makeText(getActivity(),
		 * userid+restaurantName.getText(), Toast.LENGTH_LONG).show(); new
		 * LoadRewiews().execute(); }
		 */

		loadRewiews=new LoadRewiews();
		loadRewiews.execute();
		// handleIntent(getIntent());
		

	}
	
	
    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
        	Log.e("currentPAgeOnViewPager", ""+TabMainActivity.currentPAgeOnViewPager);
    		//clla review dailog
    		if(noReview && TabMainActivity.currentPAgeOnViewPager==3){
    			callReviewDailog();
    		}
        }
    }
    

	/*
	 * // action bar menu and event
	 * 
	 * @Override public boolean onCreateOptionsMenu(Menu menu) { // TODO
	 * Auto-generated method stub
	 * 
	 * MenuInflater inflater = getActivity().getMenuInflater();
	 * inflater.inflate(R.menu.activity_main, menu); // Associate searchable
	 * configuration with the SearchView SearchManager searchManager =
	 * (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
	 * SearchView searchView = (SearchView) menu.findItem(R.id.action_search)
	 * .getActionView(); searchView.setSearchableInfo(searchManager
	 * .getSearchableInfo(getActivity().getComponentName()));
	 * 
	 * return super.getActivity().onCreateOptionsMenu(menu); }
	 */

	void callReviewDailog() {
		rankDialog = new Dialog(getActivity(), R.style.FullHeightDialog);

		rankDialog.setContentView(R.layout.review_dialog);
		rankDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT);

		// open softkeyboard
		rankDialog.getWindow().setSoftInputMode(android.view.WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

		/*
		 * DisplayMetrics metrics = getResources().getDisplayMetrics(); int
		 * screenWidth = (int) (metrics.widthPixels *1);
		 * rankDialog.setContentView(R.layout.rank_dialog);
		 * rankDialog.getWindow().setLayout(screenWidth,
		 * LayoutParams.WRAP_CONTENT);
		 */

		rankDialog.setCancelable(true);

		ratingBar = (RatingBar) rankDialog.findViewById(R.id.dialog_ratingbar);
		ratingBar.setRating(0);

		etWtiteComment = (EditText) rankDialog.findViewById(R.id.etWtiteComment);
		// text.setText(txtRestName.getText());

		// change rating bar color
		// RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingBar);
		// LayerDrawable stars = (LayerDrawable)
		// ratingBar.getProgressDrawable();
		// stars.getDrawable(2).setColorFilter(Color.YELLOW, Mode.SRC_ATOP);

		View.OnClickListener updateListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String ratingConvertedToDB = ""
						+ (ratingBar.getRating() * StaticObjects.RATING_OFFSET);

				rating = ratingConvertedToDB;
				comment = etWtiteComment.getText().toString();
				
				if (new SavedPrefernce(getActivity()).isUserExists()) {
					userid = new SavedPrefernce(getActivity()).getUserID();
					rankDialog.dismiss();
					new PostReview().execute();
					
				} else {
					StaticObjects.callRegForAction(getActivity());
				}

			}
		};

		View.OnClickListener cancelListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				rankDialog.cancel();
			}
		};

		ImageView cancelButton = (ImageView) rankDialog.findViewById(R.id.rank_dialog_cancel_button);
		cancelButton.setOnClickListener(cancelListener);

		LinearLayout layRankDailogConfirm = (LinearLayout) rankDialog.findViewById(R.id.layRankDailogConfirm);
		layRankDailogConfirm.setOnClickListener(updateListener);

		ImageView btnRankDailogConfirm = (ImageView) rankDialog.findViewById(R.id.btnRankDailogConfirm);
		btnRankDailogConfirm.setOnClickListener(updateListener);

		// now that the dialog is set up, it's time to show it
		rankDialog.show();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
		case R.id.action_search:
			// search action
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	@Override
	public void onPause() {
		super.onPause();

		if (adView != null)
			adView.pause();

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		if (adView != null)
			adView.destroy();
		super.onDestroy();
		
		if(loadRewiews != null && loadRewiews.getStatus() == Status.RUNNING) {
			loadRewiews.cancel(true);
			}
	}

	class LoadRewiews extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (currentView != null)
				currentView.findViewById(R.id.menu_custom_progress)
						.setVisibility(View.VISIBLE);
		}

		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			SavedPrefernce prefernce = new SavedPrefernce(getActivity());

			try {

				String userid = prefernce.getUserID();
				System.out.println("===============userid==================="
						+ userid);
				String postData = "";
				if (userid.equals("NotFound")) {
					postData = "resid=" + restaurantId;
				} else {
					postData = "resid=" + restaurantId + "&userid=" + userid;
				}

				reviewList = foodyResturent.getReviewList(postData,FoodyResturent.TYPE_RES);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
			protected void onPostExecute(String file_url) {
					// dismiss the dialog after getting all products
					// pDialog.dismiss();
				if(!isCancelled()){
					if ( currentView != null) {
						// currentView.findViewById(R.id.menu_custom_progress).setVisibility(View.GONE);
		
						AnimationTween.animateView( currentView.findViewById(R.id.menu_custom_progress),getActivity() );
						AnimationTween.animateViewPosition(lv, getActivity());
					}
					
					if(getActivity()==null)
						return;
					// updating UI from Background Thread
					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							/**
							 * Updating parsed Places into LISTVIEW
							 * */
							// Get json response status
		
							if (reviewList == null && currentView != null) {
		
								currentView.findViewById(R.id.menu_custom_progress).setVisibility(View.VISIBLE);
								currentView.findViewById(R.id.menu_custom_progress).findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
								TextView textPreparing = (TextView) currentView.findViewById(R.id.menu_custom_progress).findViewById(R.id.textPreparing);
								textPreparing.setText("No review found.");
								
								AnimationTween.animateViewPosition( currentView.findViewById(R.id.menu_custom_progress),getActivity() );
		
							} else if (reviewList.size() <= 0 && currentView != null) {
								currentView.findViewById(R.id.menu_custom_progress).setVisibility(View.VISIBLE);
								currentView.findViewById(R.id.menu_custom_progress).findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
								TextView textPreparing = (TextView) currentView.findViewById(R.id.menu_custom_progress).findViewById(R.id.textPreparing);
								textPreparing.setText("No review found. be the first! Tap  ADD REVIEW");
								currentView.findViewById(R.id.menu_custom_progress).setOnClickListener(new OnClickListener() {
		
											@Override
											public void onClick(View v) {
												
												SavedPrefernce savedPrefernce=new SavedPrefernce(getActivity());
												if (savedPrefernce.isUserExists()) {
													callReviewDailog();											
		
												} else {
													StaticObjects.callRegForAction( getActivity() );
													
												}
		
											}
										});
		
								RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
		
								p.setMargins(20, 0, 20, 0);
								p.addRule(RelativeLayout.BELOW,R.id.menu_custom_progress);
								Button button = (Button) currentView.findViewById(R.id.btnAddReview);
								button.setVisibility(View.VISIBLE);
								button.setLayoutParams(p);
								
								
								
								button.setOnClickListener(new OnClickListener() {
		
									@Override
									public void onClick(View v) {
										SavedPrefernce savedPrefernce=new SavedPrefernce(getActivity());
										if (savedPrefernce.isUserExists()) {
											
											callReviewDailog();
		
										} else {
											StaticObjects.callRegForAction( getActivity());
											
										}
									}
								});
								
								AnimationTween.animateViewPosition( currentView.findViewById(R.id.menu_custom_progress),getActivity() );
								
								noReview=true;
								
								
		
							} else if (reviewList != null) {
								// Check for all possible status
		
								// Successfully got places details
								if (reviewList != null) {
									noReview=false;
									// loop through each place
									reviewListItems = new ArrayList<HashMap<String, String>>();
									for (Review p : reviewList) {
		
										HashMap<String, String> map = new HashMap<String, String>();
										map.put("ID", p.ID);
										map.put("USERID", p.USERID);
										map.put("FULLNAME", p.FULLNAME);
										map.put("metaData", p.NO_OF_REVIEW
												+ " review  " + p.NO_OF_FOLLOWERS + " "
												+ "followers");
										map.put("REVIEW", p.REVIEW);
										map.put("DATE", p.DATE_DIFF);
		
										map.put("RATING", " " + p.RATING + " ");
										map.put("NO_OF_COMMENTS", " "
												+ p.NO_OF_COMMENTS + " ");
		
										map.put("NO_OF_REVIEW_LIKE",
												p.NO_OF_REVIEW_LIKE);
		
										map.put("IMG", p.IMG);
										map.put("FOLLOW", p.FOLLOW);
		
										// map.put("RATING", p.RATING);
		
										// adding HashMap to ArrayList
										reviewListItems.add(map);
									}
									// list adapter
									MyListAdapterReview adapter = new MyListAdapterReview(
											getActivity(), reviewListItems,
											new String[] { "FULLNAME", "metaData",
													"REVIEW", "DATE", "RATING",
													"NO_OF_COMMENTS",
													"NO_OF_REVIEW_LIKE", "IMG",
													"FOLLOW" });
		
									Button button = (Button) currentView.findViewById(R.id.btnAddReview);
									button.setVisibility(View.VISIBLE);
		
									button.setOnClickListener(new OnClickListener() {
		
										@Override
										public void onClick(View v) {
											SavedPrefernce savedPrefernce=new SavedPrefernce(getActivity());
											if (savedPrefernce.isUserExists()) {
												
												callReviewDailog();
		
											} else {
												StaticObjects.callRegForAction( getActivity());
												
											}
										}
									});
		
									lv.setAdapter(adapter);
								} else if (reviewList == null) {
									// Zero results found
									currentView.findViewById(R.id.menu_custom_progress).setVisibility(View.VISIBLE);
									currentView.findViewById(R.id.menu_custom_progress).findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
									TextView textPreparing = (TextView) currentView.findViewById(R.id.menu_custom_progress).findViewById(R.id.textPreparing);
									textPreparing.setText("No review found.");
								}
							}
						}
		
					});
		
				}
			}

	}

	String response;

	class PostReview extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new MyProgressDialog(getActivity());
			// pDialog.setMessage(Html.fromHtml("<b>Loading ...</b>"));
			// pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		protected String doInBackground(String... args) {

			foodyResturent = new FoodyResturent();

			try {
				response = foodyResturent.insertReview(userid, comment,restaurantId, rating);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			// updating UI from Background Thread
			getActivity().runOnUiThread(new Runnable() {

				public void run() {

					if (response.equals("success")) {
						Toast.makeText(getActivity(), "Post Successful.",
								Toast.LENGTH_SHORT).show();
						
						loadRewiews=new LoadRewiews();
						loadRewiews.execute();
						
					} else {
						Toast.makeText(getActivity(), "Try again.",
								Toast.LENGTH_SHORT).show();
					}
				}

			});

		}

	}

}
