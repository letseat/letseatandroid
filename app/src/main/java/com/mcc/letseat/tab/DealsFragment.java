package com.mcc.letseat.tab;

import java.util.ArrayList;
import java.util.HashMap;

import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.foody.jsondata.Card;
import com.foody.jsondata.ResturentDetails;
import com.foody.nearby.AlertDialogManager;
import com.foody.nearby.MyListAdapterDealsBooking;
import com.foody.nearby.MyListAdapterTwo;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mcc.letseat.R;
import com.mcc.libs.AnimationTween;

/**
 * DealsFragment provide available deals in a particular restaurant that hosted in TabMainActivity 
 * @author Arif
 *
 */
public class DealsFragment extends Fragment {

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Places Listview
	ListView lv;

	// //admob
	AdView adView = null;

	View currentView = null;

	LoadDeals asyncTask;

	// ResturentDetails List
	ArrayList<ResturentDetails> resturentList;

	TextView txtTitleNearBy;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		/*
		 * // Inflate the layout for this fragment restaurentId =
		 * getArguments().getString("restaurantId"); Log.e("restaurentId",
		 * restaurentId); currentView =
		 * inflater.inflate(R.layout.restaurant_menu, container,false); return
		 * currentView;
		 */

		setRetainInstance(true);

		if (currentView != null) {
			ViewGroup parent = (ViewGroup) currentView.getParent();
			if (parent != null)
				parent.removeView(currentView);
		}

		try {

			currentView = inflater.inflate(R.layout.n_activity_main, container,
					false);
		} catch (InflateException e) {
			/* map is already there, just return view as it is */
		}

		return currentView;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// enable action bar home button from fragment
		setHasOptionsMenu(true);
		getActivity().getActionBar().setHomeButtonEnabled(true);

		getActivity().getWindow().getDecorView()
				.findViewById(android.R.id.content)
				.setBackgroundResource(R.mipmap.home_back);

	}

	@Override
	public void onStart() {
		super.onStart();

		lv = (ListView) currentView.findViewById(R.id.list);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

			}
		});
		
		
		txtTitleNearBy=(TextView)currentView.findViewById(R.id.txtTitleNearBy);
		txtTitleNearBy.setText("");
		currentView.findViewById(R.id.ll_search_hint).setVisibility(View.GONE);

		adView = (AdView) this.currentView.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder().addTestDevice(
				AdRequest.DEVICE_ID_EMULATOR).build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});

		// configure the fragment instance to be retained on configuration
		// change
		asyncTask = new LoadDeals(this);
		asyncTask.execute();

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
		case R.id.action_search:
			// search action
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	@Override
	public void onPause() {
		super.onPause();

		if (adView != null)
			adView.pause();

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		if (adView != null)
			adView.destroy();
		super.onDestroy();
		
		if(asyncTask != null && asyncTask.getStatus() == Status.RUNNING) {
			asyncTask.cancel(true);
			}
	}

	private class LoadDeals extends AsyncTask<String, String, String> {

		private LoadDeals(DealsFragment fragment) {

		}

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			if (currentView != null)
				currentView.findViewById(R.id.main_custom_progress)
						.setVisibility(View.VISIBLE);

		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			resturentList = new ArrayList<ResturentDetails>();
			resturentList.add(0, (ResturentDetails) getArguments()
					.getSerializable("ResturentDetails"));
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
				super.onPostExecute(file_url);
				
			if(!isCancelled()){
				
				// dismiss the dialog after getting all products
				if (currentView != null) {
					// currentView.findViewById(R.id.menu_custom_progress).setVisibility(View.GONE);
	
					AnimationTween.animateView(currentView.findViewById(R.id.main_custom_progress),getActivity());
					lv.setVisibility(View.VISIBLE);
				}
	
				if (getActivity() == null)
					return;
				// updating UI from Background Thread
				getActivity().runOnUiThread(new Runnable() {
					public void run() {
						
						ArrayList<HashMap<String, String>> aa = new ArrayList<HashMap<String, String>>();
						if (resturentList != null && resturentList.get(0).Cards.size()>0){
							ResturentDetails p = resturentList.get(0);
							
							
								for (Card card:p.Cards) {
									HashMap<String, String> map = new HashMap<String, String>();
									map.put("dealsId", card.DisId);
									map.put("IsLetsEat", card.IsLetsEat);
									map.put("deals", card.DisName);
									
									if(card.DisDetail.equals("") || card.DisDetail.isEmpty()){
										map.put("details", card.DisPercent+" "+card.DisName+" from "+card.DisCompany);
									}else{
										map.put("details", card.DisDetail);
									}
									map.put("BookUrl", card.DealsBookUrl);
									
									aa.add(map);
								}
								
								currentView.findViewById(R.id.ll_search_hint).setVisibility(View.VISIBLE);
								txtTitleNearBy.setText("Deals Available:");
								
								lv.setVisibility(View.VISIBLE);
								MyListAdapterDealsBooking adapter=new MyListAdapterDealsBooking(getActivity(), aa, new String[]{"deals","details", "IsLetsEat", "dealsId", "BookUrl"}, p.Id);
								lv.setAdapter(adapter);
								adapter.notifyDataSetChanged();
							
							
							
						}else{
							currentView.findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);
							View v = currentView.findViewById(R.id.main_custom_progress);
							TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
							textPreparing.setText("No deals available in this restaurant. Find others deals in your location from  Menu>Deals");
							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
							AnimationTween.animateViewPosition(currentView.findViewById(R.id.main_custom_progress), getActivity());
						}
	
						
	
					}
	
				});
	
			}
		}

	}

}
