package com.mcc.letseat.tab;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.LayoutParams;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.foody.AppData.StaticObjects;
import com.foody.jsondata.LetsEatRequest;
import com.foody.jsondata.ResturentDetails;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.mcc.letseat.HomeT;
import com.mcc.letseat.R;
import com.mcc.libs.FragmentActivityWithSliding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import info.androidhive.tabsswipe.adapter.TabsPagerAdapter;

/**
 * Restaurant Details contain 6 tab "Info", "Deals" , "Menu", "Map", "Photo", "Review"
 * @author Arif
 *
 */
public class TabMainOpenSharedLinkActivity extends FragmentActivityWithSliding implements
		ActionBar.TabListener {

	private String ANA_TAG="Restaurant Details";
	
	public static ViewPager viewPager;
	 
	private TabsPagerAdapter mAdapter;
	private ActionBar actionBar;
	// Tab titles
	 String[] tabs = { "Info", "Deals" , "Menu", "Map", "Photo", "Review"};
	 //Map Tab position in tab list according to tabs array  
	 public static int mapTabPosition=3, dealsTabPosition=1, menuTabPosition = 2;

	String restaurantId;
	
	// ResturentDetails List
	ArrayList<ResturentDetails> resturentList;
	
	public static int currentPAgeOnViewPager; 
	
	//LoadResturentDetails loadResturentDetails;
	
	//AdView adView;
	private InterstitialAd interstitial;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.tab_activity_main);
		
		

		/*// action bar hide title change icon
		getActionBar().setIcon(R.drawable.ic_home_drawer);
		getActionBar().setDisplayShowTitleEnabled(false);
		// hide or show back icon on action bar
		getActionBar().setDisplayHomeAsUpEnabled(false);
		// enable app icon as home button
		getActionBar().setHomeButtonEnabled(true);
		// Set whether to include the application home affordance in the action
		// bar. Home is presented as either an activity icon or logo.
		//getActionBar().setDisplayShowHomeEnabled(true);
*/		
		
		//set home button
		setHomeButton(this);
		
		//create Interstitial ad 
		//createInterstitial();
		
		if(savedInstanceState==null){
			
			//restaurantId = getIntent().getStringExtra("Id");
			onNewIntent(getIntent());


		}

	}


	protected void onNewIntent(Intent intent) {
		String action = intent.getAction();
		String data = intent.getDataString();
		if (Intent.ACTION_VIEW.equals(action) && data != null) {
			 restaurantId = data.substring(data.lastIndexOf("=") + 1);


			if(restaurantId!=null && !restaurantId.isEmpty() ){
				loadData();
			}else{
				Toast.makeText(this,"Can't open the link", Toast.LENGTH_LONG).show();
				Intent i = new Intent(TabMainOpenSharedLinkActivity.this, HomeT.class);
				startActivity(i);
				finish();
			}
		}
	}
	
	
	private void loadData(){
		StringRequest postRestDetailsRequest = new StringRequest(Request.Method.POST, LetsEatRequest.RESTAURENT_DETAILS_URL,
		        new Response.Listener<String>() {
		            @Override
		            public void onResponse(String response) {
		                try {
		                	// Result handling 
		                	resturentList = new LetsEatRequest().getResturentDetails(response);
		                	
		        			
		        			// dismiss the dialog after getting all products
		        			//pDialog.dismiss();
		        			
		        			findViewById(R.id.custom_progress_tab_activity).setVisibility(View.GONE);
		        				
		        				//AnimationTween.animateView(findViewById(R.id.grid_view_custom_progress),TabMainActivity.this);
		        				//AnimationTween.animateViewPosition(gridView, TabMainActivity.this);
		        				
		        			
		        			// updating UI from Background Thread
		        			runOnUiThread(new Runnable() {
		        				public void run() {
		        					/**
		        					 * Updating parsed Places into LISTVIEW
		        					 * */
		        					// Get json response status

		        					if (resturentList == null || resturentList.size() <= 0) {

		        					
		        						 
		        					} else if (resturentList != null) {
		        						
		        						initializePager(resturentList.get(0));
		        						
		        						//Load Interstitial ad
		        						//loadInterstitial();
		        					}
		        				}

		        			});
		        		
		    		    	
		                } catch (Exception e) {
		                	
		                }
		            }
		        },
		        new Response.ErrorListener() {
		            @Override
		            public void onErrorResponse(VolleyError error) {
		                error.printStackTrace();
		            }
		        }
		) {
		    @Override
		    protected Map<String, String> getParams()
		    {
		        Map<String, String>  params = new HashMap<String, String>();
		        // the POST parameters:
		        params.put("ResId", restaurantId);
		        return params;
		    }
		};
		
		 showLoader();
		Volley.newRequestQueue(this).add(postRestDetailsRequest);
	}
	
	private void showLoader(){
		findViewById(R.id.custom_progress_tab_activity).setVisibility(View.VISIBLE);
	}
	
	public void createInterstitial(){
		// Create the interstitial.
	    interstitial = new InterstitialAd(this);
	    interstitial.setAdUnitId(getString(R.string.adUnitInterstitial));
	}
	
	
	public void loadInterstitial(){
		 // Create ad request.
	    AdRequest adRequest = new AdRequest.Builder().build();

	    // Begin loading your interstitial.
	    interstitial.loadAd(adRequest);
	    
	    interstitial.setAdListener(new AdListener(){
	          public void onAdLoaded(){
	               displayInterstitial();
	          }
	          
	          public void onAdFailedToLoad(int errorcode) {
	        	  	//Toast.makeText(HomeT.this, "Error:" + errorcode, Toast.LENGTH_SHORT).show();
					System.out.println("Error:" + errorcode);
				}
	          
	    });
	}
	
	// Invoke displayInterstitial() when you are ready to display an interstitial.
	  public void displayInterstitial() {
		 // Toast.makeText(this, ""+interstitial.isLoaded(), Toast.LENGTH_SHORT).show();
	    if (interstitial.isLoaded()) {
	      interstitial.show();
	      
	      
	    }
	  }
	
	
	void initializePager(ResturentDetails resturentDetails){

		// Initilization
		viewPager = (ViewPager) findViewById(R.id.pager);
		actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(true);

		final Bundle bundle = new Bundle();
		bundle.putString("restaurantId", restaurantId);
		bundle.putString("latitude", resturentDetails.latitude);//getIntent().getStringExtra("latitude"));
		bundle.putString("longitude", resturentDetails.longitude);//getIntent().getStringExtra("longitude"));
		bundle.putString("RestName", resturentDetails.Name);//getIntent().getStringExtra("RestName"));
		bundle.putString("address", resturentDetails.Address);//getIntent().getStringExtra("address"));
		bundle.putSerializable("ResturentDetails", resturentDetails);
		

		mAdapter = new TabsPagerAdapter(getSupportFragmentManager(), bundle, tabs);

		viewPager.setAdapter(mAdapter);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		actionBar.setStackedBackgroundDrawable(getResources().getDrawable(
				R.drawable.myselector));

		// Adding Tabs
		/*for (String tab_name : tabs) {
			actionBar.addTab(actionBar.newTab().setText(tab_name)
					.setTabListener(this));
			
		}*/
		
		for (String tab_name : tabs) {
            TextView t = new TextView(this);
            t.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);    
            t.setLayoutParams(new ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            t.setTextColor(Color.WHITE);
            t.setText(tab_name );
            t.setTypeface(StaticObjects.gettfNormal(this));

            actionBar.addTab(actionBar.newTab()
                    .setCustomView(t)
                    .setTabListener(this));

        }

		/**
		 * on swiping the viewpager make respective tab selected
		 * */
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				// on changing the page
				// make respected tab selected
				actionBar.setSelectedNavigationItem(position);
				currentPAgeOnViewPager=position;
				analyticsSendScreenView(ANA_TAG+":"+restaurantId+" tab>"+tabs[currentPAgeOnViewPager]);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
		
		actionBar.setSelectedNavigationItem(1);
		viewPager.setCurrentItem(1);
		currentPAgeOnViewPager=1;
		
		actionBar.setSelectedNavigationItem(0);
		viewPager.setCurrentItem(0);
		currentPAgeOnViewPager=0;
		
		//show app home button
		homeButtonshow();

		/*actionBar.setSelectedNavigationItem(0);
		viewPager.setCurrentItem(0);*/
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// on tab selected
		// show respected fragment view
		if(viewPager == null){return;}
		viewPager.setCurrentItem(tab.getPosition());
		currentPAgeOnViewPager=tab.getPosition();
		analyticsSendScreenView(ANA_TAG+":"+restaurantId+" tab>"+tabs[currentPAgeOnViewPager]);	
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
	}

	@Override
	protected void onPause() {
		super.onPause();

	}
	
	@Override
	protected void onStart() {		
		super.onStart();
		
		//if(restaurantId!=null)
		analyticsSendScreenView(ANA_TAG+":"+restaurantId);		
		
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
		
		case android.R.id.home:
			 
			super.mSlideHolder.toggle(true);
			
			return true;	
		
		case R.id.action_search:
			RelativeLayout relativeLayout =(RelativeLayout)findViewById(R.id.tab_activity_root_layout);
			addSearchLayout(relativeLayout);			
			return true;
		
		
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	
	/*public boolean onOptionsItemSelected(MenuItem item) {

		  switch (item.getItemId()) {
		   case android.R.id.home:
		     FragmentManager fm= getSupportFragmentManager();
		     if(fm.getBackStackEntryCount()>0){
		          fm.popBackStack();
		        }
		       break;
		    default:
		        break;
		    }
		}*/

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Toast.makeText(this, "hello tab", Toast.LENGTH_LONG).show();
			return false;
		case R.id.action_search:
			// search action
			return false;
		case R.id.action_nearby:

			// startActivity(i);
			return false;
		default:
			break;
		}

		return false;
	}*/
	
	
	
	/*class LoadResturentDetails extends AsyncTask<String, String, String> {

		*//**
		 * Before starting background thread Show Progress Dialog
		 * *//*
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			findViewById(R.id.custom_progress_tab_activity).setVisibility(View.VISIBLE);
		}

		*//**
		 * getting Places JSON
		 * *//*
		protected String doInBackground(String... args) {
			// loading restaurant details
			FoodyResturent foodyResturent = new FoodyResturent();

			try {
				
				String ResId = "ResId=" + restaurantId;

				resturentList = foodyResturent.getResturentDetails(ResId);


				

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		*//**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **//*
		protected void onPostExecute(String file_url) {
			
			if (!isCancelled()) {			
			// dismiss the dialog after getting all products
			//pDialog.dismiss();
			
			findViewById(R.id.custom_progress_tab_activity).setVisibility(View.GONE);
				
				//AnimationTween.animateView(findViewById(R.id.grid_view_custom_progress),TabMainActivity.this);
				//AnimationTween.animateViewPosition(gridView, TabMainActivity.this);
				
			
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					*//**
					 * Updating parsed Places into LISTVIEW
					 * *//*
					// Get json response status

					if (resturentList == null || resturentList.size() <= 0) {

					
						 
					} else if (resturentList != null) {
						
						initializePager(resturentList.get(0));
						
						//Load Interstitial ad
						//loadInterstitial();
					}
				}

			});
		}

		}
		
		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
			Log.e("Cancel Task", "Cancel Task");
		}

	}
	*/
	

}
