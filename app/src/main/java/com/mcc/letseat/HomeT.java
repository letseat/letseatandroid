package com.mcc.letseat;

import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.foody.AppData.StaticObjects;
import com.foody.jsondata.Cusine;
import com.foody.jsondata.Event;
import com.foody.jsondata.LetsEatRequest;
import com.foody.jsondata.MenuMainCategory;
import com.foody.jsondata.Restaurent;
import com.foody.jsondata.SearchFilter;
import com.foody.jsondata.SearchResturent;
import com.foody.jsondata.user.User;
import com.foody.nearby.FeaturedRestaurantListView;
import com.foody.nearby.GPSTracker;
import com.foody.nearby.MyListAdapterEvents;
import com.foody.nearby.NearByNativeRestaurantList;
import com.foody.nearby.NearByNativeRestaurantListNew;
import com.foody.nearby.OfferActivity;
import com.foody.nearby.ResturentListViewImage;
import com.foody.nearby.SearchAdvanceResturentListView;
import com.foody.nearby.SearchResturentListView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.mcc.letseat.Ad.FullscreenAdActivity;
import com.mcc.letseat.Ad.FullscreenAdActivityImage;
import com.mcc.letseat.imageslider.adapter.GridViewCuisineAdapte;
import com.mcc.letseat.imageslider.helper.AppConstant;
import com.mcc.letseat.imageslider.helper.Utils;
import com.mcc.letseat.quary.ExpandableActivity;
import com.mcc.letseat.quary.ScreenSlideActivity;
import com.mcc.letseat.search.DBOperation;
import com.mcc.letseat.search.SearchSuggestionDBHelper;
import com.mcc.letseat.search.ValueAdapter;
import com.mcc.letseat.tab.TabMainActivity;
import com.mcc.libs.ActivityWithSliding;
import com.mcc.libs.AnimationTween;
import com.mcc.libs.ExpandableHeightGridView;
import com.mcc.libs.GravityCompoundDrawable;
import com.mcc.libs.TouchEffectListener;
import com.mcc.pager.ImageSlideAdapter;
import com.mcc.user.SavedPrefernce;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.angmarch.circledpicker.CircledPicker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import com.facebook.appevents.AppEventsLogger;

/**
 * app home page
 *
 * @author Arif
 */
public class HomeT extends ActivityWithSliding implements OnClickListener {

    //AdView adView;
    private InterstitialAd interstitial;
    AdView adView;


    public static final int REQUEST_CODE_CHOOSE_LOCATION = 9;
    private GridViewCuisineAdapte adapter;
    private ExpandableHeightGridView gridView;
    private int columnWidth;

    private Utils utils;

    ArrayList<Cusine> cusineList;

    // ListItems data
    ArrayList<HashMap<String, String>> cusineListItems = new ArrayList<HashMap<String, String>>();

    ViewPager view_pager;

    LinearLayout ll_restaurant, ll_nearby, ll_offer;
    RelativeLayout ll_loc;

    ImageView searchAny;
    //MyEditText txtSearchHome;
    TextView txtSearchHome;

    TextView textViewLocation, textViewFeatureRes;

    Typeface tf, tfNormal;

	/*// flag for Internet connection status
    Boolean isInternetPresent = false;

	// Connection detector class
	ConnectionDetector cd;*/

    private final int LOCATION_ACCESS_SETTING_REQUEST=1;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);

        // action bar hide title change icon
        getActionBar().setIcon(R.mipmap.ic_home_drawer);
        getActionBar().setDisplayShowTitleEnabled(false);

        // hide or show back icon on action bar
        getActionBar().setDisplayHomeAsUpEnabled(false);

        // enable app icon as home button
        getActionBar().setHomeButtonEnabled(true);


        tf = Typeface.createFromAsset(getAssets(), StaticObjects.fontPath_SEGOEUI_light);
        tfNormal = Typeface.createFromAsset(getAssets(), StaticObjects.fontPath_SEGOEUI);
        ((TextView) findViewById(R.id.textView_restaurant)).setTypeface(tf);
        ((TextView) findViewById(R.id.textView_offer)).setTypeface(tf);
        ((TextView) findViewById(R.id.textView_nearby)).setTypeface(tf);
        ((TextView) findViewById(R.id.textView_cuisine)).setTypeface(tf);

        gridView = (ExpandableHeightGridView) findViewById(R.id.grid_view_cuisine);
        //as normal gridview cut off when expanded use custom gridview has Expanded feature. call setExpanded(true)
        gridView.setExpanded(true);
        /*gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				displayInterstitial();
			}
		});*/

        view_pager = (ViewPager) findViewById(R.id.view_pager);


        ll_restaurant = (LinearLayout) findViewById(R.id.ll_restaurant);
        ll_nearby = (LinearLayout) findViewById(R.id.ll_nearby);
        ll_offer = (LinearLayout) findViewById(R.id.ll_offer);
        ll_loc = (RelativeLayout) findViewById(R.id.ll_loc);
        searchAny = (ImageView) findViewById(R.id.imgDetectCity);

        ll_restaurant.setOnClickListener(this);
        ll_offer.setOnClickListener(this);
        ll_nearby.setOnClickListener(this);
        ll_loc.setOnClickListener(this);
        searchAny.setOnClickListener(this);
        findViewById(R.id.searchLayout).setOnClickListener(this);

        TouchEffectListener effectListener = new TouchEffectListener("#cc0033", "#a20033");
        ll_restaurant.setOnTouchListener(effectListener);
        ll_offer.setOnTouchListener(effectListener);
        ll_nearby.setOnTouchListener(effectListener);

        TouchEffectListener touchEffectgry = new TouchEffectListener("#FFFFFF", "#ced2d8");
        searchAny.setOnTouchListener(touchEffectgry);
        findViewById(R.id.searchLayout).setOnTouchListener(touchEffectgry);
        findViewById(R.id.txtSearchHome).setOnTouchListener(touchEffectgry);
        ll_loc.setOnTouchListener(touchEffectgry);

        txtSearchHome = (TextView) findViewById(R.id.txtSearchHome);
        txtSearchHome.setTypeface(tf);
        txtSearchHome.setOnClickListener(this);
        txtSearchHome.setOnEditorActionListener(new OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                // TODO Auto-generated method stub
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    String searchText = txtSearchHome.getText().toString()
                            .trim();
                    if (searchText.isEmpty() || searchText == ""
                            || searchText == null) {
                        AnimationTween.shakeOnError(HomeT.this, txtSearchHome);
                        return false;
                    }

                    if (!checkNetConnectionShowToast())
                        return false;

                    // hide soft keyboard
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(txtSearchHome.getWindowToken(), 0);

                    // foodyurl = getString(R.string.searchAny);
                    Intent i = new Intent(HomeT.this,
                            SearchResturentListView.class);
                    i.putExtra("searchKeyWord_home", txtSearchHome.getText()
                            .toString());
                    startActivity(i);
                }
                return false;
            }
        });


        searchAny.setOnTouchListener(new TouchEffectListener("#00000000", "#CCCCCC"));

		/*txtSearchHome.setOnTouchListener(new OnTouchListener() {
	        @Override
	        public boolean onTouch(View v, MotionEvent event) {
	            final int DRAWABLE_LEFT = 0;
	            final int DRAWABLE_TOP = 1;
	            final int DRAWABLE_RIGHT = 2;
	            final int DRAWABLE_BOTTOM = 3;
	            
	           

	            if(event!=null && event.getAction() == MotionEvent.ACTION_UP) {
	            	if(txtSearchHome.getCompoundDrawables()[DRAWABLE_RIGHT]==null)
	            		return false;
	            	
	                if(event!=null && event.getX()  >= 
	                		(txtSearchHome.getRight() - 
	                				txtSearchHome.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
	                    // your action here
	                	txtSearchHome.setText("");
	                 return true;
	                }
	            }
	            return false;
	        }
	    });
		
		txtSearchHome.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if (s.length() > 0){
					 Drawable x = getResources().getDrawable(android.R.drawable.ic_menu_close_clear_cancel);
	                	txtSearchHome.setCompoundDrawables(null, null, x, null);
				} else{
                	
					txtSearchHome.setCompoundDrawables(null, null, null, null);
                }
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.length() > 0){
					 Drawable x = getResources().getDrawable(android.R.drawable.ic_menu_close_clear_cancel);
	                	txtSearchHome.setCompoundDrawables(null, null, x, null);
				} else{
               	
					txtSearchHome.setCompoundDrawables(null, null, null, null);
               }
			}
		});*/

        textViewLocation = (TextView) findViewById(R.id.textViewLocation);
        textViewLocation.setTypeface(tf);

        textViewFeatureRes = (TextView) findViewById(R.id.textViewFeatureRes);
        textViewFeatureRes.setTypeface(tf);

        utils = new Utils(this);

        // Initilizing Grid View
        InitilizeGridLayout();

        //
        checkNetConnection();


        //checkChooseLocationAndLoadData();
        //checkUserPushDataCollectionQuary();

        // ad
        //AdRequest adRequest = new AdRequest.Builder().build();


        // Create the interstitial.
        interstitial = new InterstitialAd(this);
        interstitial.setAdUnitId(getString(R.string.adUnitInterstitial));
        //loadInterstitial();


        adView = (AdView) this.findViewById(R.id.adView);
        // adView.setVisibility(View.GONE);
        // Create ad request.
        AdRequest adRequestBanner = new AdRequest.Builder().build();
        adView.loadAd(adRequestBanner);

        adView.setAdListener(new AdListener() {
            public void onAdLoaded() {
                //	adView.setVisibility(View.VISIBLE);
            }

            public void onAdFailedToLoad(int errorcode) {
                System.out.println("Error:" + errorcode);
            }

        });

        //hit analytics
        analyticsSendScreenView(this.getClass().getSimpleName());


    }


    void checkUserPushDataCollectionQuary() {

        SavedPrefernce prefernce = new SavedPrefernce(this);
        User user = prefernce.getUser();
        if (prefernce.isUserExists() && TextUtils.isEmpty(prefernce.getUser().FavCus)) {
            Intent intent = new Intent(this, ScreenSlideActivity.class);
            startActivity(intent);
        } else {
            checkChooseLocationAndLoadData();
        }

    }


    public void createInterstitial() {
        // Create the interstitial.
        interstitial = new InterstitialAd(this);
        interstitial.setAdUnitId(getString(R.string.adUnitInterstitial));
    }


    public void loadInterstitial() {
        // Create ad request.
        AdRequest adRequest = new AdRequest.Builder().build();

        // Begin loading your interstitial.
        interstitial.loadAd(adRequest);

        interstitial.setAdListener(new AdListener() {
            public void onAdLoaded() {
                displayInterstitial();
            }

            public void onAdFailedToLoad(int errorcode) {
                //Toast.makeText(HomeT.this, "Error:" + errorcode, Toast.LENGTH_SHORT).show();
                System.out.println("Error:" + errorcode);
            }

        });
    }

    // Invoke displayInterstitial() when you are ready to display an interstitial.
    public void displayInterstitial() {
        // Toast.makeText(this, ""+interstitial.isLoaded(), Toast.LENGTH_SHORT).show();
        if (interstitial.isLoaded()) {
            interstitial.show();

            if (handlerAdmob != null) {
                handlerAdmob.removeCallbacks(runableAdMob);
            }

            //uncomment following line for auto load interstitial ad in every 60s
            // runnableInterstitial();
        }
    }

    private Runnable runableAdMob;
    private Handler handlerAdmob;
    private static final long interstitial_DELAY = 60000;
    boolean stopInterstitial = false;

    public void runnableInterstitial() {
        handlerAdmob = new Handler();
        runableAdMob = new Runnable() {
            public void run() {

                loadInterstitial();
                handlerAdmob.postDelayed(runableAdMob, interstitial_DELAY);

            }
        };
    }
	  
	  
/*// setup search view
		 private void setupSearchView(){
				SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
				 SearchView searchView = (SearchView) findViewById(R.id.searchView);
				SearchableInfo searchableInfo = searchManager.getSearchableInfo(getComponentName());
				searchView.setSearchableInfo(searchableInfo);
				//searchView.setSubmitButtonEnabled(true); 
				
				//customize
				searchView.setQueryHint("Place to eat...");
		        int searchPlateId = searchView.getContext().getResources().getIdentifier("android:id/search_plate", null, null);
		        View searchPlate = searchView.findViewById(searchPlateId);
		        if (searchPlate!=null) {
		            searchPlate.setBackgroundColor(Color.WHITE);
		            int searchTextId = searchPlate.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
		            TextView searchText = (TextView) searchPlate.findViewById(searchTextId);
		            if (searchText!=null) {
			            searchText.setTextColor(Color.GRAY);
			            searchText.setHintTextColor(Color.GRAY);
			            searchText.setTypeface( Typeface.createFromAsset(getAssets(),"fonts/SEGOEUIL.TTF"));
		            }
		        }
		        
		        	int searchIconId = searchView.getContext().getResources().getIdentifier("android:id/search_button", null, null);
		        	ImageView searchIcon = (ImageView) searchView.findViewById(searchIconId);
		        	if(searchIcon!=null){
		        		searchIcon.setImageResource(R.drawable.ic_search_home);
		        	}
		        
		        //Accessing the SearchAutoComplete
		        int queryTextViewId = getResources().getIdentifier("android:id/search_src_text", null, null);  
		        View autoComplete = searchView.findViewById(queryTextViewId);

		        
				try {
					Class<?> clazz = Class.forName("android.widget.SearchView$SearchAutoComplete");
					  SpannableStringBuilder stopHint = new SpannableStringBuilder("   ");  
				        stopHint.append("Place to eat...");

				        // Add the icon as an spannable
				        Drawable searchIcon = getResources().getDrawable(R.drawable.ic_search_home);  
				        Method textSizeMethod = clazz.getMethod("getTextSize");  
				        Float rawTextSize = (Float)textSizeMethod.invoke(autoComplete);  
				        int textSize = (int) (rawTextSize * 1.25);  
				        searchIcon.setBounds(0, 0, textSize, textSize);  
				        stopHint.setSpan(new ImageSpan(searchIcon), 1, 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

				        // Set the new hint text
				        Method setHintMethod = clazz.getMethod("setHint", CharSequence.class);  
				        setHintMethod.invoke(autoComplete, stopHint);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				int magId = getResources().getIdentifier("android:id/search_mag_icon", null, null);
				ImageView magImage = (ImageView) searchView.findViewById(magId);
				magImage.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
				magImage.setVisibility(View.GONE);
		      
		        	
		        		
			}
		 */
		/* void addDataToDB(ArrayList<SearchResturent> resturentList){
			
			 SearchSuggestionDBHelper dbHelper =new SearchSuggestionDBHelper(HomeT.this);
			     SQLiteDatabase mDatabase =dbHelper.getReadableDatabase();
			     DBOperation dbOperation=new DBOperation(mDatabase);
			     dbOperation.addRestaurantToDB(resturentList);
		 }*/

    /*void checkNetConnection() {
        // check internet
        cd = new ConnectionDetector(getApplicationContext());

        // Check if Internet present
        isInternetPresent = cd.isConnectingToInternet();

        if (!isInternetPresent) {
            // Internet Connection is not present
            Toast.makeText(this, getString(R.string.internetConMsg),
                    Toast.LENGTH_SHORT).show();

            return;
        }
    }*/
    private boolean isDataLoading = false;

    //LoadResList loadResList;
    //LoadCuisine loadCuisine;
    void checkChooseLocationAndLoadData() {
		
		/*if(isDataLoading){
			if(loadResList!= null)
				loadResList.cancel(true);
			
			if(loadCuisine!= null)
				loadCuisine.cancel(true);
		}*/


        SavedPrefernce savedPrefernce = new SavedPrefernce(this);
        //LocId = savedPrefernce.getLocationId();
        LocId = savedPrefernce.getLocationId();
        LocName = savedPrefernce.getLocationName();
        District = savedPrefernce.getDistrict();


        if (!TextUtils.isEmpty(LocId)) {
            textViewLocation.setText("Restaurants in " + LocName + " and Nearby Areas");
            Drawable innerDrawable = ContextCompat.getDrawable(this, R.mipmap.ic_chang_loc_home);
            GravityCompoundDrawable gravityDrawable = new GravityCompoundDrawable(innerDrawable);
            // set Drawable
            innerDrawable.setBounds(0, 0, innerDrawable.getIntrinsicWidth(), innerDrawable.getIntrinsicHeight());
            gravityDrawable.setBounds(0, 0, innerDrawable.getIntrinsicWidth(), innerDrawable.getIntrinsicHeight());
            textViewLocation.setCompoundDrawables(gravityDrawable, null, null, null);

            isDataLoading = true;

            showLoader();

            Volley.newRequestQueue(this).add(featureRestaurentListRequest);
            Volley.newRequestQueue(this).add(postRequest);
            loadEventAd();


            return;
        } else {
            Intent intent = new Intent(this, ExpandableActivity.class);
            startActivity(intent);

        }/*

		// Toast.makeText(this, LocId, Toast.LENGTH_LONG).show();
		String disId=savedPrefernce.getDistrictID();
		if (disId != null && !disId.isEmpty() && !disId.equals("")) {
		
			if (LocId != null && !LocId.isEmpty() && !LocId.equals("")) {
				LocName=savedPrefernce.getLocationName();
				textViewLocation.setText(savedPrefernce.getDistrict() + ", "+ savedPrefernce.getLocationName());
				
				LoadResList loadResList = new LoadResList();
				loadResList.execute();
				LoadCuisine loadCuisine=new LoadCuisine();
				loadCuisine.execute();
				
				if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
					loadResList.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		        else
		        	loadResList.execute();
				
		        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
		        	loadCuisine.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		        else
		        	loadCuisine.execute();
	
			}else{
				//Intent in = new Intent(getApplicationContext(),LocatonListView.class);
				Intent in = new Intent(getApplicationContext(),CityLocFragmentActivity.class);
				in.putExtra(StaticObjects.NEV_TYPE,StaticObjects.NEV_TYPE_CHOOSE_LOCTION_N_SAVE);
				in.putExtra("DisId", disId);
				in.putExtra("DisName", savedPrefernce.getDistrict());
				startActivity(in);
			}
		
		}else {
			//Intent intent = new Intent(HomeT.this, DistrictListView.class);
			Intent intent = new Intent(HomeT.this, CityLocFragmentActivity.class);
			intent.putExtra(StaticObjects.NEV_TYPE,StaticObjects.NEV_TYPE_CHOOSE_LOCTION_N_SAVE);
			startActivity(intent);
		}*/
    }

    //========
    private void changeViewColor(final View view) {
        // Load initial and final colors.
        final int initialColor = getResources().getColor(R.color.foodytab);
        final int finalColor = getResources().getColor(R.color.letseat_green);

        ValueAnimator anim = ValueAnimator.ofFloat(0, 1);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // Use animation position to blend colors.
                float position = animation.getAnimatedFraction();
                int blended = blendColors(initialColor, finalColor, position);

                // Apply blended color to the view.
                view.setBackgroundColor(blended);
            }
        });

        anim.setDuration(500).start();
    }

    private int blendColors(int from, int to, float ratio) {
        final float inverseRatio = 1f - ratio;

        final float r = Color.red(to) * ratio + Color.red(from) * inverseRatio;
        final float g = Color.green(to) * ratio + Color.green(from) * inverseRatio;
        final float b = Color.blue(to) * ratio + Color.blue(from) * inverseRatio;

        return Color.rgb((int) r, (int) g, (int) b);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

        if (!checkNetConnectionShowToast())
            return;

        //display add
        //displayInterstitial();

        if (v == ll_restaurant) {
            //analytic send button event
            analyticsSendEvent(this.getClass().getSimpleName(), StaticObjects.ANA_EV_CAT_ACTION, "Restaurant");

            //	changeViewColor(v);

            SavedPrefernce savedPrefernce = new SavedPrefernce(this);
            LocId = savedPrefernce.getLocationId();

            Intent i = new Intent(HomeT.this, ResturentListViewImage.class);
            i.putExtra("LocId", LocId);
            i.putExtra("LocName", savedPrefernce.getLocationName());
            i.putExtra(StaticObjects.NEV_TYPE, StaticObjects.NEV_TYPE_CHOOSE_LOCTION_N_SAVE);
            startActivity(i);
        } else if (v == ll_offer) {
            //analytic send button event
            analyticsSendEvent(this.getClass().getSimpleName(), StaticObjects.ANA_EV_CAT_ACTION, "Deals");

            Intent i = new Intent(HomeT.this, OfferActivity.class);
            startActivity(i);
        } else if (v == ll_nearby) {


           /* GPSTracker gps = new GPSTracker(this);
            // check if GPS location can get
            if (!gps.canGetLocation()) {
                //gps.showSettingsAlert(1);
                callForLocation=true;
                startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), LOCATION_ACCESS_SETTING_REQUEST);

            } else {*/

                //analytic send button event
                analyticsSendEvent(this.getClass().getSimpleName(), StaticObjects.ANA_EV_CAT_ACTION, "Nearby");

                //Intent i = new Intent(HomeT.this, NearByplaceListView.class);
                Intent i = new Intent(HomeT.this, NearByNativeRestaurantListNew.class);
                startActivity(i);

            //}


        } else if (v == searchAny || v.getId() == R.id.searchLayout || v.getId() == R.id.txtSearchHome) {

            callSearchDailog();
			
			/*Intent intent=new Intent(HomeT.this, SearchAndFilterList.class);
			startActivity(intent);
			return;*/

			/*String searchText = txtSearchHome.getText().toString().trim();
			if (searchText.isEmpty() || searchText == "" || searchText == null) {
				AnimationTween.shakeOnError(HomeT.this, txtSearchHome);
				return;
			}

			//analytic send button event
			analyticsSendEvent(this.getClass().getSimpleName(), StaticObjects.ANA_EV_CAT_ACTION, "Search", searchText);
			
			// hide soft keyboard
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(txtSearchHome.getWindowToken(), 0);

			// foodyurl = getString(R.string.searchAny);
			Intent i = new Intent(HomeT.this, SearchResturentListView.class);
			i.putExtra("searchKeyWord_home", txtSearchHome.getText().toString());

			// overridePendingTransition(R.anim.slide_up_in,R.anim.slide_up_out);
			startActivity(i);*/

        } else if (v == ll_loc) {


            analyticsSendEvent(this.getClass().getSimpleName(), StaticObjects.ANA_EV_CAT_ACTION, "Change Location");
            Intent intent = new Intent(this, ExpandableActivity.class);
            startActivity(intent);
			

			
			/*
			SavedPrefernce savedPrefernce = new SavedPrefernce(this);			
			String disId=savedPrefernce.getDistrictID();
			if (disId != null && !disId.isEmpty() && !disId.equals("")) {
			
				
					//Intent in = new Intent(getApplicationContext(),LocatonListView.class);
					Intent in = new Intent(getApplicationContext(),CityLocFragmentActivity.class);
					in.putExtra(StaticObjects.NEV_TYPE,StaticObjects.NEV_TYPE_CHOOSE_LOCTION);
					in.putExtra("DisId", disId);
					in.putExtra("DisName", savedPrefernce.getDistrict());
					startActivity(in);
				
			
			}else {
				//Intent intent = new Intent(HomeT.this, DistrictListView.class);
				Intent intent = new Intent(HomeT.this, CityLocFragmentActivity.class);
				intent.putExtra(StaticObjects.NEV_TYPE,StaticObjects.NEV_TYPE_CHOOSE_LOCTION);
				startActivity(intent);
			}*/

        }

    }

    private void InitilizeGridLayout() {
        Resources r = getResources();
        float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                AppConstant.GRID_PADDING, r.getDisplayMetrics());

        //Toast.makeText(this, R.integer.home_cuisine_col, Toast.LENGTH_LONG).show();


        int home_cuisine_col = r.getInteger(R.integer.home_cuisine_col);

        columnWidth = (int) ((utils.getScreenWidth() - ((home_cuisine_col + 1) * padding)) / home_cuisine_col);

        gridView.setNumColumns(home_cuisine_col);
        gridView.setColumnWidth(columnWidth);
        gridView.setStretchMode(GridView.NO_STRETCH);
        gridView.setPadding((int) padding, (int) padding, (int) padding,
                (int) padding);
        gridView.setHorizontalSpacing((int) padding);
        gridView.setVerticalSpacing((int) padding);
    }




	/*class LoadCuisine extends AsyncTask<String, String, String> {

		*//**
     * Before starting background thread Show Progress Dialog
     * *//*
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			// findViewById(R.id.cat_custom_progress).setVisibility(View.VISIBLE);

		}

		*//**
     * getting JSON
     * *//*
		protected String doInBackground(String... args) {
			// creating Places class object
			FoodyResturent foodyResturent = new FoodyResturent();

			try {
				cusineList = foodyResturent.getCusine(FoodyResturent.TYPE_LOAD_LOC, LocId, HomeT.this);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		*//**
     * After completing background task Dismiss the progress dialog and show
     * the data in UI Always use runOnUiThread(new Runnable()) to update UI
     * from background thread, otherwise you will get error
     * **//*
		protected void onPostExecute(String file_url) {

			
			 * AnimationTween.animateView(findViewById(R.id.cat_custom_progress),
			 * CusineListView.this); AnimationTween.animateViewPosition(lv,
			 * CusineListView.this);
			 

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					*/
    /**
     * Updating parsed Places into LISTVIEW
     *//*
					// Get json response status

					if (cusineList == null || cusineList.size() <= 0) {

						// show message
						
						 * findViewById(R.id.cat_custom_progress).setVisibility(View
						 * .VISIBLE); View v =
						 * findViewById(R.id.cat_custom_progress); TextView
						 * textPreparing = (TextView)
						 * v.findViewById(R.id.textPreparing);
						 * textPreparing.setText("Nothing found.");
						 * v.findViewById
						 * (R.id.progressBarPreparing).setVisibility(View.GONE);
						 * AnimationTween.animateViewPosition(v, HomeTest.this);
						 

					} else if (cusineList != null) {
						cusineListItems = new ArrayList<HashMap<String, String>>();
						// loop through each Cusine
						for (Cusine p : cusineList) {

							HashMap<String, String> map = new HashMap<String, String>();
							map.put("Id", p.Id);
							// Cusine name
							map.put("cusinename", p.cusinename);
							map.put("IMG", p.IMG);

							// adding HashMap to ArrayList
							cusineListItems.add(map);
						}

						adapter = new GridViewCuisineAdapte(HomeT.this,
								cusineListItems, columnWidth, new String[] {
										"Id", "cusinename", "IMG" },
								GridViewCuisineAdapte.GalleryTypeGal);

						gridView.setAdapter(adapter);
						adapter.notifyDataSetChanged();
						
						//change data loading status
						isDataLoading=false;
					}
				}

			});

		}

	}*/

    LetsEatRequest ler = new LetsEatRequest();
    // post value
    public static String LocId = "", LocName, District;

    public static String ZoneID = "", ZoneName;

    int currentPage = 0;
    int totalPage = 1;

    StringRequest postRequest = new StringRequest(Request.Method.POST, LetsEatRequest.CUSINE_URL,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {

                        try {
                            // Result handling
                            cusineList = ler.getCusine(response);

                            /**
                             * Updating parsed Places into LISTVIEW
                             * */
                            // Get json response status

                            if (cusineList == null || cusineList.size() <= 0) {

                                // show message
								/*
								 * findViewById(R.id.cat_custom_progress).setVisibility(View
								 * .VISIBLE); View v =
								 * findViewById(R.id.cat_custom_progress); TextView
								 * textPreparing = (TextView)
								 * v.findViewById(R.id.textPreparing);
								 * textPreparing.setText("Nothing found.");
								 * v.findViewById
								 * (R.id.progressBarPreparing).setVisibility(View.GONE);
								 * AnimationTween.animateViewPosition(v, HomeTest.this);
								 */

                            } else if (cusineList != null) {
                                cusineListItems = new ArrayList<HashMap<String, String>>();
                                // loop through each Cusine
                                for (Cusine p : cusineList) {

                                    HashMap<String, String> map = new HashMap<String, String>();
                                    map.put("Id", p.Id);
                                    // Cusine name
                                    map.put("cusinename", p.cusinename);
                                    map.put("IMG", p.IMG);

                                    // adding HashMap to ArrayList
                                    cusineListItems.add(map);
                                }

                                adapter = new GridViewCuisineAdapte(HomeT.this,
                                        cusineListItems, columnWidth, new String[]{
                                        "Id", "cusinename", "IMG"},
                                        GridViewCuisineAdapte.GalleryTypeGal);

                                gridView.setAdapter(adapter);
                                adapter.notifyDataSetChanged();

                                //change data loading status
                                isDataLoading = false;
                            }


                        } catch (Exception e) {


                            e.printStackTrace();
                        }

                    } catch (Exception e) {


                        e.printStackTrace();
                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            }
    ) {
        @Override
        protected Map<String, String> getParams() {
            Map<String, String> params = new HashMap<String, String>();
            // the POST parameters:
            params.put("LocId", LocId);
            return params;
        }
    };


    ArrayList<Restaurent> resturentList;

    // ListItems data
    ArrayList<HashMap<String, String>> resturantListItems = new ArrayList<HashMap<String, String>>();


    // Request a string response
    StringRequest featureRestaurentListRequest = new StringRequest(Request.Method.POST, ler.FEATURED_REST_LIST,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    resturentList = ler.getFeaturedRsturent(response);

                    findViewById(R.id.home_res_progress).setVisibility(View.GONE);

                    //Toast.makeText(HomeT.this, "onPostExecute: "+resturentList.size(),Toast.LENGTH_SHORT).show();
                    // Get json response status
                    if (resturentList == null || resturentList.size() <= 0) {
                        findViewById(R.id.home_res_progress).setVisibility(View.VISIBLE);
                        View v = findViewById(R.id.home_res_progress);
                        TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
                        textPreparing.setText("Nothing found in " + LocName + ". Change location or try again later.");
                        v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);


                        if (!checkNetConnection()) {
                            textPreparing.setText("No internet connection. Retry ");
                            textPreparing.setGravity(Gravity.CENTER);
                            textPreparing.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_refresh, 0);
                            findViewById(R.id.home_res_progress).setOnClickListener(new OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub
                                    checkChooseLocationAndLoadData();
                                }
                            });
                        }

                    } else if (resturentList != null) {
                        //search suggestion
                        ArrayList<SearchResturent> searchRestList = new ArrayList<SearchResturent>();
                        // Check for all possible status
                        resturantListItems = new ArrayList<HashMap<String, String>>();
                        // loop through each place
                        for (Restaurent p : resturentList) {

                            HashMap<String, String> map = new HashMap<String, String>();
                            map.put("Id", p.Id);
                            // Place name
                            map.put("RestName", p.RestName);
                            map.put("RestLoc", p.RestLoc + ", " + p.RestDistName);
                            map.put("IMG", p.IMG);
                            map.put("Rating", p.Rating);
                            map.put("Open", p.Open);
                            map.put("Close", p.Close);
                            map.put("Wifi", p.Wifi);
                            map.put("LiveMusic", p.LiveMusic);

                            totalPage = Integer.parseInt(p.PageNo);

                            // adding HashMap to ArrayList
                            resturantListItems.add(map);

                            //search suggestion
                            searchRestList.add(new SearchResturent(p.Id, p.RestName, null, p.RestLoc, null, null, null));
                        }
                        //search suggestion
                        //addDataToDB(searchRestList);
                        //start background job to add search suggestion
					/*Intent mServiceIntent = new Intent(HomeT.this, DataPushService.class);
					mServiceIntent.putExtra("ResData", searchRestList);
					//mServiceIntent.psetData(Uri.parse(""));
					HomeT.this.startService(mServiceIntent);*/

                        // list adapter
                        ImageSlideAdapter adapter = new ImageSlideAdapter(HomeT.this, resturantListItems, new String[]{
                                "RestName", "RestLoc", "Id", "IMG",
                                "Rating", "Open", "Close", "Wifi",
                                "LiveMusic"});

                        //Toast.makeText(HomeT.this, "init viewpager",Toast.LENGTH_LONG).show();

                        // Adding data into listview
                        view_pager = (ViewPager) findViewById(R.id.view_pager);
                        adapter.notifyDataSetChanged();
                        view_pager.setAdapter(adapter);
                        view_pager.getAdapter().notifyDataSetChanged();


                        view_pager.setOnPageChangeListener(new PageChangeListener());
                        view_pager.setOnTouchListener(new OnTouchListener() {

                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                v.getParent()
                                        .requestDisallowInterceptTouchEvent(
                                                true);
                                switch (event.getAction()) {

                                    case MotionEvent.ACTION_CANCEL:
                                        break;

                                    case MotionEvent.ACTION_UP:
                                        // calls when touch release on ViewPager
                                        if (resturantListItems != null
                                                && resturantListItems.size() != 0) {
                                            stopSliding = false;
                                            runnable(resturantListItems.size());
                                            handler.postDelayed(animateViewPager, ANIM_VIEWPAGER_DELAY_USER_VIEW);
                                        }
                                        break;

                                    case MotionEvent.ACTION_MOVE:
                                        // calls when ViewPager touch
                                        if (handler != null && stopSliding == false) {
                                            stopSliding = true;
                                            handler.removeCallbacks(animateViewPager);
                                        }
                                        break;
                                }
                                return false;
                            }
                        });

                    }


                }
            }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {

            // Error handling
            View v = findViewById(R.id.home_res_progress);
            TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
            textPreparing.setText("Retry ");
            v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);

            if (error instanceof TimeoutError ){
                textPreparing.setText("Connection timed out. Retry ");
            }else if(error instanceof NoConnectionError) {
                textPreparing.setText("Can't connect to Server. Retry ");
            } else if (error instanceof AuthFailureError) {
                textPreparing.setText("Authentication Failure . Retry ");
            } else if (error instanceof ServerError) {
                textPreparing.setText("Server Busy. Retry ");
            } else if (error instanceof NetworkError) {
                textPreparing.setText("Network Problem. Retry ");
            } else if (error instanceof ParseError) {
                textPreparing.setText("Can't read data. Retry ");
            }

            if (!checkNetConnection()) {
                textPreparing.setText("No internet. Connect to internet and retry ");

            }

              /*class 	AuthFailureError
              Error indicating that there was an authentication failure when performing a Request.
              class 	NetworkError
              Indicates that there was a network error when performing a Volley request.
              class 	NoConnectionError
              Error indicating that no connection could be established when performing a Volley request.
              class 	ParseError
              Indicates that the server's response could not be parsed.
              class 	ServerError
              Indicates that the error responded with an error response.
              class 	TimeoutError
              Indicates that the connection or the socket timed out.*/

            textPreparing.setGravity(Gravity.CENTER);
            textPreparing.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ic_refresh, 0);
            findViewById(R.id.home_res_progress).setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    checkChooseLocationAndLoadData();
                }
            });

            error.printStackTrace();

        }
    });

    private void showLoader() {
        findViewById(R.id.home_res_progress).setVisibility(View.VISIBLE);
        View v = findViewById(R.id.home_res_progress);
        TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
        textPreparing.setTypeface(tfNormal);
        textPreparing.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        textPreparing.setText("Preparing...");
        v.findViewById(R.id.progressBarPreparing).setVisibility(View.VISIBLE);
    }
	

	/*class LoadResList extends AsyncTask<String, String, String> {

		
		@Override
		protected void onPreExecute() {
			
			super.onPreExecute();
			//Toast.makeText(HomeT.this, "onPreExecute",Toast.LENGTH_SHORT).show();			
			findViewById(R.id.home_res_progress).setVisibility(View.VISIBLE);
			View v = findViewById(R.id.home_res_progress);
			TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
			textPreparing.setTypeface(tfNormal);
			textPreparing.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			textPreparing.setText("Preparing...");
			v.findViewById(R.id.progressBarPreparing).setVisibility(View.VISIBLE);	
		}

		protected String doInBackground(String... args) {
			// creating Places class object
			ler = new LetsEatRequest();

			try {
				resturentList=null;
				//String types = "LocId=" + LocId+"&Page="+currentPage;				
				//resturentList = foodyResturent.getRsturent(LocId, currentPage, HomeT.this);
				resturentList = ler.getFeaturedRsturent(HomeT.this);
				//Toast.makeText(HomeT.this, "doInBackground",Toast.LENGTH_SHORT).show();

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		*/
    /**
     * After completing background task Dismiss the progress dialog and show
     * the data in UI Always use runOnUiThread(new Runnable()) to update UI
     * from background thread, otherwise you will get error
     **//*
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
						findViewById(R.id.home_res_progress).setVisibility(View.GONE);
						
						//Toast.makeText(HomeT.this, "onPostExecute: "+resturentList.size(),Toast.LENGTH_SHORT).show();
					// Get json response status
					if (resturentList == null || resturentList.size() <= 0) {
						findViewById(R.id.home_res_progress).setVisibility(View.VISIBLE);
						View v = findViewById(R.id.home_res_progress);
						TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
						textPreparing.setText("Nothing found in " + LocName+". Change location or try again later.");
						v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
						
						
						if(!checkNetConnection())
							{
							 textPreparing.setText("No internet connection. Retry ");
							 textPreparing.setGravity(Gravity.CENTER);
							 textPreparing.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_refresh, 0);
							 findViewById(R.id.home_res_progress).setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									checkChooseLocationAndLoadData();
								}
							});
							}
						
					}else if (resturentList != null ){
						//search suggestion
						ArrayList<SearchResturent> searchRestList= new ArrayList<SearchResturent>(); 
						// Check for all possible status						
						resturantListItems = new ArrayList<HashMap<String, String>>();
						// loop through each place
						for (Restaurent p : resturentList) {

							HashMap<String, String> map = new HashMap<String, String>();
							map.put("Id", p.Id);
							// Place name
							map.put("RestName", p.RestName);
							map.put("RestLoc", p.RestLoc + ", "+ p.RestDistName);
							map.put("IMG", p.IMG);
							map.put("Rating", p.Rating);
							map.put("Open", p.Open);
							map.put("Close", p.Close);
							map.put("Wifi", p.Wifi);
							map.put("LiveMusic", p.LiveMusic);

							totalPage = Integer.parseInt(p.PageNo);

							// adding HashMap to ArrayList
							resturantListItems.add(map);
							
							//search suggestion
							searchRestList.add(new SearchResturent(p.Id, p.RestName, null, p.RestLoc, null, null, null));
						}
						//search suggestion
						//addDataToDB(searchRestList);
						//start background job to add search suggestion
						Intent mServiceIntent = new Intent(HomeT.this, DataPushService.class);
						mServiceIntent.putExtra("ResData", searchRestList);
						//mServiceIntent.psetData(Uri.parse(""));
						HomeT.this.startService(mServiceIntent);
						
						// list adapter
						ImageSlideAdapter adapter = new ImageSlideAdapter(HomeT.this, resturantListItems, new String[] {
										"RestName", "RestLoc", "Id", "IMG",
										"Rating", "Open", "Close", "Wifi",
										"LiveMusic" });

						//Toast.makeText(HomeT.this, "init viewpager",Toast.LENGTH_LONG).show();

						// Adding data into listview
						view_pager = (ViewPager) findViewById(R.id.view_pager);
						adapter.notifyDataSetChanged();
						view_pager.setAdapter(adapter);
						view_pager.getAdapter().notifyDataSetChanged();
						

						view_pager.setOnPageChangeListener(new PageChangeListener());
						view_pager.setOnTouchListener(new OnTouchListener() {

							@Override
							public boolean onTouch(View v, MotionEvent event) {
								v.getParent()
										.requestDisallowInterceptTouchEvent(
												true);
								switch (event.getAction()) {

								case MotionEvent.ACTION_CANCEL:
									break;

								case MotionEvent.ACTION_UP:
									// calls when touch release on ViewPager
									if (resturantListItems != null
											&& resturantListItems.size() != 0) {
										stopSliding = false;
										runnable(resturantListItems.size());
										handler.postDelayed(animateViewPager,ANIM_VIEWPAGER_DELAY_USER_VIEW);
									}
									break;

								case MotionEvent.ACTION_MOVE:
									// calls when ViewPager touch
									if (handler != null && stopSliding == false) {
										stopSliding = true;
										handler.removeCallbacks(animateViewPager);
									}
									break;
								}
								return false;
							}
						});

					}
				}

			});

		}

	}*/

    private Runnable animateViewPager;
    private Handler handler;
    private static final long ANIM_VIEWPAGER_DELAY = 5000;
    private static final long ANIM_VIEWPAGER_DELAY_USER_VIEW = 10000;
    boolean stopSliding = false;

    public void runnable(final int size) {
        handler = new Handler();
        animateViewPager = new Runnable() {
            public void run() {
                if (!stopSliding) {
                    if (view_pager.getCurrentItem() == size - 1) {
                        view_pager.setCurrentItem(0);
                    } else {
                        view_pager.setCurrentItem(view_pager.getCurrentItem() + 1, true);
                    }
                    handler.postDelayed(animateViewPager, ANIM_VIEWPAGER_DELAY);
                }
            }
        };
    }

    private class PageChangeListener implements OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int state) {
            if (state == ViewPager.SCROLL_STATE_IDLE) {
				/*
				 * if (products != null) { imgNameTxt.setText("" + ((Product)
				 * products.get(mViewPager .getCurrentItem())).getName()); }
				 */
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageSelected(int arg0) {
        }
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        adView.resume();
        checkUserPushDataCollectionQuary();

        // Logs 'install' and 'app activate' App Events. track on facebook dash board
        //AppEventsLogger.activateApp(this);

        // on change location reload data
		/*if (isLocationChange()) {
			Toast.makeText(this, "Location Changed", Toast.LENGTH_SHORT).show();

			checkChooseLocationAndLoadData();
			return;
		}
		
		
		
		if (checkNetConnection()&& (resturentList == null || resturentList.size() <= 0)) {
			findViewById(R.id.home_res_progress).setVisibility(View.VISIBLE);
			View v = findViewById(R.id.home_res_progress);
			TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
			textPreparing.setText("Preparing...");
			v.findViewById(R.id.progressBarPreparing).setVisibility(View.VISIBLE);			
			
			//checkChooseLocationAndLoadData();
			checkUserPushDataCollectionQuary();
		}*/


        runnable(resturantListItems.size());
        // Re-run callback
        handler.postDelayed(animateViewPager, ANIM_VIEWPAGER_DELAY);


    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        //view animation cancel
        if (handler != null) {
            // Remove callback
            handler.removeCallbacks(animateViewPager);
        }

        //adMobAuto load Cancel
        if (handlerAdmob != null) {
            handlerAdmob.removeCallbacks(runableAdMob);
        }

        adView.pause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        adView.destroy();
        super.onDestroy();

        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub

        switch (item.getItemId()) {

            case R.id.action_search:
                // search action
                if (txtSearchHome.isFocused()) {
                    txtSearchHome.clearFocus();
                    // hide soft keyboard
                    InputMethodManager immm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    immm.hideSoftInputFromWindow(txtSearchHome.getWindowToken(), 0);

                } else {
                    txtSearchHome.requestFocus();
                    // show soft keyboard
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(txtSearchHome, InputMethodManager.SHOW_IMPLICIT);
                }


                String searchText = txtSearchHome.getText().toString().trim();
                if (searchText.isEmpty() || searchText == "" || searchText == null) {
                    //AnimationTween.shakeOnError(HomeT.this, txtSearchHome);
                    return true;
                } else {


                    // foodyurl = getString(R.string.searchAny);
                    Intent i = new Intent(HomeT.this, SearchResturentListView.class);
                    i.putExtra("searchKeyWord_home", txtSearchHome.getText().toString());

                    // overridePendingTransition(R.anim.slide_up_in,R.anim.slide_up_out);
                    startActivity(i);
                }


                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main, menu);
        //hide menu
        menu.findItem(R.id.action_search).setVisible(false);
        menu.findItem(R.id.action_nearby).setVisible(false);


        return true;
    }

    private long lastPressedTime;
    private static final int PERIOD = 2000;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Handle the back button
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            if (event.getDownTime() - lastPressedTime < PERIOD) {
                finish();
                // exit application
                Intent startMain = new Intent(Intent.ACTION_MAIN);
                startMain.addCategory(Intent.CATEGORY_HOME);
                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(startMain);
                android.os.Process.killProcess(android.os.Process.myPid());
            } else {
                Toast.makeText(getApplicationContext(), "Press again to exit.",
                        Toast.LENGTH_SHORT).show();
                lastPressedTime = event.getEventTime();
            }

            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }

    }

    private boolean isLocationChange() {

        SavedPrefernce savedPrefernce = new SavedPrefernce(this);
        String newLocId = savedPrefernce.getLocationId();

        // Toast.makeText(this, LocId, Toast.LENGTH_LONG).show();

        if (newLocId != null && !newLocId.isEmpty() && !newLocId.equals("") && !newLocId.equals(LocId)) {

            LocName = savedPrefernce.getLocationName();
            textViewLocation.setText(savedPrefernce.getDistrict() + ", " + savedPrefernce.getLocationName());
            return true;
        }

        return false;

    }

    // searchDailog
    private ListView mSearchNFilterLv;

    private EditText mSearchEdt;

    private ArrayList<SearchResturent> mStringList;

    private ValueAdapter valueAdapter;

    private TextWatcher mSearchTw;

    Dialog searchDialog;
    ImageView imgFilter;

    void callSearchDailog() {

        if (searchDialog != null && searchDialog.isShowing()) {
            return;
        }

        searchDialog = new Dialog(HomeT.this, R.style.FullHeightDialog);

        searchDialog.setContentView(R.layout.activity_search_and_filter_list);
        searchDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        // open softkeyboard
        searchDialog.getWindow().setSoftInputMode(android.view.WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        searchDialog.setCancelable(true);

        initUI(searchDialog);

        searchDialog.setOnCancelListener(new OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                searchFilter = null;
                filterOn = false;

            }
        });

        //final MyEditText txtSearchHomeD = (MyEditText) rankDialog.findViewById(R.id.txt_search);
        mSearchEdt.setTypeface(tf);
        mSearchEdt.setOnEditorActionListener(new OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                // TODO Auto-generated method stub
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    String searchText = mSearchEdt.getText().toString().trim();
                    if (searchText.isEmpty() || searchText == "" || searchText == null) {
                        AnimationTween.shakeOnError(HomeT.this, mSearchEdt);
                        return false;
                    }

                    if (!checkNetConnectionShowToast())
                        return false;

                    // hide soft keyboard
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mSearchEdt.getWindowToken(), 0);

                    if (searchFilter != null) {
                        searchFilter.setSearch(mSearchEdt.getText().toString());
                        Intent i = new Intent(HomeT.this, SearchAdvanceResturentListView.class);
                        i.putExtra("searchFilter", searchFilter);
                        overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
                        startActivity(i);
                    } else {
                        Intent i = new Intent(HomeT.this, SearchResturentListView.class);
                        i.putExtra("searchKeyWord_home", mSearchEdt.getText().toString());
                        overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
                        startActivity(i);
                    }


                    searchDialog.cancel();
                }
                return false;
            }
        });

        ImageView searchAnyD = (ImageView) searchDialog.findViewById(R.id.imgSearch);
        searchAnyD.setOnTouchListener(new TouchEffectListener("#00000000", "#CCCCCC"));
        searchAnyD.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                String searchText = mSearchEdt.getText().toString().trim();
                if (searchText.isEmpty() || searchText == "" || searchText == null) {
                    AnimationTween.shakeOnError(HomeT.this, mSearchEdt);
                    return;
                }

                //analytic send button event
                analyticsSendEvent(this.getClass().getSimpleName(), StaticObjects.ANA_EV_CAT_ACTION, "Search", searchText);

                // hide soft keyboard
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mSearchEdt.getWindowToken(), 0);

                if (searchFilter != null) {
                    searchFilter.setSearch(mSearchEdt.getText().toString());
                    Toast.makeText(HomeT.this, "filter", Toast.LENGTH_SHORT).show();

                    Intent i = new Intent(HomeT.this, SearchAdvanceResturentListView.class);
                    i.putExtra("searchFilter", searchFilter);
                    overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
                    startActivity(i);
                } else {
                    Intent i = new Intent(HomeT.this, SearchResturentListView.class);
                    i.putExtra("searchKeyWord_home", mSearchEdt.getText().toString());
                    overridePendingTransition(R.anim.slide_up_in, R.anim.slide_up_out);
                    startActivity(i);
                }


                searchDialog.cancel();
            }
        });


        imgFilter = (ImageView) searchDialog.findViewById(R.id.imgFilter);
        imgFilter.setOnTouchListener(new TouchEffectListener("#00000000", "#CCCCCC"));
        imgFilter.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (filterOn) {
                    removeFilterDialog();
                } else {
                    callFilterDialog();
                }

            }
        });

        initData();

        valueAdapter = new ValueAdapter(mStringList, this);
        //dumy filter
        valueAdapter.getFilter().filter("~^*()");

        mSearchNFilterLv.setAdapter(valueAdapter);
        mSearchNFilterLv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long id) {
                String resID = valueAdapter.getFilterList().get(position).Id; //mStringList.get(position).Id;
                Log.e("", "=========================================" + "size:" + valueAdapter.getFilterList().size() + " <>" + valueAdapter.getFilterList().get(position).Id + valueAdapter.getFilterList().get(position).Name);
                analyticsSendEvent(this.getClass().getSimpleName(), StaticObjects.ANA_EV_CAT_SEARCH, "Suggestion", resID);


                Intent in = new Intent(getApplicationContext(), TabMainActivity.class);
                in.putExtra("Id", resID);
                startActivity(in);

                searchDialog.cancel();

            }
        });

        mSearchEdt.addTextChangedListener(mSearchTw);


        // now that the dialog is set up, it's time to show it

        searchDialog.show();
        AnimationTween.animateViewPositionUp(searchDialog.findViewById(R.id.searchLayout), HomeT.this);

    }


    Dialog filterDialog;
    TextView textViewCapacity;
    TextView textViewCost;
    ArrayList<ToggleButton> toggleButtons;

    private void callFilterDialog() {
        if (filterDialog != null && filterDialog.isShowing()) {
            return;
        }

        toggleButtons = new ArrayList<ToggleButton>();
        filterDialog = new Dialog(HomeT.this, R.style.FullHeightDialog);
        filterDialog.getWindow().getAttributes().windowAnimations = R.style.dialog_animation_slide_right;

        filterDialog.setContentView(R.layout.layout_search_filter);
        filterDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        filterDialog.setCancelable(true);

        textViewCapacity = (TextView) filterDialog.findViewById(R.id.textViewCapacity);
        textViewCost = (TextView) filterDialog.findViewById(R.id.textViewCost);

        filterDialog.findViewById(R.id.ll_toggle_ac).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleButton tg = (ToggleButton) filterDialog.findViewById(R.id.toggle_ac);
                tg.setTag(SearchFilter.tag_toggle_ac);
                toggleButtons.add(tg);
                tg.setChecked(!tg.isChecked());
            }
        });

        filterDialog.findViewById(R.id.ll_toggle_smoking_zone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleButton tg = (ToggleButton) filterDialog.findViewById(R.id.toggle_smoking_zone);
                tg.setTag(SearchFilter.tag_toggle_Smoking);
                toggleButtons.add(tg);
                tg.setChecked(!tg.isChecked());
            }
        });

        filterDialog.findViewById(R.id.ll_toggle_live_music).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleButton tg = (ToggleButton) filterDialog.findViewById(R.id.toggle_live_music);
                tg.setTag(SearchFilter.tag_toggle_LiveMusic);
                toggleButtons.add(tg);
                tg.setChecked(!tg.isChecked());
            }
        });

        filterDialog.findViewById(R.id.ll_toggle_seating_capacity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleButton tg = (ToggleButton) filterDialog.findViewById(R.id.toggle_seating_capacity);
                tg.setTag(SearchFilter.tag_toggle_Capacity);
                toggleButtons.add(tg);
                callCostDialog(CAPACITY, "Seat Capacity", tg);
            }
        });

        filterDialog.findViewById(R.id.ll_toggle_reservation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleButton tg = (ToggleButton) filterDialog.findViewById(R.id.toggle_reservation);
                tg.setTag(SearchFilter.tag_toggle_TRR);
                toggleButtons.add(tg);
                tg.setChecked(!tg.isChecked());
            }
        });

        filterDialog.findViewById(R.id.ll_toggle_non_veg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleButton tg = (ToggleButton) filterDialog.findViewById(R.id.toggle_non_veg);
                tg.setTag(SearchFilter.tag_toggle_NVeg);
                toggleButtons.add(tg);
                tg.setChecked(!tg.isChecked());
            }
        });

        filterDialog.findViewById(R.id.ll_toggle_location).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleButton tg = (ToggleButton) filterDialog.findViewById(R.id.toggle_location);
                tg.setTag(SearchFilter.tag_toggle_Location);
                toggleButtons.add(tg);
                TextView tv = (TextView) filterDialog.findViewById(R.id.textViewFilterLocation);

                changeLocationForFilter(tv, tg);
            }
        });

        filterDialog.findViewById(R.id.ll_toggle_kid_zone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleButton tg = (ToggleButton) filterDialog.findViewById(R.id.toggle_kid_zone);
                tg.setTag(SearchFilter.tag_toggle_Kidz);
                toggleButtons.add(tg);
                tg.setChecked(!tg.isChecked());
            }
        });

        filterDialog.findViewById(R.id.ll_toggle_home_delivary).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleButton tg = (ToggleButton) filterDialog.findViewById(R.id.toggle_home_delivary);
                tg.setTag(SearchFilter.tag_toggle_HomeDelivery);
                toggleButtons.add(tg);
                tg.setChecked(!tg.isChecked());
            }
        });

        filterDialog.findViewById(R.id.ll_toggle_dine_out).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleButton tg = (ToggleButton) filterDialog.findViewById(R.id.toggle_dine_out);
                tg.setTag(SearchFilter.tag_toggle_dine_out);
                toggleButtons.add(tg);
                tg.setChecked(!tg.isChecked());
            }
        });

        filterDialog.findViewById(R.id.ll_toggle_cost).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleButton tg = (ToggleButton) filterDialog.findViewById(R.id.toggle_cost);
                tg.setTag(SearchFilter.tag_toggle_Cost2p);
                toggleButtons.add(tg);
                callCostDialog(COST, "Cost", tg);
            }
        });

        filterDialog.findViewById(R.id.ll_toggle_cash_card).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleButton tg = (ToggleButton) filterDialog.findViewById(R.id.toggle_cash_card);
                tg.setTag(SearchFilter.tag_toggle_CreditC);
                toggleButtons.add(tg);
                tg.setChecked(!tg.isChecked());
            }
        });

        filterDialog.findViewById(R.id.ll_toggle_buffet).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleButton tg = (ToggleButton) filterDialog.findViewById(R.id.toggle_buffet);
                tg.setTag(SearchFilter.tag_toggle_Buffet);
                toggleButtons.add(tg);
                tg.setChecked(!tg.isChecked());
            }
        });

        filterDialog.findViewById(R.id.ll_toggle_breakfast).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleButton tg = (ToggleButton) filterDialog.findViewById(R.id.toggle_breakfast);
                tg.setTag(SearchFilter.tag_toggle_BFast);
                toggleButtons.add(tg);
                tg.setChecked(!tg.isChecked());
            }
        });

        filterDialog.findViewById(R.id.ll_toggle_bar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleButton tg = (ToggleButton) filterDialog.findViewById(R.id.toggle_bar);
                tg.setTag(SearchFilter.tag_toggle_Bar);
                toggleButtons.add(tg);
                tg.setChecked(!tg.isChecked());
            }
        });

        filterDialog.findViewById(R.id.ll_toggle_veg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleButton tg = (ToggleButton) filterDialog.findViewById(R.id.toggle_veg);
                tg.setTag(SearchFilter.tag_toggle_Veg);
                toggleButtons.add(tg);
                tg.setChecked(!tg.isChecked());
            }
        });

        filterDialog.findViewById(R.id.ll_toggle_wifi).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleButton tg = (ToggleButton) filterDialog.findViewById(R.id.toggle_wifi);
                tg.setTag(SearchFilter.tag_toggle_Wifi);
                toggleButtons.add(tg);
                tg.setChecked(!tg.isChecked());
            }
        });

        filterDialog.findViewById(R.id.ll_toggle_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog.dismiss();
                //set filter here
                setFilter(toggleButtons);
            }
        });
        filterDialog.findViewById(R.id.toggle_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog.dismiss();
                //set filter here
                setFilter(toggleButtons);
            }
        });


        filterDialog.show();
    }

    private boolean filterOn;
    SearchFilter searchFilter;

    void setFilter(ArrayList<ToggleButton> toggleButtons) {

        if (filterOn) {
            imgFilter.setImageResource(R.mipmap.ic_filter);
            filterOn = false;
            searchFilter = null;
        } else {
            imgFilter.setImageResource(R.mipmap.ic_filter_on);
            filterOn = true;


            String cuisine = "";
            String location = "";
            String cost2p = "";
            String capacity = "";

            boolean creditC = false;
            boolean wifi = false;
            boolean homeDelivery = false;
            boolean oUTSeat = false;
            boolean dineIn = false;
            boolean tRR = false;
            boolean bFast = false;
            boolean bar = false;
            boolean buffet = false;
            boolean veg = false;
            boolean nVeg = false;
            boolean aC = false;
            boolean kidz = false;
            boolean smoking = false;
            boolean liveMusic = false;

            for (int i = 0; i < toggleButtons.size(); i++) {
                ToggleButton tg = toggleButtons.get(i);

                if (tg.getTag().equals(SearchFilter.tag_toggle_CreditC)) {
                    creditC = tg.isChecked();
                } else if (tg.getTag().equals(SearchFilter.tag_toggle_Location)) {
                    location = LocId;
                } else if (tg.getTag().equals(SearchFilter.tag_toggle_Cost2p)) {
                    cost2p = "" + textViewCost.getText();
                } else if (tg.getTag().equals(SearchFilter.tag_toggle_Capacity)) {
                    capacity = "" + textViewCapacity.getText();
                } else if (tg.getTag().equals(SearchFilter.tag_toggle_Wifi)) {
                    wifi = tg.isChecked();
                } else if (tg.getTag().equals(SearchFilter.tag_toggle_HomeDelivery)) {
                    homeDelivery = tg.isChecked();
                } else if (tg.getTag().equals(SearchFilter.tag_toggle_dine_out)) {
                    oUTSeat = tg.isChecked();
                } else if (tg.getTag().equals(SearchFilter.tag_toggle_DineIn)) {
                    dineIn = tg.isChecked();
                } else if (tg.getTag().equals(SearchFilter.tag_toggle_TRR)) {
                    tRR = tg.isChecked();
                } else if (tg.getTag().equals(SearchFilter.tag_toggle_BFast)) {
                    bFast = tg.isChecked();
                } else if (tg.getTag().equals(SearchFilter.tag_toggle_Bar)) {
                    bar = tg.isChecked();
                } else if (tg.getTag().equals(SearchFilter.tag_toggle_Buffet)) {
                    buffet = tg.isChecked();
                } else if (tg.getTag().equals(SearchFilter.tag_toggle_Veg)) {
                    veg = tg.isChecked();
                } else if (tg.getTag().equals(SearchFilter.tag_toggle_NVeg)) {
                    nVeg = tg.isChecked();
                } else if (tg.getTag().equals(SearchFilter.tag_toggle_ac)) {
                    aC = tg.isChecked();
                } else if (tg.getTag().equals(SearchFilter.tag_toggle_Kidz)) {
                    kidz = tg.isChecked();
                } else if (tg.getTag().equals(SearchFilter.tag_toggle_Smoking)) {
                    smoking = tg.isChecked();
                } else if (tg.getTag().equals(SearchFilter.tag_toggle_LiveMusic)) {
                    liveMusic = tg.isChecked();
                }

            }

            searchFilter = new SearchFilter("", cuisine, location, cost2p, capacity, creditC, wifi, homeDelivery, oUTSeat, dineIn, tRR, bFast, bar, buffet, veg, nVeg, aC, kidz, smoking, liveMusic);

        }

    }


    private void changeLocationForFilter(final TextView textView, final ToggleButton toggleButton) {

        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
        // helpBuilder.setTitle("Pop Up");
        helpBuilder.setMessage("Use current location?");
        helpBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        textView.setText(LocName);
                        toggleButton.setChecked(true);
                    }
                });

        helpBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        textView.setText("Location");
                        toggleButton.setChecked(false);
                    }
                });

        //create and show the dialog
        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();

    }

    private void removeFilterDialog() {

        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
        // helpBuilder.setTitle("Pop Up");
        helpBuilder.setMessage("Remove current Filter?");
        helpBuilder.setPositiveButton("Remove",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        imgFilter.setImageResource(R.mipmap.ic_filter);
                        filterOn = false;
                    }
                });

        helpBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                });

        //create and show the dialog
        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();

    }

    Dialog costDialog;
    private final int COST = 0;
    private final int CAPACITY = 1;

    private void callCostDialog(final int pickerFor, String title, final ToggleButton toggleButton) {
        if (costDialog != null && costDialog.isShowing()) {
            return;
        }

        costDialog = new Dialog(HomeT.this, R.style.FullHeightDialog);
        if (pickerFor == COST) {
            costDialog.setContentView(R.layout.layout_number_picker_cost);

        } else if (pickerFor == CAPACITY) {
            costDialog.setContentView(R.layout.layout_number_picker_capacity);

        }


        costDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        ((TextView) costDialog.findViewById(R.id.textViewTitle)).setText(title);

        costDialog.setCancelable(true);
        costDialog.show();
        final CircledPicker circled_picker = (CircledPicker) costDialog.findViewById(R.id.circled_picker);

        costDialog.setOnCancelListener(new OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {


            }
        });

        costDialog.setOnDismissListener(new OnDismissListener() {

            @Override
            public void onDismiss(DialogInterface dialog) {


            }
        });


        costDialog.findViewById(R.id.layUpdateConfirm).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (pickerFor == COST) {
                    textViewCost.setText("" + circled_picker.getCurrentValue());

                } else if (pickerFor == CAPACITY) {
                    textViewCapacity.setText("" + circled_picker.getCurrentValue());

                }
                toggleButton.setChecked(true);
                costDialog.dismiss();
            }
        });

        costDialog.findViewById(R.id.btnUpdateCancel).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                costDialog.cancel();
            }
        });


    }


    private void initData() {

        mStringList = new ArrayList<SearchResturent>();

        SearchSuggestionDBHelper dbHelper = new SearchSuggestionDBHelper(HomeT.this);
        SQLiteDatabase mDatabase = dbHelper.getReadableDatabase();
        DBOperation dbOperation = new DBOperation(mDatabase);

        mStringList = dbOperation.getRestaurntList(HomeT.this);
        mSearchTw = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() == 0) {
                    //dumy filter
                    valueAdapter.getFilter().filter("~^*()");
                } else {
                    valueAdapter.getFilter().filter(s);
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

    }

    private void initUI(Dialog rankDialog) {
        mSearchNFilterLv = (ListView) rankDialog.findViewById(R.id.list_view);
        mSearchEdt = (EditText) rankDialog.findViewById(R.id.txt_search);

    }

    //
    private ArrayList<Event> eventList;
    //private Event r;
    private void loadEventAd () {
        StringRequest postRequest = new StringRequest(Request.Method.POST, LetsEatRequest.EVENT_LIST_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            // Result handling
                            eventList=new LetsEatRequest().getEvents(response);

                            // updating UI from Background Thread
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    /**
                                     * show ad
                                     * */
                                     if (eventList != null) {
                                         // r = null;
                                         SavedPrefernce p= new SavedPrefernce(HomeT.this);
                                         for (int i=0; i<eventList.size();i++){
                                            Event r=eventList.get(i);
                                             if(p.getsaveEventAdReminder(r.EVENTID) ||p.getEventAdID(r.EVENTID).isEmpty()){

                                                loadAdd(r);


                                                 break;
                                             }
                                         }



                                    }
                                }

                            });


                        } catch (Exception e) {


                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                // the POST parameters:
                params.put("LocId", "7");
                params.put("CusId", "35");
                return params;
            }
        };

        Volley.newRequestQueue(this).add(postRequest);
    }

    Target target;
    private  void loadAdd(final Event r){
        Intent intent = new Intent(HomeT.this, FullscreenAdActivityImage.class);
        intent.putExtra ( "event" , r);/*
        intent.putExtra("BitmapImage", bitmap);*/
        startActivity(intent);
        /* target= new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
                Intent intent = new Intent(HomeT.this, FullscreenAdActivity.class);
                intent.putExtra ( "event" , r);
                intent.putExtra("BitmapImage", bitmap);
                startActivity(intent);
            }

            @Override
            public void onBitmapFailed(Drawable drawable) {

            }

            @Override
            public void onPrepareLoad(Drawable drawable) {

            }
        };

        Picasso.with(HomeT.this).load(r.IMG).into(target);*/
    }


    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LOCATION_ACCESS_SETTING_REQUEST && new  GPSTracker(this).canGetLocation() && callForLocation ) {

                    //analytic send button event
                    analyticsSendEvent(this.getClass().getSimpleName(), StaticObjects.ANA_EV_CAT_ACTION, "Nearby");

                    //Intent i = new Intent(HomeT.this, NearByplaceListView.class);
                    Intent i = new Intent(HomeT.this, NearByNativeRestaurantList.class);
                    startActivity(i);
        }
        callForLocation=false;
    }*/


   /* private List<MenuMainCategory> menuMainCategoriesList ;
    private void loadNewMenuList () {
        StringRequest postRequest = new StringRequest(Request.Method.POST, LetsEatRequest.MENU_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            // Result handling
                            menuMainCategoriesList=new LetsEatRequest().getMenuMainCattegory(response);
                            System.out.print("");

                            // updating UI from Background Thread
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    *//**
                                     * show ad
                                     * *//*

                                }

                            });


                        } catch (Exception e) {


                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                // the POST parameters:
                params.put("ResId", "1740");
                return params;
            }
        };

        Volley.newRequestQueue(this).add(postRequest);
    }*/


}
