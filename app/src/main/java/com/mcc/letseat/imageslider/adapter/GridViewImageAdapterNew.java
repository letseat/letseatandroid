package com.mcc.letseat.imageslider.adapter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;



import com.foody.AppData.StaticObjects;
import com.mcc.letseat.R;
import com.mcc.letseat.CapturePhotoActivity;
import com.mcc.letseat.imageslider.FullScreenViewActivity;
import com.mcc.user.RegistrationActivity;
import com.mcc.user.SavedPrefernce;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Photo Gallery Adapter
 * @author Arif
 *
 */
public class GridViewImageAdapterNew extends BaseAdapter {

	public  static int GalleryTypeMenu=1;
	public  static int GalleryTypeGal=2;
	
	private Activity _activity;
	private ArrayList<HashMap<String, String>> _filePaths = new ArrayList<>();
	private int imageWidth;
	TypedArray imgs;
	String listTags[];
	
	public  int galleryType=0; 

	public GridViewImageAdapterNew(Activity activity,
			ArrayList<HashMap<String, String>> filePaths, int imageWidth,
			String listTags[], int galleryType) {
		this._activity = activity;
		this._filePaths = filePaths;
		this.imageWidth = imageWidth;
		this.listTags = listTags;
		imgs = _activity.getResources().obtainTypedArray(R.array.image_ids);
		this.galleryType=galleryType;
	}

	@Override
	public int getCount() {
		return this._filePaths.size();
	}

	@Override
	public Object getItem(int position) {
		return this._filePaths.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ImageView imageView;
		View view;
		if (convertView == null) {
			//imageView = new ImageView(_activity);
			LayoutInflater li = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view=li.inflate(R.layout.grid_view_image, parent,false);
			
		} else view = (View) convertView;
		
		

		// get screen dimensions
		// Bitmap image = decodeFile(_filePaths.get(position),
		// imageWidth,imageWidth);
		imageView=(ImageView)view.findViewById(R.id.imageView_grid_view_image);
		imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
		view.setLayoutParams(new GridView.LayoutParams(imageWidth,imageWidth));
		// imageView.setImageBitmap(image);
		
		TextView textView=(TextView)view.findViewById(R.id.txtStatus_grid_view_image);
		textView.setVisibility(View.INVISIBLE);
		
		
		try {
			if(_filePaths!=null && !_filePaths.get(position).get(listTags[0]).equals("add")){
				
				if(_filePaths.get(position).get(listTags[3])!=null && !_filePaths.get(position).get(listTags[3]).isEmpty() && galleryType==GalleryTypeMenu){
					
					String menuScanebUrl= _filePaths.get(position).get(listTags[3]);
					
					Picasso.with(_activity)
				    .load(menuScanebUrl)
				    .placeholder(R.drawable.lazy_ic)
				    .error(R.drawable.lazy_ic)
				    .into(imageView);
					
				}else if(_filePaths.get(position).get(listTags[3])!=null && !_filePaths.get(position).get(listTags[3]).isEmpty() && galleryType==GalleryTypeGal){
					//_filePaths.get(position).get(listTags[0]) replace "RestaurantPic" with "Thumbnail"
					
					String status=_filePaths.get(position).get("STATUS");					
					textView.setText(status);
					
					
					
					if(status.equals("")){
						textView.setVisibility(View.INVISIBLE);							
					}else if(status.equals("1")){
						textView.setText("Published");
						textView.setVisibility(View.VISIBLE);
						textView.setBackgroundColor( ContextCompat.getColor(_activity, R.color.photo_status_public));
					}else if(status.equals("0")){
						textView.setText("In Review");
						textView.setVisibility(View.VISIBLE);
						textView.setBackgroundColor(ContextCompat.getColor(_activity, R.color.photo_status_inreview));
					}
						
					
					
					
					String thumbUrl= _filePaths.get(position).get(listTags[3]);
					Picasso.with(_activity)
				    .load(thumbUrl)
				    .placeholder(R.drawable.lazy_ic)
				    .error(R.drawable.lazy_ic)
				    .into(imageView);
					
				
				}
				
				
				imageView.setTag("img");
			}else{
				imageView.setImageResource(R.drawable.ic_add_photo);
				imageView.setTag("add");
				imageView.setBackgroundResource(R.color.letseat_green);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		

		/*
		 * for (int i = 0; i < imgs.length(); i++) { Bitmap bitmap =
		 * BitmapFactory
		 * .decodeResource(_activity.getResources(),imgs.getResourceId(position,
		 * -1)); imageView.setImageBitmap(bitmap); }
		 */
		// image view click listener
		imageView.setOnClickListener(new OnImageClickListener(position));

		return view;
	}

	class OnImageClickListener implements OnClickListener {

		int _postion;

		// constructor
		public OnImageClickListener(int position) {
			this._postion = position;
		}

		@Override
		public void onClick(View v) {
			if(v.getTag()!=null && v.getTag().toString().equals("add")){
				
				
				SharedPreferences sharedpreferences = _activity.getSharedPreferences(RegistrationActivity.LET_EAT_PREFERENCES, Context.MODE_PRIVATE);
				
				 
					if(  sharedpreferences.getString(RegistrationActivity.Username, "").isEmpty() ){					
						StaticObjects.callRegForAction(_activity);
						
					}else{
						Intent intent=new Intent(_activity, CapturePhotoActivity.class);
						intent.putExtra("userid", new SavedPrefernce(_activity).getUserID() );
						
						if(listTags.length<=2)
						intent.putExtra("resid", "");
						else
							intent.putExtra("resid", _filePaths.get(0).get(listTags[2]));
						
						_activity.startActivity(intent);
					}
				
				
			}else{
				// on selecting grid view image
				// launch full screen activity
				Intent i = new Intent(_activity, FullScreenViewActivity.class);
				i.putExtra("_filePaths", _filePaths);
				i.putExtra("position", _postion);
				i.putExtra("galleryType", galleryType);
				
				_activity.startActivity(i);
				_activity.overridePendingTransition(0,R.anim.activity_open_scale);
			}
			
		}

	}

	/*
	 * Resizing image size
	 */
	public static Bitmap decodeFile(String filePath, int WIDTH, int HIGHT) {
		try {

			File f = new File(filePath);

			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f), null, o);

			final int REQUIRED_WIDTH;
			REQUIRED_WIDTH = WIDTH;
			final int REQUIRED_HIGHT;
			REQUIRED_HIGHT = HIGHT;
			int scale = 1;
			while (o.outWidth / scale / 2 >= REQUIRED_WIDTH
					&& o.outHeight / scale / 2 >= REQUIRED_HIGHT)
				scale *= 2;

			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

}
