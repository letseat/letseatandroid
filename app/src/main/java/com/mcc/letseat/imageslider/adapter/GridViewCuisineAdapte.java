package com.mcc.letseat.imageslider.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.foody.nearby.CusineMenuListView;
import com.mcc.letseat.HomeT;
import com.mcc.letseat.R;
import com.squareup.picasso.Picasso;

/**
 * Custom Adapter for Cuisine GridView
 * 
 * @author Arif
 * 
 */
public class GridViewCuisineAdapte extends BaseAdapter {

	public static int GalleryTypeMenu = 1;
	public static int GalleryTypeGal = 2;

	String fontPath = "fonts/SEGOEUI.TTF";

	private Activity _activity;
	private ArrayList<HashMap<String, String>> _filePaths = new ArrayList<HashMap<String, String>>();
	private int imageWidth;
	TypedArray imgs;
	String listTags[];
	
	int reqWidth= 400;
	int reqHeight= 400;

	public int galleryType = 0;
	Typeface tf;

	public GridViewCuisineAdapte(Activity activity,
			ArrayList<HashMap<String, String>> filePaths, int imageWidth,
			String listTags[], int galleryType) {
		this._activity = activity;
		this._filePaths = filePaths;
		this.imageWidth = imageWidth;
		this.listTags = listTags;
		imgs = _activity.getResources().obtainTypedArray(R.array.image_ids);
		this.galleryType = galleryType;
		
		 tf = Typeface.createFromAsset(_activity.getAssets(), fontPath);
	}

	@Override
	public int getCount() {
		return this._filePaths.size();
	}

	@Override
	public Object getItem(int position) {
		return this._filePaths.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	ViewHolder holder;
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		//final ViewHolder holder =null ;

		
		
		//LayoutInflater li = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		//v = li.inflate(R.layout.cuisine_item, null);
		if(convertView == null){
			try {
				// v = li.inflate(R.layout.cuisine_item, null);
				//convertView = View.inflate(_activity, R.layout.cuisine_item, null);
				 LayoutInflater inflater = LayoutInflater.from(_activity);
		          convertView = inflater.inflate(R.layout.cuisine_item, parent, false);
		          
		        	
		          
			} catch (InflateException e) {
				System.gc();
				// v = li.inflate(R.layout.cuisine_item, null);
				//convertView = View.inflate(_activity, R.layout.cuisine_item, null);
				 LayoutInflater inflater = LayoutInflater.from(_activity);
		          convertView = inflater.inflate(R.layout.cuisine_item, parent, false);
			}
			
			
			holder = new ViewHolder();
			holder.imageView = (ImageView) convertView.findViewById(R.id.imageView_cusineType);
				
			//get height and width of imageView
			ViewTreeObserver vto = holder.imageView.getViewTreeObserver();
			vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
			    public boolean onPreDraw() {
			    	holder.imageView.getViewTreeObserver().removeOnPreDrawListener(this);
			    	reqHeight = holder.imageView.getMeasuredHeight();
			        reqWidth = holder.imageView.getMeasuredWidth();
			        
			        return true;
			    }
			});
			
			holder.textView = (TextView) convertView.findViewById(R.id.textView_cusineType);
			convertView.setTag(holder);

		}else{
			// recycle the already inflated view 
			holder = (ViewHolder) convertView.getTag();
		}
		
		
		
		
		String cusineType = _filePaths.get(position).get(listTags[1]);
		holder.textView.setText(cusineType);
		holder.textView.setTypeface(tf);
		
		holder.imageView.setScaleType(ScaleType.CENTER_CROP);
		Resources res= _activity.getResources();
		int resId = getRandomResourceID( ((position + 1) % 11));
		
		
		
		
		
		/*Bitmap bitmap=  decodeBitmapFromResource(res, resId, reqWidth, reqHeight); 
		
		holder.imageView.setImageDrawable(new BitmapDrawable(res, bitmap));*/

		String cusineImgUrl = _filePaths.get(position).get("IMG");

		//loadBitmap(getRandomResourceID((position + 1) % 11), holder.imageView);
		
		if (cusineImgUrl != null && !cusineImgUrl.isEmpty()) {
			Picasso.with(_activity).load(cusineImgUrl)
					.placeholder(resId)
					.error(resId)
					.resize(reqWidth, reqWidth)
					.centerCrop() 
					.into(holder.imageView);
/*			Picasso.with(_activity).load(cusineImgUrl)
			.placeholder(holder.imageView.getDrawable())
			.error(holder.imageView.getDrawable())
			.into(holder.imageView);
*/		} 

		final int _position = position;
		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!((HomeT) _activity).checkNetConnection()) {
					((HomeT) _activity).showNetworkMsg();
					return;
				}

				Intent in = new Intent(_activity, CusineMenuListView.class);
				in.putExtra("id", _filePaths.get(_position).get(listTags[0]));
				in.putExtra("cuisineName",_filePaths.get(_position).get(listTags[1]));
				in.putExtra("img", _filePaths.get(_position).get("IMG"));

				_activity.startActivity(in);
			}
		});

		convertView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent e) {

				if (e.getAction() == MotionEvent.ACTION_UP
						|| e.getAction() == MotionEvent.ACTION_CANCEL
						|| e.getAction() == MotionEvent.ACTION_OUTSIDE) {

					v.findViewById(R.id.cusineTypeTL).setVisibility(
							View.INVISIBLE);

				} else if (e.getAction() == MotionEvent.ACTION_DOWN) {
					v.findViewById(R.id.cusineTypeTL).setVisibility(
							View.VISIBLE);
				}

				return false;
			}
		});

		return convertView;
	}
	
	private static class ViewHolder{
		ImageView imageView ;		
		TextView textView ;
	}
	
	private int getRandomResourceID(int random) {
		
		int d = 0;
		switch (random) {
		case 1:
			d = R.mipmap.back5;
			break;
		case 2:
			d = R.mipmap.back2;
			break;
		case 3:
			d = R.mipmap.back8;
			break;
		case 4:
			d = R.mipmap.back4;
			break;
		case 5:
			d = R.mipmap.back_login;
			break;
		case 6:
			d = R.mipmap.back6;
			break;
		case 7:
			d = R.mipmap.back7;
			break;
		case 8:
			d = R.mipmap.back3;
			break;
		case 9:
			d = R.mipmap.back9;
			break;
		case 10:
			d = R.mipmap.back10;
			break;
		case 11:
			d = R.mipmap.back11;
			break;

		default:
			d = R.mipmap.back8;
			break;
		}

		return d;
	}
	
	public  int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
    // Raw height and width of image
    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = 1;

    if (height > reqHeight || width > reqWidth) {

        final int halfHeight = height / 2;
        final int halfWidth = width / 2;

        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
        // height and width larger than the requested height and width.
        while ((halfHeight / inSampleSize) > reqHeight
                && (halfWidth / inSampleSize) > reqWidth) {
            inSampleSize *= 2;
        }
    }

    	return inSampleSize;
	}
	
	public  Bitmap decodeBitmapFromResource(Resources res, int resId,
	        int reqWidth, int reqHeight) {

	    // First decode with inJustDecodeBounds=true to check dimensions
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeResource(res, resId, options);

	    // Calculate inSampleSize
	    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	    return BitmapFactory.decodeResource(res, resId, options);
	}
	
	
}
