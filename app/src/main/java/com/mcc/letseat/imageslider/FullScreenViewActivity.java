package com.mcc.letseat.imageslider;

import java.util.ArrayList;
import java.util.HashMap;

import android.animation.TimeInterpolator;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Display;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;

import com.mcc.letseat.R;
import com.mcc.letseat.imageslider.adapter.FullScreenImageAdapter;
import com.mcc.letseat.imageslider.adapter.GridViewImageAdapterNew;
import com.mcc.letseat.imageslider.helper.Utils;
import com.mcc.libs.CustomViewPager;

/**
 * photo gallery full screen  
 * @author Arif
 *
 */
public class FullScreenViewActivity extends Activity {

	private Utils utils;
	private FullScreenImageAdapter adapter;
	private CustomViewPager viewPager;
	private ArrayList<HashMap<String, String>> _filePaths = new ArrayList<HashMap<String, String>>();
	//private String listTags[];

	private int focusedPage = 0;

	TextView textCaption;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fullscreen_view);

		viewPager = (CustomViewPager) findViewById(R.id.pager);
		
		textCaption = (TextView) findViewById(R.id.textCaption);

		utils = new Utils(getApplicationContext());

		Intent i = getIntent();
		int position = i.getIntExtra("position", 0);
		int galleryType = i.getIntExtra("galleryType", 0);
		
		_filePaths = (ArrayList<HashMap<String, String>>) i.getSerializableExtra("_filePaths");

		if (galleryType==GridViewImageAdapterNew.GalleryTypeGal) {
			_filePaths.remove((_filePaths.size() - 1));
		}

		adapter = new FullScreenImageAdapter(FullScreenViewActivity.this,_filePaths, new String[] { "IMG", "CAPTION" }, viewPager);

		viewPager.setAdapter(adapter);
		
		textCaption.setText(adapter.getArrayList().get(position).get("CAPTION"));

		// displaying selected image first
		viewPager.setCurrentItem(position, true);		
		animateTex();

		viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				// TODO Auto-generated method stub

				// mViewPager.getChildAt(i).isFocused()

				View currentView = viewPager.findViewWithTag(viewPager
						.getCurrentItem());
				// View view = viewPager.getFocusedChild();
				// Toast.makeText(FullScreenViewActivity.this,
				// ""+position+"<>"+adapter.mCurrentView,
				// Toast.LENGTH_SHORT).show();

				// TextView txtPhotCaption =(TextView)
				// adapter.mCurrentView.findViewById(R.id.txtPhotCaption);
				// txtPhotCaption.setText("manipulate");
				// txtPhotCaption.setAlpha(1);
				adapter.notifyDataSetChanged();

				String caption=adapter.getArrayList().get(position).get("CAPTION");
				
				if(caption.trim().equals("null")){
					caption="";
				}
				
				textCaption.setText(caption);

			}

			@Override
			public void onPageScrolled(int position, float positionOffset,
					int positionOffsetPixels) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int state) {
				// TODO Auto-generated method stub
				if (state == ViewPager.SCROLL_STATE_IDLE) {
					// adapter.changeAlpha();
					// Toast.makeText(FullScreenViewActivity.this, "IDLE",
					// Toast.LENGTH_SHORT).show();
				} else if (state == ViewPager.SCROLL_STATE_DRAGGING) {
					// adapter.changeAlpha();
					// Toast.makeText(FullScreenViewActivity.this, "DRAGGING",
					// Toast.LENGTH_SHORT).show();
				} else if (state == ViewPager.SCROLL_STATE_SETTLING) {
					// adapter.changeAlpha();
					// Toast.makeText(FullScreenViewActivity.this, "SETTLING",
					// Toast.LENGTH_SHORT).show();
					animateTex();

				}

			}
		});

		class MyPageChangeListener extends ViewPager.SimpleOnPageChangeListener {
			@Override
			public void onPageSelected(int position) {
				focusedPage = position;
			}
		}

	}

	void getsize() {
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		width = size.x;
		height = size.y;
	}

	int width = 0;
	int height = 0;
	private static final TimeInterpolator sDecelerator = new DecelerateInterpolator();
	private static final int ANIM_DURATION = 500;
	static float sAnimatorScale = 1;
	final long duration = (long) (ANIM_DURATION * sAnimatorScale);

	private void animateTex() {
		getsize();
		textCaption.setAlpha(0); 
		textCaption.setTranslationY(-textCaption.getHeight() + height);
		textCaption.animate().setDuration(duration ).translationY(0)
				.alpha(1).setInterpolator(sDecelerator);

	}
}
