package com.mcc.letseat.imageslider.adapter;



import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mcc.letseat.R;
import com.mcc.letseat.imageslider.helper.TouchImageView;
import com.mcc.libs.CustomViewPager;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Photo Gallery Fullscreen adepter
 * @author Arif
 *
 */
public class FullScreenImageAdapter extends PagerAdapter {

	public CustomViewPager customViewPager;
	
	private Activity _activity;
	private ArrayList<String> _imagePaths;
	private LayoutInflater inflater;
	TypedArray imgs;
	
	public final static String  VIEW_LAYOUT_PRIFIX="FSI";
	
	private ArrayList<HashMap<String, String>> _filePaths = new ArrayList<HashMap<String, String>>();
	String listTags[];

	// constructor
	public FullScreenImageAdapter(Activity activity,
			ArrayList<String> imagePaths) {
		this._activity = activity;
		this._imagePaths = imagePaths;
		 imgs = _activity.getResources().obtainTypedArray(R.array.image_ids);
	}
	
	// constructor
		public FullScreenImageAdapter(Activity activity,ArrayList<HashMap<String, String>> _filePaths, String listTags[], CustomViewPager customViewPager) {
			this._activity = activity;
			this._filePaths = _filePaths;
			 imgs = _activity.getResources().obtainTypedArray(R.array.image_ids);
			 this.listTags=listTags;
			 this.customViewPager=customViewPager;
		}
		
	public ArrayList<HashMap<String, String>>  getArrayList(){
		return _filePaths;
	}

	@Override
	public int getCount() {
		return this._filePaths.size();
	}

	@Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }
	
	@Override
    public Object instantiateItem(ViewGroup container, int position) {
        TouchImageView imgDisplay;
        Button btnClose;
 
        inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container,false);
 
        imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.imgDisplay);
        imgDisplay.viewPager=customViewPager;
        
        btnClose = (Button) viewLayout.findViewById(R.id.btnClose);
        TextView txtPhotCaption =(TextView) viewLayout.findViewById(R.id.txtPhotCaption);
        txtPhotCaption.setAlpha(0);
        
      /*  BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(_imagePaths.get(position), options);
        imgDisplay.setImageBitmap(bitmap);*/
       
        final ProgressBar progressBar_image_full=(ProgressBar) viewLayout.findViewById(R.id.progressBar_image_full);
      //loading image
        if(_filePaths.get(position).get(listTags[0])!=null && !_filePaths.get(position).get(listTags[0]).isEmpty()){
	        	Picasso.with(_activity)
		    .load(_filePaths.get(position).get(listTags[0]))
		    .placeholder(R.drawable.lazy_ic)
		    .error(R.drawable.lazy_ic)
		    .into(imgDisplay, new Callback() {
				
				@Override
				public void onSuccess() {					
					if(progressBar_image_full!=null){
						progressBar_image_full.setVisibility(View.GONE);
					}
					
				}
				
				@Override
				public void onError() {
					if(progressBar_image_full!=null){
						progressBar_image_full.setVisibility(View.GONE);
					}
					
				}
			});
        }
		
        
        txtPhotCaption.setText(_filePaths.get(position).get(listTags[1]));
        
        // close button click event
        btnClose.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				_activity.finish();
			}
		}); 

        ((ViewPager) container).addView(viewLayout);
        
        viewLayout.setTag(VIEW_LAYOUT_PRIFIX+position);
 
        return viewLayout;
	}
	

	
	@Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
 
    }
	
	public View mCurrentView;
	public View mCurrentTextViewCaption;

	@Override
	public void setPrimaryItem(ViewGroup container, int position, Object object) {
	    mCurrentView = (View)object;
	    mCurrentTextViewCaption=mCurrentView.findViewById(R.id.txtPhotCaption);
	}
	
	public void changeAlpha(){
		mCurrentTextViewCaption.setAlpha(1);
	}
	
	
	/*@Override
	public int getItemPosition(Object object) {
	    return POSITION_NONE;
	}*/
	/*public View findViewById(int position, int id) {
	    return _filePaths.get(position).findViewById(id);
	}*/

}
