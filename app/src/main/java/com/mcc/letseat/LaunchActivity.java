package com.mcc.letseat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.foody.AppData.StaticObjects;
import com.mcc.letseat.appstart.AppStart;
import com.mcc.user.SavedPrefernce;

public class LaunchActivity extends Activity {


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		//set picasso cache
		//StaticObjects.setPicassoCache(this);
		

		if (!new SavedPrefernce(this).isShowInfoScreen()) {
			Intent intent = new Intent(this, AppStart.class);
			startActivity(intent);
			finish();
		} else {
			Intent intent = new Intent(this, Splash.class);
			intent.putExtra(StaticObjects.NEV_TYPE_KEY,StaticObjects.NEV_TYPE_HOME);
			StaticObjects.nev_type = StaticObjects.NEV_TYPE_HOME;
			startActivity(intent);
			finish();
		}

	}
}
