package com.mcc.letseat.search;



import com.mcc.letseat.search.RestaurantSearchSuggestion.RSE;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class SearchSuggestionDBHelper extends SQLiteOpenHelper {
	
	public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "letseat.db";

	
	private static final String TEXT_TYPE = " TEXT";
	private static final String COMMA_SEP = ",";
	private static final String SQL_CREATE_ENTRIES =
	    "CREATE TABLE " + RSE.TABLE_NAME + " (" +
	    RSE._ID + " INTEGER PRIMARY KEY," +
	    RSE.RES_ID + TEXT_TYPE + COMMA_SEP +
	    RSE.RES_NAME + TEXT_TYPE + COMMA_SEP +
	    RSE.RES_ADDRESS + TEXT_TYPE + COMMA_SEP +
	    RSE.IMG_URL + TEXT_TYPE + COMMA_SEP +
	    RSE.DIS + TEXT_TYPE + COMMA_SEP +
	    RSE.LOC + TEXT_TYPE  +
	   
	    " )";

	private static final String SQL_DELETE_ENTRIES =
	    "DROP TABLE IF EXISTS " + RSE.TABLE_NAME;

	public SearchSuggestionDBHelper(Context context, String name,
			CursorFactory factory, int version) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		
	}
	
	public SearchSuggestionDBHelper(Context context){
		 super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	

	@Override
	public void onCreate(SQLiteDatabase db) {
		 db.execSQL(SQL_CREATE_ENTRIES);

		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		 db.execSQL(SQL_DELETE_ENTRIES);
	        onCreate(db);	        
	}
	
	@Override	
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }


}
