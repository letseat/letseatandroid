package com.mcc.letseat.search;

import android.provider.BaseColumns;

public class RestaurantSearchSuggestion {

	public RestaurantSearchSuggestion() {
	}
	
	 public static abstract class RSE implements BaseColumns {
	        public static final String TABLE_NAME = "res";
	        public static final String RES_ID = "resid";
	        public static final String RES_NAME = "resname";
	        public static final String RES_ADDRESS = "resaddress";
	        public static final String IMG_URL = "imgurl";
	        public static final String DIS = "disid";
	        public static final String LOC = "locid";
			public static final String COLUMN_NAME_NULLABLE = "null";
	        
	       
	    }


}
