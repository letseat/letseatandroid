/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mcc.letseat.search;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.SearchResturent;
import com.mcc.letseat.R;

import android.app.Activity;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

/**
 * The main activity for the dictionary.
 * Displays search results triggered by the search dialog and handles
 * actions from search suggestions.
 */
public class SearchableDictionary extends Activity {

    private TextView mTextView;
    private ListView mListView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_search_main);

        mTextView = (TextView) findViewById(R.id.search_main_text);
        mListView = (ListView) findViewById(R.id.search_main_list);
        
        setupSearchView();

        handleIntent(getIntent());
        
       findViewById(R.id.search_main_text).setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Toast.makeText(SearchableDictionary.this, "loading...", Toast.LENGTH_SHORT).show();
			new LoadData().execute();
			
		}
	});
       
    }
    
    private void setupSearchView(){
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		 SearchView searchView = (SearchView) findViewById(R.id.searchView);
		SearchableInfo searchableInfo = searchManager.getSearchableInfo(getComponentName());
		searchView.setSearchableInfo(searchableInfo);
		//searchView.setSubmitButtonEnabled(true); 
		
		//customize
		searchView.setQueryHint("Place to eat...");
        int searchPlateId = searchView.getContext().getResources().getIdentifier("android:id/search_plate", null, null);
        View searchPlate = searchView.findViewById(searchPlateId);
        if (searchPlate!=null) {
            searchPlate.setBackgroundColor(Color.WHITE);
            int searchTextId = searchPlate.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
            TextView searchText = (TextView) searchPlate.findViewById(searchTextId);
            if (searchText!=null) {
	            searchText.setTextColor(Color.GRAY);
	            searchText.setHintTextColor(Color.GRAY);
	            searchText.setTypeface( Typeface.createFromAsset(getAssets(),"fonts/SEGOEUIL.TTF"));
            }
        }
        
        	/*int searchIconId = searchView.getContext().getResources().getIdentifier("android:id/search_button", null, null);
        	ImageView searchIcon = (ImageView) searchView.findViewById(searchIconId);
        	if(searchIcon!=null){
        		searchIcon.setImageResource(R.drawable.ic_search_home);
        	}*/
        
        //Accessing the SearchAutoComplete
        int queryTextViewId = getResources().getIdentifier("android:id/search_src_text", null, null);  
        View autoComplete = searchView.findViewById(queryTextViewId);

        
		try {
			Class<?> clazz = Class.forName("android.widget.SearchView$SearchAutoComplete");
			  SpannableStringBuilder stopHint = new SpannableStringBuilder("   ");  
		        stopHint.append("Place to eat...");

		        // Add the icon as an spannable
		        Drawable searchIcon = getResources().getDrawable(R.mipmap.ic_search_home);
		        Method textSizeMethod = clazz.getMethod("getTextSize");  
		        Float rawTextSize = (Float)textSizeMethod.invoke(autoComplete);  
		        int textSize = (int) (rawTextSize * 1.25);  
		        searchIcon.setBounds(0, 0, textSize, textSize);  
		        stopHint.setSpan(new ImageSpan(searchIcon), 1, 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

		        // Set the new hint text
		        Method setHintMethod = clazz.getMethod("setHint", CharSequence.class);  
		        setHintMethod.invoke(autoComplete, stopHint);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int magId = getResources().getIdentifier("android:id/search_mag_icon", null, null);
		ImageView magImage = (ImageView) searchView.findViewById(magId);
		magImage.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
		magImage.setVisibility(View.GONE);
      
        	
        		
	}

    @Override
    protected void onNewIntent(Intent intent) {
        // Because this activity has set launchMode="singleTop", the system calls this method
        // to deliver the intent if this activity is currently the foreground activity when
        // invoked again (when the user executes a search from this activity, we don't create
        // a new instance of this activity, so the system delivers the search intent here)
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            // handles a click on a search suggestion; launches activity to show word
            Intent wordIntent = new Intent(this, WordActivity.class);
            wordIntent.setData(intent.getData());
            startActivity(wordIntent);
        } else if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            // handles a search query
            String query = intent.getStringExtra(SearchManager.QUERY);
            showResults(query);
        }
    }

    /**
     * Searches the dictionary and displays results for the given query.
     * @param query The search query
     */
    private void showResults(String query) {

        Cursor cursor = managedQuery(DictionaryProvider.CONTENT_URI, null, null, new String[] {query}, null);

        if (cursor == null) {
            // There are no results
            mTextView.setText(getString(R.string.no_results, new Object[] {query}));
        } else {
            // Display the number of results
            int count = cursor.getCount();
            String countString = getResources().getQuantityString(R.plurals.search_results,
                                    count, new Object[] {count, query});
            mTextView.setText(countString);

            // Specify the columns we want to display in the result
            String[] from = new String[] { DictionaryDatabase.KEY_WORD,
                                           DictionaryDatabase.KEY_DEFINITION };

            // Specify the corresponding layout elements where we want the columns to go
            int[] to = new int[] { R.id.word,
                                   R.id.definition };

            // Create a simple cursor adapter for the definitions and apply them to the ListView
            SimpleCursorAdapter words = new SimpleCursorAdapter(this,R.layout.layout_search_result, cursor, from, to);
            mListView.setAdapter(words);

            // Define the on-click listener for the list items
            mListView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    // Build the Intent used to open WordActivity with a specific word Uri
                    Intent wordIntent = new Intent(getApplicationContext(), WordActivity.class);
                    Uri data = Uri.withAppendedPath(DictionaryProvider.CONTENT_URI,String.valueOf(id));
                    wordIntent.setData(data);
                    startActivity(wordIntent);
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       /* MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            searchView.setIconifiedByDefault(false);
        }*/

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*switch (item.getItemId()) {
            case R.id.search:
                onSearchRequested();
                return true;
            default:
                return false;
        }*/
    	 return false;
    }


class Loader extends AsyncTask<String, String, String>{

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		return null;
	}
	
}

String types;
ArrayList<SearchResturent> resturentList;
class LoadData extends AsyncTask<String, String, String> {

	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
	}


	protected String doInBackground(String... args) {
		// creating Places class object
		FoodyResturent foodyResturent = new FoodyResturent();
		String searchKeywords = "kabab";
		String LocId=null;

		try {

			 types = "Search=" + searchKeywords;//+"&LocId="+LocId;//new SavedPrefernce(SearchResturentListView.this).getLocationId();

			resturentList = foodyResturent.search(types);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	protected void onPostExecute(String file_url) {

		runOnUiThread(new Runnable() {
			public void run() {
				/**
				 * Updating parsed Places into LISTVIEW
				 * */
				// Get json response status

				if (resturentList == null || resturentList.size() <= 0) {

					Toast.makeText(SearchableDictionary.this, "nothing...", Toast.LENGTH_LONG).show();
					
					 
				} else if (resturentList != null) {
					// Check for all possible status

					// Successfully got places details
					if (resturentList != null) {
						
						
						DictionaryDatabase db =new DictionaryDatabase(SearchableDictionary.this);
						db.mDatabaseOpenHelper.mDatabase= db.mDatabaseOpenHelper.getWritableDatabase();
						db.mDatabaseOpenHelper.addRestaurantToDB(resturentList);
						
					}

					else if (resturentList == null) {
						Toast.makeText(SearchableDictionary.this, "nothing...", Toast.LENGTH_LONG).show();
					}
				}
			}

		});

	}

}
}
