package com.mcc.letseat.search;


import java.util.ArrayList;

import com.foody.jsondata.SearchResturent;
import com.mcc.letseat.search.RestaurantSearchSuggestion.RSE;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DBOperation {
	
	private SQLiteDatabase mDatabase;
	public DBOperation(SQLiteDatabase mDatabase) {
		this.mDatabase = mDatabase;
	}
	
	
	 public void addRestaurantToDB(final ArrayList<SearchResturent> resturentList){
    	 new Thread(new Runnable() {
             public void run() {
                 loadRes(resturentList);
             }
         }).start();
    }
    
    void loadRes(ArrayList<SearchResturent> resturentList){
    	 for(int i=0; i<resturentList.size(); i++){
    		 SearchResturent sr= resturentList.get(i);
    		 //insertOrUpdate(sr.Id, sr.Name, sr.Address);
    		 addRestaturant(sr.Id, sr.Name, sr.Address);
         }
    }
	
	
	
	   public long addRestaturant(String resID, String name, String location) {
           ContentValues initialValues = new ContentValues();
           initialValues.put(RSE.RES_ID, resID);
           initialValues.put(RSE.RES_NAME, name);
           initialValues.put(RSE.RES_ADDRESS, location);
           
           String strFilter = RSE.RES_ID+"=" + resID;
           ContentValues updateValues = new ContentValues();
           updateValues.put(RSE.RES_NAME, name);
           updateValues.put(RSE.RES_ADDRESS, location);
           
           long rowEffect=0;
		   if(!mDatabase.isReadOnly())
		   {
         	  if(isExist(resID)){
				  rowEffect= mDatabase.update(RSE.TABLE_NAME, updateValues, strFilter, null);
			   }else{
				  rowEffect= mDatabase.insert(RSE.TABLE_NAME, null, initialValues);
			   }
		   }

		   return rowEffect;
           
         
       }
       
       private boolean isExist(String resID){
    	   Cursor cursor =null;
    	   int nOfRow=0;
    	   try {
    		   String select = "Select "+RSE.RES_ID +" from "+RSE.TABLE_NAME+" Where("+RSE.RES_ID+" like " + "'"+resID+"'" + ")";        
               //Cursor cursor = mDatabase.query(TABLE_NAME, FROM, select, null, null, null, null);
    		   cursor = mDatabase.query(RSE.TABLE_NAME, new String[] {RSE.RES_ID}, 
            		   RSE.RES_ID+" like " + "'"+resID+"'", null, null, null, null);
               //cursor.moveToFirst();
    		   nOfRow = cursor.getCount();
               
		} catch (Exception e) {
			// TODO: handle exception
		}finally{
			if(cursor!=null)
				cursor.close();
		}
       	
          
          return nOfRow>0;
           
       }
       
       
   	Cursor readData(Context context) {
		SearchSuggestionDBHelper dbHelper = new SearchSuggestionDBHelper(context);

		// Gets the data repository in write mode
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		// Define a projection that specifies which columns from the database
		// you will actually use after this query.
		String[] projection = { RSE._ID,
				RSE.RES_ID,
				RSE.RES_NAME,
				RSE.RES_ADDRESS,
				RSE.IMG_URL,
				RSE.DIS,
				RSE.LOC };

		// How you want the results sorted in the resulting Cursor
		String sortOrder = RSE.RES_ID
				+ " DESC";

		Cursor c = db.query(RSE.TABLE_NAME, // The table
																	// to query
				projection, // The columns to return
				null, // The columns for the WHERE clause
				null, // The values for the WHERE clause
				null, // don't group the rows
				null, // don't filter by row groups
				sortOrder // The sort order
				);

		c.moveToFirst();
		long itemId = c.getLong(c
				.getColumnIndexOrThrow(RSE._ID));
		
		return c;
	}
   	
	public ArrayList<SearchResturent> getRestaurntList(Context context) {
		SearchSuggestionDBHelper dbHelper = new SearchSuggestionDBHelper(context);

		// Gets the data repository in write mode
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		// Define a projection that specifies which columns from the database
		// you will actually use after this query.
		String[] projection = { RSE._ID,
				RSE.RES_ID,
				RSE.RES_NAME,
				RSE.RES_ADDRESS,
				RSE.IMG_URL,
				RSE.DIS,
				RSE.LOC };

		// How you want the results sorted in the resulting Cursor
		String sortOrder = RSE.RES_ID
				+ " DESC";

		Cursor c = db.query(RSE.TABLE_NAME, // The table
																	// to query
				projection, // The columns to return
				null, // The columns for the WHERE clause
				null, // The values for the WHERE clause
				null, // don't group the rows
				null, // don't filter by row groups
				sortOrder // The sort order
				);


		
		ArrayList<SearchResturent> list =new ArrayList<SearchResturent>();
		for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
		    // The Cursor is now set to the right position
		    
			
			String Id = c.getString(c.getColumnIndex(RSE.RES_ID));
			String Name = c.getString(c.getColumnIndex(RSE.RES_NAME)); 
			String Bestdish = "";
			String Address= c.getString(c.getColumnIndex(RSE.RES_ADDRESS)); 
			String latitude = ""; 
			String longitude= ""; 
			String IMG = c.getString(c.getColumnIndex(RSE.IMG_URL));
			
			SearchResturent sr =new SearchResturent(Id, Name, Bestdish, Address, latitude, longitude, IMG);
			
			list.add(sr);
		}
		
		return list;
	}
	

}
