package com.mcc.letseat.search;

import java.util.ArrayList;
import java.util.Locale;

import com.foody.jsondata.SearchResturent;
import com.mcc.letseat.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

public class ValueAdapter extends BaseAdapter implements Filterable{

private ArrayList<SearchResturent> mStringList;

private ArrayList<SearchResturent> mStringFilterList;

private LayoutInflater mInflater;

private ValueFilter valueFilter;

public ValueAdapter(ArrayList<SearchResturent> mStringList,Context context) {

    this.mStringList=mStringList;

    this.mStringFilterList=mStringList;

    mInflater=LayoutInflater.from(context);

    getFilter();
}

//How many items are in the data set represented by this Adapter.
@Override
public int getCount() {

    return mStringList.size();
}

//Get the data item associated with the specified position in the data set.
@Override
public Object getItem(int position) {

    return mStringList.get(position);
}

//Get the row id associated with the specified position in the list.
@Override
public long getItemId(int position) {

    return position;
}

public  ArrayList<SearchResturent> getFilterList(){
	
	return filterList;
}

//Get a View that displays the data at the specified position in the data set.
@Override
public View getView(int position, View convertView, ViewGroup parent) {

    Holder viewHolder;

    if(convertView==null) {

        viewHolder=new Holder();

        convertView=mInflater.inflate(R.layout.n_list_item, null);

        viewHolder.nameTv=(TextView)convertView.findViewById(R.id.first);
        viewHolder.loc=(TextView)convertView.findViewById(R.id.distance);

        convertView.setTag(viewHolder);

    }else{

        viewHolder=(Holder)convertView.getTag();
    }

        viewHolder.nameTv.setText(mStringList.get(position).toString());
        viewHolder.loc.setText(mStringList.get(position).Address);

        return convertView;
}

private class  Holder{

    TextView nameTv;
    TextView loc;
}

//Returns a filter that can be used to constrain data with a filtering pattern.
@Override
public Filter getFilter() {

    if(valueFilter==null) {

        valueFilter=new ValueFilter();
    }

    return valueFilter;
}

ArrayList<SearchResturent> filterList;
private class ValueFilter extends Filter {


    //Invoked in a worker thread to filter the data according to the constraint.
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {

        FilterResults results=new FilterResults();

        if(constraint!=null && constraint.length()>0){

            filterList=new ArrayList<SearchResturent>();

            for(int i=0;i<mStringFilterList.size();i++){
            	 
            	String lwName = mStringFilterList.get(i).Name.toLowerCase(Locale.getDefault());
            	String lwAddress = mStringFilterList.get(i).Address.toLowerCase(Locale.getDefault());
            	String name = mStringFilterList.get(i).Name;
            	String address = mStringFilterList.get(i).Address;
            	
            	

                if(name.contains(constraint) || lwName.contains(constraint) || lwAddress.contains(constraint) || address.contains(constraint) ) {

                    filterList.add(mStringFilterList.get(i));
                    
                   

                }
            }


            results.count=filterList.size();

            results.values=filterList;

        }else{

            results.count=mStringFilterList.size();

            results.values=mStringFilterList;

        }

        return results;
    }


    //Invoked in the UI thread to publish the filtering results in the user interface.
    @SuppressWarnings("unchecked")
    @Override
    protected void publishResults(CharSequence constraint,
            FilterResults results) {

        mStringList=(ArrayList<SearchResturent>) results.values;

        notifyDataSetChanged();


    }

}
}