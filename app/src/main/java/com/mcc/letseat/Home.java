package com.mcc.letseat;



import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;

import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

//import com.facebook.AppEventsLogger;
import com.foody.AppData.StaticObjects;
import com.foody.nearby.AlertDialogManager;
import com.foody.nearby.CategoryListView;
import com.foody.nearby.ConnectionDetector;
import com.foody.nearby.CusineListView;
import com.foody.nearby.DistrictListView;
import com.foody.nearby.FeaturedRestaurantListView;

import com.foody.nearby.NearByplaceListView;
import com.foody.nearby.OfferActivity;
import com.foody.nearby.SearchResturentListView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mcc.letseat.R;
import com.mcc.libs.ActivityWithSliding;
import com.mcc.libs.AnimationTween;
import com.mcc.libs.CyclicTransitionDrawable;
import com.mcc.libs.FlipAnimation;

public class Home extends ActivityWithSliding implements OnClickListener {

	AdView adView;
	ImageView category, category_back, resturent,resturent_back, location,location_back , 
	cuisine, cuisine_back, nearby,nearby_back, offer, offer_back,
			featured, featured_back , searchAny;
	EditText txtSearchHome;

	HorizontalScrollView sv;

	// flag for Internet connection status
	Boolean isInternetPresent = false;

	// Connection detector class
	ConnectionDetector cd;

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	RelativeLayout home_main_layout;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		 // Add code to print out the key hash
	   /*try {
	        PackageInfo info = getPackageManager().getPackageInfo(
	                "com.mcc.letseat", 
	                PackageManager.GET_SIGNATURES);
	        for (Signature signature : info.signatures) {
	            MessageDigest md = MessageDigest.getInstance("SHA");
	            md.update(signature.toByteArray());
	            Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
	            }
	    } catch (NameNotFoundException e) {

	    } catch (NoSuchAlgorithmException e) {

	    }*/
		
		
		
		setContentView(R.layout.home);
		home_main_layout = (RelativeLayout) findViewById(R.id.home_main_layout);
		/*

		int randomNum = (int) Math.round(Math.random() * 3);

		if (randomNum == 0) {
			home_main_layout.setBackgroundResource(R.drawable.home_back);
		} else if (randomNum == 1) {
			home_main_layout.setBackgroundResource(R.drawable.suggest_back);
		} else if (randomNum == 2) {
			home_main_layout.setBackgroundResource(R.drawable.back3);
		} else if (randomNum == 3) {
			home_main_layout.setBackgroundResource(R.drawable.sp);
		}*/
		
		
		// hide action bar
		getActionBar().hide();

		adView = (AdView) this.findViewById(R.id.adView);

		category = (ImageView) findViewById(R.id.category);
		category_back= (ImageView) findViewById(R.id.category_back);
		resturent = (ImageView) findViewById(R.id.resturent);
		resturent_back = (ImageView) findViewById(R.id.resturent_back);
		cuisine = (ImageView) findViewById(R.id.cuisine);
		cuisine_back = (ImageView) findViewById(R.id.cuisine_back);
		nearby = (ImageView) findViewById(R.id.nearby);
		nearby_back = (ImageView) findViewById(R.id.nearby_back);
		
		offer = (ImageView) findViewById(R.id.offer);
		offer_back = (ImageView) findViewById(R.id.offer_back);
		featured = (ImageView) findViewById(R.id.featured);
		featured_back = (ImageView) findViewById(R.id.featured_back);
		searchAny = (ImageView) findViewById(R.id.imgDetectCity);

		txtSearchHome = (EditText) findViewById(R.id.txtSearchHome);
		txtSearchHome.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				// TODO Auto-generated method stub
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					
					String searchText=txtSearchHome.getText().toString().trim();						
					if(searchText.isEmpty() || searchText=="" || searchText==null){
						AnimationTween.shakeOnError(Home.this, txtSearchHome);
						return false;
					}

					// hide soft keyboard
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(txtSearchHome.getWindowToken(), 0);

					foodyurl = getString(R.string.searchAny);
					Intent i = new Intent(Home.this,SearchResturentListView.class);
					i.putExtra("searchKeyWord_home", txtSearchHome.getText()
							.toString());
					startActivity(i);
				}
				return false;
			}
		});

		category.setOnClickListener(this);
		category_back.setOnClickListener(this);
		resturent.setOnClickListener(this);
		resturent_back.setOnClickListener(this);
		cuisine.setOnClickListener(this);
		cuisine_back.setOnClickListener(this);
		nearby.setOnClickListener(this);
		nearby_back.setOnClickListener(this);
		
		offer.setOnClickListener(this);
		offer_back.setOnClickListener(this);
		featured.setOnClickListener(this);
		featured_back.setOnClickListener(this);
		
		searchAny.setOnClickListener(this);

		sv = (HorizontalScrollView) findViewById(R.id.hsHome);
		// sv.scrollTo(0, sv.getBottom());
		// sv.scrollTo(5, 10);
		/*
		 * sv.post(new Runnable() { public void run() { sv.scrollTo(0,
		 * sv.getRight()/2); sv.fullScroll(HorizontalScrollView.FOCUS_DOWN);
		 * sv.smoothScrollTo(10, 10); } });
		 */

		sv.getViewTreeObserver().addOnGlobalLayoutListener(
				new ViewTreeObserver.OnGlobalLayoutListener() {
					@Override
					public void onGlobalLayout() {
						sv.post(new Runnable() {
							public void run() {
								Log.e("sv.getRight()",
										"sv.getRight():" + sv.getRight());

								sv.smoothScrollTo(100, 0);
							}
						});
					}
				});

		AdRequest adRequest = new AdRequest.Builder().build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});

		// initNaddSlideMenu();
		
		//auto background change
		try {
			Resources res = getResources();			
			CyclicTransitionDrawable ctd = new CyclicTransitionDrawable(
							new Drawable[]{
									res.getDrawable(R.mipmap.home_back),
									res.getDrawable(R.mipmap.back2),
									res.getDrawable(R.mipmap.back3),
									res.getDrawable(R.mipmap.back4),
									res.getDrawable(R.mipmap.back6),
									res.getDrawable(R.mipmap.back7),
									res.getDrawable(R.mipmap.back8),
									res.getDrawable(R.mipmap.back9),
									res.getDrawable(R.mipmap.back10),
									res.getDrawable(R.mipmap.back11)});

					((ImageView)findViewById(R.id.imageback)).setImageDrawable(ctd);
					ctd.startTransition(1000, 3000) ;
		}catch (OutOfMemoryError  e) {
			// TODO: handle exception
		} catch (Exception e) {
			// TODO: handle exception
		}finally{
			Resources res = getResources();
			CyclicTransitionDrawable ctd = new CyclicTransitionDrawable(
							new Drawable[]{
									res.getDrawable(R.mipmap.home_back),
									res.getDrawable(R.mipmap.back2)/*,
									res.getDrawable(R.mipmap.back3),
									res.getDrawable(R.mipmap.back4),
									res.getDrawable(R.mipmap.back6),
									res.getDrawable(R.mipmap.back7),
									res.getDrawable(R.mipmap.back8),
									res.getDrawable(R.mipmap.back9),
									res.getDrawable(R.mipmap.back10),
									res.getDrawable(R.v.back11)*/});
		
			((ImageView)findViewById(R.id.imageback)).setImageDrawable(ctd);
			ctd.startTransition(1000, 3000) ;
		}
		
		findViewById(R.id.imageViewMenuIndicator).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mSlideHolder.toggle();
				
			}
		});
				

	}

	String foodyurl = "";

	@Override
	public void onClick(View v) {

		// check internet
		cd = new ConnectionDetector(getApplicationContext());

		// Check if Internet present
		isInternetPresent = cd.isConnectingToInternet();

		if (!isInternetPresent) {
			// Internet Connection is not present
			Toast.makeText(this, "Please connect to Internet",
					Toast.LENGTH_SHORT).show();

			return;
		}
		
		//enable animation
		//Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(this, R.anim.hyperspace_jump);
		//v.startAnimation(hyperspaceJumpAnimation);
		
		

		if (v == category  || v == category_back ) {
			
			RelativeLayout card=(RelativeLayout)findViewById(R.id.category_card);
			View fromView= category;
			View toView=category_back;
			FlipAnimation flipAnimation=new FlipAnimation(fromView, toView, FlipAnimation.FLIP_VERTICAL);
			flipAnimation.setAnimationListener(new AnimationListener() {
				
				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					foodyurl = getString(R.string.category);
					Intent i = new Intent(Home.this, CategoryListView.class);
					i.putExtra("url", foodyurl);
					startActivity(i);
					
				}
			});
			
			 if (fromView.getVisibility() == View.GONE)
			    {
			        flipAnimation.reverse();
			    }
			 card.startAnimation(flipAnimation);
			//animation.start();

		} else if (v == resturent || v == resturent_back ) {
			
			
			RelativeLayout category_card=(RelativeLayout)findViewById(R.id.resturent_card);
			View fromView= resturent;
			View toView=resturent_back;
			FlipAnimation flipAnimation=new FlipAnimation(fromView, toView, FlipAnimation.FLIP_VERTICAL);
			flipAnimation.setAnimationListener(new AnimationListener() {
				
				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					foodyurl = getString(R.string.resturent);

					// Intent i = new Intent(Home.this, TabMainActivity.class);
					Intent i = new Intent(Home.this, DistrictListView.class);
					i.putExtra(StaticObjects.NEV_TYPE, StaticObjects.NEV_TYPE_CHOOSE_RES_4_DETAILS);
					i.putExtra("url", foodyurl);
					startActivity(i);
					
				}
			});
			
			 if (fromView.getVisibility() == View.GONE)
			    {
			        flipAnimation.reverse();
			    }
			category_card.startAnimation(flipAnimation);

		} else if (v == cuisine || v == cuisine_back) {
			
			
			RelativeLayout card=(RelativeLayout)findViewById(R.id.cuisine_card);
			View fromView= cuisine;
			View toView=cuisine_back;
			FlipAnimation flipAnimation=new FlipAnimation(fromView, toView, FlipAnimation.FLIP_VERTICAL);
			flipAnimation.setAnimationListener(new AnimationListener() {
				
				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					foodyurl = getString(R.string.cuisine);
					// Intent i=new Intent(Home.this,Main.class);
					Intent i = new Intent(Home.this, CusineListView.class);
					i.putExtra("url", foodyurl);
					startActivity(i);
					
				}
			});
			
			 if (fromView.getVisibility() == View.GONE)
			    {
			        flipAnimation.reverse();
			    }
			 card.startAnimation(flipAnimation);

		} else if (v == nearby || v == nearby_back) {
			RelativeLayout card=(RelativeLayout)findViewById(R.id.nearby_card);
			View fromView= nearby;
			View toView=nearby_back;
			FlipAnimation flipAnimation=new FlipAnimation(fromView, toView, FlipAnimation.FLIP_VERTICAL);
			flipAnimation.setAnimationListener(new AnimationListener() {
				
				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animation animation) {
					Intent i = new Intent(Home.this, NearByplaceListView.class);
					// Intent i=new Intent(Home.this,SearchResturentListView.class);
					startActivity(i);
					
				}
			});
			
			 if (fromView.getVisibility() == View.GONE)
			    {
			        flipAnimation.reverse();
			    }
			 card.startAnimation(flipAnimation);
			

		} else if (v == searchAny) {
			
			String searchText=txtSearchHome.getText().toString().trim();						
			if(searchText.isEmpty() || searchText=="" || searchText==null){
				AnimationTween.shakeOnError(Home.this, txtSearchHome);
				return ;
			}
			
			// hide soft keyboard
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(txtSearchHome.getWindowToken(), 0);

			foodyurl = getString(R.string.searchAny);
			Intent i = new Intent(Home.this, SearchResturentListView.class);
			i.putExtra("searchKeyWord_home", txtSearchHome.getText().toString());
			
			//overridePendingTransition(R.anim.slide_up_in,R.anim.slide_up_out);
			startActivity(i);
			
			

		} else if (v == offer || v == offer_back) {

			RelativeLayout card=(RelativeLayout)findViewById(R.id.offer_card);
			View fromView= offer;
			View toView=offer_back;
			FlipAnimation flipAnimation=new FlipAnimation(fromView, toView, FlipAnimation.FLIP_VERTICAL);
			flipAnimation.setAnimationListener(new AnimationListener() {
				
				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					Intent i = new Intent(Home.this, OfferActivity.class);
					// Intent i=new Intent(Home.this,SearchResturentListView.class);
					startActivity(i);
					
				}
			});
			
			 if (fromView.getVisibility() == View.GONE)
			    {
			        flipAnimation.reverse();
			    }
			 card.startAnimation(flipAnimation);

		} else if (v == featured || v == featured_back) {

			
			
			RelativeLayout card=(RelativeLayout)findViewById(R.id.featured_card);
			View fromView= featured;
			View toView=featured_back;
			FlipAnimation flipAnimation=new FlipAnimation(fromView, toView, FlipAnimation.FLIP_VERTICAL);
			flipAnimation.setAnimationListener(new AnimationListener() {
				
				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animation animation) {
					Intent i = new Intent(Home.this, FeaturedRestaurantListView.class);
					// Intent i=new Intent(Home.this,SearchResturentListView.class);
					startActivity(i);
					
				}
			});
			
			 if (fromView.getVisibility() == View.GONE)
			    {
			        flipAnimation.reverse();
			    }
			 card.startAnimation(flipAnimation);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		//search view widget 
		/*MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_main, menu);
		// Associate searchable configuration with the SearchView
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));*/

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
		case R.id.action_search:
			// search action
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	@Override
	public void onPause() {
		adView.pause();
		super.onPause();
		
		 // Logs 'app deactivate' App Event. track on facebook dash board
		 // AppEventsLogger.deactivateApp(this);
	}

	@Override
	public void onResume() {
		super.onResume();
		adView.resume();
		
		// Logs 'install' and 'app activate' App Events. track on facebook dash board
		  //AppEventsLogger.activateApp(this);
	}

	@Override
	public void onDestroy() {
		adView.destroy();
		super.onDestroy();
	}

	private long lastPressedTime;
	private static final int PERIOD = 2000;

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// Handle the back button
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			

			if (event.getDownTime() - lastPressedTime < PERIOD) {
				finish();
				// exit application
				Intent startMain = new Intent(Intent.ACTION_MAIN);
				startMain.addCategory(Intent.CATEGORY_HOME);
				startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(startMain);
				android.os.Process.killProcess(android.os.Process.myPid());
			} else {
				Toast.makeText(getApplicationContext(), "Press again to exit.",
						Toast.LENGTH_SHORT).show();
				lastPressedTime = event.getEventTime();
			}

			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}

	}

}
