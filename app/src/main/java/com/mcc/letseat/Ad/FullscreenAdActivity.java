package com.mcc.letseat.Ad;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.foody.jsondata.Event;
import com.foody.nearby.FeaturedRestaurantListView;
import com.mcc.letseat.R;
import com.mcc.user.SavedPrefernce;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class FullscreenAdActivity extends Activity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private View mControlsView;
    private View txtClose;

    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            mControlsView.setVisibility(View.VISIBLE);
            txtClose.setVisibility(View.VISIBLE);
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    /*
    * Intent in = new Intent(getApplicationContext(),FeaturedRestaurantListView.class);
				in.putExtra( "Id" , r.EVENTID);
				in.putExtra( "name" , r.EVNAME);
				in.putExtra( "IMG" , r.IMG);

				if(r.STARTDATE.equals(r.ENDDATE))
					in.putExtra("date", r.STARTDATE);
					else
						in.putExtra("date", r.STARTDATE+" to "+r.ENDDATE );

				startActivity(in);
    * */

    private Event event;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen_ad);

        mVisible = true;
        mControlsView = findViewById(R.id.fullscreen_content_controls);
        mContentView = findViewById(R.id.fullscreen_content);

        //get event from calling activity
        event = (Event)getIntent().getSerializableExtra("event");

        ((TextView)mContentView).setText( event != null? event.EVDESC : "");

        txtClose = findViewById(R.id.txtClose);
        txtClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addEventReminder( false);
                finish();
            }
        });

        // Set up the user interaction to manually show or hide the system UI.
        mContentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
               // finish();
               /* if (event == null) return;
                Intent in = new Intent(getApplicationContext(), FeaturedRestaurantListView.class);
                in.putExtra("Id", event.EVENTID);
                in.putExtra("name", event.EVNAME);
                in.putExtra("IMG", event.IMG);

                if (event.STARTDATE.equals(event.ENDDATE))
                    in.putExtra("date", event.STARTDATE);
                else
                    in.putExtra("date", event.STARTDATE + " to " + event.ENDDATE);

                startActivity(in);
                finish();*/
            }
        });



        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
      // findViewById(R.id.dummy_button).setOnTouchListener(mDelayHideTouchListener);
        findViewById(R.id.dummy_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //saving event id as it shown
                addEventReminder( true);
                finish();
            }});

        findViewById(R.id.dummy_button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (event == null) return;


                Intent in = new Intent(getApplicationContext(), FeaturedRestaurantListView.class);
                in.putExtra("Id", event.EVENTID);
                in.putExtra("name", event.EVNAME);
                in.putExtra("IMG", event.IMG);

                if (event.STARTDATE.equals(event.ENDDATE))
                    in.putExtra("date", event.STARTDATE);
                else
                    in.putExtra("date", event.STARTDATE + " to " + event.ENDDATE);

                startActivity(in);
                finish();

                //remove event reminder if exists
                addEventReminder( false);

            }
        });

        //saving event id as it shown
        new SavedPrefernce(FullscreenAdActivity.this).saveEventAdID(event.EVENTID);

    }

    private void addEventReminder(boolean reminder){
        new SavedPrefernce(FullscreenAdActivity.this).saveEventAdReminder(event.EVENTID, reminder);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mControlsView.setVisibility(View.GONE);
        txtClose.setVisibility(View.GONE);

        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }
}
