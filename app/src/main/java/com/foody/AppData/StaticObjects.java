
package com.foody.AppData;



import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.widget.BaseAdapter;

import com.google.android.gms.common.api.GoogleApiClient;
import com.mcc.letseat.R;
import com.mcc.user.LoginActivity;
import com.mcc.user.RegistrationOptionActivity;
import com.mcc.user.RegistrationOptionActivityAction;
import com.mcc.user.SavedPrefernce;

/**
 * 
 * @author Arif
 * Contain static object, GCM Type, Registration Type, Navigation Type etc. 
 *
 */
public class StaticObjects {
	
	//analytics event category
	public static final String ANA_EV_CAT_ACTION="Action";
	public static final String ANA_EV_CAT_SEARCH="Search";
	
	public static String nev_type="";	
	public static final String NEV_TYPE_ACTION="action";
	public static final String NEV_TYPE_HOME="homeT";
	public static final String NEV_TYPE_KEY="nev_type";
	
	//type face or font
	private static Typeface tfNormal , tfLight ;
	
	public static Typeface gettfNormal(Context c){
		
		if(tfNormal==null)
		 tfNormal = Typeface.createFromAsset(c.getAssets(), StaticObjects.fontPath_SEGOEUI);
		
		return tfNormal;
	}
	
	public static Typeface gettfLight(Context c){
		
		if(tfLight==null)
			tfLight = Typeface.createFromAsset(c.getAssets(), StaticObjects.fontPath_SEGOEUI_light);
		
		return tfLight;
	}
	
		
	public static int   LocationID=0,										
						resturentID = 1, 
						cuisineID = 2,
						eventID = 3,
						nearbyID = 4, 
						offerID = 5,  
						bookedDealsID = 6,  
						profileID=7, 
						settingID=8,  
						logoutID=9 ,
						searchAnyID = 10;
	
	//menu create
	public static ArrayList<HashMap<String, String>> appMenuData = new ArrayList<>();
	public static BaseAdapter adapter;
	
	public static String Menu_Key="menu" ;
	public static void createappMenuData(Activity activity){
		
		if(appMenuData.size()>0)
			return;
		
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(Menu_Key, "Change Location");
		appMenuData.add(map);
		
		
		map = new HashMap<String, String>();
		map.put(Menu_Key, "Restaurant");
		appMenuData.add(map);
		
		map = new HashMap<String, String>();
		map.put(Menu_Key, "Cuisines");
		appMenuData.add(map);

		map = new HashMap<String, String>();
		map.put(Menu_Key, "Event");
		appMenuData.add(map);
		
		map = new HashMap<String, String>();
		map.put(Menu_Key, "Nearby");
		appMenuData.add(map);

		map = new HashMap<String, String>();
		map.put(Menu_Key, "Deals");
		appMenuData.add(map);

		map = new HashMap<String, String>();
		map.put(Menu_Key, "Deals  Wishlist");
		appMenuData.add(map);
		
		map = new HashMap<String, String>();
		map.put(Menu_Key, "Profile");
		appMenuData.add(map);
		
		map = new HashMap<String, String>();
		map.put(Menu_Key, "Settings");
		appMenuData.add(map);
		
		SavedPrefernce prefernce=new SavedPrefernce(activity);
		if(prefernce.isUserExists()){
			map = new HashMap<String, String>();
			map.put(Menu_Key, "Logout");
			appMenuData.add(map);
		}else{
			map = new HashMap<String, String>();
			map.put(Menu_Key, "Login");
			appMenuData.add(map);
		}
	}
	
	public static ArrayList<HashMap<String, String>> getAppMenuData(Activity activity){
		
		if(appMenuData.size()==0)createappMenuData(activity);
		
		return appMenuData;
	}
	
	public static void changeLoginStatusInAppMenuData(){
		//HashMap<String, String> mapp = appMenuData.get(appMenuData.size() - 1);
		int pos=appMenuData.size() - 1;
		String status=appMenuData.get(pos).get(Menu_Key);//mapp.get(Menu_Key);
		
				if(status.equals("Login")){
					appMenuData.remove(pos);
					HashMap<String, String> map = new HashMap<String, String>();
					map.put(Menu_Key, "Logout");
					appMenuData.add(map);
				}else{
					appMenuData.remove(pos);
					HashMap<String, String> map = new HashMap<String, String>();
					map.put(Menu_Key, "Login");
					appMenuData.add(map);
				}
				
				if(adapter!=null){
					adapter.notifyDataSetChanged();
				}
		
	}

	
	
	
	// Google client to interact with Google API
		public static GoogleApiClient mGoogleApiClient;
		//public static ImageLoader imageLoader= new ImageLoader(MyApplication.getAppContext());		
		public static final int RATING_OFFSET = 4;
		
		//GCM or push  type
		public static String GCM_TYPE_GENERIC="0";//Generic Notification > Open App 
		public static String GCM_TYPE_REST="1"; //update info/new menu/food photo > Go to particular restaurant
		public static String GCM_TYPE_OFFER="2";//Discount Offer > Go to particular restaurant 
		public static String GCM_TYPE_EVENT="3";//Event Notification > list of restaurant
		public static String GCM_TYPE_FOLLOW="4";//User Follow > Go to profile page 
		public static String GCM_TYPE_PHOTO_PUBLISHED="5";//Photo Publish > Go to user's uploaded photo page
		public static String GCM_TYPE_APP_UPDATE="6";//App Update Play store 
		
		
	
		
		//Registration type
		public static String REG_TYPE_LE="l";
		public static String REG_TYPE_FB="f";
		public static String REG_TYPE_GPLUS="g";

		//navigation type 
		public static String NEV_TYPE="NevType";
		public static String NEV_TYPE_CHOOSE_RES_PHOTO_UP="res_photo_up";
		public static String NEV_TYPE_CHOOSE_LOCTION_N_SAVE="location_n_save ";		
		public static String NEV_TYPE_CHOOSE_LOCTION="location";
		public static String NEV_TYPE_CHOOSE_RES_4_DETAILS="res_details";
				
		
	
		
		//font
		public static String fontPath_SEGOEUI = "fonts/SEGOEUI.TTF";
		public static String fontPath_SEGOEUI_light = "fonts/SEGOEUIL.TTF";
		
		
		
		//R.drawable.home_back
		
		//background resource
		public static int backRes[]={R.mipmap.home_back, R.mipmap.suggest_back, R.mipmap.back2, R.mipmap.back3, R.mipmap.back4, R.mipmap.back5 };/*,
			, R.mipmap.back6, R.mipmap.back7, R.mipmap.back8, R.mipmap.back11, R.mipmap.back4, R.mipmap.back5,  R.mipmap.back8, R.mipmap.home_back};*/
		
		
		/*public static void callReg(Context c, boolean isFinish){
			Intent intent=new Intent(c, RegistrationOptionActivity.class);
			c.startActivity(intent);
			if(isFinish)
			((Activity)c).finish();	
		}*/
		
		public static void callRegAndGoHome(Context c){
			Intent intent=new Intent(c, RegistrationOptionActivity.class);
			intent.putExtra(NEV_TYPE_KEY, NEV_TYPE_HOME);
			c.startActivity(intent);
			//((Activity)c).finish();
		}
		
		public static void callRegForAction(Context c){
			Intent intent=new Intent(c, RegistrationOptionActivityAction.class);
			intent.putExtra(NEV_TYPE_KEY, NEV_TYPE_ACTION);
			c.startActivity(intent);
		}
		
		
		public static void callLogin(Context c){
			
			Intent intent=new Intent(c, LoginActivity.class);
			c.startActivity(intent);
		}
		
		
		/*public static boolean isFacebookLoggedIn() {
		    Session session = Session.getActiveSession();
		    
		    return (session != null && session.isOpened());
		}*/
		
		static String FbUserID="ID";

	
		  static String userID;
		  
		  
		  public static float convertDpToPixel(float dp, Context context){
			    Resources resources = context.getResources();
			    DisplayMetrics metrics = resources.getDisplayMetrics();
			    float px = dp * (metrics.densityDpi / 160f);
			    return px;
			}

			
			public static float convertPixelsToDp(float px, Context context){
			    Resources resources = context.getResources();
			    DisplayMetrics metrics = resources.getDisplayMetrics();
			    float dp = px / (metrics.densityDpi / 160f);
			    return dp;
			}
		

/*public static void setPicassoCache(Context context) {
				Picasso p = new Picasso.Builder(context)
			    .memoryCache(new LruCache(20000))
			    .build();
				
				
			} */




			
		
}

 class MyApplication extends Application{

    private static Context context;

    public void onCreate(){
        super.onCreate();
        MyApplication.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return MyApplication.context;
    }
}
