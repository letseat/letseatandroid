package com.foody.jsondata;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * json
 */
public class ResturentFeatured implements Serializable  {


	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String Id  ;
    public String RestName  ;
    public String BreakFast  ;
    public String SettingOutside  ;
    public String HomeDelivery  ;
    public String TableReservation  ;
    public String Discount  ;
    public String DineIn  ;
    public String Bar  ;
    public String AC  ;
    public String Wifi ;
    public String LiveMusic  ;
    public String Veg  ;
    public String NonVeg  ;
    public String CreditCard  ;
    public String Capacity  ;
    public String Cost4Two  ;
    public String RestLoc  ;
    public String RestDistName  ;
    public String latitude  ;
    public String longitude  ;
    public String Phone  ;
    public String IMG  ;
    public String Cusine;
    public String DisAmount;
    
    public ArrayList<String> MENU;
	private int headerViewStatus;


	public ResturentFeatured(String id, String restName, String breakFast,
			String settingOutside, String homeDelivery,
			String tableReservation, String discount, String dineIn,
			String bar, String aC, String wifi, String liveMusic, String veg,
			String nonVeg, String creditCard, String capacity, String cost4Two,
			String restLoc, String restDistName, String latitude,
			String longitude, String phone,String IMG , String Cusine, String DisAmount, ArrayList<String> mENU) {
		super();
		Id = id;
		RestName = restName;
		BreakFast = breakFast;
		SettingOutside = settingOutside;
		HomeDelivery = homeDelivery;
		TableReservation = tableReservation;
		Discount = discount;
		DineIn = dineIn;
		Bar = bar;
		AC = aC;
		Wifi = wifi;
		LiveMusic = liveMusic;
		Veg = veg;
		NonVeg = nonVeg;
		CreditCard = creditCard;
		Capacity = capacity;
		Cost4Two = cost4Two;
		RestLoc = restLoc;
		RestDistName = restDistName;
		this.latitude = latitude;
		this.longitude = longitude;
		Phone = phone;
		MENU = mENU;
		this.IMG=IMG;
		this.Cusine=Cusine;
		this.DisAmount=DisAmount;
	}


	public void setHeaderViewStatus(int headerViewStatus) {
		this.headerViewStatus = headerViewStatus;
	}

	public int getHeaderViewStatus() {
		return headerViewStatus;
	}
}
