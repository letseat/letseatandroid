package com.foody.jsondata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.*;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.foody.jsondata.NewsFeed.NewsFeed;
import com.foody.jsondata.user.Follower;
import com.foody.jsondata.user.User;
import com.foody.jsondata.user.UserBookedDeal;
import com.foody.jsondata.user.UserPubInfo;
import com.mcc.letseat.order.Slot;
import com.mcc.letseat.order.Table;
import com.mcc.letseat.quary.ExpandableActivity;
import com.mcc.letseat.quary.ExpandableListAdapterAll;
import com.mcc.user.SavedPrefernce;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

/**
 * 
 * @author Arif Contain all method that parse data from rest api
 * 
 */
public class LetsEatRequest {

	public static final int TYPE_RES = 0;
	public static final int TYPE_USER = 1;
	
	public static final int TYPE_LOAD_LOC=0;
	public static final int TYPE_LOAD_ZONE=1;

	public static final String FEED_TYPE_FAV = "Favorite";
	public static final String FEED_TYPE_FOLLOW = "Follow";
	public static final String FEED_TYPE_PHOTO = "PhotoShare";
	public static final String FEED_TYPE_REVIEW = "Comment";
	public static final String FEED_TYPE_RATING = "Rating";

	public static final String BU = "http://www.letseatasia.com/mobsvc";
	public static final String DU = "http://www.letseatasia.com/debugmobsvc";
	
	//current base url
//	public static final String CU = BU;
	public static final String CU = DU;

	// Let's Eat service url's
	public static final String FB_SHARE_URL = "​http://www.letseatasia.com/fb/?ResId=";
	private static final String RESTURENT_SEARCH_URL = CU + "/SearchList.php";
	private static final String RESTURENT_ADV_SEARCH_URL = CU
			+ "/AdvSearch.php";

	private static final String CATEGORY_URL = CU + "/FoodCategory.php";
	private static final String CATEGORY_MENU_URL = CU
			+ "/FoodCategoryMenu.php";
	public static final String CUSINE_URL = CU + "/FoodCusine.php";
	public static final String CUSINE_MENU_URL = CU + "/FoodCusResList.php";
	public static final String RESTURENT_LIST_URL = CU + "/RestaurentList.php";
	public static final String RESTAURENT_DETAILS_URL = CU + "/Details.php";
	public static final String MENU_URL = CU + "/RestMenuList.php";
	
	private static final String DISTRICT_LIST_URL = CU + "/District.php";
	private static final String ZONE_LIST_URL = CU + "/Zone.php";
	private static final String LOCATION_LIST_URL = CU + "/Location.php";
	public static final String DIS_LOC_ZONE_LIST_URL = CU + "/LocInfo.php";

	public static final String OFFER_LIST_URL = CU + "/DiscountList.php";
	public static final String EVENT_REST_LIST_URL = CU
			+ "/SpecialRestList.php";

	public static final String PROFILE_INSERT_URL = CU
			+ "/profile/profileInsert.php";
	public static final String PROFILE_EDIT_URL = CU
			+ "/profile/profileEdit.php";
	public static final String DELETE_PHOTOT_URL = CU
			+ "/profile/photoDelete.php";

	private static final String PROFILE_CHECK_URL = CU
			+ "/profile/getProfile.php";
	private static final String REVIEW_INSERT_URL = CU + "/profile/review.php";
	private static final String REVIEW_LIST_URL = CU
			+ "/profile/reviewList.php";

	private static final String REVIEW_LIKE_URL = CU
			+ "/profile/reviewLike.php";

	private static final String PHOTO_LIST_URL = CU + "/profile/photoList.php";
	public static final String PHOTO_INSERT_URL = CU + "/profile/photos.php";

	private static final String FOLLOWER_UPDATE_URL = CU
			+ "/profile/follower.php";

	private static final String FOLLOWER_LIST_URL = CU
			+ "/profile/followerList.php";
	private static final String FOLLOWING_LIST_URL = CU
			+ "/profile/followingList.php";

	private static final String USER_REVIEW_LIST_URL = CU
			+ "/profile/reviewListUser.php";
	private static final String USER_PHOTO_LIST_URL = CU
			+ "/profile/photoListUser.php";

	private static final String USER_FAVORITE_LIST_URL = CU
			+ "/profile/FavoriteList.php";
	public static final String USER_INFO_URL = CU + "/profile/getUserInfo.php";
	private static final String USER_FAV_INSERT_URL = CU
			+ "/profile/FavoriteInsert.php";
	private static final String USER_NEWSFEED_URL = CU
			+ "/profile/usersFeed.php";
	public static final String USER_SEARCH_URL = CU + "/profile/userSearch.php";

	private static final String FORGOT_PASSWORD_URL = CU
			+ "/profile/forgotpassword.php";
	public static final String REQUEST_SUGGETION_URL = CU + "/ReqSug.php";
	public static final String REQUEST_NEW_RESTAURANT_URL = CU
			+ "/NewRestaurant.php";

	// push registration
	// public static final String
	// PUSH_REG_URL="http://office.mcc.com.bd/pushLetsEat/droid/receivetoken.php";
	public static final String PUSH_REG_URL = CU
			+ "/pushLetsEat/droid/receivetoken.php";

	// native narby parm:lat lang rad
	public static final String RESTURENT_LIST_NEARBY_URL = CU
			+ "/NearByRes.php";

	// event list url
	public static final String EVENT_LIST_URL = CU + "/EventList.php";
	
	//Booking
	private static final String ADD_BOOKING_URL = CU + "/AddBooking.php";
	
	//featured restaurant list
	public final String FEATURED_REST_LIST= CU+  "/FeturedResList.php"; 

	static public final String USER_BOOKED_DEALS= CU+  "/MyDealsList.php"; 
	static public final String USER_REDEEM_DEALS= CU+  "/UpdateBookingStatus.php"; 
	
	
public ArrayList<UserBookedDeal> getBookedDeals(String  json) {

		
		ArrayList<UserBookedDeal> temp = new ArrayList<UserBookedDeal>();
		
		try {
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("userbookingdetails");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);
				
				 String DisId = c.getString("DisId");
			     String BookingId = c.getString("BookingId");
			     String ResId = c.getString("ResId"); 
			     String ResName = c.getString("ResName");
			     String DisName = c.getString("DisName");
			     String DisCompany = c.getString("DisCompany");
			     String DisPercent = c.getString("DisPercent");
			     String DisValid = c.getString("DisValid"); 
			     String IsHighlighted = c.getString("IsHighlighted"); 
			     String DisDetail = c.getString("DisDetail");
			     String IsLetsEat = c.getString("IsLetsEat");
			     String Location = c.getString("Location"); 
			     String District = c.getString("District"); 
			     String BookingStatus = c.getString("BookingStatus");
			     String TrackingCode = c.getString("TrackingCode");
				String Img = c.getString("Img");
				

			     UserBookedDeal nn = new UserBookedDeal(DisId, BookingId, ResId, ResName, DisName, DisCompany, DisPercent, DisValid, IsHighlighted, DisDetail, IsLetsEat, Location, District, BookingStatus, TrackingCode, Img);
			     temp.add(nn);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		/*if (temp.size() > 1) {
			sp.saveLocRestList(json, LocId + page);
			sp.setLocRestDataExpirDate(LocId + page);

		}*/

		return temp;
	}
	
	
	public ArrayList<Restaurent> getFeaturedRsturent(	String  json) {

		
		ArrayList<Restaurent> temp = new ArrayList<Restaurent>();
		
		try {
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("restaurent");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);
				String id = c.getString("Id");
				String restName = c.getString("RestName");
				String restLoc = c.getString("RestLoc");
				String restDistName = c.getString("RestDistName");
				String latitude = c.getString("latitude");
				String longitude = c.getString("longitude");
				String phone = c.getString("Phone");
				String pageNo = c.getString("PageNo");
				String IMG = c.getString("IMG");
				String Rating = c.getString("Rating");
				String Open = c.getString("Open");
				String Close = c.getString("Close");
				String Wifi = c.getString("Wifi");
				String LiveMusic = c.getString("LiveMusic");

				Restaurent nn = new Restaurent(id, restName, restLoc,
						restDistName, latitude, longitude, phone, pageNo, IMG,
						Rating, Open, Close, Wifi, LiveMusic);
				temp.add(nn);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		/*if (temp.size() > 1) {
			sp.saveLocRestList(json, LocId + page);
			sp.setLocRestDataExpirDate(LocId + page);

		}*/

		return temp;
	}
	
	public String addBooking(String ResId , String UserId, String DealId) {

		String postData = "ResId=" + ResId + "&UserId=" + UserId + "&DealId=" + DealId;

		JSONParser jParser = new JSONParser();

		String json = jParser.requestPost(ADD_BOOKING_URL, postData);
		try {
			if (json.startsWith("ErrorBlank") || json.startsWith("failed"))
				return "failed";
			else if (json.startsWith("DuplicateBooking"))
				return "DuplicateBooking";
			else if (json.startsWith("Success"))
				return "Success";
			else
				return "failed";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "failed";
	}

	public ArrayList<Event> getEvents(String json) {

		
		ArrayList<Event> temp = new ArrayList<Event>();
		
		try {
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("event");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);

				String EVENTID = c.getString("EVENTID");
				String EVNAME = c.getString("EVNAME");
				String EVDESC = c.getString("EVDESC");
				String STARTDATE = c.getString("STARTDATE");
				String ENDDATE = c.getString("ENDDATE");
				String IMG = c.getString("IMG");

				Event event = new Event(EVENTID, EVNAME, EVDESC, STARTDATE, ENDDATE,
						IMG);
				temp.add(event);

			}
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println(">>>>>>>>>>>>>>>   " + e.getMessage());
		}
		System.out.println(">>>>>>>>>>>>>>>   " + temp.size());
		return temp;
	}

	public ArrayList<Restaurent> getNearByRestaurents(String json) {

		
		ArrayList<Restaurent> temp = new ArrayList<Restaurent>();
		try {
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("restaurent");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);
				String id = c.getString("Id");
				String restName = c.getString("RestName");
				String restLoc = c.getString("RestLoc");
				String restDistName = c.getString("RestDistName");
				String latitude = c.getString("latitude");
				String longitude = c.getString("longitude");
				String phone = c.getString("Phone");
				String pageNo = c.getString("PageNo");
				String IMG = c.getString("IMG");
				String Rating = c.getString("Rating");
				String Open = c.getString("Open");
				String Close = c.getString("Close");
				String Wifi = c.getString("Wifi");
				String LiveMusic = c.getString("LiveMusic");
				
				String Address = c.getString("Address"); 	
				String BreakFast = c.getString("BreakFast");
				String SettingOutside = c.getString("SettingOutside");
				String HomeDelivery = c.getString("HomeDelivery");
				String TableReservation = c.getString("TableReservation");
				String Discount = c.getString("Discount");
				String DineIn = c.getString("DineIn");
				String Bar = c.getString("Bar");
				String AC = c.getString("AC");
				String Veg = c.getString("Veg");
				String NonVeg = c.getString("NonVeg");
				String CreditCard = c.getString("CreditCard");
				String Capacity = c.getString("Capacity");
				String Cost4Two = c.getString("Cost4Two");

				Restaurent nn = new Restaurent(id, restName, restLoc, restDistName, latitude, longitude, phone, pageNo, IMG, Rating, Open, Close, Wifi, LiveMusic, Address, BreakFast, SettingOutside, HomeDelivery, TableReservation, Discount, DineIn, Bar, AC, Veg, NonVeg, CreditCard, Capacity, Cost4Two);
				temp.add(nn);

			}
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println(">>>>>>>>>>>>>>>   " + e.getMessage());
		}
		System.out.println(">>>>>>>>>>>>>>>   " + temp.size());
		return temp;
	}

	public String resetPassword(String email) {

		String postData = "Email=" + email;

		JSONParser jParser = new JSONParser();

		String json = jParser.requestPost(FORGOT_PASSWORD_URL, postData);
		try {
			if (json.startsWith("Error") || json.startsWith("failed"))
				return "failed";
			else if (json.startsWith("Sent"))
				return "success";
			else
				return "failed";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "failed";
	}

	public ArrayList<NewsFeed> getUserNewsFeed(String userid) {

		String postData = "userid=" + userid;
		JSONParser jParser = new JSONParser();
		ArrayList<NewsFeed> temp = new ArrayList<NewsFeed>();
		String json = jParser.requestPost(USER_NEWSFEED_URL, postData);
		try {
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("userfeed");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);

				String Type = c.getString("Type");

				if (Type.equals(FEED_TYPE_FAV)) {
					com.foody.jsondata.NewsFeed.Favorite favorite = new com.foody.jsondata.NewsFeed.Favorite(
							Type, c.getString("FavResId"),
							c.getString("FavResName"), c.getString("RESIMG"),
							c.getString("FavDate"));
					temp.add(favorite);
				} else if (Type.equals(FEED_TYPE_FOLLOW)) {
					com.foody.jsondata.NewsFeed.Follow follow = new com.foody.jsondata.NewsFeed.Follow(
							Type, c.getString("FolloerId"),
							c.getString("FolloerName"),
							c.getString("IMGFOLWR"), c.getString("FolloDate"));
					temp.add(follow);
				} else if (Type.equals(FEED_TYPE_PHOTO)) {
					com.foody.jsondata.NewsFeed.PhotoShare photoShare = new com.foody.jsondata.NewsFeed.PhotoShare(
							Type, c.getString("Photo"), c.getString("PhotRes"),
							c.getString("PhotResName"), c.getString("PhotDate"));
					temp.add(photoShare);
				} else if (Type.equals(FEED_TYPE_REVIEW)) {
					com.foody.jsondata.NewsFeed.Comment comment = new com.foody.jsondata.NewsFeed.Comment(
							Type, c.getString("RevComm"),
							c.getString("RevRes"), c.getString("RevDate"),
							c.getString("RevRat"), c.getString("RevResName"));
					temp.add(comment);
				} else if (Type.equals(FEED_TYPE_RATING)) {
					com.foody.jsondata.NewsFeed.Comment comment = new com.foody.jsondata.NewsFeed.Comment(
							Type, c.getString("RevComm"),
							c.getString("RevRes"), c.getString("RevDate"),
							c.getString("RevRat"), c.getString("RevResName"));
					temp.add(comment);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return temp;
	}

	public String insertFavorite(String userid, String resid) {

		String postData = "userid=" + userid + "&resid=" + resid;

		JSONParser jParser = new JSONParser();

		String json = jParser.requestPost(USER_FAV_INSERT_URL, postData);
		try {
			if (json.startsWith("Error") || json.startsWith("failed"))
				return "failed";
			else if (json.startsWith("Fav"))
				return "Fav";
			else if (json.startsWith("UnFav"))
				return "UnFav";
			else
				return "failed";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "failed";
	}

	public ArrayList<UserPubInfo> getUserInfo(String URL, String postData) {

		JSONParser jParser = new JSONParser();
		ArrayList<UserPubInfo> temp = new ArrayList<UserPubInfo>();
		String json = jParser.requestPost(URL, postData);
		try {
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("Person");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);

				String ID = c.getString("ID");
				String NAME = c.getString("NAME");
				String IMG = c.getString("IMG");
				String COUNTRY = c.getString("COUNTRY");
				String CITY = c.getString("CITY");
				String ZIP = c.getString("ZIP");
				String ABOUT = c.getString("ABOUT");
				String NOFOllOWERS = c.getString("NOFOllOWERS");
				String NOREVIEW = c.getString("NOREVIEW");

				UserPubInfo userPubInfo = new UserPubInfo(ID, NAME, IMG,
						COUNTRY, CITY, ZIP, ABOUT, NOFOllOWERS, NOREVIEW);

				temp.add(userPubInfo);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return temp;
	}

	public ArrayList<Favorite> getFavoriteList(String postData) {

		JSONParser jParser = new JSONParser();
		ArrayList<Favorite> temp = new ArrayList<Favorite>();
		String json = jParser.requestPost(USER_FAVORITE_LIST_URL, postData);
		try {
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("Favorite");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);

				String Id = c.getString("Id");
				String RestName = c.getString("RestName");
				String BreakFast = c.getString("BreakFast");
				String SettingOutside = c.getString("SettingOutside");
				String HomeDelivery = c.getString("HomeDelivery");
				String TableReservation = c.getString("TableReservation");
				String Discount = c.getString("Discount");
				String DineIn = c.getString("DineIn");
				String Bar = c.getString("Bar");
				String AC = c.getString("AC");
				String Wifi = c.getString("Wifi");
				String LiveMusic = c.getString("LiveMusic");
				String Veg = c.getString("Veg");
				String NonVeg = c.getString("NonVeg");
				String CreditCard = c.getString("CreditCard");
				String Capacity = c.getString("Capacity");
				String Cost4Two = c.getString("Cost4Two");
				String IMG = c.getString("IMG");
				String RestLoc = c.getString("RestLoc");
				String RestDistName = c.getString("RestDistName");
				String latitude = c.getString("latitude");
				String longitude = c.getString("longitude");
				String Phone = c.getString("Phone");
				String PageNo = c.getString("PageNo");

				Favorite favorite = new Favorite(Id, RestName, BreakFast,
						SettingOutside, HomeDelivery, TableReservation,
						Discount, DineIn, Bar, AC, Wifi, LiveMusic, Veg,
						NonVeg, CreditCard, Capacity, Cost4Two, IMG, RestLoc,
						RestDistName, latitude, longitude, Phone, PageNo);

				temp.add(favorite);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return temp;
	}

	public ArrayList<Follower> getFollowerList(String postData) {

		JSONParser jParser = new JSONParser();
		ArrayList<Follower> temp = new ArrayList<Follower>();
		String json = jParser.requestPost(FOLLOWER_LIST_URL, postData);
		try {
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("followers");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);

				String ID = c.getString("ID");
				String USERID = c.getString("USERID");
				String FOLLOWERID = c.getString("FOLLOWERID");
				String FOLLOWER_NAME = c.getString("FOLLOWER_NAME");
				String SEX = c.getString("SEX");
				String TOTALFOLLOWER = c.getString("TOTALFOLLOWER");
				String DATE = c.getString("DATE");
				String IMG = c.getString("IMG");

				Follower follower = new Follower(ID, USERID, FOLLOWERID,
						FOLLOWER_NAME, SEX, TOTALFOLLOWER, DATE, IMG);

				temp.add(follower);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return temp;
	}

	public ArrayList<Follower> getFollowingList(String userid) {
		String postData = "userid=" + userid;

		JSONParser jParser = new JSONParser();
		ArrayList<Follower> temp = new ArrayList<Follower>();
		String json = jParser.requestPost(FOLLOWING_LIST_URL, postData);
		try {
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("following");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);

				String ID = c.getString("ID");
				String USERID = c.getString("USERID");
				String FOLLOWERID = c.getString("FOLLOWEDBY_ID");
				String FOLLOWER_NAME = c.getString("FOLLOWEDBY_NAME");
				String SEX = c.getString("SEX");
				String TOTALFOLLOWER = c.getString("TOTALFOLLOWER");
				String DATE = c.getString("DATE");
				String IMG = c.getString("IMG");

				Follower follower = new Follower(ID, USERID, FOLLOWERID,
						FOLLOWER_NAME, SEX, TOTALFOLLOWER, DATE, IMG);

				temp.add(follower);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return temp;
	}

	public String insertReviewLike(String revid, String userid, String comment,
			String rlike) {

		String postData = "revid=" + revid + "&userid=" + userid + "&comment="
				+ comment + "&rlike=" + rlike;

		JSONParser jParser = new JSONParser();

		String json = jParser.requestPost(REVIEW_LIKE_URL, postData);
		try {
			if (json.startsWith("Error") || json.startsWith("failed"))
				return "failed";
			else if (json.startsWith("success"))
				return "success";
			else if (json.startsWith("unlike"))
				return "unlike";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "failed";
	}

	public String updateFollow(String userid, String followerid) {

		String postData = "userid=" + userid + "&followerid=" + followerid;

		JSONParser jParser = new JSONParser();

		String json = jParser.requestPost(FOLLOWER_UPDATE_URL, postData);
		try {
			if (json.startsWith("Error"))
				return "failed";
			else if (json.startsWith("failed"))
				return "failed";
			else if (json.startsWith("success"))
				return "success";
			else if (json.startsWith("unfollow"))
				return "unfollow";

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "failed";

	}

	public ArrayList<Photo> getPhotoList(String postData, int type) {

		JSONParser jParser = new JSONParser();
		ArrayList<Photo> temp = new ArrayList<Photo>();
		String json = null;
		if (type == TYPE_RES) {
			json = jParser.requestPost(PHOTO_LIST_URL, postData);// PHOTO_LIST_URL
		} else if (type == TYPE_USER) {
			json = jParser.requestPost(USER_PHOTO_LIST_URL, postData);
		}

		if (json == null)
			return null;

		try {
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = null;
			if (type == TYPE_RES) {
				contacts = jObj.getJSONArray("resphotos");
			} else if (type == TYPE_USER) {
				contacts = jObj.getJSONArray("Userphotos");
			}

			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);

				String ID = c.getString("ID");
				String USERID = c.getString("USERID");
				String USERNAME = c.getString("USERNAME");
				String RESID = c.getString("RESID");
				String DATE = c.getString("DATE");
				String IMG = c.getString("IMG");
				String STATUS = c.getString("STATUS");
				String CAPTION = c.getString("CAPTION");
				String THUMBIMG = c.getString("THUMBIMG");

				System.out.println(IMG);

				Photo photo = new Photo(ID, USERID, USERNAME, RESID, DATE,
						STATUS, IMG, THUMBIMG, CAPTION);
				temp.add(photo);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return temp;
	}

	public ArrayList<Review> getReviewList(String postData, int type) {

		JSONParser jParser = new JSONParser();
		ArrayList<Review> temp = new ArrayList<Review>();
		String json = null;
		if (type == TYPE_RES) {
			json = jParser.requestPost(REVIEW_LIST_URL, postData);
		} else if (type == TYPE_USER) {
			json = jParser.requestPost(USER_REVIEW_LIST_URL, postData);
		}

		// if(json==null) return null;

		try {
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = null;
			if (type == TYPE_RES) {
				contacts = jObj.getJSONArray("reviews");
			} else if (type == TYPE_USER) {
				contacts = jObj.getJSONArray("reviewsUser");
			}

			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);

				String ID = c.getString("ID");
				String USERID = c.getString("USERID");
				String FULLNAME = c.getString("FULLNAME");
				String IMG = c.getString("IMG");
				String NO_OF_FOLLOWERS = c.getString("NO_OF_FOLLOWERS");
				String REVIEW = c.getString("REVIEW");
				String NO_OF_COMMENTS = c.getString("NO_OF_COMMENTS");
				String NO_OF_REVIEW = c.getString("NO_OF_REVIEW");
				String NO_OF_REVIEW_LIKE = c.getString("NO_OF_REVIEW_LIKE");
				String RESID = c.getString("RESID");
				String RESNAME = c.getString("RESNAME");
				String RESIMG = c.getString("RESIMG");
				String RATING = c.getString("RATING");
				String DATE = c.getString("DATE");
				String STATUS = c.getString("STATUS");
				String DATE_DIFF = c.getString("DATE_DIFF");
				String FOLLOW = c.getString("FOLLOW");

				Review review = new Review(ID, USERID, FULLNAME, IMG,
						NO_OF_FOLLOWERS, REVIEW, NO_OF_COMMENTS, NO_OF_REVIEW,
						NO_OF_REVIEW_LIKE, RESID, RESNAME, RESIMG, RATING,
						DATE, STATUS, DATE_DIFF, FOLLOW);
				temp.add(review);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return temp;
	}

	public String insertReview(String userid, String comment, String resid,
			String rating) {

		String postData = "userid=" + userid + "&comment=" + comment
				+ "&resid=" + resid + "&rating=" + rating;

		JSONParser jParser = new JSONParser();

		String json = jParser.requestPost(REVIEW_INSERT_URL, postData);
		try {
			if (json.startsWith("Error"))
				return "failed";
			else if (json.startsWith("failed"))
				return "failed";
			else if (json.startsWith("success"))
				return "success";

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "failed";

	}

	public String changePassword(User user) {

		String postData = "id=" + user.ID + "&username=" + user.USERNAME
				+ "&pwd=" + user.PASSWORD;

		JSONParser jParser = new JSONParser();

		String json = jParser.requestPost(PROFILE_EDIT_URL, postData);
		try {
			if (json.startsWith("Error"))
				return "failed";
			else if (json.startsWith("failed"))
				return "failed";
			else if (json.startsWith("success"))
				return "success";
			else if (json.startsWith("NotFound"))
				return "NotFound";

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "failed";
	}

	public String editProfile(User user) {

		String postData = "id=" + user.ID + "&username=" + user.USERNAME
				+ "&name=" + user.NAME + "&email=" + user.EMAIL + "&about="
				+ user.ABOUT + "&phone=" + user.PHONE + "&gender=" + user.SEX
				+ "&country=" + user.COUNTRY + "&city=" + user.CITY + "&zip="
				+ user.ZIP
				+ "&FavRes=" + user.FavRes
				+ "&FavFood=" +user.FavFood
				+ "&FavCus=" +user.FavCus
				+ "&NoResVisit=" +user.NoResVisit
				+ "&VisitWith=" +user.VisitWith
				+ "&VisitTime=" +user.VisitTime;
		JSONParser jParser = new JSONParser();

		String json = jParser.requestPost(PROFILE_EDIT_URL, postData);
		try {
			if (json.startsWith("Error"))
				return "failed";
			else if (json.startsWith("failed"))
				return "failed";
			else if (json.startsWith("success"))
				return "success";
			else if (json.startsWith("NotFound"))
				return "NotFound";

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "failed";
	}

	public String insertProfile(String photoname, String username, String pwd,
			String name, String email, String phone, String gender,
			String country, String city, String zip, String about,
			String usertype) {

		String postData = "snphoto=" + photoname + "&username=" + username
				+ "&pwd=" + pwd + "&name=" + name + "&email=" + email
				+ "&phone=" + phone + "&gender=" + gender + "&country="
				+ country + "&city=" + city + "&zip=" + zip + "&about=" + about
				+ "&usertype=" + usertype;

		JSONParser jParser = new JSONParser();

		String json = jParser.requestPost(PROFILE_INSERT_URL, postData);
		try {
			if (json.startsWith("Error"))
				return "failed";
			else if (json.startsWith("failed"))
				return "failed";
			else if (json.startsWith("success"))
				return "success";
			else if (json.startsWith("SimiralExists"))
				return "SimiralExists";

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "failed";

	}

	public static final int DELETE_TYPE_PROFILE = 0;
	public static final int DELETE_TYPE_RESTAURENT = 1;

	public String deletePhoto(String id, int deleteType) {
		String postData = "";

		if (deleteType == DELETE_TYPE_PROFILE)
			postData = "username=" + id;
		else if (deleteType == DELETE_TYPE_RESTAURENT)
			postData = "photoid=" + id;

		JSONParser jParser = new JSONParser();

		String json = jParser.requestPost(DELETE_PHOTOT_URL, postData);
		try {
			if (json.startsWith("Error") || json.startsWith("failed"))
				return "failed";
			else if (json.startsWith("success"))
				return "success";
			else
				return "failed";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "failed";
	}

	public ArrayList<User> loginCheck(String userName, String password) {

		String postData = "username=" + userName + "&pwd=" + password;
		JSONParser jParser = new JSONParser();
		ArrayList<User> temp = new ArrayList<User>();
		String json = jParser.requestPost(PROFILE_CHECK_URL, postData);

		System.out.println("fromMethod: " + json);

		try {
			Log.e("user",
					"=============================================================");
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("Person");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);

				String ID = c.getString("ID");
				String USERNAME = c.getString("USERNAME");
				String PASSWORD = c.getString("PASSWORD");
				String NAME = c.getString("NAME");
				String SEX = c.getString("SEX");
				String IMG = c.getString("IMG") == null ? "" : c
						.getString("IMG");
				String EMAIL = c.getString("EMAIL");
				String COUNTRY = c.getString("COUNTRY");
				String CITY = c.getString("CITY");
				String ZIP = c.getString("ZIP");
				String DATE = c.getString("DATE");
				String STATUS = c.getString("STATUS");
				String PHONE = c.getString("PHONE");
				String ABOUT = c.getString("ABOUT");
				String USERTYPE = c.getString("USERTYPE");
				String FavRes = c.getString("FAVRES");
				String FavFood = c.getString("FAVFOOD");
				String FavCus = c.getString("FAVCUS");
				String NoResVisit = c.getString("NORESVISIT");
				String VisitWith = c.getString("VISITWITH");
				String VisitTime = c.getString("VISITTIME");
				
				

				User user = new User(ID, USERNAME, PASSWORD, NAME, SEX, IMG,
						EMAIL, COUNTRY, CITY, ZIP, DATE, STATUS, PHONE, ABOUT,
						USERTYPE, FavRes, FavFood, FavCus, NoResVisit, VisitWith, VisitTime);

				temp.add(user);

				Log.e("Id", ID);
				Log.e("Name", NAME);
				Log.e("IMG", IMG);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return temp;
	}

	public ArrayList<SearchResturent> search(String postData) {
		System.out.println(postData);
		JSONParser jParser = new JSONParser();
		ArrayList<SearchResturent> temp = new ArrayList<SearchResturent>();
		String json = jParser.requestPost(RESTURENT_SEARCH_URL, postData);
		try {
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("RestaurantList");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);
				String Id = c.getString("Id");
				String Name = c.getString("Name");
				String Address = c.getString("Address");
				String Bestdish = c.getString("Bestdish");
				String latitude = c.getString("latitude");
				String longitude = c.getString("longitude");

				String IMG = "";
				if (c.has("IMG"))
					IMG = c.getString("IMG") == null ? "" : c.getString("IMG");

				Log.e("Id", Id);
				Log.e("Name", Name);
				Log.e("Address", Address);
				Log.e("Bestdish", Bestdish);
				Log.e("latitude", latitude);
				Log.e("longitude", longitude);
				Log.e("IMG", IMG);

				SearchResturent nn = new SearchResturent(Id, Name, Bestdish,
						Address, latitude, longitude, IMG);
				temp.add(nn);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return temp;
	}

	public ArrayList<SearchResturent> searchAdv(String Search,

	String Cuisine, String Location, String Cost2p, String Capacity,
			String CreditC, String Wifi, String HomeDelivery, String OUTSeat,
			String DineIn, String TRR, String BFast, String Bar, String Buffet,
			String Veg, String NVeg, String AC, String Kidz, String Smoking,
			String LiveMusic) {

		String postData = "";

		postData += (!Search.equals("")) ? "Search=" + Search + "&" : "";
		postData += (!Location.equals("")) ? "Location=" + Location + "&" : "";
		postData += Cuisine.equals("Yes") ? "Cuisine=" + Cuisine + "&" : "";
		postData += Cost2p.equals("Yes") ? "Cost2p=" + Cost2p + "&" : "";
		postData += Capacity.equals("Yes") ? "Capacity=" + Capacity + "&" : "";
		postData += CreditC.equals("Yes") ? "CreditC=" + CreditC + "&" : "";
		postData += Wifi.equals("Yes") ? "Wifi=" + Wifi + "&" : "";
		postData += HomeDelivery.equals("Yes") ? "HomeDelivery=" + HomeDelivery
				+ "&" : "";
		postData += OUTSeat.equals("Yes") ? "OUTSeat=" + OUTSeat + "&" : "";
		postData += DineIn.equals("Yes") ? "DineIn=" + DineIn + "&" : "";
		postData += TRR.equals("Yes") ? "TRR=" + TRR + "&" : "";
		postData += BFast.equals("Yes") ? "BFast=" + BFast + "&" : "";
		postData += Bar.equals("Yes") ? "Bar=" + Bar + "&" : "";
		postData += Buffet.equals("Yes") ? "Buffet=" + Buffet + "&" : "";
		postData += Veg.equals("Yes") ? "Veg=" + Veg + "&" : "";
		postData += NVeg.equals("Yes") ? "NVeg=" + NVeg + "&" : "";
		postData += AC.equals("Yes") ? "AC=" + AC + "&" : "";
		postData += Kidz.equals("Yes") ? "Kidz=" + Kidz + "&" : "";
		postData += Smoking.equals("Yes") ? "Smoking=" + Smoking + "&" : "";
		postData += LiveMusic.equals("Yes") ? "LiveM=" + LiveMusic + "&" : "";

		// char c= postData.charAt((postData.length()-1));

		JSONParser jParser = new JSONParser();
		ArrayList<SearchResturent> temp = new ArrayList<SearchResturent>();
		String json = jParser.requestPost(RESTURENT_ADV_SEARCH_URL, postData);
		try {
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("RestaurantList");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);
				String Id = c.getString("Id");
				String Name = c.getString("Name");
				String Address = c.getString("Address");
				String Bestdish = c.getString("Bestdish");
				String latitude = c.getString("latitude");
				String longitude = c.getString("longitude");

				String IMG = "";
				if (c.has("IMG"))
					IMG = c.getString("IMG") == null ? "" : c.getString("IMG");

				SearchResturent nn = new SearchResturent(Id, Name, Bestdish,
						Address, latitude, longitude, IMG);
				temp.add(nn);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return temp;
	}

	public ArrayList<ResturentDetails> getResturentDetails(String json) {

		
		ArrayList<ResturentDetails> temp = new ArrayList<ResturentDetails>();
		ArrayList<Card> tempCards = new ArrayList<Card>();
		
		try {
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("restaurentdetails");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(0);
				String Id = c.getString("Id");
				String Name = c.getString("Name");
				String HomeDelivery = c.getString("HomeDelivery");
				String Discount = c.getString("Discount");
				// CARDS
				JSONArray jsoncard = c.getJSONArray("CARDS");
				for (int j = 0; j < jsoncard.length(); j++) {
					JSONObject cc = jsoncard.getJSONObject(j);
					String disId = cc.getString("DisId");
					String resId = cc.getString("ResId");
					String disName = cc.getString("DisName");
					String disCompany = cc.getString("DisCompany");
					String disPercent = cc.getString("DisPercent");
					String disValid = cc.getString("DisValid");
					String IsHighlighted = cc.getString("IsHighlighted");
					String DisDetail = cc.getString("DisDetail");
					String IsLetsEat = cc.getString("IsLetsEat");
					String DealsBookUrl = cc.getString("DealsBookUrl");
					String DisBrief = cc.getString("DisBrief");

					Card card = new Card(disId, resId, disName, disCompany,
							disPercent, disValid, IsHighlighted, DisDetail, IsLetsEat, DealsBookUrl, DisBrief);
					tempCards.add(card);
					Log.e("discount",
							"=========================================================="
									+ disName);
				}

				String Address = c.getString("Address");
				String Open = c.getString("Open");
				String Close = c.getString("Close");
				String phone = c.getString("phone");
				String Price = c.getString("Price");
				String latitude = c.getString("latitude");
				String longitude = c.getString("longitude");
				String IMG = c.getString("IMG") == null ? "" : c
						.getString("IMG");
				String cusine = c.getString("cusine");
				String rateing = c.getString("rateing");
				String RestType = c.getString("RestType");

				String BreakFast = c.getString("BreakFast");
				String SettingOutside = c.getString("SettingOutside");
				String TableReservation = c.getString("TableReservation");
				String DineIn = c.getString("DineIn");
				String Bar = c.getString("Bar");
				String AC = c.getString("AC");
				String Wifi = c.getString("Wifi");
				String LiveMusic = c.getString("LiveMusic");
				String Veg = c.getString("Veg");
				String NonVeg = c.getString("NonVeg");
				String CreditCard = c.getString("CreditCard");
				String SeatingCapacity = c.getString("SeatingCapacity");
				String Cost4Two = c.getString("Cost4Two");
				String Kidz = c.getString("Kidz");
				String Smoking = c.getString("Smoking");
				String Buffet = c.getString("Buffet");

				Log.e("Id", Id);
				Log.e("Name", Name);
				Log.e("Address", Address);
				Log.e("latitude", latitude);
				Log.e("longitude", longitude);
				Log.e("IMG", IMG);

				// ResturentDetails nn = new ResturentDetails(Id, Name, Address,
				// Open, phone, Price, latitude, longitude, IMG, cusine,
				// rateing);
				// ResturentDetails nn = new ResturentDetails(Id,
				// Name,HomeDelivery, Discount, tempCards, Address, Open, phone,
				// Price, latitude, longitude, IMG, cusine, rateing, RestType);
				ResturentDetails nn = new ResturentDetails(Id, Name,
						HomeDelivery, Discount, tempCards, Address, Open,
						Close, phone, Price, latitude, longitude, IMG, cusine,
						rateing, RestType, BreakFast, SettingOutside,
						TableReservation, DineIn, Bar, AC, Wifi, LiveMusic,
						Veg, NonVeg, CreditCard, SeatingCapacity, Cost4Two,
						Kidz, Smoking, Buffet);
				temp.add(nn);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return temp;
	}

	public ArrayList<ResturentFeatured> getFeaturedResturentDetails(
			String json) {

		ArrayList<ResturentFeatured> temp = new ArrayList<ResturentFeatured>();

		ArrayList<String> tempMenu = new ArrayList<String>();
		try {
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("restaurent");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);

				String Id = c.getString("Id");
				String RestName = c.getString("RestName");
				String BreakFast = c.getString("BreakFast");
				String SettingOutside = c.getString("SettingOutside");
				String HomeDelivery = c.getString("HomeDelivery");
				String TableReservation = c.getString("TableReservation");
				String Discount = c.getString("Discount");
				String DineIn = c.getString("DineIn");
				String Bar = c.getString("Bar");
				String AC = c.getString("AC");
				String Wifi = c.getString("Wifi");
				String LiveMusic = c.getString("LiveMusic");
				String Veg = c.getString("Veg");
				String NonVeg = c.getString("NonVeg");
				String CreditCard = c.getString("CreditCard");
				String Capacity = c.getString("Capacity");
				String Cost4Two = c.getString("Cost4Two");
				String RestLoc = c.getString("RestLoc");
				String RestDistName = c.getString("RestDistName");
				String latitude = c.getString("latitude");
				String longitude = c.getString("longitude");
				String Phone = c.getString("Phone");
				String IMGres = c.getString("IMG");
				String Cusine = c.getString("Cusine");
				String DisAmount = c.getString("DisAmount");

				// MENU
				JSONArray jsonMenu = c.getJSONArray("MENU");
				for (int j = 0; j < jsonMenu.length(); j++) {
					JSONObject cc = jsonMenu.getJSONObject(j);
					String IMG = cc.getString("IMG");

					tempMenu.add(IMG);
					Log.e("menu img",
							"=========================================================="
									+ IMG);
				}

				ResturentFeatured nn = new ResturentFeatured(Id, RestName,
						BreakFast, SettingOutside, HomeDelivery,
						TableReservation, Discount, DineIn, Bar, AC, Wifi,
						LiveMusic, Veg, NonVeg, CreditCard, Capacity, Cost4Two,
						RestLoc, RestDistName, latitude, longitude, Phone,
						IMGres, Cusine, DisAmount, tempMenu);
				temp.add(nn);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return temp;
	}

	public ArrayList<Menu> getMenu(String postData) {

		JSONParser jParser = new JSONParser();
		ArrayList<Menu> temp = new ArrayList<Menu>();
		String json = jParser.requestPost(MENU_URL, postData);
		try {
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("restmenu");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);

				String IMG = c.getString("IMG");
				String MenuName = c.getString("MenuName");
				String Price = c.getString("Price");
				String FoodCat = c.getString("FoodCat");
				String ThumbIMG = c.getString("ThumbIMG");

				Menu nn = new Menu(MenuName, Price, FoodCat, IMG, ThumbIMG);
				temp.add(nn);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return temp;
	}

	ArrayList<MenuMainCategory> menuMainCategories ;//= new ArrayList<MenuMainCategory>();
	public ArrayList<MenuMainCategory> getMenuMainCattegory(String postData) {

		JSONParser jParser = new JSONParser();
		menuMainCategories = new ArrayList<MenuMainCategory>();
		String json = postData;//jParser.requestPost(MENU_URL, postData);
		try {
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("newrestmenu");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);

				String MAINCATNAME = c.getString("MAINCATNAME");





					//ITEM
					ArrayList<MenuItem> menuItems = new ArrayList<MenuItem>();
				JSONArray ITEM = c.getJSONArray("ITEM");
					for (int k = 0; k < ITEM.length(); k++) {

						JSONObject items = ITEM.getJSONObject(k);
						 String ItemId = items.getString("ItemId");
						 String ItemName= items.getString("ItemName");
						 String ItemPrice = items.getString("ItemPrice");
						 String ItemDesc = items.getString("ItemDesc");
						 String ServSize = items.getString("ServSize");
						 String ServTime = items.getString("ServTime");
						 String UnitType = items.getString("UnitType");
						 String Ingradients = items.getString("Ingradients");
						 String Currency = items.getString("Currency");
						 String IMG = items.getString("IMG");
						 String ThumbIMG = items.getString("ThumbIMG");

						MenuItem mi = new MenuItem( ItemId,
								 ItemName,
								 ItemPrice,
								 ItemDesc,
								 ServSize,
								 ServTime,
								 UnitType,
								 Ingradients,
								 Currency,
								 IMG,
								 ThumbIMG);
						menuItems.add(mi);

					}





				MenuMainCategory nn = new MenuMainCategory(MAINCATNAME, menuItems);
				menuMainCategories.add(nn);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return menuMainCategories;
	}

	///==================

	 ;//= new ArrayList<MenuMainCategory>();
	public ArrayList<Table> getTables(String postData) {

		JSONParser jParser = new JSONParser();
		ArrayList<Table> menuMainCategories = new ArrayList<Table>();
		String json = postData;//jParser.requestPost(MENU_URL, postData);
		try {
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("tablelist");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);

				String TableName = c.getString("TableName");
				String TCap = c.getString("TCap");

				//ITEM
				ArrayList<Slot> slots = new ArrayList<Slot>();
				JSONArray ITEM = c.getJSONArray("Slots");
				for (int k = 0; k < ITEM.length(); k++) {

					JSONObject items = ITEM.getJSONObject(k);
					String SlSart = items.getString("SlSart");
					String SLEnd= items.getString("SLEnd");
					String BStatus = items.getString("BStatus");

					Slot mi = new Slot(SlSart, SLEnd, BStatus);
					slots.add(mi);

				}





				Table nn = new Table(TableName, TCap,  slots);
				menuMainCategories.add(nn);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return menuMainCategories;
	}
	
	

	public ArrayList<Restaurent> getRsturent(String json) {
		
		ArrayList<Restaurent> temp = new ArrayList<Restaurent>();
		
		try {
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("restaurent");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);
				/*String id = c.getString("Id");
				String restName = c.getString("RestName");
				String restLoc = c.getString("RestLoc");
				String restDistName = c.getString("RestDistName");
				String latitude = c.getString("latitude");
				String longitude = c.getString("longitude");
				String phone = c.getString("Phone");
				String pageNo = c.getString("PageNo");
				String IMG = c.getString("IMG");
				String Rating = c.getString("Rating");
				String Open = c.getString("Open");
				String Close = c.getString("Close");
				String Wifi = c.getString("Wifi");
				String LiveMusic = c.getString("LiveMusic");*/
				
				String Id = c.getString("Id");
	            String RestName = c.getString("RestName");
	            String Address = c.getString("Address");
	            String IMG = c.getString("IMG");
	            String Rating = c.getString("Rating");
	            String BreakFast = c.getString("BreakFast");
	            String Open = c.getString("Open");
	            String Close = c.getString("Close");
	            String SettingOutside = c.getString("SettingOutside");
	            String HomeDelivery = c.getString("HomeDelivery");
	            String TableReservation = c.getString("TableReservation");
	            String Discount = c.getString("Discount");
	            String DineIn = c.getString("DineIn");
	            String Bar = c.getString("Bar");
	            String AC = c.getString("AC");
	            String Wifi = c.getString("Wifi");
	            String LiveMusic = c.getString("LiveMusic");
	            String Veg = c.getString("Veg");
	            String NonVeg = c.getString("NonVeg");
	            String CreditCard = c.getString("CreditCard");
	            String Capacity = c.getString("Capacity");
	            String Cost4Two = c.getString("Cost4Two");
	            String RestLoc = c.getString("RestLoc");
	            String latitude = c.getString("latitude");
	            String longitude = c.getString("longitude");
	            String Phone = c.getString("Phone");
	            String PageNo = c.getString("PageNo");

				/*Restaurent nn = new Restaurent(id, restName, restLoc,
						restDistName, latitude, longitude, phone, pageNo, IMG,
						Rating, Open, Close, Wifi, LiveMusic);*/
	            Restaurent nn = new Restaurent(Id, RestName, RestLoc, "", latitude, longitude, Phone, PageNo, IMG, Rating, Open, Close, Wifi, LiveMusic, Address, BreakFast, SettingOutside, HomeDelivery, TableReservation, Discount, DineIn, Bar, AC, Veg, NonVeg, CreditCard, Capacity, Cost4Two);
				temp.add(nn);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return temp;
	}

	public ArrayList<Category> getCategory() {

		JSONParser jParser = new JSONParser();
		ArrayList<Category> temp = new ArrayList<Category>();
		String json = jParser.getJSONFromUrl(CATEGORY_URL);// (CATEGORY_URL,"125");

		try {
			if (json.startsWith("Error"))
				return null;
			System.out.println("foodcategory");
			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("foodcategory");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);
				String Id = c.getString("Id");
				String categoryname = c.getString("categoryname");
				String IMG = c.getString("IMG");

				System.out.println("foodcategory");

				Log.e("Id", Id);
				Log.e("categoryname", categoryname);
				Log.e("IMG", IMG);

				Category nn = new Category(Id, categoryname, IMG);
				temp.add(nn);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return temp;
	}

	public ArrayList<Cusine> getCusine(String json) {

		JSONParser jParser = new JSONParser();
		ArrayList<Cusine> temp = new ArrayList<Cusine>();
		// String json = jParser.getJSONFromUrl(CUSINE_URL);//
		
		/*String postData = "";
		if(Type==TYPE_LOAD_ZONE){
			 postData = "ZoneId=" + LocId;
		}else{
			 postData = "LocId=" + LocId;
		}
			
		String json = "Error";// jParser.requestPost(CUSINE_URL, postData);
*/
		

		try {
			if (json.startsWith("Error"))
				return null;
			System.out.println("foodcusine");
			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("foodcusine");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);
				String Id = c.getString("Id");
				String cusinename = c.getString("cusinename");
				String IMG = c.getString("IMG");

				System.out.println("foodcusine");

				Log.e("Id", Id);
				Log.e("categoryname", cusinename);

				Cusine nn = new Cusine(Id, cusinename, IMG);
				temp.add(nn);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		
		return temp;
	}

	public ArrayList<CategoryMenu> getCategoryMenu(String postData) {

		JSONParser jParser = new JSONParser();
		ArrayList<CategoryMenu> temp = new ArrayList<CategoryMenu>();
		String json = jParser.requestPost(CATEGORY_MENU_URL, postData);

		try {
			if (json.startsWith("Error"))
				return null;
			System.out.println("foodcatmenulist");
			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("foodcatmenulist");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);

				String Id = c.getString("Id");
				String menuname = c.getString("menuname");
				String restname = c.getString("restname");
				String menuprice = c.getString("menuprice");

				CategoryMenu nn = new CategoryMenu(Id, menuname, restname,
						menuprice);
				temp.add(nn);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return temp;
	}

	public ArrayList<CusineMenu> getCusineMenu(String  json) {
		

		JSONParser jParser = new JSONParser();
		ArrayList<CusineMenu> temp = new ArrayList<CusineMenu>();
		

		

		try {
			if (json.startsWith("Error"))
				return null;
			System.out.println("cusinerestlist");
			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("cusinerestlist");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);

				String Id = c.getString("Id");
				String restaurent = c.getString("restaurent");
				String RestLoc = c.getString("RestLoc");
				String latitude = c.getString("latitude");
				String longitude = c.getString("longitude");
				String Phone = c.getString("Phone");
				String RestDistName = c.getString("RestDistName");
				String HomeDelivery = c.getString("HomeDelivery");		    
			    String IMG = c.getString("IMG");
			    String SeatingCapacity = c.getString("SeatingCapacity");
			    String Cost4Two = c.getString("Cost4Two");
			    String Discount = c.getString("Discount");
			    String Address = c.getString("Address");
			    String Open = c.getString("Open");
			    String Close = c.getString("Close");
			    String Rating = c.getString("Rating");

				CusineMenu nn = new CusineMenu(Id, restaurent, RestLoc,
						RestDistName, latitude, longitude, Phone, HomeDelivery, IMG, SeatingCapacity, Cost4Two, Discount, Address, Open,  Close, Rating);
				temp.add(nn);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return temp;
	}

	public ArrayList<Zone> getZones(String DisID, Activity activity) {
		String postData = "DisId=" + DisID;

		JSONParser jParser = new JSONParser();
		ArrayList<Zone> temp = new ArrayList<Zone>();
		String json = "Error";// jParser.requestPost(LOCATION_LIST_URL,
								// postData);
		SavedPrefernce sp = null;
		if (activity != null) {
			sp = new SavedPrefernce(activity);
			if (sp.isZoneExpire(DisID)) {
				json = jParser.requestPost(ZONE_LIST_URL, postData);

				// Log.e("", "loc Load from service");
			} else {
				json = sp.getZones(DisID);
				// Log.e("", "loc Load from cache");
			}
		}

		try {
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("zone");
			for (int i = 0; i < contacts.length(); i++) {
			
				
				JSONObject c = contacts.getJSONObject(i);
				String ZoneId = c.getString("ZoneId");
				String DisId = c.getString("DisId");
				String LocName = c.getString("LocName");
				String ZoneName = c.getString("ZoneName");
				String Lat = c.getString("Lat");
				String Lng = c.getString("Lng");
				Zone nn = new Zone(ZoneId, DisId, ZoneName, LocName, Lat, Lng);
				temp.add(nn);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		if (sp != null && temp.size() > 0) {
			sp.saveZones(json, DisID);
			sp.setZoneDataExpirDate(DisID);
		}

		return temp;
	}
	
	
	public ArrayList<District> getDistricts(Activity activity) {
		
		JSONParser jParser = new JSONParser();
		ArrayList<District> temp = new ArrayList<District>();
		String json = "Error";// jParser.getJSONFromUrl(DISTRICT_LIST_URL);
		SavedPrefernce sp = null;
		if (activity != null) {
			sp = new SavedPrefernce(activity);
			//if (sp.isCityExpire()) {
			json = jParser.getJSONFromUrl(DISTRICT_LIST_URL);
			
			// Log.e("", "Load from service");
			/*} else {
				json = sp.getCities();
				// Log.e("", "Load from cache>"+json+"<");

			}*/
		}
		
		try {
			if (json.startsWith("Error"))
				return null;
			
			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("district");
			for (int i = 0; i < contacts.length(); i++) {
				
				JSONObject c = contacts.getJSONObject(i);
				String DisId = c.getString("DisId");
				String DivId = c.getString("DivId");
				String DisName = c.getString("DisName");
				String IsComp = c.getString("IsComp");
				String Status = c.getString("Status");
				String Lat = c.getString("Lat");
				String Lng = c.getString("Lng");
				
				District nn = new District(DisId, DivId, DisName, IsComp,
						Status, Lat, Lng);
				temp.add(nn);
				
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		if (sp != null && temp.size() > 0) {
			sp.saveCities(json);
			sp.setCityDataExpirDate();
		}
		
		return temp;
	}
	
public ArrayList<DistrictLocZone> getDistrictsLocZone(String json) {
		
		JSONParser jParser = new JSONParser();
		ArrayList<DistrictLocZone> temp = new ArrayList<DistrictLocZone>();
		
		
		try {
			if (json.startsWith("Error"))
				return null;
			
			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("district");
			for (int i = 0; i < contacts.length(); i++) {
				
				JSONObject c = contacts.getJSONObject(i);
				String DisId = c.getString("DisId");
				String DivId = c.getString("DivId");
				String DisName = c.getString("DisName");
				String IsComp = c.getString("IsComp");
				String Status = c.getString("Status");
				String Lat = c.getString("Lat");
				String Lng = c.getString("Lng");
				
				//loc
				ArrayList<Location> tempLoc = new ArrayList<Location>();
				JSONArray locArr = c.getJSONArray("LOCATION");
				for (int j = 0; j < locArr.length(); j++) {

					JSONObject loc = locArr.getJSONObject(j);
					String LocId = loc.getString("LocId");
					String DisId_ = loc.getString("DisId");
					String LocName = loc.getString("LocName");
					String Lat_ = loc.getString("Lat");
					String Lng_ = loc.getString("Lng");
					Location nn = new Location(LocId, DisId_, LocName, Lat_, Lng_);
					tempLoc.add(nn);

				}
				
				
				DistrictLocZone nn = new DistrictLocZone(DisId, DivId, DisName, IsComp,
						Status, Lat, Lng, tempLoc, null);
				temp.add(nn);
				
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		
		return temp;
	}





	public ArrayList<Location> getLocation(String DisIdParam, Activity activity) {
		String postData = "DisId=" + DisIdParam;

		JSONParser jParser = new JSONParser();
		ArrayList<Location> temp = new ArrayList<Location>();
		String json = "Error";// jParser.requestPost(LOCATION_LIST_URL,
								// postData);
		SavedPrefernce sp = null;
		if (activity != null) {
			sp = new SavedPrefernce(activity);
			if (sp.isLocExpire(DisIdParam)) {
				json = jParser.requestPost(LOCATION_LIST_URL, postData);

				// Log.e("", "loc Load from service");
			} else {
				json = sp.getLocs(DisIdParam);
				// Log.e("", "loc Load from cache");
			}
		}

		try {
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("location");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);
				String LocId = c.getString("LocId");
				String DisId = c.getString("DisId");
				String LocName = c.getString("LocName");
				String Lat = c.getString("Lat");
				String Lng = c.getString("Lng");
				Location nn = new Location(LocId, DisId, LocName, Lat, Lng);
				temp.add(nn);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		if (sp != null && temp.size() > 0) {
			sp.saveLocs(json, DisIdParam);
			sp.setLocDataExpirDate(DisIdParam);
		}

		return temp;
	}
	
	String loadType(int type){
		
		 if(type==TYPE_LOAD_ZONE)
			return  "ZoneId";
		 else
				return "LocId";
	}

	public ArrayList<Offer> getOffer(String json) {

		

		JSONParser jParser = new JSONParser();
		ArrayList<Offer> temp = new ArrayList<Offer>();
		
		
		try {
			if (json.startsWith("Error"))
				return null;

			JSONObject jObj = new JSONObject(json);
			JSONArray contacts = jObj.getJSONArray("restaurent");
			for (int i = 0; i < contacts.length(); i++) {

				JSONObject c = contacts.getJSONObject(i);

				String id = c.getString("Id");
				String restName = c.getString("RestName");
				String disAmount = c.getString("DisAmount");
				String homeDelivery = c.getString("HomeDelivery");
				String Discount = c.getString("Discount");
			    String Capacity = c.getString("Capacity");
			    String Cost4Two = c.getString("Cost4Two"); 
				String restLoc = c.getString("RestLoc");
				String restDistName = c.getString("RestDistName");
				String latitude = c.getString("latitude");
				String longitude = c.getString("longitude");
				String phone = c.getString("Phone");
				String pageNo = c.getString("PageNo");
				String IMG = c.getString("IMG");
				String DisFrom = c.getString("DisFrom");
				String cusine = c.getString("cusine");

				Offer nn = new Offer(id, restName, disAmount, homeDelivery, Discount, Capacity, Cost4Two,
						restLoc, restDistName, latitude, longitude, phone,
						pageNo, IMG, DisFrom, cusine);
				temp.add(nn);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		
		return temp;
	}

}
