package com.foody.jsondata;

import java.io.Serializable;

/**
 * json
 */
public class Card implements Serializable {

	private static final long serialVersionUID = 3350259719054343796L;

	public String DisId;
	public String ResId;
	public String DisName;
	public String DisCompany;
	public String DisPercent;
	public String DisValid;
	public String IsHighlighted;
	public String DisDetail;
	public String IsLetsEat;
	public String DealsBookUrl;
	public String DisBrief;
	

	public Card(String disId, String resId, String disName, String disCompany,
			String disPercent, String disValid, String IsHighlighted,
			String DisDetail,String IsLetsEat, String DealsBookUrl, String DisBrief) {
		super();
		DisId = disId;
		ResId = resId;
		DisName = disName;
		DisCompany = disCompany;
		DisPercent = disPercent;
		DisValid = disValid;
		this.IsHighlighted = IsHighlighted;
		this.DisDetail = DisDetail;
		this.IsLetsEat = IsLetsEat;
		this.DealsBookUrl = DealsBookUrl;
		this.DisBrief = DisBrief;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "C" + DisCompany + " " + DisName + " " + DisPercent + " "
				+ DisValid;
	}

}
