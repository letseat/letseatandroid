package com.foody.jsondata;

/**
 * json
 */

public class Cusine {

	public String Id;
	public String cusinename;
	public String IMG;
	public String description;
	private boolean selected;

	// {"Id":"1","cusinename":"Bangla"}

	/*
	 * public Cusine(String Id, String categoryname){
	 * 
	 * this.Id=Id; this.cusinename=categoryname; }
	 */

	public Cusine(String Id, String categoryname, String IMG) {

		this.Id = Id;
		this.cusinename = categoryname;
		this.IMG = IMG;
	}
	
	public Cusine(String Id, String categoryname, String IMG, String description, boolean selected ) {
		
		this.Id = Id;
		this.cusinename = categoryname;
		this.IMG = IMG;
		this.description = description;
		this.selected= selected;
	}
	
	public boolean isSelected() {
        return selected;
    }
 
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}
