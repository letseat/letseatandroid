package com.foody.jsondata;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * json
 */
public class ResturentDetails implements Serializable  {

	public String Id;
	public String Name;
	public String HomeDelivery;
	public String Discount;
	public ArrayList<Card> Cards;
	public String Address;
	public String Open;
	public String Close;
	public String phone;
	public String Price;
	public String latitude;
	public String longitude;
	public String IMG;
	public String cusine;
	public String rateing;
	public String RestType;
	
	public String BreakFast;
	public String SettingOutside;
	public String TableReservation;
	public String DineIn;
	public String Bar;
	public String AC;
	public String Wifi;
	public String LiveMusic;
	public String Veg;
	public String NonVeg;
	public String CreditCard;
	public String SeatingCapacity;
	public String Cost4Two;
	public String Kidz;
	public String Smoking;
	public String Buffet;
	
	
	public ArrayList<String> menu;
	
	
	
	
	
	

	public ResturentDetails(String id, String name, String homeDelivery,
			String discount, ArrayList<Card> cards, String address,
			String open, String close, String phone, String price, String latitude,
			String longitude, String iMG, String cusine, String rateing,
			String restType, String breakFast, String settingOutside,
			String tableReservation, String dineIn, String bar, String aC,
			String wifi, String liveMusic, String veg, String nonVeg,
			String creditCard, String seatingCapacity, String cost4Two, String Kidz,
			String Smoking, String Buffet) {
		super();
		Id = id;
		Name = name;
		HomeDelivery = homeDelivery;
		Discount = discount;
		Cards = cards;
		Address = address;
		Open = open;
		this.Close=close;
		this.phone = phone;
		Price = price;
		this.latitude = latitude;
		this.longitude = longitude;
		IMG = iMG;
		this.cusine = cusine;
		this.rateing = rateing;
		RestType = restType;
		BreakFast = breakFast;
		SettingOutside = settingOutside;
		TableReservation = tableReservation;
		DineIn = dineIn;
		Bar = bar;
		AC = aC;
		Wifi = wifi;
		LiveMusic = liveMusic;
		Veg = veg;
		NonVeg = nonVeg;
		CreditCard = creditCard;
		SeatingCapacity = seatingCapacity;
		Cost4Two = cost4Two;
		this.Kidz=Kidz;
		this.Smoking=Smoking;
		this.Buffet=Buffet;
	}

	//for special event call constructor with menu image url in  ArrayList<String> menu
	public ResturentDetails(String id, String name, String homeDelivery,
			String discount, ArrayList<Card> cards, String address,
			String open, String close, String phone, String price, String latitude,
			String longitude, String iMG, String cusine, String rateing,
			String restType, String breakFast, String settingOutside,
			String tableReservation, String dineIn, String bar, String aC,
			String wifi, String liveMusic, String veg, String nonVeg,
			String creditCard, String seatingCapacity, String cost4Two, ArrayList<String> menu) {
		super();
		Id = id;
		Name = name;
		HomeDelivery = homeDelivery;
		Discount = discount;
		Cards = cards;
		Address = address;
		Open = open;
		this.Close=close;
		this.phone = phone;
		Price = price;
		this.latitude = latitude;
		this.longitude = longitude;
		IMG = iMG;
		this.cusine = cusine;
		this.rateing = rateing;
		RestType = restType;
		BreakFast = breakFast;
		SettingOutside = settingOutside;
		TableReservation = tableReservation;
		DineIn = dineIn;
		Bar = bar;
		AC = aC;
		Wifi = wifi;
		LiveMusic = liveMusic;
		Veg = veg;
		NonVeg = nonVeg;
		CreditCard = creditCard;
		SeatingCapacity = seatingCapacity;
		Cost4Two = cost4Two;
		this.menu=menu;
	}
	
}
