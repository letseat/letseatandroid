package com.foody.jsondata;

/**
 * json
 */

public class Favorite {

	public String Id;
	public String RestName;
	public String BreakFast;
	public String SettingOutside;
	public String HomeDelivery;
	public String TableReservation;
	public String Discount;
	public String DineIn;
	public String Bar;
	public String AC;
	public String Wifi;
	public String LiveMusic;
	public String Veg;
	public String NonVeg;
	public String CreditCard;
	public String Capacity;
	public String Cost4Two;
	public String IMG;
	public String RestLoc;
	public String RestDistName;
	public String latitude;
	public String longitude;
	public String Phone;
	public String PageNo;

	/**
	 * @param iD
	 * @param uSERID
	 * @param rESID
	 * @param dATE
	 * @param Id
	 * @param RestName
	 * @param BreakFast
	 * @param SettingOutside
	 * @param HomeDelivery
	 * @param TableReservation
	 * @param Discount
	 * @param DineIn
	 * @param Bar
	 * @param aC
	 * @param Wifi
	 * @param LiveMusic
	 * @param Veg
	 * @param NonVeg
	 * @param CreditCard
	 * @param Capacity
	 * @param Cost4Two
	 * @param RestLoc
	 * @param RestDistName
	 * @param latitude
	 * @param longitude
	 * @param Phone
	 * @param PageNo
	 */
	public Favorite(String Id, String RestName, String BreakFast,
			String SettingOutside, String HomeDelivery,
			String TableReservation, String Discount, String DineIn,
			String Bar, String AC, String Wifi, String LiveMusic, String Veg,
			String NonVeg, String CreditCard, String Capacity, String Cost4Two,
			String IMG, String RestLoc, String RestDistName, String latitude,
			String longitude, String Phone, String PageNo) {
		super();

		this.Id = Id;
		this.RestName = RestName;
		this.BreakFast = BreakFast;
		this.SettingOutside = SettingOutside;
		this.HomeDelivery = HomeDelivery;
		this.TableReservation = TableReservation;
		this.Discount = Discount;
		this.DineIn = DineIn;
		this.Bar = Bar;
		this.AC = AC;
		this.Wifi = Wifi;
		this.LiveMusic = LiveMusic;
		this.Veg = Veg;
		this.NonVeg = NonVeg;
		this.CreditCard = CreditCard;
		this.Capacity = Capacity;
		this.Cost4Two = Cost4Two;
		this.IMG = IMG;
		this.RestLoc = RestLoc;
		this.RestDistName = RestDistName;
		this.latitude = latitude;
		this.longitude = longitude;
		this.Phone = Phone;
		this.PageNo = PageNo;
	}

}
