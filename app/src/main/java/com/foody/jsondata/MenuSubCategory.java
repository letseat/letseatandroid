package com.foody.jsondata;

import java.util.List;

/**
 * Created by Arif on 4/24/2016.
 */
public class MenuSubCategory {
    public String SUBCATNAME;
    public List<MenuItem> menuItemList;

    public MenuSubCategory(String SUBCATNAME, List<MenuItem> menuItemList) {
        this.SUBCATNAME = SUBCATNAME;
        this.menuItemList = menuItemList;
    }
}
