package com.foody.jsondata;



import java.io.Serializable;

import com.google.api.client.util.Key;

/**
 * json
 */
public class SearchResturent implements Serializable {

	
	private static final long serialVersionUID = 8781794565844405979L;

	@Key
	public String Id;
	
	@Key
	public String Name;
	
	@Key
	public String Bestdish;
	
	@Key
	public String Address;
	
	@Key
	public String latitude;
	
	@Key
	public String longitude;
	
	@Key
	public String IMG;
	
	public String  SeatingCapacity;
    public String  Cost4Two;
    public String  Discount;
    public String  Open;
    public String  Close;
    public String  Rating;
    public String  RestLoc;
    
    private int OpenOrClose;
	
	public SearchResturent(String Id, String Name, String Bestdish,String Address,String latitude,String longitude, String IMG, 
	 String  SeatingCapacity,
     String  Cost4Two,
     String  Discount,
     String  Open,
     String  Close,
     String  Rating,
	 String  RestLoc) {
		
		this.Id=Id;				
		this.Name=Name;		
		this.Bestdish=Bestdish;		
		this.Address=Address;
		this.latitude=latitude;
		this.longitude=longitude;
		this.IMG=IMG;
		
		this.SeatingCapacity = SeatingCapacity;
	    this.Cost4Two = Cost4Two;
	    this.Discount = Discount;
	    this.Open = Open;
	    this.Close = Close;
	    this.Rating = Rating;
	    this.RestLoc = RestLoc;
	}
	
	public SearchResturent(String Id, String Name, String Bestdish,String Address,String latitude,String longitude, String IMG) {
				
				this.Id=Id;				
				this.Name=Name;		
				this.Bestdish=Bestdish;		
				this.Address=Address;
				this.latitude=latitude;
				this.longitude=longitude;
				this.IMG=IMG;
				
			}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return Name;
	}
	
	
	public int getOpenOrClose() {
		return OpenOrClose;
	}



	public void setOpenOrClose(int openOrClose) {
		OpenOrClose = openOrClose;
	}
	
	
}
