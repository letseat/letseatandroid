package com.foody.jsondata;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arif on 4/24/2016.
 */
public class MenuMainCategory {
    public String MAINCATNAME;
    public ArrayList<MenuItem> menuItemList;


    public MenuMainCategory(String MAINCATNAME, ArrayList<MenuItem> menuItemList) {
        this.MAINCATNAME = MAINCATNAME;
        this.menuItemList =  menuItemList;
    }
}
