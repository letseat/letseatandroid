package com.foody.jsondata;

/**
 * json
 */
public class Review {
	
	public String ID ;
	public String USERID ;
	public String FULLNAME ;
	public String IMG ;
	public String NO_OF_FOLLOWERS ;
	public String REVIEW  ;
	public String NO_OF_COMMENTS ;
	public String NO_OF_REVIEW ;
	public String NO_OF_REVIEW_LIKE ;
	public String RESID ;
	public String RESNAME ;
	public String RESIMG ;
	public String RATING ;
	public String DATE ;
	public String STATUS ;
	public String DATE_DIFF;
	public String FOLLOW;
	
	public Review(String ID, String USERID, String REVIEW, String RESID, 
			String RATING, String DATE, String STATUS) {
		super();
		this.ID = ID;
		this.USERID = USERID;
		this.REVIEW = REVIEW;
		this.RESID = RESID;
		this.RATING = RATING;
		this.DATE = DATE;
		this.STATUS = STATUS;
	}

	public Review(String ID, String USERID, String FULLNAME, String IMG,
			String NO_OF_FOLLOWERS, String REVIEW, String NO_OF_COMMENTS,
			String NO_OF_REVIEW, String NO_OF_REVIEW_LIKE, String RESID, String RESNAME, String RESIMG,
			String RATING, String DATE, String STATUS, String DATE_DIFF, String FOLLOW) {
		super();
		this.ID = ID;
		this.USERID = USERID;
		this.FULLNAME = FULLNAME;
		this.IMG = IMG;
		this.NO_OF_FOLLOWERS = NO_OF_FOLLOWERS;
		this.REVIEW = REVIEW;
		this.NO_OF_COMMENTS = NO_OF_COMMENTS;
		this.NO_OF_REVIEW = NO_OF_REVIEW;
		this.NO_OF_REVIEW_LIKE = NO_OF_REVIEW_LIKE;
		this.RESID = RESID;
		this.RESNAME = RESNAME;
		this.RESIMG = RESIMG;
		this.RATING = RATING;
		this.DATE = DATE;
		this.STATUS = STATUS;
		this.DATE_DIFF = DATE_DIFF;
		this.FOLLOW=FOLLOW;
	}
	
}
