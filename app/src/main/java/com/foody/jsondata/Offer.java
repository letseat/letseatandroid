package com.foody.jsondata;
/**
 * json
 */
public class Offer {

	
	public String Id;
    public String RestName; 
    public String DisAmount;
    public String HomeDelivery;
    public String Discount;
    public String Capacity;
    public String Cost4Two;
    public String RestLoc; 
    public String RestDistName; 
    public String latitude; 
    public String longitude;
    public String Phone; 
    public String PageNo;  
    public String IMG;    
    public String DisFrom;
    public String cusine;


	private int headerViewVisibility;
  
    

    
    
	public Offer(String id, String restName, String disAmount,
			String homeDelivery,    
			String Discount,
		    String Capacity,
		    String Cost4Two, 
		    String restLoc, String restDistName,
			String latitude, String longitude, String phone, String pageNo, String IMG,  
     String DisFrom, String cusine) {
		super();
		Id = id;
		RestName = restName;
		DisAmount = disAmount;
		HomeDelivery = homeDelivery;
		this.Discount = Discount;
	    this.Capacity = Capacity;
	    this.Cost4Two = Cost4Two;
		RestLoc = restLoc;
		RestDistName = restDistName;
		this.latitude = latitude;
		this.longitude = longitude;
		Phone = phone;
		PageNo = pageNo;
		this.IMG=IMG;
		this.DisFrom=DisFrom;
		this.cusine=cusine;
	}

	public void setHeaderViewStatus(int visibility){
		this.headerViewVisibility = visibility;
	}

	public int getHeaderViewStatus(){
		return  this.headerViewVisibility;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return RestName;
	}
    
    
	
}
