package com.foody.jsondata;

/**
 * json
 */
public class Menu {

	/*
	 * "restmenu": [ { "MenuName": "Angur Sweets /Kg", "Price": "380",
	 * "FoodCat": "" },
	 */

	public String MenuName;
	public String Price;
	public String FoodCat;
	public String IMG;
	public String ThumbIMG;

	public Menu(String MenuName, String Price, String FoodCat, String IMG,
			String ThumbIMG) {
		super();
		this.MenuName = MenuName;
		this.Price = Price;
		this.FoodCat = FoodCat;
		this.IMG = IMG;
		this.ThumbIMG = ThumbIMG;
	}

}
