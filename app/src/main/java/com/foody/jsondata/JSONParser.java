package com.foody.jsondata;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.util.Log;

public class JSONParser {

	static InputStream is = null;
	static JSONObject jObj = null;
	static String json = "";

	// constructor
	public JSONParser() {

	}

	public String getJSONFromUrl(String url) {

		// Making HTTP request
		try {
			// defaultHttpClient
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);

			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();			

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			json = sb.toString();
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}

//		// try parse the string to a JSON object
//		try {
//			jObj = new JSONObject(json);
//		} catch (JSONException e) {
//			Log.e("JSON Parser", "Error parsing data " + e.toString());
//		}

		// return JSON String
		return json;

	}
	
	//public JSONObject getJSONFromUrl1(String url) 

	public JSONObject getJSONFromUrl1(String url) {

		// Making HTTP request
		try {
			// defaultHttpClient
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);

			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();	
			
			

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			json = sb.toString();
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}

		// try parse the string to a JSON object
		try {
			jObj = new JSONObject(json);
		} catch (JSONException e) {
			Log.e("JSON Parser", "Error parsing data " + e.toString());
		}

		// return JSON String
		return jObj;

	}
	
	
	
	public String requestPost(String url,String postData){
			//url = URLEncoder.encode(url);
			String myString = null;
	    	InputStream myInputStream =null;
	    	StringBuilder sb = new StringBuilder();
	    	sb.append(postData);
	    	URL url2;
	    	System.out.println("WWWWWWWWW   "+url +"  "+postData);
	    	try {
	    		url2 = new URL(url);
	    		HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
	    		conn.setConnectTimeout(30000);
	    		conn.setReadTimeout(30000);
	    		conn.setDoOutput(true);
	    		conn.setRequestMethod("POST");
	    		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
	    		// this is were we're adding post data to the request
	    		wr.write(sb.toString());
	    		wr.flush();
	    		myInputStream = conn.getInputStream();
	    		BufferedInputStream bis = new BufferedInputStream(myInputStream);	        	
	        	ByteArrayBuffer baf = new ByteArrayBuffer(100);
	        	int current = 0;
	        	while((current = bis.read()) != -1){ 
	                baf.append((byte)current); 
	           }         	
	        	myString = new String(baf.toByteArray());
	        	System.out.println(">>>>>>>>>>>>>>>   "+myString);
	    		wr.close();
	    		conn.disconnect();
	    	} catch (Exception e) {
	            //handle the exception !
	    		System.out.println("tap  "+e.toString());
	    		myString = "Error: "+e.getMessage();
	    		
	    	}
	    	return myString;	
	    }
	
	
	public String requestPost(String url,String postData, int timeoutMillis ){
		//url = URLEncoder.encode(url);
		String myString = null;
    	InputStream myInputStream =null;
    	StringBuilder sb = new StringBuilder();
    	sb.append(postData);
    	URL url2;
    	System.out.println("WWWWWWWWW   "+url +"  "+postData);
    	try {
    		url2 = new URL(url);
    		HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
    		conn.setConnectTimeout(timeoutMillis );
    		conn.setReadTimeout(timeoutMillis );
    		conn.setDoOutput(true);
    		conn.setRequestMethod("POST");
    		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
    		// this is were we're adding post data to the request
    		wr.write(sb.toString());
    		wr.flush();
    		myInputStream = conn.getInputStream();
    		BufferedInputStream bis = new BufferedInputStream(myInputStream);	        	
        	ByteArrayBuffer baf = new ByteArrayBuffer(100);
        	int current = 0;
        	while((current = bis.read()) != -1){ 
                baf.append((byte)current); 
           }         	
        	myString = new String(baf.toByteArray());
        	System.out.println(">>>>>>>>>>>>>>>   "+myString);
    		wr.close();
    		conn.disconnect();
    	} catch (Exception e) {
            //handle the exception !
    		System.out.println("tap  "+e.toString());
    		myString = "Error: "+e.getMessage();
    		
    	}
    	return myString;	
    }
	
	
	public String getContents(String url) {
        String contents ="";
 
		  try {
		        URLConnection conn = new URL(url).openConnection();		 
		        conn.setConnectTimeout(30000);
		        InputStream in = conn.getInputStream();
		        contents = convertStreamToString(in);
		   } catch (MalformedURLException e) {
		        Log.v("MALFORMED URL EXCEPTION", e.toString());
		   } catch (IOException e) {
		        Log.e(e.getMessage(), e.toString());
		   }
		 
		  return contents;
	}
 
	private String convertStreamToString(InputStream is) throws UnsupportedEncodingException {
	 
	      BufferedReader reader = new BufferedReader(new    
	                              InputStreamReader(is, "UTF-8"));
	        StringBuilder sb = new StringBuilder();
	         String line = null;
	         try {
	                while ((line = reader.readLine()) != null) {
	                        sb.append(line + "\n");
	                }
	           } catch (IOException e) {
	                e.printStackTrace();
	           } finally {
	                try {
	                        is.close();
	                } catch (IOException e) {
	                        e.printStackTrace();
	                }
	            }
	        return sb.toString();
	 }

	
	public Document getDomElement(String xml){
		Document doc = null;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {

			DocumentBuilder db = dbf.newDocumentBuilder();

			InputSource is = new InputSource();
		        is.setCharacterStream(new StringReader(xml));
		        doc = db.parse(is); 

			} catch (ParserConfigurationException e) {
				Log.e("Error: ", e.getMessage());
				e.printStackTrace() ;
				return null;
			} catch (SAXException e) {
				Log.e("Error: ", e.getMessage());
				e.printStackTrace() ;
	            return null;
			} catch (IOException e) {
				Log.e("Error: ", e.getMessage());
				e.printStackTrace() ;
				return null;
			}

	        return doc;
	}
	
	 public final String getElementValue( Node elem ) {
	     Node child;
	     if( elem != null){
	         if (elem.hasChildNodes()){
	             for( child = elem.getFirstChild(); child != null; child = child.getNextSibling() ){
	                 if( child.getNodeType() == Node.TEXT_NODE  ){
	                     return child.getNodeValue();
	                 }
	             }
	         }
	     }
	     return "";
	 }
	 
	 /**
	  * Getting node value
	  * @param Element node
	  * @param key string
	  * */
	 public String getValue(Element item, String str) {		
			NodeList n = item.getElementsByTagName(str);		
			return this.getElementValue(n.item(0));
		}

	
}
