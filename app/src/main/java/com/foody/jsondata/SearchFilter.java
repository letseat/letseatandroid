package com.foody.jsondata;

import java.io.Serializable;

public class SearchFilter implements Serializable {
	private final String YES = "Yes";
	private final String NO = "";
	
	private String Search ; 
	private String Cuisine ; 
	private String Location; 
	private String Cost2p ; 
	private String Capacity ; 
	private String CreditC = NO; 
	private String Wifi = NO; 
	private String HomeDelivery = NO; 
	private String OUTSeat = NO; 
	private String DineIn = NO;
	private String TRR = NO;
	private String BFast = NO;
	private String Bar = NO;
	private String Buffet = NO;
	private String Veg = NO;
	private String NVeg = NO;
	private String AC = NO;
	private String Kidz = NO;
	private String Smoking = NO;
	private String LiveMusic = NO;
	
	
	public static final String tag_toggle_Search = "search" ; 
	public static final String tag_toggle_Cuisine = "cuisine"; 
	public static final String tag_toggle_Location = "location" ; 
	public static final String tag_toggle_Cost2p = "cost"; 
	public static final String tag_toggle_Capacity = "capacuty"; 
	public static final String tag_toggle_CreditC = "cretitcard"; 
	public static final String tag_toggle_Wifi = "wifi"; 
	public static final String tag_toggle_HomeDelivery = "hd"; 
	public static final String tag_toggle_dine_out = "outseat"; 
	public static final String tag_toggle_DineIn = "dinein";
	public static final String tag_toggle_TRR = "trr";
	public static final String tag_toggle_BFast = "bfast";
	public static final String tag_toggle_Bar = "bar";
	public static final String tag_toggle_Buffet = "buffet";
	public static final String tag_toggle_Veg = "veg";
	public static final String tag_toggle_NVeg = "nveg";
	public static final String tag_toggle_ac = "ac";
	public static final String tag_toggle_Kidz = "kidz";
	public static final String tag_toggle_Smoking = "smoking";
	public static final String tag_toggle_LiveMusic = "livemusic";
	
	
	


	/**
	 * @param search
	 * @param cuisine
	 * @param location
	 * @param cost2p
	 * @param capacity
	 * @param creditC
	 * @param wifi
	 * @param homeDelivery
	 * @param oUTSeat
	 * @param dineIn
	 * @param tRR
	 * @param bFast
	 * @param bar
	 * @param buffet
	 * @param veg
	 * @param nVeg
	 * @param aC
	 * @param kidz
	 * @param smoking
	 * @param liveMusic
	 */
	public SearchFilter(String search, String cuisine, String location,
			String cost2p, String capacity, boolean creditC, boolean wifi,
			boolean homeDelivery, boolean oUTSeat, boolean dineIn, boolean tRR,
			boolean bFast, boolean bar, boolean buffet, boolean veg, boolean nVeg,
			boolean aC, boolean kidz, boolean smoking, boolean liveMusic) {
	
		
		Search = search;
		Cuisine = ""; 
		Location = location; 
		Cost2p = cost2p;
		Capacity = capacity; 
		CreditC = creditC?YES : NO; 
		Wifi = wifi? YES : NO; 
		HomeDelivery = homeDelivery? YES : NO; 
		OUTSeat = oUTSeat? YES : NO; 
		DineIn = dineIn? YES : NO; 
		TRR = tRR? YES : NO; 
		BFast = bFast? YES : NO; 
		Bar = bar? YES : NO; 
		Buffet = buffet ? YES : NO; 
		Veg = veg? YES : NO; 
		NVeg = nVeg? YES : NO; 
		AC = aC? YES : NO; 
		Kidz = kidz ? YES : NO; 
		Smoking = smoking? YES : NO;
		LiveMusic = liveMusic? YES : NO;
	}
	
	@Override
	public String toString() {
		
		return "Search: "+ Search+ 
				"Cuisine: " +Cuisine +
		"Location: "+Location+
		"Cost2p: " +Cost2p +
		"Capacity: "+Capacity+
		"CreditC : "+CreditC +
		"Wifi: " +Wifi +
		"HomeDelivery: "+HomeDelivery+ 
		"OUTSeat: " +OUTSeat +
		"DineIn: "+DineIn+
		"TRR: " +TRR +
		"BFast: "+BFast+ 
		"Bar: " +Bar +
		"Buffet: "+Buffet+ 
		"Veg: " +Veg +
		"NVeg: "+NVeg+
		"AC: " +AC +
		"Kidz: "+Kidz+ 
		"Smoking: " +Smoking +
		"LiveMusic: "+LiveMusic; 
	}



	public String getSearch() {
		return Search;
	}



	public void setSearch(String search) {
		Search = search;
	}



	public String getCuisine() {
		return Cuisine;
	}



	public void setCuisine(String cuisine) {
		Cuisine = cuisine;
	}



	public String getLocation() {
		return Location;
	}



	public void setLocation(String location) {
		Location = location;
	}



	public String getCost2p() {
		return Cost2p;
	}



	public void setCost2p(String cost2p) {
		Cost2p = cost2p;
	}



	public String getCapacity() {
		return Capacity;
	}



	public void setCapacity(String capacity) {
		Capacity = capacity;
	}



	public String getCreditC() {
		return CreditC;
	}



	public void setCreditC(boolean creditC) {
		CreditC = creditC? YES : NO;
	}



	public String getWifi() {
		return Wifi;
	}



	public void setWifi(boolean wifi) {
		Wifi  = wifi? YES : NO;
	}



	public String getHomeDelivery() {
		return HomeDelivery;
	}



	public void setHomeDelivery(boolean homeDelivery) {
		HomeDelivery  = homeDelivery? YES : NO;
	}



	public String getOUTSeat() {
		return OUTSeat;
	}



	public void setOUTSeat(boolean oUTSeat) {
		OUTSeat = oUTSeat? YES : NO;
	}



	public String getDineIn() {
		return DineIn;
	}



	public void setDineIn(boolean dineIn) {
		DineIn = dineIn? YES : NO;
	}



	public String getTRR() {
		return TRR;
	}



	public void setTRR(boolean tRR) {
		TRR = tRR? YES : NO;
	}



	public String getBFast() {
		return BFast;
	}



	public void setBFast(boolean bFast) {
		BFast = bFast? YES : NO;
	}



	public String getBar() {
		return Bar;
	}



	public void setBar(boolean bar) {
		Bar = bar? YES : NO;
	}



	public String getBuffet() {
		return Buffet;
	}



	public void setBuffet(boolean buffet) {
		Buffet = buffet? YES : NO;
	}



	public String getVeg() {
		return Veg;
	}



	public void setVeg(boolean veg) {
		Veg = veg? YES : NO;
	}



	public String getNVeg() {
		return NVeg;
	}



	public void setNVeg(boolean nVeg) {
		NVeg = nVeg? YES : NO;
	}



	public String getAC() {
		return AC;
	}



	public void setAC(boolean aC) {
		AC = aC? YES : NO;
	}



	public String getKidz() {
		return Kidz;
	}



	public void setKidz(boolean kidz) {
		Kidz = kidz? YES : NO;
	}



	public String getSmoking() {
		return Smoking;
	}



	public void setSmoking(boolean smoking) {
		Smoking = smoking? YES : NO;
	}



	public String getLiveMusic() {
		return LiveMusic;
	}



	public void setLiveMusic(boolean liveMusic) {
		LiveMusic = liveMusic? YES : NO;
	}
	
	

}
