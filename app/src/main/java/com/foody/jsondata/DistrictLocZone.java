package com.foody.jsondata;

import java.util.ArrayList;
import java.util.List;

/**
 * json
 */

public class DistrictLocZone {

	public String DisId;
	public String DivId;
	public String DisName;
	public String IsComp;
	public String Status;
	public String Lat;
	public String Lng;
	public ArrayList< Location> locations;
	public ArrayList<Zone> zones;
	private boolean selected;

	public DistrictLocZone(String DisId, String DivId, String DisName, String IsComp,
			String Status, String Lat, String Lng, ArrayList< Location> locations, ArrayList<Zone> zones) {
		super();
		this.DisId = DisId;
		this.DivId = DivId;
		this.DisName = DisName;
		this.IsComp = IsComp;
		this.Status = Status;
		this.Lat = Lat;
		this.Lng = Lng;
		this.locations = locations ;
		this.zones = zones ;
	}
	
	public boolean isSelected() {
        return selected;
    }
 
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}
