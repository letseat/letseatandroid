package com.foody.jsondata;

/**
 * json
 */

public class District {

	public String DisId;
	public String DivId;
	public String DisName;
	public String IsComp;
	public String Status;
	public String Lat;
	public String Lng;
	private boolean selected;

	public District(String DisId, String DivId, String DisName, String IsComp,
			String Status, String Lat, String Lng) {
		super();
		this.DisId = DisId;
		this.DivId = DivId;
		this.DisName = DisName;
		this.IsComp = IsComp;
		this.Status = Status;
		this.Lat = Lat;
		this.Lng = Lng;
	}
	
	public boolean isSelected() {
        return selected;
    }
 
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}
