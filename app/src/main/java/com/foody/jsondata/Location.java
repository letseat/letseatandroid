package com.foody.jsondata;

/**
 * json
 */

public class Location {

	public String LocId;
	public String DisId;
	public String LocName;
	public String Lat;
	public String Lng;
	private boolean selected;

	public Location(String LocId, String DisId, String LocName, String Lat,
			String Lng) {
		super();
		this.LocId = LocId;
		this.DisId = DisId;
		this.LocName = LocName;
		this.Lat = Lat;
		this.Lng = Lng;
	}
	
	public boolean isSelected() {
        return selected;
    }
 
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    
    @Override
    public String toString() {
    	// TODO Auto-generated method stub
    	return LocName;
    }

}
