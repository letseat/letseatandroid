package com.foody.jsondata;

import java.io.Serializable;

/**
 * json
 */

public class Event implements Serializable {

	public String EVENTID;
	public String EVNAME;
	public String EVDESC;
	public String STARTDATE;
	public String ENDDATE;
	public String IMG;

	/**
	 * @param eVENTID
	 * @param eVNAME
	 * @param sTARTDATE
	 * @param eNDDATE
	 * @param iMG
	 */
	public Event(String eVENTID, String eVNAME, String EVDESC, String sTARTDATE,
			String eNDDATE, String iMG) {
		super();
		EVENTID = eVENTID;
		EVNAME = eVNAME;
		this.EVDESC =  EVDESC;
		STARTDATE = sTARTDATE;
		ENDDATE = eNDDATE;
		IMG = iMG;
	}

}
