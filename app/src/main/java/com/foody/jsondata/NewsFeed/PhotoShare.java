package com.foody.jsondata.NewsFeed;

/**
 * json
 */
public class PhotoShare extends NewsFeed {
	public String Photo;
	public String PhotRes;
	public String PhotResName;
	public String PhotDate;

	public PhotoShare(String Type, String Photo, String PhotRes,
			String PhotResName, String PhotDate) {
		super();
		this.Type = Type;
		this.Photo = Photo;
		this.PhotRes = PhotRes;
		this.PhotResName = PhotResName;
		this.PhotDate = PhotDate;
	}

}
