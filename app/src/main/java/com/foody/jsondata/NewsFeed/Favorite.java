package com.foody.jsondata.NewsFeed;
/**
 * json
 */
public class Favorite extends NewsFeed {

	public String FavResId;
	public String FavResName;
	public String RESIMG;
	public String FavDate;

	/**
	 * @param FavRes
	 * @param FavDate
	 */
	public Favorite(String Type, String FavResId, String FavResName, String RESIMG, String FavDate) {
		super();		
		this.Type = Type;
		this.FavResId=FavResId;
		this.FavResName=FavResName;
		this.RESIMG=RESIMG;
		this.FavDate=FavDate;
	}

}
