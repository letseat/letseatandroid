package com.foody.jsondata.NewsFeed;
/**
 * json
 */
public class Follow  extends NewsFeed{

	 
	 
	 public String FolloerId;
	 public String FolloerName;
	 public String IMGFOLWR;
	 public String FolloDate;
	 
	/**
	 * @param FolloId
	 * @param folloDate
	 */
	public Follow(String Type, String FolloerId,String FolloerName, String IMGFOLWR, String FolloDate) {
		super();
		
		this.Type=Type;
		this.FolloerId=FolloerId;
		this.FolloerName=FolloerName;
		this.IMGFOLWR=IMGFOLWR;
		this.FolloDate=FolloDate;
	}
	 
}
