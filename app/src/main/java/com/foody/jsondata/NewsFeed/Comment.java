package com.foody.jsondata.NewsFeed;


/**
 * json
 */
public class Comment extends NewsFeed  {
	
	
	public String RevComm;
	public String RevRes;
	public String RevDate;
	public String RevRat;
	public String RevResName;
	
	
	
	
	public Comment(String Type, String RevComm, String RevRes, String RevDate, String RevRat, String RevResName) {
		super();
		this.RevComm = RevComm;
		this.RevRes = RevRes;
		this.RevDate = RevDate;
		this.RevRat = RevRat;
		this.Type=Type;
		this.RevResName=RevResName;
	}
}
