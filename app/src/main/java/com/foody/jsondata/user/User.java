package com.foody.jsondata.user;

/**
 * json
 */
public class User {

	public String ID;
	public String USERNAME;
	public String PASSWORD;
	public String NAME;
	public String SEX;
	public String IMG;
	public String EMAIL;
	public String PHONE;
	public String COUNTRY;
	public String CITY;
	public String ZIP;
	public String DATE;
	public String STATUS;
	public String ABOUT;
	public String USERTYPE;
	
	public String FavRes;
	public String FavFood;
	public String FavCus;
	public String NoResVisit;
	public String VisitWith;
	public String VisitTime;

	/*public User(String ID, String USERNAME, String PASSWORD, String NAME,
			String SEX, String IMG, String EMAIL, String COUNTRY, String CITY,
			String ZIP, String DATE, String STATUS, String PHONE, String ABOUT,
			String USERTYPE) {

		this.ID = ID;
		this.USERNAME = USERNAME;
		this.PASSWORD = PASSWORD;
		this.NAME = NAME;
		this.SEX = SEX;
		this.IMG = IMG;
		this.EMAIL = EMAIL;
		this.COUNTRY = COUNTRY;
		this.CITY = CITY;
		this.ZIP = ZIP;
		this.DATE = DATE;
		this.STATUS = STATUS;
		this.PHONE = PHONE;
		this.ABOUT = ABOUT;
		this.USERTYPE = USERTYPE;
	}*/
	
	public User(String ID, String USERNAME, String PASSWORD, String NAME,
			String SEX, String IMG, String EMAIL, String COUNTRY, String CITY,
			String ZIP, String DATE, String STATUS, String PHONE, String ABOUT,
			String USERTYPE,
			String FavRes,
			String FavFood,
			String FavCus,
			String NoResVisit,
			String VisitWith,
			String VisitTime) {
		
		this.ID = ID;
		this.USERNAME = USERNAME;
		this.PASSWORD = PASSWORD;
		this.NAME = NAME;
		this.SEX = SEX;
		this.IMG = IMG;
		this.EMAIL = EMAIL;
		this.COUNTRY = COUNTRY;
		this.CITY = CITY;
		this.ZIP = ZIP;
		this.DATE = DATE;
		this.STATUS = STATUS;
		this.PHONE = PHONE;
		this.ABOUT = ABOUT;
		this.USERTYPE = USERTYPE;
		this.FavRes = FavRes;
		this.FavFood = FavFood;
		this.FavCus = FavCus;
		this.NoResVisit = NoResVisit;
		this.VisitWith = VisitWith;
		this.VisitTime = VisitTime;
		
	}

}
