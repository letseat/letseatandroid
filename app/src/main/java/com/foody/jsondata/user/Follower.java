package com.foody.jsondata.user;

/**
 * json
 */
public class Follower {

	public String ID;
	public String USERID;
	public String FOLLOWERID;
	public String FOLLOWER_NAME;
	public String SEX;
	public String TOTALFOLLOWER;
	public String IMG;
	public String DATE;

	public Follower(String ID, String USERID, String FOLLOWERID,
			String FOLLOWER_NAME, String SEX, String TOTALFOLLOWER,
			String DATE, String IMG) {

		this.ID = ID;
		this.USERID = USERID;
		this.FOLLOWERID = FOLLOWERID;
		this.FOLLOWER_NAME = FOLLOWER_NAME;
		this.SEX = SEX;
		this.TOTALFOLLOWER = TOTALFOLLOWER;
		this.DATE = DATE;
		this.IMG = IMG;
	}

}
