package com.foody.jsondata.user;

/**
 * json
 */
public class UserPubInfo {
	
	public String ID;
    public String NAME;
    public String IMG;
    public String COUNTRY;
    public String CITY;
    public String ZIP;
    public String ABOUT;
    public String NOFOllOWERS;
    public String NOREVIEW;
	/**
	 * @param iD
	 * @param nAME
	 * @param iMG
	 * @param cOUNTRY
	 * @param cITY
	 * @param zIP
	 * @param aBOUT
	 * @param nOFOllOWERS
	 * @param nOREVIEW
	 */
	public UserPubInfo(String ID, String NAME, String IMG, String COUNTRY,
			String CITY, String ZIP, String ABOUT, String NOFOllOWERS,
			String NOREVIEW) {
		super();
		this.ID = ID;
		this.NAME = NAME;
		this.IMG = IMG;
		this.COUNTRY = COUNTRY;
		this.CITY = CITY;
		this.ZIP = ZIP;
		this.ABOUT = ABOUT;
		this.NOFOllOWERS = NOFOllOWERS;
		this.NOREVIEW = NOREVIEW;
	}
    
    

}
