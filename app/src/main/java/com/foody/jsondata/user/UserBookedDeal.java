package com.foody.jsondata.user;

public class UserBookedDeal {
	
	 public String DisId;
     public String BookingId;
     public String ResId; 
     public String ResName;
     public String DisName;
     public String DisCompany;
     public String DisPercent;
     public String DisValid; 
     public String IsHighlighted; 
     public String DisDetail;
     public String IsLetsEat;
     public String Location; 
     public String District; 
     public String BookingStatus;
     public String TrackingCode;
	public  String Img;
     
     private boolean isRedeem;
     
     
	public boolean isRedeem() {
		return isRedeem;
	}


	public void setRedeem(boolean isRedeem) {
		this.isRedeem = isRedeem;
	}


	/**
	 * @param disId
	 * @param bookingId
	 * @param resId
	 * @param resName
	 * @param disName
	 * @param disCompany
	 * @param disPercent
	 * @param disValid
	 * @param isHighlighted
	 * @param disDetail
	 * @param isLetsEat
	 * @param location
	 * @param district
	 * @param bookingStatus
	 */
	public UserBookedDeal(String disId, String bookingId, String resId,
			String resName, String disName, String disCompany,
			String disPercent, String disValid, String isHighlighted,
			String disDetail, String isLetsEat, String location,
			String district, String bookingStatus, String TrackingCode, String Img) {
		super();
		DisId = disId;
		BookingId = bookingId;
		ResId = resId;
		ResName = resName;
		DisName = disName;
		DisCompany = disCompany;
		DisPercent = disPercent;
		DisValid = disValid;
		IsHighlighted = isHighlighted;
		DisDetail = disDetail;
		IsLetsEat = isLetsEat;
		Location = location;
		District = district;
		BookingStatus = bookingStatus;
		this.TrackingCode = TrackingCode;
		this.Img = Img;
	} 
	
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return DisDetail;
	}
     
     
     
    

}
