package com.foody.jsondata;

/**
 * json
 */

public class Zone {

	public String ZoneId;
	public String DisId;
	public String ZoneName;
	public String  LocName;
	public String  Lat;
	public String  Lng;
	public boolean selected;

	public Zone(String ZoneId, String DisId, String ZoneName, String  LocName, String  Lat, String  Lng) {
		
		this.ZoneId = ZoneId;
		this.DisId = DisId;
		this.ZoneName = ZoneName;
		this.LocName = LocName;
		this.Lat = Lat;
		this.Lng = Lng;
		
	}
	
	public boolean isSelected() {
        return selected;
    }
 
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}
