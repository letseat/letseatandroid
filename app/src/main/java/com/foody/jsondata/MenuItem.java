package com.foody.jsondata;

/**
 * Created by Arif on 4/24/2016.
 */
public class MenuItem {


    public String ItemId;
    public String ItemName;
    public String ItemPrice;
    public String ItemDesc;
    public String ServSize;
    public String ServTime;
    public String UnitType;
    public String Ingradients;
    public String Currency;
    public String IMG;
    public String ThumbIMG;

    public MenuItem(String itemId, String itemName, String itemPrice, String itemDesc, String servSize, String servTime, String unitType, String ingradients, String currency, String IMG, String thumbIMG) {
        ItemId = itemId;
        ItemName = itemName;
        ItemPrice = itemPrice;
        ItemDesc = itemDesc;
        ServSize = servSize;
        ServTime = servTime;
        UnitType = unitType;
        Ingradients = ingradients;
        Currency = currency;
        this.IMG = IMG;
        ThumbIMG = thumbIMG;
    }
}
