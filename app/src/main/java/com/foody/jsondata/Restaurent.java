package com.foody.jsondata;

import android.view.View;

/**
 * json
 */
public class Restaurent   {

		
	public String Id;	
	public String RestName;
	public String RestLoc;	
	public String RestDistName;
	public String latitude;	
	public String longitude;
	public String Phone;
	public String PageNo;
	public String IMG;
	public String Rating;
	public String Open;
	public String Close;
	public String Wifi;
	public String LiveMusic;
	
	 
	public String Address; 	
	public String BreakFast;
	public String SettingOutside;
	public String HomeDelivery;
	public String TableReservation;
	public String Discount;
	public String DineIn;
	public String Bar;
	public String AC;
	public String  Veg;
	public String NonVeg;
	public String CreditCard;
	public String Capacity;
	public String Cost4Two;
 	
	private boolean selected;
	
	//for nearby only
	private double distance;
	
	private int OpenOrClose;

	private int headerViewVisibility;
	
	
	public Restaurent(
			String Id,	
	String RestName,
	String RestLoc,	
	String RestDistName,
	String latitude,	
	String longitude,
	String Phone,
	String PageNo,
	String IMG,
	String Rating,
	String Open,
	String Close,
	String Wifi,
	String LiveMusic,	
	 
	String Address, 	
	String BreakFast,
	String SettingOutside,
	String HomeDelivery,
	String TableReservation,
	String Discount,
	String DineIn,
	String Bar,
	String AC,
	String  Veg,
	String NonVeg,
	String CreditCard,
	String Capacity,
	String Cost4Two) {
		
		this.Id = Id;	
		this.RestName = RestName;
		this.RestLoc = RestLoc;	
		this.RestDistName = RestDistName;
		this.latitude = latitude;	
		this.longitude = longitude;
		this.Phone = Phone;
		this.PageNo = PageNo;
		this.IMG = IMG;
		this.Rating = Rating;
		this.Open = Open;
		this.Close = Close;
		this.Wifi = Wifi;
		this.LiveMusic = LiveMusic;	
		 
		this.Address = Address; 	
		this.BreakFast = BreakFast;
		this.SettingOutside = SettingOutside;
		this.HomeDelivery = HomeDelivery;
		this.TableReservation = TableReservation;
		this.Discount = Discount;
		this.DineIn = DineIn;
		this.Bar = Bar;
		this.AC = AC;
		this.Veg = Veg;
		this.NonVeg = NonVeg;
		this.CreditCard = CreditCard;
		this.Capacity = Capacity;
		this.Cost4Two = Cost4Two;

		setHeaderViewStatus(View.GONE);

	}
	
	

	public Restaurent(String Id, String RestName, String RestLoc,
			String RestDistName, String latitude, String longitude,
			String Phone, String PageNo, String IMG, String Rating,
			String open, String close, String wifi, String liveMusic) {
		this.Id = Id;
		this.RestName = RestName;
		this.RestLoc = RestLoc;
		this.RestDistName = RestDistName;
		this.latitude = latitude;
		this.longitude = longitude;
		this.Phone = Phone;
		this.PageNo = PageNo;
		this.IMG = IMG;
		this.Rating = Rating;
		this.Open = open;
		this.Close = close;
		this.Wifi = wifi;
		this.LiveMusic = liveMusic;

		setHeaderViewStatus(View.GONE);

	}



	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}


	
	
	public boolean isSelected() {
        return selected;
    }
 
    public void setSelected(boolean selected) {
        this.selected = selected;
    }



	public int getOpenOrClose() {
		return OpenOrClose;
	}



	public void setOpenOrClose(int openOrClose) {
		OpenOrClose = openOrClose;
	}

	public void setHeaderViewStatus(int visibility){
		this.headerViewVisibility = visibility;
	}

	public int getHeaderViewStatus(){
		return  this.headerViewVisibility;
	}

	@Override
	public String toString() {

		return RestName;
	}
}
