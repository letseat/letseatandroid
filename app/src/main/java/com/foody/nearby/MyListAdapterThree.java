package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mcc.letseat.R;


/**
 * Custom data adapter for 3 item ListView
 */
public class MyListAdapterThree extends BaseAdapter{
	
	ArrayList<HashMap<String, String>> aa;
	Context c;
	String listTags[];
	
	
	
	public MyListAdapterThree(Context c, ArrayList<HashMap<String, String>> aa, String listTags[]){
		this.aa = aa;
		this.c = c;
		this.listTags=listTags;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return aa.size();
	}

	@Override
	public String getItem(int arg0) {
		// TODO Auto-generated method stub
		return aa.get(arg0).get("reference");
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	private int lastPosition = -1;
	
	@Override
	public View getView(int pos, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		
		View v;
		v = arg1;
		LayoutInflater li = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		v = li.inflate(R.layout.list_item_three, null);
		
		LinearLayout root = (LinearLayout)v.findViewById(R.id.ltnbTitle);
		
		TextView first= (TextView)v.findViewById(R.id.first);
		TextView second = (TextView)v.findViewById(R.id.second);
		TextView third = (TextView)v.findViewById(R.id.third);
		
		
		if(pos%2==0)
		{
			root.setBackgroundColor(Color.parseColor("#FCFCFC"));
		}
		else
		{
			root.setBackgroundColor(Color.parseColor("#FFFFFF"));
		}
		
		//String ww =aa.get(pos);
		if(listTags!=null){
			
			first.setText(aa.get(pos).get(listTags[1]));
			first.setTextColor(Color.parseColor("#BE1010"));
			
			second.setText(aa.get(pos).get(listTags[0]));
			second.setTextColor(Color.parseColor("#222222"));
			
			third.setText(aa.get(pos).get(listTags[2]));
		}else{
			first.setText(aa.get(pos).get(""));
			second.setText(aa.get(pos).get(""));
			third.setText(aa.get(pos).get(""));
		}
		
		/*name.setText(aa.get(pos).get("name"));
		dis.setText(aa.get(pos).get("distance"));
		ref.setText(aa.get(pos).get("reference"));
*/		
		
		
		//apply animation on current view
		 Animation animation = AnimationUtils.loadAnimation(c, (pos > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
		 v.startAnimation(animation);
		 lastPosition = pos;
		
		return v;
	}

}
