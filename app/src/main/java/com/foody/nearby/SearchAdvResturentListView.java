package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;



import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.SearchFilter;
import com.foody.jsondata.SearchResturent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mcc.letseat.CapturePhotoActivity;
import com.mcc.letseat.R;
import com.mcc.letseat.tab.TabMainActivity;
import com.mcc.libs.AnimationTween;
import com.mcc.user.SavedPrefernce;


/**
 * advance search with filter 
 */
public class SearchAdvResturentListView extends Activity implements
		ActionBar.OnNavigationListener {

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Google Places
	FoodyResturent foodyResturent;

	// Places List
	ArrayList<SearchResturent> resturentList;


	// Button
	ImageView btnShowOnMap;

	

	// Places Listview
	ListView lv;

	// ListItems data
	ArrayList<HashMap<String, String>> placesListItems = new ArrayList<HashMap<String, String>>();

	// //admob
	AdView adView = null;

	ImageView searchAny;

	String searchKeywords;
	
	TextView txtAdvSearchKey, txtAdvLoc, txtAdvCost, txtAdvCap;
	CheckBox checkbox_adv_smoking_zone, 
	checkbox_adv_ac,
	checkbox_adv_bar,
	checkbox_adv_breakfast,
	checkbox_adv_buffet,
	checkbox_adv_cash_card,
	checkbox_adv_dine_outside,
	checkbox_adv_home_delivery,
	checkbox_adv_kids_zone,
	checkbox_adv_non_veg,
	checkbox_adv_reservation,
	checkbox_adv_veg,
	checkbox_adv_wifi,
	checkbox_adv_LiveMusic;
	
	public static String LocName="";
	public static String LocId="";
	
	Button btnAdvSearch;
	SearchFilter sf;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.slide_up_in,R.anim.stay);
		
		setContentView(R.layout.activity_adv_search);

		//
		txtAdvSearchKey= (TextView) findViewById(R.id.txtAdvSearchKey);
		txtAdvLoc= (TextView) findViewById(R.id.txtAdvLoc);
		txtAdvCost= (TextView) findViewById(R.id.txtAdvCost);
		txtAdvCap= (TextView) findViewById(R.id.txtAdvCap);
		
		LocId=new SavedPrefernce(this).getLocationId();
		txtAdvLoc.setText(new SavedPrefernce(this).getLocationName());
		LocName=txtAdvLoc.getText().toString();
		txtAdvLoc.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(SearchAdvResturentListView.this, DistrictListView.class);
				i.putExtra(StaticObjects.NEV_TYPE, StaticObjects.NEV_TYPE_CHOOSE_LOCTION);			
				startActivity(i);
			}
		});
		
		
		//check box
		checkbox_adv_smoking_zone=(CheckBox)findViewById(R.id.checkbox_adv_smoking_zone);
		checkbox_adv_ac=(CheckBox)findViewById(R.id.checkbox_adv_ac);
		checkbox_adv_bar=(CheckBox)findViewById(R.id.checkbox_adv_bar);
		checkbox_adv_breakfast=(CheckBox)findViewById(R.id.checkbox_adv_breakfast);
		checkbox_adv_buffet=(CheckBox)findViewById(R.id.checkbox_adv_buffet);
		checkbox_adv_cash_card=(CheckBox)findViewById(R.id.checkbox_adv_cash_card);
		checkbox_adv_dine_outside=(CheckBox)findViewById(R.id.checkbox_adv_dine_outside);
		checkbox_adv_home_delivery=(CheckBox)findViewById(R.id.checkbox_adv_home_delivery);
		checkbox_adv_kids_zone=(CheckBox)findViewById(R.id.checkbox_adv_kids_zone);
		checkbox_adv_non_veg=(CheckBox)findViewById(R.id.checkbox_adv_non_veg);
		checkbox_adv_reservation=(CheckBox)findViewById(R.id.checkbox_adv_reservation);
		checkbox_adv_veg=(CheckBox)findViewById(R.id.checkbox_adv_veg);
		checkbox_adv_wifi=(CheckBox)findViewById(R.id.checkbox_adv_wifi);
		checkbox_adv_LiveMusic=(CheckBox)findViewById(R.id.checkbox_adv_LiveMusic);
		
		btnAdvSearch=(Button)findViewById(R.id.btnAdvSearch);
		btnAdvSearch.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new LoadPlaces().execute();
			}
		});
		
		
		// Getting listview
		lv = (ListView) findViewById(R.id.list_adv_search);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				SearchResturent resturent = (SearchResturent) resturentList.get(position);
				String nev_type=getIntent().getStringExtra(StaticObjects.NEV_TYPE);
				
				if(nev_type!=null && nev_type.equals(StaticObjects.NEV_TYPE_CHOOSE_RES_PHOTO_UP)){
					Intent in=new Intent();
					in.putExtra( "Id" , resturent.Id);	
					in.putExtra( "RestName" , resturent.Name);	
					setResult(RESULT_OK, in);
					CapturePhotoActivity.resid=resturent.Id;
					CapturePhotoActivity.resname=resturent.Name;
					
					finish();
				}else{
					
					Intent in = new Intent(getApplicationContext(),
							TabMainActivity.class);
					in.putExtra("name", resturent.Name);
					in.putExtra("address", resturent.Address);
					in.putExtra("imageLink", resturent.IMG);
					in.putExtra("Id",resturent.Id);

					startActivity(in);
				}

				
			}
		});
		
		//get search filter
		sf =(SearchFilter)getIntent().getSerializableExtra("searchFilter");
		if(sf != null){
			lv.setVisibility(View.VISIBLE);
			new LoadPlaces().execute();
		}

		adView = (AdView) this.findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder().build();
		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});

		//handleIntent(getIntent());

	}
	
	
	

	private void handleIntent(Intent intent) {
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			searchKeywords = null;
			searchKeywords = intent.getStringExtra(SearchManager.QUERY);
			new LoadPlaces().execute();
			//txtTitleNearBy.setText("Showing results for: "+ searchKeywords);

		} else if (intent.getStringExtra("searchKeyWord_home") != null) {
			searchKeywords = null;
			searchKeywords = intent.getStringExtra("searchKeyWord_home");
			new LoadPlaces().execute();
			//txtTitleNearBy.setText("Showing results for: ");
		}
		
		//
		// Spannable string allows us to edit the formatting of the text.
       /* SpannableString titleText = new SpannableString(searchKeywords);
        titleText.setSpan(new ForegroundColorSpan(Color.RED), 0, titleText.length(), 0);
        txtTitleNearBy.setText(txtTitleNearBy.getText()+""+titleText);*/
        
       
        Spannable WordToSpan = new SpannableString("Did you mean ");        
        WordToSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#333333")), 0, WordToSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtAdvSearchKey.setText(WordToSpan);
        
        Spannable WordToSpan1 = new SpannableString("'"+searchKeywords+"'");        
        WordToSpan1.setSpan(new ForegroundColorSpan(Color.parseColor("#BE1010")), 0, WordToSpan1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtAdvSearchKey.append(WordToSpan1);

	}

	

	/*// action bar menu and event
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_main, menu);
		// Associate searchable configuration with the SearchView
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) menu.findItem(R.id.action_search)
				.getActionView();
		searchView.setSearchableInfo(searchManager
				.getSearchableInfo(getComponentName()));

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
		case android.R.id.home:
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
		case R.id.action_search:
			// search action
			return true;
		case R.id.action_nearby:
			Intent i=new Intent(SearchResturentListView.this,NearByplaceListView.class);			
			startActivity(i);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}*/

	@Override
	public void onPause() {
		super.onPause();
		overridePendingTransition(R.anim.stay,R.anim.slide_down_out);
		if (adView != null)
			adView.pause();
		
		//overridePendingTransition(0,R.anim.view_close_translate);

	}

	@Override
	public void onResume() {
		super.onResume();
		
		if(!txtAdvLoc.getText().toString().equals(LocName)){
			txtAdvLoc.setText(LocName);
		}

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		if (adView != null)
			adView.destroy();
		super.onDestroy();
	}

	class LoadPlaces extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);
			
			AnimationTween.animateView(findViewById(R.id.scrollView_SearchUI), SearchAdvResturentListView.this);
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {
				
				
				
				String Search=txtAdvSearchKey.getText().toString().toString(); 
				String Cuisine=""; 
				String Location=LocId; 
				String Cost2p=txtAdvCost.getText().toString().toString(); 
				String Capacity=txtAdvCap.getText().toString().toString(); 
				String CreditC=checkbox_adv_cash_card.isChecked()?"Yes":""; 
				String Wifi=checkbox_adv_wifi.isChecked()?"Yes":""; 
				String HomeDelivery=checkbox_adv_home_delivery.isChecked()?"Yes":""; 
				String OUTSeat=checkbox_adv_dine_outside.isChecked()?"Yes":""; 
				String DineIn=""; 
				String TRR=checkbox_adv_reservation.isChecked()?"Yes":""; 
				String BFast=checkbox_adv_breakfast.isChecked()?"Yes":""; 
				String Bar=checkbox_adv_bar.isChecked()?"Yes":""; 
				String Buffet=checkbox_adv_buffet.isChecked()?"Yes":""; 
				String Veg=checkbox_adv_veg.isChecked()?"Yes":""; 
				String NVeg=checkbox_adv_non_veg.isChecked()?"Yes":""; 
				String AC=checkbox_adv_ac.isChecked()?"Yes":""; 
				String Kidz=checkbox_adv_kids_zone.isChecked()?"Yes":""; 
				String Smoking=checkbox_adv_smoking_zone.isChecked()?"Yes":"";
				String LiveMusic=checkbox_adv_LiveMusic.isChecked()?"Yes":"";
				
				if(sf!=null){
					resturentList = foodyResturent.searchAdv(sf.getSearch(), sf.getCuisine(), sf.getLocation(), sf.getCost2p(), sf.getCapacity(), sf.getCreditC(), sf.getWifi(), sf.getHomeDelivery(), sf.getOUTSeat(), sf.getDineIn(), sf.getTRR(), sf.getBFast(), sf.getBar(), sf.getBuffet(), sf.getVeg(), sf.getNVeg(), sf.getAC(), sf.getKidz(), sf.getSmoking(), sf.getLiveMusic());
				}else{
					resturentList = foodyResturent.searchAdv(Search, Cuisine, Location, Cost2p, Capacity, CreditC, Wifi, HomeDelivery, OUTSeat, DineIn, TRR, BFast, Bar, Buffet, Veg, NVeg, AC, Kidz, Smoking, LiveMusic);
				}
				

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			//findViewById(R.id.main_custom_progress).setVisibility(View.GONE);
			AnimationTween.animateView(findViewById(R.id.main_custom_progress), SearchAdvResturentListView.this);
			lv.setVisibility(View.VISIBLE);
			AnimationTween.animateViewPosition(lv, SearchAdvResturentListView.this);
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status

					if (resturentList == null || resturentList.size() <= 0) {

						//show popup for sugest request
						showPopUp();
						
						 
					} else if (resturentList != null) {
						// Check for all possible status
						placesListItems = new ArrayList<HashMap<String, String>>();

						// Successfully got search
						if (resturentList != null) {
							// loop through each place
							for (SearchResturent p : resturentList) {

								HashMap<String, String> map = new HashMap<String, String>();
								map.put("Id", p.Id);
								// Place name
								map.put("Name", p.Name);
								map.put("Address", p.Address);

								System.out
										.println(">>>>>>>>>>test>>>>>>>>>>>>>>>>>> "
												+ p.Address);

								// adding HashMap to ArrayList
								placesListItems.add(map);
							}
							// list adapter
							MyListAdapter adapter = new MyListAdapter(
									SearchAdvResturentListView.this,
									placesListItems,new String[] {  "Name", "Address","Id" });

							// Adding data into listview
							lv.setAdapter(adapter);
						}

						else if (resturentList == null) {
							showPopUp();
						}
					}
				}

			});

		}

	}
	
	

	@Override
	public boolean onNavigationItemSelected(int arg0, long arg1) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	private void showPopUp() {

		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
		// helpBuilder.setTitle("Pop Up");
		helpBuilder.setMessage("Nothing Found. Want to request this suggestion?");
		helpBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						finish();
						Intent in = new Intent(
								getApplicationContext(),
								SuggetionRequest.class);
						in.putExtra("keyword", searchKeywords);

						startActivity(in);
					}
				});

		helpBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
						
					}
				});

		// Remember, create doesn't show the dialog
		AlertDialog helpDialog = helpBuilder.create();
		helpDialog.show();

	}

}
