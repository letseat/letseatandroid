package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.foody.AppData.StaticObjects;
import com.foody.jsondata.Event;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.LetsEatRequest;
import com.foody.jsondata.Offer;
import com.foody.jsondata.ResturentFeatured;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.mcc.letseat.Adapter.EventRestaurantAdapter;
import com.mcc.letseat.MyProgressDialog;
import com.mcc.letseat.R;
import com.mcc.letseat.tab.TabMainActivity;
import com.mcc.libs.ActivityWithSliding;
import com.mcc.libs.AnimationTween;
import com.squareup.picasso.Picasso;

/**
 * 
 * @author Arif
 * Restaurants Featured in event or special occasion
 */
public class FeaturedRestaurantListView extends ActivityWithSliding implements
		ActionBar.OnNavigationListener {

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Google Places
	FoodyResturent foodyResturent;

	// Places List
	ArrayList<ResturentFeatured> resturentList;


	// Button
	ImageView btnShowOnMap;

	// Progress dialog
	MyProgressDialog pDialog;

	// Places Listview
	ListView lv;

	// ListItems data
	ArrayList<HashMap<String, String>> resturantListItems = new ArrayList<HashMap<String, String>>();
	
	// LayoutCuisine
	LinearLayout layoutCuisine;

	// //admob
	AdView adView = null;

	ImageView searchAny;

	String searchKeywords;
	
	//post value
	String EventId;
	int currentPage=0;
	int totalPage=1;
	int adjustPosition=0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_rest);

		

		// Getting listview
		lv = (ListView) findViewById(R.id.list);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {



				
				//ResturentFeatured r=resturentList.get(position-adjustPosition);
				ResturentFeatured r=((ResturentFeatured)lv.getItemAtPosition(position));
				
				//analytics
				//analytics
				analyticsSendScreenView("view events Restaurant: "+r.Id);
				
				//restaurentId= resturent.Id;
				
				Intent in = new Intent(getApplicationContext(),TabMainActivity.class);
				in.putExtra( "Id" , r.Id);
				in.putExtra( "latitude" , r.latitude);
				in.putExtra( "longitude" , r.longitude);
				in.putExtra( "RestName" , r.RestName);
				in.putExtra( "address" , r.RestLoc);
				startActivity(in);
				
			}
		});
		
		

		adView = (AdView) this.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder().build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});
		
		
		layoutCuisine=(LinearLayout)findViewById(R.id.ll_search_hint);		
		layoutCuisine.setVisibility(View.GONE);
		
		/*String name=getIntent().getStringExtra("name");
		String IMG=getIntent().getStringExtra("IMG");
		String date=getIntent().getStringExtra("date");*/
		Intent intent =  getIntent();
		Event event = (Event)intent.getSerializableExtra("event");

		if(event.EVNAME!=null & event.IMG!=null){
			/*TextView eventName= (TextView)findViewById(R.id.textEventName);	
			Typeface tf = Typeface.createFromAsset(getAssets(), StaticObjects.fontPath_SEGOEUI);	
			eventName.setTypeface(tf);		
			eventName.setText(name);
			
			TextView eventDatetime= (TextView)findViewById(R.id.textEventDateTime);	
			Typeface tfLight = Typeface.createFromAsset(getAssets(), StaticObjects.fontPath_SEGOEUI_light);	
			eventDatetime.setTypeface(tfLight);		
			eventDatetime.setText(date);
			
			ImageView imageView=(ImageView) findViewById(R.id.imageView_event);
			
			if(IMG!=null &&  !IMG.isEmpty()){
				Picasso.with(this)
			    .load(IMG)
			    .placeholder(R.drawable.sample_res_photo)
			    .error(R.drawable.sample_res_photo)
			    .into(imageView);
			}*/
			
			View v;
			
			LayoutInflater li = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = li.inflate(R.layout.list_item_event_new, null);


			ImageView imageView=(ImageView) v.findViewById(R.id.imageView_event);


			TextView textEventName = (TextView) v.findViewById(R.id.textEventName);
			TextView textSTDate = (TextView) v.findViewById(R.id.textSTDate);
			TextView textEventDatefull = (TextView) v.findViewById(R.id.textEventDatefull);
			TextView textEventStatus = (TextView) v.findViewById(R.id.textEventStatus);
			TextView textEventDsc = (TextView) v.findViewById(R.id.textEventDsc);

			Typeface tf = Typeface.createFromAsset(getAssets(), StaticObjects.fontPath_SEGOEUI);
			Typeface tfLight = Typeface.createFromAsset(getAssets(), StaticObjects.fontPath_SEGOEUI_light);

			textEventName.setTypeface(tf);
			textSTDate.setTypeface(tf);
			textEventDatefull.setTypeface(tf);
			textEventStatus.setTypeface(tf);
			textEventDsc.setTypeface(tfLight);

			textEventName.setText(event.EVNAME);
			String stdate=event.STARTDATE.replace(" ","\n");
			textSTDate.setText(stdate);
			textEventDatefull.setText(event.STARTDATE+" to "+event.ENDDATE);
			textEventStatus.setText("");
			textEventDsc.setText(event.EVDESC);
			
			if(event.IMG!=null &&  !event.IMG.isEmpty()){
				Picasso.with(this)
			    .load(event.IMG)
			    .placeholder(R.drawable.sample_res_photo)
			    .error(R.drawable.sample_res_photo)
			    .into(imageView);
			}
			
			lv.addHeaderView(v, "event", false);
			
			
			adjustPosition=1;
			
			
		}else{
			adjustPosition=0;
		}
		
		
		
		
		
		
		EventId=event.EVENTID;
		loadData();
		
		
		
		
		
		//layoutCuisine.setVisibility(View.GONE);
		

	}
	
	

	
	private void loadData() {
		showLoader();
		
		// new LoadPlaces().execute();
		
		StringRequest postRequest = new StringRequest(Request.Method.POST, LetsEatRequest.EVENT_REST_LIST_URL,
		        new Response.Listener<String>() {
		            @Override
		            public void onResponse(String response) {
		                try {
		                	// Result handling 
		                	resturentList = new LetsEatRequest().getFeaturedResturentDetails(response);

		        			// dismiss the dialog after getting all products
		        			//pDialog.dismiss();
		        			//findViewById(R.id.main_custom_progress).setVisibility(View.GONE);
		        			AnimationTween.animateView(findViewById(R.id.main_custom_progress), FeaturedRestaurantListView.this);
		        			AnimationTween.animateViewPosition(lv, FeaturedRestaurantListView.this);
		        			// updating UI from Background Thread
		        			runOnUiThread(new Runnable() {
		        				public void run() {
		        					/**
		        					 * Updating parsed Places into LISTVIEW
		        					 * */
		        					// Get json response status

		        					if (resturentList == null || resturentList.size() <= 0) {

		        						
		        							findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);						
		        							View v=findViewById(R.id.main_custom_progress);
		        							TextView textPreparing=(TextView) v.findViewById(R.id.textPreparing);
		        							textPreparing.setText("Nothing found.");
		        							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
		        							AnimationTween.animateViewPosition(v, FeaturedRestaurantListView.this);
		        							
		        						 
		        					} else if (resturentList != null) {
		        						// Check for all possible status

		        						// Successfully got places details
		        						if (resturentList != null) {
		        							// loop through each place
		        							for (ResturentFeatured p : resturentList) {

		        								HashMap<String, String> map = new HashMap<String, String>();
		        								map.put("Id", p.Id);
		        								// Place name
		        								/*map.put("RestName", p.RestName);
		        								map.put("RestLoc", p.RestLoc+", "+p.RestDistName);*/
		        								
		        								totalPage=Integer.parseInt("0");
		        								
		        								map.put("RestName", p.RestName);
		        								map.put("DisFrom", p.RestLoc+", "+p.RestDistName);//;p.DisFrom);
		        								map.put("cusine", p.Cusine);
		        								map.put("DisAmount", p.DisAmount);
		        								map.put("IMG", p.IMG);
		        								
		        								// adding HashMap to ArrayList
		        								resturantListItems.add(map);
		        							}
		        							// list adapter
		        							/*MyListAdapter adapter = new MyListAdapter(
		        									FeaturedRestaurantListView.this,
		        									resturantListItems, 
		        									new String[] { "RestName", "RestLoc", "Id" });*/
		        							//MyListAdapterImage adapter= new MyListAdapterImage(FeaturedRestaurantListView.this, resturantListItems, new String[]{"RestName","DisFrom","cusine","DisAmount","IMG"});
											EventRestaurantAdapter adapter = new EventRestaurantAdapter(FeaturedRestaurantListView.this, resturentList);
		        							

		        							// Adding data into listview
		        							lv.setAdapter(adapter);
		        							adapter.notifyDataSetChanged();
		        							currentPage++;
		        							
		        							lv.setOnScrollListener(new OnScrollListener() {
		        								
		        								@Override
		        								public void onScrollStateChanged(AbsListView view, int scrollState) {
		        									// TODO Auto-generated method stub
		        									
		        								}
		        								
		        								@Override
		        								public void onScroll(AbsListView view, int firstVisibleItem,
		        										int visibleItemCount, int totalItemCount) {
		        									// TODO Auto-generated method stub
		        									boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;

		        								        if(loadMore && currentPage<totalPage) {
		        								        	Toast.makeText(getApplicationContext(), "load More", Toast.LENGTH_SHORT).show();
		        								        	
		        								        	new LoadPlaces().execute();
		        								        	
		        								        }
		        									}
		        							});
		        							
		        							
		        							
		        						}

		        						else if (resturentList == null) {
		        							// Zero results found
		        							//alert.showAlertDialog(FeaturedRestaurantListView.this,getString(R.string.app_name),	" No Restaurant found.", false);
		        							findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);						
		        							View v=findViewById(R.id.main_custom_progress);
		        							TextView textPreparing=(TextView) v.findViewById(R.id.textPreparing);
		        							textPreparing.setText("Nothing found.");
		        							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
		        							AnimationTween.animateViewPosition(v, FeaturedRestaurantListView.this);
		        						}
		        					}
		        				}

		        			});

		        		
		                	
		    		    	
		                } catch (Exception e) {
		                	
		                	
		                    e.printStackTrace();
		                }
		            }
		        },
		        new Response.ErrorListener() {
		            @Override
		            public void onErrorResponse(VolleyError error) {
		                error.printStackTrace();
		            }
		        }
		) {
		    @Override
		    protected Map<String, String> getParams()
		    {
		        Map<String, String>  params = new HashMap<String, String>();
		        // the POST parameters:
		        params.put("EventId", EventId);
		        return params;
		    }
		};
		
		Volley.newRequestQueue(this).add(postRequest);
		
	}




	private void showLoader() {
		// TODO Auto-generated method stub
		lv.setOnScrollListener(null);
		
		pDialog = new MyProgressDialog(FeaturedRestaurantListView.this);
		//pDialog.setMessage(Html.fromHtml("<b>Loading ...</b>"));
		//pDialog.setIndeterminate(false);
		pDialog.setCancelable(false);
		//pDialog.show();
		findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);
	}




	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
	
		case R.id.action_search:
			// search action
			RelativeLayout relativeLayout=(RelativeLayout)findViewById(R.id.rest_list_root_layout);		
			addSearchLayout(relativeLayout);			
			return true;
		
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	

	@Override
	public void onPause() {
		super.onPause();

		if (adView != null)
			adView.pause();

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	protected void onStart() {
		super.onStart();
		
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		
		if (adView != null)
			{adView.destroy();}
		
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
		
	}

	class LoadPlaces extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			
			super.onPreExecute();
			showLoader();
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {

						
				resturentList = foodyResturent.getFeaturedResturentDetails(EventId);
				

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			//pDialog.dismiss();
			//findViewById(R.id.main_custom_progress).setVisibility(View.GONE);
			AnimationTween.animateView(findViewById(R.id.main_custom_progress), FeaturedRestaurantListView.this);
			AnimationTween.animateViewPosition(lv, FeaturedRestaurantListView.this);
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status

					if (resturentList == null || resturentList.size() <= 0) {

						
							findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);						
							View v=findViewById(R.id.main_custom_progress);
							TextView textPreparing=(TextView) v.findViewById(R.id.textPreparing);
							textPreparing.setText("Nothing found.");
							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
							AnimationTween.animateViewPosition(v, FeaturedRestaurantListView.this);
							
						 
					} else if (resturentList != null) {
						// Check for all possible status

						// Successfully got places details
						if (resturentList != null) {
							// loop through each place
							for (ResturentFeatured p : resturentList) {

								HashMap<String, String> map = new HashMap<String, String>();
								map.put("Id", p.Id);
								// Place name
								/*map.put("RestName", p.RestName);
								map.put("RestLoc", p.RestLoc+", "+p.RestDistName);*/
								
								totalPage=Integer.parseInt("0");
								
								map.put("RestName", p.RestName);
								map.put("DisFrom", p.RestLoc+", "+p.RestDistName);//;p.DisFrom);
								map.put("cusine", p.Cusine);
								map.put("DisAmount", p.DisAmount);
								map.put("IMG", p.IMG);
								
								// adding HashMap to ArrayList
								resturantListItems.add(map);
							}
							// list adapter
							/*MyListAdapter adapter = new MyListAdapter(
									FeaturedRestaurantListView.this,
									resturantListItems, 
									new String[] { "RestName", "RestLoc", "Id" });*/
							MyListAdapterImage adapter= new MyListAdapterImage(FeaturedRestaurantListView.this, resturantListItems, new String[]{"RestName","DisFrom","cusine","DisAmount","IMG"});
							

							// Adding data into listview
							lv.setAdapter(adapter);
							adapter.notifyDataSetChanged();
							currentPage++;
							
							lv.setOnScrollListener(new OnScrollListener() {
								
								@Override
								public void onScrollStateChanged(AbsListView view, int scrollState) {
									// TODO Auto-generated method stub
									
								}
								
								@Override
								public void onScroll(AbsListView view, int firstVisibleItem,
										int visibleItemCount, int totalItemCount) {
									// TODO Auto-generated method stub
									boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;

								        if(loadMore && currentPage<totalPage) {
								        	Toast.makeText(getApplicationContext(), "load More", Toast.LENGTH_SHORT).show();
								        	
								        	new LoadPlaces().execute();
								        	
								        }
									}
							});
							
							
							
						}

						else if (resturentList == null) {
							// Zero results found
							//alert.showAlertDialog(FeaturedRestaurantListView.this,getString(R.string.app_name),	" No Restaurant found.", false);
							findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);						
							View v=findViewById(R.id.main_custom_progress);
							TextView textPreparing=(TextView) v.findViewById(R.id.textPreparing);
							textPreparing.setText("Nothing found.");
							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
							AnimationTween.animateViewPosition(v, FeaturedRestaurantListView.this);
						}
					}
				}

			});

		}

	}
	
	
	
	

	@Override
	public boolean onNavigationItemSelected(int arg0, long arg1) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	private void showPopUp() {

		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
		// helpBuilder.setTitle("Pop Up");
		helpBuilder.setMessage("Nothing Found. Want to request this suggestion?");
		helpBuilder.setPositiveButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {

					}
				});

		helpBuilder.setNegativeButton("Yes",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						Intent in = new Intent(
								getApplicationContext(),
								SuggetionRequest.class);
						in.putExtra("keyword", searchKeywords);

						startActivity(in);
					}
				});

		// Remember, create doesn't show the dialog
		AlertDialog helpDialog = helpBuilder.create();
		helpDialog.show();

	}

	
}
