package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.foody.jsondata.Event;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.LetsEatRequest;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.mcc.letseat.MyProgressDialog;
import com.mcc.letseat.R;
import com.mcc.libs.ActivityWithSliding;
import com.mcc.libs.AnimationTween;

/**
 * 
 * @author Arif
 * List of current events
 */
public class EventListActivity extends ActivityWithSliding{
	


	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Google Places
	FoodyResturent foodyResturent;

	// Event List
	ArrayList<Event> eventList;



	// Button
	ImageView btnShowOnMap;

	// Progress dialog
	MyProgressDialog pDialog;

	// Places Listview
	ListView lv;

	// ListItems data
	ArrayList<HashMap<String, String>> eventListItems = new ArrayList<HashMap<String, String>>();
	
	// LayoutCuisine
	LinearLayout layoutCuisine;

	// //admob
	AdView adView = null;

	ImageView searchAny;

	String searchKeywords;
	
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_events);


		// Getting listview
		lv = (ListView) findViewById(R.id.listEvent);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				Event r=eventList.get(position);
				
				//analytics
				analyticsSendScreenView("User Choose event: "+r.EVNAME);
				
				//restaurentId= resturent.Id;
				
				Intent in = new Intent(getApplicationContext(),FeaturedRestaurantListView.class);
				in.putExtra( "event" , r);
				
				if(r.STARTDATE.equals(r.ENDDATE))
					in.putExtra("date", r.STARTDATE);
					else 
						in.putExtra("date", r.STARTDATE+" to "+r.ENDDATE );
				
				startActivity(in);
				
				
				
				
			}
		});
		
		

		adView = (AdView) this.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder().build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});
		
		
		
	loadData();
		
		
	//set home button
	setHomeButton(this);	

	}

	
	private void loadData() {
		// TODO Auto-generated method stub
		//new LoadPlaces().execute();
		
		showLoader();
		
		StringRequest postRequest = new StringRequest(Request.Method.POST, LetsEatRequest.EVENT_LIST_URL,
		        new Response.Listener<String>() {
		            @Override
		            public void onResponse(String response) {
		                try {
		                	// Result handling 
		                	eventList=new LetsEatRequest().getEvents(response);
		                	//populate and show data

		        			// dismiss the dialog after getting all products
		        			//pDialog.dismiss();
		        			//findViewById(R.id.main_custom_progress).setVisibility(View.GONE);
		        			AnimationTween.animateView(findViewById(R.id.events_custom_progress), EventListActivity.this);
		        			AnimationTween.animateViewPosition(lv, EventListActivity.this);
		        			// updating UI from Background Thread
		        			runOnUiThread(new Runnable() {
		        				public void run() {
		        					/**
		        					 * Updating parsed Places into LISTVIEW
		        					 * */
		        					// Get json response status

		        					if (eventList == null || eventList.size() <= 0) {

		        						
		        							findViewById(R.id.events_custom_progress).setVisibility(View.VISIBLE);						
		        							View v=findViewById(R.id.events_custom_progress);
		        							TextView textPreparing=(TextView) v.findViewById(R.id.textPreparing);
		        							textPreparing.setText("Nothing found.");
		        							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
		        							AnimationTween.animateViewPosition(v, EventListActivity.this);
		        							
		        						 
		        					} else if (eventList != null) {
		        						// Check for all possible status

		        						// Successfully got places details
		        						if (eventList != null) {

		        							// list adapter
		        							MyListAdapterEvents adapter = new MyListAdapterEvents(
		        									EventListActivity.this,eventList);

		        							// Adding data into listview
		        							lv.setAdapter(adapter);
		        							adapter.notifyDataSetChanged();
		        							
		        							
		        						}

		        						else if (eventList == null) {
		        							// Zero results found
		        							//alert.showAlertDialog(FeaturedRestaurantListView.this,getString(R.string.app_name),	" No Restaurant found.", false);
		        							findViewById(R.id.events_custom_progress).setVisibility(View.VISIBLE);						
		        							View v=findViewById(R.id.events_custom_progress);
		        							TextView textPreparing=(TextView) v.findViewById(R.id.textPreparing);
		        							textPreparing.setText("Nothing found.");
		        							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
		        							AnimationTween.animateViewPosition(v, EventListActivity.this);
		        						}
		        					}
		        				}

		        			});

		        		
		                } catch (Exception e) {
		                	
		                	
		                    e.printStackTrace();
		                }
		            }
		        },
		        new Response.ErrorListener() {
		            @Override
		            public void onErrorResponse(VolleyError error) {
		                error.printStackTrace();
		            }
		        }
		) {
		    @Override
		    protected Map<String, String> getParams()
		    {
		        Map<String, String>  params = new HashMap<String, String>();
		        // the POST parameters:
		        params.put("LocId", "7");
		        params.put("CusId", "35");
		        return params;
		    }
		};
		
		Volley.newRequestQueue(this).add(postRequest);
	}
	
	private void showLoader(){
		lv.setOnScrollListener(null);
		pDialog = new MyProgressDialog(EventListActivity.this);
		//pDialog.setMessage(Html.fromHtml("<b>Loading ...</b>"));
		//pDialog.setIndeterminate(false);
		pDialog.setCancelable(false);
		//pDialog.show();
		findViewById(R.id.events_custom_progress).setVisibility(View.VISIBLE);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
	
		case R.id.action_search:
			// search action
			RelativeLayout relativeLayout=(RelativeLayout)findViewById(R.id.rest_list_root_layout);		
			addSearchLayout(relativeLayout);			
			return true;
		
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	

	@Override
	public void onPause() {
		super.onPause();

		if (adView != null)
			adView.pause();

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		
		if (adView != null)
			{adView.destroy();}
		
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
		
	}

	class LoadPlaces extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			lv.setOnScrollListener(null);
			super.onPreExecute();
			pDialog = new MyProgressDialog(EventListActivity.this);
			//pDialog.setMessage(Html.fromHtml("<b>Loading ...</b>"));
			//pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			//pDialog.show();
			findViewById(R.id.events_custom_progress).setVisibility(View.VISIBLE);
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {

							
				eventList = foodyResturent.getEvents("");
				

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			//pDialog.dismiss();
			//findViewById(R.id.main_custom_progress).setVisibility(View.GONE);
			AnimationTween.animateView(findViewById(R.id.events_custom_progress), EventListActivity.this);
			AnimationTween.animateViewPosition(lv, EventListActivity.this);
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status

					if (eventList == null || eventList.size() <= 0) {

						
							findViewById(R.id.events_custom_progress).setVisibility(View.VISIBLE);						
							View v=findViewById(R.id.events_custom_progress);
							TextView textPreparing=(TextView) v.findViewById(R.id.textPreparing);
							textPreparing.setText("Nothing found.");
							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
							AnimationTween.animateViewPosition(v, EventListActivity.this);
							
						 
					} else if (eventList != null) {
						// Check for all possible status

						// Successfully got places details
						if (eventList != null) {

							// list adapter
							MyListAdapterEvents adapter = new MyListAdapterEvents(
									EventListActivity.this,
									eventList);

							// Adding data into listview
							lv.setAdapter(adapter);
							adapter.notifyDataSetChanged();
							
							
						}

						else if (eventList == null) {
							// Zero results found
							//alert.showAlertDialog(FeaturedRestaurantListView.this,getString(R.string.app_name),	" No Restaurant found.", false);
							findViewById(R.id.events_custom_progress).setVisibility(View.VISIBLE);						
							View v=findViewById(R.id.events_custom_progress);
							TextView textPreparing=(TextView) v.findViewById(R.id.textPreparing);
							textPreparing.setText("Nothing found.");
							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
							AnimationTween.animateViewPosition(v, EventListActivity.this);
						}
					}
				}

			});

		}

	}
	
	
	
	


	
	private void showPopUp() {

		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
		// helpBuilder.setTitle("Pop Up");
		helpBuilder.setMessage("Nothing Found. Want to request this suggestion?");
		helpBuilder.setPositiveButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {

					}
				});

		helpBuilder.setNegativeButton("Yes",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						Intent in = new Intent(
								getApplicationContext(),
								SuggetionRequest.class);
						in.putExtra("keyword", searchKeywords);

						startActivity(in);
					}
				});

		// Remember, create doesn't show the dialog
		AlertDialog helpDialog = helpBuilder.create();
		helpDialog.show();

	}

	


}
