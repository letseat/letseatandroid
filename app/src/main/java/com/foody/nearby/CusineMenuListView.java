package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils.TruncateAt;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView.ScaleType;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.foody.AppData.StaticObjects;
import com.foody.jsondata.CusineMenu;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.LetsEatRequest;
import com.foody.jsondata.SearchResturent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.mcc.letseat.R;
import com.mcc.letseat.Adapter.CuisineRestAdapter;
import com.mcc.letseat.tab.TabMainActivity;
import com.mcc.libs.ActivityWithSliding;
import com.mcc.libs.AnimationTween;
import com.mcc.libs.CF;
import com.mcc.user.SavedPrefernce;
import com.squareup.picasso.Picasso;

/**
 * 
 * @author Arif
 * List of Restaurant in a particular cuisine in selected location
 */
public class CusineMenuListView extends ActivityWithSliding implements
		ActionBar.OnNavigationListener {

	

	// Google Places
	FoodyResturent foodyResturent;

	// Places List
	ArrayList<CusineMenu> cusineMenuList;

	// Button
	ImageView btnShowOnMap;

	

	// Places Listview
	ListView lv;

	// ListItems data
	ArrayList<HashMap<String, String>> CusineMenuListItems = new ArrayList<HashMap<String, String>>();

	// //admob
	AdView adView = null;

	ImageView searchAny;

	String CusId;
	String LocId;
	String cuisineName;
	
	int adjustPosition=0;

	private int offSet;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cuisine_rest_list);

		

		// Getting listview
		lv = (ListView) findViewById(R.id.list_cat);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				/*CuisineRestAdapter adapter=((CuisineRestAdapter)((HeaderViewListAdapter)lv.getAdapter()).getWrappedAdapter());								
				String resid = adapter.getItem(position+offSet).Id;*/
				String resid =((CusineMenu)lv.getItemAtPosition(position)).Id;
				Intent in = new Intent(getApplicationContext(),TabMainActivity.class);
				in.putExtra("Id", resid);				
				

				startActivity(in);
			}
		});
		
		//==================
		
		
		
		
		
		
		adView = (AdView) this.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder().build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});
		
		CusId=getIntent().getStringExtra("id");
		cuisineName=getIntent().getStringExtra("cuisineName");
		
		//add cuisine header
		String img=getIntent().getStringExtra("img");		
		View listHeaderView=addHeader(cuisineName, img );
		
		//set up sort and filter
		// filter list
		ArrayList<String> filterBylist = new ArrayList<String>();
		filterBylist.add("Clear Filter");
		filterBylist.add("Open Now");
		filterBylist.add("Deals Available");
		
		//sort
		ArrayList<String> sortBylist = new ArrayList<String>();	
		sortBylist.add("Name (A-Z)");
		sortBylist.add("Name (Z-A)");
		sortBylist.add("Lowest Cost First");
		sortBylist.add("Highest Cost First");
		sortBylist.add("Highest Capacity First");
		sortBylist.add("Lowest Capacity First");
		sortBylist.add("Highest Rating First");
		
		Typeface tfLight= StaticObjects.gettfLight(this);
		setFilterSortSpinner(listHeaderView, tfLight, filterBylist, sortBylist);
		
		
		SavedPrefernce savedPrefernce = new SavedPrefernce(this);
		LocId = savedPrefernce.getLocationId();
		
		loadData();
		
		

		//handleIntent(getIntent());
		
		analyticsSendScreenView("Cuisine "+cuisineName);
		
		//set home button
		setHomeButton(this);

	}
	
	private void loadData(){
		//new LoadPlaces().execute();
		
		StringRequest postRequest = new StringRequest(Request.Method.POST, LetsEatRequest.CUSINE_MENU_URL,
		        new Response.Listener<String>() {
		            @Override
		            public void onResponse(String response) {
		                try {
		                	// Result handling 
		                	cusineMenuList = new LetsEatRequest().getCusineMenu(response);
		                	
		        			// hide the progressBar after getting all data
		        			//findViewById(R.id.cat_custom_progress).setVisibility(View.GONE);
		        			
		        			AnimationTween.animateView(findViewById(R.id.cat_custom_progress), CusineMenuListView.this);
		        			AnimationTween.animateViewPosition(lv, CusineMenuListView.this);
		        			
		        			// updating UI from Background Thread
		        			runOnUiThread(new Runnable() {
		        				public void run() {
		        					/**
		        					 * Updating parsed Places into LISTVIEW
		        					 * */
		        					// Get json response status					
		        					if (cusineMenuList == null || cusineMenuList.size() <= 0) {

		        						
		        						//show message
		        						findViewById(R.id.cat_custom_progress).setVisibility(View.VISIBLE);						
		        						View v=findViewById(R.id.cat_custom_progress);
		        						TextView textPreparing=(TextView) v.findViewById(R.id.textPreparing);
		        						textPreparing.setText(cuisineName+" cuisine is not available in this location, please try another cuisine or change location. You can also suggest a restaurant offering "+cuisineName+" cuisine in this location.\n\n\n                           FOR SUGGETION TAP HERE");
		        						v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
		        						AnimationTween.animateViewPosition(v, CusineMenuListView.this);
		        						v.setOnClickListener(new View.OnClickListener() {
		        							
		        							@Override
		        							public void onClick(View v) {
		        								Intent in = new Intent(getApplicationContext(),SuggetionRequest.class);
		        								in.putExtra("keyword", "");
		        								startActivity(in);
		        								
		        							}
		        						});
		        						 
		        					} else if (cusineMenuList != null) {
		        						// Check for all possible status

		        						// Successfully got places details
		        						if (cusineMenuList != null) {
		        							
		        							//search suggestion
		        							ArrayList<SearchResturent> searchRestList= new ArrayList<SearchResturent>(); 
		        							
		        							// loop through each place
		        							for (CusineMenu p : cusineMenuList) {

		        								HashMap<String, String> map = new HashMap<String, String>();
		        								
		        								
		        								map.put("Id", p.Id);
		        								map.put("RestName", p.restaurent);
		        								map.put("Rating", "0");
		        								map.put("DisFrom", p.RestLoc+", "+p.RestDistName);
		        								
		        								map.put("DisAmount", "1" );
		        								map.put("IMG", p.IMG);

		        								

		        								// adding HashMap to ArrayList
		        								CusineMenuListItems.add(map);
		        								p.setOpenOrClose(CF.openOrClose(p.Open, p.Close));
		        								//search suggestion
		        								searchRestList.add(new SearchResturent(p.Id, p.restaurent, null, p.RestLoc, null, null, null));
		        							}
		        							
		        							//search suggestion
		        							addDataToDB(searchRestList);
		        							
		        							// list adapter
		        							//MyListAdapter adapter = new MyListAdapter(CusineMenuListView.this,CusineMenuListItems,new String[] { "restaurent", "RestLoc" });
		        							
		        							CuisineRestAdapter adapter = new CuisineRestAdapter(CusineMenuListView.this, cusineMenuList); 
		        							//ListAdapterRestaurantImage adapter= new ListAdapterRestaurantImage(CusineMenuListView.this, CusineMenuListItems, new String[]{"RestName","DisFrom","Rating","DisAmount","IMG"});
		        							/*R.layout.list_item_single,
		        							new String[] { "categoryname" },
		        							new int[] {  R.id.name });*/

		        							// Adding data into listview
		        							lv.setAdapter(adapter);
		        						}

		        						else if (cusineMenuList == null) {
		        							
		        							//show message
		        							findViewById(R.id.cat_custom_progress).setVisibility(View.VISIBLE);						
		        							View v=findViewById(R.id.cat_custom_progress);
		        							TextView textPreparing=(TextView) v.findViewById(R.id.textPreparing);
		        							textPreparing.setText("Nothing found.");
		        							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
		        							AnimationTween.animateViewPosition(v, CusineMenuListView.this);
		        						}
		        					}
		        				}

		        			});

		        		
		                } catch (Exception e) {
		                	 
		                    e.printStackTrace();
		                }
		            }
		        },
		        new Response.ErrorListener() {
		            @Override
		            public void onErrorResponse(VolleyError error) {
		                error.printStackTrace();
		            }
		        }
		) {
		    @Override
		    protected Map<String, String> getParams()
		    {
		        Map<String, String>  params = new HashMap<String, String>();
		        // the POST parameters:
		        params.put("LocId", LocId);
		        params.put("CusId", CusId);
		        return params;
		    }
		};
				Volley.newRequestQueue(this).add(postRequest);
	}
	
	
	//Cuisine list header
	View addHeader(String cusine, String img){
		//as header added offset is 1
		offSet=1;
		LayoutInflater li = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = li.inflate(R.layout.cuisine_item_header, null);

		ImageView imageView=(ImageView)v.findViewById(R.id.imageView_cusineType);;
		TextView textView =(TextView)v.findViewById(R.id.textView_cusineType);;
		
		//String cusineType= _filePaths.get(position).get(listTags[1]);
		
		textView.setText(cusine);
		textView.setTypeface(StaticObjects.gettfNormal(this));
		imageView.setScaleType(ScaleType.CENTER_CROP);
		
		
		if(cusine!=null && !cusine.isEmpty()){
			Picasso.with(this)
		    .load(img)
		    .placeholder(R.drawable.sample_res_photo)
		    .error(R.drawable.sample_res_photo)
		    .into(imageView);
		}
		
		lv.addHeaderView(v, "cusine", false);
		adjustPosition=1;
		
		return v;
	}
	
	
	
	private void setFilterSortSpinner(View spinnerHolder, final Typeface tfLight, ArrayList<String> filterBylist, ArrayList<String> sortBylist){
		Spinner spinnerFilter = (Spinner) spinnerHolder.findViewById(R.id.spinner_offer);		
		ArrayAdapter<String> adapter_offer = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, filterBylist) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {

				View v = super.getView(position, convertView, parent);
				((TextView) v).setTypeface(tfLight);
				((TextView) v).setEllipsize(TruncateAt.END);

				return v;
			}

			@Override
			public View getDropDownView(int position, View convertView,
					ViewGroup parent) {

				View v = super.getDropDownView(position, convertView, parent);
				((TextView) v).setTypeface(tfLight);
				return v;
			}

		};
		// Specify the layout to use when the list of choices appears
		adapter_offer.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinnerFilter.setAdapter(adapter_offer);
		spinnerFilter.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,int pos, long id) {
				
				if(pos ==0 && parent != null){
					if( parent.getChildAt(0) instanceof TextView)
					((TextView) parent.getChildAt(0)).setText("Not Filtered");
					
				}
				
				CuisineRestAdapter adapter=((CuisineRestAdapter)((HeaderViewListAdapter)lv.getAdapter()).getWrappedAdapter());
				
				if(pos==0)
					{	adapter.getFilter().filter("");}
				
					adapter.getFilter().filter(""+pos);
					lv.setSelectionAfterHeaderView();


			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});
		
		
		// offer_provider_list.add("Others");

		Spinner spinnerSort = (Spinner) spinnerHolder.findViewById(R.id.spinner_sort);		
		ArrayAdapter<String> adapter_sort = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, sortBylist) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {

				View v = super.getView(position, convertView, parent);
				((TextView) v).setTypeface(tfLight);
				((TextView) v).setEllipsize(TruncateAt.END);

				return v;
			}

			@Override
			public View getDropDownView(int position, View convertView,
					ViewGroup parent) {

				View v = super.getDropDownView(position, convertView, parent);
				((TextView) v).setTypeface(tfLight);
				return v;
			}

		};
		// Specify the layout to use when the list of choices appears
		adapter_sort.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinnerSort.setAdapter(adapter_sort);
		spinnerSort.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,int pos, long id) {			
				//CuisineRestAdapter adapter= (CuisineRestAdapter)lv.getAdapter();
				
				CuisineRestAdapter adapter=((CuisineRestAdapter)((HeaderViewListAdapter)lv.getAdapter()).getWrappedAdapter());
				
				if(adapter ==null){return;}								
				adapter.setSortBY(pos);	
				lv.setSelectionAfterHeaderView();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});
	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
	
		case R.id.action_search:
			// search action
			RelativeLayout relativeLayout=(RelativeLayout)findViewById(R.id.cat_root_layout);		
			addSearchLayout(relativeLayout);			
			return true;
		
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	
	

	@Override
	protected void onNewIntent(Intent intent) {
		setIntent(intent);
		//handleIntent(intent);
	}

	

	@Override
	public void onPause() {
		super.onPause();

		if (adView != null)
			adView.pause();

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		
		if (adView != null)
			{adView.destroy();}
		
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}
	
	@Override
	protected void onStart() {	
		super.onStart();
		
		//analytics stop tracking
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}
	
/*	class LoadPlaces extends AsyncTask<String, String, String> {

		*//**
		 * Before starting background thread Show Progress Dialog
		 * *//*
		@Override
		protected void onPreExecute() {
			super.onPreExecute();			
			findViewById(R.id.cat_custom_progress).setVisibility(View.VISIBLE);
		}

		*//**
		 * getting Places JSON
		 * *//*
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {
				cusineMenuList = foodyResturent.getCusineMenu(FoodyResturent.TYPE_LOAD_LOC,LocId, CusId, CusineMenuListView.this);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		*//**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **//*
		protected void onPostExecute(String file_url) {
			// hide the progressBar after getting all data
			//findViewById(R.id.cat_custom_progress).setVisibility(View.GONE);
			
			AnimationTween.animateView(findViewById(R.id.cat_custom_progress), CusineMenuListView.this);
			AnimationTween.animateViewPosition(lv, CusineMenuListView.this);
			
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					*//**
					 * Updating parsed Places into LISTVIEW
					 * *//*
					// Get json response status					
					if (cusineMenuList == null || cusineMenuList.size() <= 0) {

						
						//show message
						findViewById(R.id.cat_custom_progress).setVisibility(View.VISIBLE);						
						View v=findViewById(R.id.cat_custom_progress);
						TextView textPreparing=(TextView) v.findViewById(R.id.textPreparing);
						textPreparing.setText(cuisineName+" cuisine is not available in this location, please try another cuisine or change location. You can also suggest a restaurant offering "+cuisineName+" cuisine in this location.\n\n\n                           FOR SUGGETION TAP HERE");
						v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
						AnimationTween.animateViewPosition(v, CusineMenuListView.this);
						v.setOnClickListener(new View.OnClickListener() {
							
							@Override
							public void onClick(View v) {
								Intent in = new Intent(getApplicationContext(),SuggetionRequest.class);
								in.putExtra("keyword", "");
								startActivity(in);
								
							}
						});
						 
					} else if (cusineMenuList != null) {
						// Check for all possible status

						// Successfully got places details
						if (cusineMenuList != null) {
							
							//search suggestion
							ArrayList<SearchResturent> searchRestList= new ArrayList<SearchResturent>(); 
							
							// loop through each place
							for (CusineMenu p : cusineMenuList) {

								HashMap<String, String> map = new HashMap<String, String>();
								
								
								map.put("Id", p.Id);
								map.put("RestName", p.restaurent);
								map.put("Rating", "0");
								map.put("DisFrom", p.RestLoc+", "+p.RestDistName);
								
								map.put("DisAmount", "1" );
								map.put("IMG", p.IMG);

								

								// adding HashMap to ArrayList
								CusineMenuListItems.add(map);
								p.setOpenOrClose(CF.openOrClose(p.Open, p.Close));
								//search suggestion
								searchRestList.add(new SearchResturent(p.Id, p.restaurent, null, p.RestLoc, null, null, null));
							}
							
							//search suggestion
							addDataToDB(searchRestList);
							
							// list adapter
							//MyListAdapter adapter = new MyListAdapter(CusineMenuListView.this,CusineMenuListItems,new String[] { "restaurent", "RestLoc" });
							
							CuisineRestAdapter adapter = new CuisineRestAdapter(CusineMenuListView.this, cusineMenuList); 
							//ListAdapterRestaurantImage adapter= new ListAdapterRestaurantImage(CusineMenuListView.this, CusineMenuListItems, new String[]{"RestName","DisFrom","Rating","DisAmount","IMG"});
							R.layout.list_item_single,
							new String[] { "categoryname" },
							new int[] {  R.id.name });

							// Adding data into listview
							lv.setAdapter(adapter);
						}

						else if (cusineMenuList == null) {
							
							//show message
							findViewById(R.id.cat_custom_progress).setVisibility(View.VISIBLE);						
							View v=findViewById(R.id.cat_custom_progress);
							TextView textPreparing=(TextView) v.findViewById(R.id.textPreparing);
							textPreparing.setText("Nothing found.");
							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
							AnimationTween.animateViewPosition(v, CusineMenuListView.this);
						}
					}
				}

			});

		}

	}*/
	
	
	
	

	
	

	@Override
	public boolean onNavigationItemSelected(int arg0, long arg1) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	private void showPopUp() {

		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
		// helpBuilder.setTitle("Pop Up");
		helpBuilder.setMessage("Nothing Found. Want to request this suggestion?");
		helpBuilder.setPositiveButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {

					}
				});

		helpBuilder.setNegativeButton("Yes",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						Intent in = new Intent(
								getApplicationContext(),
								SuggetionRequest.class);
						in.putExtra("keyword", CusId);

						startActivity(in);
					}
				});

		// Remember, create doesn't show the dialog
		AlertDialog helpDialog = helpBuilder.create();
		helpDialog.show();

	}

}
