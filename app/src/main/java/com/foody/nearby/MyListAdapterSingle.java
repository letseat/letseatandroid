package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.foody.AppData.StaticObjects;
import com.mcc.letseat.R;

/**
 * Custom data adapter single item(TextView) ListView
 */
public class MyListAdapterSingle extends BaseAdapter{
	
	ArrayList<HashMap<String, String>> aa;
	Context c;
	String listTag;
	
	public MyListAdapterSingle(Context c, ArrayList<HashMap<String, String>> aa, String listTag ){
		this.aa = aa;
		this.c = c;
		this.listTag=listTag;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return aa.size();
	}

	@Override
	public String getItem(int arg0) {
		// TODO Auto-generated method stub
		return aa.get(arg0).get("name");
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	private int lastPosition = -1;

	@Override
	public View getView(int pos, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		
		View v;
		v = arg1;
		LayoutInflater li = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		v = li.inflate(R.layout.list_item_single, null);
		
		LinearLayout root = (LinearLayout)v.findViewById(R.id.ltnbTitle);
		
		TextView name= (TextView)v.findViewById(R.id.first);	
		Typeface tf = Typeface.createFromAsset(c.getAssets(), StaticObjects.fontPath_SEGOEUI);	
		name.setTypeface(tf);
		
		if(pos%2==0)
		{
			root.setBackgroundColor(Color.parseColor("#00FCFCFC"));
		}
		else
		{
			root.setBackgroundColor(Color.parseColor("#00FFFFFF"));
		}
		
		//String ww =aa.get(pos);
		name.setText(aa.get(pos).get(listTag));
		
		//apply animation on current view
		 Animation animation = AnimationUtils.loadAnimation(c, (pos > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
		 v.startAnimation(animation);
		 lastPosition = pos;
		
		
		return v;
	}

}
