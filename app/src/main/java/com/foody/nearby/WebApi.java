package com.foody.nearby;



import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

/**
 * used to post various task 
 */
public class WebApi {
	public HashMap<String, Integer> getCategoryMap(){
		HashMap<String, Integer> temp = new HashMap<String, Integer>() ;
//		String response = getResponse(AppUrl.categoryUrl) ;
//		/*
//		 * parsing category 
//		 */
//		String [] cats = response.split("<br>") ;
//		int szCats = cats.length ;
//		AppData.catNames = new String[szCats] ;
//		
//		for (int i = 0 ; i < szCats ; i++){
//			String t = cats[i] ;
//			String [] codes = t.split("_") ;
//			Integer catCode = Integer.parseInt(codes[1]) ;
//			String catName = new String(codes[0]) ;
//			temp.put(catName, catCode) ;
//			AppData.catNames[i] = new String(catName) ;
//		}
//		Log.e("CATEGORY RESPONSE", response) ;
		return temp ;
	}
	
	public boolean login(String user, String pass){
		return false ;
	}
	
	
	/*
	 * Utility Method
	 * Performing Request-Response GET format in order to complete a API
	 * Input: Url
	 * Output: Response String containing JSON or XML
	 */
	public String getResponse(String url){
		String response = "" ;
		try{
			/* prepare request */
			HttpClient client = new DefaultHttpClient() ;
			HttpGet req = new HttpGet(url) ;
			/* execute to get response */
			HttpResponse res = client.execute(req) ;
			/* read response stream */
			response = readResponse(res) ;
		}catch(Exception e){
			//Log.e(this.getClass().toString() + "Get Response", e.toString()) ;
			e.printStackTrace() ;
			return "" ;
		}
		return response ;
	}
	/*
	 * Utility Method
	 * Performing Request-Response POST format in order to complete a API
	 * Input: Url
	 * Output: Response String containing JSON or XML
	 */
	public String postFeedBack(String url, ArrayList<NameValuePair>params){
		String response = "" ;
		try{
			/* prparing request */
			HttpClient client = new DefaultHttpClient() ;
			HttpPost req = new HttpPost(url) ;
			/* execute to get response */
			req.setEntity(new UrlEncodedFormEntity(params)) ;
			//for (int i = 0 ; i < params.size() ; i++)
				//Log.e(params.get(i).getName(), params.get(i).getValue()) ;
			HttpResponse res = client.execute(req) ;
			
			/* read response stream */
			response = readResponse(res) ;
			//Log.e("FEEDBACK RESPONSE", response) ;
		}catch(Exception e){
			e.printStackTrace() ;
			return "" ;
		}
		return response ;
	}

		/*
		 * Utility Method
		 * Read Response Stream from a Http Response and return it as string
		 * Input: Response Object
		 * Output:Response String  
		 */
		private String readResponse(HttpResponse res){
			String response = "" ;
			int i = 0 ; 
			try{
				BufferedReader rd = new BufferedReader(
				    new InputStreamReader(res.getEntity().getContent())) ;
				       
				String line = "" ;
				while ((line = rd.readLine()) != null){
					//Log.e("IN READ", line) ;
					if (i == 0)
						response +=  line ;
					else
						response += "\n" + line ;
					i++ ;
				}
			}catch (Exception e){
				e.printStackTrace() ;
				return "" ;
			}
			return response ;
		}

	public ArrayList<NameValuePair> prepareParamFeedback(String email, String keyword, String type){
		ArrayList<NameValuePair> temp = new ArrayList<NameValuePair>() ;
		
		BasicNameValuePair p = new BasicNameValuePair("Email", email) ;
		temp.add(p) ;
		p = new BasicNameValuePair("Search", keyword) ;
		temp.add(p) ;
		p = new BasicNameValuePair("SType", type) ;
		temp.add(p) ;
		return temp ;
	}
	//(ResId, Rating, Date) 
	public ArrayList<NameValuePair> prepareParamRating(String ResId, String Rating, String Date){
		ArrayList<NameValuePair> temp = new ArrayList<NameValuePair>() ;
		
		BasicNameValuePair p = new BasicNameValuePair("ResId", ResId) ;
		temp.add(p) ;
		p = new BasicNameValuePair("Rating", Rating) ;
		temp.add(p) ;
		p = new BasicNameValuePair("Date", Date) ;
		temp.add(p) ;
		return temp ;
	}
	
	
	public ArrayList<NameValuePair> prepareParamNewRestaurant(String name, String address, String phone, String lat, String lng){
		ArrayList<NameValuePair> temp = new ArrayList<NameValuePair>() ;
		
		BasicNameValuePair p = new BasicNameValuePair("name", name) ;
		temp.add(p) ;
		p = new BasicNameValuePair("address", address) ;
		temp.add(p) ;
		p = new BasicNameValuePair("phone", phone) ;
		temp.add(p) ;
		p = new BasicNameValuePair("lat", lat) ;
		temp.add(p) ;
		p = new BasicNameValuePair("lng", lng) ;
		temp.add(p) ;
		return temp ;
	}
	
	
}
