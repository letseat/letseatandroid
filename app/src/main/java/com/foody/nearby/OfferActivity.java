package com.foody.nearby;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.animation.Animator;
import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils.TruncateAt;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.foody.AppData.StaticObjects;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.LetsEatRequest;
import com.foody.jsondata.Offer;
import com.foody.jsondata.SearchResturent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.mcc.letseat.R;
import com.mcc.letseat.Adapter.DealsAdapter;
import com.mcc.letseat.tab.TabMainActivity;
import com.mcc.libs.ActivityWithSliding;
import com.mcc.libs.AnimationTween;
import com.mcc.user.SavedPrefernce;

/**
 * Custom ListView For Deals in a selected location e.i:Dhaka>Banani 
 */
public class OfferActivity extends ActivityWithSliding implements
		ActionBar.OnNavigationListener {

	// Google Places
	FoodyResturent foodyResturent;

	// Places List
	List<Offer> offerList;


	// Button
	ImageView btnShowOnMap;

	// Places Listview
	ListView lv;

	// ListItems data
	ArrayList<HashMap<String, String>> resturantListItems = new ArrayList<HashMap<String, String>>();
	// ListItems data sorted
	ArrayList<HashMap<String, String>> resturantListItemsSorted = new ArrayList<HashMap<String, String>>();

	// offer providerlist
	ArrayList<String> offer_provider_list = new ArrayList<String>();
	ArrayList<String> sortBylist = new ArrayList<String>();
	
	String provider_filter = "All";

	// //admob
	AdView adView = null;

	ImageView searchAny;

	String searchKeywords;

	// post value

	int currentPage = 0;
	int totalPage = 1;
	private int totoalItemCountForScroll;
	

	TextView txtTitleOffer, txtOfferLocation;

	Typeface tfLight, tfNormal;

	//LoadPlaces loadPlaces;
	
	View l_sort_filter;

	String LocId;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_offer);

		tfLight = Typeface.createFromAsset(getAssets(),
				StaticObjects.fontPath_SEGOEUI_light);
		tfNormal = Typeface.createFromAsset(getAssets(),
				StaticObjects.fontPath_SEGOEUI);

		// Getting listview
		lv = (ListView) findViewById(R.id.list);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				try {
					
					String resid =((Offer)lv.getItemAtPosition(position)).Id;
					/*DealsAdapter dealsAdapter =(DealsAdapter) lv.getAdapter();					
					
					String resid = dealsAdapter.getItem(position).Id;*/
							
					/*if(provider_filter.equals("All")){
						resid = resturantListItems.get(position).get("Id");
						
					}else{
						resid = resturantListItemsSorted.get(position).get("Id");
					}*/
					
					// restaurentId= resturent.Id;

					Intent in = new Intent(getApplicationContext(),TabMainActivity.class);
					in.putExtra("Id", resid);
					startActivity(in);
					
				} catch (Exception e) {
					// TODO: handle exception
				}

			}
		});

		/*
		 * lv.setOnScrollListener(new OnScrollListener() {
		 * 
		 * @Override public void onScrollStateChanged(AbsListView view, int
		 * scrollState) { // TODO Auto-generated method stub
		 * 
		 * }
		 * 
		 * @Override public void onScroll(AbsListView view, int
		 * firstVisibleItem, int visibleItemCount, int totalItemCount) { // TODO
		 * Auto-generated method stub boolean loadMore = firstVisibleItem +
		 * visibleItemCount >= totalItemCount;
		 * 
		 * if(loadMore && Page>=1) { Toast.makeText(getApplicationContext(),
		 * "load More", Toast.LENGTH_SHORT).show();
		 * 
		 * new LoadPlaces().execute(); Page++; } } });
		 */

		adView = (AdView) this.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder().build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});

		/*txtTitleOffer = (TextView) findViewById(R.id.txtTitleOffer);
		txtTitleOffer.setText("Filter by: ");
		txtTitleOffer.setTypeface(tfLight);*/

		SavedPrefernce savedPrefernce = new SavedPrefernce(OfferActivity.this);

		txtOfferLocation = (TextView) findViewById(R.id.txtOfferLocation);
		txtOfferLocation.setText("Deals Available at "+ savedPrefernce.getLocationName() +" and Nearby Areas");
		txtOfferLocation.setTypeface(tfNormal);
	
		LocId = savedPrefernce.getLocationId();
		
		
	
		lv.setVisibility(View.INVISIBLE);

		offer_provider_list.add(provider_filter);
		// offer_provider_list.add("Others");
		 l_sort_filter = (View) findViewById(R.id.l_sort_filter);
		 l_sort_filter.setVisibility(View.INVISIBLE);
		Spinner spinnerFilter = (Spinner) l_sort_filter.findViewById(R.id.spinner_offer);		
		ArrayAdapter<String> adapter_offer = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, offer_provider_list) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {

				View v = super.getView(position, convertView, parent);
				((TextView) v).setTypeface(tfLight);
				((TextView) v).setEllipsize(TruncateAt.END);

				return v;
			}

			@Override
			public View getDropDownView(int position, View convertView,
					ViewGroup parent) {

				View v = super.getDropDownView(position, convertView, parent);
				((TextView) v).setTypeface(tfLight);
				return v;
			}

		};
		// Specify the layout to use when the list of choices appears
		adapter_offer.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinnerFilter.setAdapter(adapter_offer);
		spinnerFilter.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				

				if (!provider_filter.equals(offer_provider_list.get(pos))) {
					
					lv.setTextFilterEnabled(true);
					provider_filter = offer_provider_list.get(pos);	
					DealsAdapter adapter = (DealsAdapter)lv.getAdapter();
					adapter.getFilter().filter(provider_filter.equals("All")?"":provider_filter);
					
									
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});
		
		//sort 
		sortBylist.add("Name (A-Z)");
		sortBylist.add("Name (Z-A)");
		sortBylist.add("Lowest Cost First");
		sortBylist.add("Highest Cost First");
		sortBylist.add("Highest Capacity First");
		sortBylist.add("Lowest Capacity First");
		sortBylist.add("Deals (A-Z)");
		
		// offer_provider_list.add("Others");

		Spinner spinnerSort = (Spinner) l_sort_filter.findViewById(R.id.spinner_sort);		
		ArrayAdapter<String> adapter_sort = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, sortBylist) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {

				View v = super.getView(position, convertView, parent);
				((TextView) v).setTypeface(tfLight);
				((TextView) v).setEllipsize(TruncateAt.END);

				return v;
			}

			@Override
			public View getDropDownView(int position, View convertView,
					ViewGroup parent) {

				View v = super.getDropDownView(position, convertView, parent);
				((TextView) v).setTypeface(tfLight);
				return v;
			}

		};
		// Specify the layout to use when the list of choices appears
		adapter_sort.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinnerSort.setAdapter(adapter_sort);
		spinnerSort.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,int pos, long id) {			
				DealsAdapter adapter= (DealsAdapter)lv.getAdapter();
				if(offerList==null || adapter ==null){return;}								
				adapter.setSortBY(pos);	
				lv.setSelectionAfterHeaderView();
				/*if(offerList==null)
					return;
				
				Collections.sort(offerList, Collections.reverseOrder());
				DealsAdapter adapter = new DealsAdapter(OfferActivity.this, offerList);
				lv.setAdapter(adapter);
				adapter.notifyDataSetChanged();*/
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

		// set home button
		setHomeButton(this);

		
		
	
		
		loadData();
		
		//analytics

	}
	
	private void loadData(){
		/*loadPlaces = new LoadPlaces();
		loadPlaces.execute();*/
		
		showLoader();
		Volley.newRequestQueue(this).add(postRequest);
	}
	
	private void showLoader(){
		lv.setOnScrollListener(null);
		offerList = null;
		lv.setAdapter(null);

		View v = findViewById(R.id.main_custom_progress);
		v.setVisibility(View.VISIBLE);
		TextView textPreparing = (TextView) v
				.findViewById(R.id.textPreparing);
		textPreparing.setText("Preparing...");
		v.findViewById(R.id.progressBarPreparing).setVisibility(
				View.VISIBLE);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {

		case R.id.action_search:
			// search action
			RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.rest_list_root_layout);
			addSearchLayout(relativeLayout);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}

	@Override
	public void onPause() {
		super.onPause();

		if (adView != null)
			adView.pause();

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if (adView != null)
			adView.destroy();

		//cancel volley request here
		
		/*if (loadPlaces != null && loadPlaces.getStatus() == Status.RUNNING) {
			loadPlaces.cancel(true);
		}*/
		
		//analytics stop tracking
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}
	
	@Override
	protected void onStart() {	
		super.onStart();
		
		//analytics stop tracking
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	/*class LoadPlaces extends AsyncTask<String, String, String> {

		*//**
		 * Before starting background thread Show Progress Dialog
		 * *//*
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			lv.setOnScrollListener(null);
			offerList = null;
			lv.setAdapter(null);

			View v = findViewById(R.id.main_custom_progress);
			v.setVisibility(View.VISIBLE);
			TextView textPreparing = (TextView) v
					.findViewById(R.id.textPreparing);
			textPreparing.setText("Preparing...");
			v.findViewById(R.id.progressBarPreparing).setVisibility(
					View.VISIBLE);
		}

		*//**
		 * getting Places JSON
		 * *//*
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {
				

				
				 * String postData=""; if(provider_filter.equals("All")){
				 * postData= "Page="+currentPage+"&LocId="+LocId+"&DisFrom=";
				 * }else{ postData= "Page="+currentPage+
				 * "&LocId="+LocId+"&DisFrom="+provider_filter; }
				 

				offerList = foodyResturent.getOffer(FoodyResturent.TYPE_LOAD_LOC, LocId, currentPage,  provider_filter, OfferActivity.this);
				
				

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		*//**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **//*
		protected void onPostExecute(String file_url) {
			if (!isCancelled()) {

				

					final Animator.AnimatorListener animatorListener = new Animator.AnimatorListener() {
						@Override
						public void onAnimationStart(Animator animation) {
						}

						@Override
						public void onAnimationRepeat(Animator animation) {
						}

						@Override
						public void onAnimationEnd(Animator animation) {
							lv.setVisibility(View.VISIBLE);
							AnimationTween.animateViewPosition(lv,OfferActivity.this);
						}

						@Override
						public void onAnimationCancel(Animator animation) {
							
						}
					};

					Animator.AnimatorListener animatorListener2 = new Animator.AnimatorListener() {

						@Override
						public void onAnimationStart(Animator animation) {
						}

						@Override
						public void onAnimationRepeat(Animator animation) {
						}

						@Override
						public void onAnimationEnd(Animator animation) {
							// TODO Auto-generated method stub
							l_sort_filter.setVisibility(View.VISIBLE);
							AnimationTween.animateViewPosition( l_sort_filter,animatorListener, OfferActivity.this);
						}

						@Override
						public void onAnimationCancel(Animator animation) {
						}
					};

					AnimationTween.animateView(findViewById(R.id.main_custom_progress),animatorListener2, OfferActivity.this);
					
					if (loadFirstTime) {
					}

				// updating UI from Background Thread
				runOnUiThread(new Runnable() {
					public void run() {
						*//**
						 * Updating parsed Places into LISTVIEW
						 * *//*
						// Get json response status

						if (offerList == null || offerList.size() <= 0) {

							lv.setVisibility(View.INVISIBLE);

							// show message
							findViewById(R.id.main_custom_progress)
									.setVisibility(View.VISIBLE);
							View v = findViewById(R.id.main_custom_progress);
							TextView textPreparing = (TextView) v
									.findViewById(R.id.textPreparing);
							textPreparing
									.setText("No deals available at the moment. Try another location.");
							v.findViewById(R.id.progressBarPreparing)
									.setVisibility(View.GONE);
							AnimationTween.animateViewPosition(v,
									OfferActivity.this);

							if (!checkNetConnection()) {

								textPreparing
										.setText("No internet connection. Retry ");
								textPreparing.setGravity(Gravity.CENTER);
								textPreparing
										.setCompoundDrawablesWithIntrinsicBounds(
												0, 0, R.drawable.ic_refresh, 0);
								findViewById(R.id.main_custom_progress)
										.setOnClickListener(
												new OnClickListener() {

													@Override
													public void onClick(View v) {
														// TODO Auto-generated
														// method stub
														loadPlaces = new LoadPlaces();
														loadPlaces.execute();

													}
												});
							}

						} else if (offerList != null) {
							// Check for all possible status

							// Successfully got places details
							if (offerList != null) {

								// sort data by offer
								// offerList=sortData(offerList);
								
								//search suggestion
								ArrayList<SearchResturent> searchRestList= new ArrayList<SearchResturent>(); 

								// loop through each offer
								for (Offer p : offerList) {

									HashMap<String, String> map = new HashMap<String, String>();
									map.put("Id", p.Id);

									map.put("RestName", p.RestName);
									map.put("DisFrom", p.DisFrom);
									map.put("cusine", p.cusine);
									map.put("DisAmount", p.DisAmount+p.Capacity+p.Cost4Two);
									map.put("IMG", p.IMG);

									totalPage = Integer.parseInt(p.PageNo);

									System.out
											.println(">>>>>>>>>>test>>>>>>>>>>>>>>>>>> "
													+ p.RestLoc);
									Log.e("oprovider_filter: ", provider_filter);

									resturantListItems.add(map);
									
									//search suggestion
									searchRestList.add(new SearchResturent(p.Id, p.RestName, null, p.RestLoc, null, null, null));

									// adding offer provider to provider list
									addToProviderList(offer_provider_list,p.DisFrom);
									

								}

								loadFirstTime = false;
								
								//search suggestion
								addDataToDB(searchRestList);
								
								MyListAdapterImage adapter = new MyListAdapterImage(
										OfferActivity.this, resturantListItems,
										new String[] { "RestName", "DisFrom",
												"cusine", "DisAmount", "IMG" });
								DealsAdapter  adapter= new DealsAdapter(OfferActivity.this, offerList);

								

								Log.e("total deals: ","============================"+ resturantListItems.size());
								// Adding data into listview
								lv.setAdapter(adapter);
								adapter.notifyDataSetChanged();
								currentPage++;
								
								if(!loadFirstTime){
									//lv.scrollBy(0, getScroll());
									//lv.smoothScrollToPosition(totoalItemCountForScroll);
									lv.setSelection(totoalItemCountForScroll-1);
								}

							}

							else if (offerList == null) {
								lv.setVisibility(View.INVISIBLE);

								// show message
								findViewById(R.id.main_custom_progress)
										.setVisibility(View.VISIBLE);
								View v = findViewById(R.id.main_custom_progress);
								TextView textPreparing = (TextView) v
										.findViewById(R.id.textPreparing);
								textPreparing
										.setText("No deals available at the moment. Try another location.");
								v.findViewById(R.id.progressBarPreparing)
										.setVisibility(View.GONE);
								AnimationTween.animateViewPosition(v,
										OfferActivity.this);
							}
						}
					}

				});

			}
		}

	}*/
	
	
	StringRequest postRequest = new StringRequest(Request.Method.POST, LetsEatRequest.OFFER_LIST_URL,
	        new Response.Listener<String>() {
	            @Override
	            public void onResponse(String response) {
	                try {
	                	// Result handling 
	                	offerList = new LetsEatRequest().getOffer(response);


	    				

						final Animator.AnimatorListener animatorListener = new Animator.AnimatorListener() {
							@Override
							public void onAnimationStart(Animator animation) {
							}

							@Override
							public void onAnimationRepeat(Animator animation) {
							}

							@Override
							public void onAnimationEnd(Animator animation) {
								lv.setVisibility(View.VISIBLE);
								AnimationTween.animateViewPosition(lv,OfferActivity.this);
							}

							@Override
							public void onAnimationCancel(Animator animation) {
								
							}
						};

						Animator.AnimatorListener animatorListener2 = new Animator.AnimatorListener() {

							@Override
							public void onAnimationStart(Animator animation) {
							}

							@Override
							public void onAnimationRepeat(Animator animation) {
							}

							@Override
							public void onAnimationEnd(Animator animation) {
								// TODO Auto-generated method stub
								l_sort_filter.setVisibility(View.VISIBLE);
								AnimationTween.animateViewPosition( l_sort_filter,animatorListener, OfferActivity.this);
							}

							@Override
							public void onAnimationCancel(Animator animation) {
							}
						};

						AnimationTween.animateView(findViewById(R.id.main_custom_progress),animatorListener2, OfferActivity.this);
						
						if (loadFirstTime) {
						}

					// updating UI from Background Thread
					runOnUiThread(new Runnable() {
						public void run() {
							/**
							 * Updating parsed Places into LISTVIEW
							 * */
							// Get json response status

							if (offerList == null || offerList.size() <= 0) {

								lv.setVisibility(View.INVISIBLE);

								// show message
								findViewById(R.id.main_custom_progress)
										.setVisibility(View.VISIBLE);
								View v = findViewById(R.id.main_custom_progress);
								TextView textPreparing = (TextView) v
										.findViewById(R.id.textPreparing);
								textPreparing
										.setText("No deals available at the moment. Try another location.");
								v.findViewById(R.id.progressBarPreparing)
										.setVisibility(View.GONE);
								AnimationTween.animateViewPosition(v,
										OfferActivity.this);

								if (!checkNetConnection()) {

									textPreparing
											.setText("No internet connection. Retry ");
									textPreparing.setGravity(Gravity.CENTER);
									textPreparing
											.setCompoundDrawablesWithIntrinsicBounds(
													0, 0, R.mipmap.ic_refresh, 0);
									findViewById(R.id.main_custom_progress)
											.setOnClickListener(
													new OnClickListener() {

														@Override
														public void onClick(View v) {
															// TODO Auto-generated
															// method stub
															/*loadPlaces = new LoadPlaces();
															loadPlaces.execute();*/
															Volley.newRequestQueue(OfferActivity.this).add(postRequest);

														}
													});
								}

							} else if (offerList != null) {
								// Check for all possible status

								// Successfully got places details
								if (offerList != null) {

									// sort data by offer
									// offerList=sortData(offerList);
									
									//search suggestion
									ArrayList<SearchResturent> searchRestList= new ArrayList<SearchResturent>(); 

									// loop through each offer
									for (Offer p : offerList) {

										HashMap<String, String> map = new HashMap<String, String>();
										map.put("Id", p.Id);

										map.put("RestName", p.RestName);
										map.put("DisFrom", p.DisFrom);
										map.put("cusine", p.cusine);
										map.put("DisAmount", p.DisAmount+p.Capacity+p.Cost4Two);
										map.put("IMG", p.IMG);

										totalPage = Integer.parseInt(p.PageNo);

										System.out
												.println(">>>>>>>>>>test>>>>>>>>>>>>>>>>>> "
														+ p.RestLoc);
										Log.e("oprovider_filter: ", provider_filter);

										resturantListItems.add(map);
										
										//search suggestion
										searchRestList.add(new SearchResturent(p.Id, p.RestName, null, p.RestLoc, null, null, null));

										// adding offer provider to provider list
										addToProviderList(offer_provider_list,p.DisFrom);
										

									}

									loadFirstTime = false;
									
									//search suggestion
									addDataToDB(searchRestList);
									
									/*MyListAdapterImage adapter = new MyListAdapterImage(
											OfferActivity.this, resturantListItems,
											new String[] { "RestName", "DisFrom",
													"cusine", "DisAmount", "IMG" });*/
									DealsAdapter  adapter= new DealsAdapter(OfferActivity.this, offerList);

									

									Log.e("total deals: ","============================"+ resturantListItems.size());
									// Adding data into listview
									lv.setAdapter(adapter);
									adapter.notifyDataSetChanged();
									currentPage++;
									
									if(!loadFirstTime){
										//lv.scrollBy(0, getScroll());
										//lv.smoothScrollToPosition(totoalItemCountForScroll);
										lv.setSelection(totoalItemCountForScroll-1);
									}

								}

								else if (offerList == null) {
									lv.setVisibility(View.INVISIBLE);

									// show message
									findViewById(R.id.main_custom_progress)
											.setVisibility(View.VISIBLE);
									View v = findViewById(R.id.main_custom_progress);
									TextView textPreparing = (TextView) v
											.findViewById(R.id.textPreparing);
									textPreparing
											.setText("No deals available at the moment. Try another location.");
									v.findViewById(R.id.progressBarPreparing)
											.setVisibility(View.GONE);
									AnimationTween.animateViewPosition(v,
											OfferActivity.this);
								}
							}
						}

					});

				
	                } catch (Exception e) {
	                	
	                	
	                    e.printStackTrace();
	                }
	            }
	        },
	        new Response.ErrorListener() {
	            @Override
	            public void onErrorResponse(VolleyError error) {
	                error.printStackTrace();
	            }
	        }
	) {
	    @Override
	    protected Map<String, String> getParams()
	    {
	        Map<String, String>  params = new HashMap<String, String>();
	        // the POST parameters:= "Page=" + all + "&"+loadType(type)+"=" + LocId + "&DisFrom=";
	        // , currentPage,  provider_filter,
	        params.put("LocId", LocId);
	        params.put("Page", ""+currentPage);
	        params.put("DisFrom", provider_filter.equals("All")?"":provider_filter);
	        return params;
	    }
	};
	
	
	
	



	@Override
	public boolean onNavigationItemSelected(int arg0, long arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	

	void addToProviderList(ArrayList<String> list, String newProvider) {
		if (!list.contains(newProvider)) {
			list.add(newProvider);
		}

	}

	void filterDataby(String filter) {
		
		if(filter.equals("All")){
			MyListAdapterImage adapterFiltered = new MyListAdapterImage(
					OfferActivity.this, resturantListItems, new String[] {
							"RestName", "DisFrom", "cusine", "DisAmount", "IMG" });
			lv.setAdapter(adapterFiltered);
			return;
			
		}

		 resturantListItemsSorted = new ArrayList<HashMap<String, String>>();

		for (int i = 0; i < resturantListItems.size(); i++) {

			HashMap<String, String> map = resturantListItems.get(i);
			String DisFrom = map.get("DisFrom");
			if (DisFrom.equals(provider_filter)) {
				resturantListItemsSorted.add(map);
			}
		}

		MyListAdapterImage adapterFiltered = new MyListAdapterImage(
				OfferActivity.this, resturantListItemsSorted, new String[] {
						"RestName", "DisFrom", "cusine", "DisAmount", "IMG" });
		lv.setAdapter(adapterFiltered);

	}

	List<Offer> sortData(List<Offer> arrayList, final int pos) {
		List<Offer> offers = arrayList;

		// Sorting
		Collections.sort(offers, new Comparator<Offer>() {

			@Override
			public int compare(Offer offer1, Offer offer2) {
				// TODO Auto-generated method stub
				if(pos == 1)
					return offer1.Cost4Two.compareTo(offer2.Cost4Two);				
				else if(pos == 2)
					return offer1.Capacity.compareTo(offer2.Capacity);
				else if(pos == 3)
					return offer1.RestLoc.compareTo(offer2.RestLoc);
				else if(pos == 4)
					return offer1.DisFrom.compareTo(offer2.DisFrom);
				else 
					return offer1.RestName.compareTo(offer2.RestName);
			}
		});

		return offers;
	}
	
	

}
