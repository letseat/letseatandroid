package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.mcc.letseat.R;
import com.squareup.picasso.Picasso;

public class MyPhotoListAdapter extends BaseAdapter{
	
	ArrayList<HashMap<String, String>> aa;
	Context c;
	String listTags[];
	
	public MyPhotoListAdapter(Context c, ArrayList<HashMap<String, String>> aa){
		this.aa = aa;
		this.c = c;
	}
	
	public MyPhotoListAdapter(Context c, ArrayList<HashMap<String, String>> aa, String listTags[]){
		this.aa = aa;
		this.c = c;
		this.listTags=listTags;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return aa.size();
	}

	@Override
	public String getItem(int arg0) {
		// TODO Auto-generated method stub
		return aa.get(arg0).get("reference");
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int pos, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		
		View v;
		v = arg1;
		LayoutInflater li = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		v = li.inflate(R.layout.photo, null);
		
		
		
		
		ImageView imageView = (ImageView)v.findViewById(R.id.PhotoList_ImageView);
		//ProgressBar progressBarOfferImage=(ProgressBar)v.findViewById(R.id.PhotoList_progressBar);
		
		
		
		
		//String ww =aa.get(pos);
		if(listTags!=null){
			
			//loading image
			if(aa.get(pos).get(listTags[0])!=null && !aa.get(pos).get(listTags[0]).isEmpty()){

				Picasso.with(c)
			    .load(aa.get(pos).get(listTags[0]))
			    .placeholder(R.mipmap.dummy_avatar)
			    .error(R.mipmap.dummy_avatar)
			    .into(imageView);
			}
			
			
			
		}else{
			//System.out.print("bingo!!!");
		}
		
		
		
		
		
		return v;
	}

}
