package com.foody.nearby;

import java.util.ArrayList;

import org.apache.http.NameValuePair;

import android.app.Activity;
import android.content.Context;

/**
 * Rate a restaurant
 */
public class PostRatingTask extends BaseTask{
	Context c ;
	String ResId = "" ;
	String Rating = "" ;
	String Date = "" ;
	boolean isFeed = false ;
	
	
	public PostRatingTask(Context ctx, String ResId, String Rating, String Date) {
		super(ctx);
		this.c = ctx ;
		this.ResId = ResId ;
		this.Rating = Rating ;		
		this.Date = Date ;
	}

	@Override
	void task() {
		
		
			WebApi w = new WebApi() ;
		
			ArrayList<NameValuePair> params = w.prepareParamRating(ResId, Rating, Date);
			String res = w.postFeedBack("http://foody.asholei.com/mobsvc/AddRating.php", params) ;
			
			
			Activity activity = (Activity) c;
			//activity.finish();
		
		
	}

}
