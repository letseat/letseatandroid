package com.foody.nearby;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import org.w3c.dom.Document;

import android.animation.Animator;
import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mcc.letseat.MyProgressDialog;
import com.mcc.letseat.R;
import com.mcc.libs.ActivityWithSliding;
import com.mcc.libs.AnimationTween;

/**
 * Custom List for nearby restaurant for Google Places API
 */
public class NearByplaceListView extends ActivityWithSliding implements
ActionBar.OnNavigationListener  {

	// flag for Internet connection status
	Boolean isInternetPresent = false;

	
	
	

	// Google Places
	GooglePlaces googlePlaces;

	// Places List
	PlacesList nearPlaces;

	// GPS Location
	GPSTracker gps;

	// Button
	ImageView btnShowOnMap;

	// Progress dialog
	MyProgressDialog pDialog;
	
	// Places Listview
	ListView lv;
	MyListAdapter adapter ;
	
	// ListItems data
	ArrayList<HashMap<String, String>> placesListItems = new ArrayList<HashMap<String,String>>();
	

	LatLng myLocation=null;
	
	////admob
	AdView adView=null;

	ImageView searchAny;
	

	// Radius in meters - increase this value if you don't find any places
	double radius = 1000; // 1000 meters 
	int spinner_current_index=0;





	private View sort_filter_nearby;
	
	// KEY Strings
	public static String KEY_REFERENCE = "reference"; // id of the place
	public static String KEY_NAME = "name"; // name of the place
	public static String KEY_VICINITY = "vicinity"; // Place area name
	public static String KEY_DISTANCE = "distance"; // Place area name

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_nearby_place);
		
	

		// creating GPS Class object
		gps = new GPSTracker(this);

		// check if GPS location can get
		if (gps.canGetLocation()) {
			Log.d("Your Location", "latitude:" + gps.getLatitude() + ", longitude: " + gps.getLongitude());
			myLocation=new LatLng( gps.getLatitude(), gps.getLongitude());
		} else {
			
			gps.showSettingsAlert();
			// stop executing code by return
			return;
		}

		// Getting listview
		lv = (ListView) findViewById(R.id.list);
		
		sort_filter_nearby= findViewById(R.id.sort_filter_nearby);		
		 TextView txtTitleNearBy=(TextView)sort_filter_nearby.findViewById(R.id.txtTitleNearBy);
		txtTitleNearBy.setText("Restaurants Nearby ");
		
		//spinner
		Spinner spinner = (Spinner) sort_filter_nearby.findViewById(R.id.spinner_offer);
		// Create an ArrayAdapter using the string array and a default spinner layout
		ArrayAdapter<CharSequence> adapter_km = ArrayAdapter.createFromResource(this,
		        R.array.km, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter_km.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinner.setAdapter(adapter_km);
		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				radius=(pos+1) * 1000;
				//new LoadPlaces().execute();
				Log.d("kilometer", radius+"");
				//Toast.makeText(getApplicationContext(), "kilometer"+radius,Toast.LENGTH_SHORT).show();
				if(spinner_current_index!=pos){
					placesListItems = new ArrayList<HashMap<String,String>>();
					spinner_current_index=pos;
					new LoadPlaces().execute();
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		} );
		
		
		
		
		

		
	
		
		
		
		
		lv.setOnItemClickListener(new OnItemClickListener() {
 
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                    int position, long id) {
            	
            	if(!new ConnectionDetector(getApplicationContext()).isConnectingToInternet()){
            		Toast.makeText(NearByplaceListView.this, "Please Connect to Intenet", Toast.LENGTH_SHORT).show();
            		return;
            	}
            	
                // Starting new intent
                Intent in = new Intent(getApplicationContext(),
                		SinglePlaceActivity.class);
                
                // Sending place refrence id to single place activity
                // place refrence id used to get "Place full details"
                
                in.putExtra(KEY_REFERENCE, adapter.getItem(position));
                in.putExtra("lat",myLocation.latitude );
                in.putExtra("lng",myLocation.longitude );  
                startActivity(in);
            }
        });
		
		
		adView = (AdView)this.findViewById(R.id.adView);
		
		AdRequest adRequest = new AdRequest.Builder()	
		.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
	    .build();
	    
	  adView.loadAd(adRequest);
	  
	  adView.setAdListener(new AdListener() {
		  public void onAdLoaded() {}
		  public void onAdFailedToLoad(int errorcode) {
			  System.out.println("Error:"+errorcode);
		  }
		  
		 
		});
	  
	 
	  
	  sort_filter_nearby.setVisibility(View.INVISIBLE);
	  lv.setVisibility(View.INVISIBLE);
	  
	//set home button
	setHomeButton(this);
		
		new LoadPlaces().execute();
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
	
		case R.id.action_search:
			// search action
			RelativeLayout relativeLayout=(RelativeLayout)findViewById(R.id.nearby_place_list_root_layout);		
			addSearchLayout(relativeLayout);			
			return true;
		
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	
	
	

@Override
public void onPause() {
	super.onPause();
	
	if(adView!=null)
  adView.pause();
	
  
}

@Override
public void onResume() {
  super.onResume();
  
  if(adView!=null)
  adView.resume();
}

@Override
public void onDestroy() {
	 if(adView!=null)
  adView.destroy();
  super.onDestroy();
}

	/**
	 * Background Async Task to Load Google places
	 * */
	class LoadPlaces extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			findViewById(R.id.nearby_custom_progress).setVisibility(View.VISIBLE);
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			googlePlaces = new GooglePlaces();
			
			try {
				// Separeate your place types by PIPE symbol "|"
				// If you want all types places make it as null
				// Check list of types supported by google
				// 
				
				// Listing places only cafes, restaurants
				String types = "cafe|restaurant"; 
				
				
				// get nearest places
				nearPlaces = googlePlaces.search(gps.getLatitude(),
						gps.getLongitude(), radius, types);
				

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * and show the data in UI
		 * Always use runOnUiThread(new Runnable()) to update UI from background
		 * thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			final Animator.AnimatorListener animatorListener= new Animator.AnimatorListener() {
				
				@Override
				public void onAnimationStart(Animator animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationRepeat(Animator animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animator animation) {
					// TODO Auto-generated method stub
					lv.setVisibility(View.VISIBLE);
					AnimationTween.animateViewPosition(lv, NearByplaceListView.this);
				}
				
				@Override
				public void onAnimationCancel(Animator animation) {
					// TODO Auto-generated method stub
					
				}
			};
			
			Animator.AnimatorListener animatorListener2= new Animator.AnimatorListener() {
				
				@Override
				public void onAnimationStart(Animator animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationRepeat(Animator animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animator animation) {
					// TODO Auto-generated method stub
					findViewById(R.id.ltnbTitle).setVisibility(View.VISIBLE);
					AnimationTween.animateViewPosition(findViewById(R.id.ltnbTitle), animatorListener, NearByplaceListView.this);
				}
				
				@Override
				public void onAnimationCancel(Animator animation) {
					// TODO Auto-generated method stub
					
				}
			};
			
			
			AnimationTween.animateView(findViewById(R.id.nearby_custom_progress), animatorListener2, NearByplaceListView.this);
			
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status
					String status=null;
					if(nearPlaces !=null){
						status = nearPlaces.status != null? nearPlaces.status:null ;
					}
					
					if(nearPlaces ==null || nearPlaces.status==null){

						
						//show message
						findViewById(R.id.nearby_custom_progress).setVisibility(View.VISIBLE);						
						View v=findViewById(R.id.nearby_custom_progress);
						TextView textPreparing=(TextView) v.findViewById(R.id.textPreparing);
						textPreparing.setText("Nothing found.");
						v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
						AnimationTween.animateViewPosition(v, NearByplaceListView.this);
					}
					else if(status!=null){
							// Check for all possible status
							if(status.equals("OK")){
								// Successfully got places details
								if (nearPlaces.results != null) {
									// loop through each place
									for (Place p : nearPlaces.results) {
										HashMap<String, String> map = new HashMap<String, String>();
										
										// Place reference won't display in listview - it will be hidden
										// Place reference is used to get "place full details"
										map.put(KEY_REFERENCE, p.reference);
										
										//distance
										double distance=getDistanceKM(new LatLng(gps.latitude, gps.longitude),new LatLng(p.geometry.location.lat, p.geometry.location.lng));
										String distancetxt=distance+" km(approx), "+ p.vicinity;
										
										
										// Place name
										map.put(KEY_NAME, p.name);
										map.put(KEY_DISTANCE, distancetxt);
										
										
										
										// adding HashMap to ArrayList
										placesListItems.add(map);
									}
									// list adapter
//									ListAdapter adapter = new SimpleAdapter(NearByplaceListView.this, placesListItems,
//							                R.layout.n_list_item,
//							                new String[] { KEY_REFERENCE, KEY_NAME,KEY_DISTANCE}, new int[] {
//							                        R.id.reference, R.id.name,R.id.distance });
									
									for (int j=0; j<placesListItems.size();j++) {
										for (int i=0; i<placesListItems.size()-1;i++) {
											 HashMap<String, String> _1st= (HashMap<String, String>) placesListItems.get(i);
											 HashMap<String, String> _2nd = (HashMap<String, String>) placesListItems.get(i+1);
											
											 String _1stDistancTxt=_1st.get(KEY_DISTANCE);
											 String _2ndDistancTxt=_2nd.get(KEY_DISTANCE);
											 
											 
											 _1stDistancTxt=_1stDistancTxt.substring(0, _1stDistancTxt.indexOf("k"));
											 _2ndDistancTxt=_2ndDistancTxt.substring(0, _2ndDistancTxt.indexOf("k"));
											 
											 Log.i("distance", Double.parseDouble(_1stDistancTxt)+"<>"+Double.parseDouble(_2ndDistancTxt));
											 
											 if(Double.parseDouble(_1stDistancTxt)>Double.parseDouble(_2ndDistancTxt)){
												 placesListItems.set(i, _2nd);
												 placesListItems.set(i+1, _1st);
											 }
											 
										}
									}
									
									

							       /* Collections.sort( placesListItems, new Comparator<HashMap<String,String>>(){
							             public int compare( HashMap<String,String> one, HashMap<String,String> two ) {
							                 return one.keySet().iterator().next().compareTo( two.keySet().iterator().next() );
							             }
							         });*/
									
									
									adapter = new MyListAdapter(NearByplaceListView.this, placesListItems);
									
									// Adding data into listview
									lv.setAdapter(adapter);
									
									
									
								}
							}
							else if(status.equals("ZERO_RESULTS")){
								
								
								//show message
								findViewById(R.id.nearby_custom_progress).setVisibility(View.VISIBLE);						
								View v=findViewById(R.id.nearby_custom_progress);
								TextView textPreparing=(TextView) v.findViewById(R.id.textPreparing);
								textPreparing.setText("Sorry no restaurant found. Try to change distance");
								v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
								AnimationTween.animateViewPosition(v, NearByplaceListView.this);
							}
							else if(status.equals("UNKNOWN_ERROR"))
							{
								//alert.showAlertDialog(NearByplaceListView.this, "Places Error","Sorry unknown error occured.",false);
								

								//show message
								findViewById(R.id.nearby_custom_progress).setVisibility(View.VISIBLE);						
								View v=findViewById(R.id.nearby_custom_progress);
								TextView textPreparing=(TextView) v.findViewById(R.id.textPreparing);
								textPreparing.setText("Sorry unknown error occured.");
								v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
								AnimationTween.animateViewPosition(v, NearByplaceListView.this);
							}
							else if(status.equals("OVER_QUERY_LIMIT"))
							{
								//alert.showAlertDialog(NearByplaceListView.this, "Places Error","Sorry query limit to google places is reached",false);
								
								//show message
								findViewById(R.id.nearby_custom_progress).setVisibility(View.VISIBLE);						
								View v=findViewById(R.id.nearby_custom_progress);
								TextView textPreparing=(TextView) v.findViewById(R.id.textPreparing);
								textPreparing.setText("Query limit to google places is reached.");
								v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
								AnimationTween.animateViewPosition(v, NearByplaceListView.this);
							}
							else if(status.equals("REQUEST_DENIED"))
							{
								//alert.showAlertDialog(NearByplaceListView.this, "Places Error","Sorry error occured. Request is denied",false);

								//show message
								findViewById(R.id.nearby_custom_progress).setVisibility(View.VISIBLE);						
								View v=findViewById(R.id.nearby_custom_progress);
								TextView textPreparing=(TextView) v.findViewById(R.id.textPreparing);
								textPreparing.setText(" Request is denied.");
								v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
								AnimationTween.animateViewPosition(v, NearByplaceListView.this);
							}
							else if(status.equals("INVALID_REQUEST"))
							{
								//alert.showAlertDialog(NearByplaceListView.this, "Places Error","Sorry error occured. Invalid Request",false);

								//show message
								findViewById(R.id.nearby_custom_progress).setVisibility(View.VISIBLE);						
								View v=findViewById(R.id.nearby_custom_progress);
								TextView textPreparing=(TextView) v.findViewById(R.id.textPreparing);
								textPreparing.setText("Invalid Request.");
								v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
								AnimationTween.animateViewPosition(v, NearByplaceListView.this);
							}
							else
							{
								//alert.showAlertDialog(NearByplaceListView.this,"Places Error", "Sorry error occured.",false);

								//show message
								findViewById(R.id.nearby_custom_progress).setVisibility(View.VISIBLE);						
								View v=findViewById(R.id.nearby_custom_progress);
								TextView textPreparing=(TextView) v.findViewById(R.id.textPreparing);
								textPreparing.setText("Nothing found.");
								v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
								AnimationTween.animateViewPosition(v, NearByplaceListView.this);
							}
						}
				}
				
				
			});

		}

	}
	
	public double getDistance(LatLng fromPosition, LatLng toPosition){
		GMapV2Direction md = new GMapV2Direction();

		Document doc = md.getDocument(fromPosition, toPosition, GMapV2Direction.MODE_DRIVING);

		ArrayList<LatLng> directionPoint = md.getDirection(doc);

		PolylineOptions rectLine = new PolylineOptions().width(3).color(Color.RED);


		for(int i = 0 ; i < directionPoint.size() ; i++) {          

		rectLine.add(directionPoint.get(i));

		}

		return  Double.valueOf(md.getDistanceValue(doc))/1000;

/*		dis.setText("Distance: "+a +" km (approx)");

		mMap.addPolyline(rectLine);*/
	}
	
	public double getDistanceKM(LatLng fromPosition, LatLng toPosition){
		Location locationA = new Location("point A");

		locationA.setLatitude(fromPosition.latitude);

		locationA.setLongitude(fromPosition.longitude);

		Location locationB = new Location("point B");

		locationB.setLatitude(toPosition.latitude);

		locationB.setLongitude(toPosition.longitude);
		
		double value = (locationA.distanceTo(locationB)/1000);
		//double rounded = (double) Math.round(value,2 ) ;
		Log.i("kilometer before format", value+"");
		
		DecimalFormat df = new DecimalFormat("###0.00");
		double rounded =Double.parseDouble(df.format(value));
		Log.i("kilometer after format", rounded+"");

		return rounded;
		
	}


	@Override
	public boolean onNavigationItemSelected(int arg0, long arg1) {
		// TODO Auto-generated method stub
		return false;
	}



	

	

}
