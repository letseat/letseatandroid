package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;
import com.mcc.letseat.R;
import com.mcc.libs.TouchEffectListener;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * 
 * @author Arif
 * Data List adapter use for single or double text view in list item
 */
public class MyListAdapter extends BaseAdapter{
	
	ArrayList<HashMap<String, String>> aa;
	Context c;
	String listTags[];
	
	/*TouchEffectListener tel1=new TouchEffectListener("#FCFCFC", "#ced2d8");
	TouchEffectListener tel2=new TouchEffectListener("#FFFFFF", "#ced2d8");*/
	
	public MyListAdapter(Context c, ArrayList<HashMap<String, String>> aa){
		this.aa = aa;
		this.c = c;
	}
	
	public MyListAdapter(Context c, ArrayList<HashMap<String, String>> aa, String listTags[]){
		this.aa = aa;
		this.c = c;
		this.listTags=listTags;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return aa.size();
	}

	@Override
	public String getItem(int arg0) {
		// TODO Auto-generated method stub
		return aa.get(arg0).get("reference");
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	private int lastPosition = -1;
	@Override
	public View getView(int pos, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		
		View v;
		v = arg1;
		LayoutInflater li = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		v = li.inflate(R.layout.n_list_item, null);
		
		LinearLayout root = (LinearLayout)v.findViewById(R.id.ltnbTitle);
		
		TextView name= (TextView)v.findViewById(R.id.first);
		TextView dis = (TextView)v.findViewById(R.id.distance);
		TextView ref = (TextView)v.findViewById(R.id.reference);
		
		
		if(pos%2==0)
		{
			v.setBackgroundColor(Color.parseColor("#FCFCFC"));
			//v.setOnTouchListener(tel1);
		}
		else
		{
			v.setBackgroundColor(Color.parseColor("#FFFFFF"));
			//v.setOnTouchListener(tel2);
		}
		
		//String ww =aa.get(pos);
		if(listTags!=null){
			name.setText(aa.get(pos).get(listTags[0]));
			dis.setText(aa.get(pos).get(listTags[1]));
			//ref.setText(aa.get(pos).get(listTags[2]));
		}else{
			name.setText(aa.get(pos).get("name"));
			dis.setText(aa.get(pos).get("distance"));
			//ref.setText(aa.get(pos).get("reference"));
		}
		
		/*name.setText(aa.get(pos).get("name"));
		dis.setText(aa.get(pos).get("distance"));
		ref.setText(aa.get(pos).get("reference"));
*/		
		
		
		//apply animation on current view
		 Animation animation = AnimationUtils.loadAnimation(c, (pos > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
		 v.startAnimation(animation);
		 lastPosition = pos;
		
		return v;
	}

}
