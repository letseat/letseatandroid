package com.foody.nearby;

import java.util.ArrayList;

import org.apache.http.NameValuePair;

import android.app.Activity;
import android.content.Context;

import com.foody.jsondata.FoodyResturent;

/**
 * 
 * Used to add or suggest a restaurant and feedback and correction of restaurant
 *
 */
public class PostFeedBackTask extends BaseTask{
	public static final int TYPE_RESTAURANT=0;
	public static final int TYPE_FOOD_CATEGORY=1;
	public static final int TYPE_CUISINE=2;
	public static final int TYPE_GENERAL=3;

	Context c ;
	String email = "" ;
	String keyword = "" ;
	String type = "" ;
	boolean isFeed = false ;
	
	
	public PostFeedBackTask(Context ctx, String email, String keyword, String type) {
		super(ctx);
		this.c = ctx ;
		this.email = email ;
		this.keyword = keyword ;		
		this.type = type ;
	}

	@Override
	void task() {
		
		
		WebApi w = new WebApi() ;
		
			ArrayList<NameValuePair> params = w.prepareParamFeedback(email, keyword, type) ;
			String res = w.postFeedBack(FoodyResturent.REQUEST_SUGGETION_URL, params) ;
			//Log.e("FEED", res) ;
			//finish caller activity
			
		
		
		Activity activity = (Activity) c;
		activity.finish();
		
		
	}

}
