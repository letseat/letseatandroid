package com.foody.nearby;

import java.util.ArrayList;

import org.apache.http.NameValuePair;

import android.app.Activity;
import android.content.Context;

import com.foody.jsondata.FoodyResturent;

public class PostNewRestaurant extends BaseTask {
	Context c;

	String name, address, phone, lat, lng;

	boolean isFeed = false;

	public PostNewRestaurant(Context ctx, String name, String address,
			String phone, String lat, String lng) {
		super(ctx);
		this.c = ctx;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.lat = lat;
		this.lng = lng;
	}

	@Override
	void task() {

		WebApi w = new WebApi();

		ArrayList<NameValuePair> params = w.prepareParamNewRestaurant(name,
				address, phone, lat, lng);
		String res = w.postFeedBack(FoodyResturent.REQUEST_NEW_RESTAURANT_URL, params);
		// Log.e("FEED", res) ;
		// finish caller activity

		Activity activity = (Activity) c;
		activity.finish();

	}

}
