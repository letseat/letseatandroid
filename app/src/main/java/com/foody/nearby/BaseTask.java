package com.foody.nearby;

/*
 * Simple async task template
 * Points of improvement: 
 * 		Orientation Check
 * 		retain non configuretion
 * 		some sort of joining mechaninsm
 * 
 * Author: Sayeed Mahmud
 */
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


abstract class BaseTask extends AsyncTask<Void, Void, Void>{
	Context ctx ;
	ProgressDialog pd ;
	
	
	public BaseTask(Context ctx){
		this.ctx = ctx ;
	}
	
	/* the first param of generic is the parameter */
	/* the third param of generic is the return val */
	protected Void doInBackground(Void ... max){
		task() ;
		return null ;
		
		
	}
	
	protected void onPreExecute(){
		this.pd = new ProgressDialog(ctx) ;
		this.pd.setTitle("Please Wait ..." ) ;
		this.pd.setMessage("Please Wait .. ") ;
		this.pd.setProgressStyle(ProgressDialog.STYLE_SPINNER) ;
		this.pd.setIndeterminate(true) ;
		this.pd.show() ;	
	}
	/* this Void is the return type of doInBackground */ 
	protected void onPostExecute(Void result){
		Log.e("ASYNC", "POST"); 
		this.pd.hide() ;
		this.pd.dismiss() ;
		
		
	}
	
	abstract void task() ;
		
	public void setMsg(String msg){
		this.pd.setMessage(msg) ;
	}

	
}
