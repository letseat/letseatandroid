package com.foody.nearby;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.foody.jsondata.DirectionsJSONParser;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mcc.letseat.MyProgressDialog;
import com.mcc.letseat.R;
import com.mcc.libs.FragmentActivityWithSliding;


/**
 * use for Google Place API restaurant
 */
public class SinglePlaceActivity extends FragmentActivityWithSliding {
	// flag for Internet connection status
	Boolean isInternetPresent = false;

	// Connection detector class
	ConnectionDetector cd;
	
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Google Places
	GooglePlaces googlePlaces;
	
	// Place Details
	PlaceDetails placeDetails;
	
	// Progress dialog
	MyProgressDialog pDialog;

	 private GoogleMap mMap;
	 private Marker mPerth; 
	
	// KEY Strings
	public static String KEY_REFERENCE = "reference"; // id of the place
	
	
	// LatLng MyLocation = null;
	
	AdView adView=null;
	
	ImageView searchAny;
	
	LatLng myLatLng;
	
	TextView lbl_name , lbl_address;
	ImageView lbl_phone ;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_single_place_);
		
		 lbl_name = (TextView) findViewById(R.id.first);
		 lbl_address = (TextView) findViewById(R.id.address);
		 lbl_phone = (ImageView) findViewById(R.id.phone);
		 
		 findViewById(R.id.searchInLetsEat).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(SinglePlaceActivity.this, SearchResturentListView.class);
				String resname=lbl_name.getText().toString();				
				int spaceIndex=lbl_name.getText().toString().indexOf(" ");
				
				if(spaceIndex==-1){
					i.putExtra("searchKeyWord_home", resname);
				}else{
					i.putExtra("searchKeyWord_home", resname.substring(0, spaceIndex));
				}
				
				
				startActivity(i);
			}
		});
		
		//set home button
		setHomeButtonLeft(this);
		
		Intent i = getIntent();
		
		// Place referece id
		String reference = i.getStringExtra(KEY_REFERENCE);
		
		myLatLng=new LatLng(i.getDoubleExtra("lat", 0), i.getDoubleExtra("lng", 0));
		
		
		
		// Calling a Async Background thread
		new LoadSinglePlaceDetails().execute(reference);
		
		
		adView = (AdView)this.findViewById(R.id.adView);
		
		AdRequest adRequest = new AdRequest.Builder()	
		.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
	    .build();
	    
	  adView.loadAd(adRequest);
	  
	  adView.setAdListener(new AdListener() {
		  public void onAdLoaded() {}
		  public void onAdFailedToLoad(int errorcode) {
			  System.out.println("Error:"+errorcode);
		  }
		  
		 
		});
		
		
	}
	
	
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
	
		case R.id.action_search:
			// search action
			RelativeLayout relativeLayout=(RelativeLayout)findViewById(R.id.single_place_root_layout);		
			addSearchLayout(relativeLayout);			
			return true;
		
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	
	
	@Override
	public void onPause() {
	  adView.pause();
	  super.onPause();
	}

	@Override
	public void onResume() {
	  super.onResume();
	  adView.resume();
	}

	@Override
	public void onDestroy() {
	  adView.destroy();
	  super.onDestroy();
	}
	
	
	/**
	 * Background Async Task to Load Google places
	 * */
	class LoadSinglePlaceDetails extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new MyProgressDialog(SinglePlaceActivity.this);
			/*pDialog.setMessage("Loading...");
			pDialog.setIndeterminate(false);*/
			pDialog.setCancelable(false);
			//pDialog.show();
			findViewById(R.id.map_custom_progress).setVisibility(View.VISIBLE);
		}

		/**
		 * getting Profile JSON
		 * */
		protected String doInBackground(String... args) {
			String reference = args[0];
			
			// creating Places class object
			googlePlaces = new GooglePlaces();

			// Check if used is connected to Internet
			try {
				placeDetails = googlePlaces.getPlaceDetails(reference);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
		
		private void setUpMapIfNeeded(LatLng latlong) {
	        // Do a null check to confirm that we have not already instantiated the map.
	        if (mMap == null) {
	            // Try to obtain the map from the SupportMapFragment.
	            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.singlemap))
	                    .getMap();
	            // Check if we were successful in obtaining the map.
	            if (mMap != null) {
	                setUpMap(latlong);
	            }
	        }
	    }
		
		 private void setUpMap(LatLng latlong) {
		        // Hide the zoom controls as the button panel will cover it.
		        mMap.getUiSettings().setZoomControlsEnabled(true);
		        
		        mMap.setMyLocationEnabled(true);
		        mMap.getUiSettings().setZoomGesturesEnabled(true);

		        // Add lots of markers to the map.`
		        addMarkersToMap(latlong);
		        
		        /*
		        // Setting an info window adapter allows us to change the both the contents and look of the
		        // info window.
		        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(){
		        	
		        });*/

		        // Set listeners for marker events.  See the bottom of this class for their behavior.
		        /*mMap.setOnMarkerClickListener(this);
		        mMap.setOnInfoWindowClickListener(this);
		        mMap.setOnMarkerDragListener(this);
		        mMap.setOnMyLocationButtonClickListener(this);*/

		        // Pan to see all markers in view.
		        // Cannot zoom to bounds until the map has a size.
		        final LatLng desttinationLatlng=latlong; 
		        
		        final View mapView = getSupportFragmentManager().findFragmentById(R.id.singlemap).getView();
		        if (mapView.getViewTreeObserver().isAlive()) {
		            mapView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
		                @SuppressWarnings("deprecation") // We use the new method when supported
		                @SuppressLint("NewApi") // We check which build version we are using.
		                @Override
		                public void onGlobalLayout() {
		                    LatLngBounds bounds = new LatLngBounds.Builder()
		                            .include(desttinationLatlng)		                           
//		                            .include(DHAKA)
//		                            .include(CHITTAGONG)
//		                            .include(RAJSHAHI)
//		                            .include(KHULNA)
		                            .build();
		                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		                      mapView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
		                    } else {
		                      mapView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
		                    }
		                    mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 150));
		                    mMap.moveCamera(CameraUpdateFactory.zoomTo(15.0f));
		                }
		            });
		        }
		    }
		 
		 
		 private void addMarkersToMap(LatLng latlong) {
		     

		        // A few more markers for good measure.
		        mPerth = mMap.addMarker(new MarkerOptions()
		                .position(latlong)
		                .title(placeDetails.result.name)
		                .snippet(placeDetails.result.formatted_address)
		                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker)));
		       
		    }

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			//pDialog.dismiss();
			findViewById(R.id.map_custom_progress).setVisibility(View.GONE);
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					if(placeDetails != null){
						String status = placeDetails.status;
						
						// check place deatils status
						// Check for all possible status
						if(status.equals("OK")){
							if (placeDetails.result != null) {
								System.out.println("Place Details: " +GooglePlaces.PLACES_DETAILS_URL+
										""+placeDetails.result.reference+"&sensor=true&key="+GooglePlaces.API_KEY);
								String name = placeDetails.result.name;
								String address = placeDetails.result.formatted_address;
								String phone = placeDetails.result.formatted_phone_number;
								double latitude = placeDetails.result.geometry.location.lat;
								double longitude = placeDetails.result.geometry.location.lng;
								float rating=placeDetails.result.rating;
																
								
								//set direction path
								LatLng origin = myLatLng;
								LatLng dest =new LatLng(latitude, longitude);
								
								setUpMapIfNeeded(dest);
								
								
								
								// Getting URL to the Google Directions API
								String url = getDirectionsUrl(origin, dest);				
								
								DownloadTask downloadTask = new DownloadTask();
								
								// Start downloading json data from Google Directions API
								downloadTask.execute(url);
								
								String link=null;
								int maxwidth=0;
								
								/*if( placeDetails.result.photos!=null){
								 link=placeDetails.result.photos[0].photo_reference;
								 maxwidth =placeDetails.result.photos[0].width;
								 
									String url="https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="+link+"&sensor=true&key="+GooglePlaces.API_KEY;
									System.out.println("photo_link: " +url);
									ImageLoader loader= new ImageLoader(getApplicationContext());
									ImageView imageView=(ImageView)findViewById(R.id.singleImage);
									loader.DisplayImage(url, imageView);
										
								 
								}*/
								
								
								
								Log.d("Place ", name + address + phone + latitude + longitude);
								

								
								// Check for null data from google
								// Sometimes place details might missing
								name = name == null ? "Not present" : name; // if name is null display as "Not present"
								final String restName=name;
								address = address == null ? "Not present" : address;
								phone = phone == null ? "None" : phone;
								/*latitude = latitude == null ? 0 : latitude;
								longitude = longitude == null ? 0: longitude;*/
								
								//ratingBar.setRating(rating);
								//txtRate.setText(""+rating);
								lbl_name.setText(name);
								lbl_address.setText(address);
								
								//lbl_phone.setText(Html.fromHtml("<b>Call</b> " ));
								final String finalPhone=phone;
								if(finalPhone.equals("None")){lbl_phone.setVisibility(View.INVISIBLE);}
								lbl_phone.setOnClickListener(new OnClickListener() {									
									@Override
									public void onClick(View v) {
										if(!finalPhone.equals("None")){
											//call to restaurant
											phoneCallIntent(finalPhone, restName);
											
										}else{
											Toast.makeText(getApplicationContext(), "Not Available.", Toast.LENGTH_LONG).show();
										}
										
									}
								});
								
								/*lbl_location.setText(Html.fromHtml("<b>Latitude:</b> " + latitude + ", <b>Longitude:</b> " + longitude));
								//ratingBar.setRating(3);
								lbl_location.setVisibility(View.GONE);*/
							}
						}
						else if(status.equals("ZERO_RESULTS")){
							alert.showAlertDialog(SinglePlaceActivity.this, getString(R.string.app_name),
									"Sorry no place found.",
									false);
						}
						else if(status.equals("UNKNOWN_ERROR"))
						{
							alert.showAlertDialog(SinglePlaceActivity.this, "Places Error",
									"Sorry unknown error occured.",
									false);
						}
						else if(status.equals("OVER_QUERY_LIMIT"))
						{
							alert.showAlertDialog(SinglePlaceActivity.this, "Places Error",
									"Sorry query limit to google places is reached",
									false);
						}
						else if(status.equals("REQUEST_DENIED"))
						{
							alert.showAlertDialog(SinglePlaceActivity.this, "Places Error",
									"Sorry error occured. Request is denied",
									false);
						}
						else if(status.equals("INVALID_REQUEST"))
						{
							alert.showAlertDialog(SinglePlaceActivity.this, "Places Error",
									"Sorry error occured. Invalid Request",
									false);
						}
						else
						{
							alert.showAlertDialog(SinglePlaceActivity.this, "Places Error",
									"Sorry error occured.",
									false);
						}
					}else{
						alert.showAlertDialog(SinglePlaceActivity.this, "Places Error",
								"Sorry error occured.",
								false);
					}
					
					
				}
			});

		}
		
	
	}
	
	
	void phoneCallIntent(final String finalPhone, String restName) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		// set title
		alertDialogBuilder.setTitle("Call "+ restName);

		// set dialog message
		alertDialogBuilder
				.setMessage( finalPhone )
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

								Intent callIntent = new Intent(
										Intent.ACTION_CALL);
								callIntent.setData(Uri.parse("tel:"
										+ finalPhone));
								startActivity(callIntent);
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	
	//direction route
	private String getDirectionsUrl(LatLng origin,LatLng dest){
		
		// Origin of route
		String str_origin = "origin="+origin.latitude+","+origin.longitude;
		
		// Destination of route
		String str_dest = "destination="+dest.latitude+","+dest.longitude;		
		
					
		// Sensor enabled
		String sensor = "sensor=false";			
					
		// Building the parameters to the web service
		String parameters = str_origin+"&"+str_dest+"&"+sensor;
					
		// Output format
		String output = "json";
		
		// Building the url to the web service
		String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;
		
		
		return url;
	}
	
	/** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException{
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
                URL url = new URL(strUrl);

                // Creating an http connection to communicate with url 
                urlConnection = (HttpURLConnection) url.openConnection();

                // Connecting to url 
                urlConnection.connect();

                // Reading data from url 
                iStream = urlConnection.getInputStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

                StringBuffer sb  = new StringBuffer();

                String line = "";
                while( ( line = br.readLine())  != null){
                        sb.append(line);
                }
                
                data = sb.toString();

                br.close();

        }catch(Exception e){
                Log.d("Exception while downloading url", e.toString());
        }finally{
                iStream.close();
                urlConnection.disconnect();
        }
        return data;
     }

	
	
	// Fetches data from url passed
	private class DownloadTask extends AsyncTask<String, Void, String>{			
				
		// Downloading data in non-ui thread
		@Override
		protected String doInBackground(String... url) {
				
			// For storing data from web service
			String data = "";
					
			try{
				// Fetching the data from web service
				data = downloadUrl(url[0]);
			}catch(Exception e){
				Log.d("Background Task",e.toString());
			}
			return data;		
		}
		
		// Executes in UI thread, after the execution of
		// doInBackground()
		@Override
		protected void onPostExecute(String result) {			
			super.onPostExecute(result);			
			
			ParserTask parserTask = new ParserTask();
			
			// Invokes the thread for parsing the JSON data
			parserTask.execute(result);
				
		}		
	}
	
	/** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{
    	
    	// Parsing the data in non-ui thread    	
		@Override
		protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
			
			JSONObject jObject;	
			List<List<HashMap<String, String>>> routes = null;			           
            
            try{
            	jObject = new JSONObject(jsonData[0]);
            	DirectionsJSONParser parser = new DirectionsJSONParser();
            	
            	// Starts parsing data
            	routes = parser.parse(jObject);    
            }catch(Exception e){
            	e.printStackTrace();
            }
            return routes;
		}
		
		// Executes in UI thread, after the parsing process
		@Override
		protected void onPostExecute(List<List<HashMap<String, String>>> result) {
			ArrayList<LatLng> points = null;
			PolylineOptions lineOptions = null;
			MarkerOptions markerOptions = new MarkerOptions();
			
			// Traversing through all the routes
			for(int i=0;i<result.size();i++){
				points = new ArrayList<LatLng>();
				lineOptions = new PolylineOptions();
				
				// Fetching i-th route
				List<HashMap<String, String>> path = result.get(i);
				
				// Fetching all the points in i-th route
				for(int j=0;j<path.size();j++){
					HashMap<String,String> point = path.get(j);					
					
					double lat = Double.parseDouble(point.get("lat"));
					double lng = Double.parseDouble(point.get("lng"));
					LatLng position = new LatLng(lat, lng);	
					
					points.add(position);						
				}
				
				// Adding all the points in the route to LineOptions
				lineOptions.addAll(points);
				lineOptions.width(10);
				lineOptions.color(Color.parseColor("#C92027"));	
				
			}
			
			// Drawing polyline in the Google Map for the i-th route
			mMap.addPolyline(lineOptions);							
		}			
    }   
    


}
