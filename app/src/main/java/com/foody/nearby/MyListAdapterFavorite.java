package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.foody.jsondata.FoodyResturent;
import com.mcc.letseat.R;
import com.mcc.libs.AnimationTween;
import com.mcc.user.SavedPrefernce;
import com.squareup.picasso.Picasso;

/**
 * Custom data adapter for User favorite restaurant list
 */
public class MyListAdapterFavorite extends BaseAdapter {

	ArrayList<HashMap<String, String>> aa;
	Context c;
	String listTags[];
	
	
	HashMap<String, String> hashMap;
	int selectedPosition;
	int likeCount;
	
	ImageView imageViewfollowing;
	TextView txtLikesCount;
	protected int lastimageViewfollowing; 
	

	public MyListAdapterFavorite(Context c, ArrayList<HashMap<String, String>> aa) {
		this.aa = aa;
		this.c = c;
	}

	public MyListAdapterFavorite(Context c,
			ArrayList<HashMap<String, String>> aa, String listTags[]) {
		this.aa = aa;
		this.c = c;
		this.listTags = listTags;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return aa.size();
	}

	@Override
	public String getItem(int arg0) {
		// TODO Auto-generated method stub
		return aa.get(arg0).get("FOLLOWERID");
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	private int lastPosition = -1;
	@Override
	public View getView(int pos, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub

		View v;
		v = arg1;
		
		LayoutInflater li = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		v = li.inflate(R.layout.list_item_favorite, null);

		RelativeLayout root = (RelativeLayout) v.findViewById(R.id.follower_main_layout);

		ImageView imageViewAvatar = (ImageView) v.findViewById(R.id.imageViewFollowerAvatar);		
		TextView txtAvatarName = (TextView) v.findViewById(R.id.txtFollowerName);
		TextView txtAvatarMetadata = (TextView) v.findViewById(R.id.txtFollowReviewInfo);
		ImageView imageViewfollow = (ImageView) v.findViewById(R.id.imageViewfollow);
		
		
		
		
		
		LinearLayout layoutBottom=(LinearLayout)v.findViewById(R.id.list_item_review_bottom_boader);
		
		
		
		 
		
		 
		 final int position=pos;
		 imageViewfollow.setTag(position);
		 imageViewfollow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				selectedPosition=position;
				
				//Toast.makeText(c, "Follow me:"+v.getId()+"<>"+aa.get(position).get("USERID"), Toast.LENGTH_SHORT).show();
				resid=aa.get(position).get("Id");
			
				SavedPrefernce prefernce=new SavedPrefernce((Activity)c);
				userid=prefernce.getUserID();
				
				 imageViewfollowing=(ImageView)v;				 
				new updateFavorite().execute();
			}
		});
		 
		 
		 
		
		 
		

		

		if (pos % 2 == 0) {
			root.setBackgroundColor(Color.parseColor("#f3f3f4"));
			layoutBottom.setBackgroundColor(Color.parseColor("#f3f3f4"));
		} else {
			root.setBackgroundColor(Color.parseColor("#FFFFFF"));
			layoutBottom.setBackgroundColor(Color.parseColor("#FFFFFF"));
		}

		// String ww =aa.get(pos);
		if (listTags != null) {
			

			txtAvatarName.setText(aa.get(pos).get(listTags[0]));
			txtAvatarMetadata.setText(aa.get(pos).get(listTags[2]));
			
			//loading image
			if(aa.get(pos).get(listTags[1])!=null && !aa.get(pos).get(listTags[1]).isEmpty()){
				Picasso.with(c)
			    .load(aa.get(pos).get(listTags[1]))
			    .placeholder(R.drawable.lazy_ic)
			    .error(R.drawable.lazy_ic)
			    .into(imageViewAvatar);

			}
						
			

			

		} else {

			txtAvatarName.setText(aa.get(pos).get(""));
			txtAvatarMetadata.setText(aa.get(pos).get(""));
			
		}

		/*
		 * name.setText(aa.get(pos).get("name"));
		 * dis.setText(aa.get(pos).get("distance"));
		 * ref.setText(aa.get(pos).get("reference"));
		 */
		
		
		//apply animation on current view
		 Animation animation = AnimationUtils.loadAnimation(c, (pos > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
		 v.startAnimation(animation);
		 lastPosition = pos;

		return v;
	}
	

			
	// Google Places
	FoodyResturent foodyResturent;
	
	String response;
	
	private String resid;
	private String userid;
	
	class updateFavorite  extends AsyncTask<String, String, String> {


		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			
			AnimationTween.zoomInOut(imageViewfollowing, ((Activity)c));
			
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {
				
				 
				response=foodyResturent.insertFavorite(userid,  resid);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		
		protected void onPostExecute(String file_url) {
			
			// updating UI from Background Thread
			((Activity)c).runOnUiThread(new Runnable() {
				public void run() {
					//
					
					

					if(response.equals("Fav")){
						Toast.makeText( ((Activity)c), "Favorite Added", Toast.LENGTH_SHORT).show();
						imageViewfollowing.setImageResource(R.drawable.ic_user_fav_red);
					}else if(response.equals("UnFav")){
						Toast.makeText( ((Activity)c), "Favorite Removed", Toast.LENGTH_SHORT).show();
						imageViewfollowing.setImageResource(R.drawable.ic_user_fav_gray);
					}else{
						Toast.makeText(((Activity)c), "Try again", Toast.LENGTH_SHORT).show();
					}
					
					
					
					
				}

			});

		}

	}
	

	
	



	

		

}
