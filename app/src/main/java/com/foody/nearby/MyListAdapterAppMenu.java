package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.foody.AppData.StaticObjects;
import com.mcc.le.gcm.PushActivity;
import com.mcc.letseat.HomeT;
import com.mcc.letseat.R;
import com.mcc.letseat.quary.ExpandableActivity;
import com.mcc.libs.TouchEffectListener;
import com.mcc.sildermenu.SlidingMenu;
import com.mcc.user.RegistrationActivity;
import com.mcc.user.SavedPrefernce;
import com.mcc.user.profile.UserActivity;
import com.mcc.user.profile.UserBookedDealsList;

/**
 * 
 * @author Arif
 * app main menu data adapter
 */

public class MyListAdapterAppMenu extends BaseAdapter{
	
	ArrayList<HashMap<String, String>> aa;
	Context c;
	String listTag;
	SlidingMenu mSlideHolder;
	
	int imageResource[]=  {R.mipmap.ic_menu_nearby,R.mipmap.ic_menu_restaurant,R.mipmap.ic_menu_cuisine,R.mipmap.ic_menu_event, R.mipmap.ic_menu_nearby,R.mipmap.ic_menu_offer, R.mipmap.ic_menu_offer, R.mipmap.ic_menu_profile, R.mipmap.ic_menu_setting, R.mipmap.ic_menu_logout};
	
	public MyListAdapterAppMenu(Context c, ArrayList<HashMap<String, String>> aa, String listTag, SlidingMenu mSlideHolder ){
		this.aa = aa;
		this.c = c;
		this.listTag=listTag;
		this.mSlideHolder=mSlideHolder;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return aa.size();
	}

	@Override
	public String getItem(int arg0) {
		// TODO Auto-generated method stub
		return aa.get(arg0).get("name");
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	
	private int lastPosition = -1;
	@Override
	public View getView(int pos, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		
		View v;
		v = arg1;
		LayoutInflater li = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		v = li.inflate(R.layout.row, null);
		
		//LinearLayout root = (LinearLayout)v.findViewById(R.id.LayoutCuisine);
		
		final TextView name= (TextView)v.findViewById(R.id.textMEnuTitle);	
		Typeface tf = Typeface.createFromAsset(c.getAssets(), StaticObjects.fontPath_SEGOEUI);
		name.setTypeface(tf);
		
		ImageView imageView=(ImageView)v.findViewById(R.id.imageViewMenuIC);
		imageView.setImageResource(imageResource[pos]);
		
		
		//String ww =aa.get(pos);
		name.setText(aa.get(pos).get(listTag));		
		name.setSelected(false);
		final int position=pos;
		
		v.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				if( checkNetConnectionShowToast() )				
				menuNevigation(position,  view);
				
			}
		});
		
		//for on
		v.setOnTouchListener(new TouchEffectListener("#FFFFFF", "#eeeded"));
		
		/*name.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				menuNevigation(position);
				
			}
		});
		imageView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				menuNevigation(position);
				
			}
		});
		
		v.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				menuNevigation(position);
				
			}
		});*/
		
		
		//apply animation on current view
		 Animation animation = AnimationUtils.loadAnimation(c, (pos > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
		 v.startAnimation(animation);
		 lastPosition = pos;
		
		return v;
	}
	
	/*cityID = 0,
			LocationID=1,
			resturentID = 2, 
			cuisineID = 3,
			nearbyID = 4, 
			offerID = 5,  
			profileID=6, 
			settingID=7,  
			logoutID=8 ,
			searchAnyID = 9;*/
	
	void menuNevigation(int pos, View view){
		//close sliding menu
		mSlideHolder.toggle();
		
		
		if (pos== StaticObjects.LocationID ) {
			
			Intent intent = new Intent(((Activity)c), ExpandableActivity.class);
			intent.putExtra(StaticObjects.NEV_TYPE,StaticObjects.NEV_TYPE_CHOOSE_LOCTION_N_SAVE);
			((Activity)c).startActivity(intent);
			
			
			/*SavedPrefernce savedPrefernce = new SavedPrefernce( ((Activity)c) );			
			String disId=savedPrefernce.getDistrictID(  );
			if (disId != null && !disId.isEmpty() && !disId.equals("")) {
			
				
					Intent in = new Intent(((Activity)c),LocatonListView.class);
					in.putExtra(StaticObjects.NEV_TYPE,StaticObjects.NEV_TYPE_CHOOSE_LOCTION_N_SAVE);
					in.putExtra("DisId", disId);
					in.putExtra("DisName", savedPrefernce.getDistrict() );
					((Activity)c).startActivity(in);
				
			
			}else {
				Intent intent = new Intent(((Activity)c), DistrictListView.class);
				intent.putExtra(StaticObjects.NEV_TYPE,StaticObjects.NEV_TYPE_CHOOSE_LOCTION_N_SAVE);
				((Activity)c).startActivity(intent);
			}*/
			

		}
		else if (pos == StaticObjects.resturentID) {
			
			SavedPrefernce savedPrefernce=new SavedPrefernce((Activity)c);
			String LocId=savedPrefernce.getLocationId();
			String LocName=savedPrefernce.getLocationName();
			
			if(LocId!=null && !LocId.isEmpty() && !LocId.equals("")){
				//Intent i = new Intent(c, ResturentListView.class);
				Intent i = new Intent(c, ResturentListViewImage.class);
				i.putExtra("LocId", LocId);
				i.putExtra("LocName", LocName);
				
				((Activity)c).startActivity(i);
				
			}else{
				Intent i = new Intent(c, DistrictListView.class);
				i.putExtra(StaticObjects.NEV_TYPE, StaticObjects.NEV_TYPE_CHOOSE_LOCTION_N_SAVE);			
				((Activity)c).startActivity(i);
			}

		} else if (pos == StaticObjects.cuisineID) {
			
			Intent i = new Intent(c, CusineListView.class);
			
			((Activity)c).startActivity(i);

		}else if (pos == StaticObjects.eventID) {
			
			//Intent i = new Intent(c, FeaturedRestaurantListView.class);
			Intent i = new Intent(c, EventListActivity.class);			
			((Activity)c).startActivity(i);

		}  else if (pos == StaticObjects.nearbyID) {
			/*Intent i = new Intent(c, NearByplaceListView.class);			
			((Activity)c).startActivity(i);*/
			
			Intent i = new Intent(c, NearByNativeRestaurantListNew.class);
			((Activity)c).startActivity(i);

		} else if (pos == StaticObjects.searchAnyID) {
			// hide soft keyboard
			InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
			//imm.hideSoftInputFromWindow(txtSearch.getWindowToken(), 0);			
			Intent i = new Intent(c, SearchResturentListView.class);
			
			((Activity)c).startActivity(i);

		} else if (pos == StaticObjects.offerID) {

			Intent i = new Intent(c, OfferActivity.class);
			
			((Activity)c).startActivity(i);
		} else if (pos == StaticObjects.bookedDealsID) {
			
			if(!new SavedPrefernce( (Activity)c ).isUserExists()){					
				StaticObjects.callRegForAction(c);
				
			}else{
				Intent i = new Intent(c, UserBookedDealsList.class);			
				((Activity)c).startActivity(i);
			}
			
		} else if (pos == StaticObjects.settingID) {

			Intent i = new Intent(c, PushActivity.class);
			
			((Activity)c).startActivity(i);
			
		} else if (pos == StaticObjects.profileID) {
			
			
			
			 if(!new SavedPrefernce( (Activity)c ).isUserExists()){					
					StaticObjects.callRegForAction(c);
					
				}else{
					/*Intent intent=new Intent(c, RegistrationActivity.class);
					//intent.putExtra("userid", new SavedPrefernce().getUserID(getActivity()) );
					//intent.putExtra("resid", getArguments().getString("restaurantId"));					
					((Activity)c).startActivity(intent);*/
					
					Intent intent=new Intent(c, UserActivity.class);
					intent.putExtra("userid", new SavedPrefernce( (Activity)c ).getUserID() );
					//intent.putExtra("resid", getArguments().getString("restaurantId"));					
					((Activity)c).startActivity(intent);
				}

			/*Intent i = new Intent(c, RegistrationOptionActivity.class);
			
			((Activity)c).startActivity(i);*/
		}else if(pos==StaticObjects.logoutID){
			SavedPrefernce savedPrefernce=new SavedPrefernce(((Activity)c) );
			if(savedPrefernce.isUserExists( ) ){
				// code for logout
				Toast.makeText(c, "Loging Out...", Toast.LENGTH_SHORT).show();
				//((Activity)c).finish(); 
				
				savedPrefernce.removeSharedpreferences();
				
				TextView name= (TextView)view.findViewById(R.id.textMEnuTitle);	
				if(name!=null){
					name.setText("Login");
				}
				
			}else{
				StaticObjects.callRegForAction(c);
				TextView name= (TextView)view.findViewById(R.id.textMEnuTitle);	
				if(name!=null){
					name.setText("Logout");
				}
			}
				
		}
		
	}
	
	
	public boolean checkNetConnectionShowToast() {
		
		if (!new ConnectionDetector(c.getApplicationContext()).isConnectingToInternet()) {
			// Internet Connection is not present
			Toast.makeText(c, c.getString(R.string.internetConMsg),Toast.LENGTH_SHORT).show();
			return false;
		}else		
			return true;
	}
	
	public boolean checkNetConnection() {
		
		// Check if Internet present
		return new ConnectionDetector(c.getApplicationContext()).isConnectingToInternet();

	}
	
	public void  showNetworkMsg(){
		Toast.makeText(c, c.getString(R.string.internetConMsg),Toast.LENGTH_SHORT).show();
	}

}
