package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.foody.AppData.StaticObjects;
import com.mcc.letseat.R;
import com.mcc.libs.CF;
import com.squareup.picasso.Picasso;

public class ListAdapterRestaurantImage extends BaseAdapter{
	
	
	
	
	Typeface tfLight ,tfNormal ;	
	
	ArrayList<HashMap<String, String>> aa;
	Context c;
	String listTags[];
	
	public ListAdapterRestaurantImage(Context c, ArrayList<HashMap<String, String>> aa){
		this.aa = aa;
		this.c = c;
		 tfLight = Typeface.createFromAsset(c.getAssets(), StaticObjects.fontPath_SEGOEUI_light);
		 tfNormal = Typeface.createFromAsset(c.getAssets(), StaticObjects.fontPath_SEGOEUI);
	}
	
	public ListAdapterRestaurantImage(Context c, ArrayList<HashMap<String, String>> aa, String listTags[]){
		this.aa = aa;
		this.c = c;
		this.listTags=listTags;
		 tfLight = Typeface.createFromAsset(c.getAssets(), StaticObjects.fontPath_SEGOEUI_light);
		 tfNormal = Typeface.createFromAsset(c.getAssets(), StaticObjects.fontPath_SEGOEUI);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return aa.size();
	}

	@Override
	public String getItem(int arg0) {
		// TODO Auto-generated method stub
		return aa.get(arg0).get("reference");
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	private int lastPosition=-1;
	@Override
	public View getView(int pos, View v, ViewGroup arg2) {
		// TODO Auto-generated method stub
		
		if (v == null) {
			LayoutInflater li = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = li.inflate(R.layout.list_item_resttaurant, null);
		} 
		
		
		RelativeLayout root = (RelativeLayout)v.findViewById(R.id.layoutOfferList);
		
		TextView txtOfferFor= (TextView)v.findViewById(R.id.txtOfferFor);
		txtOfferFor.setTypeface(tfNormal);
		
		TextView txtOfferProvider = (TextView)v.findViewById(R.id.txtOfferProvider);
		txtOfferProvider.setTypeface(tfNormal);
		
		TextView txtCuisineType = (TextView)v.findViewById(R.id.txtCuisineType);
		txtCuisineType.setTypeface(tfNormal);
		
		ImageView imgOpenClose = (ImageView)v.findViewById(R.id.imgOpenClose);
		
		
		ImageView imageView = (ImageView)v.findViewById(R.id.imageViewOfferList);
		ImageView imageCard_icon= (ImageView)v.findViewById(R.id.imageCard_icon);
		
		RatingBar  ratingBarResListItem=(RatingBar)v.findViewById(R.id.ratingBarResListItem);
		
		
		if(pos%2==0)
		{
			root.setBackgroundColor(Color.parseColor("#FCFCFC"));
		}
		else
		{
			root.setBackgroundColor(Color.parseColor("#FFFFFF"));
		}
		
		//String ww =aa.get(pos);
		if(listTags!=null){
			txtOfferFor.setText(aa.get(pos).get(listTags[0]));
			txtOfferProvider.setText(aa.get(pos).get(listTags[1]));
			txtCuisineType.setText(aa.get(pos).get(listTags[2]));
			
			float rating=Float.parseFloat(aa.get(pos).get(listTags[2]) );
			
			ratingBarResListItem.setRating(rating/StaticObjects.RATING_OFFSET);
			
			if(aa.get(pos).get(listTags[3]).toString().equals(""+CF.OPEN)){
				imgOpenClose.setImageResource(R.mipmap.ic_open);
			}
			
			
			//imageCard_icon will show different card icon based card like visa, master, still database has no option to provide this data
			imageCard_icon.setImageResource(R.drawable.ic_visa);
			imageCard_icon.setVisibility(View.GONE);
			
			
			
			//loading image
			if(aa.get(pos).get(listTags[4])!=null && !aa.get(pos).get(listTags[4]).isEmpty()){
				Picasso.with(c)
			    .load(aa.get(pos).get(listTags[4]))
			    .placeholder(R.drawable.lazy_ic)
			    .error(R.drawable.lazy_ic)
			    .into(imageView);
			}
			
			
			//Log.e("offer image link", "link: "+aa.get(pos).get(listTags[4])+"0"+aa.get(pos).get(listTags[0])+"<1:"+aa.get(pos).get(listTags[1])+"<2:"+aa.get(pos).get(listTags[2])+"<3:"+aa.get(pos).get(listTags[3]));
			//System.out.println(""+aa.get(pos).get(listTags[4]));
			
			
		}else{
			txtOfferFor.setText(aa.get(pos).get(""));
			txtOfferProvider.setText(aa.get(pos).get(""));
			txtCuisineType.setText(aa.get(pos).get(""));
			imgOpenClose.setImageResource(R.mipmap.ic_close);
		}
		
		/*name.setText(aa.get(pos).get("name"));
		dis.setText(aa.get(pos).get("distance"));
		ref.setText(aa.get(pos).get("reference"));
*/		
		
		//apply animation on current view
		 Animation animation = AnimationUtils.loadAnimation(c, (pos > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
		 v.startAnimation(animation);
		 lastPosition = pos;
		
		return v;
	}

}
