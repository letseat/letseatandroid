package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;

import android.animation.Animator;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.Restaurent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mcc.letseat.CapturePhotoActivity;
import com.mcc.letseat.R;
import com.mcc.letseat.tab.TabMainActivity;
import com.mcc.libs.ActivityWithSliding;
import com.mcc.libs.AnimationTween;
import com.mcc.libs.TouchEffectListener;
import com.mcc.user.SavedPrefernce;

/**
 *  List of Restaurant in a particular location e.i: Dhaka > Banani
 */
public class ResturentListView extends ActivityWithSliding implements
		ActionBar.OnNavigationListener {

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Google Places
	FoodyResturent foodyResturent;

	// restaurant List
	ArrayList<Restaurent> resturentList;


	// Button
	ImageView btnShowOnMap;

	

	// Places Listview
	ListView lv;

	// ListItems data
	ArrayList<HashMap<String, String>> resturantListItems = new ArrayList<HashMap<String, String>>();

	// //admob
	AdView adView = null;

	ImageView searchAny;

	String searchKeywords;
	
	//post value
	String LocId;
	int currentPage=0;
	int totalPage=1;
	
	TextView txtTitleNearBy;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_main);
		findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);
		
		
		// Getting listview
		lv = (ListView) findViewById(R.id.list);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				TouchEffectListener tel1=new TouchEffectListener("#FCFCFC", "#ced2d8");
				TouchEffectListener tel2=new TouchEffectListener("#FFFFFF", "#ced2d8");
				
				

				//Restaurent resturent = (Restaurent) resturentList.get(position);
				HashMap<String, String> map= resturantListItems.get(position);
				String resid=resturantListItems.get(position).get("Id");//map.get("Id");
				String RestName=resturantListItems.get(position).get("RestName");//map.get("Id");
				
				
				//Restaurent r=resturentList.get(position);
				
				//restaurentId= resturent.Id;
				
				//Intent in = new Intent(getApplicationContext(),RestaurantTabActivity.class);
				String nev_type=getIntent().getStringExtra(StaticObjects.NEV_TYPE);
				
				if(nev_type!=null && nev_type.equals(StaticObjects.NEV_TYPE_CHOOSE_RES_PHOTO_UP)){
					Intent in=new Intent();
					in.putExtra( "Id" , resid);	
					in.putExtra( "RestName" , RestName);	
					setResult(RESULT_OK, in);
					CapturePhotoActivity.resid=resid;
					CapturePhotoActivity.resname=RestName;
					
					finish();
				}else{
					//Intent in = new Intent(getApplicationContext(),RestaurantTabActivity.class);
					Intent in = new Intent(getApplicationContext(),TabMainActivity.class);
					in.putExtra( "Id" , resid);				
					/*.putExtra( "latitude" , r.latitude)
					.putExtra( "longitude" , r.longitude)
					.putExtra( "RestName" , r.RestName)
					.putExtra( "address" , r.RestLoc);*/
					startActivity(in);
				}
								
				
				
				
			}
		});
		lv.setVisibility(View.INVISIBLE);
		
		
		/*lv.setOnScrollListener(new OnScrollListener() {
			
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;

			        if(loadMore && Page>=1) {
			        	Toast.makeText(getApplicationContext(), "load More", Toast.LENGTH_SHORT).show();
			        	
			        	new LoadPlaces().execute();
			        	Page++;
			        }
				}
		});*/

		adView = (AdView) this.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder().build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});
		
		
		LocId=getIntent().getStringExtra("LocId");
		new LoadPlaces().execute();
		
		txtTitleNearBy=(TextView)findViewById(R.id.txtTitleNearBy);
		txtTitleNearBy.setText("Restaurants in "+getIntent().getStringExtra("LocName"));
		//txtTitleNearBy.setVisibility(View.INVISIBLE);
		//findViewById(R.id.ll_search_hint).setVisibility(View.INVISIBLE);

		setHomeButton(this);
	}
	
	
	
	
	//========

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
	
		case R.id.action_search:
			// search action
			RelativeLayout relativeLayout=(RelativeLayout)findViewById(R.id.rest_list_root_layout);		
			addSearchLayout(relativeLayout);			
			return true;
		
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	
	
	

	@Override
	public void onPause() {
		super.onPause();

		if (adView != null)
			adView.pause();

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		if (adView != null)
			adView.destroy();
		super.onDestroy();
	}

	class LoadPlaces extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			lv.setOnScrollListener(null);
			super.onPreExecute();
			
			findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {

				//String types = "LocId=" + LocId+"&Page="+currentPage;				
				resturentList = foodyResturent.getRsturent(FoodyResturent.TYPE_LOAD_ZONE,new SavedPrefernce(ResturentListView.this).getZoneID(), currentPage, ResturentListView.this);
				

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			//pDialog.dismiss();
			//findViewById(R.id.main_custom_progress).setVisibility(View.GONE);
			final Animator.AnimatorListener animatorListener= new Animator.AnimatorListener() {
				
				@Override
				public void onAnimationStart(Animator animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationRepeat(Animator animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animator animation) {
					// TODO Auto-generated method stub
					lv.setVisibility(View.VISIBLE);
					AnimationTween.animateViewPosition(lv, ResturentListView.this);
				}
				
				@Override
				public void onAnimationCancel(Animator animation) {
					// TODO Auto-generated method stub
					
				}
			};
			
			Animator.AnimatorListener animatorListener2= new Animator.AnimatorListener() {
				
				@Override
				public void onAnimationStart(Animator animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationRepeat(Animator animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animator animation) {
					// TODO Auto-generated method stub
					//txtTitleNearBy.setVisibility(View.VISIBLE);
					
					AnimationTween.animateViewPosition(findViewById(R.id.ll_search_hint), animatorListener, ResturentListView.this);
				}
				
				@Override
				public void onAnimationCancel(Animator animation) {
					// TODO Auto-generated method stub
					
				}
			};
			
			
			AnimationTween.animateView(findViewById(R.id.main_custom_progress), animatorListener2, ResturentListView.this);
			
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status
				

					if (resturentList == null || resturentList.size() <= 0) {

						
						
						String lat=getIntent().getStringExtra("Lat");
						String lng=getIntent().getStringExtra("Lng");
						
						if( lat!=null && lng!=null && !lat.equals("") && !lng.equals("")  ){
							finish();
							Intent in = new Intent(getApplicationContext(),NearByplaceListViewDivision.class);
							in.putExtra( "Lat" , lat); 
							in.putExtra( "Lng" , lng);
							in.putExtra("LocName", getIntent().getStringExtra("LocName"));
							startActivity(in);
						}else{
							
							//show message
							findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);
							View v = findViewById(R.id.main_custom_progress);
							TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
							textPreparing.setText("Nothing found.");
							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
							
							AnimationTween.animateViewPosition(findViewById(R.id.main_custom_progress), ResturentListView.this);

						}
						
						
						
						 
					} else if (resturentList != null) {
						// Check for all possible status

						// Successfully got places details
						if (resturentList != null) {
							// loop through each place
							for (Restaurent p : resturentList) {

								HashMap<String, String> map = new HashMap<String, String>();
								map.put("Id", p.Id);
								// Place name
								map.put("RestName", p.RestName);
								map.put("RestLoc", p.RestLoc+", "+p.RestDistName);
								totalPage=Integer.parseInt(p.PageNo);

								System.out
										.println(">>>>>>>>>>test>>>>>>>>>>>>>>>>>> "
												+ p.RestLoc);

								// adding HashMap to ArrayList
								resturantListItems.add(map);
							}
							// list adapter
							MyListAdapter adapter = new MyListAdapter(
									ResturentListView.this,
									resturantListItems, 
									new String[] { "RestName", "RestLoc", "Id" });

							// Adding data into listview
							lv.setAdapter(adapter);
							adapter.notifyDataSetChanged();
							currentPage++;
							
							lv.setOnScrollListener(new OnScrollListener() {
								
								@Override
								public void onScrollStateChanged(AbsListView view, int scrollState) {
									// TODO Auto-generated method stub
									
								}
								
								@Override
								public void onScroll(AbsListView view, int firstVisibleItem,
										int visibleItemCount, int totalItemCount) {
									// TODO Auto-generated method stub
									boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;

								        if(loadMore && currentPage<totalPage) {
								        	Toast.makeText(getApplicationContext(), "load More", Toast.LENGTH_SHORT).show();
								        	
								        	new LoadPlaces().execute();
								        	
								        }
									}
							});
							
							
							
						}

						else if (resturentList == null) {
							//show message
							findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);
							View v = findViewById(R.id.main_custom_progress);
							TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
							textPreparing.setText("Nothing found.");
							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
							AnimationTween.animateViewPosition(findViewById(R.id.main_custom_progress), ResturentListView.this);

						}
					}
				}

			});

		}

	}
	
	
	
	

	@Override
	public boolean onNavigationItemSelected(int arg0, long arg1) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	private void showPopUp() {

		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
		// helpBuilder.setTitle("Pop Up");
		helpBuilder.setMessage("Nothing Found. Want to request this suggestion?");
		helpBuilder.setPositiveButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {

					}
				});

		helpBuilder.setNegativeButton("Yes",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						Intent in = new Intent(
								getApplicationContext(),
								SuggetionRequest.class);
						in.putExtra("keyword", searchKeywords);

						startActivity(in);
					}
				});

		// Remember, create doesn't show the dialog
		AlertDialog helpDialog = helpBuilder.create();
		helpDialog.show();

	}

	
}
