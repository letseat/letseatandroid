package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.SearchResturent;
import com.foody.jsondata.user.UserPubInfo;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.mcc.letseat.CapturePhotoActivity;
import com.mcc.letseat.R;
import com.mcc.letseat.tab.TabMainActivity;
import com.mcc.libs.ActivityWithSliding;
import com.mcc.libs.AnimationTween;
import com.mcc.user.SavedPrefernce;
import com.mcc.user.profile.UserActivity;

/**
 * Search result ListView use in Search and advance Search
 */
public class SearchUserListView extends ActivityWithSliding implements
		ActionBar.OnNavigationListener {

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Google Places
	FoodyResturent foodyResturent;

	// Places List
	ArrayList<UserPubInfo> resturentList;

	// Button
	ImageView btnShowOnMap;

	

	// Places Listview
	ListView lv;

	// ListItems data
	ArrayList<HashMap<String, String>> placesListItems = new ArrayList<HashMap<String, String>>();

	// //admob
	AdView adView = null;

	ImageView searchAny;

	String searchKeywords;
	
	TextView txtTitleNearBy;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.slide_up_in,R.anim.stay);
		
		setContentView(R.layout.n_activity_main);

		//
		txtTitleNearBy= (TextView) findViewById(R.id.txtTitleNearBy);
		//show search msg
		findViewById(R.id.txtSearchMsg).setVisibility(View.VISIBLE);
		
		
		// Getting listview
		lv = (ListView) findViewById(R.id.list);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				UserPubInfo userPubInfo = (UserPubInfo) resturentList.get(position);
				Intent in = new Intent(SearchUserListView.this,UserActivity.class);
				in.putExtra("userid", userPubInfo.ID);				
				startActivity(in);
				

				
			}
		});

		adView = (AdView) this.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder().build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});

		handleIntent(getIntent());

	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
	
		case R.id.action_search:
			// search action
			RelativeLayout relativeLayout=(RelativeLayout)findViewById(R.id.rest_list_root_layout);		
			addSearchLayout(relativeLayout);			
			return true;
		
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu ) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate( R.menu.activity_main, menu );
		menu.findItem(R.id.action_search).setVisible(false);
		
	    return true;
	}

	private void handleIntent(Intent intent) {
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			searchKeywords = null;
			searchKeywords = intent.getStringExtra(SearchManager.QUERY);
			new LoadPlaces().execute();
			//txtTitleNearBy.setText("Showing results for: "+ searchKeywords);

		} else if (intent.getStringExtra("searchKeyWord_home") != null) {
			searchKeywords = null;
			searchKeywords = intent.getStringExtra("searchKeyWord_home");
			new LoadPlaces().execute();
			//txtTitleNearBy.setText("Showing results for: ");
		}
		
		//
		// Spannable string allows us to edit the formatting of the text.
       /* SpannableString titleText = new SpannableString(searchKeywords);
        titleText.setSpan(new ForegroundColorSpan(Color.RED), 0, titleText.length(), 0);
        txtTitleNearBy.setText(txtTitleNearBy.getText()+""+titleText);*/
        
       
        Spannable WordToSpan = new SpannableString("Search result for ");        
        WordToSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#333333")), 0, WordToSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtTitleNearBy.setText(WordToSpan);
        
        Spannable WordToSpan1 = new SpannableString("'"+searchKeywords+"'");        
        WordToSpan1.setSpan(new ForegroundColorSpan(Color.parseColor("#BE1010")), 0, WordToSpan1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtTitleNearBy.append(WordToSpan1);

	}

	@Override
	protected void onNewIntent(Intent intent) {
		setIntent(intent);
		handleIntent(intent);
	}

	/*// action bar menu and event
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_main, menu);
		// Associate searchable configuration with the SearchView
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) menu.findItem(R.id.action_search)
				.getActionView();
		searchView.setSearchableInfo(searchManager
				.getSearchableInfo(getComponentName()));

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
		case android.R.id.home:
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
		case R.id.action_search:
			// search action
			return true;
		case R.id.action_nearby:
			Intent i=new Intent(SearchResturentListView.this,NearByplaceListView.class);			
			startActivity(i);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}*/

	@Override
	public void onPause() {
		super.onPause();
		overridePendingTransition(R.anim.stay,R.anim.slide_down_out);
		if (adView != null)
			adView.pause();
		
		//overridePendingTransition(0,R.anim.view_close_translate);

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		
		if (adView != null)
			{adView.destroy();}
		
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}
	
	@Override
	protected void onStart() {	
		super.onStart();
		
		//analytics stop tracking
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}
	
	String types;
	class LoadPlaces extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {

				 types = "userkeyw=" + searchKeywords;

				resturentList = foodyResturent.getUserInfo(FoodyResturent.USER_SEARCH_URL, types);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			//findViewById(R.id.main_custom_progress).setVisibility(View.GONE);
			AnimationTween.animateView(findViewById(R.id.main_custom_progress), SearchUserListView.this);
			AnimationTween.animateViewPosition(lv, SearchUserListView.this);
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status

					if (resturentList == null || resturentList.size() <= 0) {

						//show popup for sugest request
						showPopUpAdvSearch();
						
						 
					} else if (resturentList != null) {
						// Check for all possible status

						// Successfully got places details
						if (resturentList != null) {
							
							placesListItems = new ArrayList<HashMap<String, String>>();
							// loop through each place
							for (UserPubInfo p : resturentList) {

								HashMap<String, String> map = new HashMap<String, String>();
								map.put("FOLLOWER_NAME", p.NAME);
								map.put("IMG", p.IMG);
								map.put("TOTALFOLLOWER", "Follower "+p.NOFOllOWERS + ", Review "+ p.NOREVIEW);
								map.put("ID", p.ID);
								map.put("FOLLOWERID", p.ID);								
								

								
								// adding HashMap to ArrayList
								placesListItems.add(map);
							}
							/*// list adapter
							MyListAdapter adapter = new MyListAdapter(SearchUserListView.this,	placesListItems,new String[] {  "Name", "Address","Id" });
							*/// list adapter
							MyListAdapterFollower adapter = new MyListAdapterFollower(SearchUserListView.this, placesListItems,new String[] { "FOLLOWER_NAME", "IMG","TOTALFOLLOWER" });

							
							// Adding data into listview
							lv.setAdapter(adapter);
						}

						else if (resturentList == null) {
							//showPopUpAdvSearch();
							
						}
					}
				}

			});

		}

	}
	
	

	@Override
	public boolean onNavigationItemSelected(int arg0, long arg1) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	private void showPopUp() {

		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
		// helpBuilder.setTitle("Pop Up");
		helpBuilder.setMessage("Nothing Found. Want to request this suggestion?");
		helpBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						finish();
						Intent in = new Intent(
								getApplicationContext(),
								SuggetionRequest.class);
						in.putExtra("keyword", searchKeywords);

						startActivity(in);
					}
				});

		helpBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
						
					}
				});

		// Remember, create doesn't show the dialog
		AlertDialog helpDialog = helpBuilder.create();
		
		if(!(this.isFinishing()))
		{
		    //show dialog
			helpDialog.show(); 
		}

	}
	
	private void showPopUpAdvSearch() {

		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
		// helpBuilder.setTitle("Pop Up");
		helpBuilder.setMessage("No one there. Share or send to friend");
		helpBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						/*finish();
						Intent in = new Intent(getApplicationContext(),SearchAdvResturentListView.class);
						
						in.putExtra(StaticObjects.NEV_TYPE, getIntent().getStringExtra(StaticObjects.NEV_TYPE));
						in.putExtra("searchKeyWord_home", searchKeywords);

						startActivity(in);*/
						shareLink();
					}
				});

		helpBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						shareLink();
						//finish();
						
					}
				});

		// Remember, create doesn't show the dialog
		AlertDialog helpDialog = helpBuilder.create();
		if(!(this.isFinishing()))
		{
		    //show dialog
			helpDialog.show();
		}
		

	}
	
	void shareLink(/*Uri uri*/){
		/*Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
 
        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_title));
        share.putExtra(Intent.EXTRA_TEXT, uri+"");
 
       //share window title
        startActivity(Intent.createChooser(share, "Share link!"));*/
        
        try
        { Intent i = new Intent(Intent.ACTION_SEND);  
          i.setType("text/plain");
          i.putExtra(Intent.EXTRA_SUBJECT, "Let's Eat");
          String sAux = "Let me recommend you this application\n\n";
          sAux = sAux + "https://play.google.com/store/apps/details?id=com.mcc.letseat \n\n";
          i.putExtra(Intent.EXTRA_TEXT, sAux);  
          startActivity(Intent.createChooser(i, "choose one"));
        }
        catch(Exception e)
        { //e.toString();
        } 
	}

}