package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.foody.jsondata.FoodyResturent;
import com.mcc.letseat.R;
import com.mcc.user.SavedPrefernce;
import com.mcc.user.profile.UserActivity;
import com.squareup.picasso.Picasso;

/**
 * Custom data adapter for User follower list
 */
public class MyListAdapterFollower extends BaseAdapter {

	ArrayList<HashMap<String, String>> aa;
	Context c;
	String listTags[];
	
	
	HashMap<String, String> hashMap;
	int selectedPosition;
	int likeCount;
	
	ImageView imageViewfollowing;
	TextView txtLikesCount;
	protected int lastimageViewfollowing; 
	

	public MyListAdapterFollower(Context c, ArrayList<HashMap<String, String>> aa) {
		this.aa = aa;
		this.c = c;
	}

	public MyListAdapterFollower(Context c,
			ArrayList<HashMap<String, String>> aa, String listTags[]) {
		this.aa = aa;
		this.c = c;
		this.listTags = listTags;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return aa.size();
	}

	@Override
	public String getItem(int arg0) {
		// TODO Auto-generated method stub
		return aa.get(arg0).get("FOLLOWERID");
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	private int lastPosition = -1;
	@Override
	public View getView(int pos, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub

		View v;
		v = arg1;
		
		LayoutInflater li = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		v = li.inflate(R.layout.list_item_followers, null);

		RelativeLayout root = (RelativeLayout) v.findViewById(R.id.follower_main_layout);

		ImageView imageViewAvatar = (ImageView) v.findViewById(R.id.imageViewFollowerAvatar);		
		TextView txtAvatarName = (TextView) v.findViewById(R.id.txtFollowerName);
		TextView txtAvatarMetadata = (TextView) v.findViewById(R.id.txtFollowReviewInfo);
		ImageView imageViewfollow = (ImageView) v.findViewById(R.id.imageViewfollow);
		
		
		LinearLayout layoutBottom=(LinearLayout)v.findViewById(R.id.list_item_review_bottom_boader);
		
		
		final int position=pos;
		 v.setTag(position);
		 v.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					
					Intent intent=new Intent(c, UserActivity.class);
					userid=aa.get(position).get("FOLLOWERID");
					intent.putExtra("userid", userid);
					c.startActivity(intent);
					//Toast.makeText(c, "tap"+userid, Toast.LENGTH_SHORT).show();
				}
			});
		
		 
		
		 imageViewfollow.setTag(position);
		 imageViewfollow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				selectedPosition=position;
				
				//Toast.makeText(c, "Follow me:"+v.getId()+"<>"+aa.get(position).get("USERID"), Toast.LENGTH_SHORT).show();
				userid=aa.get(position).get("FOLLOWERID");
			
				
				
				 imageViewfollowing=(ImageView)v;				 
				new PostFollowers().execute();
			}
		});
		 
		 
		 
		
		 
		

		

		if (pos % 2 == 0) {
			root.setBackgroundColor(Color.parseColor("#f3f3f4"));
			layoutBottom.setBackgroundColor(Color.parseColor("#f3f3f4"));
		} else {
			root.setBackgroundColor(Color.parseColor("#FFFFFF"));
			layoutBottom.setBackgroundColor(Color.parseColor("#FFFFFF"));
		}

		// String ww =aa.get(pos);
		if (listTags != null) {
			/*
			 * first.setText(aa.get(pos).get(listTags[0]));
			 * second.setText(aa.get(pos).get(listTags[1]));
			 * third.setText(aa.get(pos).get(listTags[2]));
			 * four.setText(aa.get(pos).get(listTags[3]));
			 */

			txtAvatarName.setText(aa.get(pos).get(listTags[0]));
			txtAvatarMetadata.setText(aa.get(pos).get(listTags[2]));
			
			
			
			//loading image
			if(aa.get(pos).get(listTags[1])!=null && !aa.get(pos).get(listTags[1]).isEmpty()){				
				Picasso.with(c)
			    .load(aa.get(pos).get(listTags[1]))
			    .placeholder(R.mipmap.dummy_avatar)
			    .error(R.mipmap.dummy_avatar)
			    .into(imageViewAvatar);
			}
			
			//follow unfollow
			/*System.out.println("=============================================="+aa.get(pos).get(listTags[8]));
			if( aa.get(pos).get(listTags[8]).equals("YES") ){
				imageViewfollow.setImageResource(R.drawable.following);
			}else if( aa.get(pos).get(listTags[8]).equals("NO") ){
				imageViewfollow.setImageResource(R.drawable.follow);
			} */

			

		} else {

			txtAvatarName.setText(aa.get(pos).get(""));
			txtAvatarMetadata.setText(aa.get(pos).get(""));
			
		}

		/*
		 * name.setText(aa.get(pos).get("name"));
		 * dis.setText(aa.get(pos).get("distance"));
		 * ref.setText(aa.get(pos).get("reference"));
		 */
		
		//apply animation on current view
		 Animation animation = AnimationUtils.loadAnimation(c, (pos > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
		 v.startAnimation(animation);
		 lastPosition = pos;
		 return v;
	}
	
	// Progress dialog
	ProgressDialog pDialog;
			
	// Google Places
	FoodyResturent foodyResturent;
	
	String respons;
	
	private String followerid;
	private String userid;
	
	class PostFollowers  extends AsyncTask<String, String, String> {


		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(c);
			pDialog.setMessage(Html
					.fromHtml("<b>Loading ...</b>"));
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
			
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {
				
				SavedPrefernce prefernce=new SavedPrefernce((Activity)c);
				followerid=prefernce.getUserID();

				
				if( userid.equals(followerid) ){
					respons = "self";
				}else{
					respons = foodyResturent.updateFollow(userid, followerid);
				}
				

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			// updating UI from Background Thread
			((Activity)c).runOnUiThread(new Runnable() {
				public void run() {
					//
					pDialog.dismiss();
					
					
					
					if(respons.equals("success")){
						imageViewfollowing.setImageResource(R.mipmap.following);
					}else if(respons.equals("unfollow")){
						imageViewfollowing.setImageResource(R.mipmap.follow);
					}else if(respons.equals("self")){
						Toast.makeText(c, "You can't follow yourself", Toast.LENGTH_SHORT).show(); 
					}
					
					
				}

			});

		}

	}
	

	
	String response;

	public String revid;

	

	public String comment;

	public String rlike;
	class ReviewLike extends AsyncTask<String, String, String> {

		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		/*	pDialog = new ProgressDialog(c);
			pDialog.setMessage(Html.fromHtml("<b>Loading ...</b>"));
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();*/
			
		}

		protected String doInBackground(String... args) {
			
			foodyResturent = new FoodyResturent();

			try {

				SavedPrefernce prefernce=new SavedPrefernce((Activity)c);
				userid=prefernce.getUserID();
				
				System.out.println("================="+revid+ userid+ comment+ rlike);
				response = foodyResturent.insertReviewLike(revid, userid, comment, rlike);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

	
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			//pDialog.dismiss();
			// updating UI from Background Thread
			((Activity) c).runOnUiThread(new Runnable() {

				public void run() {
					
					if(response.equals("success")){
						//Toast.makeText(c, "Post Successful.", Toast.LENGTH_SHORT).show();
						likeCount++;
						txtLikesCount.setText(likeCount+" Like");
						
						hashMap.put(listTags[6], ""+likeCount);
						
					
						
					}else if(response.equals("unlike")){
						//Toast.makeText(c, "Post Successful.", Toast.LENGTH_SHORT).show();
						likeCount--;
						txtLikesCount.setText(likeCount+" Like");
						hashMap.put(listTags[6], ""+likeCount);
						
					}
					else{
						Toast.makeText(c, "Try again", Toast.LENGTH_SHORT).show();
					}
				}

			});

		}

	}		

}
