package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.foody.jsondata.FoodyResturent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mcc.letseat.R;
import com.mcc.libs.ActivityWithSliding;


/**
 * 
 * @author Arif
 * app main menu
 */
public class MenuListView extends ActivityWithSliding implements
		ActionBar.OnNavigationListener {

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Google Places
	FoodyResturent foodyResturent;

	// Places List
	ArrayList<com.foody.jsondata.Menu> menuList;


	// Button
	ImageView btnShowOnMap;

	// Progress dialog
	ProgressDialog pDialog;

	// Places Listview
	ListView lv;

	// ListItems data
	ArrayList<HashMap<String, String>> resturantListItems = new ArrayList<HashMap<String, String>>();

	// //admob
	AdView adView = null;

	ImageView searchAny;
	
	TextView restaurantName;

	String restaurentId;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.restaurant_menu);

		

		// Getting listview
		lv = (ListView) findViewById(R.id.list);
		
		restaurantName= (TextView)findViewById(R.id.restName);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				/*Restaurent resturent = (Restaurent) resturentList.get(position);
				//restaurentId= resturent.Id;
				
				Intent in = new Intent(getApplicationContext(),ResturentActivity.class);
				in.putExtra( "Id" , resturentList.get(position).Id); 

				startActivity(in);*/
				
				
			}
		});

		adView = (AdView) this.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder().addTestDevice(
				AdRequest.DEVICE_ID_EMULATOR).build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});
		
		//Toast.makeText(ResturentListView.this, "hello"+searchKeywords, Toast.LENGTH_LONG).show();
		
		restaurentId=getIntent().getStringExtra("restaurentId");
		restaurantName.setText(getIntent().getStringExtra("restaurentName"));
		
		new LoadPlaces().execute();
		//handleIntent(getIntent());

	}


	

	@Override
	public void onPause() {
		super.onPause();

		if (adView != null)
			adView.pause();

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		if (adView != null)
			adView.destroy();
		super.onDestroy();
	}

	class LoadPlaces extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(MenuListView.this);
			pDialog.setMessage(Html
					.fromHtml("<b>Loading ...</b>"));
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {

				String types = "ResId=" + restaurentId;
				

				menuList = foodyResturent.getMenu(types);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status

					if (menuList == null || menuList.size() <= 0) {

						
						alert.showAlertDialog(MenuListView.this,
								getString(R.string.app_name), "Nothing found.", false);
						//showPopUp();
						
						 
					} else if (menuList != null) {
						// Check for all possible status

						// Successfully got places details
						if (menuList != null) {
							// loop through each place
							for (com.foody.jsondata.Menu p : menuList) {
								
								HashMap<String, String> map = new HashMap<String, String>();
								map.put("MenuName", p.MenuName);								
								map.put("Price", "Tk. "+p.Price);
								map.put("FoodCat", p.FoodCat);

								System.out
										.println(">>>>>>>>>>test>>>>>>>>>>>>>>>>>> "
												+ p.MenuName);

								// adding HashMap to ArrayList
								resturantListItems.add(map);
							}
							// list adapter
							MyListAdapter adapter = new MyListAdapter(
									MenuListView.this,
									resturantListItems,
									new String[] {  "MenuName", "Price", "FoodCat"});
							

							// Adding data into listview
							lv.setAdapter(adapter);
						}

						else if (menuList == null) {
							// Zero results found
							alert.showAlertDialog(MenuListView.this,
									getString(R.string.app_name),
									" No Restaurant found. Want to add?", false);
						}
					}
				}

			});

		}

	}
	
	
	
	

	@Override
	public boolean onNavigationItemSelected(int arg0, long arg1) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	private void showPopUp() {

		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
		// helpBuilder.setTitle("Pop Up");
		helpBuilder.setMessage("Nothing Found. Want to request this suggestion?");
		helpBuilder.setPositiveButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {

					}
				});

		helpBuilder.setNegativeButton("Yes",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						Intent in = new Intent(
								getApplicationContext(),
								SuggetionRequest.class);
						in.putExtra("keyword", restaurentId);

						startActivity(in);
					}
				});

		// Remember, create doesn't show the dialog
		AlertDialog helpDialog = helpBuilder.create();
		helpDialog.show();

	}

	
}
