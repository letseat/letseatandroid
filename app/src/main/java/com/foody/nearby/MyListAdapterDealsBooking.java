package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.FoodyResturent;
import com.mcc.letseat.R;
import com.mcc.user.SavedPrefernce;
import com.mcc.user.profile.UserBookedDealsList;

/**
 * Custom data adapter for 2 item ListView
 */
public class MyListAdapterDealsBooking extends BaseAdapter{
	
	ArrayList<HashMap<String, String>> aa;
	Context c;
	String listTags[];
	String resId; 
	String userId;
	
	
	public MyListAdapterDealsBooking(Context c, ArrayList<HashMap<String, String>> aa, String listTags[], String resId){
		this.aa = aa;
		this.c = c;
		this.listTags=listTags;
		this.resId = resId; 
		
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return aa.size();
	}

	@Override
	public String getItem(int arg0) {
		// TODO Auto-generated method stub
		return aa.get(arg0).get("reference");
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	private int lastPosition = -1;
	
	@Override
	public View getView(int pos, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		
		View v;
		v = arg1;
		LayoutInflater li = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		v = li.inflate(R.layout.list_item_deals_booking, null);
		
		//LinearLayout root = (LinearLayout)v.findViewById(R.id.ltnbTitle);
		
		TextView first= (TextView)v.findViewById(R.id.first);
		TextView second = (TextView)v.findViewById(R.id.second);
		Button btn_book_deal = (Button)v.findViewById(R.id.btn_book_deal);
		//show or hide "deal book" button
		final int positionFinal=pos;
		
		btn_book_deal.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					//parameter ResId UserId DealId
					
					String DealId =  aa.get(positionFinal).get(listTags[3]);
					SavedPrefernce prefernce =new SavedPrefernce(((Activity)c));
					userId=prefernce.getUserID();
					if (!prefernce.isUserExists()) {
						StaticObjects.callRegForAction(c);
					}else {
						Toast.makeText(c,"Adding to Wishlist...", Toast.LENGTH_LONG).show();
						new BookDeals().execute(resId, userId, DealId );
						
						/*Intent i = new Intent(Intent.ACTION_VIEW);
						i.setData(Uri.parse(aa.get(positionFinal).get("BookUrl")));
						c.startActivity(i);*/
					}
					
				}
			});
		//}
		
		
		/*if(pos%2==0)
		{
			root.setBackgroundColor(Color.parseColor("#FCFCFC"));
		}
		else
		{
			root.setBackgroundColor(Color.parseColor("#FFFFFF"));
		}*/
		
		//String ww =aa.get(pos);
		if(listTags!=null){
			
			first.setText(aa.get(pos).get(listTags[0]));
			first.setTextColor(Color.parseColor("#BE1010"));
			
			second.setText(aa.get(pos).get(listTags[1]));
			second.setTextColor(Color.parseColor("#222222"));
			if(aa.get(pos).get("IsLetsEat").equals("1")){
				btn_book_deal.setVisibility(View.VISIBLE);
			}else{
				btn_book_deal.setVisibility(View.GONE);
			}
			
			
			
			
		}else{
			first.setText(aa.get(pos).get(""));
			second.setText(aa.get(pos).get(""));
			
		}
		
		/*name.setText(aa.get(pos).get("name"));
		dis.setText(aa.get(pos).get("distance"));
		ref.setText(aa.get(pos).get("reference"));
*/		
		
		
		//apply animation on current view
		 Animation animation = AnimationUtils.loadAnimation(c, (pos > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
		 v.startAnimation(animation);
		 lastPosition = pos;
		
		return v;
	}
	
	
	
	//parameter ResId UserId DealId
	class BookDeals extends AsyncTask<String, String, String>{
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			FoodyResturent foodyResturent =new FoodyResturent();
			
			
			return foodyResturent.addBooking(params[0], params[1], params[2]);
		}
		
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			/*ErrorBlank
			DuplicateBooking
			Success
			failed*/
			
			if(result.equals("ErrorBlank")){
				Toast.makeText(c, "Try Again", Toast.LENGTH_LONG).show();
				
			}else if(result.equals("DuplicateBooking")){
				//Toast.makeText(c, "This Deal is already added to your Wishlist", Toast.LENGTH_LONG).show();
				showPopUp( "This Deal is already added to your Wishlist");
				
			}else if(result.equals("Success")){
				//Toast.makeText(c, result, Toast.LENGTH_LONG).show();
				showPopUp("This Deal is added to your Wishlist");
				
			}else if(result.equals("failed")){
				Toast.makeText(c, "Try Again", Toast.LENGTH_LONG).show();
			}else{
				Toast.makeText(c, "Try Again", Toast.LENGTH_LONG).show();
			}
			
		}
		
	}
	
	AlertDialog helpDialog ;
	private void showPopUp(String msg) {
		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(c);
		// helpBuilder.setTitle("Pop Up");
		helpBuilder.setMessage(msg);
		helpBuilder.setPositiveButton("See Wishlist",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						Intent i = new Intent(c, UserBookedDealsList.class);			
						((Activity)c).startActivity(i);
					}
				});

		helpBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						
						dialog.cancel();
						
					}
				});

		// Remember, create doesn't show the dialog
		 helpDialog = helpBuilder.create();
		
		
		    //show dialog
			helpDialog.show(); 
		

	}

}
