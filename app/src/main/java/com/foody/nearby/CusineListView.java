package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.foody.jsondata.Cusine;
import com.foody.jsondata.FoodyResturent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mcc.letseat.HomeT;
import com.mcc.letseat.R;
import com.mcc.letseat.MyProgressDialog;
import com.mcc.libs.ActivityWithSliding;
import com.mcc.libs.AnimationTween;
import com.mcc.user.SavedPrefernce;

/**
 * 
 * @author Arif
 *List of Cuisine in selected location 
 */
public class CusineListView extends ActivityWithSliding implements
		ActionBar.OnNavigationListener {

	

	// Google Places
	FoodyResturent foodyResturent;

	// Places List
	ArrayList<Cusine> cusineList;

	// Button
	ImageView btnShowOnMap;

	

	// Places Listview
	ListView lv;

	// ListItems data
	ArrayList<HashMap<String, String>> cusineListItems = new ArrayList<HashMap<String, String>>();

	// //admob
	AdView adView = null;

	ImageView searchAny;

	String searchKeywords;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_category);

		

		// Getting listview
		lv = (ListView) findViewById(R.id.list_cat);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				Cusine  cusine = (Cusine) cusineList.get(position);
				Intent in = new Intent(getApplicationContext(),CusineMenuListView.class);
				in.putExtra("id", cusine.Id);
				in.putExtra("cuisineName", cusine.cusinename);	
				startActivity(in);
			}
		});

		adView = (AdView) this.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder().build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});
		
		new LoadPlaces().execute();

		//handleIntent(getIntent());

	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
	
		case R.id.action_search:
			// search action
			RelativeLayout relativeLayout=(RelativeLayout)findViewById(R.id.cat_root_layout);		
			addSearchLayout(relativeLayout);			
			return true;
		
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	
	

	@Override
	protected void onNewIntent(Intent intent) {
		setIntent(intent);
		//handleIntent(intent);
	}

	

	@Override
	public void onPause() {
		super.onPause();

		if (adView != null)
			adView.pause();

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		if (adView != null)
			adView.destroy();
		super.onDestroy();
	}

	class LoadPlaces extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			findViewById(R.id.cat_custom_progress).setVisibility(View.VISIBLE);
			
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {
				cusineList = foodyResturent.getCusine(FoodyResturent.TYPE_LOAD_LOC, new SavedPrefernce(CusineListView.this).getLocationId(), CusineListView.this);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			//pDialog.dismiss();
			//findViewById(R.id.cat_custom_progress).setVisibility(View.GONE);
			AnimationTween.animateView(findViewById(R.id.cat_custom_progress), CusineListView.this);
			AnimationTween.animateViewPosition(lv, CusineListView.this);
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status
					

					if (cusineList == null || cusineList.size() <= 0) {

						//show message
						findViewById(R.id.cat_custom_progress).setVisibility(View.VISIBLE);						
						View v=findViewById(R.id.cat_custom_progress);
						TextView textPreparing=(TextView) v.findViewById(R.id.textPreparing);
						textPreparing.setText("Nothing found.");
						v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);						
						AnimationTween.animateViewPosition(v, CusineListView.this);
						 
					} else if (cusineList != null) {
						// Check for all possible status

						// Successfully got places details
						if (cusineList != null) {
							// loop through each place
							for (Cusine p : cusineList) {

								HashMap<String, String> map = new HashMap<String, String>();
								map.put("Id", p.Id);
								// Place name
								map.put("cusinename", p.cusinename);
							

								System.out.println(">>>>>>>>>>test>>>>>>>>>>>>>>>>>> "+ p.cusinename);

								// adding HashMap to ArrayList
								cusineListItems.add(map);
							}
							// list adapter
							ListAdapter adapter = new SimpleAdapter(
									CusineListView.this,
									cusineListItems, R.layout.list_item_single,
									new String[] { "cusinename" },
									new int[] {  R.id.first });
							
							MyListAdapterSingle adapter2=new MyListAdapterSingle(CusineListView.this, cusineListItems,  "cusinename" );
							
							// Adding data into listview
							lv.setAdapter(adapter2);
						}

						else if (cusineList == null) {
							
							//show message
							findViewById(R.id.cat_custom_progress).setVisibility(View.VISIBLE);						
							View v=findViewById(R.id.cat_custom_progress);
							TextView textPreparing=(TextView) v.findViewById(R.id.textPreparing);
							textPreparing.setText("Nothing found.");
							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
							AnimationTween.animateViewPosition(v, CusineListView.this);
						}
					}
				}

			});

		}

	}
	
	

	@Override
	public boolean onNavigationItemSelected(int arg0, long arg1) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	private void showPopUp() {

		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
		// helpBuilder.setTitle("Pop Up");
		helpBuilder.setMessage("Nothing Found. Want to request this suggestion?");
		helpBuilder.setPositiveButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {

					}
				});

		helpBuilder.setNegativeButton("Yes",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						Intent in = new Intent(
								getApplicationContext(),
								SuggetionRequest.class);
						in.putExtra("keyword", searchKeywords);

						startActivity(in);
					}
				});

		// Remember, create doesn't show the dialog
		AlertDialog helpDialog = helpBuilder.create();
		helpDialog.show();

	}

}
