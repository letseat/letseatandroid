package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils.TruncateAt;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.SearchResturent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.mcc.letseat.CapturePhotoActivity;
import com.mcc.letseat.R;
import com.mcc.letseat.Adapter.SearchAdapter;
import com.mcc.letseat.tab.TabMainActivity;
import com.mcc.libs.ActivityWithSliding;
import com.mcc.libs.AnimationTween;
import com.mcc.libs.CF;
import com.mcc.user.SavedPrefernce;

/**
 * Search result ListView use in Search and advance Search
 */
public class SearchResturentListView extends ActivityWithSliding implements
		ActionBar.OnNavigationListener {

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Google Places
	FoodyResturent foodyResturent;

	// Places List
	ArrayList<SearchResturent> resturentList;


	// Button
	ImageView btnShowOnMap;

	

	// Places Listview
	ListView lv;

	// ListItems data
	ArrayList<HashMap<String, String>> placesListItems = new ArrayList<HashMap<String, String>>();

	// //admob
	AdView adView = null;

	ImageView searchAny;

	String searchKeywords;
	
	TextView txtTitleNearBy;
	
	View ll_search_hint ;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.slide_up_in,R.anim.stay);
		
		setContentView(R.layout.activity_search_restaurant);

		//
		txtTitleNearBy= (TextView) findViewById(R.id.txtTitleNearBy);
		//show search msg
		findViewById(R.id.txtSearchMsg).setVisibility(View.VISIBLE);
		
		
		// Getting listview
		lv = (ListView) findViewById(R.id.list);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				SearchResturent resturent = (SearchResturent) resturentList.get(position);
				String nev_type=getIntent().getStringExtra(StaticObjects.NEV_TYPE);
				
				if(nev_type!=null && nev_type.equals(StaticObjects.NEV_TYPE_CHOOSE_RES_PHOTO_UP)){
					Intent in=new Intent();
					in.putExtra( "Id" , resturent.Id);	
					in.putExtra( "RestName" , resturent.Name);	
					setResult(RESULT_OK, in);
					CapturePhotoActivity.resid=resturent.Id;
					CapturePhotoActivity.resname=resturent.Name;
					
					finish();
				}else{
					
					Intent in = new Intent(getApplicationContext(),
							TabMainActivity.class);
					in.putExtra("name", resturent.Name);
					in.putExtra("address", resturent.Address);
					in.putExtra("imageLink", resturent.IMG);
					in.putExtra("Id",resturent.Id);

					startActivity(in);
				}

				
			}
		});
		
		
		ll_search_hint = findViewById(R.id.ll_search_hint);
		//l_sort_filter_rest.setVisibility(View.INVISIBLE);
		
		//set up sort and filter
				// filter list
				ArrayList<String> filterBylist = new ArrayList<String>();
				filterBylist.add("Clear Filter");
				filterBylist.add("Open Now");
				filterBylist.add("Deals Available");
				
				//sort
				ArrayList<String> sortBylist = new ArrayList<String>();
				sortBylist.add("Name (A-Z)");
				sortBylist.add("Name (Z-A)");
				sortBylist.add("Lowest Cost First");
				sortBylist.add("Highest Cost First");
				sortBylist.add("Highest Capacity First");
				sortBylist.add("Lowest Capacity First");
				sortBylist.add("Highest Rating First");
				
				Typeface tfLight= StaticObjects.gettfLight(this);
				setFilterSortSpinner(ll_search_hint, tfLight, filterBylist, sortBylist);
		

		adView = (AdView) this.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder().build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});

		handleIntent(getIntent());

	}
	
	
	
	private void setFilterSortSpinner(View spinnerHolder, final Typeface tfLight, ArrayList<String> filterBylist, ArrayList<String> sortBylist){
		Spinner spinnerFilter = (Spinner) spinnerHolder.findViewById(R.id.spinner_offer);		
		ArrayAdapter<String> adapter_offer = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, filterBylist) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {

				View v = super.getView(position, convertView, parent);
				((TextView) v).setTypeface(tfLight);
				((TextView) v).setEllipsize(TruncateAt.END);

				return v;
			}

			@Override
			public View getDropDownView(int position, View convertView,
					ViewGroup parent) {

				View v = super.getDropDownView(position, convertView, parent);
				((TextView) v).setTypeface(tfLight);
				return v;
			}

		};
		// Specify the layout to use when the list of choices appears
		adapter_offer.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinnerFilter.setAdapter(adapter_offer);
		spinnerFilter.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
				if(pos ==0 && parent != null){
					if( parent.getChildAt(0) instanceof TextView)
					((TextView) parent.getChildAt(0)).setText("Not Filtered");
					
				}
				
				lv.setTextFilterEnabled(true);
				SearchAdapter adapter = null;
				
				/*if(isFoooterAdded)
					 adapter=((SearchAdapter)((HeaderViewListAdapter)lv.getAdapter()).getWrappedAdapter());
				else*/
					adapter =(SearchAdapter)lv.getAdapter();
				
				if(adapter ==null){return;}	
				adapter.getFilter().filter(""+pos);
				lv.setSelectionAfterHeaderView();

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});
		
		
		// offer_provider_list.add("Others");

		Spinner spinnerSort = (Spinner) spinnerHolder.findViewById(R.id.spinner_sort);		
		ArrayAdapter<String> adapter_sort = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, sortBylist) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {

				View v = super.getView(position, convertView, parent);
				((TextView) v).setTypeface(tfLight);
				((TextView) v).setEllipsize(TruncateAt.END);

				return v;
			}

			@Override
			public View getDropDownView(int position, View convertView,
					ViewGroup parent) {

				View v = super.getDropDownView(position, convertView, parent);
				((TextView) v).setTypeface(tfLight);
				return v;
			}

		};
		// Specify the layout to use when the list of choices appears
		adapter_sort.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinnerSort.setAdapter(adapter_sort);
		spinnerSort.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,int pos, long id) {			
				//CuisineRestAdapter adapter= (CuisineRestAdapter)lv.getAdapter();
				
				SearchAdapter adapter = null;
				
				/*if(isFoooterAdded)
					 adapter=((SearchAdapter)((HeaderViewListAdapter)lv.getAdapter()).getWrappedAdapter());
				else*/
					adapter =(SearchAdapter)lv.getAdapter();
				
				if(adapter ==null){return;}								
				adapter.setSortBY(pos);		
				lv.setSelectionAfterHeaderView();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
	
		case R.id.action_search:
			// search action
			RelativeLayout relativeLayout=(RelativeLayout)findViewById(R.id.rest_list_root_layout);		
			addSearchLayout(relativeLayout);			
			return true;
		
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu ) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate( R.menu.activity_main, menu );
		menu.findItem(R.id.action_search).setVisible(false);
		
	    return true;
	}

	private void handleIntent(Intent intent) {
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			searchKeywords = null;
			searchKeywords = intent.getStringExtra(SearchManager.QUERY);
			new LoadPlaces().execute();
			//txtTitleNearBy.setText("Showing results for: "+ searchKeywords);

		} else if (intent.getStringExtra("searchKeyWord_home") != null) {
			searchKeywords = null;
			searchKeywords = intent.getStringExtra("searchKeyWord_home");
			new LoadPlaces().execute();
			//txtTitleNearBy.setText("Showing results for: ");
		}
		
		//
		// Spannable string allows us to edit the formatting of the text.
       /* SpannableString titleText = new SpannableString(searchKeywords);
        titleText.setSpan(new ForegroundColorSpan(Color.RED), 0, titleText.length(), 0);
        txtTitleNearBy.setText(txtTitleNearBy.getText()+""+titleText);*/
        
       
        Spannable WordToSpan = new SpannableString("Search result for ");        
        WordToSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#333333")), 0, WordToSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtTitleNearBy.setText(WordToSpan);
        
        Spannable WordToSpan1 = new SpannableString("'"+searchKeywords+"'");        
        WordToSpan1.setSpan(new ForegroundColorSpan(Color.parseColor("#BE1010")), 0, WordToSpan1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtTitleNearBy.append(WordToSpan1);

	}

	@Override
	protected void onNewIntent(Intent intent) {
		setIntent(intent);
		handleIntent(intent);
	}

	/*// action bar menu and event
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_main, menu);
		// Associate searchable configuration with the SearchView
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) menu.findItem(R.id.action_search)
				.getActionView();
		searchView.setSearchableInfo(searchManager
				.getSearchableInfo(getComponentName()));

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
		case android.R.id.home:
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
		case R.id.action_search:
			// search action
			return true;
		case R.id.action_nearby:
			Intent i=new Intent(SearchResturentListView.this,NearByplaceListView.class);			
			startActivity(i);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}*/

	@Override
	public void onPause() {
		super.onPause();
		overridePendingTransition(R.anim.stay,R.anim.slide_down_out);
		if (adView != null)
			adView.pause();
		
		//overridePendingTransition(0,R.anim.view_close_translate);

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		
		if (adView != null)
			{adView.destroy();}
		
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}
	
	@Override
	protected void onStart() {	
		super.onStart();
		
		//analytics stop tracking
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}
	
	String types;
	class LoadPlaces extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {

				 types = "Search=" + searchKeywords+"&LocId="+new SavedPrefernce(SearchResturentListView.this).getLocationId();

				resturentList = foodyResturent.search(types);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			//findViewById(R.id.main_custom_progress).setVisibility(View.GONE);
			AnimationTween.animateView(findViewById(R.id.main_custom_progress), SearchResturentListView.this);
			AnimationTween.animateViewPosition(lv, SearchResturentListView.this);
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status

					if (resturentList == null || resturentList.size() <= 0) {

						//show popup for sugest request
						showPopUpAdvSearch();
						
						 
					} else if (resturentList != null) {
						// Check for all possible status

						// Successfully got places details
						if (resturentList != null) {
							ArrayList<SearchResturent> searchRestList= new ArrayList<SearchResturent>(); 
							placesListItems = new ArrayList<HashMap<String, String>>();
							// loop through each place
							for (SearchResturent p : resturentList) {
								
								//set open close indicator
								p.setOpenOrClose(CF.openOrClose(p.Open, p.Close));

								HashMap<String, String> map = new HashMap<String, String>();
								map.put("Id", p.Id);
								// Place name
								map.put("Name", p.Name);
								map.put("Address", p.Address);

								System.out
										.println(">>>>>>>>>>test>>>>>>>>>>>>>>>>>> "
												+ p.Address);

								// adding HashMap to ArrayList
								placesListItems.add(map);
								
								//search suggestion
								searchRestList.add(new SearchResturent(p.Id, p.Name, null, p.Address, null, null, null));
							}
							
							//search suggestion
							addDataToDB(searchRestList);
							
							// list adapter
							//MyListAdapter adapter = new MyListAdapter(SearchResturentListView.this,placesListItems,new String[] {  "Name", "Address","Id" });
							SearchAdapter adapter = new SearchAdapter(SearchResturentListView.this, resturentList);

							// Adding data into listview
							lv.setAdapter(adapter);
						}

						else if (resturentList == null) {
							//showPopUpAdvSearch();
							showPopUp();
						}
					}
				}

			});

		}

	}
	
	

	@Override
	public boolean onNavigationItemSelected(int arg0, long arg1) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	private void showPopUp() {

		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
		// helpBuilder.setTitle("Pop Up");
		helpBuilder.setMessage("Nothing Found. Want to request this suggestion?");
		helpBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						finish();
						Intent in = new Intent(
								getApplicationContext(),
								SuggetionRequest.class);
						in.putExtra("keyword", searchKeywords);

						startActivity(in);
					}
				});

		helpBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
						
					}
				});

		// Remember, create doesn't show the dialog
		AlertDialog helpDialog = helpBuilder.create();
		
		if(!(this.isFinishing()))
		{
		    //show dialog
			helpDialog.show(); 
		}

	}
	
	private void showPopUpAdvSearch() {

		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
		// helpBuilder.setTitle("Pop Up");
		helpBuilder.setMessage("No matches found. Try advanced search.");
		helpBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						finish();
						Intent in = new Intent(getApplicationContext(),SearchAdvResturentListView.class);
						
						in.putExtra(StaticObjects.NEV_TYPE, getIntent().getStringExtra(StaticObjects.NEV_TYPE));
						in.putExtra("searchKeyWord_home", searchKeywords);

						startActivity(in);
					}
				});

		helpBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						showPopUp();
						//finish();
						
					}
				});

		// Remember, create doesn't show the dialog
		AlertDialog helpDialog = helpBuilder.create();
		if(!(this.isFinishing()))
		{
		    //show dialog
			helpDialog.show();
		}
		

	}

}