package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;

import android.animation.TimeInterpolator;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.District;
import com.foody.jsondata.FoodyResturent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.mcc.letseat.HomeT;
import com.mcc.letseat.R;
import com.mcc.letseat.MyProgressDialog;
import com.mcc.libs.ActivityWithSliding;
import com.mcc.libs.AnimationTween;
import com.mcc.user.SavedPrefernce;

/**
 * 
 * @author Arif
 * List  of Districts or Cities e.i: Dhaka
 */
public class DistrictListView extends ActivityWithSliding implements
		ActionBar.OnNavigationListener {

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Google Places
	FoodyResturent foodyResturent;

	// Places List
	ArrayList<District> districtList;

	// Places Listview
	ListView lv;

	// ListItems data
	ArrayList<HashMap<String, String>> districtListItems = new ArrayList<HashMap<String, String>>();

	

	ImageView searchAny;

	String searchKeywords;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choose_city);
				
		TextView txtChooseLoc=(TextView)findViewById(R.id.txtChooseLoc);
		txtChooseLoc.setText("Choose City");
		Typeface tf = Typeface.createFromAsset(getAssets(), StaticObjects.fontPath_SEGOEUI);	
		txtChooseLoc.setTypeface(tf);
		
		// getWindow().getDecorView().findViewById(android.R.id.content);
		//getWindow().getDecorView().getRootView();
		
		


        
		
		// Getting listview
		lv = (ListView) findViewById(R.id.list_cat);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				District district = (District) districtList.get(position);
				HomeT.District=district.DisName;
				
				//analytics
				analyticsSendScreenView("Choose City "+district.DisName);

				Intent in = new Intent(getApplicationContext(),LocatonListView.class);
				in.putExtra("DisId", district.DisId);
				in.putExtra("DisName", district.DisName);
				String nev_type=getIntent().getStringExtra(StaticObjects.NEV_TYPE);
				
				in.putExtra(StaticObjects.NEV_TYPE, nev_type);
				startActivity(in);
				
				finish();
				
				
				
				/*if(nev_type!=null && nev_type.equals(StaticObjects.NEV_TYPE_RESULT)){
					
					in.putExtra(StaticObjects.NEV_TYPE, StaticObjects.NEV_TYPE_RESULT);
					
				}else if(nev_type!=null && nev_type.equals(StaticObjects.NEV_TYPE_CHOOSE_LOCTION)){
					in.putExtra(StaticObjects.NEV_TYPE, StaticObjects.NEV_TYPE_CHOOSE_LOCTION);
					
					
					
				}else if(nev_type!=null && nev_type.equals(StaticObjects.NEV_TYPE_ADV_SEARCH)){
					in.putExtra(StaticObjects.NEV_TYPE, StaticObjects.NEV_TYPE_ADV_SEARCH);
					
				}*/
				
				
										
										
				
								
				
				

			}
		});

		
		
		new LoadPlaces().execute();

		// handleIntent(getIntent());

	}

	private void handleIntent(Intent intent) {
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			searchKeywords = null;
			searchKeywords = intent.getStringExtra(SearchManager.QUERY);
			new LoadPlaces().execute();

		} else if (intent.getStringExtra("searchKeyWord_home") != null) {
			searchKeywords = null;
			searchKeywords = intent.getStringExtra("searchKeyWord_home");
			new LoadPlaces().execute();
		}

	}

	@Override
	protected void onNewIntent(Intent intent) {
		setIntent(intent);
		// handleIntent(intent);
	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
	
		case R.id.action_search:
			// search action
			RelativeLayout relativeLayout=(RelativeLayout)findViewById(R.id.cat_root_layout);		
			addSearchLayout(relativeLayout);			
			return true;
		
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	
	@Override
	protected void onStart() {	
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}
	
	@Override
	protected void onDestroy() {	
		super.onDestroy();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}
	

	class LoadPlaces extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			findViewById(R.id.choose_city_custom_progress).setVisibility(View.VISIBLE);
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {
				districtList = foodyResturent.getDistricts(DistrictListView.this);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			// pDialog.dismiss();
			//findViewById(R.id.choose_city_custom_progress).setVisibility(View.GONE);
			AnimationTween.animateView(findViewById(R.id.choose_city_custom_progress), DistrictListView.this);
			AnimationTween.animateViewPosition(lv, DistrictListView.this);
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status

					if (districtList == null || districtList.size() <= 0) {

						findViewById(R.id.choose_city_custom_progress).setVisibility(View.VISIBLE);
						View v = findViewById(R.id.choose_city_custom_progress);
						TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
						textPreparing.setText("Nothing found.");
						v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
						AnimationTween.animateViewPosition(findViewById(R.id.choose_city_custom_progress), DistrictListView.this);

					} else if (districtList != null) {
						// Check for all possible status

						// Successfully got places details
						if (districtList != null) {
							// loop through each place
							for (District p : districtList) {

								HashMap<String, String> map = new HashMap<String, String>();
								map.put("DisId", p.DisId);
								// Place name
								map.put("DivId", p.DivId);
								map.put("DisName", p.DisName);

								System.out
										.println(">>>>>>>>>>test>>>>>>>>>>>>>>>>>> "
												+ p.DisName);

								// adding HashMap to ArrayList
								districtListItems.add(map);
							}
							// list adapter
							MyListAdapterSingle adapter = new MyListAdapterSingle(
									DistrictListView.this, districtListItems,
									"DisName");

							// Adding data into listview
							lv.setAdapter(adapter);
							
							
						}

						else if (districtList == null) {
							findViewById(R.id.choose_city_custom_progress)
									.setVisibility(View.VISIBLE);
							View v = findViewById(R.id.choose_city_custom_progress);
							TextView textPreparing = (TextView) v
									.findViewById(R.id.textPreparing);
							textPreparing.setText("Nothing found.");
							v.findViewById(R.id.progressBarPreparing)
									.setVisibility(View.GONE);
							AnimationTween.animateViewPosition(findViewById(R.id.choose_city_custom_progress), DistrictListView.this);
						}
					}
				}

			});

		}

	}
	
	
	

	@Override
	public boolean onNavigationItemSelected(int arg0, long arg1) {
		// TODO Auto-generated method stub
		return false;
	}

}
