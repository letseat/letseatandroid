package com.foody.nearby;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.foody.AppData.StaticObjects;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mcc.letseat.R;
import com.mcc.libs.ActivityWithSliding;
import com.mcc.user.SavedPrefernce;

/**
 * Restaurant suggestion or request to add a restaurant
 */
public class SuggetionRequest extends ActivityWithSliding {
	
	AdView adView;
	
	 String email;
	 String keyword;
	 String type;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sugestion_request);
		
		//action bar
		getActionBar().hide();

		final EditText txtReqEmail = (EditText) findViewById(R.id.txtReqEmail);
		txtReqEmail.setVisibility(View.GONE);
		final EditText txtReqkeyword = (EditText) findViewById(R.id.txtReqKeyword);
		final EditText etAditionalInfo = (EditText) findViewById(R.id.etAditionalInfo);
		
		final Spinner txtTypeOfSuggetion = (Spinner) findViewById(R.id.suggestionType);

		txtReqkeyword.setText(getIntent().getStringExtra("keyword"));

		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.suggestion_type,
				R.layout.spinner_item);

		// Specify the layout to use when the list of choices appears
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		txtTypeOfSuggetion.setAdapter(adapter);
		txtTypeOfSuggetion
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int pos, long id) {
						// TODO Auto-generated method stub
						parent.getItemAtPosition(pos);

					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub

					}
				});

		Button cmdSubmit = (Button) findViewById(R.id.cmdSongRequest);

		cmdSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				 email = txtReqEmail.getText().toString();
				keyword = txtReqkeyword.getText().toString();
				type = txtTypeOfSuggetion.getSelectedItem()
						.toString();

				/*if (email.equals("")) {
					Toast.makeText(SuggetionRequest.this, "Email Required",
							Toast.LENGTH_LONG).show();
					return;
				}*/
				
				if (keyword.equals("")) {
					Toast.makeText(SuggetionRequest.this, "Keyword Required",
							Toast.LENGTH_LONG).show();
					return;
				}
				if (type.equals("")) {
					Toast.makeText(SuggetionRequest.this, "Type Required",
							Toast.LENGTH_LONG).show();
					return;
				}

				//Toast.makeText(getApplicationContext(), type, Toast.LENGTH_LONG).show();
				SavedPrefernce savedPrefernce=new SavedPrefernce(SuggetionRequest.this);
				if(savedPrefernce.isUserExists()){
					email=savedPrefernce.getUserEmail().isEmpty()?savedPrefernce.getUserID():savedPrefernce.getUserEmail();
					
				}else{
					StaticObjects.callRegForAction(SuggetionRequest.this);
					return;
				}
				
				String addInfo=etAditionalInfo.getText().toString();
				if(!addInfo.isEmpty()){
					showPopUp(email, "keyword:"+keyword+"/ addInfo:"+addInfo, type);
				}else{
					showPopUp(email, "keyword:"+keyword, type);
				}
				
				

			}

		});

		Button cmdCancel = (Button) findViewById(R.id.cancelPost);

		cmdCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
				overridePendingTransition(android.R.anim.fade_in,
						android.R.anim.fade_out);
			}

		});
		
		//adMob
		adView = (AdView) this.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder().addTestDevice(
				AdRequest.DEVICE_ID_EMULATOR).build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});

	}

	private void showPopUp(final String email, final String keyword,
			final String type) {

		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
		// helpBuilder.setTitle("Pop Up");
		helpBuilder.setMessage("Confirm your suggestion");
		helpBuilder.setPositiveButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {

					}
				});

		helpBuilder.setNegativeButton("Yes",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						// last parameter false send song request, true send
						// feedback
						PostFeedBackTask pft = new PostFeedBackTask(
								SuggetionRequest.this, email, keyword, type);
						pft.execute();
					}
				});

		// Remember, create doesn't show the dialog
		AlertDialog helpDialog = helpBuilder.create();
		helpDialog.show();

	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
		case R.id.action_search:
			// search action
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	@Override
	public void onPause() {
		super.onPause();

		if (adView != null)
			adView.pause();

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		if (adView != null)
			adView.destroy();
		super.onDestroy();
	}


}
