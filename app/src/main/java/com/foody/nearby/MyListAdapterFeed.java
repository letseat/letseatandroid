package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.user.UserPubInfo;
import com.mcc.letseat.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Custom data adapter for User news feed
 */
public class MyListAdapterFeed extends BaseAdapter {

	ArrayList<HashMap<String, String>> aa;
	Context c;
	String listTags[];
	
	
	HashMap<String, String> hashMap;
	int selectedPosition;
	int likeCount;
	
	ImageView imageViewfollowing;
	TextView txtLikesCount;
	protected int lastimageViewfollowing; 
	

	View fav, photo, follow, review;
	UserPubInfo  userPubInfo;
	
	
	public MyListAdapterFeed(Context c, ArrayList<HashMap<String, String>> aa) {
		this.aa = aa;
		this.c = c;
		
	}

	public MyListAdapterFeed(Context c,
			ArrayList<HashMap<String, String>> aa, String listTags[], UserPubInfo  userPubInfo) {
		this.aa = aa;
		this.c = c;
		this.listTags = listTags;
		this.userPubInfo= userPubInfo;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return aa.size();
	}

	@Override
	public String getItem(int arg0) {
		// TODO Auto-generated method stub
		return aa.get(arg0).get("b");
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	private int lastPosition = -1;
	@Override
	public View getView(int pos, View v, ViewGroup arg2) {
		// TODO Auto-generated method stub

		String layout_type= aa.get(pos).get(listTags[0]);
		
		LayoutInflater li = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if(layout_type.equals(FoodyResturent.FEED_TYPE_FAV)){
			v = li.inflate(R.layout.feed_fav, null);
			v.setTag(layout_type);
			
			View fts=v.findViewById(R.id.feed_fav_top_strip);
			
			
			TextView fts_user_name=(TextView)fts.findViewById(R.id.fts_user_name);
			TextView fts_user_others_info=(TextView)fts.findViewById(R.id.fts_user_others_info);			
			ImageView fts_user_avater=(ImageView)fts.findViewById(R.id.fts_user_avater);
			
			
			fts_user_name.setText(userPubInfo.NAME);
			fts_user_others_info.setText(setTime(aa.get(pos).get(listTags[2])));
						
			//set typeface			
			fts_user_name.setTypeface( StaticObjects.gettfNormal(c));
			fts_user_others_info.setTypeface( StaticObjects.gettfNormal(c));
			
			
			if(userPubInfo.IMG!=null && !userPubInfo.IMG.isEmpty()){
				Picasso.with(c)
			    .load(userPubInfo.IMG)
			    .placeholder(R.mipmap.dummy_avatar)
			    .error(R.mipmap.dummy_avatar)
			    .into(fts_user_avater);
			}
			
			
			Log.e("FeedAdapter", userPubInfo.IMG);
			
			
			ImageView imageView=(ImageView) v.findViewById(R.id.imageView_fav_res);
			TextView textView_fav_res_name=(TextView) v.findViewById(R.id.textView_fav_res_name);
			TextView textView_fav_res_loc=(TextView) v.findViewById(R.id.textView_fav_res_loc);
			TextView textViewFav=(TextView)v.findViewById(R.id.textViewFav);
			textViewFav.setTypeface( StaticObjects.gettfNormal(c));
			
			if(aa.get(pos).get(listTags[4])!=null &&  !aa.get(pos).get(listTags[4]).isEmpty()){
				Picasso.with(c)
			    .load(aa.get(pos).get(listTags[4]))
			    .placeholder(R.mipmap.dummy_avatar)
			    .error(R.mipmap.dummy_avatar)
			    .into(imageView);
			}
			
			
			textView_fav_res_name.setText( aa.get(pos).get(listTags[1]) );
			textView_fav_res_name.setTypeface( StaticObjects.gettfNormal(c));
			
			textView_fav_res_loc.setText("");
			textView_fav_res_loc.setTypeface( StaticObjects.gettfNormal(c));
			
		}else if(layout_type.equals(FoodyResturent.FEED_TYPE_FOLLOW)){
			v = li.inflate(R.layout.feed_follow, null);
			
			View fts=v.findViewById(R.id.feed_follow_top_strip);
			
			
			TextView fts_user_name=(TextView)fts.findViewById(R.id.fts_user_name);
			TextView fts_user_others_info=(TextView)fts.findViewById(R.id.fts_user_others_info);			
			ImageView fts_user_avater=(ImageView)fts.findViewById(R.id.fts_user_avater);
			
			
			
			fts_user_name.setText(userPubInfo.NAME);	
			fts_user_name.setTypeface( StaticObjects.gettfNormal(c));
			
			fts_user_others_info.setText(setTime(aa.get(pos).get(listTags[2])));
			fts_user_others_info.setTypeface( StaticObjects.gettfNormal(c));
			
			if(userPubInfo.IMG!=null &&  !userPubInfo.IMG.isEmpty()){
					Picasso.with(c)
			    .load(userPubInfo.IMG)
			    .placeholder(R.mipmap.dummy_avatar)
			    .error(R.mipmap.dummy_avatar)
			    .into(fts_user_avater);
			}
			
			
			Log.e("FeedAdapter", userPubInfo.IMG);
			
			ImageView imageView=(ImageView) v.findViewById(R.id.imageViewFollowerAvatar);
			TextView textView_fav_res_name=(TextView) v.findViewById(R.id.txtFollowerName);
			TextView textView_fav_res_loc=(TextView) v.findViewById(R.id.txtFollowReviewInfo);
			TextView textViewFollow=(TextView)v.findViewById(R.id.textViewFollow);
			textViewFollow.setTypeface( StaticObjects.gettfNormal(c));		
			
			if(aa.get(pos).get(listTags[4])!=null && !aa.get(pos).get(listTags[4]).isEmpty()){
				Picasso.with(c)
			    .load(aa.get(pos).get(listTags[4]))
			    .placeholder(R.mipmap.dummy_avatar)
			    .error(R.mipmap.dummy_avatar)
			    .into(imageView);
			}
			
			
			textView_fav_res_name.setText( aa.get(pos).get(listTags[1]) );
			textView_fav_res_name.setTypeface( StaticObjects.gettfNormal(c));
			textView_fav_res_loc.setText("");
			textView_fav_res_loc.setTypeface( StaticObjects.gettfNormal(c));
			
		}else if(layout_type.equals(FoodyResturent.FEED_TYPE_PHOTO)){
			v = li.inflate(R.layout.feed_photo, null);
			
			View fts=v.findViewById(R.id.feed_photo_top_strip);
			
			TextView fts_user_name=(TextView)fts.findViewById(R.id.fts_user_name);
			TextView fts_user_others_info=(TextView)fts.findViewById(R.id.fts_user_others_info);			
			ImageView fts_user_avater=(ImageView)fts.findViewById(R.id.fts_user_avater);
			
			
			fts_user_name.setText(userPubInfo.NAME);
			fts_user_name.setTypeface( StaticObjects.gettfNormal(c));
			fts_user_others_info.setText(setTime(aa.get(pos).get(listTags[2])));
			fts_user_others_info.setTypeface( StaticObjects.gettfNormal(c));
			
			if(userPubInfo.IMG!=null && !userPubInfo.IMG.isEmpty()){
				Picasso.with(c)
			    .load(userPubInfo.IMG)
			    .placeholder(R.mipmap.dummy_avatar)
			    .error(R.mipmap.dummy_avatar)
			    .into(fts_user_avater);
			}
			
			
			Log.e("FeedAdapter", userPubInfo.IMG);
			
			ImageView imageView=(ImageView) v.findViewById(R.id.imageView_feed_photo);
			TextView textView_fav_res_name=(TextView) v.findViewById(R.id.textView_caption_feed_photo);
			final ProgressBar progressBar_feed_photo=(ProgressBar)v.findViewById(R.id.progressBar_feed_photo);
			
			if(aa.get(pos).get(listTags[4])!=null && !aa.get(pos).get(listTags[4]).isEmpty()){
				Picasso.with(c)
			    .load(aa.get(pos).get(listTags[4]))
			    .placeholder(R.drawable.lazy_ic)
			    .error(R.drawable.lazy_ic)
			    .into(imageView, new Callback(){

					@Override
					public void onError() {
						// TODO Auto-generated method stub
						if(progressBar_feed_photo!=null)
						progressBar_feed_photo.setVisibility(View.GONE);
					}

					@Override
					public void onSuccess() {
						// TODO Auto-generated method stub
						if(progressBar_feed_photo!=null)
						progressBar_feed_photo.setVisibility(View.GONE);
					}
			    	
			    });
			}
			
			
			textView_fav_res_name.setText("Post photo on " );
			textView_fav_res_name.setTypeface( StaticObjects.gettfNormal(c));
			
			Spannable WordToSpan1 = new SpannableString(aa.get(pos).get(listTags[1]));        
	       // WordToSpan1.setSpan(new ForegroundColorSpan(Color.parseColor("#BE1010")), 0, WordToSpan1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	        textView_fav_res_name.append(WordToSpan1);
			
			
		}else if(layout_type.equals(FoodyResturent.FEED_TYPE_REVIEW) || layout_type.equals(FoodyResturent.FEED_TYPE_RATING)){
			v = li.inflate(R.layout.feed_review, null);
			
			View fts=v.findViewById(R.id.feed_review_top_strip);
			
			TextView fts_user_name=(TextView)fts.findViewById(R.id.fts_user_name);
			TextView fts_user_others_info=(TextView)fts.findViewById(R.id.fts_user_others_info);			
			ImageView fts_user_avater=(ImageView)fts.findViewById(R.id.fts_user_avater);
			
			
			fts_user_name.setText(userPubInfo.NAME);
			fts_user_name.setTypeface( StaticObjects.gettfNormal(c));
			fts_user_others_info.setText(setTime(aa.get(pos).get(listTags[2])));
			fts_user_others_info.setTypeface( StaticObjects.gettfNormal(c));
			
			if(userPubInfo.IMG!=null && !userPubInfo.IMG.isEmpty()){
				Picasso.with(c)
			    .load(userPubInfo.IMG)
			    .placeholder(R.mipmap.dummy_avatar)
			    .error(R.mipmap.dummy_avatar)
			    .into(fts_user_avater);
			}
			
			
			Log.e("FeedAdapter", userPubInfo.IMG);
			
			TextView textView_feed_title=(TextView) v.findViewById(R.id.textView_feed_title);
			TextView textView_feed_review=(TextView) v.findViewById(R.id.textView_feed_review);
			TextView txtReviewRating=(TextView) v.findViewById(R.id.txtReviewRating);
			
			float rating=Float.parseFloat(aa.get(pos).get(listTags[4]))/StaticObjects.RATING_OFFSET;
			
			textView_feed_review.setText( aa.get(pos).get(listTags[1]) );
			textView_feed_review.setTypeface( StaticObjects.gettfNormal(c));
			txtReviewRating.setText( rating+"" );
			txtReviewRating.setTypeface( StaticObjects.gettfNormal(c));
			textView_feed_title.setText("Post review on ");
			textView_feed_title.setTypeface( StaticObjects.gettfNormal(c));
			
			if(layout_type.equals(FoodyResturent.FEED_TYPE_RATING)){
				textView_feed_title.setText("Rates ");
				textView_feed_title.setTypeface( StaticObjects.gettfNormal(c));
			}
			
			Spannable WordToSpan1 = new SpannableString(aa.get(pos).get(listTags[5]));        
	       // WordToSpan1.setSpan(new ForegroundColorSpan(Color.parseColor("#BE1010")), 0, WordToSpan1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	        textView_feed_title.append(WordToSpan1);
			
		}
		
		
		

		

			
		//TextView txtAvatarName = (TextView) v.findViewById(R.id.textViewFeed);
		
		
		
		
		
		//LinearLayout layoutBottom=(LinearLayout)v.findViewById(R.id.list_item_review_bottom_boader);

	/*	RelativeLayout root = (RelativeLayout) v.findViewById(R.id.feed_fav_main);

		if (pos % 2 == 0) {
			root.setBackgroundColor(Color.parseColor("#f3f3f4"));
			//layoutBottom.setBackgroundColor(Color.parseColor("#f3f3f4"));
		} else {
			root.setBackgroundColor(Color.parseColor("#FFFFFF"));
			//layoutBottom.setBackgroundColor(Color.parseColor("#FFFFFF"));
		}*/

		// String ww =aa.get(pos);
		if (listTags != null) {
			/*
			 * first.setText(aa.get(pos).get(listTags[0]));
			 * second.setText(aa.get(pos).get(listTags[1]));
			 * third.setText(aa.get(pos).get(listTags[2]));
			 * four.setText(aa.get(pos).get(listTags[3]));
			 */

			//txtAvatarName.setText(aa.get(pos).get(listTags[0]) +" "+aa.get(pos).get(listTags[1])+" "+aa.get(pos).get(listTags[2]));
			
			
			/*//loading image
			Picasso.with(c)
		    .load(aa.get(pos).get(listTags[1]))
		    .placeholder(R.drawable.dummy_avatar)
		    .error(R.drawable.dummy_avatar)
		    .into(imageViewAvatar);*/
			
		} else {

			//txtAvatarName.setText(aa.get(pos).get(""));
			
		}

		
		
		
		//apply animation on current view
		 Animation animation = AnimationUtils.loadAnimation(c, (pos > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
		 v.startAnimation(animation);
		 lastPosition = pos;
		 
		

		return v;
	}
	
	String setTime(String value){
		
		if(value.equals("0"))
			return "Today";
		else if(value.equals("1"))
			return "Yesterday";
		else 
			return value+" days ago";
	}
	
	
	
	
	

		

}
