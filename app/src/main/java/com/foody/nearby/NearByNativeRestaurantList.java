package com.foody.nearby;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.animation.Animator;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils.TruncateAt;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.foody.AppData.StaticObjects;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.LetsEatRequest;
import com.foody.jsondata.Restaurent;
import com.foody.jsondata.SearchResturent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.maps.model.LatLng;
import com.mcc.letseat.CapturePhotoActivity;
import com.mcc.letseat.R;
import com.mcc.letseat.Adapter.NearByRestaurantAdapter;
import com.mcc.letseat.Adapter.RestaurantAdapter;
import com.mcc.letseat.tab.TabMainActivity;
import com.mcc.libs.ActivityWithSliding;
import com.mcc.libs.AnimationTween;
import com.mcc.libs.CF;

/**
 * Custom ListView Nearby Restaurant
 */
public class NearByNativeRestaurantList extends ActivityWithSliding implements
		ActionBar.OnNavigationListener {

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Google Places
	FoodyResturent foodyResturent;

	// restaurant List
	ArrayList<Restaurent> resturentList;

	// GPS Location
	GPSTracker gps;
	
	LatLng myLocation=null;

	// Button
	ImageView btnShowOnMap;

	

	// Places Listview
	ListView lv;

	// ListItems data
	ArrayList<HashMap<String, String>> resturantListItems = new ArrayList<HashMap<String, String>>();

	// //admob
	AdView adView = null;

	ImageView searchAny;

	String searchKeywords;
	
	//post value
	String LocId;
	int currentPage=0;
	int totalPage=1;
	
	TextView txtTitleNearBy;
	
	// Radius in meters - increase this value if you don't find any places
		int radius = 1; // 1000 meters 
		int spinner_current_index=0;
	
//view filter include in layout
	View sort_filter_nearby;

	private final int LOCATION_ACCESS_SETTING_REQUEST=1;
	private boolean callForLocation;
		
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_nearby_place);
		findViewById(R.id.nearby_custom_progress).setVisibility(View.VISIBLE);
		
		// Getting listview
		lv = (ListView) findViewById(R.id.list);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				//Restaurent resturent = (Restaurent) resturentList.get(position);
				HashMap<String, String> map= resturantListItems.get(position);
				/*String resid=resturantListItems.get(position).get("Id");//map.get("Id");
				String RestName=resturantListItems.get(position).get("RestName");//map.get("Id");
*/				
				String resid =((Restaurent)lv.getItemAtPosition(position)).Id;
				String RestName=((Restaurent)lv.getItemAtPosition(position)).RestName;
				
				
				//Restaurent r=resturentList.get(position);
				
				//restaurentId= resturent.Id;
				
				//Intent in = new Intent(getApplicationContext(),RestaurantTabActivity.class);
				String nev_type=getIntent().getStringExtra(StaticObjects.NEV_TYPE);
				
				if(nev_type!=null && nev_type.equals(StaticObjects.NEV_TYPE_CHOOSE_RES_PHOTO_UP)){
					Intent in=new Intent();
					in.putExtra( "Id" , resid);	
					in.putExtra( "RestName" , RestName);	
					setResult(RESULT_OK, in);
					CapturePhotoActivity.resid=resid;
					CapturePhotoActivity.resname=RestName;
					
					finish();
				}else{
					//Intent in = new Intent(getApplicationContext(),RestaurantTabActivity.class);
					Intent in = new Intent(getApplicationContext(),TabMainActivity.class);
					in.putExtra( "Id" , resid);				
					/*.putExtra( "latitude" , r.latitude)
					.putExtra( "longitude" , r.longitude)
					.putExtra( "RestName" , r.RestName)
					.putExtra( "address" , r.RestLoc);*/
					startActivity(in);
				}
								
				
				
				
			}
		});
		lv.setVisibility(View.INVISIBLE);
		
		sort_filter_nearby= findViewById(R.id.sort_filter_nearby);
		
		Typeface tfLight= StaticObjects.gettfLight(this);
		
		//set up sort and filter
				// offer providerlist
				ArrayList<String> filterBylist = new ArrayList<String>();
				filterBylist.add("Rating");
				filterBylist.add("Open");
				
				//sort
				ArrayList<String> sortBylist = new ArrayList<String>();
				sortBylist.add("Name (A-Z)");
				sortBylist.add("Name (Z-A)");
				sortBylist.add("Lowest Cost First");
				sortBylist.add("Highest Cost First");
				sortBylist.add("Highest Capacity First");
				sortBylist.add("Lowest Capacity First");
				sortBylist.add("Highest Rating First");
		setFilterSortSpinner(sort_filter_nearby, tfLight, filterBylist, sortBylist);
		
		
		

		adView = (AdView) this.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder().build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});
		
		
		LocId=getIntent().getStringExtra("LocId");
		

		
		

		setHomeButton(this);

		// Initialize GPS Class object
		gps = new GPSTracker(this);


		// check if GPS location can get
		if (gps.canGetLocation()) {
			Log.d("Your Location", "latitude:" + gps.getLatitude() + ", longitude: " + gps.getLongitude());
			getMyLocation();
		} else {

			gps.showSettingsAlert(LOCATION_ACCESS_SETTING_REQUEST);
			callForLocation=true;
			// stop executing code by return
			return;
		}

		loadData();
	}

	private void getMyLocation() {
		myLocation=new LatLng( gps.getLatitude(), gps.getLongitude());
	}

	private void loadData(){
		showLoader();
		//new LoadPlaces().execute();
		Volley.newRequestQueue(this).add(postRequest);
	}
	
	
	private void showLoader() {
		lv.setOnScrollListener(null);				
		findViewById(R.id.nearby_custom_progress).setVisibility(View.VISIBLE);
		
	}

	private void setFilterSortSpinner(View spinnerHolder, final Typeface tfLight, ArrayList<String> filterBylist, ArrayList<String> sortBylist){

		txtTitleNearBy=(TextView)spinnerHolder.findViewById(R.id.txtOfferLocation);
		txtTitleNearBy.setText("Restaurants Nearby ");
		spinnerHolder.setVisibility(View.INVISIBLE);
		//spinner
		Spinner spinner = (Spinner) spinnerHolder.findViewById(R.id.spinner_offer);
		// Create an ArrayAdapter using the string array and a default spinner layout
		ArrayAdapter<CharSequence> adapter_km = ArrayAdapter.createFromResource(this,
		        R.array.km, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter_km.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinner.setAdapter(adapter_km);
		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				radius=(pos+1) ;
				//new LoadPlaces().execute();
				Log.d("kilometer", radius+"");
				//Toast.makeText(getApplicationContext(), "kilometer"+radius,Toast.LENGTH_SHORT).show();
				if(spinner_current_index!=pos){
					
					 resturantListItems = new ArrayList<HashMap<String, String>>();
					spinner_current_index=pos;
					loadData();
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		} );
		
		
		// offer_provider_list.add("Others");

		Spinner spinnerSort = (Spinner) spinnerHolder.findViewById(R.id.spinner_sort);		
		ArrayAdapter<String> adapter_sort = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, sortBylist) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {

				View v = super.getView(position, convertView, parent);
				((TextView) v).setTypeface(tfLight);
				((TextView) v).setEllipsize(TruncateAt.END);

				return v;
			}

			@Override
			public View getDropDownView(int position, View convertView,
					ViewGroup parent) {

				View v = super.getDropDownView(position, convertView, parent);
				((TextView) v).setTypeface(tfLight);
				return v;
			}

		};
		// Specify the layout to use when the list of choices appears
		adapter_sort.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinnerSort.setAdapter(adapter_sort);
		spinnerSort.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,int pos, long id) {			
				//CuisineRestAdapter adapter= (CuisineRestAdapter)lv.getAdapter();
				
				NearByRestaurantAdapter adapter = (NearByRestaurantAdapter)lv.getAdapter();
				
				if(adapter ==null){return;}								
				adapter.setSortBY(pos);	
				lv.setSelectionAfterHeaderView();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
	
		case R.id.action_search:
			// search action
			RelativeLayout relativeLayout=(RelativeLayout)findViewById(R.id.nearby_place_list_root_layout);		
			addSearchLayout(relativeLayout);			
			return true;
		
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	
	
	

	@Override
	public void onPause() {
		super.onPause();

		if (adView != null)
			adView.pause();

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		
		if (adView != null)
			{adView.destroy();}
		
		//analytics stop tracking
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}
	
	@Override
	protected void onStart() {	
		super.onStart();
		
		//analytics stop tracking
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}


	/*class LoadPlaces extends AsyncTask<String, String, String> {

		*//**
		 * Before starting background thread Show Progress Dialog
		 * *//*
		@Override
		protected void onPreExecute() {
			lv.setOnScrollListener(null);
			super.onPreExecute();
			
			findViewById(R.id.nearby_custom_progress).setVisibility(View.VISIBLE);
		}

		*//**
		 * getting Places JSON
		 * *//*
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {

				//String types = "LocId=" + LocId+"&Page="+currentPage;				
				resturentList = foodyResturent.getNearByRestaurents(""+gps.getLatitude(), ""+gps.getLongitude(), ""+radius);
				

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		*//**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **//*
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			//pDialog.dismiss();
			//findViewById(R.id.nearby_custom_progress).setVisibility(View.GONE);
			final Animator.AnimatorListener animatorListener= new Animator.AnimatorListener() {
				
				@Override
				public void onAnimationStart(Animator animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationRepeat(Animator animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animator animation) {
					// TODO Auto-generated method stub
					lv.setVisibility(View.VISIBLE);
					AnimationTween.animateViewPosition(lv, NearByNativeRestaurantList.this);
				}
				
				@Override
				public void onAnimationCancel(Animator animation) {
					// TODO Auto-generated method stub
					
				}
			};
			
			Animator.AnimatorListener animatorListener2= new Animator.AnimatorListener() {
				
				@Override
				public void onAnimationStart(Animator animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationRepeat(Animator animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animator animation) {
					// TODO Auto-generated method stub
					//txtTitleNearBy.setVisibility(View.VISIBLE);
					 sort_filter_nearby.setVisibility(View.VISIBLE);
					AnimationTween.animateViewPosition(sort_filter_nearby, animatorListener, NearByNativeRestaurantList.this);
				}
				
				@Override
				public void onAnimationCancel(Animator animation) {
					// TODO Auto-generated method stub
					
				}
			};
			
			
			AnimationTween.animateView(findViewById(R.id.nearby_custom_progress), animatorListener2, NearByNativeRestaurantList.this);
			
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					*//**
					 * Updating parsed Places into LISTVIEW
					 * *//*
					// Get json response status
				

					if (resturentList == null || resturentList.size() <= 0) {

						
						
						String lat=getIntent().getStringExtra("Lat");
						String lng=getIntent().getStringExtra("Lng");
						
						if(lat!=null && lng!=null && !lat.equals("") && !lng.equals("") ){
							
							finish();
							Intent in = new Intent(getApplicationContext(),NearByplaceListViewDivision.class);
							in.putExtra( "Lat" , lat); 
							in.putExtra( "Lng" , lng);
							in.putExtra("LocName", getIntent().getStringExtra("LocName"));
							startActivity(in);
							
						}else{
							
							//show message
							findViewById(R.id.nearby_custom_progress).setVisibility(View.VISIBLE);
							View v = findViewById(R.id.nearby_custom_progress);
							TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
							textPreparing.setText("Nothing found");
							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
							
							AnimationTween.animateViewPosition(findViewById(R.id.nearby_custom_progress), NearByNativeRestaurantList.this);

						}
						
						
						
						 
					} else if (resturentList != null) {
						// Check for all possible status

						// Successfully got places details
						if (resturentList != null) {
							// loop through each place
							
							//search suggestion
							ArrayList<SearchResturent> searchRestList= new ArrayList<SearchResturent>(); 
							
							
							for (Restaurent p : resturentList) {

								HashMap<String, String> map = new HashMap<String, String>();
								map.put("Id", p.Id);								
								map.put("RestName", p.RestName);
								map.put("RestLoc", p.RestLoc+", "+p.RestDistName);
								
								map.put("Id", p.Id);
								map.put("RestName", p.RestName);
								map.put("Rating", p.Rating);
								
								
								
								//distance
								double distance=0;
								try {
									 p.setDistance(getDistanceKM(new LatLng(gps.latitude, gps.longitude),new LatLng(Double.parseDouble(p.latitude), Double.parseDouble(p.longitude) )));
									
								} catch (Exception e) {
									// TODO: handle exception
								}
								
								String distancetxt="";
								
								if(distance<=0)
									distancetxt="";
								else
									{
									map.put("km", distance+"");
									distancetxt=distance+" km(approx), ";
									
									}
								
								
								map.put("DisFrom", p.RestLoc+", "+p.RestDistName);
								
								
								map.put("DisAmount", ""+CF.openOrClose(p.Open, p.Close) );
								map.put("IMG", p.IMG);
								
								
								
								
								totalPage=Integer.parseInt(p.PageNo);

								
								

								// adding HashMap to ArrayList
								resturantListItems.add(map);
								
								//search suggestion
								searchRestList.add(new SearchResturent(p.Id, p.RestName, null, p.RestLoc, null, null, null));
							}
							//search suggestion
							addDataToDB(searchRestList);
							
							NearByRestaurantAdapter adapter = new NearByRestaurantAdapter(NearByNativeRestaurantList.this, resturentList);
							//ListAdapterRestaurantNearByNative adapter= new ListAdapterRestaurantNearByNative(NearByNativeRestaurantList.this, resturantListItems, new String[]{"RestName","DisFrom","Rating","DisAmount","IMG", "km"});

							
							
							

							// Adding data into listview
							lv.setAdapter(adapter);
							adapter.notifyDataSetChanged();
							currentPage++;
							
							lv.setOnScrollListener(new OnScrollListener() {
								
								@Override
								public void onScrollStateChanged(AbsListView view, int scrollState) {
									// TODO Auto-generated method stub
									
								}
								
								@Override
								public void onScroll(AbsListView view, int firstVisibleItem,
										int visibleItemCount, int totalItemCount) {
									// TODO Auto-generated method stub
									boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;

								        if(loadMore && currentPage<totalPage) {
								        	Toast.makeText(getApplicationContext(), "Loading More", Toast.LENGTH_SHORT).show();
								        	
								        	new LoadPlaces().execute();
								        	
								        }
									}
							});
							
							
							
						}

						else if (resturentList == null) {
							//show message
							findViewById(R.id.nearby_custom_progress).setVisibility(View.VISIBLE);
							View v = findViewById(R.id.nearby_custom_progress);
							TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
							textPreparing.setText("Nothing found.");
							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
							AnimationTween.animateViewPosition(findViewById(R.id.nearby_custom_progress), NearByNativeRestaurantList.this);
							
							showPopUp();
							
							finish();
							googlePlacesNearby();

						}
					}
				}

			});

		}

	}*/
	
	StringRequest postRequest = new StringRequest(Request.Method.POST, LetsEatRequest.RESTURENT_LIST_NEARBY_URL,
	        new Response.Listener<String>() {
	            @Override
	            public void onResponse(String response) {
	                try {
	                	// Result handling 
	                	resturentList = new LetsEatRequest().getNearByRestaurents(response);

	        			// dismiss the dialog after getting all products
	        			//pDialog.dismiss();
	        			//findViewById(R.id.nearby_custom_progress).setVisibility(View.GONE);
	        			final Animator.AnimatorListener animatorListener= new Animator.AnimatorListener() {
	        				
	        				@Override
	        				public void onAnimationStart(Animator animation) {
	        					// TODO Auto-generated method stub
	        					
	        				}
	        				
	        				@Override
	        				public void onAnimationRepeat(Animator animation) {
	        					// TODO Auto-generated method stub
	        					
	        				}
	        				
	        				@Override
	        				public void onAnimationEnd(Animator animation) {
	        					// TODO Auto-generated method stub
	        					lv.setVisibility(View.VISIBLE);
	        					AnimationTween.animateViewPosition(lv, NearByNativeRestaurantList.this);
	        				}
	        				
	        				@Override
	        				public void onAnimationCancel(Animator animation) {
	        					// TODO Auto-generated method stub
	        					
	        				}
	        			};
	        			
	        			Animator.AnimatorListener animatorListener2= new Animator.AnimatorListener() {
	        				
	        				@Override
	        				public void onAnimationStart(Animator animation) {
	        					// TODO Auto-generated method stub
	        					
	        				}
	        				
	        				@Override
	        				public void onAnimationRepeat(Animator animation) {
	        					// TODO Auto-generated method stub
	        					
	        				}
	        				
	        				@Override
	        				public void onAnimationEnd(Animator animation) {
	        					// TODO Auto-generated method stub
	        					//txtTitleNearBy.setVisibility(View.VISIBLE);
	        					 sort_filter_nearby.setVisibility(View.VISIBLE);
	        					AnimationTween.animateViewPosition(sort_filter_nearby, animatorListener, NearByNativeRestaurantList.this);
	        				}
	        				
	        				@Override
	        				public void onAnimationCancel(Animator animation) {
	        					// TODO Auto-generated method stub
	        					
	        				}
	        			};
	        			
	        			
	        			AnimationTween.animateView(findViewById(R.id.nearby_custom_progress), animatorListener2, NearByNativeRestaurantList.this);
	        			
	        			// updating UI from Background Thread
	        			runOnUiThread(new Runnable() {
	        				public void run() {
	        					/**
	        					 * Updating parsed Places into LISTVIEW
	        					 * */
	        					// Get json response status
	        				

	        					if (resturentList == null || resturentList.size() <= 0) {

	        						
	        						
	        						String lat=getIntent().getStringExtra("Lat");
	        						String lng=getIntent().getStringExtra("Lng");
	        						
	        						if(lat!=null && lng!=null && !lat.equals("") && !lng.equals("") ){
	        							
	        							finish();
	        							Intent in = new Intent(getApplicationContext(),NearByplaceListViewDivision.class);
	        							in.putExtra( "Lat" , lat); 
	        							in.putExtra( "Lng" , lng);
	        							in.putExtra("LocName", getIntent().getStringExtra("LocName"));
	        							startActivity(in);
	        							
	        						}else{
	        							
	        							//show message
	        							findViewById(R.id.nearby_custom_progress).setVisibility(View.VISIBLE);
	        							View v = findViewById(R.id.nearby_custom_progress);
	        							TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
	        							textPreparing.setText("Nothing found");
	        							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
	        							
	        							AnimationTween.animateViewPosition(findViewById(R.id.nearby_custom_progress), NearByNativeRestaurantList.this);

	        						}
	        						
	        						
	        						
	        						 
	        					} else if (resturentList != null) {
	        						// Check for all possible status

	        						// Successfully got places details
	        						if (resturentList != null) {
	        							// loop through each place
	        							
	        							//search suggestion
	        							ArrayList<SearchResturent> searchRestList= new ArrayList<SearchResturent>(); 
	        							
	        							
	        							for (Restaurent p : resturentList) {

	        								HashMap<String, String> map = new HashMap<String, String>();
	        								/*map.put("Id", p.Id);								
	        								map.put("RestName", p.RestName);
	        								map.put("RestLoc", p.RestLoc+", "+p.RestDistName);*/
	        								
	        								map.put("Id", p.Id);
	        								map.put("RestName", p.RestName);
	        								map.put("Rating", p.Rating);
	        								
	        								
	        								
	        								//distance
	        								double distance=0;
	        								try {
	        									 p.setDistance(getDistanceKM(new LatLng(gps.latitude, gps.longitude),new LatLng(Double.parseDouble(p.latitude), Double.parseDouble(p.longitude) )));
	        									
	        								} catch (Exception e) {
	        									// TODO: handle exception
	        								}
	        								
	        								String distancetxt="";
	        								
	        								if(distance<=0)
	        									distancetxt="";
	        								else
	        									{
	        									map.put("km", distance+"");
	        									distancetxt=distance+" km(approx), ";
	        									
	        									}
	        								
	        								
	        								map.put("DisFrom", p.RestLoc+", "+p.RestDistName);
	        								
	        								
	        								map.put("DisAmount", ""+CF.openOrClose(p.Open, p.Close) );
	        								map.put("IMG", p.IMG);
	        								
	        								
	        								
	        								
	        								totalPage=Integer.parseInt(p.PageNo);

	        								
	        								

	        								// adding HashMap to ArrayList
	        								resturantListItems.add(map);
	        								
	        								//search suggestion
	        								searchRestList.add(new SearchResturent(p.Id, p.RestName, null, p.RestLoc, null, null, null));
	        							}
	        							//search suggestion
	        							addDataToDB(searchRestList);
	        							
	        							NearByRestaurantAdapter adapter = new NearByRestaurantAdapter(NearByNativeRestaurantList.this, resturentList);
	        							//ListAdapterRestaurantNearByNative adapter= new ListAdapterRestaurantNearByNative(NearByNativeRestaurantList.this, resturantListItems, new String[]{"RestName","DisFrom","Rating","DisAmount","IMG", "km"});

	        							
	        							
	        							

	        							// Adding data into listview
	        							lv.setAdapter(adapter);
	        							adapter.notifyDataSetChanged();
	        							currentPage++;
	        							
	        							lv.setOnScrollListener(new OnScrollListener() {
	        								
	        								@Override
	        								public void onScrollStateChanged(AbsListView view, int scrollState) {
	        									// TODO Auto-generated method stub
	        									
	        								}
	        								
	        								@Override
	        								public void onScroll(AbsListView view, int firstVisibleItem,
	        										int visibleItemCount, int totalItemCount) {
	        									// TODO Auto-generated method stub
	        									boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;

	        								        if(loadMore && currentPage<totalPage) {
	        								        	Toast.makeText(getApplicationContext(), "Loading More", Toast.LENGTH_SHORT).show();
	        								        	
	        								        	loadData();
	        								        	
	        								        }
	        									}
	        							});
	        							
	        							
	        							
	        						}

	        						else if (resturentList == null) {
	        							//show message
	        						/*	findViewById(R.id.nearby_custom_progress).setVisibility(View.VISIBLE);
	        							View v = findViewById(R.id.nearby_custom_progress);
	        							TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
	        							textPreparing.setText("Nothing found.");
	        							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
	        							AnimationTween.animateViewPosition(findViewById(R.id.nearby_custom_progress), NearByNativeRestaurantList.this);
	        							
	        							showPopUp();*/
	        							
	        							finish();
	        							googlePlacesNearby();

	        						}
	        					}
	        				}

	        			});

	        		
	                } catch (Exception e) {
	                	 
	                	
	                    e.printStackTrace();
	                }
	            }
	        },
	        new Response.ErrorListener() {
	            @Override
	            public void onErrorResponse(VolleyError error) {
	                error.printStackTrace();
	            }
	        }
	) {
	    @Override
	    protected Map<String, String> getParams()
	    {
	        Map<String, String>  params = new HashMap<String, String>();
	        // the POST parameters:
	        params.put("lat", ""+gps.getLatitude());
	        params.put("lang", ""+gps.getLongitude());
	        params.put("rad", ""+radius);
	        return params;
	    }
	};
	
	
	
	
	
	

	@Override
	public boolean onNavigationItemSelected(int arg0, long arg1) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	private void showPopUp() {

		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
		// helpBuilder.setTitle("Pop Up");
		helpBuilder.setMessage("No Restaurant found in Let's Eat. Try Google Places.");
		helpBuilder.setPositiveButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {

					}
				});

		helpBuilder.setNegativeButton("Yes",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						 googlePlacesNearby();
					}
				});

		// Remember, create doesn't show the dialog
		AlertDialog helpDialog = helpBuilder.create();
		helpDialog.show();

	}
	
	void googlePlacesNearby(){
		Intent i = new Intent(NearByNativeRestaurantList.this, NearByplaceListView.class);						
		startActivity(i);
		finish();
	}
	
	
	public double getDistanceKM(LatLng fromPosition, LatLng toPosition){
		Location locationA = new Location("point A");

		locationA.setLatitude(fromPosition.latitude);

		locationA.setLongitude(fromPosition.longitude);

		Location locationB = new Location("point B");

		locationB.setLatitude(toPosition.latitude);

		locationB.setLongitude(toPosition.longitude);
		
		double value = (locationA.distanceTo(locationB)/1000);
		//double rounded = (double) Math.round(value,2 ) ;
		Log.i("kilometer before format", value+"");
		
		DecimalFormat df = new DecimalFormat("###0.00");
		double rounded =Double.parseDouble(df.format(value));
		Log.i("kilometer after format", rounded + "");

		return rounded;
		
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == LOCATION_ACCESS_SETTING_REQUEST && new  GPSTracker(this).canGetLocation() && callForLocation ) {

			//analytic send button event
			analyticsSendEvent(this.getClass().getSimpleName(), StaticObjects.ANA_EV_CAT_ACTION, "Nearby");
			getMyLocation();
			loadData();

			/*//Intent i = new Intent(HomeT.this, NearByplaceListView.class);
			Intent i = new Intent(HomeT.this, NearByNativeRestaurantList.class);
			startActivity(i);*/
		}
		callForLocation=false;
	}

	
}
