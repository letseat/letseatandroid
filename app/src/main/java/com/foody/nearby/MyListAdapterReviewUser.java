package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.FoodyResturent;
import com.mcc.letseat.R;
import com.mcc.libs.AnimationTween;
import com.mcc.user.SavedPrefernce;
import com.mcc.user.profile.UserActivity;
import com.squareup.picasso.Picasso;

/**
 * Custom data adapter for user review list
 */
public class MyListAdapterReviewUser extends BaseAdapter {

	ArrayList<HashMap<String, String>> aa;
	Context c;
	String listTags[];
	
	
	HashMap<String, String> hashMap;
	int selectedPosition;
	int likeCount;
	
	ImageView imageViewfollowing;
	TextView txtLikesCount;
	protected int lastimageViewfollowing; 
	

	public MyListAdapterReviewUser(Context c, ArrayList<HashMap<String, String>> aa) {
		this.aa = aa;
		this.c = c;
	}

	public MyListAdapterReviewUser(Context c,
			ArrayList<HashMap<String, String>> aa, String listTags[]) {
		this.aa = aa;
		this.c = c;
		this.listTags = listTags;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return aa.size();
	}

	@Override
	public String getItem(int arg0) {
		// TODO Auto-generated method stub
		return aa.get(arg0).get("reference");
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	private int lastPosition = -1;

	@Override
	public View getView(int pos, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub

		View v = arg1;
		
		if(null==v){
			LayoutInflater li = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = li.inflate(R.layout.list_item_review_user, null);
		}
		
		RelativeLayout root = (RelativeLayout) v.findViewById(R.id.follower_main_layout);

		ImageView imageViewAvatar = (ImageView) v.findViewById(R.id.imageViewFollowerAvatar);		
		TextView txtAvatarName = (TextView) v.findViewById(R.id.txtFollowerName);
		TextView txtAvatarMetadata = (TextView) v.findViewById(R.id.txtFollowReviewInfo);
		ImageView imageViewfollow = (ImageView) v.findViewById(R.id.imageViewfollow);
		TextView txtReview = (TextView) v.findViewById(R.id.txtReview);
		TextView txtCommentTime = (TextView) v.findViewById(R.id.txtCommentTime);
		TextView txtReviewRating = (TextView) v.findViewById(R.id.txtReviewRating);
		TextView txtComments = (TextView) v.findViewById(R.id.txtComments);
		TextView txtLikes = (TextView) v.findViewById(R.id.txtLikes);
		LinearLayout layoutBottom=(LinearLayout)v.findViewById(R.id.list_item_review_bottom_boader);
		
		TextView textView_reviewed_res_name=(TextView) v.findViewById(R.id.textView_reviewed_res_name);
		ImageView imageView_reviewed_res_name=(ImageView) v.findViewById(R.id.imageView_reviewed_res_name);
		
		 
		
		 
		 final int position=pos;
		 imageViewfollow.setTag(position);
		 imageViewfollow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {		
				AnimationTween.zoomInOut(v, (Activity)c);
				selectedPosition=position;
				userid=aa.get(position).get("USERID");			
				imageViewfollowing=(ImageView)v;
				
				SavedPrefernce savedPrefernce=new SavedPrefernce( (Activity)c);
				if ( savedPrefernce.isUserExists( ) ){
					
					new PostFollowers().execute();

				} else {
					
					StaticObjects.callRegForAction((Activity)c);
					
				}
				
				
			}
		});
		 
		 txtLikes.setTag(position);
		 txtLikes.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					hashMap=aa.get(position);
					selectedPosition=position;
					revid=aa.get(position).get("ID");
					comment="";
					rlike="1";
					likeCount=Integer.parseInt(aa.get(position).get(listTags[6]));
					 txtLikesCount=(TextView)v;				 
					new ReviewLike().execute();
				}
			});
		 
		 txtComments.setTag(position);
		 txtComments.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					/*hashMap=aa.get(position);
					selectedPosition=position;
					revid=aa.get(position).get("ID");
					comment="";
					rlike="0";
					likeCount=Integer.parseInt(aa.get(position).get(listTags[6]));
					 txtLikesCount=(TextView)v;				 
					new ReviewLike().execute();*/
				}
			});
		 
		 imageViewAvatar.setTag(position);
		 imageViewAvatar.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// update followers
					Intent intent=new Intent(c, UserActivity.class);
					userid=aa.get(position).get("USERID");
					intent.putExtra("userid", userid);
					c.startActivity(intent);
				}
			});
		 
		 txtAvatarName.setTag(position);
		 txtAvatarName.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// update followers
					Intent intent=new Intent(c, UserActivity.class);
					userid=aa.get(position).get("USERID");
					intent.putExtra("userid", userid);
					c.startActivity(intent);
				}
			});

		

		if (pos % 2 == 0) {
			root.setBackgroundColor(Color.parseColor("#f3f3f4"));
			layoutBottom.setBackgroundColor(Color.parseColor("#f3f3f4"));
		} else {
			root.setBackgroundColor(Color.parseColor("#FFFFFF"));
			layoutBottom.setBackgroundColor(Color.parseColor("#FFFFFF"));
		}

		// String ww =aa.get(pos);
		if (listTags != null) {
			/*
			 * first.setText(aa.get(pos).get(listTags[0]));
			 * second.setText(aa.get(pos).get(listTags[1]));
			 * third.setText(aa.get(pos).get(listTags[2]));
			 * four.setText(aa.get(pos).get(listTags[3]));
			 */

			txtAvatarName.setText(aa.get(pos).get(listTags[0]));
			txtAvatarMetadata.setText(aa.get(pos).get(listTags[1]));
			txtReview.setText(aa.get(pos).get(listTags[2]));
			txtCommentTime.setText(aa.get(pos).get(listTags[3]));
			txtReviewRating.setText(""+Float.parseFloat(aa.get(pos).get(listTags[4]))/StaticObjects.RATING_OFFSET );
			txtComments.setText(aa.get(pos).get(listTags[5]));
			
			txtLikes.setText(aa.get(pos).get(listTags[6])+" Like");
			
			textView_reviewed_res_name.setText(aa.get(pos).get(listTags[9]));
			
			///
			txtAvatarName.setTypeface( StaticObjects.gettfNormal(c));
			txtAvatarMetadata.setTypeface( StaticObjects.gettfLight(c));
			txtReview.setTypeface( StaticObjects.gettfNormal(c));
			txtCommentTime.setTypeface( StaticObjects.gettfNormal(c));
			txtReviewRating.setTypeface( StaticObjects.gettfNormal(c));
			txtComments.setTypeface( StaticObjects.gettfNormal(c));
			txtLikes.setTypeface( StaticObjects.gettfNormal(c));
			textView_reviewed_res_name.setTypeface( StaticObjects.gettfNormal(c));


			
			//loading image
			if(aa.get(pos).get(listTags[7])!=null && !aa.get(pos).get(listTags[7]).isEmpty())
			{
				Picasso.with(c)
			    .load(aa.get(pos).get(listTags[7]))
			    .placeholder(R.mipmap.dummy_avatar)
			    .error(R.mipmap.dummy_avatar)
			    .into(imageViewAvatar);
			}
			
			//loading restaurant Image
			if(aa.get(pos).get(listTags[10])!=null && !aa.get(pos).get(listTags[10]).isEmpty())
			{
				Picasso.with(c)
				.load(aa.get(pos).get(listTags[10]))
				.placeholder(R.drawable.lazy_ic)
				.error(R.drawable.lazy_ic)
				.into(imageView_reviewed_res_name);
			}
			
			
			
			
			
			//follow unfollow
			System.out.println("=============================================="+aa.get(pos).get(listTags[8]));
			if( aa.get(pos).get(listTags[8]).equals("YES") ){
				imageViewfollow.setImageResource(R.mipmap.following);
			}else if( aa.get(pos).get(listTags[8]).equals("NO") ){
				imageViewfollow.setImageResource(R.mipmap.follow);
			} 

			/*Log.e("offer image link",
					"link: " + aa.get(pos).get(listTags[4]) + "0"
							+ aa.get(pos).get(listTags[0]) + "<1:"
							+ aa.get(pos).get(listTags[1]) + "<2:"
							+ aa.get(pos).get(listTags[2]) + "<3:"
							+ aa.get(pos).get(listTags[3]));
			System.out.println("" + aa.get(pos).get(listTags[4]));*/

		} else {

			txtAvatarName.setText(aa.get(pos).get(""));
			txtAvatarMetadata.setText(aa.get(pos).get(""));
			txtReview.setText(aa.get(pos).get(""));
			txtCommentTime.setText(aa.get(pos).get(""));
			txtReviewRating.setText(aa.get(pos).get(""));
			txtComments.setText(aa.get(pos).get(""));
			txtLikes.setText(aa.get(pos).get(""));
		}
		
		v.findViewById(R.id.rl_u).setVisibility(View.GONE);

		/*
		 * name.setText(aa.get(pos).get("name"));
		 * dis.setText(aa.get(pos).get("distance"));
		 * ref.setText(aa.get(pos).get("reference"));
		 */
		
		
		//apply animation on current view
		 Animation animation = AnimationUtils.loadAnimation(c, (pos > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
		 v.startAnimation(animation);
		 lastPosition = pos;
		
		return v;
	}
	
	// Progress dialog
			ProgressDialog pDialog;
			
	// Google Places
	FoodyResturent foodyResturent;
	
	String respons;
	
	private String followerid;
	private String userid;
	
	class PostFollowers  extends AsyncTask<String, String, String> {


		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			/*pDialog = new ProgressDialog(c);
			pDialog.setMessage(Html
					.fromHtml("<b>Loading ...</b>"));
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();*/
			
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {
				
				SavedPrefernce prefernce=new SavedPrefernce((Activity)c);
				followerid=prefernce.getUserID();

				if( userid.equals(followerid) ){
					respons = "self";
				}else{
					respons = foodyResturent.updateFollow(userid, followerid);
				}
				
				

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			
			// updating UI from Background Thread
			((Activity)c).runOnUiThread(new Runnable() {
				public void run() {
					
					if(respons.equals("success")){
						imageViewfollowing.setImageResource(R.mipmap.following);
					}else if(respons.equals("unfollow")){
						imageViewfollowing.setImageResource(R.mipmap.follow);
					}else if(respons.equals("self")){
						Toast.makeText(c, "You can't follow yourself", Toast.LENGTH_SHORT).show(); 
					}
					
					
				}

			});

		}

	}
	

	
	String response;

	public String revid;

	

	public String comment;

	public String rlike;
	class ReviewLike extends AsyncTask<String, String, String> {

		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		/*	pDialog = new ProgressDialog(c);
			pDialog.setMessage(Html.fromHtml("<b>Loading ...</b>"));
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();*/
			
		}

		protected String doInBackground(String... args) {
			
			foodyResturent = new FoodyResturent();

			try {

				SavedPrefernce prefernce=new SavedPrefernce((Activity)c);
				userid=prefernce.getUserID();
				
				System.out.println("================="+revid+ userid+ comment+ rlike);
				response = foodyResturent.insertReviewLike(revid, userid, comment, rlike);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

	
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			//pDialog.dismiss();
			// updating UI from Background Thread
			((Activity) c).runOnUiThread(new Runnable() {

				public void run() {
					
					if(response.equals("success")){
						//Toast.makeText(c, "Post Successful.", Toast.LENGTH_SHORT).show();
						likeCount++;
						txtLikesCount.setText(likeCount+" Like");
						
						hashMap.put(listTags[6], ""+likeCount);
						
					
						
					}else if(response.equals("unlike")){
						//Toast.makeText(c, "Post Successful.", Toast.LENGTH_SHORT).show();
						likeCount--;
						txtLikesCount.setText(likeCount+" Like");
						hashMap.put(listTags[6], ""+likeCount);
						
					}
					else{
						Toast.makeText(c, "Try again", Toast.LENGTH_SHORT).show();
					}
				}

			});

		}

	}		

}
