package com.foody.nearby;

import android.Manifest;
import android.animation.Animator;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils.TruncateAt;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.foody.AppData.StaticObjects;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.LetsEatRequest;
import com.foody.jsondata.Restaurent;
import com.foody.jsondata.SearchResturent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.mcc.letseat.Adapter.NearByRestaurantAdapter;
import com.mcc.letseat.CapturePhotoActivity;
import com.mcc.letseat.R;
import com.mcc.letseat.tab.TabMainActivity;
import com.mcc.libs.ActivityWithSliding;
import com.mcc.libs.AnimationTween;
import com.mcc.libs.CF;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Custom ListView Nearby Restaurant
 */
public class NearByNativeRestaurantListNew extends ActivityWithSliding implements
		GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Google Places
	FoodyResturent foodyResturent;

	// restaurant List
	ArrayList<Restaurent> resturentList;

	// GPS Location
	//GPSTracker gps;

	LatLng myLocation = null;

	// Button
	ImageView btnShowOnMap;


	// Places Listview
	ListView lv;

	// ListItems data
	ArrayList<HashMap<String, String>> resturantListItems = new ArrayList<HashMap<String, String>>();

	// //admob
	AdView adView = null;

	ImageView searchAny;

	String searchKeywords;

	//post value
	String LocId;
	int currentPage = 0;
	int totalPage = 1;

	TextView txtTitleNearBy;

	// Radius in meters - increase this value if you don't find any places
	int radius = 1; // 1000 meters
	int spinner_current_index = 0;

	//view filter include in layout
	View sort_filter_nearby;

	private final int LOCATION_ACCESS_SETTING_REQUEST = 1;
	private boolean callForLocation;


	private GoogleApiClient mGoogleApiClient;
	private Location mLastLocation;
	private Location mCurrentLocation;
	private String mLastUpdateTime;
	private boolean locationEnabled;
	private final int REQUEST_CHECK_SETTINGS = 1000;
	LocationRequest mLocationRequest;
	//private

	private final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 2000;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_nearby_place);
		findViewById(R.id.nearby_custom_progress).setVisibility(View.VISIBLE);

		// Getting listview
		lv = (ListView) findViewById(R.id.list);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {

				//Restaurent resturent = (Restaurent) resturentList.get(position);
				HashMap<String, String> map = resturantListItems.get(position);
				/*String resid=resturantListItems.get(position).get("Id");//map.get("Id");
				String RestName=resturantListItems.get(position).get("RestName");//map.get("Id");
*/
				String resid = ((Restaurent) lv.getItemAtPosition(position)).Id;
				String RestName = ((Restaurent) lv.getItemAtPosition(position)).RestName;


				//Restaurent r=resturentList.get(position);

				//restaurentId= resturent.Id;

				//Intent in = new Intent(getApplicationContext(),RestaurantTabActivity.class);
				String nev_type = getIntent().getStringExtra(StaticObjects.NEV_TYPE);

				if (nev_type != null && nev_type.equals(StaticObjects.NEV_TYPE_CHOOSE_RES_PHOTO_UP)) {
					Intent in = new Intent();
					in.putExtra("Id", resid);
					in.putExtra("RestName", RestName);
					setResult(RESULT_OK, in);
					CapturePhotoActivity.resid = resid;
					CapturePhotoActivity.resname = RestName;

					finish();
				} else {
					//Intent in = new Intent(getApplicationContext(),RestaurantTabActivity.class);
					Intent in = new Intent(getApplicationContext(), TabMainActivity.class);
					in.putExtra("Id", resid);
					/*.putExtra( "latitude" , r.latitude)
					.putExtra( "longitude" , r.longitude)
					.putExtra( "RestName" , r.RestName)
					.putExtra( "address" , r.RestLoc);*/
					startActivity(in);
				}


			}
		});
		lv.setVisibility(View.INVISIBLE);

		sort_filter_nearby = findViewById(R.id.sort_filter_nearby);

		Typeface tfLight = StaticObjects.gettfLight(this);

		//set up sort and filter
		// offer providerlist
		ArrayList<String> filterBylist = new ArrayList<String>();
		filterBylist.add("Rating");
		filterBylist.add("Open");

		//sort
		ArrayList<String> sortBylist = new ArrayList<String>();
		sortBylist.add("Near to Far");
		sortBylist.add("Name (A-Z)");
		sortBylist.add("Name (Z-A)");
		sortBylist.add("Lowest Cost First");
		sortBylist.add("Highest Cost First");
		sortBylist.add("Highest Capacity First");
		sortBylist.add("Lowest Capacity First");
		sortBylist.add("Highest Rating First");
		setFilterSortSpinner(sort_filter_nearby, tfLight, filterBylist, sortBylist);


		adView = (AdView) this.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder().build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});


		LocId = getIntent().getStringExtra("LocId");


		setHomeButton(this);

		///last known location
		// create instance of GoogleApiClient
		createGoogleApiClientAndBuild();
		//checking if hardware setting need to enable
		checkLocationSetting();

		// Initialize GPS Class object
		//gps = new GPSTracker(this);


		// check if GPS location can get
		/*if (gps.canGetLocation()) {
			Log.d("Your Location", "latitude:" + gps.getLatitude() + ", longitude: " + gps.getLongitude());
			getMyLocation();
		} else {

			gps.showSettingsAlert(LOCATION_ACCESS_SETTING_REQUEST);
			callForLocation=true;
			// stop executing code by return
			return;
		}*/

		//loadData();
	}


	private synchronized void createGoogleApiClientAndBuild() {
		if (mGoogleApiClient == null) {
			mGoogleApiClient = new GoogleApiClient.Builder(this)
					.addConnectionCallbacks(this)
					.addOnConnectionFailedListener(this)
					.addApi(LocationServices.API)
					.build();


		}
	}

	private void loadData() {
		msgReset();

		showLoader();
		//new LoadPlaces().execute();
		Volley.newRequestQueue(this).add(postRequest);
	}


	void checkLocationSetting() {

		msgLocation();

		LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
		builder.addLocationRequest(createLocationRequest());

		//**************************
		builder.setAlwaysShow(true); //this is the key ingredient
		//**************************
		PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(
				mGoogleApiClient,
				builder.build());
		result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
			@Override
			public void onResult(LocationSettingsResult locationSettingsResult) {
				final Status status = locationSettingsResult.getStatus();
				int statusCode = status.getStatusCode();
				switch (statusCode) {
					case LocationSettingsStatusCodes.SUCCESS:
						// Location is available
						// Create an instance of GoogleAPIClient.

						break;
					case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
						// Location is not available, but we can ask permission from users
						// Show the dialog by calling startResolutionForResult(),
						// and check the result in onActivityResult().
						try {
							status.startResolutionForResult(NearByNativeRestaurantListNew.this, REQUEST_CHECK_SETTINGS);
						} catch (IntentSender.SendIntentException e) {
							// Ignore the error.
						}
						break;
					case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
						// Location is not available, and we cannot recover from the situation
						break;
				}
			}
		});
	}


	private void showLoader() {
		lv.setOnScrollListener(null);
		findViewById(R.id.nearby_custom_progress).setVisibility(View.VISIBLE);

	}

	private void setFilterSortSpinner(View spinnerHolder, final Typeface tfLight, ArrayList<String> filterBylist, ArrayList<String> sortBylist) {

		txtTitleNearBy = (TextView) spinnerHolder.findViewById(R.id.txtOfferLocation);
		txtTitleNearBy.setText("Restaurants Nearby ");
		spinnerHolder.setVisibility(View.INVISIBLE);
		//spinner
		Spinner spinner = (Spinner) spinnerHolder.findViewById(R.id.spinner_offer);
		// Create an ArrayAdapter using the string array and a default spinner layout
		ArrayAdapter<CharSequence> adapter_km = ArrayAdapter.createFromResource(this,
				R.array.km, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter_km.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinner.setAdapter(adapter_km);
		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
									   int pos, long id) {
				radius = (pos + 1);
				//new LoadPlaces().execute();
				Log.d("kilometer", radius + "");
				//Toast.makeText(getApplicationContext(), "kilometer"+radius,Toast.LENGTH_SHORT).show();
				if (spinner_current_index != pos) {

					resturantListItems = new ArrayList<HashMap<String, String>>();
					spinner_current_index = pos;
					loadData();
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});


		// offer_provider_list.add("Others");

		Spinner spinnerSort = (Spinner) spinnerHolder.findViewById(R.id.spinner_sort);
		ArrayAdapter<String> adapter_sort = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, sortBylist) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {

				View v = super.getView(position, convertView, parent);
				((TextView) v).setTypeface(tfLight);
				((TextView) v).setEllipsize(TruncateAt.END);

				return v;
			}

			@Override
			public View getDropDownView(int position, View convertView,
										ViewGroup parent) {

				View v = super.getDropDownView(position, convertView, parent);
				((TextView) v).setTypeface(tfLight);
				return v;
			}

		};
		// Specify the layout to use when the list of choices appears
		adapter_sort.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinnerSort.setAdapter(adapter_sort);
		spinnerSort.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
				//CuisineRestAdapter adapter= (CuisineRestAdapter)lv.getAdapter();

				NearByRestaurantAdapter adapter = (NearByRestaurantAdapter) lv.getAdapter();

				if (adapter == null) {
					return;
				}
				adapter.setSortBY(pos);
				lv.setSelectionAfterHeaderView();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {

			case R.id.action_search:
				// search action
				RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.nearby_place_list_root_layout);
				addSearchLayout(relativeLayout);
				return true;

			default:
				return super.onOptionsItemSelected(item);
		}

	}


	@Override
	public void onPause() {
		super.onPause();
		//stopLocationUpdates();


		if (adView != null)
			adView.pause();

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if (adView != null) {
			adView.destroy();
		}
		//analytics stop tracking
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

	@Override
	protected void onStart() {
		super.onStart();

		mGoogleApiClient.connect();
		//analytics stop tracking
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	protected void onStop() {
		if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
			mGoogleApiClient.disconnect();
		super.onStop();
	}


	//
	@Override
	public void onConnected(Bundle connectionHint) {

		try {
			mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
		} catch (SecurityException e) {

		}


		if (mLastLocation != null) {
			updateUI();
			stopLocationUpdates();
		} else {
			startLocationUpdates();
		}
	}

	@Override
	public void onConnectionSuspended(int i) {

	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {

	}

	@Override
	public void onLocationChanged(Location location) {
		mLastLocation = mCurrentLocation = location;
		mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
		updateUI();
		//cancel or stop location update
		stopLocationUpdates();
	}

	private void updateUI() {
		if (mLastLocation != null) {
			/*txtTitleNearBy.setText(String.valueOf(mLastLocation.getLatitude()));
			txtTitleNearBy.append(" : " + String.valueOf(mLastLocation.getLongitude()) + mLastUpdateTime);

			Toast.makeText(NearByNativeRestaurantListNew.this, txtTitleNearBy.getText(), Toast.LENGTH_SHORT).show();*/

			myLocation = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
			loadData();
		}
	}

	protected void startLocationUpdates() {
		boolean left = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED;
		boolean right = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED;

		if (left && right) {
			// TODO: Consider calling
			//    ActivityCompat#requestPermissions
			// here to request the missing permissions, and then overriding
			//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
			//                                          int[] grantResults)
			// to handle the case where the user grants the permission. See the documentation
			// for ActivityCompat#requestPermissions for more details.

			ActivityCompat.requestPermissions(NearByNativeRestaurantListNew.this,
					new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
					MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

			return;
		}

		LocationServices.FusedLocationApi.requestLocationUpdates(
				mGoogleApiClient, createLocationRequest(), this);

	}

	protected void stopLocationUpdates() {
		if (mGoogleApiClient.isConnected())
			LocationServices.FusedLocationApi.removeLocationUpdates(
					mGoogleApiClient, this);
	}

	protected LocationRequest createLocationRequest() {
		if (mLocationRequest == null) {

			mLocationRequest = new LocationRequest();
			mLocationRequest.setInterval(10000);
			mLocationRequest.setFastestInterval(5000);
			mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
		}
		return mLocationRequest;
	}


	StringRequest postRequest = new StringRequest(Request.Method.POST, LetsEatRequest.RESTURENT_LIST_NEARBY_URL,
			new Response.Listener<String>() {
				@Override
				public void onResponse(String response) {
					try {
						// Result handling
						resturentList = new LetsEatRequest().getNearByRestaurents(response);

						// dismiss the dialog after getting all products
						//pDialog.dismiss();
						//findViewById(R.id.nearby_custom_progress).setVisibility(View.GONE);
						final Animator.AnimatorListener animatorListener = new Animator.AnimatorListener() {

							@Override
							public void onAnimationStart(Animator animation) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onAnimationRepeat(Animator animation) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onAnimationEnd(Animator animation) {
								// TODO Auto-generated method stub
								lv.setVisibility(View.VISIBLE);
								AnimationTween.animateViewPosition(lv, NearByNativeRestaurantListNew.this);
							}

							@Override
							public void onAnimationCancel(Animator animation) {
								// TODO Auto-generated method stub

							}
						};

						Animator.AnimatorListener animatorListener2 = new Animator.AnimatorListener() {

							@Override
							public void onAnimationStart(Animator animation) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onAnimationRepeat(Animator animation) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onAnimationEnd(Animator animation) {
								// TODO Auto-generated method stub
								//txtTitleNearBy.setVisibility(View.VISIBLE);
								sort_filter_nearby.setVisibility(View.VISIBLE);
								AnimationTween.animateViewPosition(sort_filter_nearby, animatorListener, NearByNativeRestaurantListNew.this);
							}

							@Override
							public void onAnimationCancel(Animator animation) {
								// TODO Auto-generated method stub

							}
						};


						AnimationTween.animateView(findViewById(R.id.nearby_custom_progress), animatorListener2, NearByNativeRestaurantListNew.this);

						// updating UI from Background Thread
						runOnUiThread(new Runnable() {
							public void run() {
								/**
								 * Updating parsed Places into LISTVIEW
								 * */
								// Get json response status


								if (resturentList == null || resturentList.size() <= 0) {


									String lat = getIntent().getStringExtra("Lat");
									String lng = getIntent().getStringExtra("Lng");

									if (lat != null && lng != null && !lat.equals("") && !lng.equals("")) {

										finish();
										Intent in = new Intent(getApplicationContext(), NearByplaceListViewDivision.class);
										in.putExtra("Lat", lat);
										in.putExtra("Lng", lng);
										in.putExtra("LocName", getIntent().getStringExtra("LocName"));
										startActivity(in);

									} else {

										//show message
										findViewById(R.id.nearby_custom_progress).setVisibility(View.VISIBLE);
										View v = findViewById(R.id.nearby_custom_progress);
										TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
										textPreparing.setText("Nothing found");
										v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);

										AnimationTween.animateViewPosition(findViewById(R.id.nearby_custom_progress), NearByNativeRestaurantListNew.this);

									}


								} else if (resturentList != null) {
									// Check for all possible status

									// Successfully got places details
									if (resturentList != null) {
										// loop through each place

										//search suggestion
										ArrayList<SearchResturent> searchRestList = new ArrayList<SearchResturent>();


										for (Restaurent p : resturentList) {

											HashMap<String, String> map = new HashMap<String, String>();
	        								/*map.put("Id", p.Id);								
	        								map.put("RestName", p.RestName);
	        								map.put("RestLoc", p.RestLoc+", "+p.RestDistName);*/

											map.put("Id", p.Id);
											map.put("RestName", p.RestName);
											map.put("Rating", p.Rating);


											//distance
											double distance = 0;
											try {
												p.setDistance(getDistanceKM(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), new LatLng(Double.parseDouble(p.latitude), Double.parseDouble(p.longitude))));

											} catch (Exception e) {
												// TODO: handle exception
											}

											String distancetxt = "";

											if (distance <= 0)
												distancetxt = "";
											else {
												map.put("km", distance + "");
												distancetxt = distance + " km(approx), ";

											}


											map.put("DisFrom", p.RestLoc + ", " + p.RestDistName);


											map.put("DisAmount", "" + CF.openOrClose(p.Open, p.Close));
											map.put("IMG", p.IMG);


											totalPage = Integer.parseInt(p.PageNo);


											// adding HashMap to ArrayList
											resturantListItems.add(map);

											//search suggestion
											searchRestList.add(new SearchResturent(p.Id, p.RestName, null, p.RestLoc, null, null, null));
										}
										//search suggestion
										addDataToDB(searchRestList);

										NearByRestaurantAdapter adapter = new NearByRestaurantAdapter(NearByNativeRestaurantListNew.this, resturentList);
										//ListAdapterRestaurantNearByNative adapter= new ListAdapterRestaurantNearByNative(NearByNativeRestaurantList.this, resturantListItems, new String[]{"RestName","DisFrom","Rating","DisAmount","IMG", "km"});


										// Adding data into listview
										lv.setAdapter(adapter);
										adapter.notifyDataSetChanged();
										currentPage++;

										lv.setOnScrollListener(new OnScrollListener() {

											@Override
											public void onScrollStateChanged(AbsListView view, int scrollState) {
												// TODO Auto-generated method stub

											}

											@Override
											public void onScroll(AbsListView view, int firstVisibleItem,
																 int visibleItemCount, int totalItemCount) {
												// TODO Auto-generated method stub
												boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;

												if (loadMore && currentPage < totalPage) {
													Toast.makeText(getApplicationContext(), "Loading More", Toast.LENGTH_SHORT).show();

													loadData();

												}
											}
										});


									} else if (resturentList == null) {
										//show message
	        						/*	findViewById(R.id.nearby_custom_progress).setVisibility(View.VISIBLE);
	        							View v = findViewById(R.id.nearby_custom_progress);
	        							TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
	        							textPreparing.setText("Nothing found.");
	        							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
	        							AnimationTween.animateViewPosition(findViewById(R.id.nearby_custom_progress), NearByNativeRestaurantList.this);
	        							
	        							showPopUp();*/

										finish();
										googlePlacesNearby();

									}
								}
							}

						});


					} catch (Exception e) {


						e.printStackTrace();
					}
				}
			},
			new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					error.printStackTrace();
				}
			}
	) {
		@Override
		protected Map<String, String> getParams() {
			Map<String, String> params = new HashMap<String, String>();
			// the POST parameters:
			params.put("lat", "" + myLocation.latitude);
			params.put("lang", "" + myLocation.longitude);
			params.put("rad", "" + radius);
			return params;
		}
	};


	private void showPopUp() {

		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
		// helpBuilder.setTitle("Pop Up");
		helpBuilder.setMessage("No Restaurant found in Let's Eat. Try Google Places.");
		helpBuilder.setPositiveButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {

					}
				});

		helpBuilder.setNegativeButton("Yes",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						googlePlacesNearby();
					}
				});

		// Remember, create doesn't show the dialog
		AlertDialog helpDialog = helpBuilder.create();
		helpDialog.show();

	}

	void googlePlacesNearby() {
		Intent i = new Intent(NearByNativeRestaurantListNew.this, NearByplaceListView.class);
		startActivity(i);
		finish();
	}


	public double getDistanceKM(LatLng fromPosition, LatLng toPosition) {
		Location locationA = new Location("point A");

		locationA.setLatitude(fromPosition.latitude);

		locationA.setLongitude(fromPosition.longitude);

		Location locationB = new Location("point B");

		locationB.setLatitude(toPosition.latitude);

		locationB.setLongitude(toPosition.longitude);

		double value = (locationA.distanceTo(locationB) / 1000);
		//double rounded = (double) Math.round(value,2 ) ;
		Log.i("kilometer before format", value + "");

		DecimalFormat df = new DecimalFormat("###0.00");
		double rounded = Double.parseDouble(df.format(value));
		Log.i("kilometer after format", rounded + "");

		return rounded;

	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		final LocationSettingsStates states = LocationSettingsStates.fromIntent(intent);
		switch (requestCode) {
			case REQUEST_CHECK_SETTINGS:
				switch (resultCode) {
					case Activity.RESULT_OK:
						// All required changes were successfully made
						analyticsSendEvent(this.getClass().getSimpleName(), StaticObjects.ANA_EV_CAT_ACTION, "Nearby");
						// connect GoogleAPIClient.
						mGoogleApiClient.connect();
						/*getMyLocation();
						loadData();*/
						break;
					case Activity.RESULT_CANCELED:
						// The user was asked to change settings, but chose not to
						msgLocationFiled();

						break;
					default:
						break;
				}
				break;
		}
	}


	@Override
	public void onRequestPermissionsResult(int requestCode,
										   String permissions[], int[] grantResults) {
		switch (requestCode) {
			case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
				// If request is cancelled, the result arrays are empty.
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

					if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
						// TODO: Consider calling
						//    ActivityCompat#requestPermissions
						// here to request the missing permissions, and then overriding
						//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
						//                                          int[] grantResults)
						// to handle the case where the user grants the permission. See the documentation
						// for ActivityCompat#requestPermissions for more details.
						return;
					}
					LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, createLocationRequest(), this);

				} else {

					// permission denied, boo! Disable the
					// functionality that depends on this permission.

					Toast.makeText(this, "Permission denied! functionality depends on this permission.",Toast.LENGTH_LONG).show();
				}
				return;
			}

			// other 'case' lines to check for other
			// permissions this app might request
		}
	}

	/*@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == LOCATION_ACCESS_SETTING_REQUEST && new  GPSTracker(this).canGetLocation() && callForLocation ) {

			//analytic send button event
			analyticsSendEvent(this.getClass().getSimpleName(), StaticObjects.ANA_EV_CAT_ACTION, "Nearby");
			getMyLocation();
			loadData();


		}
		callForLocation=false;
	}*/


	private void msgLocation(){

		View v = findViewById(R.id.nearby_custom_progress);
		v.setVisibility(View.VISIBLE);
		TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
		textPreparing.setText("Loading Location Info");
		v.findViewById(R.id.progressBarPreparing).setVisibility(View.VISIBLE);
	}

	private void msgLocationFiled(){

		View v = findViewById(R.id.nearby_custom_progress);
		v.setVisibility(View.VISIBLE);
		TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
		textPreparing.setText("Failed To Load Location Info. Check your Location Setting. ");
		v.findViewById(R.id.progressBarPreparing).setVisibility(View.INVISIBLE);
	}

	private void msgReset(){

		View v = findViewById(R.id.nearby_custom_progress);
		v.setVisibility(View.VISIBLE);
		TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
		textPreparing.setText("Preparing...");
		v.findViewById(R.id.progressBarPreparing).setVisibility(View.VISIBLE);
	}

	
}
