package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.Location;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.mcc.letseat.HomeT;
import com.mcc.letseat.MyProgressDialog;
import com.mcc.letseat.R;
import com.mcc.libs.ActivityWithSliding;
import com.mcc.libs.AnimationTween;
import com.mcc.user.SavedPrefernce;

/**
 * 
 * @author Arif
 * List of location under a city e.i. Dhaka>Banani
 */
public class LocatonListView extends ActivityWithSliding implements
		ActionBar.OnNavigationListener {

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Google Places
	FoodyResturent foodyResturent;

	// Places List
	ArrayList<Location> locationList;

	// Button
	ImageView btnShowOnMap;

	// Progress dialog
	MyProgressDialog pDialog;

	// Places Listview
	ListView lv;

	// ListItems data
	ArrayList<HashMap<String, String>> locationListItems = new ArrayList<HashMap<String, String>>();

	

	ImageView searchAny;

	String searchKeywords;
	
	String DisId, DisName;
	
	RelativeLayout relativeLayout;		
	//View searchview ;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choose_location);
		TextView txtChooseLoc=(TextView)findViewById(R.id.txtChooseLoc);
		txtChooseLoc.setText("Choose Location");
		Typeface tf = Typeface.createFromAsset(getAssets(), StaticObjects.fontPath_SEGOEUI);	
		txtChooseLoc.setTypeface(tf);
		
		 relativeLayout=(RelativeLayout)findViewById(R.id.cat_root_layout);		
		/* searchview = getLayoutInflater().inflate(R.layout.search,  relativeLayout, false);
		 searchview.setTag(searchview);*/

		// Getting listview
		lv = (ListView) findViewById(R.id.list_cat);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				Location location = (Location) locationList.get(position);
				
				
					String nev_type=getIntent().getStringExtra(StaticObjects.NEV_TYPE);
				
					
					
					
					
					if(nev_type!=null && nev_type.equals(StaticObjects.NEV_TYPE_CHOOSE_RES_PHOTO_UP)){
						Intent in = new Intent(LocatonListView.this, ResturentListView.class);
						in.putExtra("LocId", location.LocId);
						in.putExtra("Lat", location.Lat);
						in.putExtra("Lng", location.Lng);
						in.putExtra("LocName", location.LocName);						
						in.putExtra(StaticObjects.NEV_TYPE, nev_type);
						startActivity(in);
						finish();
						
					}else if(nev_type!=null && nev_type.equals(StaticObjects.NEV_TYPE_CHOOSE_LOCTION_N_SAVE)){
						
						//save data 
						 SavedPrefernce savedPrefernce=new SavedPrefernce(LocatonListView.this);
						 savedPrefernce.setDistrictID(DisId );
						 savedPrefernce.setDistrict(DisName);
						 
					     savedPrefernce.setLocationId(location.LocId);
					     savedPrefernce.setLocationName(location.LocName);
					     
					     //analytics
					     analyticsSendScreenView("Choose Location "+DisName);
						
						Intent in = new Intent(LocatonListView.this, HomeT.class);
						in.putExtra("LocId", location.LocId);
						in.putExtra("Lat", location.Lat);
						in.putExtra("Lng", location.Lng);
						in.putExtra("LocName", location.LocName);	
						in.putExtra(StaticObjects.NEV_TYPE, nev_type);
						startActivity(in);
						finish();
					}else if(nev_type!=null && nev_type.equals(StaticObjects.NEV_TYPE_CHOOSE_LOCTION)){
						
						//analytics
					     analyticsSendScreenView("Choose Location "+DisName);
					     
						SearchAdvResturentListView.LocName=location.LocName;
						SearchAdvResturentListView.LocId=location.LocId;
						finish();
					}
					
					
					

				
			}
		});

		
		
		DisId=getIntent().getStringExtra("DisId");
		DisName=getIntent().getStringExtra("DisName");
		
		
		
		new LoadPlaces().execute();

		//handleIntent(getIntent());

	}

	private void handleIntent(Intent intent) {
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			searchKeywords = null;
			searchKeywords = intent.getStringExtra(SearchManager.QUERY);
			new LoadPlaces().execute();

		} else if (intent.getStringExtra("searchKeyWord_home") != null) {
			searchKeywords = null;
			searchKeywords = intent.getStringExtra("searchKeyWord_home");
			new LoadPlaces().execute();
		}

	}

	@Override
	protected void onNewIntent(Intent intent) {
		setIntent(intent);
		//handleIntent(intent);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
	
		case R.id.action_search:
			// search action
			RelativeLayout relativeLayout=(RelativeLayout)findViewById(R.id.cat_root_layout);		
			addSearchLayout(relativeLayout);			
			return true;
		
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	
	@Override
	protected void onStart() {	
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}
	
	@Override
	protected void onDestroy() {	
		super.onDestroy();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}
	

	

	

	class LoadPlaces extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			findViewById(R.id.choose_loc_custom_progress).setVisibility(View.VISIBLE);
			
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {
				locationList = foodyResturent.getLocation(DisId, LocatonListView.this);
				

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			//pDialog.dismiss();
			//findViewById(R.id.choose_loc_custom_progress).setVisibility(View.GONE);
			AnimationTween.animateView(findViewById(R.id.choose_loc_custom_progress), LocatonListView.this);
			AnimationTween.animateViewPosition(lv, LocatonListView.this);
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status
					
					if (locationList == null || locationList.size() <= 0) {

						findViewById(R.id.choose_loc_custom_progress).setVisibility(View.VISIBLE);
						View v = findViewById(R.id.choose_loc_custom_progress);
						TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
						textPreparing.setText("Nothing found.");
						v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
						AnimationTween.animateViewPosition(findViewById(R.id.choose_loc_custom_progress), LocatonListView.this);
	 
					} else if (locationList != null) {
						// Check for all possible status

						// Successfully got places details
						if (locationList != null) {
							// loop through each place
							for (Location p : locationList) {

								HashMap<String, String> map = new HashMap<String, String>();
								map.put("DisId", p.DisId);
								// Place name
								map.put("LocId", p.LocId);
								map.put("LocName", p.LocName);
								map.put("Lat", p.Lat);
								map.put("Lng", p.Lng);

								System.out
										.println(">>>>>>>>>>test>>>>>>>>>>>>>>>>>> "
												+ p.LocName);

								// adding HashMap to ArrayList
								locationListItems.add(map);
							}
							// list adapter
							MyListAdapterSingle adapter = new MyListAdapterSingle(
									LocatonListView.this,
									locationListItems,"LocName");

							// Adding data into listview
							lv.setAdapter(adapter);
						}

						else if (locationList == null) {
							findViewById(R.id.choose_loc_custom_progress).setVisibility(View.VISIBLE);
							View v = findViewById(R.id.choose_loc_custom_progress);
							TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
							textPreparing.setText("Nothing found.");
							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);

						}
					}
				}

			});

		}

	}
	
	

	@Override
	public boolean onNavigationItemSelected(int arg0, long arg1) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	private void showPopUp() {

		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
		// helpBuilder.setTitle("Pop Up");
		helpBuilder.setMessage("Nothing Found. Want to request this suggestion?");
		helpBuilder.setPositiveButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {

					}
				});

		helpBuilder.setNegativeButton("Yes",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						Intent in = new Intent(
								getApplicationContext(),
								SuggetionRequest.class);
						in.putExtra("keyword", searchKeywords);

						startActivity(in);
					}
				});

		// Remember, create doesn't show the dialog
		AlertDialog helpDialog = helpBuilder.create();
		helpDialog.show();

	}

}
