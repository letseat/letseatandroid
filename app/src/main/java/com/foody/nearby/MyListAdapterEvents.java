package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.foody.AppData.StaticObjects;
import com.foody.jsondata.Event;
import com.foody.jsondata.Offer;
import com.mcc.letseat.R;
import com.squareup.picasso.Picasso;

/**
 * 
 * @author Arif
 * Custom data adapter for Event List
 */

public class MyListAdapterEvents extends ArrayAdapter<Event> {
	
	ArrayList<Event> eventList;
	Context c;
	String listTag[];
	
	public MyListAdapterEvents(Context c, ArrayList<Event> eventList){
		super(c, 0 , eventList);
		this.eventList = eventList;
		this.c = c;
		this.listTag=listTag;
	}






	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	private int lastPosition = -1;

	@Override
	public View getView(int pos, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		
		View v;
		v = arg1;
		LayoutInflater li = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		v = li.inflate(R.layout.list_item_event, null);
		
		final Event event= getItem(pos);
		
		TextView eventName= (TextView)v.findViewById(R.id.textEventName);	
		Typeface tf = Typeface.createFromAsset(c.getAssets(), StaticObjects.fontPath_SEGOEUI);	
		eventName.setTypeface(tf);		
		eventName.setText(event.EVNAME);
		
		TextView eventDatetime= (TextView)v.findViewById(R.id.textEventDateTime);	
		Typeface tfLight = Typeface.createFromAsset(c.getAssets(), StaticObjects.fontPath_SEGOEUI_light);	
		eventDatetime.setTypeface(tfLight);
		String stdate=event.STARTDATE.replace(" ","\n");
		eventDatetime.setText(stdate);
		
		
		
		ImageView imageView=(ImageView) v.findViewById(R.id.imageView_event);
		
		if(event.IMG!=null &&  !event.IMG.isEmpty()){
			Picasso.with(c)
		    .load(event.IMG)
		    .placeholder(R.drawable.sample_res_photo)
		    .error(R.drawable.sample_res_photo)
		    .into(imageView);
		}


		Button buttonEventDetails=(Button) v.findViewById(R.id.buttonEventDetails);
		buttonEventDetails.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//analytics
				//analyticsSendScreenView("User Choose event: "+r.EVNAME);

				//call event details activity

				Intent in = new Intent(c,FeaturedRestaurantListView.class);
				in.putExtra( "event" , event);

				if(event.STARTDATE.equals(event.ENDDATE))
					in.putExtra("date", event.STARTDATE);
				else
					in.putExtra("date", event.STARTDATE+" to "+event.ENDDATE );

				c.startActivity(in);
			}
		});
		
		
		//apply animation on current view
		 Animation animation = AnimationUtils.loadAnimation(c, (pos > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
		 v.startAnimation(animation);
		 lastPosition = pos;
		
		
		return v;
	}

}
