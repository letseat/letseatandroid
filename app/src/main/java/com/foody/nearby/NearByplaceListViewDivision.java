package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;

import org.w3c.dom.Document;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mcc.letseat.MyProgressDialog;
import com.mcc.letseat.R;
import com.mcc.libs.ActivityWithSliding;


/**
 * Custom ListView for nearby restaurant in a particular division ffrom Goole Places API 
 */
public class NearByplaceListViewDivision extends ActivityWithSliding implements
ActionBar.OnNavigationListener {

	// flag for Internet connection status
	Boolean isInternetPresent = false;

	// Connection detector class
	ConnectionDetector cd;
	
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Google Places
	GooglePlaces googlePlaces;

	// Places List
	PlacesList nearPlaces;

	// GPS Location
	GPSTracker gps;

	// Button
	ImageView btnShowOnMap;

	// Progress dialog
	MyProgressDialog pDialog;
	
	// Places Listview
	ListView lv;
	MyListAdapter adapter ;
	
	// ListItems data
	ArrayList<HashMap<String, String>> placesListItems = new ArrayList<HashMap<String,String>>();
	

	LatLng myLocation=null;
	
	////admob
	AdView adView=null;

	ImageView searchAny;
	
	double locLat, locLng;
	
	// KEY Strings
	public static String KEY_REFERENCE = "reference"; // id of the place
	public static String KEY_NAME = "name"; // name of the place
	public static String KEY_VICINITY = "vicinity"; // Place area name
	public static String KEY_DISTANCE = "distance"; // Place area name

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_main);
		
		cd = new ConnectionDetector(getApplicationContext());

		// Check if Internet present
		isInternetPresent = cd.isConnectingToInternet();
		if (!isInternetPresent) {
			// Internet Connection is not present
			alert.showAlertDialog(NearByplaceListViewDivision.this, "Internet Connection Error",
					"Please connect to working Internet connection", false);
			// stop executing code by return
			return;
		}

		// creating GPS Class object
		gps = new GPSTracker(this);
		locLat=Double.parseDouble(getIntent().getStringExtra("Lat").trim());
		locLng= Double.parseDouble(getIntent().getStringExtra("Lng").trim());		
		myLocation=new LatLng(gps.latitude , gps.longitude);
		System.out.println(locLat+" : "+locLng);
		
		TextView txtTitleNearBy=(TextView)findViewById(R.id.txtTitleNearBy);
		txtTitleNearBy.setText("Restaurants in "+getIntent().getStringExtra("LocName"));
		
		
		
		
/*
		// check if GPS location can get
		if (gps.canGetLocation()) {
			Log.d("Your Location", "latitude:" + gps.getLatitude() + ", longitude: " + gps.getLongitude());
			myLocation=new LatLng( gps.getLatitude(), gps.getLongitude());
		} else {
			
			gps.showSettingsAlert();
			// stop executing code by return
			return;
		}*/
		
		/*gps.latitude=Double.parseDouble(getIntent().getStringExtra("lat"));
		gps.longitude= Double.parseDouble(getIntent().getStringExtra("lng"));*/
		
		

		// Getting listview
		lv = (ListView) findViewById(R.id.list);
		
		//search through webview
		/*searchAny=(ImageView)findViewById(R.id.searchAny);		
		searchAny.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Intent i=new Intent(NearByplaceListView.this,Main.class);
				i.putExtra("url", getString(R.string.searchAny));
				startActivity(i);
				
			}
		});*/

		// calling background Async task to load Google Places
		// After getting places from Google all the data is shown in listview
		new LoadPlaces().execute();
		
		
		
		lv.setOnItemClickListener(new OnItemClickListener() {
 
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                    int position, long id) {
            	
					/*if((i+1)%2==1){
						View v = lv.getChildAt(i);
					    v.setBackgroundColor(Color.DKGRAY);
					    System.out.println("---------------"+i);
					}*/
//					View v=null;
//					for (int i = 0; i < lv.getCount(); i++) {
//				        v = lv.getChildAt(i);
//				        //v.setBackgroundColor(Color.DKGRAY);
//				        LinearLayout layout=(LinearLayout) v.findViewById(R.id.linearLayout1);
//				        layout.setBackgroundColor(Color.DKGRAY);
//				    }
				
            
            	
//            	view.setBackgroundColor(Color.DKGRAY);
            	//
            	// getting values from selected ListItem
//                String reference = ((TextView) view.findViewById(R.id.reference)).getText().toString();
                
                // Starting new intent
                Intent in = new Intent(getApplicationContext(),
                		SinglePlaceActivity.class);
                
                // Sending place refrence id to single place activity
                // place refrence id used to get "Place full details"
                
                in.putExtra(KEY_REFERENCE, adapter.getItem(position));
                in.putExtra("lat",myLocation.latitude );
                in.putExtra("lng",myLocation.longitude );  
                startActivity(in);
            }
        });
		
		
		adView = (AdView)this.findViewById(R.id.adView);
		
		AdRequest adRequest = new AdRequest.Builder()
		.build();
	    
		adView.loadAd(adRequest);
	  
		adView.setAdListener(new AdListener() {
		  public void onAdLoaded() {}
		  public void onAdFailedToLoad(int errorcode) {
			  System.out.println("Error:"+errorcode);
		  }
		});
		
		
	}
	
	

@Override
public void onPause() {
	super.onPause();
	
	if(adView!=null)
  adView.pause();
	
  
}

@Override
public void onResume() {
  super.onResume();
  
  if(adView!=null)
  adView.resume();
}

@Override
public void onDestroy() {
	 if(adView!=null)
  adView.destroy();
  super.onDestroy();
}

	/**
	 * Background Async Task to Load Google places
	 * */
	class LoadPlaces extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new MyProgressDialog(NearByplaceListViewDivision.this);
			/*pDialog.setMessage(Html.fromHtml("<b>Loading...</b>"));
			pDialog.setIndeterminate(false);*/
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			googlePlaces = new GooglePlaces();
			
			try {
				// Separeate your place types by PIPE symbol "|"
				// If you want all types places make it as null
				// Check list of types supported by google
				// 
				String types = "cafe|restaurant"; // Listing places only cafes, restaurants
				
				// Radius in meters - increase this value if you don't find any places
				double radius = 1000; // 1000 meters 
				
				// get nearest places 
				nearPlaces = googlePlaces.search(locLat,locLng, radius, types);
				
				/*
				 * 
				 * ;
		gps.longitude= ;*/
				

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * and show the data in UI
		 * Always use runOnUiThread(new Runnable()) to update UI from background
		 * thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed Places into LISTVIEW
					 * */
					// Get json response status
					String status=null;
					if(nearPlaces !=null){
						status = nearPlaces.status != null? nearPlaces.status:null ;
					}
					
					if(nearPlaces ==null || nearPlaces.status==null){

						alert.showAlertDialog(NearByplaceListViewDivision.this, getString(R.string.app_name),
								"Nothing found.",
								false);
					}
					else if(status!=null){
							// Check for all possible status
							if(status.equals("OK")){
								// Successfully got places details
								if (nearPlaces.results != null) {
									// loop through each place
									for (Place p : nearPlaces.results) {
										HashMap<String, String> map = new HashMap<String, String>();
										
										// Place reference won't display in listview - it will be hidden
										// Place reference is used to get "place full details"
										map.put(KEY_REFERENCE, p.reference);
										
										//distance
										double distance=getDistanceKM(new LatLng(gps.latitude, gps.longitude),new LatLng(p.geometry.location.lat, p.geometry.location.lng));
										String distancetxt=distance+"km away";
										
										
										// Place name
										map.put(KEY_NAME, p.name);
										map.put(KEY_DISTANCE, p.vicinity);
										
										
										
										// adding HashMap to ArrayList
										placesListItems.add(map);
									}
									// list adapter
//									ListAdapter adapter = new SimpleAdapter(NearByplaceListView.this, placesListItems,
//							                R.layout.n_list_item,
//							                new String[] { KEY_REFERENCE, KEY_NAME,KEY_DISTANCE}, new int[] {
//							                        R.id.reference, R.id.name,R.id.distance });
									
									
									adapter = new MyListAdapter(NearByplaceListViewDivision.this, placesListItems);
									
									// Adding data into listview
									lv.setAdapter(adapter);
									
									
									
								}
							}
							else if(status.equals("ZERO_RESULTS")){
								// Zero results found
								alert.showAlertDialog(NearByplaceListViewDivision.this, getString(R.string.app_name),
										"Sorry no places found. Try to change the types of places",
										false);
							}
							else if(status.equals("UNKNOWN_ERROR"))
							{
								alert.showAlertDialog(NearByplaceListViewDivision.this, "Places Error",
										"Sorry unknown error occured.",
										false);
							}
							else if(status.equals("OVER_QUERY_LIMIT"))
							{
								alert.showAlertDialog(NearByplaceListViewDivision.this, "Places Error",
										"Sorry query limit to google places is reached",
										false);
							}
							else if(status.equals("REQUEST_DENIED"))
							{
								alert.showAlertDialog(NearByplaceListViewDivision.this, "Places Error",
										"Sorry error occured. Request is denied",
										false);
							}
							else if(status.equals("INVALID_REQUEST"))
							{
								alert.showAlertDialog(NearByplaceListViewDivision.this, "Places Error",
										"Sorry error occured. Invalid Request",
										false);
							}
							else
							{
								alert.showAlertDialog(NearByplaceListViewDivision.this, "Places Error",
										"Sorry error occured.",
										false);
							}
						}
				}
				
				
			});

		}

	}
	
	public double getDistance(LatLng fromPosition, LatLng toPosition){
		GMapV2Direction md = new GMapV2Direction();

		Document doc = md.getDocument(fromPosition, toPosition, GMapV2Direction.MODE_DRIVING);

		ArrayList<LatLng> directionPoint = md.getDirection(doc);

		PolylineOptions rectLine = new PolylineOptions().width(3).color(Color.RED);


		for(int i = 0 ; i < directionPoint.size() ; i++) {          

		rectLine.add(directionPoint.get(i));

		}

		return  Double.valueOf(md.getDistanceValue(doc))/1000;

/*		dis.setText("Distance: "+a +" km (approx)");

		mMap.addPolyline(rectLine);*/
	}
	
	public double getDistanceKM(LatLng fromPosition, LatLng toPosition){
		Location locationA = new Location("point A");

		locationA.setLatitude(fromPosition.latitude);

		locationA.setLongitude(fromPosition.longitude);

		Location locationB = new Location("point B");

		locationB.setLatitude(toPosition.latitude);

		locationB.setLongitude(toPosition.longitude);
		
		double value = (locationA.distanceTo(locationB)/1000);
		double rounded = (double) Math.round(value * 100) / 100;

		return rounded;
		
		
		
	}

	
	



	@Override
	public boolean onNavigationItemSelected(int arg0, long arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	

}
