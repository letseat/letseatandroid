package com.foody.nearby;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.animation.Animator;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils.TruncateAt;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.foody.AppData.StaticObjects;
import com.foody.jsondata.FoodyResturent;
import com.foody.jsondata.LetsEatRequest;
import com.foody.jsondata.Restaurent;
import com.foody.jsondata.SearchResturent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.mcc.letseat.CapturePhotoActivity;
import com.mcc.letseat.R;
import com.mcc.letseat.Adapter.RestaurantAdapter;
import com.mcc.letseat.tab.TabMainActivity;
import com.mcc.libs.ActivityWithSliding;
import com.mcc.libs.AnimationTween;
import com.mcc.libs.CF;
import com.mcc.libs.TouchEffectListener;
import com.mcc.user.SavedPrefernce;

/**
 * Custom Restaurant ListView with image and work hour info
 */
public class ResturentListViewImage extends ActivityWithSliding implements
		ActionBar.OnNavigationListener {

	
	
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Google Places
	FoodyResturent foodyResturent;

	// restaurant List
	ArrayList<Restaurent> resturentList;


	// Button
	ImageView btnShowOnMap;

	

	// Places Listview
	ListView lv;

	// ListItems data
	ArrayList<HashMap<String, String>> resturantListItems = new ArrayList<HashMap<String, String>>();

	// //admob
	AdView adView = null;

	ImageView searchAny;

	String searchKeywords;
	
	//post value
	String LocId;
	int currentPage=0;
	int totalPage=1;
	
	
	View l_sort_filter_rest ;
	TextView txtTitleNearBy;
	
	private int totalItemCountForScroll;
	
	private boolean isFoooterAdded;
	private  View footerView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_rest_list);
		findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);
		
		
		// Getting listview
		lv = (ListView) findViewById(R.id.list);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				TouchEffectListener tel1=new TouchEffectListener("#FCFCFC", "#ced2d8");
				TouchEffectListener tel2=new TouchEffectListener("#FFFFFF", "#ced2d8");
				
				

				//Restaurent resturent = (Restaurent) resturentList.get(position);
				HashMap<String, String> map= resturantListItems.get(position);
				//String resid=resturantListItems.get(position).get("Id");//map.get("Id");
				//String RestName=resturantListItems.get(position).get("RestName");//map.get("Id");
				
				String resid =((Restaurent)lv.getItemAtPosition(position)).Id;
				String RestName=((Restaurent)lv.getItemAtPosition(position)).RestName;
				
				
				
				//Restaurent r=resturentList.get(position);
				
				//restaurentId= resturent.Id;
				
				//Intent in = new Intent(getApplicationContext(),RestaurantTabActivity.class);
				String nev_type=getIntent().getStringExtra(StaticObjects.NEV_TYPE);
				
				if(nev_type!=null && nev_type.equals(StaticObjects.NEV_TYPE_CHOOSE_RES_PHOTO_UP)){
					Intent in=new Intent();
					in.putExtra( "Id" , resid);	
					in.putExtra( "RestName" , RestName);	
					setResult(RESULT_OK, in);
					CapturePhotoActivity.resid=resid;
					CapturePhotoActivity.resname=RestName;
					
					finish();
				}else{
					//Intent in = new Intent(getApplicationContext(),RestaurantTabActivity.class);
					Intent in = new Intent(getApplicationContext(),TabMainActivity.class);
					in.putExtra( "Id" , resid);				
					/*.putExtra( "latitude" , r.latitude)
					.putExtra( "longitude" , r.longitude)
					.putExtra( "RestName" , r.RestName)
					.putExtra( "address" , r.RestLoc);*/
					startActivity(in);
				}
								
				
				
				
			}
		});
		
		lv.setVisibility(View.INVISIBLE);
		l_sort_filter_rest = findViewById(R.id.l_sort_filter_rest);
		l_sort_filter_rest.setVisibility(View.INVISIBLE);
		
		txtTitleNearBy=(TextView)l_sort_filter_rest.findViewById(R.id.txtOfferLocation);
		txtTitleNearBy.setText("Restaurants at "+new SavedPrefernce(this).getLocationName()+" and Nearby Areas" );
		
		
		//set up sort and filter
		// filter list
		ArrayList<String> filterBylist = new ArrayList<String>();
		filterBylist.add("Clear Filter");
		filterBylist.add("Open Now");
		filterBylist.add("Deals Available");
		
		//sort
		ArrayList<String> sortBylist = new ArrayList<String>();
		sortBylist.add("Name (A-Z)");
		sortBylist.add("Name (Z-A)");
		sortBylist.add("Lowest Cost First");
		sortBylist.add("Highest Cost First");
		sortBylist.add("Highest Capacity First");
		sortBylist.add("Lowest Capacity First");
		sortBylist.add("Highest Rating First");
		
		Typeface tfLight= StaticObjects.gettfLight(this);
		setFilterSortSpinner(l_sort_filter_rest, tfLight, filterBylist, sortBylist);
		

		adView = (AdView) this.findViewById(R.id.adView);

		AdRequest adRequest = new AdRequest.Builder().build();

		adView.loadAd(adRequest);

		adView.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}

			public void onAdFailedToLoad(int errorcode) {
				System.out.println("Error:" + errorcode);
			}

		});
		
		LocId=getIntent().getStringExtra("LocId");
		loadData();
		
		
		
		
		//txtTitleNearBy.setVisibility(View.INVISIBLE);
		//findViewById(R.id.ll_search_hint).setVisibility(View.INVISIBLE);

		setHomeButton(this);
	}
	
	
	
	
	/*private void loadData(View v) {
		showLoader();
		//new LoadPlaces().execute();
		v.findViewById(R.id.seeMoreProgressBar).setVisibility(View.INVISIBLE);
		v.findViewById(R.id.txtseeMore).setVisibility(View.VISIBLE);
		Volley.newRequestQueue(this).add(postRequest);
	}*/

	private void loadData() {
		showLoader();
		//new LoadPlaces().execute();

		Volley.newRequestQueue(this).add(postRequest);
	}




	private void showLoader() {
		
		lv.setOnScrollListener(null);		
		findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);
		
	}




	private void setFilterSortSpinner(View spinnerHolder, final Typeface tfLight, ArrayList<String> filterBylist, ArrayList<String> sortBylist){
		Spinner spinnerFilter = (Spinner) spinnerHolder.findViewById(R.id.spinner_offer);		
		ArrayAdapter<String> adapter_offer = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, filterBylist) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {

				View v = super.getView(position, convertView, parent);
				((TextView) v).setTypeface(tfLight);
				((TextView) v).setEllipsize(TruncateAt.END);

				return v;
			}

			@Override
			public View getDropDownView(int position, View convertView,
					ViewGroup parent) {

				View v = super.getDropDownView(position, convertView, parent);
				((TextView) v).setTypeface(tfLight);
				return v;
			}

		};
		// Specify the layout to use when the list of choices appears
		adapter_offer.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinnerFilter.setAdapter(adapter_offer);
		spinnerFilter.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
				if(pos ==0 && parent != null){
					if( parent.getChildAt(0) instanceof TextView)
					((TextView) parent.getChildAt(0)).setText("Not Filtered");
					
				}
				
				
				lv.setTextFilterEnabled(true);
				RestaurantAdapter adapter = null;
				
				if(isFoooterAdded)
					 adapter=((RestaurantAdapter)((HeaderViewListAdapter)lv.getAdapter()).getWrappedAdapter());
				else
					adapter =(RestaurantAdapter)lv.getAdapter();
				
				if(adapter ==null){return;}	
				adapter.getFilter().filter(""+pos);
				lv.setSelectionAfterHeaderView();

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});
		
		
		// offer_provider_list.add("Others");

		Spinner spinnerSort = (Spinner) spinnerHolder.findViewById(R.id.spinner_sort);		
		ArrayAdapter<String> adapter_sort = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, sortBylist) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {

				View v = super.getView(position, convertView, parent);
				((TextView) v).setTypeface(tfLight);
				((TextView) v).setEllipsize(TruncateAt.END);

				return v;
			}

			@Override
			public View getDropDownView(int position, View convertView,
					ViewGroup parent) {

				View v = super.getDropDownView(position, convertView, parent);
				((TextView) v).setTypeface(tfLight);
				return v;
			}

		};
		// Specify the layout to use when the list of choices appears
		adapter_sort.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinnerSort.setAdapter(adapter_sort);
		spinnerSort.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,int pos, long id) {			
				//CuisineRestAdapter adapter= (CuisineRestAdapter)lv.getAdapter();
				
				RestaurantAdapter adapter = null;
				
				if(isFoooterAdded)
					 adapter=((RestaurantAdapter)((HeaderViewListAdapter)lv.getAdapter()).getWrappedAdapter());
				else
					adapter =(RestaurantAdapter)lv.getAdapter();
				
				if(adapter ==null){return;}								
				adapter.setSortBY(pos);		

				//lv.scrollBy(0, 0);
				lv.setSelectionAfterHeaderView();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});
	}
	
	
	
	
	
	
	
	//========

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
	
		case R.id.action_search:
			// search action
			RelativeLayout relativeLayout=(RelativeLayout)findViewById(R.id.rest_list_root_layout);		
			addSearchLayout(relativeLayout);			
			return true;
		
		default:
			return super.onOptionsItemSelected(item);
		}

	}
	
	
	
	@Override
	protected void onStart() {		
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	public void onPause() {
		super.onPause();

		if (adView != null)
			adView.pause();

	}

	@Override
	public void onResume() {
		super.onResume();

		if (adView != null)
			adView.resume();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		
		if (adView != null)
		{	adView.destroy();}
		
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

	/*class LoadPlaces extends AsyncTask<String, String, String> {
		
		ArrayList<Restaurent> resturentListTemp;

		
		*//**
		 * Before starting background thread Show Progress Dialog
		 * *//*
		@Override
		protected void onPreExecute() {
			lv.setOnScrollListener(null);
			super.onPreExecute();
			
			findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);
		}

		*//**
		 * getting Places JSON
		 * *//*
		protected String doInBackground(String... args) {
			// creating Places class object
			foodyResturent = new FoodyResturent();

			try {

				//String types = "LocId=" + LocId+"&Page="+currentPage;				
				resturentListTemp = foodyResturent.getRsturent(FoodyResturent.TYPE_LOAD_LOC,LocId, currentPage, ResturentListViewImage.this);
				

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		*//**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **//*
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			//pDialog.dismiss();
			//findViewById(R.id.main_custom_progress).setVisibility(View.GONE);
			final Animator.AnimatorListener animatorListener= new Animator.AnimatorListener() {
				
				@Override
				public void onAnimationStart(Animator animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationRepeat(Animator animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animator animation) {
					// TODO Auto-generated method stub
					lv.setVisibility(View.VISIBLE);
					AnimationTween.animateViewPosition(lv, ResturentListViewImage.this);
				}
				
				@Override
				public void onAnimationCancel(Animator animation) {
					// TODO Auto-generated method stub
					
				}
			};
			
			Animator.AnimatorListener animatorListener2= new Animator.AnimatorListener() {
				
				@Override
				public void onAnimationStart(Animator animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationRepeat(Animator animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animator animation) {
					// TODO Auto-generated method stub
					//txtTitleNearBy.setVisibility(View.VISIBLE);
					
					l_sort_filter_rest.setVisibility(View.VISIBLE);					
					AnimationTween.animateViewPosition(l_sort_filter_rest, animatorListener, ResturentListViewImage.this);
				}
				
				@Override
				public void onAnimationCancel(Animator animation) {
					// TODO Auto-generated method stub
					
				}
			};
			
			if (loadFirstTime) {
				AnimationTween.animateView(findViewById(R.id.main_custom_progress), animatorListener2, ResturentListViewImage.this);
			}else{
				AnimationTween.animateView(findViewById(R.id.main_custom_progress), ResturentListViewImage.this);
			}
				
			
			
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					*//**
					 * Updating parsed Places into LISTVIEW
					 * *//*
					// Get json response status
				

					if (resturentListTemp == null || resturentListTemp.size() <= 0) {

						
						
						String lat=getIntent().getStringExtra("Lat");
						String lng=getIntent().getStringExtra("Lng");
						
						if( lat!=null && lng!=null && !lat.equals("") && !lng.equals("")  ){
							finish();
							Intent in = new Intent(getApplicationContext(),NearByplaceListViewDivision.class);
							in.putExtra( "Lat" , lat); 
							in.putExtra( "Lng" , lng);
							in.putExtra("LocName", getIntent().getStringExtra("LocName"));
							startActivity(in);
						}else{
							
							//show message
							findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);
							View v = findViewById(R.id.main_custom_progress);
							TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
							textPreparing.setText("Nothing found.");
							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
							
							AnimationTween.animateViewPosition(findViewById(R.id.main_custom_progress), ResturentListViewImage.this);

						}
						
						
						
						 
					} else if (resturentListTemp != null) {
						// Check for all possible status

						// Successfully got places details
						if (resturentListTemp != null) {
							
							//search suggestion
							ArrayList<SearchResturent> searchRestList= new ArrayList<SearchResturent>(); 
							
							// loop through each place
							for (Restaurent p : resturentListTemp) {
								
								if(resturentList == null)
									resturentList = new ArrayList<Restaurent>();
								
								//set open close info. 
								p.setOpenOrClose(CF.openOrClose(p.Open, p.Close));
								//add to main restaurant list
								resturentList.add(p);

								HashMap<String, String> map = new HashMap<String, String>();
								map.put("Id", p.Id);								
								map.put("RestName", p.RestName);
								map.put("RestLoc", p.RestLoc+", "+p.RestDistName);
								
								map.put("Id", p.Id);
								map.put("RestName", p.RestName);
								map.put("Rating", p.Rating);
								map.put("DisFrom", p.RestLoc+", "+p.RestDistName);
								
								map.put("DisAmount", ""+CF.openOrClose(p.Open, p.Close) );
								map.put("IMG", p.IMG);
								
								totalPage=Integer.parseInt(p.PageNo);

								System.out.println(">>>>>>>>>>test>>>>>>>>>>>>>>>>>> "+ p.RestLoc);

								// adding HashMap to ArrayList
								resturantListItems.add(map);
								
								//search suggestion
								searchRestList.add(new SearchResturent(p.Id, p.RestName, null, p.RestLoc, null, null, null));

							}
							
							//search suggestion
							addDataToDB(searchRestList);
							
							// list adapter
							//MyListAdapter adapter = new MyListAdapter(ResturentListView.this,resturantListItems,new String[] { "RestName", "RestLoc", "Id" });
							
							//ListAdapterRestaurantImage adapter= new ListAdapterRestaurantImage(ResturentListViewImage.this, resturantListItems, new String[]{"RestName","DisFrom","Rating","DisAmount","IMG"});
							
							RestaurantAdapter adapter= new RestaurantAdapter(ResturentListViewImage.this, resturentList);
							
							// Adding data into listview
							lv.setAdapter(adapter);
							adapter.notifyDataSetChanged();
							
							if(!loadFirstTime){
								//lv.scrollBy(0, getScroll());
								//lv.smoothScrollToPosition(totoalItemCountForScroll);
								lv.setSelection(totalItemCountForScroll-1);
							}
							
							currentPage++;
							
							lv.setOnScrollListener(new OnScrollListener() {
								
								@Override
								public void onScrollStateChanged(AbsListView view, int scrollState) {
									// TODO Auto-generated method stub
									
								}
								
								@Override
								public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
									// TODO Auto-generated method stub
									boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
									totalItemCountForScroll=totalItemCount;

								        if(loadMore && currentPage<totalPage) {
								        	Toast.makeText(getApplicationContext(), "Load more...", Toast.LENGTH_SHORT).show();
								        	loadFirstTime=false;
								        	new LoadPlaces().execute();
								        	
								        }
									}
							});
							
							
							
							
							
						}

						else if (resturentList == null) {
							//show message
							findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);
							View v = findViewById(R.id.main_custom_progress);
							TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
							textPreparing.setText("Nothing found.");
							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
							AnimationTween.animateViewPosition(findViewById(R.id.main_custom_progress), ResturentListViewImage.this);

						}
					}
				}

			});

		}

	}*/
	
	
	StringRequest postRequest = new StringRequest(Request.Method.POST, LetsEatRequest.RESTURENT_LIST_URL,
	        new Response.Listener<String>() {
		
			ArrayList<Restaurent> resturentListTemp;
			
	            @Override
	            public void onResponse(String response) {
	                try {
	                	// Result handling 
	                resturentListTemp= new LetsEatRequest().getRsturent(response);

						if (loadFirstTime) {
							loadFirstTime=false;
	        			// dismiss the dialog after getting all products
	        			//pDialog.dismiss();
	        			//findViewById(R.id.main_custom_progress).setVisibility(View.GONE);
	        			final Animator.AnimatorListener animatorListener= new Animator.AnimatorListener() {
	        				
	        				@Override
	        				public void onAnimationStart(Animator animation) {
	        					// TODO Auto-generated method stub
	        					
	        				}
	        				
	        				@Override
	        				public void onAnimationRepeat(Animator animation) {
	        					// TODO Auto-generated method stub
	        					
	        				}
	        				
	        				@Override
	        				public void onAnimationEnd(Animator animation) {
	        					// TODO Auto-generated method stub
	        					lv.setVisibility(View.VISIBLE);
	        					AnimationTween.animateViewPosition(lv, ResturentListViewImage.this);
	        				}
	        				
	        				@Override
	        				public void onAnimationCancel(Animator animation) {
	        					// TODO Auto-generated method stub
	        					
	        				}
	        			};
	        			
	        			Animator.AnimatorListener animatorListener2= new Animator.AnimatorListener() {
	        				
	        				@Override
	        				public void onAnimationStart(Animator animation) {
	        					// TODO Auto-generated method stub
	        					
	        				}
	        				
	        				@Override
	        				public void onAnimationRepeat(Animator animation) {
	        					// TODO Auto-generated method stub
	        					
	        				}
	        				
	        				@Override
	        				public void onAnimationEnd(Animator animation) {
	        					// TODO Auto-generated method stub
	        					//txtTitleNearBy.setVisibility(View.VISIBLE);
	        					
	        					l_sort_filter_rest.setVisibility(View.VISIBLE);					
	        					AnimationTween.animateViewPosition(l_sort_filter_rest, animatorListener, ResturentListViewImage.this);
	        				}
	        				
	        				@Override
	        				public void onAnimationCancel(Animator animation) {
	        					// TODO Auto-generated method stub
	        					
	        				}
	        			};
	        			


	        				AnimationTween.animateView(findViewById(R.id.main_custom_progress), animatorListener2, ResturentListViewImage.this);
	        			}else{
	        				//AnimationTween.animateView(findViewById(R.id.main_custom_progress), ResturentListViewImage.this);
	        			}
	        				
	        			
	        			
	        			// updating UI from Background Thread
	        			runOnUiThread(new Runnable() {
	        				

							public void run() {
	        					/**
	        					 * Updating parsed Places into LISTVIEW
	        					 * */
	        					// Get json response status
	        				

	        					if (resturentListTemp == null || resturentListTemp.size() <= 0) {

	        						
	        						
	        						String lat=getIntent().getStringExtra("Lat");
	        						String lng=getIntent().getStringExtra("Lng");
	        						
	        						if( lat!=null && lng!=null && !lat.equals("") && !lng.equals("")  ){
	        							finish();
	        							Intent in = new Intent(getApplicationContext(),NearByplaceListViewDivision.class);
	        							in.putExtra( "Lat" , lat); 
	        							in.putExtra( "Lng" , lng);
	        							in.putExtra("LocName", getIntent().getStringExtra("LocName"));
	        							startActivity(in);
	        						}else{
	        							
	        							//show message
	        							findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);
	        							View v = findViewById(R.id.main_custom_progress);
	        							TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
	        							textPreparing.setText("Nothing found.");
	        							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
	        							
	        							AnimationTween.animateViewPosition(findViewById(R.id.main_custom_progress), ResturentListViewImage.this);

	        						}
	        						
	        						
	        						
	        						 
	        					} else if (resturentListTemp != null) {
	        						// Check for all possible status

	        						// Successfully got places details
	        						if (resturentListTemp != null) {
	        							
	        							if(!isFoooterAdded && Integer.parseInt(resturentListTemp.get(0).PageNo)>0){
	        								LayoutInflater li = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        								footerView = li.inflate(R.layout.footer_load_more, null);
											footerView.findViewById(R.id.seeMoreProgressBar).setVisibility(View.INVISIBLE);
											footerView.findViewById(R.id.txtseeMore).setVisibility(View.VISIBLE);
	        								lv.addFooterView(footerView);
	        								isFoooterAdded = true;

											footerView.setOnClickListener(new View.OnClickListener(){
	        							        @Override
	        							        public void onClick(View v) {
	        							        	//currentPage
													v.findViewById(R.id.seeMoreProgressBar).setVisibility(View.VISIBLE);
													v.findViewById(R.id.txtseeMore).setVisibility(View.INVISIBLE);
	        							            loadData();
	        							        }
	        							    });
	        							}

										if(isFoooterAdded){

											footerView.findViewById(R.id.seeMoreProgressBar).setVisibility(View.INVISIBLE);
											footerView.findViewById(R.id.txtseeMore).setVisibility(View.VISIBLE);

										}
	        							
	        							//search suggestion
	        							ArrayList<SearchResturent> searchRestList= new ArrayList<SearchResturent>(); 
	        							
	        							// loop through each place
	        							for (Restaurent p : resturentListTemp) {
	        								
	        								if(resturentList == null)
	        									resturentList = new ArrayList<Restaurent>();
	        								
	        								//set open close info. 
	        								p.setOpenOrClose(CF.openOrClose(p.Open, p.Close));
	        								//add to main restaurant list
	        								resturentList.add(p);

	        								HashMap<String, String> map = new HashMap<String, String>();
	        								/*map.put("Id", p.Id);								
	        								map.put("RestName", p.RestName);
	        								map.put("RestLoc", p.RestLoc+", "+p.RestDistName);*/
	        								
	        								map.put("Id", p.Id);
	        								map.put("RestName", p.RestName);
	        								map.put("Rating", p.Rating);
	        								map.put("DisFrom", p.RestLoc+", "+p.RestDistName);
	        								
	        								map.put("DisAmount", ""+CF.openOrClose(p.Open, p.Close) );
	        								map.put("IMG", p.IMG);
	        								
	        								totalPage=Integer.parseInt(p.PageNo);

	        								System.out.println(">>>>>>>>>>test>>>>>>>>>>>>>>>>>> "+ p.RestLoc);

	        								// adding HashMap to ArrayList
	        								resturantListItems.add(map);
	        								
	        								//search suggestion
	        								searchRestList.add(new SearchResturent(p.Id, p.RestName, null, p.RestLoc, null, null, null));

	        							}
	        							
	        							//search suggestion
	        							addDataToDB(searchRestList);
	        							
	        							// list adapter
	        							//MyListAdapter adapter = new MyListAdapter(ResturentListView.this,resturantListItems,new String[] { "RestName", "RestLoc", "Id" });
	        							
	        							//ListAdapterRestaurantImage adapter= new ListAdapterRestaurantImage(ResturentListViewImage.this, resturantListItems, new String[]{"RestName","DisFrom","Rating","DisAmount","IMG"});
	        							
	        							RestaurantAdapter adapter= new RestaurantAdapter(ResturentListViewImage.this, resturentList);
	        							
	        							// Adding data into listview
	        							lv.setAdapter(adapter);
	        							adapter.notifyDataSetChanged();
	        							
	        							
	        							
	        							if(!loadFirstTime){
	        								//lv.scrollBy(0, getScroll());
	        								//lv.smoothScrollToPosition(totoalItemCountForScroll);
	        								lv.setSelection(totalItemCountForScroll-1);
	        							}
	        							
	        							currentPage++;
	        							
	        							lv.setOnScrollListener(new OnScrollListener() {
	        								
	        								@Override
	        								public void onScrollStateChanged(AbsListView view, int scrollState) {
	        									// TODO Auto-generated method stub
	        									
	        								}
	        								
	        								@Override
	        								public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
	        									// TODO Auto-generated method stub
	        									boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
	        									totalItemCountForScroll=totalItemCount;

	        								        if(loadMore && currentPage<totalPage) {
	        								        	/*Toast.makeText(getApplicationContext(), "Load more...", Toast.LENGTH_SHORT).show();
	        								        	loadFirstTime=false;
	        								        	loadData();*/
	        								        	
	        								        	
	        								        }
	        									}
	        							});
	        							
	        							
	        							
	        							
	        							
	        						}

	        						else if (resturentList == null) {
	        							//show message
	        							findViewById(R.id.main_custom_progress).setVisibility(View.VISIBLE);
	        							View v = findViewById(R.id.main_custom_progress);
	        							TextView textPreparing = (TextView) v.findViewById(R.id.textPreparing);
	        							textPreparing.setText("Nothing found.");
	        							v.findViewById(R.id.progressBarPreparing).setVisibility(View.GONE);
	        							AnimationTween.animateViewPosition(findViewById(R.id.main_custom_progress), ResturentListViewImage.this);

	        						}
	        					}
	        				}

	        			});

	        		
	                	
	                	
	                } catch (Exception e) {
	                	
	                	
	                    e.printStackTrace();
	                }
	            }
	        },
	        new Response.ErrorListener() {
	            @Override
	            public void onErrorResponse(VolleyError error) {
	                error.printStackTrace();
	            }
	        }
	) {
	    @Override
	    protected Map<String, String> getParams()
	    {
	        Map<String, String>  params = new HashMap<String, String>();
	        // the POST parameters: 
	        params.put("LocId", LocId);
	        params.put("Page", ""+currentPage);
	        return params;
	    }
	};
	
	
	
	
	
	

	@Override
	public boolean onNavigationItemSelected(int arg0, long arg1) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	private void showPopUp() {

		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
		// helpBuilder.setTitle("Pop Up");
		helpBuilder.setMessage("Nothing Found. Want to request this suggestion?");
		helpBuilder.setPositiveButton("No",
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {

					}
				});

		helpBuilder.setNegativeButton("Yes",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						Intent in = new Intent(
								getApplicationContext(),
								SuggetionRequest.class);
						in.putExtra("keyword", searchKeywords);

						startActivity(in);
					}
				});

		// Remember, create doesn't show the dialog
		AlertDialog helpDialog = helpBuilder.create();
		helpDialog.show();

	}

	
}
