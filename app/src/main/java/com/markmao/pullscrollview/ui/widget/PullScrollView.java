package com.markmao.pullscrollview.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.ScrollView;

import com.mcc.letseat.R;
/**
 * è‡ªå®šä¹‰ScrollView
 *
 * @author markmjw
 * @date 2013-09-13
 */
public class PullScrollView extends ScrollView {
    /** é˜»å°¼ç³»æ•°,è¶Šå°�é˜»åŠ›å°±è¶Šå¤§. */
    private static final float SCROLL_RATIO = 0.5f;

    /** æ»‘åŠ¨è‡³ç¿»è½¬çš„è·�ç¦». */
    private static final int TURN_DISTANCE = 100;

    /** å¤´éƒ¨view. */
    private View mHeader;

    /** å¤´éƒ¨viewé«˜åº¦. */
    private int mHeaderHeight;

    /** å¤´éƒ¨viewæ˜¾ç¤ºé«˜åº¦. */
    private int mHeaderVisibleHeight;

    /** ScrollViewçš„content view. */
    private View mContentView;

    /** ScrollViewçš„content viewçŸ©å½¢. */
    private Rect mContentRect = new Rect();

    /** é¦–æ¬¡ç‚¹å‡»çš„Yå��æ ‡. */
    private float mTouchDownY;

    /** æ˜¯å�¦å…³é—­ScrollViewçš„æ»‘åŠ¨. */
    private boolean mEnableTouch = false;

    /** æ˜¯å�¦å¼€å§‹ç§»åŠ¨. */
    private boolean isMoving = false;

    /** æ˜¯å�¦ç§»åŠ¨åˆ°é¡¶éƒ¨ä½�ç½®. */
    private boolean isTop = false;

    /** å¤´éƒ¨å›¾ç‰‡åˆ�å§‹é¡¶éƒ¨å’Œåº•éƒ¨. */
    private int mInitTop, mInitBottom;

    /** å¤´éƒ¨å›¾ç‰‡æ‹–åŠ¨æ—¶é¡¶éƒ¨å’Œåº•éƒ¨. */
    private int mCurrentTop, mCurrentBottom;

    /** çŠ¶æ€�å�˜åŒ–æ—¶çš„ç›‘å�¬å™¨. */
    private OnTurnListener mOnTurnListener;

    private enum State {
        /**é¡¶éƒ¨*/
        UP,
        /**åº•éƒ¨*/
        DOWN,
        /**æ­£å¸¸*/
        NORMAL
    }

    /** çŠ¶æ€�. */
    private State mState = State.NORMAL;

    public PullScrollView(Context context) {
        super(context);
        init(context, null);
    }

    public PullScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public PullScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        // set scroll mode
        setOverScrollMode(OVER_SCROLL_NEVER);

        if (null != attrs) {
            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.PullScrollView);

            if (ta != null) {
                mHeaderHeight = (int) ta.getDimension(R.styleable.PullScrollView_headerHeight, -1);
                mHeaderVisibleHeight = (int) ta.getDimension(R.styleable
                        .PullScrollView_headerVisibleHeight, -1);
                ta.recycle();
            }
        }
    }

    /**
     * è®¾ç½®Header
     *
     * @param view
     */
    public void setHeader(View view) {
        mHeader = view;
    }

    /**
     *
     *
     * @param turnListener
     */
    public void setOnTurnListener(OnTurnListener turnListener) {
        mOnTurnListener = turnListener;
    }

    @Override
    protected void onFinishInflate() {
        if (getChildCount() > 0) {
            mContentView = getChildAt(0);
        }
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);

        if (getScrollY() == 0) {
            isTop = true;
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            mTouchDownY = ev.getY();
            mCurrentTop = mInitTop = mHeader.getTop();
            mCurrentBottom = mInitBottom = mHeader.getBottom();
        }
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (mContentView != null) {
            doTouchEvent(ev);
        }

        // ç¦�æ­¢æŽ§ä»¶æœ¬èº«çš„æ»‘åŠ¨.
        return mEnableTouch || super.onTouchEvent(ev);
    }

    /**
     * è§¦æ‘¸äº‹ä»¶å¤„ç�†
     *
     * @param event
     */
    private void doTouchEvent(MotionEvent event) {
        int action = event.getAction();

        switch (action) {
            case MotionEvent.ACTION_MOVE:
                doActionMove(event);
                break;

            case MotionEvent.ACTION_UP:
                //
                if (isNeedAnimation()) {
                    rollBackAnimation();
                }

                if (getScrollY() == 0) {
                    mState = State.NORMAL;
                }

                isMoving = false;
                mEnableTouch = false;
                break;

            default:
                break;
        }
    }

    /**
     * 
     *
     * @param event
     */
    private void doActionMove(MotionEvent event) {
        //
        if (getScrollY() == 0) {
            mState = State.NORMAL;

            // 
            if (isTop) {
                isTop = false;
                mTouchDownY = event.getY();
            }
        }

        float deltaY = event.getY() - mTouchDownY;

        // å¯¹äºŽé¦–æ¬¡Touchæ“�ä½œè¦�åˆ¤æ–­æ–¹ä½�ï¼šUP OR DOWN
        if (deltaY < 0 && mState == State.NORMAL) {
            mState = State.UP;
        } else if (deltaY > 0 && mState == State.NORMAL) {
            mState = State.DOWN;
        }

        if (mState == State.UP) {
            deltaY = deltaY < 0 ? deltaY : 0;

            isMoving = false;
            mEnableTouch = false;

        } else if (mState == State.DOWN) {
            if (getScrollY() <= deltaY) {
                mEnableTouch = true;
                isMoving = true;
            }
            deltaY = deltaY < 0 ? 0 : deltaY;
        }

        if (isMoving) {
            // åˆ�å§‹åŒ–content viewçŸ©å½¢
            if (mContentRect.isEmpty()) {
                // ä¿�å­˜æ­£å¸¸çš„å¸ƒå±€ä½�ç½®
                mContentRect.set(mContentView.getLeft(), mContentView.getTop(), mContentView.getRight(),
                        mContentView.getBottom());
            }

            // è®¡ç®—headerç§»åŠ¨è·�ç¦»(æ‰‹åŠ¿ç§»åŠ¨çš„è·�ç¦»*é˜»å°¼ç³»æ•°*0.5)
            float headerMoveHeight = deltaY * 0.5f * SCROLL_RATIO;
            mCurrentTop = (int) (mInitTop + headerMoveHeight);
            mCurrentBottom = (int) (mInitBottom + headerMoveHeight);

            // è®¡ç®—contentç§»åŠ¨è·�ç¦»(æ‰‹åŠ¿ç§»åŠ¨çš„è·�ç¦»*é˜»å°¼ç³»æ•°)
            float contentMoveHeight = deltaY * SCROLL_RATIO;

            // ä¿®æ­£contentç§»åŠ¨çš„è·�ç¦»ï¼Œé�¿å…�è¶…è¿‡headerçš„åº•è¾¹ç¼˜
            int headerBottom = mCurrentBottom - mHeaderVisibleHeight;
            int top = (int) (mContentRect.top + contentMoveHeight);
            int bottom = (int) (mContentRect.bottom + contentMoveHeight);

            if (top <= headerBottom) {
                // ç§»åŠ¨content view
                mContentView.layout(mContentRect.left, top, mContentRect.right, bottom);

                // ç§»åŠ¨header view
                mHeader.layout(mHeader.getLeft(), mCurrentTop, mHeader.getRight(), mCurrentBottom);
            }
        }
    }

    private void rollBackAnimation() {
        TranslateAnimation tranAnim = new TranslateAnimation(0, 0,
                Math.abs(mInitTop - mCurrentTop), 0);
        tranAnim.setDuration(200);
        mHeader.startAnimation(tranAnim);

        mHeader.layout(mHeader.getLeft(), mInitTop, mHeader.getRight(), mInitBottom);

        //
        TranslateAnimation innerAnim = new TranslateAnimation(0, 0, mContentView.getTop(), mContentRect.top);
        innerAnim.setDuration(200);
        mContentView.startAnimation(innerAnim);
        mContentView.layout(mContentRect.left, mContentRect.top, mContentRect.right, mContentRect.bottom);

        mContentRect.setEmpty();

        // å›žè°ƒç›‘å�¬å™¨
        if (mCurrentTop > mInitTop + TURN_DISTANCE && mOnTurnListener != null){
            mOnTurnListener.onTurn();
        }
    }

    /**
     * 
     */
    private boolean isNeedAnimation() {
        return !mContentRect.isEmpty() && isMoving;
    }

    /**
     * 
     *
     * @author markmjw
     */
    public interface OnTurnListener {
        /**
         * 
         */
        public void onTurn();
    }
}
