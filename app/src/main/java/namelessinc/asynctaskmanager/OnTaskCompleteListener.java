package namelessinc.asynctaskmanager;

public interface OnTaskCompleteListener {
    // Notifies about task completeness
    void onTaskComplete(Task task);
}