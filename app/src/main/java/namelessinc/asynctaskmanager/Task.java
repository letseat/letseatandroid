package namelessinc.asynctaskmanager;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager.OnCancelListener;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;

public class Task extends AsyncTask<Void, String, Boolean> {
    
    protected final Resources mResources;
    
    private Boolean mResult;
    private String mProgressMessage;
    private IProgressTracker mProgressTracker;
    public ProgressDialog pd;
Context ctx;
    /* UI Thread */
    public Task(Context ctx,Resources resources, String start) {
    	this.ctx=ctx;
	// Keep reference to resources
	mResources = resources;
	// Initialise initial pre-execute message
	mProgressMessage = new String(start) ;
    }
    
	protected void onPreExecute(){
		if(ctx!=null){
		pd = new ProgressDialog(ctx);
		pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		pd.setMessage("Uploading...");
		pd.setCancelable(true);
		pd.show();}
	}
	
    /* UI Thread */
    public void setProgressTracker(IProgressTracker progressTracker) {
	// Attach to progress tracker
	mProgressTracker = progressTracker;
	// Initialise progress tracker with current task state
	if (mProgressTracker != null) {
	    mProgressTracker.onProgress(mProgressMessage);
	    if (mResult != null) {
		mProgressTracker.onComplete();
	    }
	}
    }

    /* UI Thread */
    @Override
    protected void onCancelled() {
	// Detach from progress tracker
	mProgressTracker = null;
	if(ctx!=null && pd!=null)
	pd.cancel();
    }
    
    /* UI Thread */
    @Override
    protected void onProgressUpdate(String... values) {
	// Update progress message 
	mProgressMessage = values[0];
	// And send it to progress tracker
	if (mProgressTracker != null) {
	    mProgressTracker.onProgress(mProgressMessage);
	}
    }

    /* UI Thread */
    @Override
    protected void onPostExecute(Boolean result) {
    	
    	
	// Update result
	mResult = result;
	// And send it to progress tracker
	if (mProgressTracker != null) {
	    mProgressTracker.onComplete();
	}
	// Detach from progress tracker
	mProgressTracker = null;
	if(ctx!=null && pd!=null && pd.isShowing())
	{	pd.dismiss();
	((Activity) ctx).finish();}
    }

    public void updateProgress(int per) {
    	if(ctx!=null && pd!=null)
    	pd.setProgress(per);
		
	}
    /* Separate Thread */
    @Override
    protected Boolean doInBackground(Void... arg0) {
	// Working in separate thread
	for (int i = 10; i > 0; --i)
	{
	    // Check if task is cancelled
	    if (isCancelled()) {
		// This return causes onPostExecute call on UI thread
		return false;
	    }

	    try {
		// This call causes onProgressUpdate call on UI thread
		//publishProgress(mResources.getString(com.mnm.asynctaskmanager.R.string.task_working, i));
		Thread.sleep(1000);
	    } catch (InterruptedException e) {
		e.printStackTrace();
		// This return causes onPostExecute call on UI thread
		return false;
	    }
	}
	// This return causes onPostExecute call on UI thread
	return true;
    }

	
	
}