package info.androidhive.tabsswipe.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mcc.letseat.tab.DealsFragment;
import com.mcc.letseat.tab.InfoFragment;
import com.mcc.letseat.tab.MapFragment;
import com.mcc.letseat.tab.MenuFragment;
import com.mcc.letseat.tab.PhotoFragment;
import com.mcc.letseat.tab.ReviewFragment;

/**
 * FragmentPagerAdapter for restaurant details (TabMainActivity) 
 * @author Arif
 *
 */
public class TabsPagerAdapter extends FragmentPagerAdapter {
	
	Bundle bundle;
	String [] tabs;

	public TabsPagerAdapter(FragmentManager fm) {
		super(fm);
	}
	
	public TabsPagerAdapter(FragmentManager fm, Bundle bundle, String[] tabs) {
		super(fm);
		this.bundle=bundle;
		this.tabs=tabs;
	}
	
	InfoFragment infoFragment;

	@Override
	public Fragment getItem(int index) {

		switch (index) {
		case 0:
			if(infoFragment==null){
				infoFragment = new InfoFragment();	
			}
			 
			infoFragment.setArguments(bundle);
			return infoFragment;
			
		case 1:
			 DealsFragment dealsFragment=new DealsFragment();
			 dealsFragment.setArguments(bundle);
			return dealsFragment;
			
		case 2:
			MenuFragment menuFragment=new MenuFragment();
			menuFragment.setArguments(bundle);
			return menuFragment;
			
		case 3:
			 MapFragment mapFragment=new MapFragment();
			 mapFragment.setArguments(bundle);
			return mapFragment;
			
		case 4:			
			PhotoFragment photoFragment=new PhotoFragment();
			photoFragment.setArguments(bundle);
			return photoFragment;
			
		case 5:
			ReviewFragment reviewFragment= new ReviewFragment();
			reviewFragment.setArguments(bundle);
			return reviewFragment;
			
		
		
		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return tabs.length;
	}

}
