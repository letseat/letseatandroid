# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Users\Arif\AppData\Local\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# To enable ProGuard in your project, edit project.properties
# to define the proguard.config property as described in that file.
#
# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in ${sdk.dir}/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the ProGuard
# include property in project.properties.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

#-libraryjars /libs/acra-4.5.0.jar
#-libraryjars /libs/android-support-v4.jar
#-libraryjars /libs/apache-mime4j-0.6.jar
#-libraryjars /libs/google-api-client-1.10.3-beta.jar
#-libraryjars /libs/google-api-client-android2-1.10.3-beta.jar
#-libraryjars /libs/google-http-client-1.10.3-beta.jar
#-libraryjars /libs/google-http-client-android2-1.10.3-beta.jar
#-libraryjars /libs/google-oauth-client-1.10.1-beta.jar
#-libraryjars /libs/gson-2.1.jar

#-libraryjars /libs/httpmime-4.0.jar
#-libraryjars /libs/jackson-core-asl-1.9.4.jar

#-libraryjars /libs/org.apache.httpcomponents.httpcore_4.2.2.jar
#-libraryjars /libs/picasso-2.5.2.jar
#-libraryjars /libs/protobuf-java-2.2.0.jar
#-libraryjars /libs/volley.jar





-dontpreverify
-repackageclasses ''
-allowaccessmodification
-optimizations !code/simplification/arithmetic
-keepattributes *Annotation*

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider

-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
    public void set*(...);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers class * extends android.content.Context {
   public void *(android.view.View);
   public void *(android.view.MenuItem);
}

-keepclassmembers class * implements android.os.Parcelable {
    static ** CREATOR;
}

-keepclassmembers class **.R$* {
    public static <fields>;
}

-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}




-dontwarn com.google.**
-keep class com.google.** {*;}

-dontwarn org.apache.**
-keep class org.apache.** {*;}


-dontwarn com.squareup.picasso.**
-keep class com.squareup.picasso.** {*;}

-dontwarn com.foody.**
-keep class com.foody.** {*;}

-dontwarn com.mcc.**
-keep class com.mcc.** {*;}

-dontwarn com.facebook.**
-keep class com.facebook.** {*;}

-dontwarn com.emilsjolander.components.StickyScrollViewItems.**
-keep class com.emilsjolander.components.StickyScrollViewItems.** {*;}


-dontwarn javax.jdo.**
-dontwarn com.google.api.client.googleapis.extensions.android.gms.**


# Keep all the ACRA classes
-keep class org.acra.** { *; }

-keep class android.support.** { *; }

-keep class info.androidhive.tabsswipe.adapter.** { *; }


-ignorewarnings



# ProGuard Configuration file
#
# See http://proguard.sourceforge.net/index.html#manual/usage.html

# Needed to keep generic types and @Key annotations accessed via reflection

-keepattributes Signature,RuntimeVisibleAnnotations,AnnotationDefault

-keepclassmembers class * {
  @com.google.api.client.util.Key <fields>;
}

# Needed by google-http-client-android when linking against an older platform version

-dontwarn com.google.api.client.extensions.android.**

# Needed by google-api-client-android when linking against an older platform version

-dontwarn com.google.api.client.googleapis.extensions.android.**

# Needed by google-play-services when linking against an older platform version

-dontwarn com.google.android.gms.**
-dontnote com.google.android.gms.**

# com.google.client.util.IOUtils references java.nio.file.Files when on Java 7+
-dontnote java.nio.file.Files, java.nio.file.Path

# Suppress notes on LicensingServices
-dontnote **.ILicensingService

# Suppress warnings on sun.misc.Unsafe
-dontnote sun.misc.Unsafe
-dontwarn sun.misc.Unsafe



